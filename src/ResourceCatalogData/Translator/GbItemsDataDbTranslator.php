<?php
namespace BaseData\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\GbItemsData;
use BaseData\ResourceCatalogData\Model\NullGbItemsData;

class GbItemsDataDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $gbItemsData = null) : GbItemsData
    {
        if (!isset($expression['gb_items_data_id'])) {
            return NullGbItemsData::getInstance();
        }

        if ($gbItemsData == null) {
            $gbItemsData = new GbItemsData();
        }

        $gbItemsData->setId($expression['gb_items_data_id']);

        if (isset($expression['data'])) {
            $gbItemsData->setData(unserialize(gzuncompress(base64_decode($expression['data'], true))));
        }

        return $gbItemsData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($gbItemsData, array $keys = array()) : array
    {
        if (!$gbItemsData instanceof GbItemsData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'data'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['gb_items_data_id'] = $gbItemsData->getId();
        }
        if (in_array('data', $keys)) {
            $expression['data'] = base64_encode(gzcompress(serialize($gbItemsData->getData())));
        }

        return $expression;
    }
}

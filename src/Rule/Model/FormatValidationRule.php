<?php
namespace BaseData\Rule\Model;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Strategy\DataStrategyFactory;

class FormatValidationRule extends Rule implements IRule
{
    use RuleTrait;

    protected $dataStrategyFactory;

    public function __construct()
    {
        parent::__construct();
        $this->dataStrategyFactory = new DataStrategyFactory();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->dataStrategyFactory);
    }

    protected function getDataStrategyFactory() : DataStrategyFactory
    {
        return $this->dataStrategyFactory;
    }

    public function validate() : bool
    {
        return true;
    }

    //实现父类的抽象方法
    protected function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        unset($transformationSearchData);
        return true;
    }

    /**
     * 初始化数据(实现父类的抽象方法)
     * @param ISearchDataAble $sourceSearchData 来源资源目录数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * condition 规则验证条件
        * sourceItemsData 来源资源目录数据模板数据
     */
    protected function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {

        unset($transformationSearchData);
        //获取比对条件
        $condition = $this->getCondition();
        
        //获取来源资源目录数据信息项
        $sourceItemsData = $sourceSearchData->getItemsData()->getData();

        return array(
            'condition' => $condition,
            'sourceItemsData' => $sourceItemsData
        );
    }

    //验证初始化数据格式
    protected function validateInitializationDataFormat(array $data) : bool
    {
        return (isset($data['condition']) && isset($data['sourceItemsData']));
    }

    /**
     * 执行转换过程(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * transformationItemsData 目标资源目录数据模板数据
        * failureItems 格式不正确的数据信息项
     */
    protected function transformationExecute(array $data, ISearchDataAble $transformationSearchData) : array
    {
        //验证初始化数据格式,false返回空数组
        if (!$this->validateInitializationDataFormat($data)) {
            return array();
        }
        
        $failureItems = array();
        //将模板信息项和信息项数据组成新数组
        $itemsData = $this->combinationItemsData($data['condition'], $data['sourceItemsData']);
        //验证组合后的数据格式
        if (!$this->validateItemsDataFormat($itemsData)) {
            return array();
        }

        foreach ($itemsData as $identify => $itemData) {
            //验证必填项是否为空,验证单条数据项格式是否正确
            if ($this->isNecessaryItemEmpty($itemData) || !$this->validateItemDataFormat($itemData)) {
                $failureItems[$identify] = $itemData;
            }
        }

        return array(
            'failureItems' => $failureItems,
            'transformationItemsData' => $data['sourceItemsData']
        );
    }
    
    //将模板信息项和信息项数据组成新数组
    protected function combinationItemsData(array $items, array $sourceItemsData) : array
    {
        $itemsData = array();

        foreach ($items as $identify => $item) {
            $itemsData[$identify] = $item;
            $itemsData[$identify]['value'] = isset($sourceItemsData[$identify]) ? $sourceItemsData[$identify] : '';
        }

        return $itemsData;
    }

    //验证数据信息项的格式正确
    protected function validateItemsDataFormat(array $itemsData) : bool
    {
        foreach ($itemsData as $itemData) {
            if (!isset($itemData['isNecessary']) ||
                !isset($itemData['value']) ||
                !isset($itemData['type']) ||
                !isset($itemData['length']) ||
                !isset($itemData['options'])) {
                return false;
            }
        }

        return true;
    }

    //验证必填项的值是否为空
    protected function isNecessaryItemEmpty(array $itemData) : bool
    {
        return ($itemData['isNecessary'] == Template::IS_NECESSARY['SHI'] && empty($itemData['value']));
    }

    //验证单条数据信息项格式
    protected function validateItemDataFormat(array $itemData) : bool
    {
        $strategy = $this->getDataStrategyFactory()->getStrategy($itemData['type']);

        return $strategy->validate($itemData['value'], $itemData);
    }

    //验证执行后返回的数据格式
    protected function validateDataFormat(array $data) : bool
    {
        return (isset($data['failureItems']) && isset($data['transformationItemsData']));
    }

    /**
     * 判断并返回结果(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return ISearchDataAble
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function transformationResult(array $data, ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        if ($transformationSearchData instanceof ErrorData) {
            $transformationSearchData->setErrorType(0);
            $transformationSearchData->setErrorReason(array());
        }
        //验证数据格式不正确,直接返回异常失败的目标资源目录数据
        if (!$this->validateDataFormat($data)) {
            $transformationSearchData = $this->failureDataException(
                $transformationSearchData,
                ErrorData::STATUS['PROGRAM_EXCEPTION']
            );
        }

        $failureItems = isset($data['failureItems']) ? $data['failureItems'] : array();
        $transformationItemsData = isset($data['transformationItemsData']) ? $data['transformationItemsData'] : array();
        //判断比对失败的资源目录信息项是否为空,为空则返回失败资源目录数据,不为空则返回完善的资源目录数据
        if (empty($failureItems) && !$transformationSearchData instanceof FailureData) {
            $transformationSearchData = $this->transformationSearchData($transformationSearchData);
        }

        if (empty($failureItems) && $transformationSearchData instanceof FailureData) {
            $transformationSearchData = $transformationSearchData;
        }

        if (!empty($failureItems)) {
            $transformationSearchData =  $this->failureData(
                $transformationSearchData,
                ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED'],
                $failureItems
            );
        }

        $transformationSearchData->getItemsData()->setData($transformationItemsData);
        $transformationSearchData->generateHash();

        return $transformationSearchData;
    }
}

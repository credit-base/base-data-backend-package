<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件资源目录数据时,在政务网OA中,可以搜索我需要的所有资源目录数据,
 *           通过列表形式查看我搜索的所有资源目录数据 ,以便于我可以快速查看到我需要的资源目录数据
 * @Scenario: 委办局工作人员通过资源信息类别搜索
 */
class InfoCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_ys_items_data');
        $this->clear('pcore_ys_search_data');
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchYsSearchDataList()
    {
        $repository = new YsSearchDataRepository();
 
        $filter['infoCategory'] = ISearchDataAble::INFO_CATEGORY['SHOUXXX'];

        list($ysSearchDataList, $count) = $repository->filter($filter);
        
        unset($count);

        return $ysSearchDataList;
    }

    /**
     * @Then: 我可以看到信息分类为守信信息的所有资源目录数据
     */
    public function testViewYsSearchDataList()
    {
        $setYsSearchDataList = $this->ysSearchData()['pcore_ys_search_data'];

        foreach ($setYsSearchDataList as $key => $setYsSearchData) {
            unset($setYsSearchData);
            $setYsSearchDataList[$key]['ys_search_data_id'] = $key +1;
        }

        $ysSearchDataList = $this->fetchYsSearchDataList();
        $translator = new YsSearchDataDbTranslator();
        foreach ($ysSearchDataList as $ysSearchData) {
            $ysSearchDataArray[] = $translator->objectToArray($ysSearchData);
        }

        $this->assertEquals($ysSearchDataArray, $setYsSearchDataList);

        foreach ($ysSearchDataArray as $ysSearchData) {
            $this->assertEquals(ISearchDataAble::INFO_CATEGORY['SHOUXXX'], $ysSearchData['info_category']);
        }
    }
}

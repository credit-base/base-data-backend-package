<?php
namespace BaseData\WorkOrderTask\Model;

use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockParentTask extends ParentTask
{
    public function getRepository() : IParentTaskAdapter
    {
        return parent::getRepository();
    }

    public function getWorkOrderTask() : WorkOrderTask
    {
        return parent::getWorkOrderTask();
    }

    public function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return parent::getWorkOrderTaskRepository();
    }
    
    public function addActionPublic() : bool
    {
        return parent::addAction();
    }

    public function revokeActionPublic(string $reason) : bool
    {
        return parent::revokeAction($reason);
    }
    
    public function bindTaskPublic() : bool
    {
        return parent::bindTask();
    }
    
    public function revokeWorkOrderTaskPublic(string $reason) : bool
    {
        return parent::revokeWorkOrderTask($reason);
    }
    
    public function fetchWorkOrderTaskPublic() : array
    {
        return parent::fetchWorkOrderTask();
    }
}

<?php
namespace BaseData\Rule\RuleService\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\RuleUtils;
use BaseData\Rule\Model\RuleService;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试规则接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, RuleUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在多条规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_rule' => $this->rule()
            ]
        );
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function testViewRuleService()
    {
        $setRuleServiceList = $this->rule();

        $setGbTemplateList = $this->gbTemplate();
        foreach ($setGbTemplateList as $setGbTemplate) {
            $setGbTemplateList[$setGbTemplate['gb_template_id']] = $setGbTemplate;
        }

        $setWbjTemplateList = $this->wbjTemplate();
        foreach ($setWbjTemplateList as $setWbjTemplate) {
            $setWbjTemplateList[$setWbjTemplate['wbj_template_id']] = $setWbjTemplate;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'rules/1,2?include=transformationTemplate,sourceTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $ruleService) {
            $this->ruleCompareData($setRuleServiceList[$key], $ruleService);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'gbTemplates') {
                $this->gbTemplateCompareData($setGbTemplateList[$value['id']], $value);
            }
            if ($value['type'] == 'wbjTemplates') {
                $this->wbjTemplateCompareData($setWbjTemplateList[$value['id']], $value);
            }
        }
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

use BaseData\Enterprise\Adapter\Enterprise\EnterpriseDbAdapter;

class AdapterFactory
{
    const MAPS = array(
        ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'] =>
            '\BaseData\Enterprise\Adapter\Enterprise\EnterpriseDbAdapter',
        ISearchDataAble::SUBJECT_CATEGORY['GTGSH'] =>
            '\BaseData\Enterprise\Adapter\Enterprise\EnterpriseDbAdapter',
    );

    public function getAdapter(int $category)
    {
        $adapter = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';
        return class_exists($adapter) ? new $adapter : new EnterpriseDbAdapter();
    }
}

<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Command\ParentTask\AddParentTaskCommand;
use BaseData\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;

class ParentTaskCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new ParentTaskCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddParentTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddParentTaskCommand(
                $this->faker->randomElement(ParentTask::TEMPLATE_TYPE),
                $this->faker->word,
                $this->faker->word,
                $this->faker->word,
                array($this->faker->word),
                $this->faker->randomDigitNotNull,
                array($this->faker->randomDigit)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\CommandHandler\ParentTask\AddParentTaskCommandHandler',
            $commandHandler
        );
    }

    public function testRevokeParentTaskCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new RevokeParentTaskCommand(
                $this->faker->word,
                $this->faker->randomDigit
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\CommandHandler\ParentTask\RevokeParentTaskCommandHandler',
            $commandHandler
        );
    }
}

<?php
namespace BaseData\Enterprise\Adapter\Enterprise\Query\Persistence;

use Marmot\Framework\Classes\Db;

class EnterpriseDb extends Db
{
    const TABLE = 'enterprise';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

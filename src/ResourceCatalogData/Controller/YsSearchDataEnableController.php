<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use BaseData\Common\Controller\Interfaces\IEnableAbleController;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\View\YsSearchDataView;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;
use BaseData\ResourceCatalogData\Command\YsSearchData\EnableYsSearchDataCommand;
use BaseData\ResourceCatalogData\Command\YsSearchData\DisableYsSearchDataCommand;
use BaseData\ResourceCatalogData\CommandHandler\YsSearchData\YsSearchDataCommandHandlerFactory;

class YsSearchDataEnableController extends Controller implements IEnableAbleController
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new YsSearchDataRepository();
        $this->commandBus = new CommandBus(new YsSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : IYsSearchDataAdapter
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /ysSearchData/{id:\d+}/enable
     * 启用, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function enable(int $id)
    {
        if (!empty($id)) {
            $command = new EnableYsSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $ysSearchData  = $this->getRepository()->fetchOne($id);
                if ($ysSearchData instanceof YsSearchData) {
                    $this->render(new YsSearchDataView($ysSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /ysSearchData/{id:\d+}/disable
     * 禁用, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableYsSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $ysSearchData  = $this->getRepository()->fetchOne($id);
                if ($ysSearchData instanceof YsSearchData) {
                    $this->render(new YsSearchDataView($ysSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}

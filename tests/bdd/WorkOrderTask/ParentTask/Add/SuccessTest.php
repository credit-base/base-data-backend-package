<?php
namespace BaseData\WorkOrderTask\ParentTask\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\UserGroup\Model\UserGroup;

use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\SetDataTrait as TemplateSetDataTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要我指定的委办局根据我的资源目录模板上传数据时,在政务网OA中,可以给我指定的委办局指派我指定的资源目录模板
 *           资源目录模板以任务的形式指派委办局确认无误后会生成我指派的模板
 *           以便于委办局可以按照我的模板来填报数据
 * @Scenario: 新增父任务
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, TemplateSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_parent_task');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate()
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $templateRepository = new GbTemplateRepository();
        $gbTemplate = $templateRepository->fetchOne(1);

        $parentTaskArray = $this->parentTask()[0];

        $assignObjects = array();
        $assignObjectsTemp = json_decode($parentTaskArray['assign_objects'], true);

        foreach ($assignObjectsTemp as $assignObject) {
            $assignObjects[] = new UserGroup($assignObject);
        }

        $parentTask = new ParentTask();
        $parentTask->setTitle($parentTaskArray['title']);
        $parentTask->setDescription($parentTaskArray['description']);
        $parentTask->setEndTime($parentTaskArray['end_time']);
        $parentTask->setAttachment(json_decode($parentTaskArray['attachment'], true));
        $parentTask->setTemplateType($parentTaskArray['template_type']);
        $parentTask->setTemplate($gbTemplate);
        $parentTask->setAssignObjects($assignObjects);
        $parentTask->setFinishCount($parentTaskArray['finish_count']);
        $parentTask->setReason($parentTaskArray['reason']);
        
        return $parentTask->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setParentTask = $this->parentTask()[0];
        $setParentTask['parent_task_id'] = 1;
        $setParentTask['attachment'] = json_decode($setParentTask['attachment'], true);
        $setParentTask['assign_objects'] = json_decode($setParentTask['assign_objects'], true);
        
        $repository = new ParentTaskRepository();

        $parentTask = $repository->fetchOne($result);

        $translator = new ParentTaskDbTranslator();
        $parentTaskArray = $translator->objectToArray($parentTask);

        $this->assertEquals($parentTaskArray, $setParentTask);
    }
}

<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\MockTemplate;
use BaseData\Template\Model\MockBjTemplate;
use BaseData\Template\Model\MockGbTemplate;
use BaseData\Template\Model\MockWbjTemplate;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Model\SearchDataModelFactory;
use BaseData\Rule\Repository\SearchDataRepositoryFactory;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\MockErrorData;
use BaseData\ResourceCatalogData\Model\IncompleteData;
use BaseData\ResourceCatalogData\Model\MockSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

/**
 * @SuppressWarnings(PHPMD)
 * @todo 需要拆分大的类
 */
class RuleTraitTest extends TestCase
{
    private $ruleTrait;

    public function setUp()
    {
        $this->ruleTrait = new MockRuleTrait();
    }

    public function tearDown()
    {
        unset($this->ruleTrait);
    }

    public function testGetRepositoryFactory()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RepositoryFactory',
            $this->ruleTrait->getRepositoryFactoryPublic()
        );
    }

    public function testGetGbTemplateRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Repository\GbTemplateRepository',
            $this->ruleTrait->getGbTemplateRepositoryPublic()
        );
    }

    public function testGetGbSearchDataRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\GbSearchDataRepository',
            $this->ruleTrait->getGbSearchDataRepositoryPublic()
        );
    }

    public function testGetSearchDataRepositoryFactory()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\SearchDataRepositoryFactory',
            $this->ruleTrait->getSearchDataRepositoryFactoryPublic()
        );
    }

    public function testGetSearchDataModelFactory()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\SearchDataModelFactory',
            $this->ruleTrait->getSearchDataModelFactoryPublic()
        );
    }

    //templateItemComparison
    public function testTemplateItemComparison()
    {
        $transformationItem = ['transformationItem'];
        $sourceItem = ['sourceItem'];

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'dataTypeComparison',
                                    'dataLengthComparison',
                                    'dataDimensionComparison',
                                    'dataIsMaskedComparison'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataTypeComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(true);

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataLengthComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(true);

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataDimensionComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(true);

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataIsMaskedComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(false);

        $result = $ruleTrait->templateItemComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    public function testComparisonRuleComparison()
    {
        $transformationItem = ['transformationItem'];
        $sourceItem = ['sourceItem'];

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)
                           ->setMethods(
                               [
                                    'dataTypeComparison',
                                    'dataLengthComparison'
                                ]
                           )
                           ->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataTypeComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(true);

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataLengthComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(false);

        $result = $ruleTrait->comparisonRuleComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    //dataTypeComparison
    public function testDataTypeComparisonTrue()
    {
        $transformationItem['type'] = Template::TYPE['FDX'];//4
        $sourceItem['type'] = Template::TYPE['ZSX'];//1

        $result = $this->ruleTrait->dataTypeComparisonPublic($transformationItem, $sourceItem);
        $this->assertTrue($result);
    }

    public function testDataTypeComparisonFailTransformationTypeNotExist()
    {
        $transformationItem['type'] = 99;//4
        $sourceItem['type'] = Template::TYPE['ZSX'];//1

        $result = $this->ruleTrait->dataTypeComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    public function testDataTypeComparisonFailTypeOutOfRange()
    {
        $transformationItem['type'] = Template::TYPE['FDX'];//4
        $sourceItem['type'] = Template::TYPE['ZFX'];//1

        $result = $this->ruleTrait->dataTypeComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    //dataLengthComparison
    public function testDataLengthComparisonFailLength()
    {
        $transformationItem['length'] = 20;
        $sourceItem['length'] = 30;

        $result = $this->ruleTrait->dataLengthComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    public function testDataLengthComparisonTrueLength()
    {
        $transformationItem['length'] = 40;
        $sourceItem['length'] = 30;

        $result = $this->ruleTrait->dataLengthComparisonPublic($transformationItem, $sourceItem);
        $this->assertTrue($result);
    }

    public function testDataLengthComparisonTrueCollection()
    {
        $transformationItem['length'] = 40;
        $transformationItem['type'] = Template::TYPE['MJX'];
        $sourceItem['length'] = 30;

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)
                           ->setMethods(
                               [
                                    'dataOptionComparison'
                                ]
                           )
                           ->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataOptionComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(true);

        $result = $ruleTrait->dataLengthComparisonPublic($transformationItem, $sourceItem);
        $this->assertTrue($result);
    }

    //dataOptionComparison
    public function testDataOptionComparisonTrue()
    {
        $transformationItem['options'] = ["基础信息", "守信信息", "失信信息", "其他信息"];
        $sourceItem['options'] = ["守信信息", "失信信息"];

        $result = $this->ruleTrait->dataOptionComparisonPublic($transformationItem, $sourceItem);
        $this->assertTrue($result);
    }

    public function testDataOptionComparisonFalse()
    {
        $transformationItem['options'] = ["基础信息", "失信信息", "其他信息"];
        $sourceItem['options'] = ["守信信息", "失信信息"];

        $result = $this->ruleTrait->dataOptionComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    //dataDimensionComparison
    public function testDataDimensionComparisonTrue()
    {
        $transformationItem['dimension'] = Template::DIMENSION['ZWGX'];
        $sourceItem['dimension'] = Template::DIMENSION['SHGK'];

        $result = $this->ruleTrait->dataDimensionComparisonPublic($transformationItem, $sourceItem);
        $this->assertTrue($result);
    }

    public function testDataDimensionComparisonFail()
    {
        $transformationItem['dimension'] = Template::DIMENSION['SHGK'];
        $sourceItem['dimension'] = Template::DIMENSION['ZWGX'];

        $result = $this->ruleTrait->dataDimensionComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    //dataIsMaskedComparison
    public function testDataIsMaskedComparisonTrue()
    {
        $transformationItem['isMasked'] = Template::IS_MASKED['FOU'];
        $sourceItem['isMasked'] = Template::IS_MASKED['FOU'];

        $result = $this->ruleTrait->dataIsMaskedComparisonPublic($transformationItem, $sourceItem);
        $this->assertTrue($result);
    }

    public function testDataIsMaskedComparisonMaskFalse()
    {
        $transformationItem['isMasked'] = Template::IS_MASKED['FOU'];
        $sourceItem['isMasked'] = Template::IS_MASKED['SHI'];

        $result = $this->ruleTrait->dataIsMaskedComparisonPublic($transformationItem, $sourceItem);
        $this->assertFalse($result);
    }

    public function testDataIsMaskedComparisonMaskRuleComparisonFalse()
    {
        $transformationItem['isMasked'] = Template::IS_MASKED['SHI'];
        $sourceItem['isMasked'] = Template::IS_MASKED['SHI'];

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)
                           ->setMethods(
                               [
                                    'dataMaskRuleComparison'
                                ]
                           )
                           ->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('dataMaskRuleComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn(true);

        $result = $ruleTrait->dataIsMaskedComparisonPublic($transformationItem, $sourceItem);
        $this->assertTrue($result);
    }

    /**
     * dataMaskRuleComparison
     *
     * @dataProvider maskRuleProvider
     */
    public function testDataMaskRuleComparisonPublic($transformationItem, $sourceItem, $expected)
    {
        $this->assertEquals(
            $expected,
            $this->ruleTrait->dataMaskRuleComparisonPublic($transformationItem, $sourceItem)
        );
    }

    public function maskRuleProvider()
    {
        return [
            [['maskRule'=>[4,5]],['maskRule'=>[2,3]], false],
            [['maskRule'=>[1,5]],['maskRule'=>[2,3]], false],
            [['maskRule'=>[1,2]],['maskRule'=>[2,3]], true]
        ];
    }

    //fetchTemplateItems
    public function testFetchTemplateItems()
    {
        $template = new MockTemplate();

        $items = array(
                array(
                    "name" => '主体名称',    //信息项名称
                    "identify" => 'ZTMC',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '200',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(),    //脱敏规则
                    "remarks" => '信用主体名称',    //备注
                ),
                array(
                    "name" => '统一社会信用代码',    //信息项名称
                    "identify" => 'TYSHXYDM',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                    "remarks" => '信用主体代码',    //备注
                )
        );
        $itemsFormat = [];
        $itemsFormat[$items[0]['identify']] = $items[0];
        $itemsFormat[$items[1]['identify']] = $items[1];

        $template->setItems($items);

        $result = $this->ruleTrait->fetchTemplateItemsPublic($template);
        $this->assertEquals($itemsFormat, $result);
    }

    //fetchTemplateList
    public function testFetchTemplateList()
    {
        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(['getGbTemplateRepository'])->getMock();

        $templateList = ['templateList'];

        $condition =  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC')
                ),
                'TYSHXYDM' => array(
                    array('id'=>2, 'base'=> array(1,2), 'item'=>'TYSHXYDM')
                ),
        );

        $repository = $this->prophesize(GbTemplateRepository::class);
        $repository->fetchList([1,2])->shouldBeCalledTimes(1)->willReturn($templateList);

        $ruleTrait->expects($this->exactly(1))->method(
            'getGbTemplateRepository'
        )->willReturn($repository->reveal());

        $result = $ruleTrait->fetchTemplateListPublic($condition, $repository->reveal());
        $this->assertEquals($templateList, $result);
    }

    //validateBase
    public function testValidateBaseTrue()
    {
        $template = new MockTemplate();
        $template->setSubjectCategory([Template::SUBJECT_CATEGORY['ZRR']]);

        $result = $this->ruleTrait->validateBasePublic($template, IRule::COMPLETION_BASE);
        $this->assertTrue($result);
    }

    public function testValidateBaseFalseNotInCategory()
    {
        $template = new MockTemplate();
        $template->setSubjectCategory([Template::SUBJECT_CATEGORY['FRJFFRZZ']]);

        $result = $this->ruleTrait->validateBasePublic($template, IRule::COMPLETION_BASE);
        $this->assertTrue($result);
    }

    public function testValidateBaseFalseBaseOutOfRange()
    {
        $template = new MockTemplate();
        $template->setSubjectCategory([Template::SUBJECT_CATEGORY['ZRR']]);

        $result = $this->ruleTrait->validateBasePublic($template, [3]);
        $this->assertFalse($result);
    }

    //initializationTransformationSearchData
    public function testInitializationTransformationSearchData()
    {
        $transformationSearchData = $this->prophesize(IncompleteData::class);
        $transformationTemplate = new MockTemplate();
        $failureItems = ['failureItems'];
        $sourceSearchData = new MockSearchData();

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'generateCommonSearchData',
                                    'transformationTemplateRequired',
                                    'errorInfo'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('generateCommonSearchData')
                         ->with(
                             $sourceSearchData,
                             $this->isInstanceOf(
                                 'BaseData\ResourceCatalogData\Model\IncompleteData'
                             ),
                             $transformationTemplate
                         )
                        ->willReturn($transformationSearchData->reveal());

        $ruleTrait->expects($this->exactly(1))
                         ->method('transformationTemplateRequired')
                         ->with(
                             $transformationTemplate
                         )
                        ->willReturn($failureItems);

        $ruleTrait->expects($this->exactly(1))
                         ->method('errorInfo')
                         ->with(
                             $transformationSearchData->reveal(),
                             ErrorData::ERROR_TYPE['MISSING_DATA'],
                             $failureItems
                         )->willReturn($transformationSearchData->reveal());

        $transformationSearchData->setCategory(
            Argument::exact($transformationTemplate->getCategory())
        )->shouldBeCalledTimes(1);

        $result = $ruleTrait->initializationTransformationSearchDataPublic(
            $sourceSearchData,
            $transformationTemplate
        );

        $this->assertEquals($transformationSearchData->reveal(), $result);
    }

    //generateCommonSearchData
    public function testGenerateCommonSearchData()
    {
        $transformationTemplate = new MockTemplate();
        $searchData = new MockSearchData();
        $transformationSearchData = $this->prophesize(MockSearchData::class);

        $ruleTrait = new MockRuleTrait();

        $transformationSearchData->setCrew(
            Argument::exact($searchData->getCrew())
        )->shouldBeCalledTimes(1);

        $transformationSearchData->setSourceUnit(
            Argument::exact($searchData->getSourceUnit())
        )->shouldBeCalledTimes(1);

        $transformationSearchData->setExpirationDate(
            Argument::exact($searchData->getExpirationDate())
        )->shouldBeCalledTimes(1);

        $transformationSearchData->setTemplate(
            Argument::exact($transformationTemplate)
        )->shouldBeCalledTimes(1);

        $transformationSearchData->setInfoClassify(
            Argument::exact($transformationTemplate->getInfoClassify())
        )->shouldBeCalledTimes(1);

        $transformationSearchData->setInfoCategory(
            Argument::exact($transformationTemplate->getInfoCategory())
        )->shouldBeCalledTimes(1);

        $transformationSearchData->setSubjectCategory(
            Argument::exact($searchData->getSubjectCategory())
        )->shouldBeCalledTimes(1);

        $transformationSearchData->setDimension(
            Argument::exact($transformationTemplate->getDimension())
        )->shouldBeCalledTimes(1);

        $result = $ruleTrait->generateCommonSearchDataPublic(
            $searchData,
            $transformationSearchData->reveal(),
            $transformationTemplate
        );

        $this->assertEquals($transformationSearchData->reveal(), $result);
    }
    //generateErrorData
    public function testGenerateErrorData()
    {
        $transformationSearchData = new MockErrorData();
        $transformationSearchData->setTemplate(new MockBjTemplate());

        $errorData = $this->prophesize(MockErrorData::class);

        $ruleTrait = new MockRuleTrait();

        $errorData->setStatus(
            Argument::exact($transformationSearchData->getStatus())
        )->shouldBeCalledTimes(1);

        $errorData->setErrorType(
            Argument::exact($transformationSearchData->getErrorType())
        )->shouldBeCalledTimes(1);

        $errorData->setErrorReason(
            Argument::exact($transformationSearchData->getErrorReason())
        )->shouldBeCalledTimes(1);

        $errorData->setCategory(
            Argument::exact($transformationSearchData->getTemplate()->getCategory())
        )->shouldBeCalledTimes(1);

        $result = $ruleTrait->generateErrorDataPublic(
            $errorData->reveal(),
            $transformationSearchData
        );

        $this->assertEquals($errorData->reveal(), $result);
    }

    //getSearchData
    public function testGetSearchData()
    {
        $transformationTemplate = new MockTemplate();
        $bjSearchData = new BjSearchData();

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)
                    ->setMethods(['getSearchDataModelFactory'])
                    ->getMock();

        $factory = $this->prophesize(SearchDataModelFactory::class);
        $factory->getModel(
            Argument::exact($transformationTemplate->getCategory())
        )->shouldBeCalledTimes(1)->willReturn($bjSearchData);

        $ruleTrait->expects($this->exactly(1))
                         ->method('getSearchDataModelFactory')
                         ->willReturn($factory->reveal());

        $result = $ruleTrait->getSearchDataPublic($transformationTemplate);
        $this->assertEquals($bjSearchData, $result);
    }

    //fetchTransformationSearchData
    public function testFetchTransformationSearchData()
    {
        $transformationSearchData = new MockSearchData();
        $perfectSearchData = new MockSearchData();
        $template = new MockTemplate();
        $transformationSearchData->setTemplate($template);

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'getSearchData',
                                    'generateCommonSearchData'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                   ->method('getSearchData')
                         ->with(
                             $template
                         )
                    ->willReturn($perfectSearchData);

        $ruleTrait->expects($this->exactly(1))
                   ->method('generateCommonSearchData')
                         ->with(
                             $transformationSearchData,
                             $perfectSearchData,
                             $template
                         )
                    ->willReturn($perfectSearchData);

        $result = $ruleTrait->fetchTransformationSearchDataPublic($transformationSearchData);
        $this->assertEquals($perfectSearchData, $result);
    }

    //fetchFailureData
    public function testFetchFailureData()
    {
        $errorType = 1;
        $failureItems = [['name'=>'data1'],['name'=>'data2']];
        $failureData = new FailureData();
        $transformationSearchData = new FailureData();
        $template = new MockTemplate();
        $transformationSearchData->setTemplate($template);

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'generateCommonSearchData',
                                    'errorInfo',
                                    'generateErrorData'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('generateCommonSearchData')
                         ->with(
                             $transformationSearchData,
                             $this->isInstanceOf(
                                 'BaseData\ResourceCatalogData\Model\FailureData'
                             ),
                             $template
                         )
                        ->willReturn($failureData);

        $ruleTrait->expects($this->exactly(1))
                   ->method('generateErrorData')
                         ->with(
                             $failureData,
                             $transformationSearchData
                         )
                    ->willReturn($failureData);

        $ruleTrait->expects($this->exactly(1))
                   ->method('errorInfo')
                         ->with(
                             $failureData,
                             $errorType,
                             $failureItems
                         )
                    ->willReturn($failureData);

        $result = $ruleTrait->fetchFailureDataPublic(
            $transformationSearchData,
            $errorType,
            $failureItems
        );
        $failureData->getItemsData()->setData($transformationSearchData->getItemsData()->getData());
        $failureData->setHash($transformationSearchData->getHash());

        $this->assertEquals($failureData, $result);
    }

    //fetchIncompleteData
    public function testFetchIncompleteData()
    {
        $errorType = 1;
        $failureItems = [['name'=>'data1'],['name'=>'data2']];
        $incompleteData = new IncompleteData();
        $transformationSearchData = new IncompleteData();
        $template = new MockTemplate();
        $transformationSearchData->setTemplate($template);

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'generateCommonSearchData',
                                    'errorInfo',
                                    'generateErrorData'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('generateCommonSearchData')
                         ->with(
                             $transformationSearchData,
                             $this->isInstanceOf(
                                 'BaseData\ResourceCatalogData\Model\IncompleteData'
                             ),
                             $template
                         )
                        ->willReturn($incompleteData);

        $ruleTrait->expects($this->exactly(1))
                   ->method('generateErrorData')
                         ->with(
                             $incompleteData,
                             $transformationSearchData
                         )
                    ->willReturn($incompleteData);

        $ruleTrait->expects($this->exactly(1))
                   ->method('errorInfo')
                         ->with(
                             $incompleteData,
                             $errorType,
                             $failureItems
                         )
                    ->willReturn($incompleteData);

        $result = $ruleTrait->fetchIncompleteDataPublic(
            $transformationSearchData,
            $errorType,
            $failureItems
        );
        $this->assertEquals($incompleteData, $result);
    }

    //errorInfo
    public function testErrorInfo()
    {
        $errorData = $this->prophesize(ErrorData::class);
        $errorType = 1;
        $failureItems = [['name'=>'data1']];
        $template = new MockTemplate();
        $errorReason = array();

        $errorData->getErrorReason()->shouldBeCalledTimes(1)->willReturn($errorReason);
        $errorReason = array($errorType => array('name'));
        $errorData->setErrorReason(Argument::exact($errorReason))->shouldBeCalledTimes(1);
        $errorData->setErrorType(Argument::exact($errorType))->shouldBeCalledTimes(1);
        $errorData->getTemplate()->shouldBeCalledTimes(1)->willReturn($template);
        $errorData->setCategory(Argument::exact($template->getCategory()))->shouldBeCalledTimes(1);

        $result = $this->ruleTrait->errorInfoPublic(
            $errorData->reveal(),
            $errorType,
            $failureItems
        );

        $this->assertEquals($errorData->reveal(), $result);
    }

    //transformationTemplateRequired
    public function testTransformationTemplateRequired()
    {
        $templateItems = [
            ['isNecessary'=>Template::IS_NECESSARY['SHI'],'identify'=>'identify1'],
            ['isNecessary'=>Template::IS_NECESSARY['FOU'],'identify'=>'identify2']
        ];
        $transformationTemplate = new MockTemplate();
        $requiredItems = [
            'identify1'=>['isNecessary'=>Template::IS_NECESSARY['SHI'],'identify'=>'identify1']
        ];

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'fetchTemplateItems'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('fetchTemplateItems')
                         ->with($transformationTemplate)
                         ->willReturn($templateItems);

        $result = $ruleTrait->transformationTemplateRequiredPublic($transformationTemplate);
        $this->assertEquals($requiredItems, $result);
    }

    //getSearchDataRepository
    public function testGetSearchDataRepository()
    {
        $searchData = new MockSearchData();
        $template = new MockTemplate();
        $searchData->setTemplate($template);
        $repository = new GbSearchDataRepository();

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)
                    ->setMethods(['getSearchDataRepositoryFactory'])
                    ->getMock();

        $factory = $this->prophesize(SearchDataRepositoryFactory::class);
        $factory->getRepository(
            Argument::exact($template->getCategory())
        )->shouldBeCalledTimes(1)->willReturn($repository);

        $ruleTrait->expects($this->exactly(1))
                         ->method('getSearchDataRepositoryFactory')
                         ->willReturn($factory->reveal());

        $result = $ruleTrait->getSearchDataRepositoryPublic($searchData);
        $this->assertEquals($repository, $result);
    }

    //fetchSearchDataList
    public function testFetchSearchDataList()
    {
        $filter = ['filter'];
        $list = ['list'];
        $pattern['id'] = 1;
        $count = 1;
        $transformationSearchData = new MockSearchData();

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'getGbSearchDataRepository',
                                    'filterFormatChange'
                                ]
        )->getMock();

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->filter($filter)->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $ruleTrait->expects($this->exactly(1))
                         ->method('getGbSearchDataRepository')
                         ->willReturn($repository->reveal());

        $ruleTrait->expects($this->exactly(1))
                         ->method('filterFormatChange')
                         ->with($pattern, $transformationSearchData)
                         ->willReturn($filter);

        $result = $ruleTrait->fetchSearchDataListPublic($pattern, $transformationSearchData);
        $this->assertEquals($list, $result);
    }

    //filterFormatChange
    public function testFilterFormatChange()
    {
        $id = 1;
        $name = 'name';
        $identify = 'identify';
        $patterns = ['base'=>IRule::COMPLETION_BASE,'id'=>$id];

        $transformationSearchData = $this->prophesize(MockSearchData::class);
        $transformationSearchData->getName()->shouldBeCalledTimes(1)->willReturn($name);
        $transformationSearchData->getIdentify()->shouldBeCalledTimes(1)->willReturn($identify);

        $filter = ['name'=>$name,'identify'=>$identify,'template'=>$id];

        $ruleTrait = new MockRuleTrait();
        $result = $ruleTrait->filterFormatChangePublic($patterns, $transformationSearchData->reveal());
        $this->assertEquals($filter, $result);
    }

    public function testFilterFormatChangeExistIdentify()
    {
        $id = 1;
        $name = 'name';
        $patterns = ['base'=>[IRule::COMPLETION_BASE['NAME']],'id'=>$id];

        $transformationSearchData = $this->prophesize(MockSearchData::class);
        $transformationSearchData->getName()->shouldBeCalledTimes(1)->willReturn($name);

        $filter = ['name'=>$name, 'template'=>$id];

        $ruleTrait = new MockRuleTrait();
        $result = $ruleTrait->filterFormatChangePublic($patterns, $transformationSearchData->reveal());
        $this->assertEquals($filter, $result);
    }

    //fetchIncompleteItems
    public function testFetchIncompleteItems()
    {
        $transformationItemsData = ['identify'=>'data1', 'identify'=>'data2', 'identify2'=>'data3'];
        $requiredItems = ['identify'=>'data', 'identify3'=>'data'];
        $requiredItemsIdentify = ['identify', 'identify3'];

        $incompleteItems = ['identify3'=>'data'];

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'fetchItemsIdentify'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('fetchItemsIdentify')
                         ->with($requiredItems)
                         ->willReturn($requiredItemsIdentify);

        $result = $ruleTrait->fetchIncompleteItemsPublic($transformationItemsData, $requiredItems);
        $this->assertEquals($incompleteItems, $result);
    }

    //fetchItemsIdentify
    public function testFetchItemsIdentify()
    {
        $items = ['identify1'=>'data', 'identify2'=>'data'];
        $ruleTrait = new MockRuleTrait();

        $result = $ruleTrait->fetchItemsIdentifyPublic($items);
        $this->assertEquals(['identify1','identify2'], $result);
    }

    //filterExistItemsDataByIdentifyPublic
    public function testFilterExistItemsDataByIdentifyPublicExit()
    {
        $identify = 'identify';
        $data = 'data';
        $ruleTrait = new MockRuleTrait();

        $searchDataList[0] = new MockSearchData();
        $searchDataList[0]->getItemsData()->setData([$identify=>$data]);
        $searchDataList[1] = new MockSearchData();
        $searchDataList[1]->getItemsData()->setData([$identify=>'']);

        $result = $ruleTrait->filterExistItemsDataByIdentifyPublic($searchDataList, $identify);
        $this->assertEquals($data, $result);
    }

    public function testFilterExistItemsDataByIdentifyPublicNotExit()
    {
        $identify = 'identify';
        $ruleTrait = new MockRuleTrait();

        $searchDataList[0] = new MockSearchData();
        $searchDataList[0]->getItemsData()->setData([]);
        $searchDataList[1] = new MockSearchData();
        $searchDataList[1]->getItemsData()->setData([$identify=>'']);

        $result = $ruleTrait->filterExistItemsDataByIdentifyPublic($searchDataList, $identify);
        $this->assertEquals('', $result);
    }

    //generateHash
    public function testGenerateHash()
    {
        $itemsData = ['itemsData'];
        $ruleTrait = new MockRuleTrait();
        $result = $ruleTrait->generateHashPublic($itemsData);
        $this->assertEquals($result, md5(serialize($itemsData)));
    }

    //incompleteData
    public function testIncompleteData()
    {
        $searchData = new IncompleteData();
        $transformationSearchData = new IncompleteData();
        $errorType = 1;
        $incompleteItems = ['incompleteItems'];

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'fetchIncompleteData'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('fetchIncompleteData')
                         ->with($transformationSearchData, $errorType, $incompleteItems)
                         ->willReturn($searchData);

        $result = $ruleTrait->incompleteDataPublic($transformationSearchData, $errorType, $incompleteItems);

        $this->assertEquals($searchData, $result);
    }

    //transformationSearchData
    public function testTransformationSearchData()
    {
        $searchData = new MockSearchData();
        $transformationSearchData = new MockSearchData();

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'fetchTransformationSearchData'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('fetchTransformationSearchData')
                         ->with($transformationSearchData)
                         ->willReturn($searchData);

        $result = $ruleTrait->transformationSearchDataPublic($transformationSearchData);
        $this->assertEquals($searchData, $result);
    }

    //failureData
    public function testFailureData()
    {
        $searchData = new FailureData();
        $transformationSearchData = new FailureData();
        $errorType = 1;
        $failureItems = ['failureItems'];

        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods(
            [
                                    'fetchFailureData'
                                ]
        )->getMock();

        $ruleTrait->expects($this->exactly(1))
                         ->method('fetchFailureData')
                         ->with($transformationSearchData, $errorType, $failureItems)
                         ->willReturn($searchData);

        $result = $ruleTrait->failureDataPublic($transformationSearchData, $errorType, $failureItems);

        $this->assertEquals($searchData, $result);
    }

    //failureDataException
    public function testFailureDataException()
    {
        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods([
            'generateCommonSearchData',
            'generateErrorData'
        ])->getMock();

        $failureData = new FailureData();
        $transformationSearchData = new FailureData();
        $status = ErrorData::STATUS['PROGRAM_EXCEPTION'];

        $template = new MockTemplate();
        $transformationSearchData->setTemplate($template);

        $ruleTrait->expects($this->exactly(1))
                            ->method('generateCommonSearchData')
                            ->with(
                                $transformationSearchData,
                                $this->isInstanceOf(
                                    'BaseData\ResourceCatalogData\Model\FailureData'
                                ),
                                $template
                            )
                        ->willReturn($failureData);

        $ruleTrait->expects($this->exactly(1))
                    ->method('generateErrorData')
                            ->with(
                                $failureData,
                                $transformationSearchData
                            )
                    ->willReturn($failureData);

        $failureData->getItemsData()->setData($transformationSearchData->getItemsData()->getData());
        $failureData->setHash($transformationSearchData->getHash());
        $failureData->setStatus($status);
        
        $result = $ruleTrait->failureDataExceptionPublic(
            $transformationSearchData,
            $status
        );

        $this->assertEquals($failureData, $result);
    }

    public function testErrorReasonFormatConversion()
    {
        $errorReasonOld = array(
            ErrorData::ERROR_TYPE['MISSING_DATA'] => array('ZTMC')
        );
        
        $errorDataOld = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
        $errorDataOld->setErrorReason($errorReasonOld);

        $errorReason = array(
            'ZTMC' => array(ErrorData::ERROR_TYPE['MISSING_DATA'])
        );

        $result = $this->ruleTrait->errorReasonFormatConversionPublic($errorDataOld);

        $errorData = $errorDataOld;
        $errorData->setErrorReason($errorReason);

        $this->assertEquals($errorData, $result);
    }

    public function testTransformationDataResult()
    {
        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods([
            'transformationSearchData'
        ])->getMock();

        $incompleteItems = array();
        $transformationSearchData = new MockSearchData();

        $ruleTrait->expects($this->exactly(1))
                    ->method('transformationSearchData')
                    ->with($transformationSearchData)
                    ->willReturn($transformationSearchData);

        $result = $ruleTrait->transformationDataResultPublic($incompleteItems, $transformationSearchData);

        $this->assertEquals($transformationSearchData, $result);
    }

    public function testTransformationDataResultFailureData()
    {
        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods([
            'transformationSearchData'
        ])->getMock();

        $incompleteItems = array();
        $transformationSearchData = new FailureData();

        $result = $ruleTrait->transformationDataResultPublic($incompleteItems, $transformationSearchData);

        $this->assertEquals($transformationSearchData, $result);
    }

    public function testTransformationDataResultIncompleteItems()
    {
        $ruleTrait = $this->getMockBuilder(MockRuleTrait::class)->setMethods([
            'incompleteData'
        ])->getMock();

        $incompleteItems = array('incompleteItems');
        $transformationSearchData = new MockSearchData();

        $ruleTrait->expects($this->exactly(1))
                    ->method('incompleteData')
                    ->with(
                        $transformationSearchData,
                        ErrorData::ERROR_TYPE['MISSING_DATA'],
                        $incompleteItems
                    )
                    ->willReturn($transformationSearchData);

        $result = $ruleTrait->transformationDataResultPublic($incompleteItems, $transformationSearchData);

        $this->assertEquals($transformationSearchData, $result);
    }
}

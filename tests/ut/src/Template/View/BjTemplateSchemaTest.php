<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class BjTemplateSchemaTest extends TestCase
{
    private $bjTemplateSchema;

    private $bjTemplate;

    public function setUp()
    {
        $this->bjTemplateSchema = new BjTemplateSchema(new Factory());

        $this->bjTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->bjTemplateSchema);
        unset($this->bjTemplate);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->bjTemplateSchema);
    }

    public function testGetId()
    {
        $result = $this->bjTemplateSchema->getId($this->bjTemplate);

        $this->assertEquals($result, $this->bjTemplate->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->bjTemplateSchema->getAttributes($this->bjTemplate);

        $this->assertEquals($result['name'], $this->bjTemplate->getName());
        $this->assertEquals($result['identify'], $this->bjTemplate->getIdentify());
        $this->assertEquals($result['subjectCategory'], $this->bjTemplate->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->bjTemplate->getDimension());
        $this->assertEquals($result['exchangeFrequency'], $this->bjTemplate->getExchangeFrequency());
        $this->assertEquals($result['infoClassify'], $this->bjTemplate->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->bjTemplate->getInfoCategory());
        $this->assertEquals($result['description'], $this->bjTemplate->getDescription());
        $this->assertEquals($result['category'], $this->bjTemplate->getCategory());
        $this->assertEquals($result['items'], $this->bjTemplate->getItems());
        $this->assertEquals($result['status'], $this->bjTemplate->getStatus());
        $this->assertEquals($result['createTime'], $this->bjTemplate->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->bjTemplate->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->bjTemplate->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->bjTemplateSchema->getRelationships($this->bjTemplate, 0, array());

        $this->assertEquals($result['sourceUnit'], ['data' => $this->bjTemplate->getSourceUnit()]);
        $this->assertEquals($result['gbTemplate'], ['data' => $this->bjTemplate->getGbTemplate()]);
    }
}

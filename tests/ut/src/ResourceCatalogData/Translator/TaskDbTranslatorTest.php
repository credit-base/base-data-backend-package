<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\TaskUtils;

class TaskDbTranslatorTest extends TestCase
{
    use TaskUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TaskDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullTask', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);

        $expression['task_id'] = $task->getId();
        $expression['crew_id'] = $task->getCrew()->getId();
        $expression['user_group_id'] = $task->getUserGroup()->getId();
        $expression['pid'] = $task->getPid();
        $expression['total'] = $task->getTotal();
        $expression['success_number'] = $task->getSuccessNumber();
        $expression['failure_number'] = $task->getFailureNumber();
        $expression['source_category'] = $task->getSourceCategory();
        $expression['source_template_id'] = $task->getSourceTemplate();
        $expression['target_category'] = $task->getTargetCategory();
        $expression['target_template_id'] = $task->getTargetTemplate();
        $expression['target_rule_id'] = $task->getTargetRule();
        $expression['schedule_task_id'] = $task->getScheduleTask();
        $expression['error_number'] = $task->getErrorNumber();
        $expression['file_name'] = $task->getFileName();
        $expression['status'] = $task->getStatus();
        $expression['status_time'] = $task->getStatusTime();
        $expression['create_time'] = $task->getCreateTime();
        $expression['update_time'] = $task->getUpdateTime();

        $task = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\Task', $task);
        $this->compareArrayAndObject($expression, $task);
    }

    public function testObjectToArray()
    {
        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);

        $expression = $this->translator->objectToArray($task);

        $this->compareArrayAndObject($expression, $task);
    }
}

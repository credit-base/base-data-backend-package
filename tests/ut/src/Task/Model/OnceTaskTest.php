<?php
namespace BaseData\Task\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Task\Adapter\Task\ITaskAdapter;

class OnceTaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new MockOnceTask();
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("BaseData\Task\Model\Task", $this->task);
    }

    public function testConstructor()
    {
        $this->assertEquals(Task::TYPE['ONCE'], $this->task->getType());
    }

    //close -- 开始
    public function testClose()
    {
        $this->task = $this->getMockBuilder(MockOnceTask::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->task->setStatus(Task::STATUS['SUCCESS']);
        $this->task->setUpdateTime(time());

        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->edit(
            Argument::exact($this->task),
            Argument::exact(array(
                'status',
                'statusTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->task->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->task->close();
        
        $this->assertTrue($result);
    }
    //close -- 结束

    //afterExecute -- 开始
    public function testAfterExecute()
    {
        $this->task = $this->getMockBuilder(MockOnceTask::class)
            ->setMethods(['close'])
            ->getMock();

        $this->task->expects($this->once())
                   ->method('close')
                   ->willReturn(true);

        $result = $this->task->afterExecute();
        
        $this->assertTrue($result);
    }
    //afterExecute -- 结束
}

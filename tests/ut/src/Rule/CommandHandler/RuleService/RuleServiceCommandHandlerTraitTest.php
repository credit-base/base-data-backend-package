<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Repository\CrewRepository;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\MockRule;
use BaseData\Rule\Model\ModelFactory;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Translator\RuleServiceDbTranslator;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

use BaseData\Template\Model\Template;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @author chloroplast
 */
class RuleServiceCommandHandlerTraitTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new MockRuleServiceCommandHandlerTrait();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testGetCrewRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Crew\Repository\CrewRepository',
            $this->commandHandler->getCrewRepositoryPublic()
        );
    }

    public function testFetchCrew()
    {
        $id = 1;
        $crew = new Crew();

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getCrewRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($crew);

        $this->commandHandler->expects($this->once())
                       ->method('getCrewRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->fetchCrewPublic($id);
        $this->assertInstanceOf(
            'BaseData\Crew\Model\Crew',
            $result
        );
    }

    public function testGetRuleServiceRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RuleServiceRepository',
            $this->commandHandler->getRuleServiceRepositoryPublic()
        );
    }

    public function testFetchRuleService()
    {
        $id = 1;
        $ruleService = new RuleService();

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getRuleServiceRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(RuleServiceRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($ruleService);

        $this->commandHandler->expects($this->once())
                       ->method('getRuleServiceRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->fetchRuleServicePublic($id);
        $this->assertInstanceOf(
            'BaseData\Rule\Model\RuleService',
            $result
        );
    }

    public function testGetRuleServiceDbTranslator()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Translator\RuleServiceDbTranslator',
            $this->commandHandler->getRuleServiceDbTranslatorPublic()
        );
    }

    public function testApplyInfo()
    {
        $unAuditedRuleService = new UnAuditedRuleService();
        $applyInfo = ['applyInfo'];

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getRuleServiceDbTranslator'
                                         ]
                                     )
                                     ->getMock();

        $translator = $this->prophesize(RuleServiceDbTranslator::class);
        $translator->objectToArray($unAuditedRuleService)->shouldBeCalledTimes(1)
                   ->willReturn($applyInfo);

        $this->commandHandler->expects($this->once())
                       ->method('getRuleServiceDbTranslator')
                       ->willReturn($translator->reveal());

        $result = $this->commandHandler->applyInfoPublic($unAuditedRuleService);
        $this->assertEquals($applyInfo, $result);
    }

    public function testGetUnAuditedRuleServiceRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\UnAuditedRuleServiceRepository',
            $this->commandHandler->getUnAuditedRuleServiceRepositoryPublic()
        );
    }

    public function testFetchUnAuditedRuleService()
    {
        $id = 1;
        $unAuditedRuleService = new UnAuditedRuleService();

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getUnAuditedRuleServiceRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(UnAuditedRuleServiceRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($unAuditedRuleService);

        $this->commandHandler->expects($this->once())
                       ->method('getUnAuditedRuleServiceRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->fetchUnAuditedRuleServicePublic($id);
        $this->assertInstanceOf(
            'BaseData\Rule\Model\UnAuditedRuleService',
            $result
        );
    }

    public function testGetUnAuditedRuleService()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\UnAuditedRuleService',
            $this->commandHandler->getUnAuditedRuleServicePublic()
        );
    }

    public function testGetRepositoryFactory()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RepositoryFactory',
            $this->commandHandler->getRepositoryFactoryPublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->commandHandler->getRepositoryPublic(Template::CATEGORY['BJ'])
        );
    }

    public function testGetModelFactory()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\ModelFactory',
            $this->commandHandler->getModelFactoryPublic()
        );
    }

    public function testGenerateUnAuditedRuleServiceCrew()
    {
        $crewId = 1;
        $command = new class implements ICommand {
        };
        $command->crew = $crewId;
        $command->rules = [
            ['condition1'],
            ['condition2']
        ];

        $crew = new Crew($crewId);
        $unAuditedRuleService = new UnAuditedRuleService();

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'fetchCrew'
                                         ]
                                     )
                                     ->getMock();

        $this->commandHandler->expects($this->once())
                       ->method('fetchCrew')
                       ->with($command->crew)
                       ->willReturn($crew);
        
        $this->commandHandler->generateUnAuditedRuleServiceCrewPublic(
            $command->crew,
            $unAuditedRuleService
        );

        $this->assertEquals($crew, $unAuditedRuleService->getCrew());
        $this->assertEquals($crew, $unAuditedRuleService->getPublishCrew());
    }

    public function testGenerateUnAuditedRuleServiceRules()
    {
        $crewId = 1;
        $command = new class implements ICommand {
        };
        $command->crew = $crewId;
        $command->rules = [
            ['condition1'],
            ['condition2']
        ];
        $rules = [];

        $unAuditedRuleService = new UnAuditedRuleService();

        $this->commandHandler = $this->getMockBuilder(MockRuleServiceCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getModelFactory'
                                         ]
                                     )
                                     ->getMock();

        $rules[0] = new MockRule();
        $rules[1] = new MockRule();

        $moodelFactory = $this->prophesize(ModelFactory::class);
        $moodelFactory->getModel(0)->shouldBeCalledTimes(1)->willReturn($rules[0]);
        $moodelFactory->getModel(1)->shouldBeCalledTimes(1)->willReturn($rules[1]);

        $this->commandHandler->expects($this->exactly(2))
                       ->method('getModelFactory')
                       ->willReturn($moodelFactory->reveal());
        
        $this->commandHandler->generateUnAuditedRuleServiceRulesPublic(
            $command->rules,
            $unAuditedRuleService
        );

        $this->assertEquals($rules, $unAuditedRuleService->getRules());
    }
}

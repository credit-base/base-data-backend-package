<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\ResourceCatalogData\View\ErrorDataView;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;
use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class ErrorDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ErrorDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IErrorDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new ErrorDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'errorDatas';
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\Persistence;

class MockWbjItemsDataCache extends WbjItemsDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

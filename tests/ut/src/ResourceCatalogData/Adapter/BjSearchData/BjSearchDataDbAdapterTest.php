<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\NullBjSearchData;
use BaseData\ResourceCatalogData\Translator\BjSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\BjSearchDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class BjSearchDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockBjSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchTemplate',
                                'fetchItemsData',
                                'searchDataFormatFilter'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IBjSearchDataAdapter
     */
    public function testImplementsIBjSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 BjSearchDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockBjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\BjSearchDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 BjSearchDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockBjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\BjSearchDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetItemsDataRepository()
    {
        $adapter = new MockBjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\BjItemsDataRepository',
            $adapter->getItemsDataRepository()
        );
    }

    public function testGetTemplateRepository()
    {
        $adapter = new MockBjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\BjTemplateRepository',
            $adapter->getTemplateRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockBjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedBjSearchData = new BjSearchData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedBjSearchData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedBjSearchData);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedBjSearchData = new BjSearchData();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedBjSearchData, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedBjSearchData, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedBjSearchData = new BjSearchData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedBjSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedBjSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedBjSearchData);

        //验证
        $bjSearchData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedBjSearchData, $bjSearchData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $bjSearchDataOne = new BjSearchData(1);
        $bjSearchDataTwo = new BjSearchData(2);

        $ids = [1, 2];

        $expectedBjSearchDataList = [];
        $expectedBjSearchDataList[$bjSearchDataOne->getId()] = $bjSearchDataOne;
        $expectedBjSearchDataList[$bjSearchDataTwo->getId()] = $bjSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedBjSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedBjSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedBjSearchDataList);
        //验证
        $bjSearchDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedBjSearchDataList, $bjSearchDataList);
    }

    public function testFormatFilter()
    {
        $filter = array();
        $bjSearchData = new BjSearchData();
        $expectedCondition = ' 1 ';

        $this->adapter->expects($this->exactly(1))
                        ->method('searchDataFormatFilter')
                        ->with($filter, $bjSearchData)
                        ->willReturn($expectedCondition);

        $condition = $this->adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter/template
    public function testFormatFilterTemplate()
    {
        $filter = array(
            'template' => 1
        );
        $adapter = new MockBjSearchDataDbAdapter();

        $expectedCondition = 'bj_template_id = '.$filter['template'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-id
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockBjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY bj_search_data_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-id
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockBjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY bj_search_data_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockBjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockBjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

<?php
namespace BaseData\Rule\Model;

use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

class MockUnAuditedRuleService extends UnAuditedRuleService
{
    public function getUnAuditedRuleServiceRepository() : UnAuditedRuleServiceRepository
    {
        return parent::getUnAuditedRuleServiceRepository();
    }

    public function apply() : bool
    {
        return parent::apply();
    }

    public function resubmitAction() : bool
    {
        return parent::resubmitAction();
    }

    public function approveAction() : bool
    {
        return parent::approveAction();
    }

    public function approveApplyInfo(int $operationType) : bool
    {
        return parent::approveApplyInfo($operationType);
    }

    public function rejectAction() : bool
    {
        return parent::rejectAction();
    }

    public function updateApplyStatus(int $applyStatus) : bool
    {
        return parent::updateApplyStatus($applyStatus);
    }

    public function updateRelationId() : bool
    {
        return parent::updateRelationId();
    }
}

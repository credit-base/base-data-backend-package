<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter;

class BjItemsDataTest extends TestCase
{
    private $bjItemsData;

    public function setUp()
    {
        $this->bjItemsData = new BjItemsData();
    }

    public function tearDown()
    {
        unset($this->bjItemsData);
    }

    public function testGetRepository()
    {
        $bjItemsData = new MockBjItemsData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter',
            $bjItemsData->getRepository()
        );
    }

    public function testAddSuccess()
    {
        $bjItemsData = $this->getMockBuilder(BjItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IBjItemsDataAdapter::class);
        $repository->add(Argument::exact($bjItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $bjItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $bjItemsData->add();
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        $bjItemsData = $this->getMockBuilder(BjItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IBjItemsDataAdapter::class);
        $repository->add(Argument::exact($bjItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        $bjItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $bjItemsData->add();
        $this->assertFalse($result);
    }
}

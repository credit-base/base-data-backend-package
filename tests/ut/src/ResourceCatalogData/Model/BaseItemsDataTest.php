<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class BaseItemsDataTest extends TestCase
{
    private $baseItemsData;

    public function setUp()
    {
        $this->baseItemsData = new BaseItemsData();
    }

    public function tearDown()
    {
        unset($this->baseItemsData);
    }

    public function testGetRepository()
    {
        $baseItemsData = new MockBaseItemsData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter',
            $baseItemsData->getRepository()
        );
    }
}

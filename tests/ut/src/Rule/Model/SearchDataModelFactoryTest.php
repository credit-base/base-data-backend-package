<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Model\NullRule;

class SearchDataModelFactoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new SearchDataModelFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullModel()
    {
        $stub = $this->stub->getModel(0);
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\NullBjSearchData',
                $stub
            );
    }

    public function testGetModel()
    {
        foreach (SearchDataModelFactory::MAPS as $key => $model) {
            $this->assertInstanceOf(
                $model,
                $this->stub->getModel($key)
            );
        }
    }
}

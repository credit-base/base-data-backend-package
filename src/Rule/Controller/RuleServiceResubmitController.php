<?php
namespace BaseData\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use BaseData\Common\Controller\Interfaces\IResubmitAbleController;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\View\UnAuditedRuleServiceView;
use BaseData\Rule\Command\RuleService\ResubmitRuleServiceCommand;

class RuleServiceResubmitController extends Controller implements IResubmitAbleController
{
    use JsonApiTrait, RuleServiceControllerTrait;

    /**
     * 对应路由 /unAuditedRules/{id:\d+}/resubmit
     * 重新提交, 通过PATCH传参
     * @param int id
     * @return jsonApi
     */
    public function resubmit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $rules = $attributes['rules'];

        $crew = $relationships['crew']['data'][0]['id'];
        
        if ($this->validateCommonScenario(
            $rules,
            $crew
        )) {
            $command = new ResubmitRuleServiceCommand(
                $rules,
                $crew,
                $id
            );

            if ($this->getCommandBus()->send($command)) {
                $unAuditedRuleService = $this->getUnAuditedRuleServiceRepository()->fetchOne($id);
                if ($unAuditedRuleService instanceof UnAuditedRuleService) {
                    $this->render(new UnAuditedRuleServiceView($unAuditedRuleService));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}

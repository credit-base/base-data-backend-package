<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\Persistence;

class MockYsItemsDataCache extends YsItemsDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

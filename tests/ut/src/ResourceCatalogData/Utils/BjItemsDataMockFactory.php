<?php
namespace BaseData\ResourceCatalogData\Utils;

use BaseData\ResourceCatalogData\Model\BjItemsData;

class BjItemsDataMockFactory
{
    public static function generateBjItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : BjItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $bjItemsData = new BjItemsData($id);
        $bjItemsData->setId($id);

        self::generateData($bjItemsData, $faker, $value);

        return $bjItemsData;
    }

    private static function generateData($bjItemsData, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            $faker->words(3, false);
        
        $bjItemsData->setData($data);
    }
}

<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use BaseData\Crew\Adapter\Crew\ICrewAdapter;
use BaseData\Crew\Repository\CrewRepository;
use BaseData\Crew\Model\Crew;

use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use BaseData\UserGroup\Repository\UserGroupRepository;
use BaseData\UserGroup\Model\UserGroup;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Model\WbjTemplate;

trait WorkOrderTaskCommandHandlerTrait
{
    protected function getCrewRepository() : ICrewAdapter
    {
        return new CrewRepository();
    }

    protected function getUserGroupRepository() : IUserGroupAdapter
    {
        return new UserGroupRepository();
    }

    protected function getWbjTemplateRepository() : IWbjTemplateAdapter
    {
        return new WbjTemplateRepository();
    }

    protected function fetchOneCrew($id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function fetchOneUserGroup($id) : UserGroup
    {
        return $this->getUserGroupRepository()->fetchOne($id);
    }

    protected function fetchOneWbjTemplate($id) : WbjTemplate
    {
        return $this->getWbjTemplateRepository()->fetchOne($id);
    }
}

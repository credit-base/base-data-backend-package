<?php
namespace BaseData\WorkOrderTask\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskDbAdapter;

use BaseData\WorkOrderTask\Utils\WorkOrderTaskMockFactory;

class WorkOrderTaskRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WorkOrderTaskRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockWorkOrderTaskRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new WorkOrderTaskDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask($id);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->add(Argument::exact($workOrderTask))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($workOrderTask, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask($id);
        $keys = array();
        
        $adapter = $this->prophesize(IWorkOrderTaskAdapter::class);
        $adapter->edit(Argument::exact($workOrderTask), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($workOrderTask, $keys);
    }
}

<?php
namespace BaseData\Common\Adapter;

use Marmot\Interfaces\INull;

trait FetchAbleRestfulAdapterTrait
{
    abstract protected function getResource() : string;

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function fetchListAction(array $ids) : array
    {
        $this->get(
            $this->getResource().'/'.implode(',', $ids)
        );


        $result = array();

        if ($this->isSuccess()) {
            list($list, $count) = $this->translateToObjects();
            unset($count);
            $result = $list;
        }
        
        return $result;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->filterAction($filter, $sort, $offset, $size);
    }

    protected function filterAction(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        $this->get(
            $this->getResource(),
            array(
                'filter'=>$filter,
                'sort'=>(implode(',', $sort)) ? implode(',', $sort) : 1,
                'page'=>array('size'=>$size, 'number'=>$offset)
            )
        );
       
        return $this->isSuccess() ? $this->translateToObjects() : array(array(), 0);
    }
}

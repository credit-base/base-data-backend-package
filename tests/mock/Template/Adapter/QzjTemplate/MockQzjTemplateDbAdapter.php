<?php
namespace BaseData\Template\Adapter\QzjTemplate;

use Marmot\Interfaces\INull;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Translator\QzjTemplateDbTranslator;
use BaseData\Template\Adapter\QzjTemplate\Query\QzjTemplateRowCacheQuery;

use BaseData\UserGroup\Repository\UserGroupRepository;

class MockQzjTemplateDbAdapter extends QzjTemplateDbAdapter
{
    public function getDbTranslator() : QzjTemplateDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : QzjTemplateRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function fetchSourceUnit($qzjTemplate)
    {
        return parent::fetchSourceUnit($qzjTemplate);
    }

    public function fetchSourceUnitByObject(QzjTemplate $qzjTemplate)
    {
        return parent::fetchSourceUnitByObject($qzjTemplate);
    }

    public function fetchSourceUnitByList(array $qzjTemplateList)
    {
        return parent::fetchSourceUnitByList($qzjTemplateList);
    }
}

<?php
namespace BaseData\Task\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullNotifyRecord extends NotifyRecord implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function add() : bool
    {
        return $this->resourceNotExist();
    }


    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}

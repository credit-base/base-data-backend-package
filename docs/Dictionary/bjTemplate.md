# 本级目录字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [目录字典](./docs/Dictionary/template.md "目录字典")

### gbTemplate

**国标目录** 

* GbTemplate
* 必填项

### sourceUnit

**来源单位** 

* UserGroup
* 必填项
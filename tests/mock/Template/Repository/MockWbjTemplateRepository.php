<?php
namespace BaseData\Template\Repository;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;

class MockWbjTemplateRepository extends WbjTemplateRepository
{
    public function getAdapter() : IWbjTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWbjTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWbjTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}

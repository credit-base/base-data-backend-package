<?php
namespace BaseData\Template\WbjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\WbjTemplate;

/**
 * 测试委办局资源目录接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * 存在多条委办局资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate()
            ]
        );
    }

    public function testViewWbjTemplate()
    {
        $setWbjTemplateList = $this->wbjTemplate();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'templates?filter[sourceUnit]=1&filter[dimension]='.Template::DIMENSION['SHGK'].
            '&filter[infoClassify]='.Template::INFO_CLASSIFY['QT'].
            '&filter[infoCategory]='.Template::INFO_CATEGORY['QTXX'].
            '&filter[name]=企业基本信息&filter[identify]=WBJQYJBXX&include=sourceUnit',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];

        foreach ($data as $key => $wbjTemplate) {
            $this->wbjTemplateCompareData($setWbjTemplateList[$key], $wbjTemplate);
        }
    }
}

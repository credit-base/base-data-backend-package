<?php
namespace BaseData\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class WbjItemsDataSchema extends SchemaProvider
{
    protected $resourceType = 'wbjItemsData';

    public function getId($wbjItemsData) : int
    {
        return $wbjItemsData->getId();
    }

    public function getAttributes($wbjItemsData) : array
    {
        return [
            'data' => $wbjItemsData->getData()
        ];
    }
}

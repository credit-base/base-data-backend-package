<?php
namespace BaseData\WorkOrderTask\Repository;

use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;

class MockParentTaskRepository extends ParentTaskRepository
{
    public function getAdapter() : IParentTaskAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IParentTaskAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IParentTaskAdapter
    {
        return parent::getMockAdapter();
    }
}

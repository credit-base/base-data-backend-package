<?php
namespace BaseData\Rule\Adapter;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Repository;

use BaseData\Template\Repository\GbTemplateRepository;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Model\Rule;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\RepositoryFactory;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
trait RuleServiceAdapterTrait
{
    protected function getGbTemplateRepository() : GbTemplateRepository
    {
        return new GbTemplateRepository();
    }

    protected function getRepositoryFactory() : RepositoryFactory
    {
        return new RepositoryFactory();
    }
    
    protected function getRepository(int $category) : Repository
    {
        return $this->getRepositoryFactory()->getRepository($category);
    }

    protected function fetchTransformationTemplate($ruleService)
    {
        return is_array($ruleService) ?
        $this->fetchTransformationTemplateByList($ruleService) :
        $this->fetchTransformationTemplateByObject($ruleService);
    }

    protected function fetchTransformationTemplateByObject($ruleService)
    {
        if (!$ruleService instanceof INull) {
            $transformationTemplateId = $ruleService->getTransformationTemplate()->getId();
            $repository = $this->getRepository($ruleService->getTransformationCategory());
            $transformationTemplate = $repository->fetchOne($transformationTemplateId);
            $ruleService->setTransformationTemplate($transformationTemplate);
        }
    }
   /**
     * 获取目标资源目录列表,并赋值,目标资源目录分为多种类型,所以在获取资源目录时需要通过不同的分类获取相对应的仓库
     * @param array $ruleServiceList 规则数据列表
     */
    protected function fetchTransformationTemplateByList(array $ruleServiceList)
    {
        if (!empty($ruleServiceList)) {
            $transformationTemplateIds = array();
            foreach ($ruleServiceList as $ruleService) {
                $ruleServiceId = $ruleService->getId();
                if ($ruleService instanceof UnAuditedRuleService) {
                    $ruleServiceId = $ruleService->getApplyId();
                }
                //根据分类获取目标资源目录id集合
                $transformationTemplateIds[$ruleService->getTransformationCategory()][] = array(
                    'transformationTemplateId' => $ruleService->getTransformationTemplate()->getId(),
                    'ruleServiceId' => $ruleServiceId
                );
            }

            $objectReferenceList = array();

            //通过不同的分类获取仓库,并获取列表,并将列表通过id拼成一个大的数组,方便下一步的数据赋值
            foreach ($transformationTemplateIds as $category => $val) {
                $ids = array_unique(array_column($val, 'transformationTemplateId'));
                $transformationTemplateRepository = $this->getRepository($category);
                $transformationTemplateListTemp = $transformationTemplateRepository->fetchList($ids);
                
                foreach ($val as $v) {
                    if (isset($transformationTemplateListTemp[$v['transformationTemplateId']])) {
                        $objectReferenceList[$v['ruleServiceId']] =
                        $transformationTemplateListTemp[$v['transformationTemplateId']];
                    }
                }
            }

            //将获取到的目标资源目录数据赋值
            if (!empty($objectReferenceList)) {
                foreach ($ruleServiceList as $ruleService) {
                    $ruleServiceId = $ruleService->getId();
                    if ($ruleService instanceof UnAuditedRuleService) {
                        $ruleServiceId = $ruleService->getApplyId();
                    }
                    if (isset($objectReferenceList[$ruleServiceId])) {
                        $ruleService->setTransformationTemplate($objectReferenceList[$ruleServiceId]);
                    }
                }
            }
        }
    }

    protected function fetchSourceTemplate($ruleService)
    {
        return is_array($ruleService) ?
        $this->fetchSourceTemplateByList($ruleService) :
        $this->fetchSourceTemplateByObject($ruleService);
    }

    protected function fetchSourceTemplateByObject($ruleService)
    {
        if (!$ruleService instanceof INull) {
            $sourceTemplateId = $ruleService->getSourceTemplate()->getId();
            $repository = $this->getRepository($ruleService->getSourceCategory());
            $sourceTemplate = $repository->fetchOne($sourceTemplateId);
            $ruleService->setSourceTemplate($sourceTemplate);
        }
    }

   /**
     * 获取来源资源目录列表,并赋值,来源资源目录分为多种类型,所以在获取资源目录时需要通过不同的分类获取相对应的仓库
     * @param array $ruleServiceList 规则数据列表
     */
    protected function fetchSourceTemplateByList(array $ruleServiceList)
    {
        if (!empty($ruleServiceList)) {
            $sourceTemplateIds = array();
            foreach ($ruleServiceList as $ruleService) {
                $ruleServiceId = $ruleService->getId();
                if ($ruleService instanceof UnAuditedRuleService) {
                    $ruleServiceId = $ruleService->getApplyId();
                }
                //根据分类获取来源资源目录id集合
                $sourceTemplateIds[$ruleService->getSourceCategory()][] = array(
                    'sourceTemplateId' => $ruleService->getSourceTemplate()->getId(),
                    'ruleServiceId' => $ruleServiceId
                );
            }

            $objectReferenceList = array();

            //通过不同的分类获取仓库,并获取列表,并将列表通过id拼成一个大的数组,方便下一步的数据赋值
            foreach ($sourceTemplateIds as $category => $val) {
                $ids = array_unique(array_column($val, 'sourceTemplateId'));
                $sourceTemplateRepository = $this->getRepository($category);
                $sourceTemplateListTemp = $sourceTemplateRepository->fetchList($ids);
                
                foreach ($val as $v) {
                    if (isset($sourceTemplateListTemp[$v['sourceTemplateId']])) {
                        $objectReferenceList[$v['ruleServiceId']] =
                        $sourceTemplateListTemp[$v['sourceTemplateId']];
                    }
                }
            }

            //将获取到的来源资源目录数据赋值
            if (!empty($objectReferenceList)) {
                foreach ($ruleServiceList as $ruleService) {
                    $ruleServiceId = $ruleService->getId();
                    if ($ruleService instanceof UnAuditedRuleService) {
                        $ruleServiceId = $ruleService->getApplyId();
                    }
                    if (isset($objectReferenceList[$ruleServiceId])) {
                        $ruleService->setSourceTemplate($objectReferenceList[$ruleServiceId]);
                    }
                }
            }
        }
    }
    
    /**
     * 补全规则和比对规则获取信息项来源目录名称和信息项名称
     * @param RuleService $ruleService
     * @return RuleService
     */
    protected function fetchRules($ruleService)
    {
        if (!$ruleService instanceof INull) {
            $rules = $ruleService->getRules();

            foreach ($rules as $key => $rule) {
                if ($key == IRule::RULE_NAME['COMPLETION_RULE'] || $key == IRule::RULE_NAME['COMPARISON_RULE']) {
                    $condition = $this->fetchCondition($rule);
                    $rule->setCondition($condition);
                    $rules[$key] = $rule;
                }
            }

            $ruleService->setRules($rules);
        }
    }

    /**
     * 通过规则条件获取信息项来源目录名称和信息项名称
     * rules->rule->condition->patterns->pattern
     * @param Rule $rule
     * @return array
     */
    protected function fetchCondition(Rule $rule)
    {
        $condition = $rule->getCondition();
        $templateList = $this->fetchRuleTemplate($condition);

        if (!empty($templateList)) {
            foreach ($condition as $identify => $patterns) {
                foreach ($patterns as $key => $pattern) {
                    $templateName = $this->fetchTemplateName($templateList, $pattern);
                    $itemName = $this->fetchItemName($templateList, $pattern);

                    $condition[$identify][$key] = array(
                        'id' => $pattern['id'],
                        'name' => $templateName,
                        'base' => $pattern['base'],
                        'item' => $pattern['item'],
                        'itemName' => $itemName
                    );
                }
            }
        }

        return $condition;
    }

    //获取资源目录列表
    protected function fetchRuleTemplate(array $condition)
    {
        $templateList = array();
        //获取资源目录ids集合
        $templateIds = array();
        foreach ($condition as $patterns) {
            foreach ($patterns as $pattern) {
                $templateIds[] = $pattern['id'];
            }
        }
        $templateIds = array_unique($templateIds);

        return $this->getGbTemplateRepository()->fetchList($templateIds);
    }

    //获取资源目录名称
    protected function fetchTemplateName(array $templateList, array $pattern)
    {
        return isset($templateList[$pattern['id']]) ? $templateList[$pattern['id']]->getName() : '';
    }

    //获取资源目录信息项名称
    protected function fetchItemName(array $templateList, array $pattern)
    {
        $itemsTemp = isset($templateList[$pattern['id']]) ? $templateList[$pattern['id']]->getItems() : array();
        $items = array();

        if (!empty($itemsTemp)) {
            foreach ($itemsTemp as $item) {
                $items[$item['identify']] = $item;
            }
        }

        return isset($items[$pattern['item']]) ? $items[$pattern['item']]['name'] : '';
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData\Query;

use Marmot\Framework\Interfaces\DbLayer;
use Marmot\Interfaces\CacheLayer;

class MockBjItemsDataRowCacheQuery extends BjItemsDataRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

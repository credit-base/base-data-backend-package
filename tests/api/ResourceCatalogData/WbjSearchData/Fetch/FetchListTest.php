<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试委办局资源目录接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_wbj_search_data');
        $this->clear('pcore_wbj_items_data');
    }

    /**
     * 存在多条委办局资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data'],
                'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data']
            ]
        );
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function testViewWbjSearchData()
    {
        $setWbjSearchDataList = $this->wbjSearchData()['pcore_wbj_search_data'];
        
        $setTemplateList = $this->wbjTemplate();
        foreach ($setTemplateList as $setTemplate) {
            $setTemplateList[$setTemplate['wbj_template_id']] = $setTemplate;
        }
        $setWbjItemsDataList = $this->wbjSearchData()['pcore_wbj_items_data'];
        foreach ($setWbjItemsDataList as $setWbjItemsData) {
            $setWbjItemsDataList[$setWbjItemsData['wbj_items_data_id']] = $setWbjItemsData;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'wbjSearchData/1,2?include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $wbjSearchData) {
            $this->wbjSearchDataCompareData($setWbjSearchDataList[$key], $wbjSearchData);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'wbjItemsData') {
                $this->wbjItemsDataCompareData($setWbjItemsDataList[$value['id']], $value);
            }
            if ($value['type'] == 'templates') {
                $this->wbjTemplateCompareData($setTemplateList[$value['id']], $value);
            }
        }
    }
}

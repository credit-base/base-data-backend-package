<?php
namespace BaseData\Template\CommandHandler\WbjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Command\WbjTemplate\EditWbjTemplateCommand;

use BaseData\UserGroup\Model\UserGroup;

class EditWbjTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditWbjTemplateCommandHandler::class)
                                     ->setMethods(['getWbjTemplateRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetWbjTemplateRepository()
    {
        $commandHandler = new MockEditWbjTemplateCommandHandler();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\WbjTemplateRepository',
            $commandHandler->getWbjTemplateRepository()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $wbjTemplate = $this->initWbjTemplate($command, true);

        $wbjTemplateRepository = $this->prophesize(WbjTemplateRepository::class);
        $wbjTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($wbjTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getWbjTemplateRepository')
            ->willReturn($wbjTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $wbjTemplate = $this->initWbjTemplate($command, false);

        $wbjTemplateRepository = $this->prophesize(WbjTemplateRepository::class);
        $wbjTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($wbjTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getWbjTemplateRepository')
            ->willReturn($wbjTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new EditWbjTemplateCommand(
            $this->faker->word,
            $this->faker->regexify('[A-Z_]{1,100}'),
            $this->faker->randomElements(Template::SUBJECT_CATEGORY, 2),
            $this->faker->randomElement(Template::DIMENSION),
            $this->faker->randomElement(Template::EXCHANGE_FREQUENCY),
            $this->faker->randomElement(Template::INFO_CLASSIFY),
            $this->faker->randomElement(Template::INFO_CATEGORY),
            $this->faker->sentence,
            $this->faker->randomElements(
                array(
                    array(
                        "name" => '金额',    //信息项名称
                        "identify" => 'JE',    //数据标识
                        "type" => Template::TYPE['FDX'],    //数据类型
                        "length" => '24',    //数据长度
                    ),
                    array(
                        "name" => '日期',    //信息项名称
                        "identify" => 'RQ',    //数据标识
                        "type" => Template::TYPE['RQX'],    //数据类型
                        "length" => '8',    //数据长度
                    ),
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => Template::TYPE['ZFX'],    //数据类型
                        "length" => '50',    //数据长度
                    )
                ),
                2
            ),
            $this->faker->randomDigit,
            $this->faker->randomDigit
        );
        return $command;
    }

    private function initWbjTemplate(ICommand $command, bool $result)
    {
        $wbjTemplate = $this->prophesize(WbjTemplate::class);
        $wbjTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $wbjTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $wbjTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $wbjTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $wbjTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $wbjTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $wbjTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);

        $items = $this->commandHandler->setDefaultInfo($command);
        $wbjTemplate->setItems(Argument::exact($items))->shouldBeCalledTimes(1);
        $wbjTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $wbjTemplate->setSourceUnit(new UserGroup($command->sourceUnit))->shouldBeCalledTimes(1);
        $wbjTemplate->edit()->shouldBeCalledTimes(1)->willReturn($result);

        return $wbjTemplate;
    }
}

<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class WorkOrderTaskCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand' =>
        'BaseData\WorkOrderTask\CommandHandler\WorkOrderTask\RevokeWorkOrderTaskCommandHandler',
        'BaseData\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand' =>
        'BaseData\WorkOrderTask\CommandHandler\WorkOrderTask\ConfirmWorkOrderTaskCommandHandler',
        'BaseData\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand' =>
        'BaseData\WorkOrderTask\CommandHandler\WorkOrderTask\EndWorkOrderTaskCommandHandler',
        'BaseData\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand' =>
        'BaseData\WorkOrderTask\CommandHandler\WorkOrderTask\FeedbackWorkOrderTaskCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

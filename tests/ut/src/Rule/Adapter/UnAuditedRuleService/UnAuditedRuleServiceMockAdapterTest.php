<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService;

use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\UnAuditedRuleService;

class UnAuditedRuleServiceMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new UnAuditedRuleServiceMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new UnAuditedRuleService()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new UnAuditedRuleService(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\UnAuditedRuleService',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Rule\Model\UnAuditedRuleService',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Rule\Model\UnAuditedRuleService',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

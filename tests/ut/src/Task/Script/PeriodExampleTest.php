<?php
namespace BaseData\Task\Script;

use PHPUnit\Framework\TestCase;

class PeriodExampleTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new PeriodExample();
    }

    public function testExtendsPeriodTask()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Model\PeriodTask',
            $this->task
        );
    }

    public function testExecuteAction()
    {
        $result = $this->task->executeAction();

        $this->assertTrue($result);
    }

    public function testGetPeriod()
    {
        $this->assertEquals(
            30,
            $this->task->getPeriod()
        );
    }
}

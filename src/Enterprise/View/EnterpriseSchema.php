<?php
namespace BaseData\Enterprise\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class EnterpriseSchema extends SchemaProvider
{
    protected $resourceType = 'enterprises';

    public function getId($enterprise) : int
    {
        return $enterprise->getId();
    }

    public function getAttributes($enterprise) : array
    {
        return [
            'name' => $enterprise->getName(),
            'unifiedSocialCreditCode' => $enterprise->getUnifiedSocialCreditCode(),
            'establishmentDate' => $enterprise->getEstablishmentDate(),
            'approvalDate' => $enterprise->getApprovalDate(),
            'address' => $enterprise->getAddress(),
            'registrationCapital' => $enterprise->getRegistrationCapital(),
            'businessTermStart' => $enterprise->getBusinessTermStart(),
            'businessTermTo' => $enterprise->getBusinessTermTo(),
            'businessScope' => $enterprise->getBusinessScope(),
            'registrationAuthority' => $enterprise->getRegistrationAuthority(),
            'principal' => $enterprise->getPrincipal(),
            'principalCardId' => $enterprise->getPrincipalCardId(),
            'registrationStatus'  => $enterprise->getRegistrationStatus(),
            'enterpriseTypeCode'  => $enterprise->getEnterpriseTypeCode(),
            'enterpriseType'  => $enterprise->getEnterpriseType(),
            'data'  => $enterprise->getData(),
            'industryCategory'  => $enterprise->getIndustryCategory(),
            'industryCode'  => $enterprise->getIndustryCode(),
            'administrativeArea'  => $enterprise->getAdministrativeArea(),
            'hash' => $enterprise->getHash(),
            'taskId' => $enterprise->getTask()->getId(),
            'status' => $enterprise->getStatus(),
            'createTime' => $enterprise->getCreateTime(),
            'updateTime' => $enterprise->getUpdateTime(),
            'statusTime' => $enterprise->getStatusTime(),
        ];
    }
}

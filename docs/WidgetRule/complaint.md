# 信用投诉控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

* [互动类通用控件](./docs/WidgetRule/interaction.md "互动类通用控件")
* [信用表扬/信用投诉通用控件](./docs/WidgetRule/commentInteraction.md "信用表扬/信用投诉通用控件")

## 信用投诉控件规范

* `TITLE`: 标题控件规范.
* `CONTENT`: 内容控件规范.
* `MEMBER`: 前台用户控件规范.
* `ACCEPT_USER_GROUP`: 受理委办局控件规范.
* `REPLAY_CONTENT`: 回复内容控件规范.
* `REPLAY_IMAGES`: 回复图片控件规范.
* `REPLAY_CREW`: 受理人控件规范.
* `REPLAY_ADMISSIBILITY`: 回复受理情况控件规范.
* `NAME`: 反馈人真实姓名/企业名称控件规范.
* `IDENTIFY`: 统一社会信用代码/反馈人身份证号控件规范.
* `SUBJECT`: 被投诉主体控件规范.
* `TYPE`: 被投诉类型控件规范.
* `CONTACT`: 联系方式控件规范.
* `IMAGES`: 图片控件规范.

### `TITLE`
* [标题控件规范(TITLE)](interaction.md)

### `CONTENT`
* [内容控件规范(CONTENT)](interaction.md)

### `MEMBER`
* [前台用户控件规范(MEMBER)](interaction.md)

### `ACCEPT_USER_GROUP`
* [受理委办局控件规范(ACCEPT_USER_GROUP)](interaction.md)

### `REPLAY_CONTENT`
* [回复内容控件规范(REPLAY_CONTENT)](interaction.md)

### `REPLAY_IMAGES`
* [回复图片控件规范(REPLAY_IMAGES)](interaction.md)

### `REPLAY_CREW`
* [受理人控件规范(REPLAY_CREW)](interaction.md)

### `REPLAY_ADMISSIBILITY`
* [回复受理情况控件规范(REPLAY_ADMISSIBILITY)](interaction.md)

### `NAME`
* [反馈人真实姓名/企业名称控件规范(NAME)](commentInteraction.md)

### `IDENTIFY`
* [反馈人真实姓名/企业名称控件规范(IDENTIFY)](commentInteraction.md)

### `SUBJECT`
* [被投诉主体控件规范(SUBJECT)](commentInteraction.md)

### `TYPE`
* [被投诉类型控件规范(TYPE)](commentInteraction.md)

### `CONTACT`
* [联系方式控件规范(CONTACT)](commentInteraction.md)

### `IMAGES`
* [图片控件规范(IMAGES)](commentInteraction.md)

<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\Persistence;

class MockWorkOrderTaskCache extends WorkOrderTaskCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

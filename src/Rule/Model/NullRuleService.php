<?php
namespace BaseData\Rule\Model;

use Marmot\Interfaces\INull;

use BaseData\Common\Model\NullOperateTrait;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class NullRuleService extends RuleService implements INull
{
    use NullOperateTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function isNormal() : bool
    {
        return $this->resourceNotExist();
    }

    public function delete() : bool
    {
        return $this->resourceNotExist();
    }

    protected function validate() : bool
    {
        return $this->resourceNotExist();
    }

    protected function commonValidate() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isCrewNull() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isUserGroupNull() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isSourceTemplateNull() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isTransformationTemplateNull() : bool
    {
        return $this->resourceNotExist();
    }

    protected function isRuleExist() : bool
    {
        return $this->resourceNotExist();
    }

    protected function requiredItemVerification() : bool
    {
        return $this->resourceNotExist();
    }

    protected function rulesValidate() : bool
    {
        return $this->resourceNotExist();
    }

    protected function updateTemplateRuleCount() : bool
    {
        return $this->resourceNotExist();
    }

    public function transformation(ISearchDataAble $sourceSearchData) : ISearchDataAble
    {
        return $sourceSearchData;
    }

    public function getSortRules(array $rules) : array
    {
        return $rules;
    }
}

<?php
namespace BaseData\Template\Adapter\QzjTemplate\Query;

use Marmot\Framework\Query\RowCacheQuery;

class QzjTemplateRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'qzj_template_id',
            new Persistence\QzjTemplateCache(),
            new Persistence\QzjTemplateDb()
        );
    }
}

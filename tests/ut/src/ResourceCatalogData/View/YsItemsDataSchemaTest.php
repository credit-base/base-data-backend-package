<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class YsItemsDataSchemaTest extends TestCase
{
    private $ysItemsDataSchema;

    private $ysItemsData;

    public function setUp()
    {
        $this->ysItemsDataSchema = new YsItemsDataSchema(new Factory());

        $this->ysItemsData = \BaseData\ResourceCatalogData\Utils\YsItemsDataMockFactory::generateYsItemsData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->ysItemsDataSchema);
        unset($this->ysItemsData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->ysItemsDataSchema);
    }

    public function testGetId()
    {
        $result = $this->ysItemsDataSchema->getId($this->ysItemsData);

        $this->assertEquals($result, $this->ysItemsData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->ysItemsDataSchema->getAttributes($this->ysItemsData);

        $this->assertEquals($result['data'], $this->ysItemsData->getData());
    }
}

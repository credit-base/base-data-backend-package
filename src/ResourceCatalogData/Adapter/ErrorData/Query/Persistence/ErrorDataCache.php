<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ErrorDataCache extends Cache
{
    const KEY = 'error_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

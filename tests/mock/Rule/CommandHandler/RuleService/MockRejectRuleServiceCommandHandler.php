<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use BaseData\Common\Model\IApproveAble;
use BaseData\Common\Command\RejectCommand;

class MockRejectRuleServiceCommandHandler extends RejectRuleServiceCommandHandler
{
    public function executeAction(RejectCommand $command)
    {
        return parent::executeAction($command);
    }

    public function fetchIApplyObject($id) : IApproveAble
    {
        return parent::fetchIApplyObject($id);
    }
}

CREATE TABLE `pcore_enterprise` (
  `enterprise_id` int(10) NOT NULL COMMENT '企业主键id',
  `name` varchar(255) NOT NULL COMMENT '企业名称/个体名称',
  `unified_social_credit_code` varchar(50) NOT NULL COMMENT '统一社会信用代码',
  `establishment_date` int(10) NOT NULL COMMENT '成立日期/注册日期',
  `approval_date` int(10) NOT NULL COMMENT '核准日期',
  `address` varchar(255) NOT NULL COMMENT '住所/经营场所',
  `registration_capital` varchar(50) NOT NULL COMMENT '注册资本（万）',
  `business_term_start` int(10) NOT NULL COMMENT '营业期限自',
  `business_term_to` int(10) NOT NULL COMMENT '营业期限至',
  `business_scope` text NOT NULL COMMENT '经营范围',
  `registration_authority` varchar(255) NOT NULL COMMENT '登记机关',
  `principal` varchar(255) NOT NULL COMMENT '法定代表人/经营者',
  `principal_card_id` varchar(20) NOT NULL COMMENT '法人身份证号',
  `registration_status` varchar(255) NOT NULL COMMENT '登记状态(1=存续(在营、开业、在册);2=吊销，未注销;3=吊 销，已注销;4=注销;5=撤 销;6=迁出;9=其他)',
  `enterprise_type_code` varchar(255) NOT NULL COMMENT '企业类型代码',
  `enterprise_type` varchar(255) NOT NULL COMMENT '企业类型',
  `data` text NOT NULL COMMENT '其他数据',
  `industry_category` char(1) NOT NULL COMMENT '行业门类',
  `industry_code` char(5) NOT NULL COMMENT '行业代码',
  `administrative_area` int(4) NOT NULL COMMENT '所属区域',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_NORMAL,0,默认),(STATUS_DELETE,-2,删除)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业表';

ALTER TABLE `pcore_enterprise`
  ADD PRIMARY KEY (`enterprise_id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `unified_social_credit_code` (`unified_social_credit_code`);

ALTER TABLE `pcore_enterprise`
  MODIFY `enterprise_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '企业主键id';

-- --------------------------------------------------------

--
-- 统计企业的行政许可,行政处罚,守信激励,失信惩戒数据
--

DROP PROCEDURE IF EXISTS statics_enterprise_relation_information_count;

DELIMITER //
CREATE PROCEDURE statics_enterprise_relation_information_count(in ug1 varchar(255), in ug2 varchar(255)) 
BEGIN
  DECLARE licensing_total, penalty_total, incentive_total, punish_total INT;
  SET licensing_total = 0, penalty_total = 0, incentive_total = 0, punish_total = 0;
  SELECT COUNT(bj_search_data_id) INTO licensing_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 1 AND dimension IN (ug2);
    SELECT COUNT(bj_search_data_id) INTO penalty_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 2 AND dimension IN (ug2);
  SELECT COUNT(bj_search_data_id) INTO incentive_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 3 AND dimension IN (ug2);
  SELECT COUNT(bj_search_data_id) INTO punish_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 4 AND dimension IN (ug2);
  SELECT licensing_total, penalty_total, incentive_total, punish_total;
END//
DELIMITER ;


INSERT INTO `pcore_enterprise` (`enterprise_id`, `name`, `unified_social_credit_code`, `establishment_date`, `approval_date`, `address`, `registration_capital`, `business_term_start`, `business_term_to`, `business_scope`, `registration_authority`, `principal`, `principal_card_id`, `registration_status`, `enterprise_type_code`, `enterprise_type`, `data`, `industry_category`, `industry_code`, `administrative_area`, `hash`, `task_id`, `create_time`, `update_time`, `status`, `status_time`) VALUES
(1, '百瑞泰管业股份有限公司', '21120101600530263M', 20000110, 20000110, '南阳', '100', 20000110, 20300110, '装饰类', '发展和改革委员会', '百泰', '610526199703036713', '营业', '3661', '有限公司', 'eJxLtDKwqq4FAAZPAf4=', 'H', '2374', 0, 'ef77a36213267f193c3398e658d291d4', 0, 1516174523, 1516174523, 0, 0),
(2, '德恒科技有限公司', '31120101600530264M', 20000101, 20000109, '南阳', '18', 20000109, 20300109, '科技类', '发展和改革委员会', '赵楚', '111525198503245845', '营业', '3661', '有限公司', 'eJxLtDKwqq4FAAZPAf4=', 'G', '2373', 0, '657a5fcc2da0e06496527d498a1418d0', 0, 1516174523, 1516174523, 0, 0),
(3, '青曲烟酒有限公司', '323434323234545001', 20200411, 20200411, '南阳', '100', 20200411, 20500411, '便利店', '发展和改革委员会', '李而', '610526199603036713', '登记状态', '3661', '有限公司', 'eJxLtDKwqq4FAAZPAf4=', 'A', '11', 0, 'ce323d3e5714c741a3d2ecef34ca5676', 0, 1516174523, 1516174523, 0, 0),
(4, '宏远园林工程有限公司', '610527199403165500', 19980323, 20180504, '黑牛城道298号', '100', 19980323, 20180504, '园林绿化工程；园林绿化工程设计；花、苗、木（种子除外）生产销售、租、摆；室内外装饰；市政公用工程施工；园林机械设备、建材制品、五金交电化工（不含危险品）、日用杂品、装饰材料、百货零售兼批发；场地租赁；劳务服务（不含劳务派遣）；房屋租赁。（依法须经批准的项目，经相关部门批准后方可开展经营活动', '市场和质量监督管理局', '王新华', '610526199703036719', '存续（在营、开业、在册)', '150', '有限责任公司', 'eJxLtDKwqq4FAAZPAf4=', 'A', '11', 0, 'ae5e00d0e0e658dbca84ef7be329ef16', 0, 1516174523, 1516174523, 0, 0);

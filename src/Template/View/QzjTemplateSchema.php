<?php
namespace BaseData\Template\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class QzjTemplateSchema extends SchemaProvider
{
    protected $resourceType = 'qzjTemplates';

    public function getId($qzjTemplate) : int
    {
        return $qzjTemplate->getId();
    }

    public function getAttributes($qzjTemplate) : array
    {
        return [
            'name' => $qzjTemplate->getName(),
            'identify' => $qzjTemplate->getIdentify(),
            'subjectCategory' => $qzjTemplate->getSubjectCategory(),
            'dimension' => $qzjTemplate->getDimension(),
            'exchangeFrequency' => $qzjTemplate->getExchangeFrequency(),
            'infoClassify' => $qzjTemplate->getInfoClassify(),
            'infoCategory' => $qzjTemplate->getInfoCategory(),
            'description' => $qzjTemplate->getDescription(),
            'category' => $qzjTemplate->getCategory(),
            'items' => $qzjTemplate->getItems(),

            'status' => $qzjTemplate->getStatus(),
            'createTime' => $qzjTemplate->getCreateTime(),
            'updateTime' => $qzjTemplate->getUpdateTime(),
            'statusTime' => $qzjTemplate->getStatusTime(),
        ];
    }

    public function getRelationships($qzjTemplate, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'sourceUnit' => [self::DATA => $qzjTemplate->getSourceUnit()]
        ];
    }
}

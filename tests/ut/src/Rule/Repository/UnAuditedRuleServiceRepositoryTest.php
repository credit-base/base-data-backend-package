<?php
namespace BaseData\Rule\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Utils\MockFactory;
use BaseData\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;
use BaseData\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceDbAdapter;

class UnAuditedRuleServiceRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(UnAuditedRuleServiceRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockUnAuditedRuleServiceRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new UnAuditedRuleServiceDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $ruleService = MockFactory::generateUnAuditedRuleService($id);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->add(Argument::exact($ruleService))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($ruleService, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $ruleService = MockFactory::generateUnAuditedRuleService($id);
        $keys = array();
        
        $adapter = $this->prophesize(IUnAuditedRuleServiceAdapter::class);
        $adapter->edit(Argument::exact($ruleService), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($ruleService, $keys);
    }
}

<?php
namespace BaseData\Template\Adapter\GbTemplate;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Utils\GbTemplateMockFactory;

class GbTemplateMockAdapter implements IGbTemplateAdapter
{
    public function fetchOne($id) : GbTemplate
    {
        return GbTemplateMockFactory::generateGbTemplate($id);
    }

    public function fetchList(array $ids) : array
    {
        $gbTemplateList = array();

        foreach ($ids as $id) {
            $gbTemplateList[$id] = GbTemplateMockFactory::generateGbTemplate($id);
        }

        return $gbTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(GbTemplate $gbTemplate) : bool
    {
        unset($gbTemplate);
        return true;
    }

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool
    {
        unset($gbTemplate);
        unset($keys);
        return true;
    }
}

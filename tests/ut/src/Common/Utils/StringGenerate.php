<?php
namespace Common\Utils;

class StringGenerate
{
    public static function generate($length = 0)
    {
        $chars = '2019-01-10,天气温度零下5度,小雪,阴转小雪转阴';
        $max = mb_strlen($chars, 'utf-8') - 1;
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= mb_substr($chars, mt_rand(0, $max), 1, 'utf-8');
        }
        return $string;
    }
}

<?php
namespace BaseData\Template\Adapter\BjTemplate\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class BjTemplateDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockBjTemplateDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(BjTemplateDb::TABLE, $this->database->getTable());
    }
}

<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

class MockGbSearchDataRepository extends GbSearchDataRepository
{
    public function getAdapter() : IGbSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IGbSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IGbSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}

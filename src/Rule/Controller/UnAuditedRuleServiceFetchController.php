<?php
namespace BaseData\Rule\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Rule\View\UnAuditedRuleServiceView;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;

class UnAuditedRuleServiceFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UnAuditedRuleServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IUnAuditedRuleServiceAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UnAuditedRuleServiceView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'unAuditedRules';
    }
}

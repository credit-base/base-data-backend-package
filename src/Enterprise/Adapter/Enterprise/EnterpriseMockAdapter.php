<?php
namespace BaseData\Enterprise\Adapter\Enterprise;

use BaseData\Enterprise\Model\Enterprise;
use BaseData\Enterprise\Utils\MockFactory;

class EnterpriseMockAdapter implements IEnterpriseAdapter
{
    public function fetchOne($id) : Enterprise
    {
        return MockFactory::generateEnterprise($id);
    }

    public function fetchList(array $ids) : array
    {
        $enterpriseList = array();

        foreach ($ids as $id) {
            $enterpriseList[$id] = MockFactory::generateEnterprise($id);
        }

        return $enterpriseList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Enterprise $enterprise) : bool
    {
        unset($enterprise);
        return true;
    }

    public function edit(Enterprise $enterprise, array $keys = array()) : bool
    {
        unset($enterprise);
        unset($keys);
        return true;
    }
}

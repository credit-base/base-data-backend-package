<?php
namespace BaseData\Statistical\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Statistical\Model\Statistical;

use BaseData\Statistical\Adapter\Statistical\IStatisticalAdapter;
use BaseData\Statistical\Adapter\Statistical\MockStatisticalAdapter;

class StatisticalRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = new StatisticalRepository(
            new MockStatisticalAdapter()
        );
        parent::setUp();
    }

    public function testImplementsIStatisticalAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Statistical\Adapter\Statistical\IStatisticalAdapter',
            $this->repository
        );
    }

    public function testAnalyse()
    {
        $filter = array();

        $adapter = $this->prophesize(IStatisticalAdapter::class);
        $adapter->analyse(Argument::exact($filter))->shouldBeCalledTimes(1);

        $this->repository->setAdapter($adapter->reveal());

        $this->repository->analyse($filter);
    }
}

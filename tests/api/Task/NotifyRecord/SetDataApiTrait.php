<?php
namespace BaseData\Task\NotifyRecord;

use Marmot\Core;

use BaseData\Task\Model\NotifyRecord;

trait SetDataApiTrait
{
    protected function notifyRecord() : array
    {
        return
        [
            [
                'record_id' => 1,
                'name' => 'test1.xlxs',
                'hash' => 'C4CA4238A0B923820DCC509A6F75849B',
                'status' => NotifyRecord::STATUS['PENDING'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'record_id' => 2,
                'name' => 'test2.xlxs',
                'hash' => 'fb918afc06ff4c28b87dc03b5030a2d4',
                'status' => NotifyRecord::STATUS['SUCCESS'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}

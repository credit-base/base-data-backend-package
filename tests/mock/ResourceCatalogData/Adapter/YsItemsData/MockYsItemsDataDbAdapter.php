<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Translator\YsItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\YsItemsDataRowCacheQuery;

class MockYsItemsDataDbAdapter extends YsItemsDataDbAdapter
{
    public function getDbTranslator() : YsItemsDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : YsItemsDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

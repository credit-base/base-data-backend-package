<?php
namespace BaseData\Enterprise\Adapter\Enterprise\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class EnterpriseCache extends Cache
{
    const KEY = 'enterprise';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

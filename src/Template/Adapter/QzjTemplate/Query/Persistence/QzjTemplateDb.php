<?php
namespace BaseData\Template\Adapter\QzjTemplate\Query\Persistence;

use Marmot\Framework\Classes\Db;

class QzjTemplateDb extends Db
{
    const TABLE = 'qzj_template';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

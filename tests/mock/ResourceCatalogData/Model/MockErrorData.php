<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class MockErrorData extends ErrorData
{
    public function getRepository() : IErrorDataAdapter
    {
        return parent::getRepository();
    }
}

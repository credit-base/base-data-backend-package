<?php
namespace BaseData\WorkOrderTask\Utils;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;
use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\UserGroup\Model\UserGroup;

class ParentTaskMockFactory
{
    public static function generateParentTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ParentTask {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $parentTask = new ParentTask($id);
        $parentTask->setId($id);

        self::generateTemplateType($parentTask, $faker, $value);
        self::generateTemplate($parentTask, $faker, $value);
        self::generateAssignObjects($parentTask, $faker, $value);
        self::generateStatus($parentTask, $faker, $value);
        
        $parentTask->setCreateTime($faker->unixTime());
        $parentTask->setUpdateTime($faker->unixTime());
        $parentTask->setStatusTime($faker->unixTime());

        return $parentTask;
    }

    private static function generateTemplateType($parentTask, $faker, $value) : void
    {
        $templateType = isset($value['templateType']) ?
            $value['templateType'] :
            $faker->randomElement(ParentTask::TEMPLATE_TYPE);
        
        $parentTask->setTemplateType($templateType);
    }

    private static function generateTemplate($parentTask, $faker, $value) : void
    {
        unset($value);
        if ($parentTask->getTemplateType() == ParentTask::TEMPLATE_TYPE['GB']) {
            $template = new GbTemplate($faker->randomDigit);
        }
        if ($parentTask->getTemplateType() == ParentTask::TEMPLATE_TYPE['BJ']) {
            $template = new BjTemplate($faker->randomDigit);
        }
        
        $parentTask->setTemplate($template);
    }

    private static function generateAssignObjects($parentTask, $faker, $value) : void
    {
        $assignObjects = isset($value['assignObjects']) ?
            array(new UserGroup($value['assignObjects'])) :
            array(new UserGroup($faker->randomDigit));
            
        $parentTask->setAssignObjects($assignObjects);
    }

    private static function generateStatus($parentTask, $faker, $value) : void
    {
        unset($faker);
        $status = isset($value['status']) ?
            $value['status'] :
            0;
        
        $parentTask->setStatus($status);
    }
}

<?php
namespace BaseData\Rule\Model;

use Marmot\Core;

use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

use BaseData\Common\Model\IApproveAble;
use BaseData\Common\Model\IResubmitAble;
use BaseData\Common\Model\ApproveAbleTrait;
use BaseData\Common\Model\ResubmitAbleTrait;

use BaseData\Crew\Model\Crew;

use BaseData\UserGroup\Model\UserGroup;

class UnAuditedRuleService extends RuleService implements IApproveAble, IResubmitAble
{
    use ApproveAbleTrait, ResubmitAbleTrait;

    protected $applyId;

    protected $publishCrew;

    protected $userGroup;

    protected $applyCrew;

    protected $applyUserGroup;

    protected $operationType;
   
    protected $applyInfo;

    protected $relationId;

    protected $repository;
    
    public function __construct(int $applyId = 0)
    {
        parent::__construct();
        $this->applyId = !empty($applyId) ? $applyId : 0;
        $this->publishCrew = new Crew();
        $this->userGroup = new UserGroup();
        $this->applyCrew = new Crew();
        $this->applyUserGroup = new UserGroup();
        $this->operationType = self::OPERATION_TYPE['NULL'];
        $this->applyInfo = array();
        $this->relationId = 0;
        $this->applyStatus = self::APPLY_STATUS['PENDING'];
        $this->rejectReason = '';
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->applyId);
        unset($this->publishCrew);
        unset($this->userGroup);
        unset($this->applyCrew);
        unset($this->applyUserGroup);
        unset($this->operationType);
        unset($this->applyInfo);
        unset($this->relationId);
        unset($this->applyStatus);
        unset($this->rejectReason);
    }
    
    public function setApplyId($applyId) : void
    {
        $this->applyId = $applyId;
    }

    public function getApplyId()
    {
        return $this->applyId;
    }

    public function setPublishCrew(Crew $publishCrew) : void
    {
        $this->publishCrew = $publishCrew;
    }

    public function getPublishCrew() : Crew
    {
        return $this->publishCrew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setApplyCrew(Crew $applyCrew) : void
    {
        $this->applyCrew = $applyCrew;
    }

    public function getApplyCrew() : Crew
    {
        return $this->applyCrew;
    }

    public function setApplyUserGroup(UserGroup $applyUserGroup) : void
    {
        $this->applyUserGroup = $applyUserGroup;
    }

    public function getApplyUserGroup() : UserGroup
    {
        return $this->applyUserGroup;
    }

    public function setOperationType(int $operationType) : void
    {
        $this->operationType = in_array($operationType, IApproveAble::OPERATION_TYPE)
        ? $operationType
        : IApproveAble::OPERATION_TYPE['NULL'];
    }

    public function getOperationType() : int
    {
        return $this->operationType;
    }

    public function setApplyInfo(array $applyInfo) : void
    {
        $this->applyInfo = $applyInfo;
    }

    public function getApplyInfo() : array
    {
        return $this->applyInfo;
    }

    public function setRelationId(int $relationId) : void
    {
        $this->relationId = $relationId;
    }

    public function getRelationId() : int
    {
        return $this->relationId;
    }

    public function generateVersion() : void
    {
        $this->version = $this->getCreateTime();
    }

    protected function getUnAuditedRuleServiceRepository() : UnAuditedRuleServiceRepository
    {
        return new UnAuditedRuleServiceRepository();
    }

    public function add() : bool
    {
        return $this->apply();
    }

    public function edit() : bool
    {
        return $this->apply();
    }

    protected function apply() : bool
    {
        return $this->validate() && $this->getUnAuditedRuleServiceRepository()->add($this);
    }

    protected function resubmitAction() : bool
    {
        $this->setApplyStatus(IApproveAble::APPLY_STATUS['PENDING']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getUnAuditedRuleServiceRepository()->edit($this, array(
            'publishCrew',
            'applyInfo',
            'applyStatus',
            'updateTime',
            'statusTime'
        ));
    }

    protected function approveAction() : bool
    {
        return $this->updateApplyStatus(IApproveAble::APPLY_STATUS['APPROVE'])
            && $this->approveApplyInfo($this->getOperationType())
            && $this->updateRelationId();
    }
    /**
     * @codeCoverageIgnore
     */
    protected function approveApplyInfo(int $operationType) : bool
    {
        switch ($operationType) {
            //添加
            case IApproveAble::OPERATION_TYPE['ADD']:
                return parent::add();
            //编辑
            case IApproveAble::OPERATION_TYPE['EDIT']:
                return parent::edit();
        }
    }
    
    protected function rejectAction() : bool
    {
        return $this->updateApplyStatus(IApproveAble::APPLY_STATUS['REJECT']);
    }

    protected function updateApplyStatus(int $applyStatus) : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'applyStatus'));
            return false;
        }

        $this->setApplyStatus($applyStatus);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getUnAuditedRuleServiceRepository()->edit(
            $this,
            array(
                'updateTime',
                'applyStatus',
                'statusTime',
                'applyCrew',
                'applyUserGroup',
                'rejectReason',
                'publishCrew',
                'crew'
            )
        );
    }

    protected function updateRelationId() : bool
    {
        if ($this->getOperationType() == IApproveAble::OPERATION_TYPE['ADD']) {
            $this->setRelationId($this->getId());
     
            return $this->getUnAuditedRuleServiceRepository()->edit(
                $this,
                array('relationId')
            );
        }

        return true;
    }

    public function revoke() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'applyStatus'));
            return false;
        }

        return $this->updateApplyStatus(IApproveAble::APPLY_STATUS['REVOKE']);
    }
}

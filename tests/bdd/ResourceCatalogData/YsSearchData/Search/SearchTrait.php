<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Search;

use PHPUnit\DbUnit\DataSet\ArrayDataSet;

trait SearchTrait
{
    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_ys_items_data' => $this->ysSearchData()['pcore_ys_items_data'],
            'pcore_ys_search_data' => $this->ysSearchData()['pcore_ys_search_data']
        ]);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Service;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\NullWbjTemplate;
use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\FormatValidationRule;
use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Model\SearchDataModelFactory;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Model\NullCrew;
use BaseData\Crew\Repository\CrewRepository;

use BaseData\ResourceCatalogData\Model\SearchData;
use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Model\BjItemsData;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class SyncTaskServiceTest extends TestCase
{
    private $stub;

    protected $fileName;

    protected $initializationData;

    public function setUp()
    {
        $this->fileName = 'XZXK_1_'.Template::CATEGORY['BJ'].'_1_123456789.xsl';

        $this->stub = new MockSyncTaskService($this->fileName);

        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew(1);
        $sourceTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $fileContent = array('fileContent1', 'fileContent2');
        $this->initializationData = array(
            'crew' => $crew,
            'sourceTemplate' => $sourceTemplate,
            'fileContent' => $fileContent,
            'sourceCategory' => Template::CATEGORY['BJ']
        );
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->stub);
        unset($this->fileName);
        unset($this->initializationData);
    }

    public function testGetFileName()
    {
        $this->assertIsString($this->stub->getFileName());
    }

    public function testGetCrewRepository()
    {
        $this->assertInstanceof(
            'BaseData\Crew\Repository\CrewRepository',
            $this->stub->getCrewRepository()
        );
    }

    public function testFetchCrew()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['getCrewRepository'])
                        ->getMock();
        $id = 1;

        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew($id);

        $repository = $this->prophesize(CrewRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($crew);
        $stub->expects($this->exactly(1))->method('getCrewRepository')->willReturn($repository->reveal());

        $result = $stub->fetchCrew($id);

        $this->assertEquals($result, $crew);
    }

    public function testGetTask()
    {
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\Task',
            $this->stub->getTask()
        );
    }

    public function testGetRuleService()
    {
        $this->assertInstanceof(
            'BaseData\Rule\Model\RuleService',
            $this->stub->getRuleService()
        );
    }

    public function testGetFormatValidationRule()
    {
        $this->assertInstanceof(
            'BaseData\Rule\Model\FormatValidationRule',
            $this->stub->getFormatValidationRule()
        );
    }

    public function testExecuteValidateDataCorrect()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['validateFileFormat'])
                        ->getMock();

        $stub->expects($this->once())->method('validateFileFormat')->willReturn(false);

        $result = $stub->execute();

        $this->assertEquals(array(array(), new NullTask()), $result);
    }

    public function testExecuteCountEmpty()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['validateFileFormat', 'initializationData', 'validateDataCorrect'])
                        ->getMock();

        $stub->expects($this->once())->method('validateFileFormat')->willReturn(true);
        $stub->expects($this->once())->method('initializationData')->willReturn($this->initializationData);
        $stub->expects($this->once())->method('validateDataCorrect')->willReturn(false);

        $result = $stub->execute();

        $this->assertEquals(array(array(), new NullTask()), $result);
    }

    public function testExecuteNullTask()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['validateFileFormat', 'validateDataCorrect', 'addTask', 'initializationData'])
                        ->getMock();

        $stub->expects($this->once())->method('validateFileFormat')->willReturn(true);
        $stub->expects($this->once())->method('initializationData')->willReturn($this->initializationData);
        $stub->expects($this->once())->method('validateDataCorrect')->willReturn(true);
        $stub->expects($this->once())->method('addTask')->willReturn(new NullTask());

        $result = $stub->execute();

        $this->assertEquals(array(array(), new NullTask()), $result);
    }

    public function testExecute()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods([
                            'getFileName',
                            'validateFileFormat',
                            'initializationData',
                            'validateDataCorrect',
                            'addTask',
                            'creditRuleService',
                            'fileContentToSearchDataObjectArray',
                            'searchDataTransformationAndAdd',
                            'updateTask',
                            'fetchRuleServiceList'
                        ])->getMock();

        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService(1);
        $sourceSearchDataList = array('sourceSearchDataList');
        $ruleServiceList = array($ruleService);

        $stub->expects($this->once())->method('getFileName')->willReturn($this->fileName);
        $stub->expects($this->once())->method('validateFileFormat')->willReturn(true);
        $stub->expects($this->once())->method('initializationData')->willReturn($this->initializationData);
        $stub->expects($this->once())->method('validateDataCorrect')->willReturn(true);
        $stub->expects($this->once())->method('addTask')->willReturn($task);
        $stub->expects($this->once())->method('creditRuleService')->willReturn($ruleService);
        $stub->expects($this->once())->method('fileContentToSearchDataObjectArray')->willReturn($sourceSearchDataList);
        $stub->expects($this->once())->method('searchDataTransformationAndAdd');
        $stub->expects($this->once())->method('updateTask');
        $stub->expects($this->once())->method('fetchRuleServiceList')->willReturn($ruleServiceList);

        $result = $stub->execute();

        $this->assertEquals(array($ruleServiceList, $task), $result);
    }

    public function testValidateFileFormat()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['isFileExist', 'isFileNameFormatCorrect'])
                        ->getMock();
                       
        $stub->expects($this->once())->method('isFileExist')->with($this->fileName)->willReturn(true);
        $stub->expects($this->once())->method('isFileNameFormatCorrect')->with($this->fileName)->willReturn(true);

        $result = $stub->validateFileFormat($this->fileName);

        $this->assertTrue($result);
    }

    public function testIsFileExistTrue()
    {
        $fileName = '';
        $result = $this->stub->isFileExist($fileName);

        $this->assertTrue($result);
    }

    public function testIsFileExistFalse()
    {
        $result = $this->stub->isFileExist($this->fileName);

        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('file', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
    
    /**
     * @dataProvider isFileNameFormatCorrectProvider
     */
    public function testIsFileNameFormatCorrect($actual, $expected)
    {
        $result = $this->stub->isFileNameFormatCorrect($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
            $this->assertEquals('fileName', Core::getLastError()->getSource()['pointer']);
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function isFileNameFormatCorrectProvider()
    {
        return array(
            array('XZXK_1_'.Template::CATEGORY['BJ'].'_1_123456789.xsl', true),
            array('XZXK_1', false),
            array('XZXK_template_1_1_3455', false),
            array('XZXK_1_1000_1_3455', false),
            array('XZXK_1_1_crew_3455', false),
        );
    }

    public function testInitializationData()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['fetchCrew', 'fetchSourceTemplate', 'fetchFileContent'])
                        ->getMock();
                             
        $fileNameInfo = explode('_', $this->fileName);
        $sourceCategory = $fileNameInfo[2];

        $data = $this->initializationData;
        $data['sourceCategory'] = $sourceCategory;

        $stub->expects($this->once())->method('fetchCrew')->willReturn($data['crew']);
        $stub->expects($this->once())->method('fetchSourceTemplate')->willReturn($data['sourceTemplate']);
        $stub->expects($this->once())->method('fetchFileContent')->willReturn($data['fileContent']);

        $result = $stub->initializationData($this->fileName);

        $this->assertEquals($data, $result);
    }

    private function initialFetchSourceTemplate($count)
    {
        $this->stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['getRepositoryFactory'])
                        ->getMock();

        $template = new WbjTemplate(1);
        $template->setIdentify('identify');
        $sourceCategory = 1;
        $sourceTemplateId = 1;

        $factory = $this->prophesize(RepositoryFactory::class);
        $repository = $this->prophesize(WbjTemplateRepository::class);

        $repository->fetchOne(
            Argument::exact($sourceTemplateId)
        )->shouldBeCalledTimes(1)->willReturn($template);

        $factory->getRepository(
            Argument::exact($sourceCategory)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $this->stub->expects($this->once())->method('getRepositoryFactory')->willReturn($factory->reveal());

        return [$sourceCategory, $sourceTemplateId];
    }

    public function testFetchSourceTemplateTrue()
    {
        $identify = 'identify';

        list($sourceCategory, $sourceTemplateId) = $this->initialFetchSourceTemplate(
            SyncTaskService::SOURCE_TEMPLATE_COUNT,
            $identify
        );

        $result = $this->stub->fetchSourceTemplate($sourceCategory, $identify, $sourceTemplateId);

        $this->assertInstanceof(
            'BaseData\Template\Model\WbjTemplate',
            $result
        );
    }

    public function testFetchSourceTemplateFalse()
    {
        $identify = 'identifyFalse';

        list($sourceCategory, $sourceTemplateId) = $this->initialFetchSourceTemplate(
            SyncTaskService::SOURCE_TEMPLATE_COUNT+1,
            $identify
        );

        $result = $this->stub->fetchSourceTemplate($sourceCategory, $identify, $sourceTemplateId);

        $this->assertInstanceof(
            'BaseData\Template\Model\NullWbjTemplate',
            $result
        );
    }

    public function testValidateDataCorrect()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['isCrewExist', 'isSourceTemplateExist', 'isFileContentEmpty'])
                        ->getMock();

        $stub->expects($this->once())->method(
            'isCrewExist'
        )->with($this->initializationData['crew'])->willReturn(true);
        $stub->expects($this->once())->method(
            'isSourceTemplateExist'
        )->with($this->initializationData['sourceTemplate'])->willReturn(true);
        $stub->expects($this->once())->method(
            'isFileContentEmpty'
        )->with($this->initializationData['fileContent'])->willReturn(true);

        $result = $stub->validateDataCorrect($this->initializationData);

        $this->assertTrue($result);
    }

    public function testIsCrewExistTrue()
    {
        $result = $this->stub->isCrewExist($this->initializationData['crew']);

        $this->assertTrue($result);
    }
    
    public function testIsCrewExistFalse()
    {
        $result = $this->stub->isCrewExist(new NullCrew());

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crewId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsSourceTemplateExistTrue()
    {
        $result = $this->stub->isSourceTemplateExist($this->initializationData['sourceTemplate']);

        $this->assertTrue($result);
    }
    
    public function testIsSourceTemplateExistFalse()
    {
        $result = $this->stub->isSourceTemplateExist(new NullWbjTemplate());

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('sourceTemplateId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsFileContentEmptyTrue()
    {
        $result = $this->stub->isFileContentEmpty($this->initializationData['fileContent']);

        $this->assertTrue($result);
    }
    
    public function testIsFileContentEmptyFalse()
    {
        $result = $this->stub->isFileContentEmpty(array());

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('file', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    private function initialAddTask($result)
    {
        $this->stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array($this->fileName))
                        ->setMethods(['getTask'])
                        ->getMock();
                
        $task = $this->prophesize(Task::class);
        $task->setCrew(Argument::exact($this->initializationData['crew']))->shouldBeCalledTimes(1);
        $task->setUserGroup(
            Argument::exact($this->initializationData['crew']->getUserGroup())
        )->shouldBeCalledTimes(1);
        $task->setTotal(Argument::exact(count($this->initializationData['fileContent'])-1))->shouldBeCalledTimes(1);
        $task->setSourceCategory(Argument::exact($this->initializationData['sourceCategory']))->shouldBeCalledTimes(1);
        $task->setSourceTemplate(
            Argument::exact($this->initializationData['sourceTemplate']->getId())
        )->shouldBeCalledTimes(1);
        $task->setTargetCategory(Argument::exact($this->initializationData['sourceCategory']))->shouldBeCalledTimes(1);
        $task->setTargetTemplate(
            Argument::exact($this->initializationData['sourceTemplate']->getId())
        )->shouldBeCalledTimes(1);
        $task->setTargetRule(
            Argument::exact($this->initializationData['sourceTemplate']->getId())
        )->shouldBeCalledTimes(1);
        $task->setScheduleTask(Argument::exact(posix_getpid()))->shouldBeCalledTimes(1);
        $task->setFileName(Argument::exact($this->fileName))->shouldBeCalledTimes(1);

        $task->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->once())->method('getTask')->willReturn($task->reveal());
    }

    public function testAddTaskTrue()
    {
        $this->initialAddTask(true);

        $result = $this->stub->addTask($this->initializationData);

        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\Task',
            $result
        );
    }

    public function testAddTaskFalse()
    {
        $this->initialAddTask(false);

        $result = $this->stub->addTask($this->initializationData);

        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\NullTask',
            $result
        );
    }

    public function testCreditRuleService()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['fetchTemplateItems', 'getFormatValidationRule', 'getRuleService'])
                        ->getMock();

        $items = array('items');
        $sourceTemplate = $this->initializationData['sourceTemplate'];

        $stub->expects($this->once())->method('fetchTemplateItems')->willReturn($items);

        $formatValidationRule = $this->prophesize(FormatValidationRule::class);
        $formatValidationRule->setCondition(Argument::exact($items))->shouldBeCalledTimes(1);
        $formatValidationRule->setTransformationTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);
        $formatValidationRule->setSourceTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);

        $rules = array('formatValidationRule' => $formatValidationRule->reveal());

        $ruleService = $this->prophesize(RuleService::class);
        $ruleService->setRules(Argument::exact($rules))->shouldBeCalledTimes(1);
        $ruleService->setTransformationTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);
        $ruleService->setSourceTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);
        $ruleService->setTransformationCategory(
            Argument::exact($sourceTemplate->getCategory())
        )->shouldBeCalledTimes(1);
        $ruleService->setSourceCategory(Argument::exact($sourceTemplate->getCategory()))->shouldBeCalledTimes(1);

        $stub->expects($this->once())->method(
            'getFormatValidationRule'
        )->willReturn($formatValidationRule->reveal());
        $stub->expects($this->once())->method('getRuleService')->willReturn($ruleService->reveal());

        $result = $stub->creditRuleService($sourceTemplate);

        $this->assertInstanceof(
            'BaseData\Rule\Model\RuleService',
            $result
        );
    }

    public function testGenerateSourceSearchData()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['getSourceSearchData'])
                        ->getMock();

        $sourceTemplate = $this->initializationData['sourceTemplate'];
        $crew = $this->initializationData['crew'];

        $sourceSearchData = $this->prophesize(BjSearchData::class);
        $sourceSearchData->setCrew(Argument::exact($crew))->shouldBeCalledTimes(1);
        $sourceSearchData->setSourceUnit(Argument::exact($crew->getUserGroup()))->shouldBeCalledTimes(1);
        $sourceSearchData->setTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);
        $sourceSearchData->setInfoClassify(
            Argument::exact($sourceTemplate->getInfoClassify())
        )->shouldBeCalledTimes(1);
        $sourceSearchData->setInfoCategory(
            Argument::exact($sourceTemplate->getInfoCategory())
        )->shouldBeCalledTimes(1);
        $sourceSearchData->setDimension(
            Argument::exact($sourceTemplate->getDimension())
        )->shouldBeCalledTimes(1);

        $stub->expects($this->once())->method('getSourceSearchData')->willReturn($sourceSearchData->reveal());

        $result = $stub->generateSourceSearchData($this->initializationData);

        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\ISearchDataAble',
            $result
        );
    }

    public function testGetSourceSearchData()
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['getSearchDataModelFactory'])
                        ->getMock();

        $sourceCategory = 1;
        $model = new BjSearchData();

        $factory = $this->prophesize(SearchDataModelFactory::class);

        $factory->getModel(Argument::exact($sourceCategory))->shouldBeCalledTimes(1)->willReturn($model);

        $stub->expects($this->once())->method('getSearchDataModelFactory')->willReturn($factory->reveal());

        $result = $stub->getSourceSearchData($sourceCategory);

        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\ISearchDataAble',
            $result
        );
    }

    //getReader
    public function testGetReader()
    {
        $fileName = '/var/www/html/tests/mock/ResourceCatalogData/Service/MockFile';
        $stub = new MockSyncTaskService($fileName);
        $reader = $stub->getReader($fileName);
        $this->assertInstanceof(
            '\PhpOffice\PhpSpreadsheet\Reader\BaseReader',
            $reader
        );
    }
    
    /**
     * 设置 Task setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException InvalidArgumentException
     */
    public function testGetReaderFile()
    {
        $fileName = '';
        $stub = new MockSyncTaskService($fileName);
        $reader = $stub->getReader($fileName);
        $this->assertInstanceof(
            '\PhpOffice\PhpSpreadsheet\Reader\BaseReader',
            $reader
        );
    }

    //fetchFileContent
    public function testFetchFileContent()
    {
        $fileName = 'fileName';
        $data = ['data'];

        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods(['getReader'])
                        ->getMock();

        $workSheet = $this->prophesize(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::class);
        $workSheet->toArray()->shouldBeCalledTimes(1)->willReturn($data);
        $spreadsheet = $this->prophesize(\PhpOffice\PhpSpreadsheet\Spreadsheet::class);
        $spreadsheet->getActiveSheet()->shouldBeCalledTimes(1)->willReturn($workSheet->reveal());
        
        $reader = $this->prophesize(MockReader::class);
        $reader->setReadDataOnly(true)->shouldBeCalledTimes(1);
        $reader->load(
            Core::$container->get('attachment.upload.path').$fileName
        )->shouldBeCalledTimes(1)->willReturn($spreadsheet->reveal());

        $stub->expects($this->once())->method('getReader')->willReturn($reader->reveal());

        $result = $stub->fetchFileContent($fileName);
        $this->assertEquals($data, $result);
    }

    public function testFetchFileContentFail()
    {
        $fileName = '';
        $stub = new MockSyncTaskService($fileName);

        $result = $stub->fetchFileContent($fileName);
        $this->assertEquals([], $result);
    }

    //fileDataToItemsData
    public function testFileDataToItemsData()
    {
        $items = ['key'=>['type'=>Template::TYPE['ZFX'],'identify'=>'identify']];
        $fileData = ['key'=>'value'];

        $fileName = 'fileName';
        $stub = new MockSyncTaskService($fileName);

        $result = $stub->fileDataToItemsData($fileData, $items);
        $this->assertEquals(['identify'=>'value'], $result);
    }

    public function testFileDataToItemsDataDefaultValue()
    {
        $items = ['key'=>['type'=>Template::TYPE['ZFX'],'identify'=>'identify']];
        $fileData = ['keyNotExist'=>'value'];

        $fileName = 'fileName';
        $stub = new MockSyncTaskService($fileName);

        $result = $stub->fileDataToItemsData($fileData, $items);
        $this->assertEquals(['identify'=>Template::TEMPLATE_TYPE_DEFAULT[Template::TYPE['ZFX']]], $result);
    }

    public function testFileDataToItemsDataEmpty()
    {
        $fileName = 'fileName';
        $stub = new MockSyncTaskService($fileName);
        $fileData = [];
        $items = [];

        $result = $stub->fileDataToItemsData($fileData, $items);
        $this->assertEquals([], $result);
    }

    //fileContentToSearchDataObjectArray
    public function initialFileContentToSearchDataObjectArray($itemsDataData)
    {
        $stub = $this->getMockBuilder(MockSyncTaskService::class)
                        ->setConstructorArgs(array(''))
                        ->setMethods([
                            'generateSourceSearchData',
                            'fileDataToItemsData',
                            'generateSubjectCategory'])->getMock();

        $data = ['fileContent'=>[1=>['fileData']]];
        $items = ['items'];
        $subjectCategory = 1;

        $expirationDate = isset($itemsDataData['GQSJ']) ?
                        strtotime($itemsDataData['GQSJ']) :
                        SearchData::EXPIRATION_DATE_DEFAULT;

        $template = $this->prophesize(Template::class);
        $searchData = $this->prophesize(BjSearchData::class);
        $itemsData = $this->prophesize(BjItemsData::class);
        $itemsData->setData($itemsDataData)->shouldBeCalledTimes(1);

        $data['sourceTemplate'] = $template->reveal();

        $template->getItems()->shouldBeCalledTimes(1)->willReturn($items);

        $stub->expects($this->once())->method('generateSourceSearchData')
                                      ->with($data)
                                      ->willReturn($searchData->reveal());

        $stub->expects($this->once())->method('fileDataToItemsData')
                                      ->with(['fileData'], $items)
                                      ->willReturn($itemsDataData);

        $stub->expects($this->once())->method('generateSubjectCategory')
                                      ->with($itemsDataData)
                                      ->willReturn($subjectCategory);
        
        $searchData->setId(1)->shouldBeCalledTimes(1);
        $searchData->setExpirationDate($expirationDate)->shouldBeCalledTimes(1);
        $searchData->setSubjectCategory($subjectCategory)->shouldBeCalledTimes(1);
        $searchData->getItemsData()->shouldBeCalledTimes(1)->willReturn($itemsData->reveal());
        $searchData->generateHash()->shouldBeCalledTimes(1);

        $result = $stub->fileContentToSearchDataObjectArray($data);
        $this->assertEquals([$searchData->reveal()], $result);
    }

    public function testFileContentToSearchDataObjectArray()
    {
        $itemsDataData = ['itemsDataData'];
        $this->initialFileContentToSearchDataObjectArray($itemsDataData);
    }

    public function testFileContentToSearchDataObjectArrayExpirationDate()
    {
        $itemsDataData = ['GQSJ'=>'20210817'];
        $this->initialFileContentToSearchDataObjectArray($itemsDataData);
    }

    //generateSubjectCategory
    public function testGenerateSubjectCategory()
    {
        $fileName = 'fileName';
        $itemsData = ['ZTLB'=>'自然人'];

        $stub = new MockSyncTaskService($fileName);
        $result = $stub->generateSubjectCategory($itemsData);
        $this->assertEquals(ISearchDataAble::SUBJECT_CATEGORY_CN['自然人'], $result);
    }

    public function testGenerateSubjectCategoryFail()
    {
        $fileName = 'fileName';
        $itemsData = [];

        $stub = new MockSyncTaskService($fileName);
        $result = $stub->generateSubjectCategory($itemsData);
        $this->assertEquals(0, $result);
    }
}

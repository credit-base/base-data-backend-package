

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_gb_template;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_bj_template;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_wbj_template;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_qzj_template;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_parent_task;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_work_order_task;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_ys_search_data;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_wbj_search_data;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_bj_search_data;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_gb_search_data;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_schedule_task;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_apply_form_rule;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_rule;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_task;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_schedule_task_notify_record;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_error_data;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_enterprise;

-- --------------------------------------------------------

DROP TABLE IF EXISTS pcore_base_template;

-- --------------------------------------------------------

DROP PROCEDURE IF EXISTS statics_enterprise_relation_information_count;

-- --------------------------------------------------------
<?php
namespace BaseData\ResourceCatalogData\Strategy;

class NullDataStrategy implements IDataStrategy
{
    public function validate($value, $rule = array()) : bool
    {
        unset($value);
        unset($rule);
        return false;
    }

    public function mock($rule = array())
    {
        unset($rule);
        return false;
    }
}

<?php
namespace BaseData\Template\BjTemplate\Search;

use PHPUnit\DbUnit\DataSet\ArrayDataSet;

trait SearchTrait
{
    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_gb_template' => $this->gbTemplate(),
        ]);
    }
}

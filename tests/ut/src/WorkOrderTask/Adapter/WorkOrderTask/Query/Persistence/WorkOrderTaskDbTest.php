<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class WorkOrderTaskDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockWorkOrderTaskDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(WorkOrderTaskDb::TABLE, $this->database->getTable());
    }
}

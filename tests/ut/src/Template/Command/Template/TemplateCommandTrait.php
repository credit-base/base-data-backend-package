<?php
namespace BaseData\Template\Command\Template;

use PHPUnit\Framework\TestCase;

trait TemplateCommandTrait
{
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testNameParameter()
    {
        $this->assertEquals($this->fakerData['name'], $this->command->name);
    }

    public function testIdentifyParameter()
    {
        $this->assertEquals($this->fakerData['identify'], $this->command->identify);
    }

    public function testSubjectCategoryParameter()
    {
        $this->assertEquals($this->fakerData['subjectCategory'], $this->command->subjectCategory);
    }

    public function testDimensionParameter()
    {
        $this->assertEquals($this->fakerData['dimension'], $this->command->dimension);
    }

    public function testExchangeFrequencyParameter()
    {
        $this->assertEquals($this->fakerData['exchangeFrequency'], $this->command->exchangeFrequency);
    }

    public function testInfoClassifyParameter()
    {
        $this->assertEquals($this->fakerData['infoClassify'], $this->command->infoClassify);
    }

    public function testInfoCategoryParameter()
    {
        $this->assertEquals($this->fakerData['infoCategory'], $this->command->infoCategory);
    }

    public function testDescriptionParameter()
    {
        $this->assertEquals($this->fakerData['description'], $this->command->description);
    }

    public function testItemsParameter()
    {
        $this->assertEquals($this->fakerData['items'], $this->command->items);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\NullGbSearchData;
use BaseData\ResourceCatalogData\Translator\GbSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\GbSearchDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class GbSearchDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockGbSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchTemplate',
                                'fetchItemsData',
                                'searchDataFormatFilter'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IGbSearchDataAdapter
     */
    public function testImplementsIGbSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 GbSearchDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockGbSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\GbSearchDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 GbSearchDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockGbSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\GbSearchDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetItemsDataRepository()
    {
        $adapter = new MockGbSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\GbItemsDataRepository',
            $adapter->getItemsDataRepository()
        );
    }

    public function testGetTemplateRepository()
    {
        $adapter = new MockGbSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\GbTemplateRepository',
            $adapter->getTemplateRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockGbSearchDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedGbSearchData = new GbSearchData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedGbSearchData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedGbSearchData);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedGbSearchData = new GbSearchData();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedGbSearchData, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedGbSearchData, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedGbSearchData = new GbSearchData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedGbSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedGbSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedGbSearchData);

        //验证
        $gbSearchData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedGbSearchData, $gbSearchData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $gbSearchDataOne = new GbSearchData(1);
        $gbSearchDataTwo = new GbSearchData(2);

        $ids = [1, 2];

        $expectedGbSearchDataList = [];
        $expectedGbSearchDataList[$gbSearchDataOne->getId()] = $gbSearchDataOne;
        $expectedGbSearchDataList[$gbSearchDataTwo->getId()] = $gbSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedGbSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedGbSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedGbSearchDataList);
        //验证
        $gbSearchDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedGbSearchDataList, $gbSearchDataList);
    }

    public function testFormatFilter()
    {
        $filter = array();
        $gbSearchData = new GbSearchData();
        $expectedCondition = ' 1 ';

        $this->adapter->expects($this->exactly(1))
                        ->method('searchDataFormatFilter')
                        ->with($filter, $gbSearchData)
                        ->willReturn($expectedCondition);

        $condition = $this->adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter/template
    public function testFormatFilterTemplate()
    {
        $filter = array(
            'template' => 1
        );
        $adapter = new MockGbSearchDataDbAdapter();

        $expectedCondition = 'gb_template_id = '.$filter['template'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockGbSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY gb_search_data_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockGbSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY gb_search_data_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockGbSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockGbSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Translator\GbItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\GbItemsDataRowCacheQuery;

class MockGbItemsDataDbAdapter extends GbItemsDataDbAdapter
{
    public function getDbTranslator() : GbItemsDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : GbItemsDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

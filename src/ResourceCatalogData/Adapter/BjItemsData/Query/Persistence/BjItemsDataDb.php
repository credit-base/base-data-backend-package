<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class BjItemsDataDb extends Db
{
    const TABLE = 'bj_items_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

<?php
namespace BaseData\Template\Model;

use PHPUnit\Framework\TestCase;

class NullBaseTemplateTest extends TestCase
{
    private $baseTemplate;

    public function setUp()
    {
        $this->baseTemplate = NullBaseTemplate::getInstance();
    }

    public function tearDown()
    {
        unset($this->baseTemplate);
    }

    public function testExtendsBaseTemplate()
    {
        $this->assertInstanceof('BaseData\Template\Model\BaseTemplate', $this->baseTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->baseTemplate);
    }
}

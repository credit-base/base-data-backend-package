<?php
namespace BaseData\Task\Model;

use BaseData\Task\Adapter\Task\ITaskAdapter;
use BaseData\Task\Repository\TaskRepository;

use Marmot\Core;

class Schedule
{
    protected $runTime;

    protected $repository;

    public function __construct()
    {
        $this->runTime = new RunTime();
        $this->repository = new TaskRepository();
    }

    public function __destruct()
    {
        unset($this->runTime);
        unset($this->taskRepostiroy);
    }

    protected function getRepository() : ITaskAdapter
    {
        return $this->repository;
    }

    protected function getRunTime() : RunTime
    {
        return $this->runTime;
    }

    /**
     * 多进程调度，暂不运行单元测试
     * @SuppressWarnings(PHPMD)
     * @codeCoverageIgnore
     */
    public function run() : void
    {
        $tasks = $this->dispatch();

        foreach ($tasks as $task) {
            //log
            $task->scheduled();

            pcntl_signal(SIGCHLD, SIG_IGN);
            $pid = pcntl_fork();
            if ($pid) {
            } elseif ($pid == 0) {
                cli_set_process_title('marmot-schedule: '.$task->getIdentify());

                $core = Core::getInstance();
                $core->initCli();
                
                $task->refresh();
                $task->execute();

                exit();
            }
        }
    }

    public function dispatch() : array
    {
        return $this->getRepository()->schedule();
    }
}

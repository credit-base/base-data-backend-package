<?php
namespace BaseData\Rule\Adapter\RuleService\Query\Persistence;

class MockRuleServiceDb extends RuleServiceDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

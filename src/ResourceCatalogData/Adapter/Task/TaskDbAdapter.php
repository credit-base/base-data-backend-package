<?php
namespace BaseData\ResourceCatalogData\Adapter\Task;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;
use BaseData\ResourceCatalogData\Translator\TaskDbTranslator;
use BaseData\ResourceCatalogData\Adapter\Task\Query\TaskRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class TaskDbAdapter implements ITaskAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new TaskDbTranslator();
        $this->rowCacheQuery = new TaskRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : TaskDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : TaskRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullTask::getInstance();
    }

    public function add(Task $task) : bool
    {
        return $this->addAction($task);
    }

    public function edit(Task $task, array $keys = array()) : bool
    {
        return $this->editAction($task, $keys);
    }

    public function fetchOne($id) : Task
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $task = new Task();

            if (isset($filter['crew'])) {
                $task->getCrew()->setId($filter['crew']);
                $info = $this->getDbTranslator()->objectToArray($task, array('crew'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['userGroup'])) {
                $task->getUserGroup()->setId($filter['userGroup']);
                $info = $this->getDbTranslator()->objectToArray($task, array('userGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['pid'])) {
                $task->setPid($filter['pid']);
                $info = $this->getDbTranslator()->objectToArray($task, array('pid'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceCategory'])) {
                $task->setSourceCategory($filter['sourceCategory']);
                $info = $this->getDbTranslator()->objectToArray($task, array('sourceCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceTemplate'])) {
                $task->setSourceTemplate($filter['sourceTemplate']);
                $info = $this->getDbTranslator()->objectToArray($task, array('sourceTemplate'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['targetCategory'])) {
                $task->setTargetCategory($filter['targetCategory']);
                $info = $this->getDbTranslator()->objectToArray($task, array('targetCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['targetTemplate'])) {
                $task->setTargetTemplate($filter['targetTemplate']);
                $info = $this->getDbTranslator()->objectToArray($task, array('targetTemplate'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['targetRule'])) {
                $task->setTargetRule($filter['targetRule']);
                $info = $this->getDbTranslator()->objectToArray($task, array('targetRule'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $info = $this->getDbTranslator()->objectToArray($task, array('status'));
                $condition .= $conjection.key($info).' IN ('.$filter['status'].')';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['createTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new Task(), array('createTime'));
                $condition .= $conjection.key($info).' '.($sort['createTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

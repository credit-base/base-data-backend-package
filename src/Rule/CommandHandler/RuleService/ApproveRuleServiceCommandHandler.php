<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Common\Model\IApproveAble;
use BaseData\Common\Command\ApproveCommand;
use BaseData\Common\CommandHandler\ApproveCommandHandler;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Command\RuleService\ApproveRuleServiceCommand;

class ApproveRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ApproveRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedRuleService = $this->fetchUnAuditedRuleService($command->id);

        $applyCrew = $this->fetchCrew($command->applyCrew);
        $unAuditedRuleService->setApplyCrew($applyCrew);

        foreach ($unAuditedRuleService->getRules() as $key => $condition) {
            $condition->setTransformationTemplate($unAuditedRuleService->getTransformationTemplate());
            $condition->setSourceTemplate($unAuditedRuleService->getSourceTemplate());
            $rules[$key] = $condition;
        }
        $unAuditedRuleService->setRules($rules);
        
        return $unAuditedRuleService->approve();
    }
}

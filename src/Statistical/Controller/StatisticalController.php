<?php
namespace BaseData\Statistical\Controller;

use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use BaseData\Statistical\View\StatisticalView;
use BaseData\Statistical\Repository\StatisticalRepository;

class StatisticalController extends Controller
{
    use JsonApiTrait;

    protected function getStatisticalRepository(string $type) : StatisticalRepository
    {
        $adapterFactory = new IAdapterFactory();
        $adapter = $adapterFactory->getAdapter($type);
        return new StatisticalRepository(new $adapter);
    }

    public function analyse(string $type)
    {
        list($filter, $sort, $curpage, $perpage) = $this->formatParameters();

        unset($sort);
        unset($curpage);
        unset($perpage);

        $repository = $this->getStatisticalRepository($type);

        $statistical = $repository->analyse($filter);

        if (!$statistical instanceof INull) {
            $this->renderView(new StatisticalView($statistical));
            return true;
        }

        $this->displayError();
        return false;
    }
}

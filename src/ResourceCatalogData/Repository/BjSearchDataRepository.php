<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataMockAdapter;

class BjSearchDataRepository extends Repository implements IBjSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BjSearchDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IBjSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IBjSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IBjSearchDataAdapter
    {
        return new BjSearchDataMockAdapter();
    }

    public function fetchOne($id) : BjSearchData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(BjSearchData $bjSearchData) : bool
    {
        return $this->getAdapter()->add($bjSearchData);
    }

    public function edit(BjSearchData $bjSearchData, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($bjSearchData, $keys);
    }
}

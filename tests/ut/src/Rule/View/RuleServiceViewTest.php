<?php
namespace BaseData\Rule\View;

use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\RuleService;

class RuleServiceViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $ruleService = new RuleServiceView(new RuleService());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $ruleService);
    }
}

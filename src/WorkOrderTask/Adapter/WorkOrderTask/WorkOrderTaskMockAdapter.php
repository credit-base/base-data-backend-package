<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Utils\WorkOrderTaskMockFactory;

class WorkOrderTaskMockAdapter implements IWorkOrderTaskAdapter
{
    public function fetchOne($id) : WorkOrderTask
    {
        return WorkOrderTaskMockFactory::generateWorkOrderTask($id);
    }

    public function fetchList(array $ids) : array
    {
        $workOrderTaskList = array();

        foreach ($ids as $id) {
            $workOrderTaskList[$id] = WorkOrderTaskMockFactory::generateWorkOrderTask($id);
        }

        return $workOrderTaskList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(WorkOrderTask $workOrderTask) : bool
    {
        unset($workOrderTask);
        return true;
    }

    public function edit(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        unset($workOrderTask);
        unset($keys);
        return true;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Model\NullYsSearchData;
use BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\YsSearchDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class YsSearchDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockYsSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchTemplate',
                                'fetchItemsData',
                                'searchDataFormatFilter'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IYsSearchDataAdapter
     */
    public function testImplementsIYsSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 YsSearchDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockYsSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 YsSearchDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockYsSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\YsSearchDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetItemsDataRepository()
    {
        $adapter = new MockYsSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\YsItemsDataRepository',
            $adapter->getItemsDataRepository()
        );
    }

    public function testGetTemplateRepository()
    {
        $adapter = new MockYsSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\WbjTemplateRepository',
            $adapter->getTemplateRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockYsSearchDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedYsSearchData = new YsSearchData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedYsSearchData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedYsSearchData);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedYsSearchData = new YsSearchData();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedYsSearchData, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedYsSearchData, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedYsSearchData = new YsSearchData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedYsSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedYsSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedYsSearchData);

        //验证
        $ysSearchData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedYsSearchData, $ysSearchData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $ysSearchDataOne = new YsSearchData(1);
        $ysSearchDataTwo = new YsSearchData(2);

        $ids = [1, 2];

        $expectedYsSearchDataList = [];
        $expectedYsSearchDataList[$ysSearchDataOne->getId()] = $ysSearchDataOne;
        $expectedYsSearchDataList[$ysSearchDataTwo->getId()] = $ysSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedYsSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedYsSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedYsSearchDataList);
        //验证
        $ysSearchDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedYsSearchDataList, $ysSearchDataList);
    }

    public function testFormatFilter()
    {
        $filter = array();
        $ysSearchData = new YsSearchData();
        $expectedCondition = ' 1 ';

        $this->adapter->expects($this->exactly(1))
                        ->method('searchDataFormatFilter')
                        ->with($filter, $ysSearchData)
                        ->willReturn($expectedCondition);

        $condition = $this->adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter/template
    public function testFormatFilterTemplate()
    {
        $filter = array(
            'template' => 1
        );
        $adapter = new MockYsSearchDataDbAdapter();

        $expectedCondition = 'wbj_template_id = '.$filter['template'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockYsSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY ys_search_data_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockYsSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY ys_search_data_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatSort-updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockYsSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockYsSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

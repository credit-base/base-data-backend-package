<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Model\NullUnAuditedRuleService;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;
use BaseData\Rule\Adapter\UnAuditedRuleService\Query\UnAuditedRuleServiceRowCacheQuery;

use BaseData\Template\Repository\WbjTemplateRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UnAuditedRuleServiceDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchRules',
                                'fetchSourceTemplate',
                                'fetchTransformationTemplate'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IUnAuditedRuleServiceAdapter
     */
    public function testImplementsIUnAuditedRuleServiceAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 UnAuditedRuleServiceDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockUnAuditedRuleServiceDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 UnAuditedRuleServiceRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockUnAuditedRuleServiceDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\UnAuditedRuleService\Query\UnAuditedRuleServiceRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockUnAuditedRuleServiceDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedUnAuditedRuleService = new UnAuditedRuleService();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedUnAuditedRuleService)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedUnAuditedRuleService);
        $this->assertTrue($result);
    }

    private function initialAddAction($result)
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceDbAdapter::class)
                        ->setMethods(['getDbTranslator', 'getRowQuery'])
                        ->getMock();

        $id = 1;
        $unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($id);
        $info = array($unAuditedRuleService);

        //预言
        $translator = $this->prophesize(UnAuditedRuleServiceDbTranslator::class);
        //期望objectToArray()被执行一次,并返回$info
        $translator->objectToArray($unAuditedRuleService)->shouldBeCalledTimes(1)->willReturn($info);

        //预言
        $rowQuery = $this->prophesize(UnAuditedRuleServiceRowCacheQuery::class);
        //期望add()被执行一次,并返回$result
        $rowQuery->add($info)->shouldBeCalledTimes(1)->willReturn($result);
        
        //揭示
        $adapter->expects($this->any())
                ->method('getRowQuery')
                ->willReturn($rowQuery->reveal());

        //揭示
        $adapter->expects($this->exactly(1))
                ->method('getDbTranslator')
                ->willReturn($translator->reveal());
                
        return [$adapter, $unAuditedRuleService];
    }

    //添加失败的情况
    public function testAddActionFailure()
    {
        list($adapter, $unAuditedRuleService) = $this->initialAddAction(false);

        //验证
        $result = $adapter->addAction($unAuditedRuleService);
        $this->assertFalse($result);
    }

    //添加成功的情况
    public function testAddActionSuccess()
    {
        list($adapter, $unAuditedRuleService) = $this->initialAddAction(true);

        //验证
        $result = $adapter->addAction($unAuditedRuleService);
        $this->assertTrue($result);
    }
    //edit
    public function testEdit()
    {
        //初始化
        $expectedUnAuditedRuleService = new UnAuditedRuleService();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedUnAuditedRuleService, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedUnAuditedRuleService, $keys);
        $this->assertTrue($result);
    }

    private function initialEditAction($result)
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceDbAdapter::class)
                        ->setMethods(['getDbTranslator', 'getRowQuery'])
                        ->getMock();

        //初始化
        $id = 1;
        $unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($id);

        $info = array($unAuditedRuleService);
        $keys = array();

        $conditionArray[$unAuditedRuleService->getApplyId()] = $unAuditedRuleService->getApplyId();

        //预言
        $translator = $this->prophesize(UnAuditedRuleServiceDbTranslator::class);
        //期望objectToArray()被执行一次并返回$info
        $translator->objectToArray($unAuditedRuleService, $keys)->shouldBeCalledTimes(1)->willReturn($info);

        //预言
        $rowQuery = $this->prophesize(UnAuditedRuleServiceRowCacheQuery::class);
        //期望getPrimaryKey()被执行一次返回applyId
        $rowQuery->getPrimaryKey()->shouldBeCalledTimes(1)->willReturn($unAuditedRuleService->getApplyId());
        //update()被执行一次返回$result
        $rowQuery->update($info, $conditionArray)->shouldBeCalledTimes(1)->willReturn($result);
        
        //揭示
        $adapter->expects($this->any())
                ->method('getRowQuery')
                ->willReturn($rowQuery->reveal());

        //揭示
        $adapter->expects($this->exactly(1))
                ->method('getDbTranslator')
                ->willReturn($translator->reveal());
                
        return [$adapter, $unAuditedRuleService, $keys];
    }

    public function testEditActionFailure()
    {
        list($adapter, $unAuditedRuleService, $keys) = $this->initialEditAction(false);

        //验证
        $result = $adapter->editAction($unAuditedRuleService, $keys);
        $this->assertFalse($result);
    }

    public function testEditActionSuccess()
    {
        list($adapter, $unAuditedRuleService, $keys) = $this->initialEditAction(true);

        //验证
        $result = $adapter->editAction($unAuditedRuleService, $keys);
        $this->assertTrue($result);
    }
    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedUnAuditedRuleService = new UnAuditedRuleService();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedUnAuditedRuleService);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceTemplate')
                         ->with($expectedUnAuditedRuleService);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTransformationTemplate')
                         ->with($expectedUnAuditedRuleService);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchRules')
                         ->with($expectedUnAuditedRuleService);
        //验证
        $ruleService = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedUnAuditedRuleService, $ruleService);
    }

    public function testFetchOneActionNotExist()
    {
        $id = 1;

        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceDbAdapter::class)
                        ->setMethods(['getRowQuery'])
                        ->getMock();

        $rowQuery = $this->prophesize(UnAuditedRuleServiceRowCacheQuery::class);
        $rowQuery->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn(array());

        $adapter->expects($this->exactly(1))
            ->method('getRowQuery')
            ->willReturn($rowQuery->reveal());

        $result = $adapter->fetchOneAction($id);
        $this->assertInstanceOf('BaseData\Rule\Model\NullUnAuditedRuleService', $result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testFetchOneActionSuccess()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceDbAdapter::class)
                ->setMethods([
                    'getRowQuery',
                    'getDbTranslator'
                ])->getMock();

        //初始化
        $id = 1;
        $info = array(
            'apply_info'=>base64_encode(gzcompress(serialize(array('apply_info')))),
            'apply_info_category'=>1
        );
        $unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($id);

        //预言
        $rowQuery = $this->prophesize(UnAuditedRuleServiceRowCacheQuery::class);
        $rowQuery->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($info);

        //揭示
        $adapter->expects($this->exactly(1))
                ->method('getRowQuery')
                ->willReturn($rowQuery->reveal());

        $applyInfo =unserialize(gzuncompress(base64_decode($info['apply_info'], true)));

        $info = array_merge($applyInfo, $info);

        //预言
        $unAuditedRuleServiceTranslator = $this->prophesize(UnAuditedRuleServiceDbTranslator::class);
        $unAuditedRuleServiceTranslator->arrayToObject(
            Argument::exact($info)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedRuleService);
        $unAuditedRuleServiceTranslator->arrayToObjects(
            Argument::exact($info),
            Argument::exact($unAuditedRuleService)
        )->shouldBeCalledTimes(1)->willReturn($unAuditedRuleService);

        //揭示
        $adapter->expects($this->exactly(1))
                ->method('getDbTranslator')
                ->willReturn($unAuditedRuleServiceTranslator->reveal());

        //验证
        $result = $adapter->fetchOneAction($id);
        $this->assertEquals($result, $unAuditedRuleService);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $ruleServiceOne = new UnAuditedRuleService(1);
        $ruleServiceTwo = new UnAuditedRuleService(2);

        $ids = [1, 2];

        $expectedUnAuditedRuleServiceList = [];
        $expectedUnAuditedRuleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $expectedUnAuditedRuleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedUnAuditedRuleServiceList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceTemplate')
                         ->with($expectedUnAuditedRuleServiceList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTransformationTemplate')
                         ->with($expectedUnAuditedRuleServiceList);

        //验证
        $ruleServiceList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedUnAuditedRuleServiceList, $ruleServiceList);
    }

    public function testFetchListActionFail()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceDbAdapter::class)
                        ->setMethods(['getRowQuery'])
                        ->getMock();
                        
        $ids = [1, 2];
        $ruleServiceList = array();

        $rowQuery = $this->prophesize(UnAuditedRuleServiceRowCacheQuery::class);
        $rowQuery->fetchList(Argument::exact($ids))
                            ->shouldBeCalledTimes(1)
                            ->willReturn($ruleServiceList);

        $adapter->expects($this->exactly(1))
                ->method('getRowQuery')
                ->willReturn($rowQuery->reveal());

        $result = $adapter->fetchListAction($ids);
        $this->assertEquals([], $result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testFetchListAction()
    {
        $adapter = $this->getMockBuilder(MockUnAuditedRuleServiceDbAdapter::class)
                        ->setMethods(['getRowQuery', 'getDbTranslator'])
                        ->getMock();
        //初始化
        $unAuditedRuleServiceOne = new UnAuditedRuleService(1);
        $unAuditedRuleServiceTwo = new UnAuditedRuleService(2);

        $info = array(
            $unAuditedRuleServiceOne->getApplyId() => array(
                'apply_info'=>base64_encode(gzcompress(serialize(array('apply_info')))),
                'apply_info_category'=>1
            ),
            $unAuditedRuleServiceTwo->getApplyId() => array(
                'apply_info'=>base64_encode(gzcompress(serialize(array('apply_info')))),
                'apply_info_category'=>2
            ),
        );
    
        $expectedObjectList = [];
        $expectedObjectList[$unAuditedRuleServiceOne->getApplyId()] = $unAuditedRuleServiceOne;
        $expectedObjectList[$unAuditedRuleServiceTwo->getApplyId()] = $unAuditedRuleServiceTwo;

        $ids = [1, 2];

        //预言
        $rowQuery = $this->prophesize(UnAuditedRuleServiceRowCacheQuery::class);
        $rowQuery->fetchList($ids)->shouldBeCalledTimes(1)->willReturn($info);

        $adapter->expects($this->exactly(1))
                         ->method('getRowQuery')
                         ->willReturn($rowQuery->reveal());
        
        $unAuditedRuleServiceTranslator = $this->prophesize(UnAuditedRuleServiceDbTranslator::class);
        
        foreach ($info as $key => $value) {
            $applyInfo =unserialize(gzuncompress(base64_decode($value['apply_info'], true)));

            $value = array_merge($applyInfo, $value);

            $unAuditedRuleServiceTranslator->arrayToObject(
                Argument::exact($value)
            )->shouldBeCalledTimes(1)->willReturn($expectedObjectList[$key]);
            $unAuditedRuleServiceTranslator->arrayToObjects(
                Argument::exact($value),
                Argument::exact($expectedObjectList[$key])
            )->shouldBeCalledTimes(1)->willReturn($expectedObjectList[$key]);
        }

        $adapter->expects($this->any())
                ->method('getDbTranslator')
                ->willReturn($unAuditedRuleServiceTranslator->reveal());
                         
        //验证
        $objectList = $adapter->fetchListAction($ids);

        $this->assertEquals($expectedObjectList, $objectList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-userGroup
    public function testFormatFilterUserGroup()
    {
        $filter = array(
            'userGroup' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'usergroup_id = '.$filter['userGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-relationId
    public function testFormatFilterRelationId()
    {
        $filter = array(
            'relationId' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'relation_id = '.$filter['relationId'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-transformationTemplate
    public function testFormatFilterTransformationTemplate()
    {
        $filter = array(
            'transformationTemplate' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'transformation_template_id = '.$filter['transformationTemplate'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-sourceTemplate
    public function testFormatFilterSourceTemplate()
    {
        $filter = array(
            'sourceTemplate' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'source_template_id = '.$filter['sourceTemplate'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-applyStatus
    public function testFormatFilterApplyStatus()
    {
        $filter = array(
            'applyStatus' => 1,2
        );
        
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'apply_status IN ('.$filter['applyStatus'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-operationType
    public function testFormatFilterOperationType()
    {
        $filter = array(
            'operationType' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'operation_type = '.$filter['operationType'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatFilter-transformationCategory
    public function testFormatFilterTransformationCategory()
    {
        $filter = array(
            'transformationCategory' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'transformation_category IN ('.$filter['transformationCategory'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatFilter-sourceCategory
    public function testFormatFilterSourceCategory()
    {
        $filter = array(
            'sourceCategory' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'source_category IN ('.$filter['sourceCategory'].')';
        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-excludeTransformationCategory
    public function testFormatFilterExcludeTransformationCategory()
    {
        $filter = array(
            'excludeTransformationCategory' => 1
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'transformation_category <> '.$filter['excludeTransformationCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-sourceTemplateIds
    public function testFormatFilterSourceTemplateIds()
    {
        $filter = array(
            'sourceTemplateIds' => 2
        );
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = 'source_template_id IN ('.$filter['sourceTemplateIds'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $sortAdapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $sortAdapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--applyStatus
    public function testFormatSortStatusDesc()
    {
        $sort = array('applyStatus' => -1);
        $sortAdapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY apply_status DESC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-applyStatus
    public function testFormatSortStatusAsc()
    {
        $sort = array('applyStatus' => 1);
        $sortAdapter = new MockUnAuditedRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY apply_status ASC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use BaseData\ResourceCatalogData\Model\BaseSearchData;
use BaseData\ResourceCatalogData\Translator\Strategy\SearchDataDbTranslatorStrategyFactory;

class BaseSearchDataDbAdapter implements IBaseSearchDataAdapter
{
    private $translatorStrategyFactory;

    private $rowQueryFactory;

    private $adapterFactory;

    public function __construct()
    {
        $this->translatorStrategyFactory = new SearchDataDbTranslatorStrategyFactory();
        $this->rowQueryFactory = new RowQueryFactory();
        $this->adapterFactory = new AdapterFactory();
    }

    public function __destruct()
    {
        unset($this->translatorStrategyFactory);
        unset($this->rowQueryFactory);
        unset($this->adapterFactory);
    }
    
    protected function getTranslatorStrategyFactory() : SearchDataDbTranslatorStrategyFactory
    {
        return $this->translatorStrategyFactory;
    }

    protected function getDbTranslator(int $category)
    {
        return $this->getTranslatorStrategyFactory()->getStrategy($category);
    }
    
    protected function getRowQueryFactory() : RowQueryFactory
    {
        return $this->rowQueryFactory;
    }

    protected function getRowQuery(int $category)
    {
        return $this->getRowQueryFactory()->getRowQuery($category);
    }
    
    protected function getAdapterFactory() : AdapterFactory
    {
        return $this->adapterFactory;
    }

    protected function getAdapter(int $category)
    {
        return $this->getAdapterFactory()->getAdapter($category);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        $category = isset($filter['baseSubjectCategory']) ? $filter['baseSubjectCategory'] : 0;

        $adapter = $this->getAdapter($category);

        return $adapter->filter($filter, $sort, $offset, $size);
    }

    public function add(BaseSearchData $baseSearchData) : bool
    {
        $info = array();

        $category = $baseSearchData->getSubjectCategory();
        $info = $this->getDbTranslator($category)->objectToArray($baseSearchData);
        
        $id = $this->getRowQuery($category)->add($info);

        if (!$id) {
            return false;
        }

        $baseSearchData->setId($id);
        return true;
    }
}

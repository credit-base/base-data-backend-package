<?php
namespace BaseData\Template\Utils;

trait BjTemplateUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjTemplate
    ) {
        $this->assertEquals($expectedArray['bj_template_id'], $bjTemplate->getId());
        $this->assertEquals($expectedArray['name'], $bjTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $bjTemplate->getIdentify());

        $subjectCategory = is_string($expectedArray['subject_category']) ?
        json_decode($expectedArray['subject_category'], true) :
        $expectedArray['subject_category'];
        $this->assertEquals($subjectCategory, $bjTemplate->getSubjectCategory());

        $this->assertEquals($expectedArray['dimension'], $bjTemplate->getDimension());
        $this->assertEquals($expectedArray['exchange_frequency'], $bjTemplate->getExchangeFrequency());
        $this->assertEquals($expectedArray['info_classify'], $bjTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $bjTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['description'], $bjTemplate->getDescription());

        $items = is_string($expectedArray['items']) ?
        json_decode($expectedArray['items'], true) :
        $expectedArray['items'];
        $this->assertEquals($items, $bjTemplate->getItems());
        $this->assertEquals($expectedArray['source_unit_id'], $bjTemplate->getSourceUnit()->getId());
        $this->assertEquals($expectedArray['gb_template_id'], $bjTemplate->getGbTemplate()->getId());

        $this->assertEquals($expectedArray['status'], $bjTemplate->getStatus());
        $this->assertEquals($expectedArray['create_time'], $bjTemplate->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $bjTemplate->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $bjTemplate->getStatusTime());
    }
}

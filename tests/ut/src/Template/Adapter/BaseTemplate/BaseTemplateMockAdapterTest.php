<?php
namespace BaseData\Template\Adapter\BaseTemplate;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\BaseTemplate;

class BaseTemplateMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new BaseTemplateMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new BaseTemplate()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new BaseTemplate(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\BaseTemplate',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Template\Model\BaseTemplate',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Template\Model\BaseTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

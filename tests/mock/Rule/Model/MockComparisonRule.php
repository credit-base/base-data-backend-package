<?php
namespace BaseData\Rule\Model;

class MockComparisonRule extends ComparisonRule
{
    use MockConcretenessRuleTrait;

    public function validate() : bool
    {
        return parent::validate();
    }

    public function validateIdentify(string $transformationItemIdentify, array $base) : bool
    {
        return parent::validateIdentify($transformationItemIdentify, $base);
    }

    public function identifyAndBaseDuplicateWithCompanyName(string $transformationItemIdentify, array $base) : bool
    {
        return parent::identifyAndBaseDuplicateWithCompanyName($transformationItemIdentify, $base);
    }

    public function identifyAndBaseDuplicateWithIdentify(string $transformationItemIdentify, array $base) : bool
    {
        return parent::identifyAndBaseDuplicateWithIdentify($transformationItemIdentify, $base);
    }
}

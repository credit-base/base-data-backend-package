<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;

class RevokeParentTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(RevokeParentTaskCommandHandler::class)
                                     ->setMethods(['getParentTaskRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetParentTaskRepository()
    {
        $commandHandler = new MockRevokeParentTaskCommandHandler();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Repository\ParentTaskRepository',
            $commandHandler->getParentTaskRepository()
        );
    }

    public function testExecute()
    {
        $command = new RevokeParentTaskCommand(
            $this->faker->word,
            $this->faker->randomNumber()
        );

        $parentTask = $this->prophesize(ParentTask::class);
        $parentTask->revoke(Argument::exact($command->reason))->shouldBeCalledTimes(1)->willReturn(true);

        $parentTaskRepository = $this->prophesize(ParentTaskRepository::class);
        $parentTaskRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($parentTask->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getParentTaskRepository')
            ->willReturn($parentTaskRepository->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\CommandHandler\EnableCommandHandler;

class EnableWbjSearchDataCommandHandler extends EnableCommandHandler
{
    use WbjSearchDataCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchWbjSearchData($id);
    }
}

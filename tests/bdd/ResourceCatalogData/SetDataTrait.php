<?php
namespace BaseData\ResourceCatalogData;

use Marmot\Core;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

use BaseData\Common\Model\IEnableAble;

trait SetDataTrait
{
    private function items() : array
    {
        return
        [
            [
                "name" => '主体名称',    //信息项名称
                "identify" => 'ZTMC',    //数据标识
                "type" => 1,    //数据类型
                "length" => '200',    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则
                "remarks" => '信用主体名称',    //备注
            ],
            [
                "name" => '统一社会信用代码',    //信息项名称
                "identify" => 'TYSHXYDM',    //数据标识
                "type" => 1,    //数据类型
                "length" => '50',    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '信用主体代码',    //备注
            ],
        ];
    }

    protected function wbjTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '登记信息',
                'identify' => 'DJXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']]),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 1,
                'info_classify' => ISearchDataAble::INFO_CLASSIFY['XZXK'],
                'info_category' => ISearchDataAble::INFO_CATEGORY['SHOUXXX'],
                'description' =>'目录描述信息',
                'items' => json_encode($items),
                'source_unit_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function ysSearchData(int $status = IEnableAble::STATUS['ENABLED'])  : array
    {
        $data = array(
            'ZTMC' => '陕西传媒有限公司',
            'TYSHXYDM' => '912398452312309087',
        );

        $hash = md5(base64_encode(gzcompress(serialize($data))));

        return [
            'pcore_ys_search_data' =>
                [
                    [
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['XZXK'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['SHOUXXX'],
                        'wbj_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['SHGK'],
                        'name' => $data['ZTMC'],
                        'identify' => $data['TYSHXYDM'],
                        'expiration_date' => 4102329600,
                        'ys_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => $status,
                        'hash' => $hash,
                        'task_id' => 0
                    ]
                ],
            'pcore_ys_items_data' =>
                [
                    [
                        'data' => base64_encode(gzcompress(serialize($data)))
                    ]
                ],
        ];
    }

    protected function wbjSearchData(int $status = IEnableAble::STATUS['ENABLED'])  : array
    {
        $data = array(
            'ZTMC' => '中研社食品有限公司',
            'TYSHXYDM' => '91120104MA06FBJY36',
        );

        $hash = md5(base64_encode(gzcompress(serialize($data))));

        return [
            'pcore_wbj_search_data' =>
                [
                    [
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['XZXK'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['SHOUXXX'],
                        'wbj_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['ZWGX'],
                        'name' => $data['ZTMC'],
                        'identify' => $data['TYSHXYDM'],
                        'expiration_date' => 2147483647,
                        'wbj_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => $status,
                        'hash' => $hash,
                        'task_id' => 0
                    ]
                ],
            'pcore_wbj_items_data' =>
                [
                    [
                        'data' => base64_encode(gzcompress(serialize($data)))
                    ]
                ],
        ];
    }

    protected function bjTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '变更登记信息',
                'identify' => 'BGDJXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']]),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 1,
                'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                'info_category' => ISearchDataAble::INFO_CATEGORY['QTXX'],
                'description' =>'变更登记信息',
                'items' => json_encode($items),
                'source_unit_id' => 1,
                'gb_template_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function bjSearchData(int $status = BjSearchData::STATUS['ENABLED'])  : array
    {
        $data = array(
            'ZTMC' => '陕西大药房',
            'TYSHXYDM' => '91120104MA06FBJY39',
        );

        $hash = md5(base64_encode(gzcompress(serialize($data))));

        return [
            'pcore_bj_search_data' =>
                [
                    [
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['QTXX'],
                        'bj_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['SHGK'],
                        'name' => $data['ZTMC'],
                        'identify' => $data['TYSHXYDM'],
                        'expiration_date' => 2147483647,
                        'bj_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => $status,
                        'hash' => $hash,
                        'task_id' => 0,
                        'description' => '',
                        'front_end_processor_status' => BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
                    ]
                ],
            'pcore_bj_items_data' =>
                [
                    [
                        'data' => base64_encode(gzcompress(serialize($data)))
                    ]
                ],
        ];
    }

    protected function baseTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '企业基本信息',
                'identify' => 'QYJBXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 4,
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'企业基本信息',
                'items' => json_encode($items, true),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function baseSearchData(int $status = IEnableAble::STATUS['ENABLED'])  : array
    {
        $data = array(
            'ZTMC' => '企业名称',
            'TYSHXYDM' => '912908Q21903E74AX2',
        );

        $hash = md5(base64_encode(gzcompress(serialize($data))));

        return [
            'pcore_base_search_data' =>
                [
                    [
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['QTXX'],
                        'base_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['SHGK'],
                        'name' => $data['ZTMC'],
                        'identify' => $data['TYSHXYDM'],
                        'expiration_date' => 2147483647,
                        'base_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => $status,
                        'hash' => $hash,
                        'task_id' => 0
                    ]
                ],
            'pcore_base_items_data' =>
                [
                    [
                        'data' => base64_encode(gzcompress(serialize($data)))
                    ]
                ],
        ];
    }

    protected function gbTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '欠税信息',
                'identify' => 'QSXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']]),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 7,
                'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                'info_category' => ISearchDataAble::INFO_CATEGORY['QTXX'],
                'description' =>'欠税信息',
                'items' => json_encode($items),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function gbSearchData(int $status = GbSearchData::STATUS['ENABLED'])  : array
    {
        $data = array(
            'ZTMC' => '陕西出租公司',
            'TYSHXYDM' => '91220104MA06FBJY39',
        );

        $hash = md5(base64_encode(gzcompress(serialize($data))));

        return [
            'pcore_gb_search_data' =>
                [
                    [
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['QTXX'],
                        'gb_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['SHGK'],
                        'name' => $data['ZTMC'],
                        'identify' => $data['TYSHXYDM'],
                        'expiration_date' => 2147483647,
                        'gb_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => $status,
                        'hash' => $hash,
                        'task_id' => 0,
                        'description' => '',
                        'front_end_processor_status' => GbSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
                    ]
                ],
            'pcore_gb_items_data' =>
                [
                    [
                        'data' => base64_encode(gzcompress(serialize($data)))
                    ]
                ],
        ];
    }

    protected function task() : array
    {
        return
        [
            [
                'task_id' => 1,
                'crew_id' => 1,
                'user_group_id' => 1,
                'pid' => 0,
                'total' => 100,
                'success_number' => 60,
                'failure_number' => 40,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 1,
                'target_category' => Template::CATEGORY['WBJ'],
                'target_template_id' => 1,
                'target_rule_id' => 1,
                'schedule_task_id' => 1,
                'error_number' => 1,
                'file_name' => 'file_name',

                'status' => Task::STATUS['FAILURE'],
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function errorData()  : array
    {
        $data = array(
            'ZTMC' => '陕西出租公司',
            'TYSHXYDM' => '91220104MA06FBJY39',
        );

        return
        [
            [
                'error_data_id' => 1,
                'task_id' => 1,
                'category' => Template::CATEGORY['BJ'],
                'template_id' => 1,
                'items_data' => base64_encode(gzcompress(serialize($data))),
                'error_type' => ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED'],
                'error_reason' => json_encode(array('ZTMC'=>array(8))),
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'status' => ErrorData::STATUS['NORMAL']
            ]
        ];
    }
}

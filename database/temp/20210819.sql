

ALTER TABLE `pcore_apply_form_rule` ADD COLUMN `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)';
ALTER TABLE `pcore_apply_form_rule` change `type` `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)';

UPDATE `pcore_apply_form_rule` SET `source_category`=10;

ALTER TABLE `pcore_rule` ADD COLUMN `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)';
ALTER TABLE `pcore_rule` change `type` `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)';

UPDATE `pcore_rule` SET `source_category`=10;

ALTER TABLE `pcore_error_data` change `error_reason` `error_reason` json NOT NULL COMMENT '错误原因';
ALTER TABLE `pcore_error_data` change `status` `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, 1 程序异常, 2 入库异常)';

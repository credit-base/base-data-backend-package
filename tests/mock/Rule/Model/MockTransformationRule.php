<?php
namespace BaseData\Rule\Model;

class MockTransformationRule extends TransformationRule
{
    use MockConcretenessRuleTrait;
    
    public function validate() : bool
    {
        return parent::validate();
    }

    public function generateItemsData(
        array $sourceItemsData,
        array $transformationItemsData,
        array $condition
    ) : array {
        return parent::generateItemsData($sourceItemsData, $transformationItemsData, $condition);
    }
}

<?php
namespace BaseData\Template\CommandHandler\BjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseData\Template\Command\BjTemplate\EditBjTemplateCommand;
use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;

use BaseData\UserGroup\Model\UserGroup;

class EditBjTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo;

    private $repository;

    public function __construct()
    {
        $this->repository = new BjTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getBjTemplateRepository() : IBjTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditBjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getBjTemplateRepository();
        $bjTemplate = $repository->fetchOne($command->id);

        $bjTemplate->setName($command->name);
        $bjTemplate->setIdentify($command->identify);
        $bjTemplate->setSubjectCategory($command->subjectCategory);
        $bjTemplate->setDimension($command->dimension);
        $bjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $bjTemplate->setInfoClassify($command->infoClassify);
        $bjTemplate->setInfoCategory($command->infoCategory);
        $bjTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $bjTemplate->setItems($items);
        $bjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));
        $bjTemplate->setGbTemplate(new GbTemplate($command->gbTemplate));

        return $bjTemplate->edit();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Utils;

trait TaskUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $task
    ) {
        $this->assertEquals($expectedArray['task_id'], $task->getId());
        $this->assertEquals($expectedArray['crew_id'], $task->getCrew()->getId());
        $this->assertEquals($expectedArray['user_group_id'], $task->getUserGroup()->getId());
        $this->assertEquals($expectedArray['pid'], $task->getPid());
        $this->assertEquals($expectedArray['total'], $task->getTotal());
        $this->assertEquals($expectedArray['success_number'], $task->getSuccessNumber());
        $this->assertEquals($expectedArray['failure_number'], $task->getFailureNumber());
        $this->assertEquals($expectedArray['source_category'], $task->getSourceCategory());
        $this->assertEquals($expectedArray['source_template_id'], $task->getSourceTemplate());
        $this->assertEquals($expectedArray['target_category'], $task->getTargetCategory());
        $this->assertEquals($expectedArray['target_template_id'], $task->getTargetTemplate());
        $this->assertEquals($expectedArray['target_rule_id'], $task->getTargetRule());
        $this->assertEquals($expectedArray['schedule_task_id'], $task->getScheduleTask());
        $this->assertEquals($expectedArray['error_number'], $task->getErrorNumber());
        $this->assertEquals($expectedArray['file_name'], $task->getFileName());
        $this->assertEquals($expectedArray['status'], $task->getStatus());
        $this->assertEquals($expectedArray['create_time'], $task->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $task->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $task->getStatusTime());
    }
}

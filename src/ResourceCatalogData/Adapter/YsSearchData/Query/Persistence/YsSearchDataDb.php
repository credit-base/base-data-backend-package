<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class YsSearchDataDb extends Db
{
    const TABLE = 'ys_search_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

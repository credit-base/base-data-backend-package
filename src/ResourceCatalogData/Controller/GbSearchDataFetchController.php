<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\ResourceCatalogData\View\GbSearchDataView;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

class GbSearchDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IGbSearchDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new GbSearchDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'gbSearchData';
    }
}

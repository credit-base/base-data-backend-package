<?php
namespace BaseData\Enterprise;

use Marmot\Core;

use BaseData\Enterprise\Model\Enterprise;

trait SetDataTrait
{
    protected function enterprise() : array
    {
        return
        [
            [
                'enterprise_id' => 1,
                'name' => '企业名称',
                'unified_social_credit_code' => '912908Q21903E74AX2',
                'establishment_date' => 20210831,
                'approval_date' => 20210831,
                'address' => '陕西省西安市雁塔区长延堡街道',
                'registration_capital' => '1000',
                'business_term_start' => 20180831,
                'business_term_to' => 20210831,
                'business_scope' => '经营范围',
                'registration_authority' => '发展和改革委员会',
                'principal' => '张萍',
                'principal_card_id' => '412825199009094352',
                'registration_status' => '存续(在营、开业、在册)',
                'enterprise_type_code' => '100',
                'enterprise_type' => '内资企业',
                'data' => base64_encode(gzcompress(serialize(array('其他数据')))),
                'industry_category' => 'A',
                'industry_code' => '0111',
                'administrative_area' => 001,
                'hash' => md5(base64_encode(gzcompress(serialize(array('其他数据'))))),
                'task_id' => 1,
                'status' => Enterprise::STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}

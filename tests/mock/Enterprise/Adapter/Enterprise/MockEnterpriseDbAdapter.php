<?php
namespace BaseData\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\INull;

use BaseData\Enterprise\Translator\EnterpriseDbTranslator;
use BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery;

class MockEnterpriseDbAdapter extends EnterpriseDbAdapter
{
    public function getDbTranslator() : EnterpriseDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : EnterpriseRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

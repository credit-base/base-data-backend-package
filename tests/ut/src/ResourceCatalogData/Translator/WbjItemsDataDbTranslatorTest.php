<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\WbjItemsDataUtils;
use BaseData\ResourceCatalogData\Utils\WbjItemsDataMockFactory;

class WbjItemsDataDbTranslatorTest extends TestCase
{
    use WbjItemsDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjItemsDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullWbjItemsData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $wbjItemsData = WbjItemsDataMockFactory::generateWbjItemsData(1);

        $expression['wbj_items_data_id'] = $wbjItemsData->getId();

        $expression['data'] = base64_encode(gzcompress(serialize($wbjItemsData->getData())));

        $wbjItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\WbjItemsData', $wbjItemsData);
        $this->compareArrayAndObject($expression, $wbjItemsData);
    }

    public function testObjectToArray()
    {
        $wbjItemsData = WbjItemsDataMockFactory::generateWbjItemsData(1);

        $expression = $this->translator->objectToArray($wbjItemsData);

        $this->compareArrayAndObject($expression, $wbjItemsData);
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\ResourceCatalogData\Command\BjSearchData\DisableBjSearchDataCommand;

class DisableBjSearchDataCommandHandler implements ICommandHandler
{
    use BjSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof DisableBjSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }

        $bjSearchData = $this->fetchBjSearchData($command->id);

        return $bjSearchData->disable();
    }
}

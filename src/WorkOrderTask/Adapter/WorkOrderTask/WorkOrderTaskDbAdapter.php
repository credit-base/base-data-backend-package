<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Model\NullWorkOrderTask;
use BaseData\WorkOrderTask\Translator\WorkOrderTaskDbTranslator;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\WorkOrderTaskRowCacheQuery;

use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;

class WorkOrderTaskDbAdapter implements IWorkOrderTaskAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $gbTemplateRepository;
    private $bjTemplateRepository;
    private $parentTaskRepository;

    public function __construct()
    {
        $this->dbTranslator = new WorkOrderTaskDbTranslator();
        $this->rowCacheQuery = new WorkOrderTaskRowCacheQuery();
        $this->gbTemplateRepository = new GbTemplateRepository();
        $this->bjTemplateRepository = new BjTemplateRepository();
        $this->parentTaskRepository = new ParentTaskRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->gbTemplateRepository);
        unset($this->bjTemplateRepository);
        unset($this->parentTaskRepository);
    }
    
    protected function getDbTranslator() : WorkOrderTaskDbTranslator
    {
        return $this->dbTranslator;
    }
    
    protected function getRowQuery() : WorkOrderTaskRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getGbTemplateRepository() : GbTemplateRepository
    {
        return $this->gbTemplateRepository;
    }

    protected function getBjTemplateRepository() : BjTemplateRepository
    {
        return $this->bjTemplateRepository;
    }

    protected function getParentTaskRepository() : ParentTaskRepository
    {
        return $this->parentTaskRepository;
    }

    protected function getNullObject() : INull
    {
        return NullWorkOrderTask::getInstance();
    }

    public function add(WorkOrderTask $workOrderTask) : bool
    {
        return $this->addAction($workOrderTask);
    }

    public function edit(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->editAction($workOrderTask, $keys);
    }

    public function fetchOne($id) : WorkOrderTask
    {
        $workOrderTask = $this->fetchOneAction($id);
        if ($workOrderTask->getParentTask()->getTemplateType() == ParentTask::TEMPLATE_TYPE['GB']) {
            $this->fetchGbTemplateByObject($workOrderTask);
        }
        if ($workOrderTask->getParentTask()->getTemplateType() == ParentTask::TEMPLATE_TYPE['BJ']) {
            $this->fetchBjTemplateByObject($workOrderTask);
        }
        $this->fetchParentTaskByObject($workOrderTask);

        return $workOrderTask;
    }

    protected function fetchGbTemplateByObject(WorkOrderTask $workOrderTask)
    {
        $templateId = $workOrderTask->getTemplate()->getId();
        $template = $this->getGbTemplateRepository()->fetchOne($templateId);
        $workOrderTask->setTemplate($template);
    }

    protected function fetchBjTemplateByObject(WorkOrderTask $workOrderTask)
    {
        $templateId = $workOrderTask->getTemplate()->getId();
        $template = $this->getBjTemplateRepository()->fetchOne($templateId);
        $workOrderTask->setTemplate($template);
    }

    protected function fetchParentTaskByObject(WorkOrderTask $workOrderTask)
    {
        $parentTaskId = $workOrderTask->getParentTask()->getId();
        $parentTask = $this->getParentTaskRepository()->fetchOne($parentTaskId);
        $workOrderTask->setParentTask($parentTask);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $workOrderTask = new WorkOrderTask();

            if (isset($filter['title'])) {
                $workOrderTask->getParentTask()->setTitle($filter['title']);
                $info = $this->getDbTranslator()->objectToArray($workOrderTask, array('title'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['assignObject'])) {
                $workOrderTask->getAssignObject()->setId($filter['assignObject']);
                $info = $this->getDbTranslator()->objectToArray($workOrderTask, array('assignObject'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $info = $this->getDbTranslator()->objectToArray($workOrderTask, array('status'));
                $condition .= $conjection.key($info).' IN ('.$filter['status'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['parentTask'])) {
                $workOrderTask->getParentTask()->setId($filter['parentTask']);
                $info = $this->getDbTranslator()->objectToArray($workOrderTask, array('parentTask'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new WorkOrderTask(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['endTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new WorkOrderTask(), array('endTime'));
                $condition .= $conjection.key($info).' '.($sort['endTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

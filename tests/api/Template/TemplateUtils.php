<?php
namespace BaseData\Template;

use BaseData\Template\Model\Template;

trait TemplateUtils
{
    private function wbjTemplateCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->templateCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['wbj_template_id'], $responseDataArray['id']);
        $this->assertEquals('templates', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals(Template::CATEGORY['WBJ'], $attributes['category']);

        $relationshipsUserGroup = $responseDataArray['relationships']['sourceUnit']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['source_unit_id'], $relationshipsUserGroup['id']);
    }

    private function bjTemplateCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->templateCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['bj_template_id'], $responseDataArray['id']);
        $this->assertEquals('bjTemplates', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals(Template::CATEGORY['BJ'], $attributes['category']);

        $relationshipsUserGroup = $responseDataArray['relationships']['sourceUnit']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['source_unit_id'], $relationshipsUserGroup['id']);

        $relationshipsUserGroup = $responseDataArray['relationships']['gbTemplate']['data'];
        $this->assertEquals('gbTemplates', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['gb_template_id'], $relationshipsUserGroup['id']);
    }

    private function gbTemplateCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->templateCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['gb_template_id'], $responseDataArray['id']);
        $this->assertEquals('gbTemplates', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals(Template::CATEGORY['GB'], $attributes['category']);
    }

    private function baseTemplateCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->templateCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['base_template_id'], $responseDataArray['id']);
        $this->assertEquals('baseTemplates', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals(Template::CATEGORY['BASE'], $attributes['category']);
    }

    private function qzjTemplateCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->templateCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['qzj_template_id'], $responseDataArray['id']);
        $this->assertEquals('qzjTemplates', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['category'], $attributes['category']);

        $relationshipsUserGroup = $responseDataArray['relationships']['sourceUnit']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['source_unit_id'], $relationshipsUserGroup['id']);
    }

    private function templateCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $attributes = $responseDataArray['attributes'];
        
        $this->assertEquals($setDataArray['name'], $attributes['name']);
        $this->assertEquals($setDataArray['identify'], $attributes['identify']);
        $this->assertEquals(json_decode($setDataArray['subject_category'], true), $attributes['subjectCategory']);
        $this->assertEquals($setDataArray['dimension'], $attributes['dimension']);
        $this->assertEquals($setDataArray['exchange_frequency'], $attributes['exchangeFrequency']);
        $this->assertEquals($setDataArray['info_classify'], $attributes['infoClassify']);
        $this->assertEquals($setDataArray['info_category'], $attributes['infoCategory']);
        $this->assertEquals($setDataArray['description'], $attributes['description']);
        $this->assertEquals(json_decode($setDataArray['items'], true), $attributes['items']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);
    }
}

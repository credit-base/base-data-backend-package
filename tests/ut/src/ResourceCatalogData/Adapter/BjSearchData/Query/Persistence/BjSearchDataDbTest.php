<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class BjSearchDataDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockBjSearchDataDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(BjSearchDataDb::TABLE, $this->database->getTable());
    }
}

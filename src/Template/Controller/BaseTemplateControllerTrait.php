<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\CommandHandler\BaseTemplate\BaseTemplateCommandHandlerFactory;

trait BaseTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }
    
    protected function getRepository() : BaseTemplateRepository
    {
        return new BaseTemplateRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new BaseTemplateCommandHandlerFactory());
    }

    protected function validateOperateScenario(
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $items
    ) : bool {
        return $this->getTemplateWidgetRule()->subjectCategory($subjectCategory)
        && $this->getTemplateWidgetRule()->dimension($dimension)
        && $this->getCommonWidgetRule()->formatNumeric($exchangeFrequency, 'exchangeFrequency')
        && $this->getCommonWidgetRule()->formatNumeric($infoClassify, 'infoClassify')
        && $this->getCommonWidgetRule()->formatNumeric($infoCategory, 'infoCategory')
        && $this->getTemplateWidgetRule()->items($subjectCategory, $items);
    }
}

<?php
namespace BaseData\Rule\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;
use BaseData\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceDbAdapter;
use BaseData\Rule\Adapter\UnAuditedRuleService\UnAuditedRuleServiceMockAdapter;

class UnAuditedRuleServiceRepository extends Repository implements IUnAuditedRuleServiceAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new UnAuditedRuleServiceDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IUnAuditedRuleServiceAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IUnAuditedRuleServiceAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IUnAuditedRuleServiceAdapter
    {
        return new UnAuditedRuleServiceMockAdapter();
    }

    public function fetchOne($id) : UnAuditedRuleService
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(UnAuditedRuleService $unAuditedRuleService) : bool
    {
        return $this->getAdapter()->add($unAuditedRuleService);
    }

    public function edit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($unAuditedRuleService, $keys);
    }
}

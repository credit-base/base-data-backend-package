<?php
namespace BaseData\Crew\Utils;

use BaseData\Crew\Model\Crew;

class MockFactory
{
    public static function generateCrew(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Crew {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $crew = new Crew($id);
        $crew->setId($id);

        //cellphone
        self::generateCellphone($crew, $faker, $value);
        //realName
        self::generateRealName($crew, $faker, $value);
        //userGroup
        self::generateUserGroup($crew, $faker, $value);

        $crew->setStatus(0);
        $crew->setCreateTime($faker->unixTime());
        $crew->setUpdateTime($faker->unixTime());
        $crew->setStatusTime($faker->unixTime());

        return $crew;
    }

    private static function generateCellphone($crew, $faker, $value) : void
    {
        $cellphone = isset($value['cellphone']) ?
            $value['cellphone'] :
            $faker->phoneNumber();
        
        $crew->setCellphone($cellphone);
    }

    private static function generateRealName($crew, $faker, $value) : void
    {
        $realName = isset($value['realName']) ?
            $value['realName'] :
            $faker->name();
        
        $crew->setRealName($realName);
    }

    private static function generateUserGroup($crew, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
            $value['userGroup'] :
            \BaseData\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
        $crew->setUserGroup($userGroup);
    }
}

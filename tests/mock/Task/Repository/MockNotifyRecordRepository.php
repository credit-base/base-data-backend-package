<?php
namespace BaseData\Task\Repository;

use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;

class MockNotifyRecordRepository extends NotifyRecordRepository
{
    public function getAdapter() : INotifyRecordAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : INotifyRecordAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : INotifyRecordAdapter
    {
        return parent::getMockAdapter();
    }
}

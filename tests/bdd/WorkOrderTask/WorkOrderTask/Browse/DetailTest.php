<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Translator\WorkOrderTaskDbTranslator;

use BaseData\Template\SetDataTrait as TemplateSetDataTrait;

/**
 * @Feature: 我是委办局领导,我登录的账号可以管理指派给我的任务,在政务网OA中,我可以管理给我指派的工单任务,
 *            通过列表形式呈现,以便于我可以把我的数据通过发改委指派的资源目录上报上去
 * @Scenario: 查看工单任务详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, TemplateSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_parent_task');
        $this->clear('pcore_work_order_task');
    }

    /**
     * @Given: 存在一条工单任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_parent_task' => $this->parentTask(),
            'pcore_work_order_task' => $this->workOrderTask()
        ]);
    }

    /**
     * @When: 当我查看该条工单任务详情时
     */
    public function fetchWorkOrderTask($id)
    {
        $repository = new WorkOrderTaskRepository();

        $workOrderTask = $repository->fetchOne($id);

        return $workOrderTask;
    }

    /**
     * @Then: 我可以看见该条工单任务的全部信息
     */
    public function testViewWorkOrderTaskList()
    {
        $id = 1;
        
        $setWorkOrderTask = $this->workOrderTask()[0];
        $setWorkOrderTask['work_order_task_id'] = $id;
        $setWorkOrderTask['feedback_records'] = json_decode($setWorkOrderTask['feedback_records'], true);

        $workOrderTask = $this->fetchWorkOrderTask($id);
        $translator = new WorkOrderTaskDbTranslator();
        $workOrderTaskArray = $translator->objectToArray($workOrderTask);

        $this->assertEquals($workOrderTaskArray, $setWorkOrderTask);
    }
}

<?php
namespace BaseData\WorkOrderTask\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use BaseData\Template\Model\Template;

use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class ParentTask implements IObject
{
    use Object;

    /**
     * @var TEMPLATE_TYPE['GB']  国标目录 1
     * @var TEMPLATE_TYPE['BJ']  本级目录 2
     */
    const TEMPLATE_TYPE = array(
        'GB' => 1,
        'BJ' => 2
    );

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var string $title 任务标题
     */
    protected $title;
    /**
     * @var string $description 任务描述
     */
    protected $description;
    /**
     * @var string $endTime 终结时间
     */
    protected $endTime;
    /**
     * @var array $attachment 依据附件
     */
    protected $attachment;
    /**
     * @var int $templateType 基础目录
     */
    protected $templateType;
    /**
     * @var Template $template 指派目录
     */
    protected $template;
    /**
     * @var array $assignObjects 指派对象
     */
    protected $assignObjects;
    /**
     * @var int $finishCount 已完结总数，任务状态为已撤销、已确认、已终结时，算作已完结
     */
    protected $finishCount;
    /**
     * @var string $reason 撤销原因
     */
    protected $reason;
    protected $repository;
    protected $workOrderTaskRepository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->title = '';
        $this->description = '';
        $this->endTime = '0000-00-00';
        $this->attachment = array();
        $this->templateType = 0;
        $this->template = new Template();
        $this->assignObjects = array();
        $this->finishCount = 0;
        $this->reason = '';

        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new ParentTaskRepository();
        $this->workOrderTaskRepository = new WorkOrderTaskRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->description);
        unset($this->endTime);
        unset($this->attachment);
        unset($this->templateType);
        unset($this->template);
        unset($this->assignObjects);
        unset($this->finishCount);
        unset($this->reason);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
        unset($this->workOrderTaskRepository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setEndTime(string $endTime) : void
    {
        $this->endTime = $endTime;
    }

    public function getEndTime() : string
    {
        return $this->endTime;
    }

    public function setAttachment(array $attachment) : void
    {
        $this->attachment = $attachment;
    }

    public function getAttachment() : array
    {
        return $this->attachment;
    }

    public function setTemplateType(int $templateType) : void
    {
        $this->templateType = in_array($templateType, self::TEMPLATE_TYPE) ? $templateType : 0;
    }

    public function getTemplateType() : int
    {
        return $this->templateType;
    }

    public function setTemplate(Template $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function setAssignObjects(array $assignObjects) : void
    {
        $this->assignObjects = $assignObjects;
    }

    public function getAssignObjects() : array
    {
        return $this->assignObjects;
    }

    public function setFinishCount(int $finishCount) : void
    {
        $this->finishCount = $finishCount;
    }

    public function getFinishCount() : int
    {
        return $this->finishCount;
    }

    public function setReason(string $reason) : void
    {
        $this->reason = $reason;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : IParentTaskAdapter
    {
        return $this->repository;
    }

    protected function getWorkOrderTask() : WorkOrderTask
    {
        return new WorkOrderTask();
    }

    protected function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return $this->workOrderTaskRepository;
    }

    public function add() : bool
    {
        //1、添加父任务 && 2、绑定子任务
        return $this->addAction() && $this->bindTask();
    }

    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function bindTask() : bool
    {
        foreach ($this->getAssignObjects() as $assignObject) {
            $workOrderTask = $this->getWorkOrderTask();
            $workOrderTask->setParentTask($this);
            $workOrderTask->setTemplate($this->getTemplate());
            $workOrderTask->setAssignObject($assignObject);
            $this->getWorkOrderTaskRepository()->add($workOrderTask);
        }
        return true;
    }

    public function revoke(string $reason) : bool
    {
        // 撤销父任务【当所有子任务状态均为待确认时，可撤销以下全部子任务】
        //1、撤销子任务 && 2、撤销父任务
        return  $this->revokeWorkOrderTask($reason) && $this->revokeAction($reason);
    }

    protected function revokeAction(string $reason) : bool
    {
        // 撤销【当所有子任务状态均为待确认时，可撤销】；说明撤销原因
        if ($this->getFinishCount()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        $this->setReason($reason);
        $this->setFinishCount(count($this->getAssignObjects()));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'finishCount',
                'reason',
                'updateTime'
            )
        );
    }

    protected function revokeWorkOrderTask(string $reason) : bool
    {
        if ($this->fetchWorkOrderTask()) {
            foreach ($this->fetchWorkOrderTask() as $workOrderTask) {
                $workOrderTask->revoke($reason);
            }
        }
        return true;
    }

    protected function fetchWorkOrderTask() : array
    {
        $filter['parentTask'] = $this->getId();
        list($workOrderTaskList, $count) = $this->getWorkOrderTaskRepository()->filter($filter);
        return $count ? $workOrderTaskList : array();
    }
    
    public function updateFinishCount() : bool
    {
        $finishCount = $this->getFinishCount();
        $this->setFinishCount(intval($finishCount+1));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'finishCount',
                'updateTime'
            )
        );
    }
}

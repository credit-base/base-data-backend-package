<?php
namespace BaseData\UserGroup\Adapter\UserGroup;

use BaseData\UserGroup\Model\UserGroup;

interface IUserGroupAdapter
{
    public function fetchOne(int $id) : UserGroup;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}

<?php
namespace BaseData\Template\CommandHandler\BjTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;
use BaseData\Template\Command\BjTemplate\AddBjTemplateCommand;
use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\GbTemplate;
use BaseData\UserGroup\Model\UserGroup;

class AddBjTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo;
    
    protected function getBjTemplate() : BjTemplate
    {
        return new BjTemplate();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddBjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $bjTemplate = $this->getBjTemplate();

        $bjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));
        $bjTemplate->setGbTemplate(new GbTemplate($command->gbTemplate));
        $bjTemplate->setName($command->name);
        $bjTemplate->setIdentify($command->identify);
        $bjTemplate->setSubjectCategory($command->subjectCategory);
        $bjTemplate->setDimension($command->dimension);
        $bjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $bjTemplate->setInfoClassify($command->infoClassify);
        $bjTemplate->setInfoCategory($command->infoCategory);
        $bjTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $bjTemplate->setItems($items);

        if ($bjTemplate->add()) {
            $command->id = $bjTemplate->getId();
            return true;
        }
        return false;
    }
}

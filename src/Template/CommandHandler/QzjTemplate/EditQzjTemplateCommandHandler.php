<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;
use BaseData\Template\Command\QzjTemplate\EditQzjTemplateCommand;

class EditQzjTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo, QzjTemplateCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof EditQzjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $sourceUnit = $this->fetchUserGroup($command->sourceUnit);

        $qzjTemplate = $this->fetchQzjTemplate($command->id);

        $qzjTemplate->setName($command->name);
        $qzjTemplate->setIdentify($command->identify);
        $qzjTemplate->setCategory($command->category);
        $qzjTemplate->setSourceUnit($sourceUnit);
        $qzjTemplate->setSubjectCategory($command->subjectCategory);
        $qzjTemplate->setDimension($command->dimension);
        $qzjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $qzjTemplate->setInfoClassify($command->infoClassify);
        $qzjTemplate->setInfoCategory($command->infoCategory);
        $qzjTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $qzjTemplate->setItems($items);

        return $qzjTemplate->edit();
    }
}

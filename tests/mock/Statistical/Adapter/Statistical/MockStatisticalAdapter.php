<?php
namespace BaseData\Statistical\Adapter\Statistical;

use BaseData\Statistical\Model\Statistical;

class MockStatisticalAdapter implements IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        unset($filter);
        return Statistical();
    }
}

<?php
namespace BaseData\WorkOrderTask\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

use BaseData\Template\Model\WbjTemplate;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WorkOrderTaskTest extends TestCase
{
    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = new WorkOrderTask();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->workOrderTask);
    }

    public function testImplementsIObjec()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->workOrderTask
        );
    }

    public function testGetRepository()
    {
        $workOrderTask = new MockWorkOrderTask();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $workOrderTask->getRepository()
        );
    }

    public function testGetWbjTemplate()
    {
        $workOrderTask = new MockWorkOrderTask();
        $this->assertInstanceOf(
            'BaseData\Template\Model\WbjTemplate',
            $workOrderTask->getWbjTemplate()
        );
    }

    public function testRevokeFail()
    {
        $reason = 'reason';
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->revoke($reason);
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testRevokeSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods([
                               'getRepository',
                               'getParentTask'
                            ])->getMock();

        $reason = 'reason';
        $workOrderTask->setStatus(WorkOrderTask::STATUS['DQR']);
                           
        $parentTask = $this->prophesize(ParentTask::class);
        $parentTask->updateFinishCount()->shouldBeCalledTimes(1)->willReturn(true);
        $workOrderTask->expects($this->exactly(1))->method('getParentTask')->willReturn($parentTask->reveal());

        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->edit(
            Argument::exact($workOrderTask),
            Argument::exact(
                [
                    'status',
                    'reason',
                    'statusTime',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $workOrderTask->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTask->revoke($reason);
        $this->assertTrue($result);
    }

    public function testConfirmSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods([
                               'setUpdateTime',
                               'getRepository',
                               'isTemplateExist',
                               'getParentTask',
                               'addWbjTemplate',
                               'getItems'
                            ])->getMock();
                           
        $workOrderTask->setStatus(WorkOrderTask::STATUS['DQR']);
        
        $workOrderTask->expects($this->exactly(1))->method('isTemplateExist')->willReturn(false);
        $workOrderTask->expects($this->exactly(1))->method('getItems')->willReturn(array('items'));
        $workOrderTask->expects($this->exactly(1))->method('addWbjTemplate')->with(array('items'))->willReturn(false);

        $parentTask = $this->prophesize(ParentTask::class);
        $parentTask->updateFinishCount()->shouldBeCalledTimes(1)->willReturn(true);
        $workOrderTask->expects($this->exactly(1))->method('getParentTask')->willReturn($parentTask->reveal());

        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->edit(
            Argument::exact($workOrderTask),
            Argument::exact(
                [
                    'status',
                    'statusTime',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $workOrderTask->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTask->confirm();
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->confirm();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testEndFail()
    {
        $reason = 'reason';
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->end($reason);
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testEndSuccess()
    {
        //初始化
        $workOrderTaskEnd = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods([
                               'getRepository',
                               'getParentTask'
                            ])->getMock();

        $reason = 'reason';
        $workOrderTaskEnd->setStatus(WorkOrderTask::STATUS['GJZ']);
                           
        $parentTask = $this->prophesize(ParentTask::class);
        $parentTask->updateFinishCount()->shouldBeCalledTimes(1)->willReturn(true);
        $workOrderTaskEnd->expects($this->exactly(1))->method('getParentTask')->willReturn($parentTask->reveal());

        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->edit(
            Argument::exact($workOrderTaskEnd),
            Argument::exact(
                [
                    'status',
                    'reason',
                    'statusTime',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $workOrderTaskEnd->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTaskEnd->end($reason);
        $this->assertTrue($result);
    }

    public function testFeedbackSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(WorkOrderTask::class)
                           ->setMethods(['setStatus', 'setStatusTime', 'setUpdateTime', 'getRepository'])
                           ->getMock();

        $workOrderTask->expects($this->exactly(1))
             ->method('setStatus')
             ->with(WorkOrderTask::STATUS['GJZ']);
        $workOrderTask->expects($this->exactly(1))
             ->method('setStatusTime')
             ->with(Core::$container->get('time'));
        $workOrderTask->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IWorkOrderTaskAdapter
        $repository = $this->prophesize(IWorkOrderTaskAdapter::class);
        $repository->edit(
            Argument::exact($workOrderTask),
            Argument::exact(
                [
                    'status',
                    'feedbackRecords',
                    'statusTime',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $workOrderTask->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $workOrderTask->feedback();
        $this->assertTrue($result);
    }

    public function testFeedbackFail()
    {
        //初始化
        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setStatus(WorkOrderTask::STATUS['YZJ']);
        
        //验证
        $result = $workOrderTask->feedback();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testIsDqr()
    {
        $workOrderTask = new MockWorkOrderTask();

        $workOrderTask->setStatus(WorkOrderTask::STATUS['DQR']);

        $result = $workOrderTask->isDqr();

        $this->assertTrue($result);
    }

    public function testIsGjz()
    {
        $workOrderTask = new MockWorkOrderTask();

        $workOrderTask->setStatus(WorkOrderTask::STATUS['GJZ']);

        $result = $workOrderTask->isGjz();

        $this->assertTrue($result);
    }

    public function testIsTemplateExistFail()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(MockWorkOrderTask::class)
                           ->setMethods([
                               'getFeedbackRecords',
                            ])->getMock();

        $workOrderTask->expects($this->exactly(1))->method('getFeedbackRecords')->willReturn(array());

        //验证
        $result = $workOrderTask->isTemplateExist();
        $this->assertFalse($result);
    }

    public function testIsTemplateExistSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(MockWorkOrderTask::class)
                           ->setMethods([
                               'getFeedbackRecords',
                            ])->getMock();

        $feedbackRecords = array(
            array('isExistedTemplate'=> WorkOrderTask::IS_EXISTED_TEMPLATE['SHI'])
        );
        $workOrderTask->expects($this->exactly(1))->method('getFeedbackRecords')->willReturn($feedbackRecords);

        //验证
        $result = $workOrderTask->isTemplateExist();
        $this->assertTrue($result);
    }

    public function testGetItemsFail()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(MockWorkOrderTask::class)
                           ->setMethods([
                               'getFeedbackRecords',
                            ])->getMock();

        $workOrderTask->expects($this->exactly(1))->method('getFeedbackRecords')->willReturn(array());

        //验证
        $result = $workOrderTask->getItems();
        $this->assertEquals(array(), $result);
    }

    public function testGetItemsSuccess()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(MockWorkOrderTask::class)
                           ->setMethods([
                               'getFeedbackRecords',
                            ])->getMock();

        $items = array('items');
        $feedbackRecords = array(
            array('items'=> $items)
        );
        $workOrderTask->expects($this->exactly(1))->method('getFeedbackRecords')->willReturn($feedbackRecords);

        //验证
        $result = $workOrderTask->getItems();
        $this->assertEquals($items, $result);
    }

    public function testAddWbjTemplate()
    {
        //初始化
        $workOrderTask = $this->getMockBuilder(MockWorkOrderTask::class)
                           ->setMethods([
                               'getWbjTemplate',
                               'getTemplate',
                               'getAssignObject'
                            ])->getMock();

        $items = array('items');
        $template = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $userGroup = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $workOrderTask->expects($this->exactly(1))->method('getTemplate')->willReturn($template);
        $workOrderTask->expects($this->exactly(1))->method('getAssignObject')->willReturn($userGroup);

        $wbjTemplate = $this->prophesize(WbjTemplate::class);
        $wbjTemplate->setName(Argument::exact($template->getName()))->shouldBeCalledTimes(1);
        $wbjTemplate->setIdentify(Argument::exact($template->getIdentify()))->shouldBeCalledTimes(1);
        $wbjTemplate->setSubjectCategory(Argument::exact($template->getSubjectCategory()))->shouldBeCalledTimes(1);
        $wbjTemplate->setDimension(Argument::exact($template->getDimension()))->shouldBeCalledTimes(1);
        $wbjTemplate->setExchangeFrequency(Argument::exact($template->getExchangeFrequency()))->shouldBeCalledTimes(1);
        $wbjTemplate->setInfoClassify(Argument::exact($template->getInfoClassify()))->shouldBeCalledTimes(1);
        $wbjTemplate->setInfoCategory(Argument::exact($template->getInfoCategory()))->shouldBeCalledTimes(1);
        $wbjTemplate->setDescription(Argument::exact($template->getDescription()))->shouldBeCalledTimes(1);
        $wbjTemplate->setSourceUnit(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        $wbjTemplate->setItems(Argument::exact($items))->shouldBeCalledTimes(1);
        $wbjTemplate->add()->shouldBeCalledTimes(1)->willReturn(true);

        $workOrderTask->expects($this->once())->method('getWbjTemplate')->willReturn($wbjTemplate->reveal());
        //验证
        $result = $workOrderTask->addWbjTemplate($items);
        $this->assertTrue($result);
    }
}

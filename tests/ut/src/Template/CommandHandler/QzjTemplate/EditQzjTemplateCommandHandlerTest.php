<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Command\QzjTemplate\EditQzjTemplateCommand;

use BaseData\UserGroup\Model\UserGroup;

class EditQzjTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditQzjTemplateCommandHandler::class)
                                     ->setMethods(['fetchQzjTemplate', 'fetchUserGroup'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $this->initQzjTemplate($command, true);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $this->initQzjTemplate($command, false);

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new EditQzjTemplateCommand(
            $this->faker->word(),
            $this->faker->regexify('[A-Z_]{1,100}'),
            $this->faker->randomElements(Template::SUBJECT_CATEGORY, 1),
            $this->faker->randomElement(Template::DIMENSION),
            $this->faker->randomElement(Template::EXCHANGE_FREQUENCY),
            $this->faker->randomElement(Template::INFO_CLASSIFY),
            $this->faker->randomElement(Template::INFO_CATEGORY),
            $this->faker->sentence(),
            $this->faker->randomElements(
                array(
                    array(
                        "name" => '主体标识',    //信息项名称
                        "identify" => 'ZTBS',    //数据标识
                        "type" => Template::TYPE['ZFX'],    //数据类型
                        "length" => '200',    //数据长度
                    ),
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => Template::TYPE['ZFX'],    //数据类型
                        "length" => '50',    //数据长度
                    ),
                    array(
                        "name" => '日期',    //信息项名称
                        "identify" => 'RQ',    //数据标识
                        "type" => Template::TYPE['RQX'],    //数据类型
                        "length" => '8',    //数据长度
                    )
                ),
                2
            ),
            $this->faker->randomDigit(),
            $this->faker->randomElement(QzjTemplate::QZJ_TEMPLATE_CATEGORY),
            $this->faker->randomDigit()
        );
        return $command;
    }

    private function initQzjTemplate(ICommand $command, bool $result)
    {
        $sourceUnit = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup($command->sourceUnit);
        $this->commandHandler->expects($this->exactly(1))
            ->method('fetchUserGroup')
            ->willReturn($sourceUnit);

        $qzjTemplate = $this->prophesize(QzjTemplate::class);
        $qzjTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $qzjTemplate->setCategory(Argument::exact($command->category))->shouldBeCalledTimes(1);
        $qzjTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $qzjTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $qzjTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $qzjTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $qzjTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $qzjTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);

        $items = $this->commandHandler->setDefaultInfo($command);
        $qzjTemplate->setItems(Argument::exact($items))->shouldBeCalledTimes(1);
        $qzjTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $qzjTemplate->setSourceUnit($sourceUnit)->shouldBeCalledTimes(1);
        $qzjTemplate->edit()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->exactly(1))
            ->method('fetchQzjTemplate')
            ->willReturn($qzjTemplate->reveal());
    }
}

<?php
namespace BaseData\Common\Model;

use Marmot\Core;

trait ApproveAbleTrait
{
    protected $applyStatus;

    protected $rejectReason;

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = in_array($applyStatus, IApproveAble::APPLY_STATUS)
        ? $applyStatus
        : IApproveAble::APPLY_STATUS['PENDING'];
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }

    public function approve() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->approveAction();
    }
    
    abstract protected function approveAction() : bool;

    public function reject() : bool
    {
        if (!$this->isPending()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->rejectAction();
    }

    abstract protected function rejectAction() : bool;

    public function isPending() : bool
    {
        return $this->getApplyStatus() == self::APPLY_STATUS['PENDING'];
    }

    public function isApprove() : bool
    {
        return $this->getApplyStatus() == self::APPLY_STATUS['APPROVE'];
    }

    public function isReject() : bool
    {
        return $this->getApplyStatus() == self::APPLY_STATUS['REJECT'];
    }
}

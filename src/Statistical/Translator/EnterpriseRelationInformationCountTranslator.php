<?php
namespace BaseData\Statistical\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Statistical\Model\Statistical;

class EnterpriseRelationInformationCountTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $statistical = null)
    {

        if ($statistical == null) {
            $statistical = new Statistical();
        }

        $result = array(
            'licensingTotal' => $expression['licensing_total'],
            'penaltyTotal' => $expression['penalty_total'],
            'incentiveTotal' => $expression['incentive_total'],
            'punishTotal' => $expression['punish_total']
        );
        $statistical->setResult($result);

        return $statistical;
    }

    public function objectToArray($statistical, array $keys = array())
    {
        unset($statistical);
        unset($keys);

        return array();
    }
}

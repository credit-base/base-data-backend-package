<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\YsItemsDataUtils;
use BaseData\ResourceCatalogData\Utils\YsItemsDataMockFactory;

class YsItemsDataDbTranslatorTest extends TestCase
{
    use YsItemsDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new YsItemsDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullYsItemsData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $ysItemsData = YsItemsDataMockFactory::generateYsItemsData(1);

        $expression['ys_items_data_id'] = $ysItemsData->getId();

        $expression['data'] = base64_encode(gzcompress(serialize($ysItemsData->getData())));

        $ysItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\YsItemsData', $ysItemsData);
        $this->compareArrayAndObject($expression, $ysItemsData);
    }

    public function testObjectToArray()
    {
        $ysItemsData = YsItemsDataMockFactory::generateYsItemsData(1);

        $expression = $this->translator->objectToArray($ysItemsData);

        $this->compareArrayAndObject($expression, $ysItemsData);
    }
}

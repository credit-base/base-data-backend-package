<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Common\Model\NullOperateTrait;

trait NullSearchDataTrait
{
    use NullOperateTrait;
    
    public function isCrewNull() : bool
    {
        return $this->resourceNotExist();
    }

    public function isSourceUnitNull() : bool
    {
        return $this->resourceNotExist();
    }

    public function isTemplateNull() : bool
    {
        return $this->resourceNotExist();
    }

    public function isHashExist() : bool
    {
        return $this->resourceNotExist();
    }

    public function isSubjectCategoryExist() : bool
    {
        return $this->resourceNotExist();
    }

    public function validate() : bool
    {
        return $this->resourceNotExist();
    }

    public function assignment()
    {
        return $this->resourceNotExist();
    }

    public function addItemsData() : bool
    {
        return $this->resourceNotExist();
    }
}

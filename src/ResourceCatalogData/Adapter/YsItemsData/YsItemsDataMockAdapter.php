<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData;

use BaseData\ResourceCatalogData\Model\YsItemsData;
use BaseData\ResourceCatalogData\Utils\YsItemsDataMockFactory;

class YsItemsDataMockAdapter implements IYsItemsDataAdapter
{
    public function fetchOne($id) : YsItemsData
    {
        return YsItemsDataMockFactory::generateYsItemsData($id);
    }

    public function fetchList(array $ids) : array
    {
        $ysItemsDataList = array();

        foreach ($ids as $id) {
            $ysItemsDataList[$id] = YsItemsDataMockFactory::generateYsItemsData($id);
        }

        return $ysItemsDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(YsItemsData $ysItemsData) : bool
    {
        unset($ysItemsData);
        return true;
    }
}

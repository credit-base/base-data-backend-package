<?php
namespace BaseData\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class GbItemsDataSchema extends SchemaProvider
{
    protected $resourceType = 'gbItemsData';

    public function getId($gbItemsData) : int
    {
        return $gbItemsData->getId();
    }

    public function getAttributes($gbItemsData) : array
    {
        return [
            'data' => $gbItemsData->getData()
        ];
    }
}

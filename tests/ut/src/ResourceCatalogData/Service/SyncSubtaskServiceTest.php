<?php
namespace BaseData\ResourceCatalogData\Service;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\NullRuleService;
use BaseData\Rule\Repository\SearchDataRepositoryFactory;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class SyncSubtaskServiceTest extends TestCase
{
    private $stub;

    private $ruleService;

    private $task;

    public function setUp()
    {
        $this->stub = new MockSyncSubtaskService(new RuleService(), new Task());

        $this->ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService(1);
        $this->task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->stub);
        unset($this->ruleService);
        unset($this->task);
    }

    public function testGetRuleService()
    {
        $this->assertInstanceof(
            'BaseData\Rule\Model\RuleService',
            $this->stub->getRuleService()
        );
    }

    public function testGetTask()
    {
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\Task',
            $this->stub->getTask()
        );
    }

    public function testGetSubtask()
    {
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\Task',
            $this->stub->getSubtask()
        );
    }

    public function testExecuteValidateDataCorrect()
    {
        $stub = $this->getMockBuilder(MockSyncSubtaskService::class)
                        ->setConstructorArgs(array(new RuleService(), new Task()))
                        ->setMethods(['validateDataCorrect'])
                        ->getMock();

        $stub->expects($this->once())
            ->method('validateDataCorrect')
            ->willReturn(false);

        $result = $stub->execute();

        $this->assertEquals(array(array(), new NullTask()), $result);
    }

    public function testExecuteCountEmpty()
    {
        $stub = $this->getMockBuilder(MockSyncSubtaskService::class)
                        ->setConstructorArgs(array(new RuleService(), new Task()))
                        ->setMethods(['validateDataCorrect', 'fetchSourceSearchData'])
                        ->getMock();

        $stub->expects($this->once())
            ->method('validateDataCorrect')
            ->willReturn(true);

        $stub->expects($this->once())
            ->method('fetchSourceSearchData')
            ->willReturn([array(), 0]);

        $result = $stub->execute();

        $this->assertEquals(array(array(), new NullTask()), $result);
    }

    public function testExecuteNullTask()
    {
        $stub = $this->getMockBuilder(MockSyncSubtaskService::class)
                        ->setConstructorArgs(array(new RuleService(), new Task()))
                        ->setMethods(['validateDataCorrect', 'fetchSourceSearchData', 'addSubtask'])
                        ->getMock();

        $stub->expects($this->once())
            ->method('validateDataCorrect')
            ->willReturn(true);

        $stub->expects($this->once())
            ->method('fetchSourceSearchData')
            ->willReturn([array('searchData'), 1]);

        $stub->expects($this->once())
            ->method('addSubtask')
            ->willReturn(new NullTask());

        $result = $stub->execute();

        $this->assertEquals(array(array(), new NullTask()), $result);
    }

    public function testExecute()
    {
        $stub = $this->getMockBuilder(MockSyncSubtaskService::class)
                        ->setConstructorArgs(array(new RuleService(), new Task()))
                        ->setMethods([
                            'getRuleService',
                            'getTask',
                            'validateDataCorrect',
                            'fetchSourceSearchData',
                            'addSubtask',
                            'searchDataTransformationAndAdd',
                            'updateTask',
                            'fetchRuleServiceList'
                        ])->getMock();

        $subtask = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(2);
        $sourceSearchDataList = array('sourceSearchDataList');
        $count = 1;
        $ruleServiceList = array($this->ruleService);

        $stub->expects($this->once())->method('getRuleService')->willReturn($this->ruleService);
        $stub->expects($this->once())->method('getTask')->willReturn($this->task);
        $stub->expects($this->once())->method('validateDataCorrect')->willReturn(true);
        $stub->expects($this->once())->method('fetchSourceSearchData')->willReturn([$sourceSearchDataList, $count]);
        $stub->expects($this->once())->method('addSubtask')->willReturn($subtask);
        $stub->expects($this->once())->method('searchDataTransformationAndAdd');
        $stub->expects($this->once())->method('updateTask');
        $stub->expects($this->once())->method('fetchRuleServiceList')->willReturn($ruleServiceList);

        $result = $stub->execute();

        $this->assertEquals(array($ruleServiceList, $subtask), $result);
    }

    public function testValidateDataCorrect()
    {
        $stub = $this->getMockBuilder(MockSyncSubtaskService::class)
                        ->setConstructorArgs(array(new RuleService(), new Task()))
                        ->setMethods(['isRuleServiceExist', 'isTaskExist'])
                        ->getMock();
                       
        $stub->expects($this->once())->method('isRuleServiceExist')->with($this->ruleService)->willReturn(true);
        $stub->expects($this->once())->method('isTaskExist')->with($this->task)->willReturn(true);

        $result = $stub->validateDataCorrect($this->ruleService, $this->task);

        $this->assertTrue($result);
    }

    public function testIsRuleServiceExistTrue()
    {
        $result = $this->stub->isRuleServiceExist($this->ruleService);

        $this->assertTrue($result);
    }
    
    public function testIsRuleServiceExistFalse()
    {
        $result = $this->stub->isRuleServiceExist(new NullRuleService());

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('ruleServiceId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsTaskExistTrue()
    {
        $result = $this->stub->isTaskExist($this->task);

        $this->assertTrue($result);
    }
    
    public function testIsTaskExistFalse()
    {
        $result = $this->stub->isTaskExist(new NullTask());

        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('taskId', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testFetchSourceSearchData()
    {
        $stub = $this->getMockBuilder(MockSyncSubtaskService::class)
                        ->setConstructorArgs(array(new RuleService(), new Task()))
                        ->setMethods(['getSearchDataRepositoryFactory'])
                        ->getMock();

        $sourceSearchDataList = array('sourceSearchDataList');
        $count = 1;
        $filter['task'] = $this->task->getId();

        $factory = $this->prophesize(SearchDataRepositoryFactory::class);
        $repository = $this->prophesize(WbjSearchDataRepository::class);

        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$sourceSearchDataList, $count]);

        $factory->getRepository(
            Argument::exact($this->ruleService->getSourceCategory())
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $stub->expects($this->once())->method('getSearchDataRepositoryFactory')->willReturn($factory->reveal());

        $result = $stub->fetchSourceSearchData($this->ruleService, $this->task);

        $this->assertEquals(array($sourceSearchDataList, $count), $result);
    }

    private function initialAddSubtask($result)
    {
        $this->stub = $this->getMockBuilder(MockSyncSubtaskService::class)
                        ->setConstructorArgs(array(new RuleService(), new Task()))
                        ->setMethods(['getSubtask'])
                        ->getMock();

        $count = 1;

        $task = $this->prophesize(Task::class);
        $task->setCrew(Argument::exact($this->task->getCrew()))->shouldBeCalledTimes(1);
        $task->setUserGroup(Argument::exact($this->task->getUserGroup()))->shouldBeCalledTimes(1);
        $task->setFileName(Argument::exact($this->task->getFileName()))->shouldBeCalledTimes(1);
        $task->setTotal(Argument::exact($count))->shouldBeCalledTimes(1);
        $task->setPid(Argument::exact($this->task->getId()))->shouldBeCalledTimes(1);
        $task->setSourceCategory(Argument::exact($this->ruleService->getSourceCategory()))->shouldBeCalledTimes(1);
        $task->setSourceTemplate(
            Argument::exact($this->ruleService->getSourceTemplate()->getId())
        )->shouldBeCalledTimes(1);
        $task->setTargetCategory(
            Argument::exact($this->ruleService->getTransformationCategory())
        )->shouldBeCalledTimes(1);
        $task->setTargetTemplate(
            Argument::exact($this->ruleService->getTransformationTemplate()->getId())
        )->shouldBeCalledTimes(1);
        $task->setTargetRule(Argument::exact($this->ruleService->getId()))->shouldBeCalledTimes(1);
        $task->setScheduleTask(Argument::exact(posix_getpid()))->shouldBeCalledTimes(1);

        $task->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->once())->method('getSubtask')->willReturn($task->reveal());
    }

    public function testAddSubtaskTrue()
    {
        $this->initialAddSubtask(true);

        $result = $this->stub->addSubtask($this->ruleService, $this->task, 1);

        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\Task',
            $result
        );
    }

    public function testAddSubtaskFalse()
    {
        $this->initialAddSubtask(false);

        $result = $this->stub->addSubtask($this->ruleService, $this->task, 1);

        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\NullTask',
            $result
        );
    }
}

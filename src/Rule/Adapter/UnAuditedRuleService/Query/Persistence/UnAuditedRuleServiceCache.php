<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class UnAuditedRuleServiceCache extends Cache
{
    const KEY = 'apply_form_rule';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Repository\BjTemplateRepository;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Command\RuleService\AddRuleServiceCommand;

use BaseData\Crew\Model\Crew;
use BaseData\UserGroup\Model\UserGroup;

class AddRuleServiceCommandHandlerTest extends TestCase
{
    private $command;
    
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(AddRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         [
                                            'getRepository',
                                            'getUnAuditedRuleService',
                                            'generateUnAuditedRuleServiceCrew',
                                            'generateUnAuditedRuleServiceRules',
                                            'applyInfo'
                                         ]
                                     )
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $this->command = new class implements ICommand {
        };
        $this->commandHandler->execute($this->command);
    }

    private function initialExecute($result, $id = 0)
    {
        //初始化
        $transformationTemplateId = 1;
        $sourceTemplateId = 2;
        $transformationCategory = 1;
        $sourceCategory = 10;
        $crew = 4;
        $rules = ['rules'];
        $applyInfo = ['applyInfo'];

        $this->command = new AddRuleServiceCommand(
            $rules,
            $transformationTemplateId,
            $sourceTemplateId,
            $transformationCategory,
            $sourceCategory,
            $crew
        );

        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew(1);
        $userGroup = $crew->getUserGroup();

        $sourceTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($sourceTemplateId);
        $transformationTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $transformationTemplateId
        );
        $sourceTemplate->setSourceUnit($userGroup);

        $transformationRepository =  $this->prophesize(BjTemplateRepository::class);
        $transformationRepository->fetchOne(
            Argument::exact($transformationTemplateId)
        )->willReturn($transformationTemplate);

        $sourceRepository =  $this->prophesize(WbjTemplateRepository::class);
        $sourceRepository->fetchOne(Argument::exact($sourceTemplateId))->willReturn($sourceTemplate);

        $this->commandHandler->expects($this->exactly(2))
                   ->method('getRepository')
                   ->will($this->returnValueMap(
                       [
                        [$sourceCategory,$sourceRepository->reveal()],
                        [$transformationCategory,$transformationRepository->reveal()]
                       ]
                   ));

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);
        $unAuditedRuleService->setTransformationTemplate(Argument::exact($transformationTemplate))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->setSourceTemplate(Argument::exact($sourceTemplate))->shouldBeCalledTimes(1);
        $unAuditedRuleService->setUserGroup(Argument::exact($userGroup))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->setTransformationCategory(Argument::exact($transformationCategory))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->setSourceCategory(Argument::exact($sourceCategory))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->setOperationType(Argument::exact($this->command->operationType))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->generateVersion()
                             ->shouldBeCalledTimes(1);
        if ($result) {
            $unAuditedRuleService->getApplyId()->shouldBeCalledTimes(1)->willReturn($id);
        }

        $unAuditedRuleService->add()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->once())->method('getUnAuditedRuleService')
                       ->willReturn($unAuditedRuleService->reveal());

        $this->commandHandler->expects($this->once())
                       ->method('generateUnAuditedRuleServiceRules')
                       ->with(
                           $this->command->rules,
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($unAuditedRuleService->reveal());

        $this->commandHandler->expects($this->once())
                       ->method('generateUnAuditedRuleServiceCrew')
                       ->with(
                           $this->command->crew,
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($unAuditedRuleService->reveal());

        $this->commandHandler->expects($this->once())
                       ->method('applyInfo')
                       ->with(
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($applyInfo);
    }

    public function testExecuteSuccess()
    {
        $id = 10;

        $this->initialExecute(true, $id);

        $result = $this->commandHandler->execute($this->command);
        $this->assertTrue($result);
        $this->assertEquals($id, $this->command->id);
    }

    public function testExecuteFalse()
    {
        $this->initialExecute(false);

        $result = $this->commandHandler->execute($this->command);
        $this->assertFalse($result);
        $this->assertEquals(0, $this->command->id);
    }
}

<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试国标资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_items_data');
        $this->clear('pcore_gb_search_data');
    }

    /**
     * 存在一条国标资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data'],
                'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data']
            ]
        );
    }

    public function testViewGbSearchData()
    {
        $setGbSearchData = $this->gbSearchData()['pcore_gb_search_data'][0];
        $setTemplate = $this->gbTemplate()[0];
        $setGbItemsData = $this->gbSearchData()['pcore_gb_items_data'][0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'gbSearchData/1?include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->gbSearchDataCompareData($setGbSearchData, $contents['data']);
        $this->gbItemsDataCompareData($setGbItemsData, $contents['included'][0]);
        $this->gbTemplateCompareData($setTemplate, $contents['included'][1]);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\Persistence;

class MockGbItemsDataDb extends GbItemsDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

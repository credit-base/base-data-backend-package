<?php
namespace BaseData\Template\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Utils\BjTemplateUtils;
use BaseData\Template\Utils\BjTemplateMockFactory;

class BjTemplateDbTranslatorTest extends TestCase
{
    use BjTemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjTemplateDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Template\Model\NullBjTemplate', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $bjTemplate = BjTemplateMockFactory::generateBjTemplate(1);

        $expression['bj_template_id'] = $bjTemplate->getId();
        $expression['name'] = $bjTemplate->getName();
        $expression['identify'] = $bjTemplate->getIdentify();
        $expression['subject_category'] = json_encode($bjTemplate->getSubjectCategory());
        $expression['dimension'] = $bjTemplate->getDimension();
        $expression['exchange_frequency'] = $bjTemplate->getExchangeFrequency();
        $expression['info_classify'] = $bjTemplate->getInfoClassify();
        $expression['info_category'] = $bjTemplate->getInfoCategory();
        $expression['description'] = $bjTemplate->getDescription();
        $expression['items'] = json_encode($bjTemplate->getItems());
        $expression['source_unit_id'] = $bjTemplate->getSourceUnit()->getId();
        $expression['gb_template_id'] = $bjTemplate->getGbTemplate()->getId();

        $expression['status'] = $bjTemplate->getStatus();
        $expression['status_time'] = $bjTemplate->getStatusTime();
        $expression['create_time'] = $bjTemplate->getCreateTime();
        $expression['update_time'] = $bjTemplate->getUpdateTime();

        $bjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Template\Model\BjTemplate', $bjTemplate);
        $this->compareArrayAndObject($expression, $bjTemplate);
    }

    public function testObjectToArray()
    {
        $bjTemplate = BjTemplateMockFactory::generateBjTemplate(1);

        $expression = $this->translator->objectToArray($bjTemplate);

        $this->compareArrayAndObject($expression, $bjTemplate);
    }
}

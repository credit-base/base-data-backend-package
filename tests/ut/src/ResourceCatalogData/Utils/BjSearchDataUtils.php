<?php
namespace BaseData\ResourceCatalogData\Utils;

trait BjSearchDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjSearchData
    ) {
        $this->assertEquals($expectedArray['bj_search_data_id'], $bjSearchData->getId());
        $this->assertEquals($expectedArray['info_classify'], $bjSearchData->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $bjSearchData->getInfoCategory());
        $this->assertEquals($expectedArray['crew_id'], $bjSearchData->getCrew()->getId());
        $this->assertEquals($expectedArray['usergroup_id'], $bjSearchData->getSourceUnit()->getId());
        $this->assertEquals($expectedArray['subject_category'], $bjSearchData->getSubjectCategory());
        $this->assertEquals($expectedArray['dimension'], $bjSearchData->getDimension());
        $this->assertEquals($expectedArray['name'], $bjSearchData->getName());
        $this->assertEquals($expectedArray['identify'], $bjSearchData->getIdentify());
        $this->assertEquals($expectedArray['expiration_date'], $bjSearchData->getExpirationDate());
        $this->assertEquals($expectedArray['bj_template_id'], $bjSearchData->getTemplate()->getId());
        $this->assertEquals($expectedArray['bj_items_data_id'], $bjSearchData->getItemsData()->getId());
        $this->assertEquals($expectedArray['status'], $bjSearchData->getStatus());
        $this->assertEquals($expectedArray['hash'], $bjSearchData->getHash());
        $this->assertEquals($expectedArray['create_time'], $bjSearchData->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $bjSearchData->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $bjSearchData->getStatusTime());
        $this->assertEquals($expectedArray['task_id'], $bjSearchData->getTask()->getId());
        $this->assertEquals($expectedArray['description'], $bjSearchData->getDescription());
        $this->assertEquals($expectedArray['front_end_processor_status'], $bjSearchData->getFrontEndProcessorStatus());
    }
}

<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query\Persistence;

use Marmot\Framework\Classes\Db;

class UnAuditedRuleServiceDb extends Db
{
    const TABLE = 'apply_form_rule';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

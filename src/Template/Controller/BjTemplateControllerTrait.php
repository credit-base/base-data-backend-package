<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\CommandHandler\BjTemplate\BjTemplateCommandHandlerFactory;

trait BjTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : BjTemplateRepository
    {
        return new BjTemplateRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new BjTemplateCommandHandlerFactory());
    }
}

<?php
namespace BaseData\Rule\Adapter\RuleService;

use Marmot\Interfaces\INull;

use Marmot\Interfaces\ITranslator;
use BaseData\Rule\Adapter\RuleService\Query\RuleServiceRowCacheQuery;

class MockRuleServiceDbAdapter extends RuleServiceDbAdapter
{
    public function getDbTranslator() : ITranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : RuleServiceRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class GbItemsDataCache extends Cache
{
    const KEY = 'gb_items_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace Home\Controller;

use PHPUnit\Framework\TestCase;

class IndexControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new IndexController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testIndex()
    {
        $this->expectOutputString('Hello World Base Data Backend Package');

        $result = $this->controller->index();

        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullGbSearchDataTest extends TestCase
{
    private $gbStub;

    public function setUp()
    {
        $this->gbStub = $this->getMockBuilder(NullGbSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->gbStub);
    }

    public function testExtendsGbSearchData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\GbSearchData', $this->gbStub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->gbStub);
    }

    public function testConfirm()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->confirm();
        $this->assertFalse($result);
    }

    public function testIsConfirm()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->isConfirm();
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->disable();
        $this->assertFalse($result);
    }

    public function testIsEnabled()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->isEnabled();
        $this->assertFalse($result);
    }

    public function testDelete()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->delete();
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $this->mockResourceNotExist();
        
        $result = $this->gbStub->updateStatus(1);
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->gbStub->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}

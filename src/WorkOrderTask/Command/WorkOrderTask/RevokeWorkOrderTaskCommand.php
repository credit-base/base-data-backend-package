<?php
namespace BaseData\WorkOrderTask\Command\WorkOrderTask;

use Marmot\Interfaces\ICommand;

class RevokeWorkOrderTaskCommand implements ICommand
{
    public $id;
    public $reason;

    public function __construct(
        string $reason,
        int $id
    ) {
        $this->reason = $reason;
        $this->id = $id;
    }
}

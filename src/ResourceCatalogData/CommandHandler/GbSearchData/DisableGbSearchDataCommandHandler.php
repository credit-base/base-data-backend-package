<?php
namespace BaseData\ResourceCatalogData\CommandHandler\GbSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand;

class DisableGbSearchDataCommandHandler implements ICommandHandler
{
    use GbSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof DisableGbSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }

        $gbSearchData = $this->fetchGbSearchData($command->id);

        return $gbSearchData->disable();
    }
}

<?php
namespace BaseData\Template\Model;

use PHPUnit\Framework\TestCase;

class NullWbjTemplateTest extends TestCase
{
    private $wbjTemplate;

    public function setUp()
    {
        $this->wbjTemplate = NullWbjTemplate::getInstance();
    }

    public function tearDown()
    {
        unset($this->wbjTemplate);
    }

    public function testExtendsWbjTemplate()
    {
        $this->assertInstanceof('BaseData\Template\Model\WbjTemplate', $this->wbjTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->wbjTemplate);
    }
}

<?php
namespace BaseData\UserGroup\Repository;

use Marmot\Core;
use Marmot\Framework\Classes\Repository;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use BaseData\UserGroup\Adapter\UserGroup\UserGroupMockAdapter;
use BaseData\UserGroup\Adapter\UserGroup\UserGroupRestfulAdapter;

class UserGroupRepository extends Repository implements IUserGroupAdapter
{
    private $adapter;

    const LIST_MODEL_UN = 'USER_GROUP_LIST';
    const FETCH_ONE_MODEL_UN = 'USER_GROUP_FETCH_ONE';
    const ENV_TEST = 'test';

    public function __construct()
    {
        $this->adapter = Core::$container->has('env.test') && Core::$container->get('env.test') == self::ENV_TEST ?
        new UserGroupMockAdapter() :
        new UserGroupRestfulAdapter(
            Core::$container->has('baseSdk.url') ? Core::$container->get('baseSdk.url') : '',
            Core::$container->has('baseSdk.authKey') ? Core::$container->get('baseSdk.authKey') : []
        );
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function getActualAdapter()
    {
        return $this->adapter;
    }

    public function getMockAdapter()
    {
        return new UserGroupMockAdapter();
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }

    public function fetchOne(int $id) : UserGroup
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
}

<?php
namespace BaseData\Rule\Model;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

trait MockConcretenessRuleTrait
{
    public function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        return parent::validateTransformation($transformationSearchData);
    }

    public function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {
        return parent::transformationInitialization($sourceSearchData, $transformationSearchData);
    }

    public function validateInitializationDataFormat(array $data) : bool
    {
        return parent::validateInitializationDataFormat($data);
    }

    public function transformationExecute(array $data, ISearchDataAble $transformationSearchData) : array
    {
        return parent::transformationExecute($data, $transformationSearchData);
    }
    
    public function validateDataFormat(array $data) : bool
    {
        return parent::validateDataFormat($data);
    }

    public function transformationResult(array $data, ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        return parent::transformationResult($data, $transformationSearchData);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Model\IEnableAble;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\NullWbjTemplate;

use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WbjSearchDataTest extends TestCase
{
    private $wbjSearchData;

    private $faker;

    public function setUp()
    {
        $this->wbjSearchData = new MockWbjSearchData();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->wbjSearchData);
        unset($this->faker);
    }

    public function testImplementsIEnableAble()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IEnableAble',
            $this->wbjSearchData
        );
    }
    
    public function testSearchDataConstructor()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\WbjTemplate',
            $this->wbjSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\WbjItemsData',
            $this->wbjSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new WbjItemsData();

        $this->wbjSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->wbjSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->wbjSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end
    
    public function testGetWbjItemsDataRepository()
    {
        $wbjSearchData = new MockWbjSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter',
            $wbjSearchData->getWbjItemsDataRepository()
        );
    }

    public function testGetRepository()
    {
        $wbjSearchData = new MockWbjSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $wbjSearchData->getRepository()
        );
    }

    public function testAddItemsData()
    {
        $wbjSearchData = $this->getMockBuilder(MockWbjSearchData::class)
                           ->setMethods(['getWbjItemsDataRepository'])
                           ->getMock();

        $repository = $this->prophesize(IWbjItemsDataAdapter::class);
        $repository->add(Argument::exact($wbjSearchData->getItemsData()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $wbjSearchData->expects($this->once())
             ->method('getWbjItemsDataRepository')
             ->willReturn($repository->reveal());

        $result = $wbjSearchData->addItemsData();
        $this->assertTrue($result);
    }

    public function testAdd()
    {
        $wbjSearchData = $this->getMockBuilder(MockWbjSearchData::class)
                           ->setMethods(['assignment', 'validate', 'addItemsData', 'getRepository'])
                           ->getMock();

        $wbjSearchData->expects($this->once())
            ->method('assignment');

        $wbjSearchData->expects($this->once())
            ->method('validate')
            ->willReturn(true);

        $wbjSearchData->expects($this->once())
            ->method('addItemsData')
            ->willReturn(true);

        $repository = $this->prophesize(IWbjSearchDataAdapter::class);
        $repository->add(Argument::exact($wbjSearchData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $wbjSearchData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $wbjSearchData->add();
        $this->assertTrue($result);
    }

    public function testUpdateStatus()
    {
        $this->wbjSearchData = $this->getMockBuilder(MockWbjSearchData::class)
            ->setMethods(['getRepository'])
            ->getMock();
            
        $status = IEnableAble::STATUS['ENABLED'];
        $this->wbjSearchData->setStatus($status);
        $this->wbjSearchData->setUpdateTime(Core::$container->get('time'));
        $this->wbjSearchData->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(IWbjSearchDataAdapter::class);

        $repository->edit(
            Argument::exact($this->wbjSearchData),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->wbjSearchData->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->wbjSearchData->updateStatus($status);
        
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Template\Adapter\WbjTemplate;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\NullWbjTemplate;
use BaseData\Template\Translator\WbjTemplateDbTranslator;
use BaseData\Template\Adapter\WbjTemplate\Query\WbjTemplateRowCacheQuery;

class WbjTemplateDbAdapter implements IWbjTemplateAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new WbjTemplateDbTranslator();
        $this->rowCacheQuery = new WbjTemplateRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : WbjTemplateDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : WbjTemplateRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullWbjTemplate::getInstance();
    }

    public function add(WbjTemplate $wbjTemplate) : bool
    {
        return $this->addAction($wbjTemplate);
    }

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool
    {
        return $this->editAction($wbjTemplate, $keys);
    }

    public function fetchOne($id) : WbjTemplate
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }
    
    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $wbjTemplate = new WbjTemplate();

            if (isset($filter['name'])) {
                $wbjTemplate->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($wbjTemplate, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['identify'])) {
                $wbjTemplate->setIdentify($filter['identify']);
                $info = $this->getDbTranslator()->objectToArray($wbjTemplate, array('identify'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['dimension'])) {
                $wbjTemplate->setDimension($filter['dimension']);
                $info = $this->getDbTranslator()->objectToArray($wbjTemplate, array('dimension'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoClassify'])) {
                $wbjTemplate->setInfoClassify($filter['infoClassify']);
                $info = $this->getDbTranslator()->objectToArray($wbjTemplate, array('infoClassify'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoCategory'])) {
                $wbjTemplate->setInfoCategory($filter['infoCategory']);
                $info = $this->getDbTranslator()->objectToArray($wbjTemplate, array('infoCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceUnit'])) {
                $wbjTemplate->getSourceUnit()->setId($filter['sourceUnit']);
                $info = $this->getDbTranslator()->objectToArray($wbjTemplate, array('sourceUnit'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new WbjTemplate(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray(new WbjTemplate(), array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

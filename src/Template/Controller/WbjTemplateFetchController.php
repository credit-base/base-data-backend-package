<?php
namespace BaseData\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\View\WbjTemplateView;
use BaseData\Template\Repository\WbjTemplateRepository;

class WbjTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IWbjTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new WbjTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'templates';
    }
}

<?php
namespace BaseData\Common\Command;

use PHPUnit\Framework\TestCase;

class RejectCommandTest extends TestCase
{
    private $command;

    /**
     * 初始化测试场景
     */
    public function setUp()
    {
        $this->command = new MockRejectCommand('rejectReason');
    }

    public function tearDown()
    {
        unset($this->command);
    }

    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    /**
     * 1. 测试初始化赋值id
     * 2. 测试初始化赋值id是否正确
     */
    public function testConstructWithId()
    {
        $id = 2;
        $rejectReason = 'rejectReason';

        $this->command = new MockRejectCommand($rejectReason, $id);
        $this->assertEquals($id, $this->command->id);
        $this->assertEquals($rejectReason, $this->command->rejectReason);
    }
}

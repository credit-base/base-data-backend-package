<?php
namespace BaseData\Template\BjTemplate\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Translator\BjTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我之前的本级目录需要修改时,在原有的本级目录上,修改本级目录内容
 *           根据我需要修改的内容和规范修改原有本级目录,以便于我有本级目录可以使用
 * @Scenario: 编辑资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_gb_template' => $this->gbTemplate(),
        ]);
    }

    /**
     * @When: 获取需要编辑的用户数据
     */
    public function fetchTemplate($id)
    {
        $repository = new BjTemplateRepository();

        $bjTemplate = $repository->fetchOne($id);

        return $bjTemplate;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $bjTemplate = $this->fetchTemplate(1);

        $bjTemplate->setSubjectCategory(array(Template::SUBJECT_CATEGORY['ZRR']));
        $bjTemplate->setDimension(Template::DIMENSION['ZWGX']);
        $bjTemplate->setExchangeFrequency(2);
        $bjTemplate->setInfoClassify(Template::INFO_CLASSIFY['XZXK']);
        $bjTemplate->setInfoCategory(Template::INFO_CATEGORY['SHOUXXX']);
        $bjTemplate->setItems(array('items'));
        
        return $bjTemplate->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $bjTemplate = $this->fetchTemplate(1);

        $this->assertEquals(array(Template::SUBJECT_CATEGORY['ZRR']), $bjTemplate->getSubjectCategory());
        $this->assertEquals(Template::DIMENSION['ZWGX'], $bjTemplate->getDimension());
        $this->assertEquals(2, $bjTemplate->getExchangeFrequency());
        $this->assertEquals(Template::INFO_CLASSIFY['XZXK'], $bjTemplate->getInfoClassify());
        $this->assertEquals(Template::INFO_CATEGORY['SHOUXXX'], $bjTemplate->getInfoCategory());
        $this->assertEquals(array('items'), $bjTemplate->getItems());
    }
}

<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Model\IProgress;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;

/**
 * @Feature: 我是委办局领导,当我们有数据但没有对应的资源目录可以上报时,在政务网OA中,可以通过接受发改委领导指派的工单任务生成对应的资源目录
 *           当任务我确认无误时可以同意使用任务中的资源目录上报数据，当任务有问题时我可以进行反馈,以便于我可以把我的数据通过发改委指派的资源目录上报上去
 * @Scenario: 反馈工单任务
 */
class FeedbackTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_work_order_task');
    }

    /**
     * @Given: 存在需要反馈的父任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_parent_task' => $this->parentTask(),
            'pcore_work_order_task' => $this->workOrderTask(IProgress::STATUS['GJZ'])
        ]);
    }

    protected function getWorkOrderTaskRepository() : WorkOrderTaskRepository
    {
        return new WorkOrderTaskRepository();
    }
    /**
     * @When: 获取需要反馈的父任务
     */
    public function fetchWorkOrderTask($id)
    {
        $repository = $this->getWorkOrderTaskRepository();

        $workOrderTask = $repository->fetchOne($id);

        return $workOrderTask;
    }

    private function feedbackRecords() : array
    {
        return array(  //反馈记录
            array(
                'crew' => 1,  //反馈人
                'userGroup' => 1,  //反馈委办局
                'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                'templateId' => '1',  //目录id
                'items' => array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    ),
                ),
            )
        );
    }
    /**
     * @And: 当我调用反馈函数,期待返回true
     */
    public function feedback()
    {
        $workOrderTask = $this->fetchWorkOrderTask(1);
        $workOrderTask->setFeedbackRecords($this->feedbackRecords());
        
        return $workOrderTask->feedback();
    }

    /**
     * @Then: 数据已经被反馈
     */
    public function testValidate()
    {
        $result = $this->feedback();

        $this->assertTrue($result);

        $workOrderTask = $this->fetchWorkOrderTask(1);

        $this->assertEquals($this->feedbackRecords(), $workOrderTask->getFeedbackRecords());
        $this->assertEquals(IProgress::STATUS['GJZ'], $workOrderTask->getStatus());
        $this->assertEquals(Core::$container->get('time'), $workOrderTask->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $workOrderTask->getStatusTime());
    }
}

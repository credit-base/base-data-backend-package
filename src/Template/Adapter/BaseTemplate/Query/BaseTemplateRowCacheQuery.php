<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query;

use Marmot\Framework\Query\RowCacheQuery;

class BaseTemplateRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'base_template_id',
            new Persistence\BaseTemplateCache(),
            new Persistence\BaseTemplateDb()
        );
    }
}

<?php
namespace BaseData\WorkOrderTask\ParentTask\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\WorkOrderTaskUtils;
use BaseData\WorkOrderTask\Model\WorkOrderTask;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试父工单任务接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WorkOrderTaskUtils, TemplateSetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_bj_template');
    }

    /**
     * 不存在父工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_bj_template' => $this->bjTemplate()
            ]
        );
    }

    public function testViewParentTask()
    {
        $setParentTask = $this->parentTask()[0];
        
        $data = array(
            "data" => array(
                "type" => "parentTasks",
                "attributes" => array(
                    "templateType" => 2,    //基础目录，国标目录 1 | 本级目录 2
                    "title" => '归集企业基本信息',    //任务标题
                    "description" => '依据国家要求，现需要按要求归集企业基本信息',    //任务描述
                    "endTime" => '2022-10-01',    //终结时间
                    "attachment" => array(  //依据附件
                        'name' => '附件名称',
                        'identify' => '依据附件.pdf'
                    )
                ),
                "relationships" => array(
                    "template" => array( // 指派目录，根据所选择基础目录，在以下示例二选一
                        "data" => array(
                            array("type" => "bjTemplate", "id" => 1), //国标目录
                        )
                    ),
                    "assignObjects" => array( // 指派对象
                        "data" => array(
                            array("type" => "userGroups", "id" => 1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'parentTasks',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->parentTaskCompareData($setParentTask, $contents['data']);
    }
}

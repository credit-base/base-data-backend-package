<?php
namespace BaseData\Common\View;

use Marmot\Interfaces\IView;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(PHPMD)
 */
abstract class CommonView implements IView
{
    use JsonApiView;

    private $rules;

    private $data;
    
    private $encodingParameters;

    public function __construct($data, $encodingParameters = null)
    {
        $this->data = $data;
        $this->encodingParameters = $encodingParameters;

        $this->rules = array(
            \BaseData\UserGroup\Model\UserGroup::class => \BaseData\UserGroup\View\UserGroupSchema::class,
            \BaseData\UserGroup\Model\NullUserGroup::class => \BaseData\UserGroup\View\UserGroupSchema::class,

            \BaseData\Crew\Model\Crew::class => \BaseData\Crew\View\CrewSchema::class,
            \BaseData\Crew\Model\NullCrew::class => \BaseData\Crew\View\CrewSchema::class,

            \BaseData\Template\Model\GbTemplate::class => \BaseData\Template\View\GbTemplateSchema::class,
            \BaseData\Template\Model\NullGbTemplate::class => \BaseData\Template\View\GbTemplateSchema::class,
            
            \BaseData\Template\Model\BjTemplate::class => \BaseData\Template\View\BjTemplateSchema::class,
            \BaseData\Template\Model\NullBjTemplate::class => \BaseData\Template\View\BjTemplateSchema::class,

            \BaseData\Template\Model\BaseTemplate::class => \BaseData\Template\View\BaseTemplateSchema::class,
            \BaseData\Template\Model\NullBaseTemplate::class => \BaseData\Template\View\BaseTemplateSchema::class,
            
            \BaseData\Template\Model\WbjTemplate::class => \BaseData\Template\View\WbjTemplateSchema::class,
            \BaseData\Template\Model\NullWbjTemplate::class => \BaseData\Template\View\WbjTemplateSchema::class,

            \BaseData\Template\Model\QzjTemplate::class => \BaseData\Template\View\QzjTemplateSchema::class,
            \BaseData\Template\Model\NullQzjTemplate::class => \BaseData\Template\View\QzjTemplateSchema::class,
            
            \BaseData\WorkOrderTask\Model\ParentTask::class => \BaseData\WorkOrderTask\View\ParentTaskSchema::class,
            \BaseData\WorkOrderTask\Model\NullParentTask::class => \BaseData\WorkOrderTask\View\ParentTaskSchema::class,
            \BaseData\WorkOrderTask\Model\WorkOrderTask::class =>
            \BaseData\WorkOrderTask\View\WorkOrderTaskSchema::class,
            \BaseData\WorkOrderTask\Model\NullWorkOrderTask::class =>
            \BaseData\WorkOrderTask\View\WorkOrderTaskSchema::class,

            \BaseData\ResourceCatalogData\Model\YsSearchData::class =>
            \BaseData\ResourceCatalogData\View\YsSearchDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullYsSearchData::class =>
            \BaseData\ResourceCatalogData\View\YsSearchDataSchema::class,

            \BaseData\ResourceCatalogData\Model\WbjSearchData::class =>
            \BaseData\ResourceCatalogData\View\WbjSearchDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullWbjSearchData::class =>
            \BaseData\ResourceCatalogData\View\WbjSearchDataSchema::class,

            \BaseData\ResourceCatalogData\Model\BjSearchData::class =>
            \BaseData\ResourceCatalogData\View\BjSearchDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullBjSearchData::class =>
            \BaseData\ResourceCatalogData\View\BjSearchDataSchema::class,

            \BaseData\ResourceCatalogData\Model\GbSearchData::class =>
            \BaseData\ResourceCatalogData\View\GbSearchDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullGbSearchData::class =>
            \BaseData\ResourceCatalogData\View\GbSearchDataSchema::class,

            \BaseData\ResourceCatalogData\Model\YsItemsData::class =>
            \BaseData\ResourceCatalogData\View\YsItemsDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullYsItemsData::class =>
            \BaseData\ResourceCatalogData\View\YsItemsDataSchema::class,

            \BaseData\ResourceCatalogData\Model\WbjItemsData::class =>
            \BaseData\ResourceCatalogData\View\WbjItemsDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullWbjItemsData::class =>
            \BaseData\ResourceCatalogData\View\WbjItemsDataSchema::class,

            \BaseData\ResourceCatalogData\Model\BjItemsData::class =>
            \BaseData\ResourceCatalogData\View\BjItemsDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullBjItemsData::class =>
            \BaseData\ResourceCatalogData\View\BjItemsDataSchema::class,

            \BaseData\ResourceCatalogData\Model\GbItemsData::class =>
            \BaseData\ResourceCatalogData\View\GbItemsDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullGbItemsData::class =>
            \BaseData\ResourceCatalogData\View\GbItemsDataSchema::class,

            \BaseData\Statistical\Model\Statistical::class =>
            \BaseData\Statistical\View\StatisticalSchema::class,
            \BaseData\Statistical\Model\NullStatistical::class =>
            \BaseData\Statistical\View\StatisticalSchema::class,

            \BaseData\Rule\Model\RuleService::class =>
            \BaseData\Rule\View\RuleServiceSchema::class,
            \BaseData\Rule\Model\NullRuleService::class =>
            \BaseData\Rule\View\RuleServiceSchema::class,
            \BaseData\Rule\Model\UnAuditedRuleService::class =>
            \BaseData\Rule\View\UnAuditedRuleServiceSchema::class,
            \BaseData\Rule\Model\NullUnAuditedRuleService::class =>
            \BaseData\Rule\View\UnAuditedRuleServiceSchema::class,

            \BaseData\ResourceCatalogData\Model\Task::class =>
            \BaseData\ResourceCatalogData\View\TaskSchema::class,
            \BaseData\ResourceCatalogData\Model\NullTask::class =>
            \BaseData\ResourceCatalogData\View\TaskSchema::class,

            \BaseData\ResourceCatalogData\Model\ErrorData::class =>
            \BaseData\ResourceCatalogData\View\ErrorDataSchema::class,
            \BaseData\ResourceCatalogData\Model\NullErrorData::class =>
            \BaseData\ResourceCatalogData\View\ErrorDataSchema::class,

            \BaseData\Enterprise\Model\Enterprise::class =>
            \BaseData\Enterprise\View\EnterpriseSchema::class,
            \BaseData\Enterprise\Model\NullEnterprise::class =>
            \BaseData\Enterprise\View\EnterpriseSchema::class,
        );
    }
    
    public function display()
    {
        return $this->jsonApiFormat($this->data, $this->rules, $this->encodingParameters);
    }
}

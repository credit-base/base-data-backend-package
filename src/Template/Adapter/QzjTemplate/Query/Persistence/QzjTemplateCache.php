<?php
namespace BaseData\Template\Adapter\QzjTemplate\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class QzjTemplateCache extends Cache
{
    const KEY = 'qzj_template';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use BaseData\ResourceCatalogData\Model\BaseSearchData;

interface IBaseSearchDataAdapter
{
    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(BaseSearchData $baseSearchData) : bool;
}

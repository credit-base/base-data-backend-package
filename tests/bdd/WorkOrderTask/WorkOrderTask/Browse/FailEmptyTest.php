<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;

/**
 * @Feature: 我是委办局领导,我登录的账号可以管理指派给我的任务,在政务网OA中,我可以管理给我指派的工单任务,
 *            通过列表形式呈现,以便于我可以把我的数据通过发改委指派的资源目录上报上去
 * @Scenario: 查看工单任务列表-异常数据不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在工单任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看工单任务列表时
     */
    public function fetchWorkOrderTaskList()
    {
        $repository = new WorkOrderTaskRepository();

        list($workOrderTaskList, $count) = $repository->filter([]);
        
        unset($count);

        return $workOrderTaskList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewWorkOrderTask()
    {
        $workOrderTaskList = $this->fetchWorkOrderTaskList();

        $this->assertEmpty($workOrderTaskList);
    }
}

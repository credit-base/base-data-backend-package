<?php
namespace BaseData\Template\GbTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Translator\GbTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要查看我的国标目录时,在政务网OA中,可以查看到我的所有国标目录,
 *           通过列表的形式查看所有国标目录,以便于我可以快速找到我的所有国标目录
 * @Scenario: 查看资源目录列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 存在资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
        ]);
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchGbTemplateList()
    {
        $repository = new GbTemplateRepository();

        list($gbTemplateList, $count) = $repository->filter([]);
        
        unset($count);

        return $gbTemplateList;
    }

    /**
     * @Then: 我可以看到资源目录的全部信息
     */
    public function testViewGbTemplateList()
    {
        $setGbTemplateList = $this->gbTemplate();

        foreach ($setGbTemplateList as $key => $setGbTemplate) {
            $setGbTemplateList[$key]['gb_template_id'] = $key +1;
            $setGbTemplateList[$key]['subject_category'] = json_decode($setGbTemplate['subject_category'], true);
            $setGbTemplateList[$key]['items'] = json_decode($setGbTemplate['items'], true);
        }

        $gbTemplateList = $this->fetchGbTemplateList();

        $translator = new GbTemplateDbTranslator();
        foreach ($gbTemplateList as $gbTemplate) {
            $gbTemplateArray[] = $translator->objectToArray($gbTemplate);
        }

        $this->assertEquals($gbTemplateArray, $setGbTemplateList);
    }
}

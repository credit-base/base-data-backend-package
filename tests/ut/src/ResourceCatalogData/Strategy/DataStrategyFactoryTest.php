<?php
namespace BaseData\ResourceCatalogData\Strategy;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DataStrategyFactoryTest extends TestCase
{
    private $strategy;

    public function setUp()
    {
        $this->strategy = new DataStrategyFactory();
    }

    public function tearDown()
    {
        unset($this->strategy);
    }

    public function testNullDataStrategy()
    {
        $strategy = $this->strategy->getStrategy(0);
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Strategy\NullDataStrategy',
                $strategy
            );
    }

    public function testGetStrategy()
    {
        foreach (DataStrategyFactory::MAPS as $key => $strategy) {
            $this->assertInstanceOf(
                $strategy,
                $this->strategy->getStrategy($key)
            );
        }
    }
}

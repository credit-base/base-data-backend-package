<?php
namespace BaseData\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Common\Controller\IFetchController;

class FetchControllerTest extends TestCase
{
    private $controller;

    private $mockController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(FetchController::class)
                                 ->setMethods(['getController'])
                                 ->getMock();

        $this->mockController = new class extends FetchController
        {
            public function getController(string $resource) : IFetchController
            {
                return parent::getController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->mockController);
        unset($this->resource);
    }

    public function testGetController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->mockController->getController($this->resource)
        );
    }

    public function testFilter()
    {
        $fetchController = $this->prophesize(IFetchController::class);
        $fetchController->filter()->shouldBeCalledTimes(1);

        $this->controller->expects($this->once())
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($fetchController->reveal());

        $this->controller->filter($this->resource);
    }

    public function testFetchList()
    {
        $ids = '1,2';

        $fetchController = $this->prophesize(IFetchController::class);
        $fetchController->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1);
        $this->controller->expects($this->once())
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($fetchController->reveal());

        $this->controller->fetchList($this->resource, $ids);
    }

    public function testFetchOne()
    {
        $id = 1;

        $fetchController = $this->prophesize(IFetchController::class);
        $fetchController->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1);
        $this->controller->expects($this->once())
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($fetchController->reveal());

        $this->controller->fetchOne($this->resource, $id);
    }
}

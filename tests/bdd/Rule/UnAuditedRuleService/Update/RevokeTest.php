<?php
namespace BaseData\Rule\UnAuditedRuleService\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Common\Model\IApproveAble;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限,在政务网OA的资源目录规则管理中,我可以管理我提交的资源目录的规则
 *           可以撤销审核状态为待审核的资源目录规则,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 撤销
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * @Given: 存在一条待审核的规则审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form_rule' => $this->applyFormRule(IApproveAble::APPLY_STATUS['PENDING'])
        ]);
    }

    /**
     * @When: 获取需要撤销的规则
     */
    public function fetchUnAuditedRuleService($id)
    {
        $repository = new UnAuditedRuleServiceRepository();

        $unAuditedRuleService = $repository->fetchOne($id);

        return $unAuditedRuleService;
    }

    /**
     * @And: 当我调用撤销函数,期待返回true
     */
    public function revoke()
    {
        $unAuditedRuleService = $this->fetchUnAuditedRuleService(1);
        $unAuditedRuleService->getApplyCrew()->setId(1);
        
        return $unAuditedRuleService->revoke();
    }

    /**
     * @Then: 数据已经被撤销
     */
    public function testValidate()
    {
        $result = $this->revoke();

        $this->assertTrue($result);

        $unAuditedRuleService = $this->fetchUnAuditedRuleService(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['REVOKE'], $unAuditedRuleService->getApplyStatus());
        $this->assertEquals(Core::$container->get('time'), $unAuditedRuleService->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedRuleService->getStatusTime());
        $this->assertEquals(1, $unAuditedRuleService->getApplyCrew()->getId());
    }
}

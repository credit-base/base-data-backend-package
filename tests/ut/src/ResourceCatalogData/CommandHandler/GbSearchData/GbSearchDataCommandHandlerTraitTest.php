<?php
namespace BaseData\ResourceCatalogData\CommandHandler\GbSearchData;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;

class GbSearchDataCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockGbSearchDataCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\GbSearchDataRepository',
            $this->trait->publicGetRepository()
        );
    }

    public function testFetchGbSearchData()
    {
        $trait = $this->getMockBuilder(
            MockGbSearchDataCommandHandlerTrait::class
        )->setMethods(['getRepository'])->getMock();

        $id = 1;
        $gbSearchData = \BaseData\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData($id);

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($gbSearchData);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchGbSearchData($id);

        $this->assertEquals($result, $gbSearchData);
    }
}

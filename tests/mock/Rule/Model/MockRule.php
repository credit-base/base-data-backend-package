<?php
namespace BaseData\Rule\Model;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class MockRule extends Rule
{
    public function validate() : bool
    {
        return true;
    }

    public function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        unset($transformationSearchData);
        return true;
    }

    public function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {
        unset($sourceSearchData);
        unset($transformationSearchData);
        return array();
    }

    public function transformationExecute(
        array $data,
        ISearchDataAble $transformationSearchData
    ) : array {
        unset($data);
        unset($transformationSearchData);
        return array();
    }

    public function transformationResult(
        array $data,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        unset($data);
        unset($transformationSearchData);
        return array();
    }
}

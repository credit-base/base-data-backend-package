<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

use BaseData\Common\Model\IEnableAble;

/**
 * 测试委办局资源目录接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_wbj_items_data');
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * 存在多条委办局资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data'],
                'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data']
            ]
        );
    }

    public function testViewWbjSearchData()
    {
        $setWbjSearchDataList = $this->wbjSearchData()['pcore_wbj_search_data'];
        $setTemplateList = $this->wbjTemplate();
        $setWbjItemsDataList = $this->wbjSearchData()['pcore_wbj_items_data'];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);

        $response = $client->request(
            'GET',
            'wbjSearchData?filter[sourceUnit]=1&filter[template]=1&filter[dimension]='.
            ISearchDataAble::DIMENSION['ZWGX'].
            '&filter[infoClassify]='.ISearchDataAble::INFO_CLASSIFY['HEIMD'].
            '&filter[infoCategory]='.ISearchDataAble::INFO_CATEGORY['SHIXXX'].
            '&filter[subjectCategory]='.ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'].
            '&filter[status]='.IEnableAble::STATUS['ENABLED'].'&filter[expirationDate]=2147483646
            &filter[name]=中研社食品有限公司&filter[identify]=91120104MA06FBJY36
            &include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $wbjSearchData) {
            $this->wbjSearchDataCompareData($setWbjSearchDataList[$key], $wbjSearchData);
        }

        $this->wbjItemsDataCompareData($setWbjItemsDataList[0], $included[0]);
        $this->wbjTemplateCompareData($setTemplateList[0], $included[1]);
    }
}

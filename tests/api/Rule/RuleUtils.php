<?php
namespace BaseData\Rule;

trait RuleUtils
{
    private function ruleCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['rule_id'], $responseDataArray['id']);
        $this->assertEquals('rules', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $rules = unserialize(gzuncompress(base64_decode($setDataArray['rules'], true)));
        $this->assertEquals($rules, $attributes['rules']);
        $this->assertEquals($setDataArray['version'], $attributes['version']);
        $this->assertEquals($setDataArray['data_total'], $attributes['dataTotal']);
        $this->assertEquals($setDataArray['transformation_category'], $attributes['transformationCategory']);
        $this->assertEquals($setDataArray['source_category'], $attributes['sourceCategory']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        // $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        // $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        // $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $ruleRelationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $ruleRelationshipsCrew['type']);
        $this->assertEquals($setDataArray['crew_id'], $ruleRelationshipsCrew['id']);

        $ruleRelationshipsUserGroup = $responseDataArray['relationships']['userGroup']['data'];
        $this->assertEquals('userGroups', $ruleRelationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['usergroup_id'], $ruleRelationshipsUserGroup['id']);

        $transformationTemplate = $responseDataArray['relationships']['transformationTemplate']['data'];
        $this->assertEquals('gbTemplates', $transformationTemplate['type']);
        $this->assertEquals($setDataArray['transformation_template_id'], $transformationTemplate['id']);

        $sourceTemplate = $responseDataArray['relationships']['sourceTemplate']['data'];
        $this->assertEquals('templates', $sourceTemplate['type']);
        $this->assertEquals($setDataArray['source_template_id'], $sourceTemplate['id']);
    }

    private function unAuditedRuleCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['apply_form_rule_id'], $responseDataArray['id']);
        $this->assertEquals('unAuditedRules', $responseDataArray['type']);

        $applyInfo = unserialize(gzuncompress(base64_decode($setDataArray['apply_info'], true)));

        $attributes = $responseDataArray['attributes'];

        $rules = unserialize(gzuncompress(base64_decode($applyInfo['rules'], true)));
        $this->assertEquals($rules, $attributes['rules']);
        $this->assertEquals($applyInfo['version'], $attributes['version']);
        $this->assertEquals($applyInfo['data_total'], $attributes['dataTotal']);
        $this->assertEquals($applyInfo['transformation_category'], $attributes['transformationCategory']);
        $this->assertEquals($applyInfo['source_category'], $attributes['sourceCategory']);

        $this->assertEquals($applyInfo['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $this->assertEquals($setDataArray['operation_type'], $attributes['operationType']);
        $this->assertEquals($setDataArray['apply_status'], $attributes['applyStatus']);
        $this->assertEquals($setDataArray['reject_reason'], $attributes['rejectReason']);
        $this->assertEquals($setDataArray['relation_id'], $attributes['relationId']);

        $applyCrew = $responseDataArray['relationships']['applyCrew']['data'];
        $this->assertEquals('crews', $applyCrew['type']);
        $this->assertEquals($setDataArray['apply_crew_id'], $applyCrew['id']);

        $publishCrew = $responseDataArray['relationships']['publishCrew']['data'];
        $this->assertEquals('crews', $publishCrew['type']);
        $this->assertEquals($setDataArray['publish_crew_id'], $publishCrew['id']);

        $userGroup = $responseDataArray['relationships']['userGroup']['data'];
        $this->assertEquals('userGroups', $userGroup['type']);
        $this->assertEquals($setDataArray['usergroup_id'], $userGroup['id']);

        $applyUserGroup = $responseDataArray['relationships']['applyUserGroup']['data'];
        $this->assertEquals('userGroups', $applyUserGroup['type']);
        $this->assertEquals($setDataArray['apply_usergroup_id'], $applyUserGroup['id']);

        $sourceTemplate = $responseDataArray['relationships']['sourceTemplate']['data'];
        $this->assertEquals('templates', $sourceTemplate['type']);
        $this->assertEquals($setDataArray['source_template_id'], $sourceTemplate['id']);

        $transformationTemplate = $responseDataArray['relationships']['transformationTemplate']['data'];
        $this->assertEquals('gbTemplates', $transformationTemplate['type']);
        $this->assertEquals($setDataArray['transformation_template_id'], $transformationTemplate['id']);
    }
}

<?php
namespace BaseData\Rule\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

class NullRuleTest extends TestCase
{
    private $rule;

    public function setUp()
    {
        $this->rule = NullRule::getInstance();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\Rule',
            $this->rule
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->rule
        );
    }

    public function testValidate()
    {
        $rule = new MockNullRule();
        $result = $rule->validate();
        
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_NOT_EXIST);
        $this->assertFalse($result);
    }

    public function testValidateTransformation()
    {
        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        
        $result = $this->rule->validateTransformation($transformationSearchData);
        
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_NOT_EXIST);
        $this->assertFalse($result);
    }

    public function testTransformationInitialization()
    {
        $sourceSearchData = \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        
        $result = $this->rule->transformationInitialization($sourceSearchData, $transformationSearchData);
        
        $this->assertEquals(array(), $result);
    }

    public function testTransformationExecute()
    {
        $data = array('data');
        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        
        $result = $this->rule->transformationExecute($data, $transformationSearchData);
        
        $this->assertEquals(array(), $result);
    }

    public function testTransformationResult()
    {
        $data = array('data');
        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        
        $result = $this->rule->transformationResult($data, $transformationSearchData);
        
        $this->assertEquals($transformationSearchData, $result);
    }

    public function testTransformation()
    {
        $sourceSearchData = \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        
        $result = $this->rule->transformation($sourceSearchData, $transformationSearchData);
        
        $this->assertEquals($transformationSearchData, $result);
    }
}

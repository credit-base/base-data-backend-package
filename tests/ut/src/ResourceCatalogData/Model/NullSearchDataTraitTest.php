<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullSearchDataTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullSearchDataTrait::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testIsCrewNull()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isCrewNull();
        $this->assertFalse($result);
    }

    public function testIsSourceUnitNull()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isSourceUnitNull();
        $this->assertFalse($result);
    }

    public function testIsTemplateNull()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isTemplateNull();
        $this->assertFalse($result);
    }

    public function testIsHashExist()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->isHashExist();
        $this->assertFalse($result);
    }

    public function testIsSubjectCategoryExist()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->isSubjectCategoryExist();
        $this->assertFalse($result);
    }

    public function testValidate()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->validate();
        $this->assertFalse($result);
    }

    public function testAssignment()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->assignment();
        $this->assertFalse($result);
    }

    public function testAddItemsData()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->addItemsData();
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->trait->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}

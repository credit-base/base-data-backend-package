<?php
namespace BaseData\Template\CommandHandler\BjTemplate;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;

class MockEditBjTemplateCommandHandler extends EditBjTemplateCommandHandler
{
    public function getBjTemplateRepository() : IBjTemplateAdapter
    {
        return parent::getBjTemplateRepository();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\NullErrorData;
use BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\ErrorData\Query\ErrorDataRowCacheQuery;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\TaskRepository;

use BaseData\Rule\Repository\RepositoryFactory;

use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BjTemplateRepository;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class ErrorDataDbAdapterTest extends TestCase
{
    private $adapter;

    private $faker;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction',
                                    'fetchTask',
                                    'fetchTemplate'
                                ]
                           )
                           ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->faker);
    }

    /**
     * 测试是否 实现 IErrorDataAdapter
     */
    public function testImplementsIErrorDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 ErrorDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockErrorDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 ErrorDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockErrorDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\Query\ErrorDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    /**
     * 测试是否初始化 TaskRepository
     */
    public function testGetTaskRepository()
    {
        $adapter = new MockErrorDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\TaskRepository',
            $adapter->getTaskRepository()
        );
    }

    public function testGetRepositoryFactory()
    {
        $adapter = new MockErrorDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RepositoryFactory',
            $adapter->getRepositoryFactory()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockErrorDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedErrorData = new ErrorData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedErrorData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedErrorData);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedErrorData = new ErrorData();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedErrorData, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedErrorData, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedErrorData = new ErrorData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedErrorData);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTask')
                         ->with($expectedErrorData);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTemplate')
                         ->with($expectedErrorData);

        //验证
        $errorData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedErrorData, $errorData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $errorDataOne = new ErrorData(1);
        $errorDataTwo = new ErrorData(2);

        $ids = [1, 2];

        $expectedErrorDataList = [];
        $expectedErrorDataList[$errorDataOne->getId()] = $errorDataOne;
        $expectedErrorDataList[$errorDataTwo->getId()] = $errorDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedErrorDataList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTask')
                         ->with($expectedErrorDataList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTemplate')
                         ->with($expectedErrorDataList);

        //验证
        $errorDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedErrorDataList, $errorDataList);
    }

    //fetchTask
    public function testFetchTaskIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchTaskByObject'
                                ]
                           )
                           ->getMock();
        
        $errorData = new ErrorData();

        $adapter->expects($this->exactly(1))
                         ->method('fetchTaskByObject')
                         ->with($errorData);

        $adapter->fetchTask($errorData);
    }

    public function testFetchTaskIsArray()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchTaskByList'
                                ]
                           )
                           ->getMock();
        
        $errorDataOne = new ErrorData(1);
        $errorDataTwo = new ErrorData(2);
        
        $errorDataList[$errorDataOne->getId()] = $errorDataOne;
        $errorDataList[$errorDataTwo->getId()] = $errorDataTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchTaskByList')
                         ->with($errorDataList);

        $adapter->fetchTask($errorDataList);
    }

    //fetchTaskByObject
    public function testFetchTaskByObject()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'getTaskRepository'
                                ]
                           )
                           ->getMock();

        $errorData = new ErrorData();
        $errorData->setTask(new Task(1));
        
        $taskId = 1;

        // 为 Task 类建立预言
        $task  = $this->prophesize(Task::class);
        $taskRepository = $this->prophesize(TaskRepository::class);
        $taskRepository->fetchOne(Argument::exact($taskId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($task);

        $adapter->expects($this->exactly(1))
                         ->method('getTaskRepository')
                         ->willReturn($taskRepository->reveal());

        $adapter->fetchTaskByObject($errorData);
    }

    //fetchTaskByList
    public function testFetchTaskByList()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'getTaskRepository'
                                ]
                           )
                           ->getMock();

        $errorDataOne = new ErrorData(1);
        $taskOne = new Task(1);
        $errorDataOne->setTask($taskOne);

        $errorDataTwo = new ErrorData(2);
        $taskTwo = new Task(2);
        $errorDataTwo->setTask($taskTwo);
        $taskIds = [1, 2];
        
        $taskList[$taskOne->getId()] = $taskOne;
        $taskList[$taskTwo->getId()] = $taskTwo;
        
        $errorDataList[$errorDataOne->getId()] = $errorDataOne;
        $errorDataList[$errorDataTwo->getId()] = $errorDataTwo;

        $taskRepository = $this->prophesize(TaskRepository::class);
        $taskRepository->fetchList(Argument::exact($taskIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($taskList);

        $adapter->expects($this->exactly(1))
                         ->method('getTaskRepository')
                         ->willReturn($taskRepository->reveal());

        $adapter->fetchTaskByList($errorDataList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockErrorDataDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterTask()
    {
        $filter = array(
            'task' => 1
        );
        $adapter = new MockErrorDataDbAdapter();

        $expectedCondition = 'task_id = '.$filter['task'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterCategory()
    {
        $filter = array(
            'category' => 1
        );
        $adapter = new MockErrorDataDbAdapter();

        $expectedCondition = 'category = '.$filter['category'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterTemplate()
    {
        $filter = array(
            'template' => 1
        );
        $adapter = new MockErrorDataDbAdapter();

        $expectedCondition = 'template_id = '.$filter['template'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockErrorDataDbAdapter();

        $expectedCondition = ' ORDER BY error_data_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockErrorDataDbAdapter();

        $expectedCondition = ' ORDER BY error_data_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //fetchTemplate
    public function testFetchTemplateIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchTemplateByObject'
                                ]
                           )
                           ->getMock();
        
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))
                         ->method('fetchTemplateByObject')
                         ->with($errorData);

        $adapter->fetchTemplate($errorData);
    }

    public function testFetchTemplateIsArray()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchTemplateByList'
                                ]
                           )
                           ->getMock();
                
        $errorDataOne = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(
            $this->faker->randomNumber(1)
        );

        $errorDataTwo = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(
            $this->faker->randomNumber(2)
        );

        $errorDataList[$errorDataOne->getId()] = $errorDataOne;
        $errorDataList[$errorDataTwo->getId()] = $errorDataTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchTemplateByList')
                         ->with($errorDataList);

        $adapter->fetchTemplate($errorDataList);
    }

    //fetchTemplateByObject
    public function testFetchTemplateByObject()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'getRepositoryFactory'
                                ]
                           )
                           ->getMock();

        $id = $templateId = $this->faker->randomNumber();
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData($id);
        $errorData->setCategory(Template::CATEGORY['BJ']);
        $template = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $templateId
        );
        $errorData->setTemplate($template);

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $templateRepository = $this->prophesize(BjTemplateRepository::class);
        $templateRepository->fetchOne(Argument::exact($templateId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($template);
        $repositoryFactory->getRepository(Argument::exact($errorData->getCategory()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($templateRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->fetchTemplateByObject($errorData);
    }

    public function testFetchTemplateByList()
    {
        $adapter = $this->getMockBuilder(MockErrorDataDbAdapter::class)
                           ->setMethods(
                               [
                                    'getRepositoryFactory'
                                ]
                           )
                           ->getMock();

        $id = $templateId = $this->faker->randomNumber();
        $errorDataOne = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData($id);
        $templateOne = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $templateId
        );
        $errorDataOne->setCategory(Template::CATEGORY['BJ']);
        $errorDataOne->setTemplate($templateOne);

        $idTwo = $templateIdTwo = $this->faker->randomNumber(2);
        $errorDataTwo = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData($idTwo);
        $templateTwo = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $templateIdTwo
        );
        $errorDataTwo->setCategory(Template::CATEGORY['BJ']);
        $errorDataTwo->setTemplate($templateTwo);

        $errorDataTwo->setTemplate($templateTwo);
        $templateIds = [$templateId, $templateIdTwo];
        
        $templateList[$templateOne->getId()] = $templateOne;
        $templateList[$templateTwo->getId()] = $templateTwo;
        
        $errorDataList[$errorDataOne->getId()] = $errorDataOne;
        $errorDataList[$errorDataTwo->getId()] = $errorDataTwo;

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $templateRepository = $this->prophesize(BjTemplateRepository::class);
        $templateRepository->fetchList(Argument::exact($templateIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($templateList);
        $repositoryFactory->getRepository(Argument::exact(Template::CATEGORY['BJ']))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($templateRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->fetchTemplateByList($errorDataList);
    }
}

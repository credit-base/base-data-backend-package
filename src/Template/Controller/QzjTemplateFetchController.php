<?php
namespace BaseData\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Template\View\QzjTemplateView;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;

class QzjTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new QzjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IQzjTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new QzjTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'qzjTemplates';
    }
}

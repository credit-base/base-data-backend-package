<?php
namespace BaseData\Common\Controller\Factory;

use BaseData\Common\Controller\NullResubmitController;
use BaseData\Common\Controller\Interfaces\IResubmitAbleController;

class ResubmitControllerFactory
{
    const MAPS = array(
        'unAuditedRules'=>'\BaseData\Rule\Controller\RuleServiceResubmitController'
    );

    public static function getController(string $resource) : IResubmitAbleController
    {
        $resubmitController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($resubmitController) ? new $resubmitController : NullResubmitController::getInstance();
    }
}

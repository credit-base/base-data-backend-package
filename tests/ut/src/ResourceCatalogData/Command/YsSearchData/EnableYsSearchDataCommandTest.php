<?php
namespace BaseData\ResourceCatalogData\Command\YsSearchData;

use PHPUnit\Framework\TestCase;

class EnableYsSearchDataCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new EnableYsSearchDataCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsEnableCommand()
    {
        $this->assertInstanceof('BaseData\Common\Command\EnableCommand', $this->stub);
    }
}

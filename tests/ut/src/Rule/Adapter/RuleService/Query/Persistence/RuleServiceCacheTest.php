<?php
namespace BaseData\Rule\Adapter\RuleService\Query\Persistence;

use PHPUnit\Framework\TestCase;

class RuleServiceCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockRuleServiceCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(RuleServiceCache::KEY, $this->cache->getKey());
    }
}

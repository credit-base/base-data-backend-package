<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\CommandHandler\DisableCommandHandler;

class DisableWbjSearchDataCommandHandler extends DisableCommandHandler
{
    use WbjSearchDataCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchWbjSearchData($id);
    }
}

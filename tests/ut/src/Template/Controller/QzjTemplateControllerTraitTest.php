<?php
namespace BaseData\Template\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;

class QzjTemplateControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockQzjTemplateControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetTemplateWidgetRule()
    {
        $this->assertInstanceOf(
            'BaseData\Template\WidgetRule\TemplateWidgetRule',
            $this->trait->getTemplateWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'BaseData\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Repository\QzjTemplateRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateOperateScenario()
    {
        $controller = $this->getMockBuilder(
            MockQzjTemplateControllerTrait::class
        )->setMethods(['getCommonWidgetRule', 'getTemplateWidgetRule']) ->getMock();

        $qzjTemplate = \BaseData\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(
            Argument::exact($qzjTemplate->getExchangeFrequency()),
            Argument::exact('exchangeFrequency')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->formatNumeric(
            Argument::exact($qzjTemplate->getInfoClassify()),
            Argument::exact('infoClassify')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $commonWidgetRule->formatNumeric(
            Argument::exact($qzjTemplate->getInfoCategory()),
            Argument::exact('infoCategory')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(3))->method(
            'getCommonWidgetRule'
        )->willReturn($commonWidgetRule->reveal());

        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->name(Argument::exact($qzjTemplate->getName()))->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->identify(
            Argument::exact($qzjTemplate->getIdentify())
        )->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->subjectCategory(
            Argument::exact($qzjTemplate->getSubjectCategory())
        )->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->dimension(
            Argument::exact($qzjTemplate->getDimension())
        )->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->description(
            Argument::exact($qzjTemplate->getDescription())
        )->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->items(
            Argument::exact($qzjTemplate->getSubjectCategory()),
            Argument::exact($qzjTemplate->getItems())
        )->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->qzjCategory(
            Argument::exact($qzjTemplate->getCategory())
        )->shouldBeCalledTimes(1)->willReturn(true);
        $templateWidgetRule->qzjSourceUnit(
            Argument::exact($qzjTemplate->getCategory()),
            Argument::exact($qzjTemplate->getSourceUnit()->getId())
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->any())->method(
            'getTemplateWidgetRule'
        )->willReturn($templateWidgetRule->reveal());

        $result = $controller->validateOperateScenarioPublic(
            $qzjTemplate->getName(),
            $qzjTemplate->getIdentify(),
            $qzjTemplate->getSubjectCategory(),
            $qzjTemplate->getDimension(),
            $qzjTemplate->getExchangeFrequency(),
            $qzjTemplate->getInfoClassify(),
            $qzjTemplate->getInfoCategory(),
            $qzjTemplate->getDescription(),
            $qzjTemplate->getItems(),
            $qzjTemplate->getCategory(),
            $qzjTemplate->getSourceUnit()->getId()
        );
        
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Command\BjSearchData;

use PHPUnit\Framework\TestCase;

class DeleteBjSearchDataCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = array(
            'id' => 10,
        );

        $this->command = new DeleteBjSearchDataCommand(
            $this->fakerData['id']
        );
    }

    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}

<?php
namespace BaseData\WorkOrderTask\ParentTask\Search;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\Model\Template;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要查看特定条件工单任务时,在政务网OA中,我可以搜索我指派的工单任务,
 *           通过列表的形式呈现 ,以便于我可以快速查看到我需要的工单任务数据
 * @Scenario: 通过标题搜索
 */
class TitleTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
    }

    /**
     * @Given: 存在一条工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_parent_task' => $this->parentTask()
        ]);
    }

    /**
     * @When: 当我查看工单任务数据列表时
     */
    public function fetchParentTaskList()
    {
        $repository = new ParentTaskRepository();
 
        $filter['title'] = '归集地方性红名单信息';

        list($parentTaskList, $count) = $repository->filter($filter);
        
        unset($count);

        return $parentTaskList;
    }

    /**
     * @Then: 我可以看到标题为"归集地方性红名单信息"的所有工单任务数据
     */
    public function testViewParentTaskList()
    {
        $setParentTaskList = $this->parentTask();

        foreach ($setParentTaskList as $key => $setParentTask) {
            $setParentTaskList[$key]['parent_task_id'] = $key +1;
            $setParentTaskList[$key]['attachment'] = json_decode($setParentTask['attachment'], true);
            $setParentTaskList[$key]['assign_objects'] = json_decode($setParentTask['assign_objects'], true);
        }

        $parentTaskList = $this->fetchParentTaskList();
        $translator = new ParentTaskDbTranslator();
        foreach ($parentTaskList as $parentTask) {
            $parentTaskArray[] = $translator->objectToArray($parentTask);
        }

        $this->assertEquals($parentTaskArray, $setParentTaskList);

        foreach ($parentTaskArray as $parentTask) {
            $this->assertEquals('归集地方性红名单信息', $parentTask['title']);
        }
    }
}

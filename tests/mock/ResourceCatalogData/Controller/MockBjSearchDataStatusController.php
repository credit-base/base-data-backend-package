<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class MockBjSearchDataStatusController extends BjSearchDataStatusController
{
    public function getRepository() : IBjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}

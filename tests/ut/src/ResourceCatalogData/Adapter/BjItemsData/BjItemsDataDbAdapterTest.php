<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\BjItemsData;
use BaseData\ResourceCatalogData\Model\NullBjItemsData;
use BaseData\ResourceCatalogData\Translator\BjItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\BjItemsDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class BjItemsDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(BjItemsDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'fetchOneAction',
                                'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IBjItemsDataAdapter
     */
    public function testImplementsIBjItemsDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 BjItemsDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockBjItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\BjItemsDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 BjItemsDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockBjItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\BjItemsDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockBjItemsDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }
    
    //add
    public function testInsert()
    {
        //初始化
        $expectedBjItemsData = new BjItemsData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedBjItemsData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedBjItemsData);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedBjItemsData = new BjItemsData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedBjItemsData);

        //验证
        $bjItemsData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedBjItemsData, $bjItemsData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $bjItemsDataOne = new BjItemsData(1);
        $bjItemsDataTwo = new BjItemsData(2);

        $ids = [1, 2];

        $expectedBjItemsDataList = [];
        $expectedBjItemsDataList[$bjItemsDataOne->getId()] = $bjItemsDataOne;
        $expectedBjItemsDataList[$bjItemsDataTwo->getId()] = $bjItemsDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedBjItemsDataList);

        //验证
        $bjItemsDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedBjItemsDataList, $bjItemsDataList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockBjItemsDataDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockBjItemsDataDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }
}

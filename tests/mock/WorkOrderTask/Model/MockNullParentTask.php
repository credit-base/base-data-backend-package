<?php
namespace BaseData\WorkOrderTask\Model;

class MockNullParentTask extends NullParentTask
{
    public function publicResourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}

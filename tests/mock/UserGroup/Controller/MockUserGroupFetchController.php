<?php
namespace BaseData\UserGroup\Controller;

use Marmot\Interfaces\IView;

use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class MockUserGroupFetchController extends UserGroupFetchController
{
    public function getRepository() : IUserGroupAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

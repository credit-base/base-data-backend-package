<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask;

use BaseData\WorkOrderTask\Model\ParentTask;

interface IParentTaskAdapter
{
    public function fetchOne($id) : ParentTask;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(ParentTask $parentTask) : bool;

    public function edit(ParentTask $parentTask, array $keys = array()) : bool;
}

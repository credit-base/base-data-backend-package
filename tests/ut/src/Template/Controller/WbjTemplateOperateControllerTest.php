<?php
namespace BaseData\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Command\WbjTemplate\AddWbjTemplateCommand;
use BaseData\Template\Command\WbjTemplate\EditWbjTemplateCommand;

class WbjTemplateOperateControllerTest extends TestCase
{
    private $wbjController;

    public function setUp()
    {
        $this->wbjController = new WbjTemplateOperateController();
    }

    public function tearDown()
    {
        unset($this->wbjController);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->wbjController
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->wbjController
        );
    }

    /**
     * 测试 add 成功
     * 1. 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initAdd(), 期望结果为 true
     * 3. 为 WbjTemplate 类建立预言
     * 4. 为 WbjTemplateRepository 类建立预言, WbjTemplateRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的WbjTemplate, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且wbjController返回结果为 true
     * 6. wbjController->add 方法被调用一次, 且返回结果为 true
     */
    public function testAdd()
    {
        // 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、getRepository、render方法
        $wbjController = $this->getMockBuilder(WbjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 true
        $this->initAdd($wbjController, true);

        // 为 WbjTemplate 类建立预言
        $wbjTemplate  = $this->prophesize(WbjTemplate::class);

        // 为 WbjTemplateRepository 类建立预言, WbjTemplateRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的WbjTemplate, getRepository 方法被调用一次
        $id = 0;
        $repository = $this->prophesize(WbjTemplateRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjTemplate);
        $wbjController->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且wbjController返回结果为 true
        $wbjController->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // wbjController->add 方法被调用一次, 且返回结果为 true
        $result = $wbjController->add();
        $this->assertTrue($result);
    }

    /**
     * 测试 add 失败
     * 1. 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initAdd(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且wbjController返回结果为 false
     * 4. wbjController->add 方法被调用一次, 且返回结果为 false
     */
    public function testAddFail()
    {
        // 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、displayError 方法
        $wbjController = $this->getMockBuilder(WbjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        // 调用 $this->initAdd(), 期望结果为 false
        $this->initAdd($wbjController, false);

        // displayError 方法被调用一次, 且wbjController返回结果为 false
        $wbjController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // wbjController->add 方法被调用一次, 且返回结果为 false
        $result = $wbjController->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 add 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用一次
     * 4. 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用一次
     * 5. 为 CommandBus 类建立预言, 传入 AddWbjTemplateCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initAdd(WbjTemplateOperateController $wbjController, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $wbjController->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->initCommonWidgetRule($attributes, $relationships, $wbjController);
        $this->initTemplateWidgetRule($attributes, $wbjController);

        // 为 CommandBus 类建立预言, 传入 AddWbjTemplateCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddWbjTemplateCommand(
                    $attributes['name'],
                    $attributes['identify'],
                    $attributes['subjectCategory'],
                    $attributes['dimension'],
                    $attributes['exchangeFrequency'],
                    $attributes['infoClassify'],
                    $attributes['infoCategory'],
                    $attributes['description'],
                    $attributes['items'],
                    $relationships['sourceUnit']['data'][0]['id']
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $wbjController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 edit 成功
     * 1. 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 WbjTemplate 类建立预言
     * 4. 为 WbjTemplateRepository 类建立预言, WbjTemplateRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的WbjTemplate, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且wbjController返回结果为 true
     * 6. wbjController->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、getRepository、render方法
        $wbjController = $this->getMockBuilder(WbjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initEdit($wbjController, $id, true);

        // 为 WbjTemplate 类建立预言
        $wbjTemplate = $this->prophesize(WbjTemplate::class);

        // 为 WbjTemplateRepository 类建立预言, WbjTemplateRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的WbjTemplate, getRepository 方法被调用一次
        $repository = $this->prophesize(WbjTemplateRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjTemplate);
        $wbjController->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且wbjController返回结果为 true
        $wbjController->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // wbjController->edit 方法被调用一次, 且返回结果为 true
        $result = $wbjController->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且wbjController返回结果为 false
     * 4. wbjController->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 WbjTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、displayError 方法
        $wbjController = $this->getMockBuilder(WbjTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getTemplateWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($wbjController, $id, false);

        // displayError 方法被调用一次, 且wbjController返回结果为 false
        $wbjController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // wbjController->edit 方法被调用一次, 且返回结果为 false
        $result = $wbjController->edit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditWbjTemplateCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(WbjTemplateOperateController $wbjController, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $wbjController->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->initCommonWidgetRule($attributes, $relationships, $wbjController);
        $this->initTemplateWidgetRule($attributes, $wbjController);

        // 为 CommandBus 类建立预言, 传入 EditWbjTemplateCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditWbjTemplateCommand(
                    $attributes['name'],
                    $attributes['identify'],
                    $attributes['subjectCategory'],
                    $attributes['dimension'],
                    $attributes['exchangeFrequency'],
                    $attributes['infoClassify'],
                    $attributes['infoCategory'],
                    $attributes['description'],
                    $attributes['items'],
                    $relationships['sourceUnit']['data'][0]['id'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $wbjController->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    private function mockRequestData() : array
    {
        $data = array(
            "attributes" => array(
                "name" => '登记信息',    //目录名称
                "identify" => 'DJXX',    //目录标识
                "subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "exchangeFrequency" => 1,    //更新频率
                "infoClassify" => 1,    //信息分类
                "infoCategory" => 1,    //信息类别
                "description" => "目录描述信息",    //目录描述
                "items" => array(
                    array(
                        "name" => '企业名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '企业名称'    //备注
                    ),
                    array(
                        "name" => '认定依据',    //信息项名称
                        "identify" => 'RDYJ',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '认定依据'    //备注
                    )
                )
            ),
            "relationships" => array(
                "sourceUnit" => array( // 来源单位
                    "data" => array(
                        array("type" => "userGroups", "id" => 1)
                    )
                )
            )
        );

        return $data;
    }

    private function initCommonWidgetRule(array $attributes, array $relationships, $wbjController)
    {
        $sourceUnit = $relationships['sourceUnit']['data'][0]['id'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];

        // 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用4次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($sourceUnit), Argument::exact('sourceUnit'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($exchangeFrequency), Argument::exact('exchangeFrequency'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($infoClassify), Argument::exact('infoClassify'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($infoCategory), Argument::exact('infoCategory'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $wbjController->expects($this->exactly(4))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());
    }

    private function initTemplateWidgetRule(array $attributes, $wbjController)
    {
        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $description = $attributes['description'];
        $items = $attributes['items'];

        // 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用6次
        $templateWidgetRule = $this->prophesize(TemplateWidgetRule::class);
        $templateWidgetRule->description(Argument::exact($description))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->name(Argument::exact($name))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->identify(Argument::exact($identify))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->subjectCategory(Argument::exact($subjectCategory))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->dimension(Argument::exact($dimension))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $templateWidgetRule->items(Argument::exact($subjectCategory), Argument::exact($items))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $wbjController->expects($this->exactly(6))
            ->method('getTemplateWidgetRule')
            ->willReturn($templateWidgetRule->reveal());
    }
}

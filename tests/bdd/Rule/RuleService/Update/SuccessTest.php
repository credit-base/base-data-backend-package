<?php
namespace BaseData\Rule\RuleService\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\CompletionRule;
use BaseData\Rule\Model\TransformationRule;
use BaseData\Rule\Repository\RuleServiceRepository;

/**
 * @Feature: 超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则编辑权限,在政务网OA的资源目录规则管理中,可以对设置的规则进行编辑
 *           可以查看使用规则的情况或编辑已经设置好的规则并重新提交,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 正常编辑规则数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_rule' => $this->rule(),
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_gb_template' => $this->gbTemplate()
        ]);
    }

    /**
     * @When: 获取需要编辑的规则数据
     */
    public function fetchRuleService($id)
    {
        $repository = new RuleServiceRepository();

        $ruleService = $repository->fetchOne($id);

        return $ruleService;
    }

    private function rules(RuleService $ruleService) : array
    {
        $transformationRule = new TransformationRule();
        $transformationRule->setCondition(array('ZTMC'=>'ZTMC'));
        $transformationRule->setTransformationTemplate($ruleService->getTransformationTemplate());
        $transformationRule->setSourceTemplate($ruleService->getSourceTemplate());
        
        $completionRule = new CompletionRule();

        $condition = array(
            'TYSHXYDM' => array(
                array('id' => 1, 'base' => array(1), 'item' => 'TYSHXYDM')
            )
        );
        $completionRule->setCondition($condition);
        $completionRule->setTransformationTemplate($ruleService->getTransformationTemplate());
        $completionRule->setSourceTemplate($ruleService->getSourceTemplate());

        $rules = array(
            IRule::RULE_NAME['TRANSFORMATION_RULE'] => $transformationRule,
            IRule::RULE_NAME['COMPLETION_RULE'] => $completionRule
        );

        return $rules;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $ruleService = $this->fetchRuleService(1);

        $rules = $this->rules($ruleService);

        $ruleService->setRules($rules);
        
        return $ruleService->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $ruleService = $this->fetchRuleService(1);

        $gbTemplate = $this->gbTemplate()[0];

        $transformationRule = new TransformationRule();
        $transformationRule->setCondition(array('ZTMC'=>'ZTMC'));
        $transformationRule->getTransformationTemplate()->setId($ruleService->getTransformationTemplate()->getId());
        $transformationRule->getSourceTemplate()->setId($ruleService->getSourceTemplate()->getId());
        
        $completionRule = new CompletionRule();

        $condition = array(
            'TYSHXYDM' => array(
                array(
                    'id' => 1,
                    'name'=> $gbTemplate['name'],
                    'base' => array(1),
                    'item' => 'TYSHXYDM',
                    'itemName' => '统一社会信用代码'
                )
            )
        );
        $completionRule->setCondition($condition);
        $completionRule->getTransformationTemplate()->setId($ruleService->getTransformationTemplate()->getId());
        $completionRule->getSourceTemplate()->setId($ruleService->getSourceTemplate()->getId());

        $rules = array(
            IRule::RULE_NAME['TRANSFORMATION_RULE'] => $transformationRule,
            IRule::RULE_NAME['COMPLETION_RULE'] => $completionRule
        );
        
        $this->assertEquals($rules, $ruleService->getRules());
    }
}

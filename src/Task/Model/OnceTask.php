<?php
namespace BaseData\Task\Model;

use Marmot\Core;

abstract class OnceTask extends Task
{
    public function __construct()
    {
        parent::__construct();
        $this->type = Task::TYPE['ONCE'];
    }

    public function close() : bool
    {
        $this->setStatus(Task::STATUS['SUCCESS']);
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'statusTime'
            )
        );
    }

    protected function afterExecute() : bool
    {
        return $this->close();
    }
}

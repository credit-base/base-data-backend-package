<?php
namespace BaseData\Statistical\Controller;

use PHPUnit\Framework\TestCase;

class IAdapterFactoryTest extends TestCase
{
    public function testStaticsServiceAuthenticationCount()
    {
        $adapter = new IAdapterFactory();
        $this->assertInstanceOf(
            'BaseData\Statistical\Adapter\Statistical\NullStatisticalAdapter',
            $adapter->getAdapter('test')
        );
    }
}

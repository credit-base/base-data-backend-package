<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class WbjItemsDataCache extends Cache
{
    const KEY = 'wbj_items_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

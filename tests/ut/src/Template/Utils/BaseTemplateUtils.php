<?php
namespace BaseData\Template\Utils;

trait BaseTemplateUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $baseTemplate
    ) {
        $this->assertEquals($expectedArray['base_template_id'], $baseTemplate->getId());
        $this->assertEquals($expectedArray['name'], $baseTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $baseTemplate->getIdentify());

        $subjectCategory = is_string($expectedArray['subject_category']) ?
        json_decode($expectedArray['subject_category'], true) :
        $expectedArray['subject_category'];
        $this->assertEquals($subjectCategory, $baseTemplate->getSubjectCategory());

        $this->assertEquals($expectedArray['dimension'], $baseTemplate->getDimension());
        $this->assertEquals($expectedArray['exchange_frequency'], $baseTemplate->getExchangeFrequency());
        $this->assertEquals($expectedArray['info_classify'], $baseTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $baseTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['description'], $baseTemplate->getDescription());

        $items = is_string($expectedArray['items']) ?
        json_decode($expectedArray['items'], true) :
        $expectedArray['items'];
        $this->assertEquals($items, $baseTemplate->getItems());

        $this->assertEquals($expectedArray['status'], $baseTemplate->getStatus());
        $this->assertEquals($expectedArray['create_time'], $baseTemplate->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $baseTemplate->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $baseTemplate->getStatusTime());
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class MockBjSearchData extends BjSearchData
{
    public function getRepository() : IBjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getBjItemsDataRepository() : IBjItemsDataAdapter
    {
        return parent::getBjItemsDataRepository();
    }

    public function addItemsData() : bool
    {
        return parent::addItemsData();
    }

    public function isTemplateNull() : bool
    {
        return parent::isTemplateNull();
    }

    public function isSubjectCategoryExist() : bool
    {
        return parent::isSubjectCategoryExist();
    }

    public function assignment()
    {
        return parent::assignment();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }
}

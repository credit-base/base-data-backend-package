<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;
use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class ErrorData extends SearchData
{
    const ERROR_TYPE = array(
        'MISSING_DATA' => 1,//数据缺失
        'COMPARISON_FAILED' => 2,//比对失败
        'DUPLICATION_DATA' => 4,//数据重复
        'FORMAT_VALIDATION_FAILED' => 8,//数据格式错误
    );

    const STATUS = array(
        'NORMAL' => 0, //正常
        'PROGRAM_EXCEPTION' => 1, //程序异常
        'STORAGE_EXCEPTION' => 2, //入库异常
    );

    const ERROR_REASON = array(
        self::ERROR_TYPE['MISSING_DATA'] => '数据缺失',
        self::ERROR_TYPE['COMPARISON_FAILED'] => '比对失败',
        self::ERROR_TYPE['DUPLICATION_DATA'] => '数据重复',
        self::ERROR_TYPE['FORMAT_VALIDATION_FAILED'] => '数据格式错误'
    );
     /**
     * @var int $category 资源目录类型
     */
    protected $category;
    
     /**
     * @var int $errorType 失败类型
     */
    protected $errorType;

     /**
     * @var array $errorReason 失败原因
     */
    protected $errorReason;

    protected $repository;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->category = 0;
        $this->template = new Template();
        $this->itemsData = new ErrorItemsData();
        $this->errorType = 0;
        $this->errorReason = array();
        $this->status = self::STATUS['NORMAL'];
        $this->repository = new ErrorDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->category);
        unset($this->template);
        unset($this->itemsData);
        unset($this->errorType);
        unset($this->errorReason);
        unset($this->status);
        unset($this->repository);
    }

    public function setCategory(int $category) : void
    {
        $this->category = $category;
    }

    public function getCategory() : int
    {
        return $this->category;
    }

    public function setItemsData(ErrorItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : ErrorItemsData
    {
        return $this->itemsData;
    }

    public function setErrorType(int $errorType) : void
    {
        $this->errorType = $errorType;
    }

    public function getErrorType() : int
    {
        return $this->errorType;
    }

    public function setErrorReason(array $errorReason) : void
    {
        $this->errorReason = $errorReason;
    }

    public function getErrorReason() : array
    {
        return $this->errorReason;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : IErrorDataAdapter
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }
}

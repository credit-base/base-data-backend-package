<?php
namespace BaseData\Rule\Model;

use Marmot\Interfaces\INull;

use BaseData\Common\Model\NullOperateTrait;
use BaseData\Common\Model\NullApproveAbleTrait;
use BaseData\Common\Model\NullResubmitAbleTrait;

class NullUnAuditedRuleService extends UnAuditedRuleService implements INull
{
    use NullOperateTrait,
        NullApproveAbleTrait,
        NullResubmitAbleTrait;

    private static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function apply() : bool
    {
        return $this->resourceNotExist();
    }

    protected function approveApplyInfo(int $operationType) : bool
    {
        unset($operationType);
        return $this->resourceNotExist();
    }

    protected function updateApplyStatus(int $applyStatus) : bool
    {
        unset($applyStatus);
        return $this->resourceNotExist();
    }

    protected function updateRelationId() : bool
    {
        return $this->resourceNotExist();
    }

    public function revoke() : bool
    {
        return $this->resourceNotExist();
    }
}

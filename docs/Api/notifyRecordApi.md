# 上传记录接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")

## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| name           | string     |             | XXZK_10_1_C4CA4238A0B923820DCC509A6F75849B.xlsx | 文件名字        |
| hash 			  | string 		| 			  | C4CA4238A0B923820DCC509A6F75849B | 哈希 |
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[notifyRecords]=name
	    1.2 fields[notifyRecords]=hash
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'notifyRecords/1?fields[notifyRecords]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/notifyRecords/{id:\d+}

示例

	$response = $client->request('GET', 'notifyRecords/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/notifyRecords/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'notifyRecords/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/notifyRecords

	1、检索条件
	    1.1 filter[hash] | 根据哈希查询上传信用目录数据
示例

	$response = $client->request('GET', 'notifyRecords?filter[hash]=hash',['headers'=>['Content-' => 'application/vnd.api+json']]);


### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

```php
{
    "meta": [],
    "data": {
        "type": "notifyRecords",
        "id": "2",
        "attributes": {
            "name": "XZXK_10_1_hash1.xlsx",
            "hash": "hash1",
            "status": 0,
            "createTime": 1627372869,
            "updateTime": 1627372869,
            "statusTime": 0
        },
        "links": {
            "self": "127.0.0.1:8088\/notifyRecords\/2"
        }
    }
}
```
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

```php
{
    "meta": {
        "count": 3,
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        }
    },
    "links": {
        "first": null,
        "last": null,
        "prev": null,
        "next": null
    },
    "data": [
        {
            "type": "notifyRecords",
            "id": "2",
            "attributes": {
                "name": "XZXK_10_1_hash1.xlsx",
                "hash": "hash1",
                "status": 0,
                "createTime": 1627372869,
                "updateTime": 1627372869,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8088\/notifyRecords\/2"
            }
        },
        {
            "type": "notifyRecords",
            "id": "3",
            "attributes": {
                "name": "XZXK_10_2_hash2.xlsx",
                "hash": "hash2",
                "status": 0,
                "createTime": 1627372905,
                "updateTime": 1627372905,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8088\/notifyRecords\/3"
            }
        },
        {
            "type": "notifyRecords",
            "id": "4",
            "attributes": {
                "name": "XZXK_10_2_hash3.xlsx",
                "hash": "hash3",
                "status": 0,
                "createTime": 1627373329,
                "updateTime": 1627373329,
                "statusTime": 0
            },
            "links": {
                "self": "127.0.0.1:8088\/notifyRecords\/4"
            }
        }
    ]
}
```
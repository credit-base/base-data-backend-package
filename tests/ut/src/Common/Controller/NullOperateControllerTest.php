<?php
namespace BaseData\Common\Controller;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullOperateControllerTest extends TestCase
{
    private $controller;
    
    public function setUp()
    {
        $this->controller = new NullOperateController();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->controller);
    }

    public function testImplementIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->controller
        );
    }

    public function testAdd()
    {
        $this->controller->add();
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }

    public function testEdit()
    {
        $this->controller->edit(1);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }
}

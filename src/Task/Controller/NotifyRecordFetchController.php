<?php
namespace BaseData\Task\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Task\View\NotifyRecordView;
use BaseData\Task\Repository\NotifyRecordRepository;
use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;

class NotifyRecordFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new NotifyRecordRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : INotifyRecordAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new NotifyRecordView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'notifyRecords';
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class YsSearchDataCache extends Cache
{
    const KEY = 'ys_search_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

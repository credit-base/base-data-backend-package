# 新闻审核管理接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [重新编辑](#重新编辑)
	* [审核通过](#审核通过)
	* [审核驳回](#审核驳回)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [员工项目字典](../Dictionary/crew.md "员工项目字典")
	* [委办局项目字典](../Dictionary/UserGroup.md "委办局项目字典")
	* [新闻项目字典](../Dictionary/news.md "新闻项目字典")
	* [新闻审核项目字典](../Dictionary/unAuditedNews.md "新闻审核项目字典")
	* [新闻分类对应关系定义](../About/newsCategoryDefine.md "新闻分类对应关系定义")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [新闻控件规范](../WidgetRule/news.md "新闻控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	101=>array(
		'title'=>标题格式不正确
		'source'=>来源格式不正确
		'cover'=>封面格式不正确
		'attachments'=>附件格式不正确
		'content'=>内容格式不正确
		'status'=>状态格式不正确
		'stick'=>置顶状态格式不正确
		'newsType'=>新闻类型格式不正确
		'dimension'=>数据纬度格式不正确
		'homePageShowStatus'=>首页展示状态格式不正确
		'bannerStatus'=>轮播状态格式不正确
		'bannerImage'=>轮播图图片格式不正确
		'crewId'=>发布人格式不正确
		'applyCrewId'=>审核人格式不正确
		'rejectReason'=>驳回原因格式不正确
	),
    ```
## <a name="参数说明">参数说明</a>
     
| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 中央应对新型冠状病毒感染肺炎疫情工作领导小组关于全面落实进一步保护关心爱护医务人员若干措施的通知 | 新闻标题        |
| source          | string     | 是            | 中国政务网                                       | 新闻来源          |
| content         | string     | 是            | 新闻内容                                         | 新闻内容          |
| description     | string     | 是            | 新闻描述                                         | 新闻描述          |
| parentCategory  | int        | 是            | 1                                               | 新闻父级分类      |
| category        | int        | 是            | 1                                               | 新闻分类         |
| newsType        | int        | 是            | 1                                               | 新闻类型         |
| dimension       | int        | 是            | 1                                               | 数据共享维度(1 社会公开 2 政务共享 3 授权查询)|
| cover           | array      | 否            | array('name'=>'封面名称', 'identify'=>'封面地址.jpg') | 新闻封面      |
| attachments     | array      | 否            | array(array('name'=>'附件名称', 'identify'=>'附件地址.doc')) | 新闻附件      |
| crew            | int        | 是            | Crew                                             | 发布人           |
| publishUserGroup| int        | 是            | UserGroup                                           | 发布委办局          |
| bannerStatus    | int        | 否            | 0                                            | 轮播状态(默认 0不轮播 2轮播)|
| bannerImage     | array      | 否            | array('name'=>'轮播图图片名称', 'identify'=>'轮播图图片地址.jpg') | 轮播图图片      |
| homePageShowStatus| int      | 否            | 0                                            | 首页展示状态(默认 0不展示 2展示)|
| status          | int        | 是            | 0                                            | 状态(0启用 -2禁用)|
| stick           | int        | 是            | 0                                            | 置顶状态(默认 0不置顶 2置顶)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |
| applyStatus     | int        | 是            | 0                                            | 审核状态(默认 0待审核 2审核通过 -2 审核驳回)|
| rejectReason    | string     | 是            | 内容不合理                                     | 驳回原因          |
| operationType   | int        | 是            | 0                                            | 操作类型(1 添加 2 编辑 3 启用 4 禁用 5 置顶 6 取消置顶 7 移动)|
| applyInfoCategory| int        | 是            | 0                                           | 可以申请的信息分类(1 新闻 )|
| applyInfoType    | int        | 是            | 0                                           | 可以申请的最小信息分类(新闻类型)|
| applyCrew        | int        | 是            | Crew                                             | 审核人           |
| applyUserGroup   | int        | 是            | UserGroup                                           | 审核委办局          |
| relation         | int        | 是            | Crew                                           | 修改人          |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[crews]
	    1.2 fields[userGroups]
	    1.3 fields[unAuditedNews]
		1.4 include = crew,publishUserGroup,applyUserGroup,applyCrew,relation
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'unAuditedNews/1?fields[unAuditedNews]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/unAuditedNews/{id:\d+}

示例

	$response = $client->request('GET', 'unAuditedNews/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/unAuditedNews/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'unAuditedNews/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/unAuditedNews

	1、检索条件
	    1.1 filter[applyUserGroup] | 根据审核委办局搜索,委办局id
	    1.2 filter[applyInfoCategory] | 根据可以申请的信息分类搜索
	    1.3 filter[applyInfoType] | 可以申请的最小信息分类,此处为新闻类型
	    1.4 filter[relation] | 根据关联信息搜索,此处为员工id
	    1.5 filter[title] | 根据新闻标题搜索
	    1.6 filter[applyStatus] | 根据审核状态搜索 | 0 待审核 | -2 审核驳回 | 2 审核通过
	    1.7 filter[operationType] | 根据操作类型搜索

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-applyStatus | -applyStatus 根据审核状态倒序 | 审核状态 根据审核状态正序

示例

	$response = $client->request('GET', 'unAuditedNews?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="重新编辑">重新编辑</a>
	
路由

	通过PATCH传参
	/unAuditedNews/{id:\d+}/resubmit

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedNews",
			"attributes"=>array(
				"title"=>"标题",
				"source"=>"来源",
				"cover"=>array('name' => '封面名称', 'identify' => '封面地址.jpg'),
				"attachments"=>array(
					array('name' => 'name', 'identify' => 'identify'),
					array('name' => 'name', 'identify' => 'identify'),
					array('name' => 'name', 'identify' => 'identify')
				),
				"content"=>"内容",
				"newsType"=>新闻类型,
				"dimension"=>数据共享维度,
				"status"=>状态,
				"stick"=>置顶状态,
				"bannerStatus"=>轮播状态,
				"bannerImage"=>array('name' => '轮播图图片名称', 'identify' => '轮播图图片地址.jpg'),
				"homePageShowStatus"=>首页展示状态,
			),
			"relationships"=>array(
				"crew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedNews/1/resubmit',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核通过">审核通过</a>

路由

	通过PATCH传参
	/unAuditedNews/{id:\d+}/approve

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedNews",
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedNews/1/approve',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="审核驳回">审核驳回</a>

路由

	通过PATCH传参
	/unAuditedNews/{id:\d+}/reject

示例

	$data = array(
		'data' => array(
			"type"=>"unAuditedNews",
			"attributes"=>array(
				"rejectReason"=>"审核驳回原因"
			),
			"relationships"=>array(
				"applyCrew"=>array(
					"data"=>array(
						array("type"=>"crews","id"=>发布人id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'unAuditedNews/1/reject',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "unAuditedNews",
			"id": "13",
			"attributes": {
				"applyInfoCategory": 1,
				"applyInfoType": 2,
				"applyStatus": 2,
				"rejectReason": "",
				"operationType": 1,
				"newsId": 0,
				"title": "测试新闻标题1",
				"source": "测试新闻来源1",
				"cover": {
					"name": "测试封面名称1",
					"identify": "测试封面地址1.jpg"
				},
				"attachments": [
					{
						"name": "测试name1",
						"identify": "测试identify1.doc"
					}
				],
				"content": "测试内容1",
				"description": "测试内容1",
				"parentCategory": 0,
				"category": 1,
				"newsType": 2,
				"dimension": 2,
				"bannerImage": {
					"name": "测试轮播图图片名称1",
					"identify": "测试轮播图图片地址1.jpg"
				},
				"bannerStatus": 2,
				"homePageShowStatus": 2,
				"stick": 2,
				"status": -2,
				"createTime": 1620831782,
				"updateTime": 1620831782,
				"statusTime": 0
			},
			"relationships": {
				"relation": {
					"data": {
						"type": "crews",
						"id": "3"
					}
				},
				"applyCrew": {
					"data": {
						"type": "crews",
						"id": "0"
					}
				},
				"applyUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "0"
					}
				},
				"publishUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "0"
					}
				},
				"crew": {
					"data": {
						"type": "crews",
						"id": "3"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/unAuditedNews/13"
			}
		},
		"included": [
			{
				"type": "crews",
				"id": "3",
				"attributes": {
					"realName": "李玟雨",
					"cardId": "412825199009094567",
					"userName": "18800000002",
					"cellphone": "18800000002",
					"category": 2,
					"purview": [
						"1",
						"2",
						"3",
						"4",
						"5",
						"6",
						"7",
						"8"
					],
					"status": 0,
					"createTime": 1619576124,
					"updateTime": 1619576124,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "0"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "0"
						}
					}
				}
			},
			{
				"type": "crews",
				"id": "0",
				"attributes": {
					"realName": "",
					"cardId": "",
					"userName": "",
					"cellphone": "",
					"category": 4,
					"purview": [],
					"status": 0,
					"createTime": 1620964775,
					"updateTime": 1620964775,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "0"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "0"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "0",
				"attributes": {
					"name": "",
					"shortName": "",
					"status": 0,
					"createTime": 1620964775,
					"updateTime": 1620964775,
					"statusTime": 0
				}
			}
		]
	}
	
#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 9,
			"links": {
				"first": 1,
				"last": 5,
				"prev": null,
				"next": 2
			}
		},
		"links": {
			"first": "127.0.0.1:8089/unAuditedNews/?include=relation,applyUserGroup,applyCrew,crew,publishUserGroup&filter[applyStatus]=0,-2&page[number]=1&page[size]=2",
			"last": "127.0.0.1:8089/unAuditedNews/?include=relation,applyUserGroup,applyCrew,crew,publishUserGroup&filter[applyStatus]=0,-2&page[number]=5&page[size]=2",
			"prev": null,
			"next": "127.0.0.1:8089/unAuditedNews/?include=relation,applyUserGroup,applyCrew,crew,publishUserGroup&filter[applyStatus]=0,-2&page[number]=2&page[size]=2"
		},
		"data": [
			{
				"type": "unAuditedNews",
				"id": "1",
				"attributes": {
					"applyInfoCategory": 1,
					"applyInfoType": 1,
					"applyStatus": 0,
					"rejectReason": "审核驳回原因",
					"operationType": 1,
					"newsId": 0,
					"title": "新闻标题测试",
					"source": "来源",
					"cover": [],
					"attachments": [
						{
							"name": "name",
							"identify": "identify.doc"
						}
					],
					"content": "内容",
					"description": "内容",
					"parentCategory": 0,
					"category": 1,
					"newsType": 1,
					"dimension": 1,
					"bannerImage": {
						"name": "轮播图图片名称",
						"identify": "轮播图图片地址.jpg"
					},
					"bannerStatus": 0,
					"homePageShowStatus": 0,
					"stick": 0,
					"status": 0,
					"createTime": 1620821849,
					"updateTime": 1620821849,
					"statusTime": 0
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "2"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedNews/1"
				}
			},
			{
				"type": "unAuditedNews",
				"id": "2",
				"attributes": {
					"applyInfoCategory": 1,
					"applyInfoType": 1,
					"applyStatus": 0,
					"rejectReason": "审核驳回原因",
					"operationType": 1,
					"newsId": 0,
					"title": "新闻标题测试",
					"source": "来源",
					"cover": [],
					"attachments": [
						{
							"name": "name",
							"identify": "identify.doc"
						}
					],
					"content": "内容",
					"description": "内容",
					"parentCategory": 0,
					"category": 1,
					"newsType": 1,
					"dimension": 1,
					"bannerImage": {
						"name": "轮播图图片名称",
						"identify": "轮播图图片地址.jpg"
					},
					"bannerStatus": 0,
					"homePageShowStatus": 0,
					"stick": 0,
					"status": 0,
					"createTime": 1620821904,
					"updateTime": 1620821904,
					"statusTime": 0
				},
				"relationships": {
					"relation": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					},
					"applyCrew": {
						"data": {
							"type": "crews",
							"id": "2"
						}
					},
					"applyUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"publishUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/unAuditedNews/2"
				}
			}
		],
		"included": [
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "crews",
				"id": "2",
				"attributes": {
					"realName": "王文",
					"cardId": "412825199009094533",
					"userName": "18800000001",
					"cellphone": "18800000001",
					"category": 3,
					"purview": [],
					"status": -2,
					"createTime": 1618284059,
					"updateTime": 1619578659,
					"statusTime": 1619578659
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "1"
						}
					}
				}
			},
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			}
		]
	}
<?php
namespace BaseData\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\Model\BaseTemplate;
use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class BaseTemplateOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new BaseTemplateOperateController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->controller
        );
    }

    public function testAdd()
    {
        $result = $this->controller->add();
        $this->assertFalse($result);
    }

    /**
     * 初始化 edit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 TemplateWidgetRule 类建立预言, 验证请求参数, getTemplateWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 EditBaseTemplateCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEdit(BaseTemplateOperateController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);

        $request->patch(Argument::exact('data'))->shouldBeCalledTimes(1)->willReturn($data);

        $controller->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('validateOperateScenario')->willReturn(true);


        // 为 CommandBus 类建立预言, 传入 EditBaseTemplateCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditBaseTemplateCommand(
                    $attributes['subjectCategory'],
                    $attributes['items'],
                    $attributes['dimension'],
                    $attributes['exchangeFrequency'],
                    $attributes['infoClassify'],
                    $attributes['infoCategory'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    /**
     * 测试 edit 成功
     * 1. 为 BaseTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、getRepository、render方法
     * 2. 调用 $this->initEdit(), 期望结果为 true
     * 3. 为 BaseTemplate 类建立预言
     * 4. 为 BaseTemplateRepository 类建立预言, BaseTemplateRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的BaseTemplate, getRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->edit 方法被调用一次, 且返回结果为 true
     */
    public function testEdit()
    {
        // 为 BaseTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(BaseTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateOperateScenario',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;

        $this->initEdit($controller, $id, true);

        // 为 BaseTemplate 类建立预言
        $baseTemplate = $this->prophesize(BaseTemplate::class);

        // 为 BaseTemplateRepository 类建立预言, BaseTemplateRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的BaseTemplate, getRepository 方法被调用一次
        $repository = $this->prophesize(BaseTemplateRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($baseTemplate);
        $controller->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))->method('render')->willReturn(true);

        // controller->edit 方法被调用一次, 且返回结果为 true
        $result = $controller->edit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 edit 失败
     * 1. 为 BaseTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getTemplateWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initEdit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->edit 方法被调用一次, 且返回结果为 false
     */
    public function testEditFail()
    {
        // 为 BaseTemplateOperateController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getTemplateWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(BaseTemplateOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateOperateScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEdit(), 期望结果为 false
        $this->initEdit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        // controller->edit 方法被调用一次, 且返回结果为 false
        $result = $controller->edit($id);
        $this->assertFalse($result);
    }

    private function mockRequestData() : array
    {
        return array(
            "attributes" => array(
                "name" => '企业基本信息',    //目录名称
                "identify" => 'QYJBXX',    //目录标识
                "subjectCategory" => array(1),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "exchangeFrequency" => 1,    //更新频率
                "infoClassify" => 1,    //信息分类
                "infoCategory" => 1,    //信息类别
                "description" => "企业基本信息",    //目录描述
                "items" => array(
                    array(
                        "name" => '主体名称',    //信息项名称
                        "identify" => 'ZTMC',    //数据标识
                        "type" => 1,    //数据类型
                        "length" => '200',    //数据长度
                        "options" => array(),    //可选范围
                        "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                        "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                        "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                        "maskRule" => array(),    //脱敏规则
                        "remarks" => '信用主体名称',    //备注
                    )
                )
            )
        );
    }
}

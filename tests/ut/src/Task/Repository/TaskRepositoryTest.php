<?php
namespace BaseData\Task\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Task\Model\MockTask;
use BaseData\Task\Adapter\Task\ITaskAdapter;
use BaseData\Task\Adapter\Task\TaskDbAdapter;

use BaseData\Task\Utils\MockFactory;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class TaskRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(TaskRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockTaskRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new TaskDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\Task\ITaskAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\Task\TaskDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\Task\ITaskAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\Task\TaskMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testSchedule()
    {
        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->schedule()->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->schedule();
    }

    public function testExpire()
    {
        $list = ['list'];

        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->expire()->shouldBeCalledTimes(1)->willReturn($list);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $result = $this->repository->expire();
        $this->assertEquals($list, $result);
    }

    public function testAdd()
    {
        $id = 1;
        $task = new MockTask($id);
        $keys = array();
        
        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->add(Argument::exact($task))
                ->shouldBeCalledTimes(1)
                ->willReturn(true);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $result = $this->repository->add($task, $keys);
        $this->assertTrue($result);
    }

    public function testEdit()
    {
        $id = 1;
        $task = new MockTask($id);
        $keys = array();
        
        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->edit(Argument::exact($task), Argument::exact($keys))
                ->shouldBeCalledTimes(1)
                ->willReturn(true);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $result = $this->repository->edit($task, $keys);
        $this->assertTrue($result);
    }

    public function testNotifySubTasks()
    {
        $task = new MockTask();
        
        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->notifySubTasks(Argument::exact($task))->shouldBeCalledTimes(1)->willReturn(true);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $result = $this->repository->notifySubTasks($task);
        $this->assertTrue($result);
    }
}

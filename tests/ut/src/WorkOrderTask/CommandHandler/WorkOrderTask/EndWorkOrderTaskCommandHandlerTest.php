<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;

class EndWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EndWorkOrderTaskCommandHandler::class)
                                     ->setMethods(['getWorkOrderTaskRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetWorkOrderTaskRepository()
    {
        $commandHandler = new MockEndWorkOrderTaskCommandHandler();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository',
            $commandHandler->getWorkOrderTaskRepository()
        );
    }

    public function testExecute()
    {
        $command = new EndWorkOrderTaskCommand(
            $this->faker->word,
            $this->faker->randomNumber()
        );

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->end(Argument::exact($command->reason))->shouldBeCalledTimes(1)->willReturn(true);

        $workOrderTaskRepository = $this->prophesize(WorkOrderTaskRepository::class);
        $workOrderTaskRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($workOrderTask->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getWorkOrderTaskRepository')
            ->willReturn($workOrderTaskRepository->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}

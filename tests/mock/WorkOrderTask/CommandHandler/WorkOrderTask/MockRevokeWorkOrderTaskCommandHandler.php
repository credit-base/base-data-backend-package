<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockRevokeWorkOrderTaskCommandHandler extends RevokeWorkOrderTaskCommandHandler
{
    public function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return parent::getWorkOrderTaskRepository();
    }
}

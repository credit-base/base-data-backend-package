<?php
namespace BaseData\Common\Repository;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullRepositoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullRepository::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testGetActualAdapter()
    {
        $result = $this->stub->getActualAdapter();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetMockAdapter()
    {
        $result = $this->stub->getMockAdapter();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetFetchOne()
    {
        $result = $this->stub->fetchOne(1);
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetFetchList()
    {
        $result = $this->stub->fetchList(array(1));
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetFilter()
    {
        $result = $this->stub->filter();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}

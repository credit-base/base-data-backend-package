<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\Persistence;

class MockBjItemsDataDb extends BjItemsDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

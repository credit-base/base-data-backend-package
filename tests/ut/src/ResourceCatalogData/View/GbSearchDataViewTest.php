<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\GbSearchData;

class GbSearchDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $gbSearchData = new GbSearchDataView(new GbSearchData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $gbSearchData);
    }
}

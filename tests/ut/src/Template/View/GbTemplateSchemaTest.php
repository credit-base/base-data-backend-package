<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class GbTemplateSchemaTest extends TestCase
{
    private $gbTemplateSchema;

    private $gbTemplate;

    public function setUp()
    {
        $this->gbTemplateSchema = new GbTemplateSchema(new Factory());

        $this->gbTemplate = \BaseData\Template\Utils\GbTemplateMockFactory::generateGbTemplate(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->gbTemplateSchema);
        unset($this->gbTemplate);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->gbTemplateSchema);
    }

    public function testGetId()
    {
        $result = $this->gbTemplateSchema->getId($this->gbTemplate);

        $this->assertEquals($result, $this->gbTemplate->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->gbTemplateSchema->getAttributes($this->gbTemplate);

        $this->assertEquals($result['name'], $this->gbTemplate->getName());
        $this->assertEquals($result['identify'], $this->gbTemplate->getIdentify());
        $this->assertEquals($result['subjectCategory'], $this->gbTemplate->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->gbTemplate->getDimension());
        $this->assertEquals($result['exchangeFrequency'], $this->gbTemplate->getExchangeFrequency());
        $this->assertEquals($result['infoClassify'], $this->gbTemplate->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->gbTemplate->getInfoCategory());
        $this->assertEquals($result['description'], $this->gbTemplate->getDescription());
        $this->assertEquals($result['category'], $this->gbTemplate->getCategory());
        $this->assertEquals($result['items'], $this->gbTemplate->getItems());
        $this->assertEquals($result['status'], $this->gbTemplate->getStatus());
        $this->assertEquals($result['createTime'], $this->gbTemplate->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->gbTemplate->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->gbTemplate->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->gbTemplateSchema->getRelationships($this->gbTemplate, 0, array());

        $this->assertEquals($result, array());
    }
}

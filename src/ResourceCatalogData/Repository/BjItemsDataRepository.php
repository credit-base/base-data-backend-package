<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\BjItemsData;
use BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\BjItemsData\BjItemsDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\BjItemsData\BjItemsDataMockAdapter;

class BjItemsDataRepository extends Repository implements IBjItemsDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BjItemsDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IBjItemsDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IBjItemsDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IBjItemsDataAdapter
    {
        return new BjItemsDataMockAdapter();
    }

    public function fetchOne($id) : BjItemsData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(BjItemsData $bjItemsData) : bool
    {
        return $this->getAdapter()->add($bjItemsData);
    }
}

<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockFeedbackWorkOrderTaskCommandHandler extends FeedbackWorkOrderTaskCommandHandler
{
    public function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return parent::getWorkOrderTaskRepository();
    }
}

<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query;

use Marmot\Framework\Query\RowCacheQuery;

class UnAuditedRuleServiceRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'apply_form_rule_id',
            new Persistence\UnAuditedRuleServiceCache(),
            new Persistence\UnAuditedRuleServiceDb()
        );
    }
}

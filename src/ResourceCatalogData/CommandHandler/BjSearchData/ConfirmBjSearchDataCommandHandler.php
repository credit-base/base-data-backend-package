<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\ResourceCatalogData\Command\BjSearchData\ConfirmBjSearchDataCommand;

class ConfirmBjSearchDataCommandHandler implements ICommandHandler
{
    use BjSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof ConfirmBjSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }

        $bjSearchData = $this->fetchBjSearchData($command->id);

        return $bjSearchData->confirm();
    }
}

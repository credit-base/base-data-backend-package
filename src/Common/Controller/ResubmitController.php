<?php
namespace BaseData\Common\Controller;

use Marmot\Framework\Classes\Controller;

use BaseData\Common\Controller\Factory\ResubmitControllerFactory;
use BaseData\Common\Controller\Interfaces\IResubmitAbleController;

class ResubmitController extends Controller
{
    protected function getController(string $resource) : IResubmitAbleController
    {
        return ResubmitControllerFactory::getController($resource);
    }

    public function resubmit(string $resource, int $id)
    {
        $controller = $this->getController($resource);

        return $controller->resubmit($id);
    }
}

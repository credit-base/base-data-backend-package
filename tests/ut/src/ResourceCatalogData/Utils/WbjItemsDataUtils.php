<?php
namespace BaseData\ResourceCatalogData\Utils;

trait WbjItemsDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $wbjItemsData
    ) {
        $this->assertEquals($expectedArray['wbj_items_data_id'], $wbjItemsData->getId());

        $data = array();
        if (is_string($expectedArray['data'])) {
            $data = unserialize(gzuncompress(base64_decode($expectedArray['data'], true)));
        }
        if (is_array($expectedArray['data'])) {
            $data = $expectedArray['data'];
        }

        $this->assertEquals($data, $wbjItemsData->getData());
    }
}

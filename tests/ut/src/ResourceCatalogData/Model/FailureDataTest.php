<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class FailureDataTest extends TestCase
{
    public function testCorrectExtendsErrorData()
    {
        $model = new FailureData();
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\ErrorData', $model);
    }
}

<?php
namespace BaseData\Rule\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Utils\MockFactory;
use BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter;
use BaseData\Rule\Adapter\RuleService\RuleServiceDbAdapter;

class RuleServiceRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(RuleServiceRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockRuleServiceRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new RuleServiceDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\RuleService\RuleServiceDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\RuleService\RuleServiceMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $ruleService = MockFactory::generateRuleService($id);
        $keys = array();
        
        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->add(Argument::exact($ruleService))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($ruleService, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $ruleService = MockFactory::generateRuleService($id);
        $keys = array();
        
        $adapter = $this->prophesize(IRuleServiceAdapter::class);
        $adapter->edit(Argument::exact($ruleService), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($ruleService, $keys);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter;

class MockGbItemsDataRepository extends GbItemsDataRepository
{
    public function getAdapter() : IGbItemsDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IGbItemsDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IGbItemsDataAdapter
    {
        return parent::getMockAdapter();
    }
}

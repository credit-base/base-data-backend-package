<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要将待确认的资源目录数据确认时,在资源目录数据子系统下的国标数据管理,确认对应的资源目录数据
 *           根据我核实需要确认的数据确认,以便于我能更好的实现数据的报送和平台的呈现
 * @Scenario: 确认资源目录数据
 */
class ConfirmTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_items_data');
        $this->clear('pcore_gb_search_data');
    }

    /**
     * @Given: 存在需要确认的资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data'],
            'pcore_gb_search_data' => $this->gbSearchData(GbSearchData::STATUS['CONFIRM'])['pcore_gb_search_data']
        ]);
    }

    /**
     * @When: 获取需要确认的资源目录数据
     */
    public function fetchGbSearchData($id)
    {
        $repository = new GbSearchDataRepository();

        $gbSearchData = $repository->fetchOne($id);

        return $gbSearchData;
    }

    /**
     * @And: 当我调用确认函数,期待返回true
     */
    public function confirm()
    {
        $gbSearchData = $this->fetchGbSearchData(1);
        
        return $gbSearchData->confirm();
    }

    /**
     * @Then: 数据已经被确认
     */
    public function testValidate()
    {
        $result = $this->confirm();

        $this->assertTrue($result);

        $gbSearchData = $this->fetchGbSearchData(1);

        $this->assertEquals(GbSearchData::STATUS['ENABLED'], $gbSearchData->getStatus());
        $this->assertEquals(Core::$container->get('time'), $gbSearchData->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $gbSearchData->getStatusTime());
    }
}

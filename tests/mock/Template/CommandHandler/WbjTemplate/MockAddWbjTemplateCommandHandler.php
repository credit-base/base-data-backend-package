<?php
namespace BaseData\Template\CommandHandler\WbjTemplate;

use BaseData\Template\Model\WbjTemplate;

class MockAddWbjTemplateCommandHandler extends AddWbjTemplateCommandHandler
{
    public function getWbjTemplate() : WbjTemplate
    {
        return parent::getWbjTemplate();
    }
}

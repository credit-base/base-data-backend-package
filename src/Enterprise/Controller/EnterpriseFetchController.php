<?php
namespace BaseData\Enterprise\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Enterprise\View\EnterpriseView;
use BaseData\Enterprise\Repository\EnterpriseRepository;
use BaseData\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;

class EnterpriseFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IEnterpriseAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new EnterpriseView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'enterprises';
    }
}

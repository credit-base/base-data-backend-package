<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData;

use BaseData\ResourceCatalogData\Model\YsSearchData;

interface IYsSearchDataAdapter
{
    public function fetchOne($id) : YsSearchData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(YsSearchData $ysSearchData) : bool;

    public function edit(YsSearchData $ysSearchData, array $keys = array()) : bool;
}

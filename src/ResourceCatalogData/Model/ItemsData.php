<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

abstract class ItemsData implements IOperate, IObject
{
    use OperateTrait, Object;

    private $id;

    private $data;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->data = array();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->data);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setData(array $data) : void
    {
        $this->data = $data;
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    abstract protected function getRepository();

    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return false;
    }
}

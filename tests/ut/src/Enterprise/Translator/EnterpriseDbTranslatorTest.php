<?php
namespace BaseData\Enterprise\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Enterprise\Utils\MockFactory;
use BaseData\Enterprise\Utils\EnterpriseUtils;

class EnterpriseDbTranslatorTest extends TestCase
{
    use EnterpriseUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new EnterpriseDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Enterprise\Model\NullEnterprise', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $enterprise = MockFactory::generateEnterprise(1);

        $expression['enterprise_id'] = $enterprise->getId();

        $expression['name'] = $enterprise->getName();
        $expression['unified_social_credit_code'] = $enterprise->getUnifiedSocialCreditCode();
        $expression['establishment_date'] = $enterprise->getEstablishmentDate();
        $expression['approval_date'] = $enterprise->getApprovalDate();
        $expression['address'] = $enterprise->getAddress();
        $expression['registration_capital'] = $enterprise->getRegistrationCapital();
        $expression['business_term_start'] = $enterprise->getBusinessTermStart();
        $expression['business_term_to'] = $enterprise->getBusinessTermTo();
        $expression['business_scope'] = $enterprise->getBusinessScope();
        $expression['registration_authority'] = $enterprise->getRegistrationAuthority();
        $expression['principal'] = $enterprise->getPrincipal();
        $expression['principal_card_id'] = $enterprise->getPrincipalCardId();
        $expression['registration_status'] = $enterprise->getRegistrationStatus();
        $expression['enterprise_type_code'] = $enterprise->getEnterpriseTypeCode();
        $expression['enterprise_type'] = $enterprise->getEnterpriseType();
        $expression['data'] = base64_encode(gzcompress(serialize($enterprise->getData())));
        $expression['industry_category'] = $enterprise->getIndustryCategory();
        $expression['industry_code'] = $enterprise->getIndustryCode();
        $expression['administrative_area'] = $enterprise->getAdministrativeArea();
        $expression['hash'] = $enterprise->getHash();
        $expression['task_id'] = $enterprise->getTask()->getId();
        $expression['status'] = $enterprise->getStatus();
        $expression['status_time'] = $enterprise->getStatusTime();
        $expression['create_time'] = $enterprise->getCreateTime();
        $expression['update_time'] = $enterprise->getUpdateTime();

        $enterprise = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Enterprise\Model\Enterprise', $enterprise);
        $this->compareArrayAndObject($expression, $enterprise);
    }

    public function testObjectToArray()
    {
        $enterprise = MockFactory::generateEnterprise(1);

        $expression = $this->translator->objectToArray($enterprise);

        $this->compareArrayAndObject($expression, $enterprise);
    }
}

<?php
namespace BaseData\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Controller\Interfaces\IApproveAbleController;

class ApproveControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(ApproveController::class)
                                 ->setMethods(['getApproveController'])
                                 ->getMock();
                      
        $this->childController = new class extends ApproveController
        {
            public function getApproveController(string $resource) : IApproveAbleController
            {
                return parent::getApproveController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
    }

    public function testGetApproveController()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Controller\Interfaces\IApproveAbleController',
            $this->childController->getApproveController($this->resource)
        );
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testIndex()
    {
        $id = 1;
        $status = 'approve';

        $approveController = $this->prophesize(IApproveAbleController::class);
        $approveController->approve(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->exactly(1))
                         ->method('getApproveController')
                         ->with($this->resource)
                         ->willReturn($approveController->reveal());

        $result = $this->controller->index($this->resource, $id, $status);
        $this->assertTrue($result);
    }
}

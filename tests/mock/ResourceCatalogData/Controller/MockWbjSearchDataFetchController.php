<?php
namespace BaseData\ResourceCatalogData\Controller;

use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use Marmot\Interfaces\IView;

class MockWbjSearchDataFetchController extends WbjSearchDataFetchController
{
    public function getRepository() : IWbjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

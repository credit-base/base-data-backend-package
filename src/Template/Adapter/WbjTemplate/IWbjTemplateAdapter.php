<?php
namespace BaseData\Template\Adapter\WbjTemplate;

use BaseData\Template\Model\WbjTemplate;

interface IWbjTemplateAdapter
{
    public function fetchOne($id) : WbjTemplate;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(WbjTemplate $wbjTemplate) : bool;

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool;
}

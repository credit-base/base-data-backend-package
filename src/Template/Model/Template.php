<?php
namespace BaseData\Template\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class Template implements IObject
{
    use Object;

    /**
     * 主体类别
     * @var SUBJECT_CATEGORY['FRJFFRZZ']  法人及非法人组织 1
     * @var SUBJECT_CATEGORY['ZRR']  自然人 2
     * @var SUBJECT_CATEGORY['GTGSH']  个体工商户 3
     */
    const SUBJECT_CATEGORY = array(
        'FRJFFRZZ' => 1,
        'ZRR' => 2,
        'GTGSH' => 3
    );
    const SUBJECT_CATEGORY_CN = array(
        self::SUBJECT_CATEGORY['FRJFFRZZ'] => '法人及非法人组织',
        self::SUBJECT_CATEGORY['ZRR'] => '自然人',
        self::SUBJECT_CATEGORY['GTGSH'] => '个体工商户'
    );

    /**
     * 公开范围
     * @var DIMENSION['SHGK']  社会公开 1
     * @var DIMENSION['ZWGX']  政务共享 2
     * @var DIMENSION['SQCX']  授权查询 3
     */
    const DIMENSION = array(
        'SHGK' => 1,
        'ZWGX' => 2,
        'SQCX' => 3
    );
    const DIMENSION_CN = array(
        self::DIMENSION['SHGK'] => '社会公开',
        self::DIMENSION['ZWGX'] => '政务共享',
        self::DIMENSION['SQCX'] => '授权查询'
    );

    /**
     * 更新频率
     * @var EXCHANGE_FREQUENCY['SS']  实时 1
     * @var EXCHANGE_FREQUENCY['MR']  每日 2
     * @var EXCHANGE_FREQUENCY['MZ']  每周 3
     * @var EXCHANGE_FREQUENCY['MY']  每月 4
     * @var EXCHANGE_FREQUENCY['MJD']  每季度 5
     * @var EXCHANGE_FREQUENCY['MBN']  每半年 6
     * @var EXCHANGE_FREQUENCY['MYN']  每一年 7
     * @var EXCHANGE_FREQUENCY['MLN']  每两年 8
     * @var EXCHANGE_FREQUENCY['MSN']  每三年 9
     */
    const EXCHANGE_FREQUENCY = array(
        'SS' => 1,
        'MR' => 2,
        'MZ' => 3,
        'MY' => 4,
        'MJD' => 5,
        'MBN' => 6,
        'MYN' => 7,
        'MLN' => 8,
        'MSN' => 9
    );
    const EXCHANGE_FREQUENCY_CN = array(
        self::EXCHANGE_FREQUENCY['SS'] => '实时',
        self::EXCHANGE_FREQUENCY['MR'] => '每日',
        self::EXCHANGE_FREQUENCY['MZ'] => '每周',
        self::EXCHANGE_FREQUENCY['MY'] => '每月',
        self::EXCHANGE_FREQUENCY['MJD'] => '每季度',
        self::EXCHANGE_FREQUENCY['MBN'] => '每半年',
        self::EXCHANGE_FREQUENCY['MYN'] => '每一年',
        self::EXCHANGE_FREQUENCY['MLN'] => '每两年',
        self::EXCHANGE_FREQUENCY['MSN'] => '每三年'
    );
    
    /**
     * 信息分类
     * @var INFO_CLASSIFY['XZXK']  行政许可 1
     * @var INFO_CLASSIFY['XZCF']  行政处罚 2
     * @var INFO_CLASSIFY['HONGMD']  红名单 3
     * @var INFO_CLASSIFY['HEIMD']  黑名单 4
     * @var INFO_CLASSIFY['QT']  其他 5
     */
    const INFO_CLASSIFY = array(
        'XZXK' => 1,
        'XZCF' => 2,
        'HONGMD' => 3,
        'HEIMD' => 4,
        'QT' => 5
    );
    const INFO_CLASSIFY_CN = array(
        self::INFO_CLASSIFY['XZXK'] => '行政许可',
        self::INFO_CLASSIFY['XZCF'] => '行政处罚',
        self::INFO_CLASSIFY['HONGMD'] => '红名单',
        self::INFO_CLASSIFY['HEIMD'] => '黑名单',
        self::INFO_CLASSIFY['QT'] => '其他'
    );

    /**
     * 信息类别
     * @var INFO_CATEGORY['JCXX']  基础信息 1
     * @var INFO_CATEGORY['SHOUXXX']  守信信息 2
     * @var INFO_CATEGORY['SHIXXX']  失信信息 3
     * @var INFO_CATEGORY['QTXX']  其他信息 4
     */
    const INFO_CATEGORY = array(
        'JCXX' => 1,
        'SHOUXXX' => 2,
        'SHIXXX' => 3,
        'QTXX' => 4
    );
    const INFO_CATEGORY_CN = array(
        self::INFO_CATEGORY['JCXX'] => '基础信息',
        self::INFO_CATEGORY['SHOUXXX'] => '守信信息',
        self::INFO_CATEGORY['SHIXXX'] => '失信信息',
        self::INFO_CATEGORY['QTXX'] => '其他信息'
    );

    /**
     * 数据类型
     * @var TYPE['ZFX']  字符型 1
     * @var TYPE['RQX']  日期型 2
     * @var TYPE['ZSX']  整数型 3
     * @var TYPE['FDX']  浮点型 4
     * @var TYPE['MJX']  枚举型 5
     * @var TYPE['JHX']  集合型 6
     */
    const TYPE = array(
        'ZFX' => 1,
        'RQX' => 2,
        'ZSX' => 3,
        'FDX' => 4,
        'MJX' => 5,
        'JHX' => 6
    );
    /**
     * 数据类型-默认值
     */
    const TEMPLATE_TYPE_DEFAULT = array(
        self::TYPE['ZFX'] => '',
        self::TYPE['RQX'] => 0,
        self::TYPE['ZSX'] => 0,
        self::TYPE['FDX'] => 0,
        self::TYPE['MJX'] => '',
        self::TYPE['JHX'] => ''
    );
    
    /**
     * 是否必填
     * @var IS_NECESSARY['FOU']  否 0
     * @var IS_NECESSARY['SHI']  是 1，默认
     */
    const IS_NECESSARY = array(
        'FOU' => 0,
        'SHI' => 1
    );

    /**
     * 是否脱敏
     * @var IS_MASKED['FOU']  否 0，默认
     * @var IS_MASKED['SHI']  是 1
     */
    const IS_MASKED = array(
        'FOU' => 0,
        'SHI' => 1
    );

    /**
     * 目录类别
     * @var CATEGORY['WBJ']  委办局 10
     * @var CATEGORY['BJ']  本级 1
     * @var CATEGORY['GB']  国标 2
     * @var CATEGORY['BASE']  基础 3
     * @var CATEGORY['QZJ_WBJ']  前置机委办局 20
     * @var CATEGORY['QZJ_BJ']  前置机本级 21
     * @var CATEGORY['QZJ_GB']  前置机国标 22
     */
    const CATEGORY = array(
        'WBJ' => 10,
        'BJ' => 1,
        'GB' => 2,
        'BASE' => 3,
        'QZJ_WBJ' => 20,
        'QZJ_BJ' => 21,
        'QZJ_GB' => 22,
    );

    const DEFAULT_LAST_ITEMS_IDENTIFY = array(
        'ZTLB' => 'ZTLB',
        'GKFW' => 'GKFW',
        'GXPL' => 'GXPL',
        'XXFL' => 'XXFL',
        'XXLB' => 'XXLB'
    );
    const DEFAULT_LAST_ITEMS_NAME = array(
        self::DEFAULT_LAST_ITEMS_IDENTIFY['ZTLB'] => '主体类别',
        self::DEFAULT_LAST_ITEMS_IDENTIFY['GKFW'] => '公开范围',
        self::DEFAULT_LAST_ITEMS_IDENTIFY['GXPL'] => '更新频率',
        self::DEFAULT_LAST_ITEMS_IDENTIFY['XXFL'] => '信息分类',
        self::DEFAULT_LAST_ITEMS_IDENTIFY['XXLB'] => '信息类别'
    );

    const DEFAULT_LAST_ITEMS = array(
        array(
            "name" => self::DEFAULT_LAST_ITEMS_NAME['ZTLB'],    //信息项名称
            "identify" => self::DEFAULT_LAST_ITEMS_IDENTIFY['ZTLB'],    //数据标识
            "type" => self::TYPE['JHX'],    //数据类型
            "length" => '50',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => self::DIMENSION['ZWGX'],    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => self::IS_NECESSARY['SHI'],    //是否必填，否 0 | 默认 是 1
            "isMasked" => self::IS_MASKED['FOU'],    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
            "remarks" => '法人及非法人组织;自然人;个体工商户，支持多选',    //备注
        ),
        array(
            "name" => self::DEFAULT_LAST_ITEMS_NAME['GKFW'],    //信息项名称
            "identify" => self::DEFAULT_LAST_ITEMS_IDENTIFY['GKFW'],    //数据标识
            "type" => self::TYPE['MJX'],    //数据类型
            "length" => '20',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => self::DIMENSION['ZWGX'],    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => self::IS_NECESSARY['SHI'],    //是否必填，否 0 | 默认 是 1
            "isMasked" => self::IS_MASKED['FOU'],    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
            "remarks" => '支持单选',    //备注
        ),
        array(
            "name" => self::DEFAULT_LAST_ITEMS_NAME['GXPL'],    //信息项名称
            "identify" => self::DEFAULT_LAST_ITEMS_IDENTIFY['GXPL'],    //数据标识
            "type" => self::TYPE['MJX'],    //数据类型
            "length" => '20',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => self::DIMENSION['ZWGX'],    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => self::IS_NECESSARY['SHI'],    //是否必填，否 0 | 默认 是 1
            "isMasked" => self::IS_MASKED['FOU'],    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
            "remarks" => '支持单选',    //备注
        ),
        array(
            "name" => self::DEFAULT_LAST_ITEMS_NAME['XXFL'],    //信息项名称
            "identify" => self::DEFAULT_LAST_ITEMS_IDENTIFY['XXFL'],    //数据标识
            "type" => self::TYPE['MJX'],    //数据类型
            "length" => '50',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => self::DIMENSION['ZWGX'],    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => self::IS_NECESSARY['SHI'],    //是否必填，否 0 | 默认 是 1
            "isMasked" => self::IS_MASKED['FOU'],    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
            "remarks" => '支持单选',    //备注
        ),
        array(
            "name" => self::DEFAULT_LAST_ITEMS_NAME['XXLB'],    //信息项名称
            "identify" => self::DEFAULT_LAST_ITEMS_IDENTIFY['XXLB'],    //数据标识
            "type" => self::TYPE['MJX'],    //数据类型
            "length" => '50',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => self::DIMENSION['ZWGX'],    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => self::IS_NECESSARY['SHI'],    //是否必填，否 0 | 默认 是 1
            "isMasked" => self::IS_MASKED['FOU'],    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则，即左边保留1位字符，右边保留2位字符
            "remarks" => '信息性质类型，支持单选',    //备注
        )
    );

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var string $name 目录名称
     */
    protected $name;
    /**
     * @var string $identify 目录标识
     */
    protected $identify;
    /**
     * @var array $subjectCategory 主体类别
     */
    protected $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    protected $dimension;
    /**
     * @var int $exchangeFrequency 更新频率
     */
    protected $exchangeFrequency;
    /**
     * @var int $infoClassify 信息分类
     */
    protected $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    protected $infoCategory;
    /**
     * @var string $description 目录描述
     */
    protected $description;
    /**
     * @var array $items 模板信息
     */
    protected $items;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->identify = '';
        $this->subjectCategory = array();
        $this->dimension = 0;
        $this->exchangeFrequency = 0;
        $this->infoClassify = 0;
        $this->infoCategory = 0;
        $this->description = '';
        $this->items = array();

        $this->status = 0;
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->identify);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->exchangeFrequency);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->description);
        unset($this->items);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
    }

    /**
     * @marmot-set-id
     */
    public function setId($id) : void
    {
        $this->id = $id;
    }
    /**
     * @marmot-get-id
     */
    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setSubjectCategory(array $subjectCategory) : void
    {
        $this->subjectCategory = $subjectCategory;
    }

    public function getSubjectCategory() : array
    {
        return $this->subjectCategory;
    }

    public function setDimension(int $dimension) : void
    {
        $this->dimension = in_array($dimension, self::DIMENSION) ? $dimension : 0;
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function setExchangeFrequency(int $exchangeFrequency) : void
    {
        $this->exchangeFrequency = $exchangeFrequency;
    }

    public function getExchangeFrequency() : int
    {
        return $this->exchangeFrequency;
    }

    public function setInfoClassify(string $infoClassify) : void
    {
        $this->infoClassify = $infoClassify;
    }

    public function getInfoClassify() : string
    {
        return $this->infoClassify;
    }

    public function setInfoCategory(string $infoCategory) : void
    {
        $this->infoCategory = $infoCategory;
    }

    public function getInfoCategory() : string
    {
        return $this->infoCategory;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setItems(array $items) : void
    {
        $this->items = $items;
    }

    public function getItems() : array
    {
        return $this->items;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Model\IEnableAble;

use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\NullBjTemplate;

use BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class BjSearchDataTest extends TestCase
{
    private $bjSearchData;

    private $faker;

    public function setUp()
    {
        $this->bjSearchData = new MockBjSearchData();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->bjSearchData);
        unset($this->faker);
    }

    public function testSearchDataConstructor()
    {
        $this->assertEquals('', $this->bjSearchData->getDescription());
        $this->assertEquals(
            BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'],
            $this->bjSearchData->getFrontEndProcessorStatus()
        );
        $this->assertInstanceOf(
            'BaseData\Template\Model\BjTemplate',
            $this->bjSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\BjItemsData',
            $this->bjSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new BjItemsData();

        $this->bjSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->bjSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->bjSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->bjSearchData->setStatus($actual);
        $this->assertEquals($expected, $this->bjSearchData->getStatus());
    }
    /**
     * 循环测试 DispatchDepartment setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(BjSearchData::STATUS['CONFIRM'], BjSearchData::STATUS['CONFIRM']),
            array(BjSearchData::STATUS['ENABLED'], BjSearchData::STATUS['ENABLED']),
            array(BjSearchData::STATUS['DISABLED'], BjSearchData::STATUS['DISABLED']),
            array(BjSearchData::STATUS['DELETED'], BjSearchData::STATUS['DELETED']),
            array(999, BjSearchData::STATUS['CONFIRM'])
        );
    }
    /**
     * 设置 setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->bjSearchData->setStatus($this->faker->word());
    }
    //status 测试 ------------------------------------------------------   end
    //frontEndProcessorStatus 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setFrontEndProcessorStatus() 是否符合预定范围
     *
     * @dataProvider frontEndProcessorStatusProvider
     */
    public function testSetFrontEndProcessorStatus($actual, $expected)
    {
        $this->bjSearchData->setFrontEndProcessorStatus($actual);
        $this->assertEquals($expected, $this->bjSearchData->getFrontEndProcessorStatus());
    }
    /**
     * 循环测试 DispatchDepartment setFrontEndProcessorStatus() 数据构建器
     */
    public function frontEndProcessorStatusProvider()
    {
        return array(
            array(
                BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'],
                BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
            ),
            array(
                BjSearchData::FRONT_END_PROCESSOR_STATUS['IMPORT'],
                BjSearchData::FRONT_END_PROCESSOR_STATUS['IMPORT']
            ),
            array(999, BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'])
        );
    }
    /**
     * 设置 setFrontEndProcessorStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFrontEndProcessorStatusWrongType()
    {
        $this->bjSearchData->setFrontEndProcessorStatus($this->faker->word());
    }
    //frontEndProcessorStatus 测试 ------------------------------------------------------   end
    
    //description 测试 ------------------------------------------------------- start
    /**
     * 设置 setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $description = $this->faker->word();

        $this->bjSearchData->setDescription($description);
        $this->assertEquals($description, $this->bjSearchData->getDescription());
    }

    /**
     * 设置 setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->bjSearchData->setDescription($this->faker->words(3, false));
    }
    //description 测试 -------------------------------------------------------   end
    
    public function testGetBjItemsDataRepository()
    {
        $bjSearchData = new MockBjSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter',
            $bjSearchData->getBjItemsDataRepository()
        );
    }

    public function testGetRepository()
    {
        $bjSearchData = new MockBjSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $bjSearchData->getRepository()
        );
    }

    public function testAddItemsData()
    {
        $bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
                           ->setMethods(['getBjItemsDataRepository'])
                           ->getMock();

        $repository = $this->prophesize(IBjItemsDataAdapter::class);
        $repository->add(Argument::exact($bjSearchData->getItemsData()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $bjSearchData->expects($this->once())
             ->method('getBjItemsDataRepository')
             ->willReturn($repository->reveal());

        $result = $bjSearchData->addItemsData();
        $this->assertTrue($result);
    }

    public function testAdd()
    {
        $bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
                           ->setMethods(['assignment', 'validate', 'addItemsData', 'getRepository'])
                           ->getMock();

        $bjSearchData->expects($this->once())
            ->method('assignment');

        $bjSearchData->expects($this->once())
            ->method('validate')
            ->willReturn(true);

        $bjSearchData->expects($this->once())
            ->method('addItemsData')
            ->willReturn(true);

        $repository = $this->prophesize(IBjSearchDataAdapter::class);
        $repository->add(Argument::exact($bjSearchData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $bjSearchData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $bjSearchData->add();
        $this->assertTrue($result);
    }
    
    public function testIsTemplateNullTrue()
    {
        $template = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate($this->faker->randomNumber());
        $this->bjSearchData->setTemplate($template);

        $result = $this->bjSearchData->isTemplateNull();
        $this->assertTrue($result);
    }

    public function testIsTemplateNullFalse()
    {
        $template = new NullBjTemplate();
        $this->bjSearchData->setTemplate($template);
        
        $result = $this->bjSearchData->isTemplateNull();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('template', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsSubjectCategoryExistTrue()
    {
        $subjectCategory = ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'];
        $templateSubjectCategory = array(
            ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
            ISearchDataAble::SUBJECT_CATEGORY['GTGSH']
        );

        $this->bjSearchData->setSubjectCategory($subjectCategory);
        $template = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate($this->faker->randomNumber());
        $this->bjSearchData->setTemplate($template);
        $this->bjSearchData->getTemplate()->setSubjectCategory($templateSubjectCategory);
        
        $result = $this->bjSearchData->isSubjectCategoryExist();
        $this->assertTrue($result);
    }

    public function testIsSubjectCategoryExistFalse()
    {
        $subjectCategory = ISearchDataAble::SUBJECT_CATEGORY['ZRR'];
        $templateSubjectCategory = array(
            ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
            ISearchDataAble::SUBJECT_CATEGORY['GTGSH']
        );

        $this->bjSearchData->setSubjectCategory($subjectCategory);
        $this->bjSearchData->getTemplate()->setSubjectCategory($templateSubjectCategory);
        
        $result = $this->bjSearchData->isSubjectCategoryExist();
        
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertEquals('subjectCategory', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    private function initialAssignment($subjectCategory, $data)
    {
        $template = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $this->faker->randomNumber()
        );
        $itemsData = \BaseData\ResourceCatalogData\Utils\BjItemsDataMockFactory::generateBjItemsData(
            $this->faker->randomNumber()
        );

        $this->bjSearchData->setTemplate($template);
        $this->bjSearchData->setItemsData($itemsData);
        $this->bjSearchData->setSubjectCategory($subjectCategory);
        $this->bjSearchData->getItemsData()->setData($data);

        $this->bjSearchData->assignment();
        $this->assertEquals(
            $this->bjSearchData->getInfoClassify(),
            $this->bjSearchData->getTemplate()->getInfoClassify()
        );
        $this->assertEquals(
            $this->bjSearchData->getInfoCategory(),
            $this->bjSearchData->getTemplate()->getInfoCategory()
        );
        $this->assertEquals($this->bjSearchData->getName(), $data['ZTMC']);
        
        if ($subjectCategory == ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'] ||
            $subjectCategory == ISearchDataAble::SUBJECT_CATEGORY['GTGSH']) {
            $this->assertEquals($this->bjSearchData->getIdentify(), $data['TYSHXYDM']);
        }
        
        if ($subjectCategory == ISearchDataAble::SUBJECT_CATEGORY['ZRR']) {
            $this->assertEquals($this->bjSearchData->getIdentify(), $data['ZJHM']);
        }
    }

    public function testAssignmentToFrjffrzz()
    {
        $subjectCategory = ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'];
        
        $data = array(
            'ZTMC' => $this->faker->name(),
            'TYSHXYDM' => $this->faker->creditCardNumber(),
        );

        $this->initialAssignment($subjectCategory, $data);
    }

    public function testAssignmentToZrr()
    {
        $subjectCategory = ISearchDataAble::SUBJECT_CATEGORY['ZRR'];
        
        $data = array(
            'ZTMC' => $this->faker->name(),
            'ZJHM' => $this->faker->creditCardNumber(),
        );

        $this->initialAssignment($subjectCategory, $data);
    }

    public function testAssignmentToGtgsh()
    {
        $subjectCategory = ISearchDataAble::SUBJECT_CATEGORY['GTGSH'];
        
        $data = array(
            'ZTMC' => $this->faker->name(),
            'TYSHXYDM' => $this->faker->creditCardNumber(),
        );

        $this->initialAssignment($subjectCategory, $data);
    }

    public function testUpdateStatus()
    {
        $this->bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['getRepository'])
            ->getMock();
            
        $status = BjSearchData::STATUS['CONFIRM'];
        $this->bjSearchData->setStatus($status);
        $this->bjSearchData->setUpdateTime(Core::$container->get('time'));
        $this->bjSearchData->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(IBjSearchDataAdapter::class);

        $repository->edit(
            Argument::exact($this->bjSearchData),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->bjSearchData->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->bjSearchData->updateStatus($status);
        
        $this->assertTrue($result);
    }

    public function testDelete()
    {
        $bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $status = BjSearchData::STATUS['CONFIRM'];
        $bjSearchData->setStatus($status);

        $bjSearchData->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(BjSearchData::STATUS['DELETED'])
                    ->willReturn(true);

        $result = $bjSearchData->delete();
        
        $this->assertTrue($result);
    }

    public function testDeleteFail()
    {
        $status = BjSearchData::STATUS['DELETED'];
        $this->bjSearchData->setStatus($status);

        $result = $this->bjSearchData->delete();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsConfirm()
    {
        $this->bjSearchData->setStatus(BjSearchData::STATUS['CONFIRM']);

        $result = $this->bjSearchData->isConfirm();
        $this->assertTrue($result);
    }

    public function testIsEnabled()
    {
        $this->bjSearchData->setStatus(BjSearchData::STATUS['ENABLED']);

        $result = $this->bjSearchData->isEnabled();
        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $status = BjSearchData::STATUS['CONFIRM'];
        $bjSearchData->setStatus($status);

        $bjSearchData->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(BjSearchData::STATUS['ENABLED'])
                    ->willReturn(true);

        $result = $bjSearchData->confirm();
        
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $status = BjSearchData::STATUS['DELETED'];
        $this->bjSearchData->setStatus($status);

        $result = $this->bjSearchData->confirm();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $bjSearchData = $this->getMockBuilder(MockBjSearchData::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $status = BjSearchData::STATUS['ENABLED'];
        $bjSearchData->setStatus($status);

        $bjSearchData->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(BjSearchData::STATUS['DISABLED'])
                    ->willReturn(true);

        $result = $bjSearchData->disable();
        
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $status = BjSearchData::STATUS['DELETED'];
        $this->bjSearchData->setStatus($status);

        $result = $this->bjSearchData->disable();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }
}

<?php
namespace BaseData\Task\Utils;

use BaseData\Task\Model\Task;
use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\Model\MockTask;

class MockFactory
{
    public static function generateTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Task {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $task = new MockTask($id);
        $task->setId($id);

        //parentId
        self::generateParentId($task, $faker, $value);
        //data
        self::generateData($task, $faker, $value);
        //nextScheduleTime
        self::generateNextScheduleTime($task, $faker, $value);
        //nice
        self::generateNice($task, $faker, $value);
        //statues
        self::generateTaskStatus($task, $faker, $value);
        //pid
        self::generatePid($task, $faker, $value);

        //status
        $task->setCreateTime($faker->unixTime());
        $task->setUpdateTime($faker->unixTime());
        $task->setStatusTime($faker->unixTime());

        return $task;
    }

    private static function generateParentId($task, $faker, $value) : void
    {
        $parentId = isset($value['parentId']) ?
            $value['parentId'] :
            $faker->randomNumber();
        
        $task->setParentId($parentId);
    }

    private static function generateData($task, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            [$faker->name()];
        
        $task->setData($data);
    }

    private static function generateNextScheduleTime($task, $faker, $value) : void
    {
        unset($faker);
        $data = isset($value['nextScheduleTime']) ?
            $value['nextScheduleTime'] :
            time();
        
        $task->setNextScheduleTime($data);
    }

    private static function generateNice($task, $faker, $value) : void
    {
        $nice = isset($value['nice']) ?
            $value['nice'] :
            $faker->randomNumber();
        
        $task->setNice($nice);
    }

    private static function generateTaskStatus($task, $faker, $value) : void
    {
        $category = isset($value['status']) ? $value['status'] : $faker->randomElement(Task::STATUS);
        $task->setStatus($category);
    }

    private static function generatePid($task, $faker, $value) : void
    {
        $pid = isset($value['pid']) ? $value['pid'] : $faker->randomNumber();
        $task->setPid($pid);
    }

    public static function generateNotifyRecord(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : NotifyRecord {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $notifyRecord = new NotifyRecord($id);
        $notifyRecord->setId($id);

        //parentId
        self::generateName($notifyRecord, $faker, $value);
        //statues
        self::generateNotifyRecordStatus($notifyRecord, $faker, $value);

        //status
        $notifyRecord->setCreateTime($faker->unixTime());
        $notifyRecord->setUpdateTime($faker->unixTime());
        $notifyRecord->setStatusTime($faker->unixTime());

        return $notifyRecord;
    }

    private static function generateName($task, $faker, $value) : void
    {
        unset($faker);
        $name = isset($value['name']) ?
            $value['name'] :
            'XZXK_10_1_hash.xlsx';
        
        $task->setName($name);
    }

    private static function generateNotifyRecordStatus($task, $faker, $value) : void
    {
        $category = isset($value['status']) ? $value['status'] : $faker->randomElement(NotifyRecord::STATUS);
        $task->setStatus($category);
    }
}

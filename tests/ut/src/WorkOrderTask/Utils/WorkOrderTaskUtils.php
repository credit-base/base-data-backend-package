<?php
namespace BaseData\WorkOrderTask\Utils;

trait WorkOrderTaskUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $workOrderTask
    ) {
        $this->assertEquals($expectedArray['work_order_task_id'], $workOrderTask->getId());

        $this->assertEquals($expectedArray['status'], $workOrderTask->getStatus());
        $this->assertEquals($expectedArray['create_time'], $workOrderTask->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $workOrderTask->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $workOrderTask->getStatusTime());
    }
}

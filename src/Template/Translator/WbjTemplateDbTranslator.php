<?php
namespace BaseData\Template\Translator;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\NullWbjTemplate;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class WbjTemplateDbTranslator extends TemplateDbTranslator
{
    public function arrayToObject(array $expression, $wbjTemplate = null) : WbjTemplate
    {
        if (!isset($expression['wbj_template_id'])) {
            return NullWbjTemplate::getInstance();
        }

        if ($wbjTemplate == null) {
            $wbjTemplate = new WbjTemplate($expression['wbj_template_id']);
        }

        $wbjTemplate->setId($expression['wbj_template_id']);

        if (isset($expression['source_unit_id'])) {
            $wbjTemplate->getSourceUnit()->setId($expression['source_unit_id']);
        }

        $wbjTemplate = parent::arrayToObject($expression, $wbjTemplate);
        
        return $wbjTemplate;
    }

    public function objectToArray($wbjTemplate, array $keys = array()) : array
    {
        if (!$wbjTemplate instanceof WbjTemplate) {
            return [];
        }

        $templateExpression = parent::objectToArray($wbjTemplate, $keys);
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'sourceUnit'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['wbj_template_id'] = $wbjTemplate->getId();
        }
        if (in_array('sourceUnit', $keys)) {
            $expression['source_unit_id'] = $wbjTemplate->getSourceUnit()->getId();
        }

        $expression = array_merge($expression, $templateExpression);
        
        return $expression;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Model\NullCrew;
use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Model\NullUserGroup;

use BaseData\Template\Model\Template;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class SearchDataTest extends TestCase
{
    private $searchData;

    private $faker;

    public function setUp()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
                                ->getMockForAbstractClass();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->searchData);
        unset($this->faker);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->searchData
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IOperate',
            $this->searchData
        );
    }

    public function testImplementsISearchDataAble()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\ISearchDataAble',
            $this->searchData
        );
    }

    public function testSearchDataConstructor()
    {
        $this->assertEquals(0, $this->searchData->getId());
        $this->assertEquals(0, $this->searchData->getInfoClassify());
        $this->assertEquals(0, $this->searchData->getInfoCategory());
        $this->assertEquals(0, $this->searchData->getSubjectCategory());
        $this->assertEquals(0, $this->searchData->getDimension());
        $this->assertEquals(SearchData::EXPIRATION_DATE_DEFAULT, $this->searchData->getExpirationDate());
        $this->assertEquals('', $this->searchData->getHash());
        $this->assertInstanceOf(
            'BaseData\Crew\Model\Crew',
            $this->searchData->getCrew()
        );
        $this->assertInstanceOf(
            'BaseData\UserGroup\Model\UserGroup',
            $this->searchData->getSourceUnit()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\Task',
            $this->searchData->getTask()
        );
    }

    public function testSetId()
    {
        $id = $this->faker->randomNumber();
        $this->searchData->setId($id);
        $this->assertEquals($id, $this->searchData->getId());
    }
    
    //infoClassify 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setInfoClassify() 是否符合预定范围
     *
     * @dataProvider infoClassifyProvider
     */
    public function testSetInfoClassify($actual, $expected)
    {
        $this->searchData->setInfoClassify($actual);
        $this->assertEquals($expected, $this->searchData->getInfoClassify());
    }
    /**
     * 循环测试 DispatchDepartment setInfoClassify() 数据构建器
     */
    public function infoClassifyProvider()
    {
        return array(
            array(ISearchDataAble::INFO_CLASSIFY['XZXK'], ISearchDataAble::INFO_CLASSIFY['XZXK']),
            array(ISearchDataAble::INFO_CLASSIFY['XZCF'], ISearchDataAble::INFO_CLASSIFY['XZCF']),
            array(ISearchDataAble::INFO_CLASSIFY['HONGMD'], ISearchDataAble::INFO_CLASSIFY['HONGMD']),
            array(ISearchDataAble::INFO_CLASSIFY['HEIMD'], ISearchDataAble::INFO_CLASSIFY['HEIMD']),
            array(ISearchDataAble::INFO_CLASSIFY['QT'], ISearchDataAble::INFO_CLASSIFY['QT'])
        );
    }

    //template 测试 ---------------------------------------------------- start
    /**
     * 设置 setTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTemplateCorrectType()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods(['getItemsData'])
            ->getMock();

        $template = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate($this->faker->randomNumber());
        $items = array(
            array('identify'=>'ZTMC', 'type' => 1),
            array('identify'=>'TYSHXYDM', 'type' => 1),
        );
        $template->setItems($items);

        $itemsData = array();
        foreach ($items as $item) {
            $itemsData[$item['identify']] = isset(
                Template::TEMPLATE_TYPE_DEFAULT[$item['type']]
            ) ? Template::TEMPLATE_TYPE_DEFAULT[$item['type']] : '';
        }

        $bjItemsData = new BjItemsData();
        $bjItemsData->setData($itemsData);

        $this->searchData->expects($this->any())
                    ->method('getItemsData')
                    ->willReturn($bjItemsData);

        $this->searchData->setTemplate($template);

        $this->assertEquals($template, $this->searchData->getTemplate());
        $this->assertEquals($itemsData, $this->searchData->getItemsData()->getData());
    }

    /**
     * 设置 setTemplate() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTemplateWrongType()
    {
        $template = array($this->faker->randomNumber());

        $this->searchData->setTemplate($template);
    }
    //template 测试 ----------------------------------------------------   end
    
    /**
     * 设置 setInfoClassify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoClassifyWrongType()
    {
        $this->searchData->setInfoClassify($this->faker->word());
    }
    //infoClassify 测试 ------------------------------------------------------   end
    
    //infoCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setInfoCategory() 是否符合预定范围
     *
     * @dataProvider infoCategoryProvider
     */
    public function testSetInfoCategory($actual, $expected)
    {
        $this->searchData->setInfoCategory($actual);
        $this->assertEquals($expected, $this->searchData->getInfoCategory());
    }
    /**
     * 循环测试 DispatchDepartment setInfoCategory() 数据构建器
     */
    public function infoCategoryProvider()
    {
        return array(
            array(ISearchDataAble::INFO_CATEGORY['JCXX'], ISearchDataAble::INFO_CATEGORY['JCXX']),
            array(ISearchDataAble::INFO_CATEGORY['SHOUXXX'], ISearchDataAble::INFO_CATEGORY['SHOUXXX']),
            array(ISearchDataAble::INFO_CATEGORY['SHIXXX'], ISearchDataAble::INFO_CATEGORY['SHIXXX']),
            array(ISearchDataAble::INFO_CATEGORY['QTXX'], ISearchDataAble::INFO_CATEGORY['QTXX'])
        );
    }
    /**
     * 设置 setInfoCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetInfoCategoryWrongType()
    {
        $this->searchData->setInfoCategory($this->faker->word());
    }
    //infoCategory 测试 ------------------------------------------------------   end
    
    //crew 测试 ---------------------------------------------------- start
    /**
     * 设置 setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $crew = new Crew();

        $this->searchData->setCrew($crew);
        $this->assertEquals($crew, $this->searchData->getCrew());
    }

    /**
     * 设置 setCrew() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $crew = array($this->faker->randomNumber());

        $this->searchData->setCrew($crew);
    }
    //crew 测试 ----------------------------------------------------   end
    
    //sourceUnit 测试 ---------------------------------------------------- start
    /**
     * 设置 setSourceUnit() 正确的传参类型,期望传值正确
     */
    public function testSetSourceUnitCorrectType()
    {
        $sourceUnit = new UserGroup();

        $this->searchData->setSourceUnit($sourceUnit);
        $this->assertEquals($sourceUnit, $this->searchData->getSourceUnit());
    }

    /**
     * 设置 setSourceUnit() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceUnitWrongType()
    {
        $sourceUnit = array($this->faker->randomNumber());

        $this->searchData->setSourceUnit($sourceUnit);
    }
    //sourceUnit 测试 ----------------------------------------------------   end
        
    //subjectCategory 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setSubjectCategory() 是否符合预定范围
     *
     * @dataProvider subjectCategoryProvider
     */
    public function testSetSubjectCategory($actual, $expected)
    {
        $this->searchData->setSubjectCategory($actual);
        $this->assertEquals($expected, $this->searchData->getSubjectCategory());
    }
    /**
     * 循环测试 DispatchDepartment setSubjectCategory() 数据构建器
     */
    public function subjectCategoryProvider()
    {
        return array(
            array(ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'], ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ']),
            array(ISearchDataAble::SUBJECT_CATEGORY['ZRR'], ISearchDataAble::SUBJECT_CATEGORY['ZRR']),
            array(ISearchDataAble::SUBJECT_CATEGORY['GTGSH'], ISearchDataAble::SUBJECT_CATEGORY['GTGSH'])
        );
    }
    /**
     * 设置 setSubjectCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubjectCategoryWrongType()
    {
        $this->searchData->setSubjectCategory($this->faker->word());
    }
    //subjectCategory 测试 ------------------------------------------------------   end
    
    //dimension 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setDimension() 是否符合预定范围
     *
     * @dataProvider dimensionProvider
     */
    public function testSetDimension($actual, $expected)
    {
        $this->searchData->setDimension($actual);
        $this->assertEquals($expected, $this->searchData->getDimension());
    }
    /**
     * 循环测试 DispatchDepartment setDimension() 数据构建器
     */
    public function dimensionProvider()
    {
        return array(
            array(ISearchDataAble::DIMENSION['SHGK'], ISearchDataAble::DIMENSION['SHGK']),
            array(ISearchDataAble::DIMENSION['ZWGX'], ISearchDataAble::DIMENSION['ZWGX']),
            array(ISearchDataAble::DIMENSION['SQCX'], ISearchDataAble::DIMENSION['SQCX'])
        );
    }
    /**
     * 设置 setDimension() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDimensionWrongType()
    {
        $this->searchData->setDimension($this->faker->word());
    }
    //dimension 测试 ------------------------------------------------------   end
    
    //expirationDate 测试 ------------------------------------------------------- start
    /**
     * 设置 setExpirationDate() 正确的传参类型,期望传值正确
     */
    public function testSetExpirationDateCorrectType()
    {
        $expirationDate = $this->faker->unixTime();

        $this->searchData->setExpirationDate($expirationDate);
        $this->assertEquals($expirationDate, $this->searchData->getExpirationDate());
    }

    /**
     * 设置 setExpirationDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExpirationDateWrongType()
    {
        $this->searchData->setExpirationDate($this->faker->words(3, false));
    }
    //expirationDate 测试 -------------------------------------------------------   end
    
    //hash 测试 ------------------------------------------------------- start
    /**
     * 设置 setHash() 正确的传参类型,期望传值正确
     */
    public function testSetHashCorrectType()
    {
        $hash = $this->faker->md5();

        $this->searchData->setHash($hash);
        $this->assertEquals($hash, $this->searchData->getHash());
    }

    /**
     * 设置 setHash() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHashWrongType()
    {
        $this->searchData->setHash($this->faker->words(3, false));
    }
    //hash 测试 -------------------------------------------------------   end

    //task 测试 ---------------------------------------------------- start
    /**
     * 设置 setTask() 正确的传参类型,期望传值正确
     */
    public function testSetTaskCorrectType()
    {
        $task = new Task();

        $this->searchData->setTask($task);
        $this->assertEquals($task, $this->searchData->getTask());
    }

    /**
     * 设置 setTask() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTaskWrongType()
    {
        $task = array($this->faker->randomNumber());

        $this->searchData->setTask($task);
    }
    //task 测试 ----------------------------------------------------   end
    
    // public function getName() : string
    // {
    //     $itemsData = $this->getItemsData()->getData();
    //     return isset($itemsData['ZTMC']) ? $itemsData['ZTMC'] : array();
    // }

    // public function getIdentify() : string
    // {
    //     $identify = '';
    //     $itemsData = $this->getItemsData()->getData();

    //     if ($this->getSubjectCategory() == self::SUBJECT_CATEGORY['FRJFFRZZ'] ||
    //     $this->getSubjectCategory() == self::SUBJECT_CATEGORY['GTGSH']) {
    //         $identify = isset($itemsData['TYSHXYDM']) ? $itemsData['TYSHXYDM'] : '';
    //     }
    
    //     if ($this->getSubjectCategory() == self::SUBJECT_CATEGORY['ZRR']) {
    //         $identify = isset($itemsData['ZJHM']) ? $itemsData['ZJHM'] : '';
    //     }
    //     return $identify;
    // }

    //name
    public function testGetName()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods(['getItemsData'])
            ->getMock();

        $itemsData = array('ZTMC'=> '主体名称');

        $bjItemsData = new BjItemsData();
        $bjItemsData->setData($itemsData);

        $this->searchData->expects($this->once())
                    ->method('getItemsData')
                    ->willReturn($bjItemsData);

        $result = $this->searchData->getName();
        $this->assertEquals('主体名称', $result);
    }
    //name

    //identify
    public function testGetIdentifyZrr()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods(['getItemsData', 'getSubjectCategory'])
            ->getMock();

        $itemsData = array('ZJHM'=> '身份证号');

        $bjItemsData = new BjItemsData();
        $bjItemsData->setData($itemsData);

        $this->searchData->expects($this->once())
                    ->method('getItemsData')
                    ->willReturn($bjItemsData);

        $this->searchData->expects($this->any())
                    ->method('getSubjectCategory')
                    ->willReturn(ISearchDataAble::SUBJECT_CATEGORY['ZRR']);

        $result = $this->searchData->getIdentify();
        $this->assertEquals('身份证号', $result);
    }

    public function testGetIdentifyQy()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods(['getItemsData', 'getSubjectCategory'])
            ->getMock();

        $itemsData = array('TYSHXYDM'=> '统一社会信用代码');

        $bjItemsData = new BjItemsData();
        $bjItemsData->setData($itemsData);

        $this->searchData->expects($this->once())
                    ->method('getItemsData')
                    ->willReturn($bjItemsData);

        $this->searchData->expects($this->any())
                    ->method('getSubjectCategory')
                    ->willReturn(ISearchDataAble::SUBJECT_CATEGORY['GTGSH']);

        $result = $this->searchData->getIdentify();
        $this->assertEquals('统一社会信用代码', $result);
    }
    //identify
    public function testIsCrewNullTrue()
    {
        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew($this->faker->randomNumber());
        $this->searchData->setCrew($crew);

        $result = $this->searchData->isCrewNull();
        $this->assertTrue($result);
    }

    public function testIsCrewNullFalse()
    {
        $crew = new NullCrew();
        $this->searchData->setCrew($crew);
        
        $result = $this->searchData->isCrewNull();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('crew', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsSourceUnitNullTrue()
    {
        $sourceUnit = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup($this->faker->randomNumber());
        $this->searchData->setSourceUnit($sourceUnit);

        $result = $this->searchData->isSourceUnitNull();
        $this->assertTrue($result);
    }

    public function testIsSourceUnitNullFalse()
    {
        $sourceUnit = new NullUserGroup();
        $this->searchData->setSourceUnit($sourceUnit);
        
        $result = $this->searchData->isSourceUnitNull();
        
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals('sourceUnit', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    private function initialIsHashExist($result)
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $id = $this->faker->randomDigit();
        $ysSearchData = \BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory::generateYsSearchData($id);
        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($ysSearchData);
        }

        $filter['hash'] = $ysSearchData->getHash();

        $repository = $this->prophesize(YsSearchDataRepository::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->searchData->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testIsHashExistFalse()
    {
        $this->initialIsHashExist(false);

        $result = $this->searchData->isHashExist();

        $this->assertEquals('hash', Core::getLastError()->getSource()['pointer']);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testIsHashExistTrue()
    {
        $this->initialIsHashExist(true);
        
        $result = $this->searchData->isHashExist();

        $this->assertTrue($result);
    }

    public function testGenerateHash()
    {
        $itemsData = array('itemsData');

        $this->searchData->generateHash($itemsData);

        $hash = md5(base64_encode(gzcompress(serialize($itemsData))));

        $this->assertEquals($hash, $this->searchData->getHash());
    }

    public function testGenerateHashItemsDataEmpty()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods(['getItemsData'])
            ->getMock();

        $itemsData = array(
            'ZTMC' => 'ZTMC'
        );

        $bjItemsData = new BjItemsData();
        $bjItemsData->setData($itemsData);

        $this->searchData->expects($this->once())
                    ->method('getItemsData')
                    ->willReturn($bjItemsData);

        $this->searchData->generateHash();

        $hash = md5(base64_encode(gzcompress(serialize($itemsData))));

        $this->assertEquals($hash, $this->searchData->getHash());
    }

    public function testValidateFalse()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods([
                'isSourceUnitNull',
                'isTemplateNull',
                'isHashExist',
                'isSubjectCategoryExist'
            ])
            ->getMock();

        $this->searchData->expects($this->once())
            ->method('isSourceUnitNull')
            ->willReturn(false);

        $result = $this->searchData->validate();

        $this->assertFalse($result);
    }

    public function testValidateTrue()
    {
        $this->searchData = $this->getMockBuilder(MockSearchData::class)
            ->setMethods([
                'isSourceUnitNull',
                'isTemplateNull',
                'isHashExist',
                'isSubjectCategoryExist'
            ])
            ->getMock();

        $this->searchData->expects($this->once())
                ->method('isSourceUnitNull')
                ->willReturn(true);

        $this->searchData->expects($this->once())
            ->method('isTemplateNull')
            ->willReturn(true);

        $this->searchData->expects($this->once())
            ->method('isHashExist')
            ->willReturn(true);

        $this->searchData->expects($this->once())
            ->method('isSubjectCategoryExist')
            ->willReturn(true);
        
        $result = $this->searchData->validate();

        $this->assertTrue($result);
    }

    public function testAdd()
    {
        $result = $this->searchData->add();
        $this->assertFalse($result);
    }

    public function testEdit()
    {
        $result = $this->searchData->edit();
        $this->assertFalse($result);
    }
}

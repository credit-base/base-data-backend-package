<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Translator\BjItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\BjItemsDataRowCacheQuery;

class MockBjItemsDataDbAdapter extends BjItemsDataDbAdapter
{
    public function getDbTranslator() : BjItemsDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : BjItemsDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

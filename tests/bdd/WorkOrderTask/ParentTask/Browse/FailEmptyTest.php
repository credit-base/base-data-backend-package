<?php
namespace BaseData\WorkOrderTask\ParentTask\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;

/**
 * @Feature: 我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务,
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 查看工单任务列表-异常数据不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在工单任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看工单任务列表时
     */
    public function fetchParentTaskList()
    {
        $repository = new ParentTaskRepository();

        list($parentTaskList, $count) = $repository->filter([]);
        
        unset($count);

        return $parentTaskList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewParentTask()
    {
        $parentTaskList = $this->fetchParentTaskList();

        $this->assertEmpty($parentTaskList);
    }
}

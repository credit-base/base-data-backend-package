<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class GbItemsDataDb extends Db
{
    const TABLE = 'gb_items_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

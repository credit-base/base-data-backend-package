<?php
namespace BaseData\Rule;

use Marmot\Core;

use BaseData\Template\Model\Template;

use BaseData\Common\Model\IApproveAble;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Model\RuleService;

trait SetDataTrait
{
    protected function rule(
        $status = RuleService::STATUS['NORMAL']
    ) : array {
        $rules = array(IRule::RULE_NAME['TRANSFORMATION_RULE']=>array('ZTMC'=>'ZTMC', 'TYSHXYDM' => 'TYSHXYDM'));
        return
        [
            [
                'rule_id' => 1,
                'transformation_category' => Template::CATEGORY['BJ'],
                'transformation_template_id' => 1,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 1,
                'crew_id' => 1,
                'usergroup_id' => 1,
                'rules' => base64_encode(gzcompress(serialize($rules))),
                'version' => Core::$container->get('time'),
                'data_total' => 0,
                'status' => $status,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyFormRule(
        $applyStatus = IApproveAble::APPLY_STATUS['APPROVE'],
        $relationId = 1,
        $operationType = IApproveAble::OPERATION_TYPE['ADD']
    ) : array {
        $applyInfo = $this->rule()[0];
        
        return
        [
            [
                'apply_form_rule_id' => 1,
                'publish_crew_id' => 1,
                'usergroup_id' => 1,
                'apply_usergroup_id' => 1,
                'apply_crew_id' => 1,
                'operation_type' => $operationType,
                'apply_info' => base64_encode(gzcompress(serialize($applyInfo))),
                'reject_reason' => '驳回原因',
                'transformation_category' => Template::CATEGORY['BJ'],
                'transformation_template_id' => 1,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 1,
                'relation_id' => $relationId,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'apply_status' => $applyStatus,
                'status_time' => 0,
            ]
        ];
    }
}

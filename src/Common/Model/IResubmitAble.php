<?php
namespace BaseData\Common\Model;

interface IResubmitAble
{
    public function resubmit() : bool;
}

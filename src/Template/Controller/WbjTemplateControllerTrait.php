<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\CommandHandler\WbjTemplate\WbjTemplateCommandHandlerFactory;

trait WbjTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : WbjTemplateRepository
    {
        return new WbjTemplateRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new WbjTemplateCommandHandlerFactory());
    }
}

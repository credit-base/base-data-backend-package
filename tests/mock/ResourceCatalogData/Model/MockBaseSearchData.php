<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter;

class MockBaseSearchData extends BaseSearchData
{
    public function getRepository() : IBaseSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
}

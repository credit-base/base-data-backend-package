<?php
namespace BaseData\Rule\UnAuditedRuleService\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\RuleUtils;
use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\Model\UnAuditedRuleService;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试规则审核接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, RuleUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * 存在多条规则审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_apply_form_rule' => $this->applyFormRule()
            ]
        );
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function testViewUnAuditedRuleService()
    {
        $setUnAuditedRuleServiceList = $this->applyFormRule();

        $setWbjTemplateList = $this->wbjTemplate();
        foreach ($setWbjTemplateList as $setWbjTemplate) {
            $setWbjTemplateList[$setWbjTemplate['wbj_template_id']] = $setWbjTemplate;
        }
        $setGbTemplateList = $this->gbTemplate();
        foreach ($setGbTemplateList as $setGbTemplate) {
            $setGbTemplateList[$setGbTemplate['gb_template_id']] = $setGbTemplate;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedRules/1,2?include=transformationTemplate,sourceTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $unAuditedRuleService) {
            $this->unAuditedRuleCompareData($setUnAuditedRuleServiceList[$key], $unAuditedRuleService);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'gbTemplates') {
                $this->gbTemplateCompareData($setGbTemplateList[$value['id']], $value);
            }
            if ($value['type'] == 'wbjTemplates') {
                $this->wbjTemplateCompareData($setWbjTemplateList[$value['id']], $value);
            }
        }
    }
}

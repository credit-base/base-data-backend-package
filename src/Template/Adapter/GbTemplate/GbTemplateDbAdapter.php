<?php
namespace BaseData\Template\Adapter\GbTemplate;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\NullGbTemplate;
use BaseData\Template\Translator\GbTemplateDbTranslator;
use BaseData\Template\Adapter\GbTemplate\Query\GbTemplateRowCacheQuery;

class GbTemplateDbAdapter implements IGbTemplateAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new GbTemplateDbTranslator();
        $this->rowCacheQuery = new GbTemplateRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : GbTemplateDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : GbTemplateRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullGbTemplate::getInstance();
    }

    public function add(GbTemplate $gbTemplate) : bool
    {
        return $this->addAction($gbTemplate);
    }

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool
    {
        return $this->editAction($gbTemplate, $keys);
    }

    public function fetchOne($id) : GbTemplate
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $gbTemplate = new GbTemplate();

            if (isset($filter['name'])) {
                $gbTemplate->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($gbTemplate, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['identify'])) {
                $gbTemplate->setIdentify($filter['identify']);
                $info = $this->getDbTranslator()->objectToArray($gbTemplate, array('identify'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['dimension'])) {
                $gbTemplate->setDimension($filter['dimension']);
                $info = $this->getDbTranslator()->objectToArray($gbTemplate, array('dimension'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoClassify'])) {
                $gbTemplate->setInfoClassify($filter['infoClassify']);
                $info = $this->getDbTranslator()->objectToArray($gbTemplate, array('infoClassify'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoCategory'])) {
                $gbTemplate->setInfoCategory($filter['infoCategory']);
                $info = $this->getDbTranslator()->objectToArray($gbTemplate, array('infoCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new GbTemplate(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray(new GbTemplate(), array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

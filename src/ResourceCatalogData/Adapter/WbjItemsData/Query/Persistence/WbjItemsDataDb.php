<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class WbjItemsDataDb extends Db
{
    const TABLE = 'wbj_items_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class GbItemsDataSchemaTest extends TestCase
{
    private $gbItemsDataSchema;

    private $gbItemsData;

    public function setUp()
    {
        $this->gbItemsDataSchema = new GbItemsDataSchema(new Factory());

        $this->gbItemsData = \BaseData\ResourceCatalogData\Utils\GbItemsDataMockFactory::generateGbItemsData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->gbItemsDataSchema);
        unset($this->gbItemsData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->gbItemsDataSchema);
    }

    public function testGetId()
    {
        $result = $this->gbItemsDataSchema->getId($this->gbItemsData);

        $this->assertEquals($result, $this->gbItemsData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->gbItemsDataSchema->getAttributes($this->gbItemsData);

        $this->assertEquals($result['data'], $this->gbItemsData->getData());
    }
}

<?php
namespace BaseData\Common\Model;

class MockApproveAbleTrait implements IApproveAble
{
    use ApproveAbleTrait;

    public function approveAction() : bool
    {
        return false;
    }

    public function rejectAction() : bool
    {
        return false;
    }
}

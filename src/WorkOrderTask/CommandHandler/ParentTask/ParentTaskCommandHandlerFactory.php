<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class ParentTaskCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\WorkOrderTask\Command\ParentTask\AddParentTaskCommand' =>
        'BaseData\WorkOrderTask\CommandHandler\ParentTask\AddParentTaskCommandHandler',
        'BaseData\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand' =>
        'BaseData\WorkOrderTask\CommandHandler\ParentTask\RevokeParentTaskCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

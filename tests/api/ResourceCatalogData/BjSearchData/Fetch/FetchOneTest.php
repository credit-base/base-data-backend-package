<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试本级资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * 存在一条本级资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
                'pcore_bj_search_data' => $this->bjSearchData()['pcore_bj_search_data']
            ]
        );
    }

    public function testViewBjSearchData()
    {
        $setBjSearchData = $this->bjSearchData()['pcore_bj_search_data'][0];
        $setTemplate = $this->bjTemplate()[0];
        $setBjItemsData = $this->bjSearchData()['pcore_bj_items_data'][0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'bjSearchData/1?include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->bjSearchDataCompareData($setBjSearchData, $contents['data']);
        $this->bjItemsDataCompareData($setBjItemsData, $contents['included'][0]);
        $this->bjTemplateCompareData($setTemplate, $contents['included'][1]);
    }
}

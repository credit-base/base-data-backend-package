<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Repository\BjItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter;

class BjItemsData extends ItemsData
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjItemsDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IBjItemsDataAdapter
    {
        return $this->repository;
    }
}

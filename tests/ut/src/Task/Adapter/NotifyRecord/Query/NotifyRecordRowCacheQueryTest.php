<?php
namespace BaseData\Task\Adapter\NotifyRecord\Query;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NotifyRecordRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockNotifyRecordRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'BaseData\Task\Adapter\NotifyRecord\Query\Persistence\NotifyRecordCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'BaseData\Task\Adapter\NotifyRecord\Query\Persistence\NotifyRecordDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}

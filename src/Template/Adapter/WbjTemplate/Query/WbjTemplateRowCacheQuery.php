<?php
namespace BaseData\Template\Adapter\WbjTemplate\Query;

use Marmot\Framework\Query\RowCacheQuery;

class WbjTemplateRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'wbj_template_id',
            new Persistence\WbjTemplateCache(),
            new Persistence\WbjTemplateDb()
        );
    }
}

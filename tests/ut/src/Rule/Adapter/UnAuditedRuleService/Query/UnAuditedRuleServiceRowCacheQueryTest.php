<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query;

use PHPUnit\Framework\TestCase;

class UnAuditedRuleServiceRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockUnAuditedRuleServiceRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'BaseData\Rule\Adapter\UnAuditedRuleService\Query\Persistence\UnAuditedRuleServiceCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'BaseData\Rule\Adapter\UnAuditedRuleService\Query\Persistence\UnAuditedRuleServiceDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}

<?php
namespace BaseData\Template\Adapter\BjTemplate;

use BaseData\Template\Model\BjTemplate;

interface IBjTemplateAdapter
{
    public function fetchOne($id) : BjTemplate;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(BjTemplate $bjTemplate) : bool;

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool;
}

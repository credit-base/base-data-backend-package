<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use BaseData\ResourceCatalogData\Command\BjSearchData\ConfirmBjSearchDataCommand;
use BaseData\ResourceCatalogData\Command\BjSearchData\DisableBjSearchDataCommand;
use BaseData\ResourceCatalogData\Command\BjSearchData\DeleteBjSearchDataCommand;

class BjSearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new BjSearchDataCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testDisableBjSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableBjSearchDataCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\CommandHandler\BjSearchData\DisableBjSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testConfirmBjSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new ConfirmBjSearchDataCommand(
                $this->faker->randomNumber(3)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\CommandHandler\BjSearchData\ConfirmBjSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testDeleteBjSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DeleteBjSearchDataCommand(
                $this->faker->randomNumber(3)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\CommandHandler\BjSearchData\DeleteBjSearchDataCommandHandler',
            $commandHandler
        );
    }
}

<?php
namespace BaseData\Template\Model;

class MockTemplate extends Template
{
    public function getCategory() : int
    {
        return Template::CATEGORY['BJ'];
    }
}

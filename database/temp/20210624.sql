ALTER TABLE `pcore_ys_search_data` ADD COLUMN `task_id` int(10) NOT NULL COMMENT '父任务id';
ALTER TABLE `pcore_ys_search_data` ADD COLUMN `subtask_id` int(10) NOT NULL COMMENT '子任务id';
ALTER TABLE `pcore_ys_search_data` change `status` `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认正常),(STATUS_DISABLED -2 屏蔽)';

-- --------------------------------------------------------

ALTER TABLE `pcore_wbj_search_data` ADD COLUMN `task_id` int(10) NOT NULL COMMENT '父任务id';
ALTER TABLE `pcore_wbj_search_data` ADD COLUMN `subtask_id` int(10) NOT NULL COMMENT '子任务id';
ALTER TABLE `pcore_wbj_search_data` change `status` `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认正常),(STATUS_DISABLED -2 屏蔽)';

-- --------------------------------------------------------

ALTER TABLE `pcore_bj_search_data` ADD COLUMN `task_id` int(10) NOT NULL COMMENT '父任务id';
ALTER TABLE `pcore_bj_search_data` ADD COLUMN `subtask_id` int(10) NOT NULL COMMENT '子任务id';
ALTER TABLE `pcore_bj_search_data` ADD COLUMN `front_end_processor_status` tinyint(1) NOT NULL COMMENT '前置机数据导入状态 0 未导入 2 导入';
ALTER TABLE `pcore_bj_search_data` ADD COLUMN `description` text NOT NULL COMMENT '待确认规则描述';
ALTER TABLE `pcore_bj_search_data` change `status` `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_CONFIRM 0 待确认),(STATUS_ENABLED 2 已确认),(STATUS_DISABLED -2 屏蔽),(STATUS_DELETED -4 封存)';

-- --------------------------------------------------------

ALTER TABLE `pcore_gb_search_data` ADD COLUMN `task_id` int(10) NOT NULL COMMENT '父任务id';
ALTER TABLE `pcore_gb_search_data` ADD COLUMN `subtask_id` int(10) NOT NULL COMMENT '子任务id';
ALTER TABLE `pcore_gb_search_data` ADD COLUMN `front_end_processor_status` tinyint(1) NOT NULL COMMENT '前置机数据导入状态 0 未导入 2 导入';
ALTER TABLE `pcore_gb_search_data` ADD COLUMN `description` text NOT NULL COMMENT '待确认规则描述';
ALTER TABLE `pcore_gb_search_data` change `status` `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_CONFIRM 0 待确认),(STATUS_ENABLED 2 已确认),(STATUS_DISABLED -2 屏蔽),(STATUS_DELETED -4 封存)';



 
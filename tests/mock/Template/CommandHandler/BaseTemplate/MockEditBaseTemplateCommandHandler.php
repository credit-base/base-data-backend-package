<?php
namespace BaseData\Template\CommandHandler\BaseTemplate;

use BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class MockEditBaseTemplateCommandHandler extends EditBaseTemplateCommandHandler
{
    public function getBaseTemplateRepository() : IBaseTemplateAdapter
    {
        return parent::getBaseTemplateRepository();
    }
}

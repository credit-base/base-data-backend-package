<?php
namespace BaseData\Task\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullTask extends Task implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function add(array $subTasks = array()) : bool
    {
        unset($subTasks);
        return $this->resourceNotExist();
    }

    public function edit() : bool
    {
        return $this->resourceNotExist();
    }

    public function scheduled() : bool
    {
        return $this->resourceNotExist();
    }

    public function execute() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    protected function executeAction() : bool
    {
        return false;
    }
}

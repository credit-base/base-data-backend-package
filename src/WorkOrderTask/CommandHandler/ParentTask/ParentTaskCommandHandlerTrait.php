<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Model\GbTemplate;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Model\BjTemplate;

use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use BaseData\UserGroup\Repository\UserGroupRepository;

trait ParentTaskCommandHandlerTrait
{
    protected function getGbTemplateRepository() : IGbTemplateAdapter
    {
        return new GbTemplateRepository();
    }

    protected function getBjTemplateRepository() : IBjTemplateAdapter
    {
        return new BjTemplateRepository();
    }
    
    protected function getUserGroupRepository() : IUserGroupAdapter
    {
        return new UserGroupRepository();
    }

    protected function fetchOneGbTemplate($id) : GbTemplate
    {
        return $this->getGbTemplateRepository()->fetchOne($id);
    }

    protected function fetchOneBjTemplate($id) : BjTemplate
    {
        return $this->getBjTemplateRepository()->fetchOne($id);
    }

    protected function fetchListUserGroup(array $ids) : array
    {
        return $this->getUserGroupRepository()->fetchList($ids);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\NullBjSearchData;
use BaseData\ResourceCatalogData\Repository\BjItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\SearchDataDbAdapterTrait;
use BaseData\ResourceCatalogData\Translator\BjSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\BjSearchDataRowCacheQuery;

use BaseData\Template\Repository\BjTemplateRepository;

class BjSearchDataDbAdapter implements IBjSearchDataAdapter
{
    use DbAdapterTrait, SearchDataDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $bjItemsDataRepository;

    private $bjTemplateRepository;

    public function __construct()
    {
        $this->dbTranslator = new BjSearchDataDbTranslator();
        $this->rowCacheQuery = new BjSearchDataRowCacheQuery();
        $this->bjItemsDataRepository = new BjItemsDataRepository();
        $this->bjTemplateRepository = new BjTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->bjItemsDataRepository);
        unset($this->bjTemplateRepository);
    }
    
    protected function getDbTranslator() : BjSearchDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : BjSearchDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullBjSearchData::getInstance();
    }

    protected function getItemsDataRepository() : BjItemsDataRepository
    {
        return $this->bjItemsDataRepository;
    }

    protected function getTemplateRepository() : BjTemplateRepository
    {
        return $this->bjTemplateRepository;
    }

    public function add(BjSearchData $bjSearchData) : bool
    {
        return $this->addAction($bjSearchData);
    }

    public function edit(BjSearchData $bjSearchData, array $keys = array()) : bool
    {
        return $this->editAction($bjSearchData, $keys);
    }

    public function fetchOne($id) : BjSearchData
    {
        $bjSearchData = $this->fetchOneAction($id);
        $this->fetchTemplate($bjSearchData);
        $this->fetchItemsData($bjSearchData);
        
        return $bjSearchData;
    }

    public function fetchList(array $ids) : array
    {
        $bjSearchDataList = $this->fetchListAction($ids);
        $this->fetchTemplate($bjSearchDataList);
        $this->fetchItemsData($bjSearchDataList);

        return $bjSearchDataList;
    }

    protected function formatFilter(array $filter) : string
    {
        $bjSearchData = new BjSearchData();

        return $this->searchDataFormatFilter($filter, $bjSearchData);
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $bjSearchData = new BjSearchData();
 
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray($bjSearchData, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($bjSearchData, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

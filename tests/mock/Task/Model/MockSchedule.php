<?php
namespace BaseData\Task\Model;

use BaseData\Task\Adapter\Task\ITaskAdapter;

class MockSchedule extends Schedule
{
    public function getRepository() : ITaskAdapter
    {
        return parent::getRepository();
    }

    public function getRunTime() : RunTime
    {
        return parent::getRunTime();
    }

    public function dispatch() : array
    {
        return parent::dispatch();
    }
}

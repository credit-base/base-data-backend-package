<?php
namespace BaseData\Rule\Model;

class ModelFactory
{
    const MAPS = array(
        IRule::RULE_NAME['TRANSFORMATION_RULE'] =>
        'BaseData\Rule\Model\TransformationRule',
        IRule::RULE_NAME['COMPLETION_RULE'] =>
        'BaseData\Rule\Model\CompletionRule',
        IRule::RULE_NAME['COMPARISON_RULE'] =>
        'BaseData\Rule\Model\ComparisonRule',
        IRule::RULE_NAME['DE_DUPLICATION_RULE'] =>
        'BaseData\Rule\Model\DeDuplicationRule',
    );

    public function getModel($category)
    {
        $model = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($model) ? new $model : NullRule::getInstance();
    }
}

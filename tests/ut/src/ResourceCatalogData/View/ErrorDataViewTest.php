<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\ErrorData;

class ErrorDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $errorData = new ErrorDataView(new ErrorData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $errorData);
    }
}

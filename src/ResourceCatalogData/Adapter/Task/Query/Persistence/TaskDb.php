<?php
namespace BaseData\ResourceCatalogData\Adapter\Task\Query\Persistence;

use Marmot\Framework\Classes\Db;

class TaskDb extends Db
{
    const TABLE = 'task';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

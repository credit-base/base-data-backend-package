<?php
namespace BaseData\Template\Model;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;

class MockBjTemplate extends BjTemplate
{
    public function getRepository() : IBjTemplateAdapter
    {
        return parent::getRepository();
    }
}

<?php
namespace BaseData\Template\Utils;

use BaseData\Template\Model\Template;

trait TemplateMockFactoryTrait
{
    protected static function generateName(Template $template, $faker, $value) : void
    {
        $name = isset($value['name']) ?
            $value['name'] :
            $faker->word;
        
        $template->setName($name);
    }

    protected static function generateIdentify(Template $template, $faker, $value) : void
    {
        $identify = isset($value['identify']) ?
            $value['identify'] :
            $faker->regexify('[A-Z_]{1,100}');
        
        $template->setIdentify($identify);
    }

    protected static function generateSubjectCategory(Template $template, $faker, $value) : void
    {
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            $faker->randomElements(array(1,2,3), 2);
        
        $template->setSubjectCategory($subjectCategory);
    }

    protected static function generateDimension(Template $template, $faker, $value) : void
    {
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            $faker->randomElement(array(1,2,3));
        
        $template->setDimension($dimension);
    }

    protected static function generateExchangeFrequency(Template $template, $faker, $value) : void
    {
        $exchangeFrequency = isset($value['exchangeFrequency']) ?
            $value['exchangeFrequency'] :
            $faker->randomDigit;
        
        $template->setExchangeFrequency($exchangeFrequency);
    }

    protected static function generateInfoClassify(Template $template, $faker, $value) : void
    {
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            $faker->randomDigit;
        
        $template->setInfoClassify($infoClassify);
    }

    protected static function generateInfoCategory(Template $template, $faker, $value) : void
    {
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            $faker->randomDigit;
        
        $template->setInfoCategory($infoCategory);
    }

    protected static function generateDescription(Template $template, $faker, $value) : void
    {
        $description = isset($value['description']) ?
            $value['description'] :
            $faker->sentence;
        
        $template->setDescription($description);
    }

    protected static function generateItems(Template $template, $faker, $value) : void
    {
        $items = isset($value['items']) ?
            $value['items'] :
            $faker->randomElements(array('1','2','3'), 2);
        
        $template->setItems($items);
    }

    protected static function generateStatus(Template $template, $faker, $value) : void
    {
        unset($faker);
        $status = isset($value['status']) ?
            $value['status'] :
            0;
        
        $template->setStatus($status);
    }
}

<?php
namespace BaseData\Task\Model;

use Marmot\Core;

//值对象 - 运行时间

class RunTime
{
    protected $startTime;

    protected $endTime;

    public function __construct()
    {
        $this->startTime = Core::$container->get('time');
        $this->endTime = 0;
    }

    public function updateEndTime()
    {
        $this->endTime = Core::$container->get('time');
    }

    public function getStartTime() : int
    {
        return $this->startTime;
    }

    public function getEndTime() : int
    {
        return $this->endTime;
    }
}

<?php
namespace BaseData\Statistical\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Statistical\Model\Statistical;

class EnterpriseRelationInformationCountTranslatorTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new EnterpriseRelationInformationCountTranslator();
        parent::setUp();
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    /**
     * 测试翻译数组转换对象
     */
    public function testArrayToObjectCorrectArray()
    {
        $faker = \Faker\Factory::create('zh_CN');
        //保证每次生成数据一致
        $faker->seed(1000);

        $expression = array();

        $expression = array(
            'licensing_total' => $faker->randomDigit(),
            'penalty_total' => $faker->randomDigit(),
            'incentive_total' => $faker->randomDigit(),
            'punish_total' => $faker->randomDigit(),
        );

        $statistical = $this->translator->arrayToObject($expression);

        $this->assertInstanceof('BaseData\Statistical\Model\Statistical', $statistical);

        $result = $statistical->getResult();

        $this->assertEquals($expression['licensing_total'], $result['licensingTotal']);
        $this->assertEquals($expression['penalty_total'], $result['penaltyTotal']);
        $this->assertEquals($expression['incentive_total'], $result['incentiveTotal']);
        $this->assertEquals($expression['punish_total'], $result['punishTotal']);
    }

    public function testObjectToArray()
    {
        $statistical = array();
        $keys = array();

        $result = $this->translator->objectToArray($statistical, $keys);
        $this->assertEquals(array(), $result);
    }
}

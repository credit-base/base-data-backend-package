<?php
namespace BaseData\Task\Adapter\NotifyRecord\Query;

use Marmot\Framework\Query\RowCacheQuery;

class NotifyRecordRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'record_id',
            new Persistence\NotifyRecordCache(),
            new Persistence\NotifyRecordDb()
        );
    }
}

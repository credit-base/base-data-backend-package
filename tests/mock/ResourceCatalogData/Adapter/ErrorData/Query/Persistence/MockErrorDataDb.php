<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData\Query\Persistence;

class MockErrorDataDb extends ErrorDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

<?php
namespace BaseData\Task\Adapter\Task\Query\Persistence;

class MockTaskDb extends TaskDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

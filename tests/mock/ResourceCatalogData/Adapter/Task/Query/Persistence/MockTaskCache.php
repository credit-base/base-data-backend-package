<?php
namespace BaseData\ResourceCatalogData\Adapter\Task\Query\Persistence;

class MockTaskCache extends TaskCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

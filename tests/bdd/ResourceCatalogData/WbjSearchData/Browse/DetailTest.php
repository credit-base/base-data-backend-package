<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的委办局数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_wbj_items_data');
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * @Given: 存在一条资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data'],
            'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data']
        ]);
    }

    /**
     * @When: 当我查看该条资源目录数据详情时
     */
    public function fetchWbjSearchData($id)
    {
        $repository = new WbjSearchDataRepository();

        $wbjSearchData = $repository->fetchOne($id);

        return $wbjSearchData;
    }

    /**
     * @Then: 我可以看见该条资源目录数据的全部信息
     */
    public function testViewWbjSearchDataList()
    {
        $id = 1;
        
        $setWbjSearchData = $this->wbjSearchData()['pcore_wbj_search_data'][0];
        $setWbjSearchData['wbj_search_data_id'] = $id;

        $wbjSearchData = $this->fetchWbjSearchData($id);
        $translator = new WbjSearchDataDbTranslator();
        $wbjSearchDataArray = $translator->objectToArray($wbjSearchData);

        $this->assertEquals($wbjSearchDataArray, $setWbjSearchData);
    }
}

<?php
namespace BaseData\WorkOrderTask\WidgetRule;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

/**
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new WorkOrderTaskWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //feedbackRecords -- start
    /**
     * @dataProvider invalidFeedbackRecordsProvider
     */
    public function testFeedbackRecordsInvalid($actual, $expected)
    {
        $result = $this->widgetRule->feedbackRecords($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidFeedbackRecordsProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array(array(), false),
            array($faker->word, false),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '主体名称',    //信息项名称
                                "identify" => 'ZTMC',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '200',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(),    //脱敏规则
                                "remarks" => '信用主体名称',    //备注
                            )
                        ),  //信息项
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 'string',  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '主体名称',    //信息项名称
                                "identify" => 'ZTMC',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '200',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(),    //脱敏规则
                                "remarks" => '信用主体名称',    //备注
                            ),
                        ),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 'string',  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '统一社会信用代码',    //信息项名称
                                "identify" => 'TYSHXYDM',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '50',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                                "remarks" => '信用主体代码',    //备注
                            )
                        ),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 1,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => 100,  //目录id
                        'items' => array(),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 3,  //是否已存在目录，是 1 | 否 0
                        'templateId' => 100,  //目录id
                        'items' => array(),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 1,  //是否已存在目录，是 1 | 否 0
                        'templateId' => 100,  //目录id
                        'items' => array(array('items')),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => 'items',  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(array('items')),  //信息项
                        'reason' => '',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '主体名称',    //信息项名称
                                "identify" => 'ZTMC',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '200',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(),    //脱敏规则
                                "remarks" => '信用主体名称',    //备注
                            ),
                            array(
                                "name" => '身份证号',    //信息项名称
                                "identify" => 'ZJHM',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '20',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                                "remarks" => '信用主体代码',    //备注
                            )
                        ),  //信息项
                        'reason' => '',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '主体名称',    //信息项名称
                                "identify" => 'ZTMC',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '200',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(),    //脱敏规则
                                "remarks" => '信用主体名称',    //备注
                            ),
                            array(
                                "name" => '身份证号',    //信息项名称
                                "identify" => 'SFZH',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '18',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                                "remarks" => '信用主体代码',    //备注
                            )
                        ),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                false
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '主体名称',    //信息项名称
                                "identify" => 'ZTMC',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '200',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(),    //脱敏规则
                                "remarks" => '信用主体名称',    //备注
                            ),
                            array(
                                "name" => '身份证号',    //信息项名称
                                "identify" => 'ZJHM',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '50',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                                "remarks" => '信用主体代码',    //备注
                            )
                        ),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                true
            ),
            array(
                array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '主体名称',    //信息项名称
                                "identify" => 'ZTMC',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '200',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(),    //脱敏规则
                                "remarks" => '主体名称',    //备注
                            ),
                            array(
                                "name" => '统一社会信用代码',    //信息项名称
                                "identify" => 'TYSHXYDM',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '18',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                                "remarks" => '统一社会信用代码',    //备注
                            )
                        ),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                ),
                true
            )
        );
    }
    //feedbackRecords -- end
}

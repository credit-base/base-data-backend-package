<?php
namespace BaseData\Task\Script;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Service\SyncTaskService;

class SyncTaskTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockSyncTask();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsOnceTask()
    {
        $this->assertInstanceof("BaseData\Task\Model\OnceTask", $this->stub);
    }

    public function testGetSyncTaskService()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Service\SyncTaskService',
            $this->stub->getSyncTaskService('')
        );
    }

    public function testGetSyncSubtask()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Script\SyncSubtask',
            $this->stub->getSyncSubtask()
        );
    }

    public function testGetSyncErrorDataDownload()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Script\SyncErrorDataDownload',
            $this->stub->getSyncErrorDataDownload()
        );
    }

    public function testExecuteAction()
    {
        $rule = $this->getMockBuilder(MockSyncTask::class)
            ->setMethods([
                'getData',
                'getSyncTaskService',
                'getSyncSubtask',
                'getSyncErrorDataDownload'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('getData')
             ->willReturn(array('XZXK_1_1_12122.xsl'));

        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
        $task->setStatus(Task::STATUS['FAILURE']);
        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService(1);
        $ruleServiceList = array($ruleService);

        $syncTaskService = $this->prophesize(SyncTaskService::class);
        $syncTaskService->execute()->shouldBeCalledTimes(1)->willReturn([$ruleServiceList, $task]);
        $rule->expects($this->once())->method('getSyncTaskService')->willReturn($syncTaskService->reveal());

        $syncErrorDataDownload = $this->prophesize(SyncErrorDataDownload::class);
        $syncErrorDataDownload->setData(Argument::exact(
            array('task' => $task->getId())
        ))->shouldBeCalledTimes(1);
        $syncErrorDataDownload->add()->shouldBeCalledTimes(1);
        $rule->expects($this->once())->method('getSyncErrorDataDownload')->willReturn($syncErrorDataDownload->reveal());

        $syncSubtask = $this->prophesize(SyncSubtask::class);
        $syncSubtask->setData(Argument::exact(
            array('task' => $task->getId(), 'ruleService' => $ruleService->getId())
        ))->shouldBeCalledTimes(1);
        $syncSubtask->add()->shouldBeCalledTimes(1);
        $rule->expects($this->once())->method('getSyncSubtask')->willReturn($syncSubtask->reveal());

        $result = $rule->executeAction();

        $this->assertTrue($result);
    }
}

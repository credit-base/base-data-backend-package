<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Model\NullCrew;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Model\NullUserGroup;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\NullBjTemplate;
use BaseData\Template\Model\NullWbjTemplate;

use BaseData\Rule\Model\Rule;
use BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter;
use BaseData\Rule\Repository\RuleServiceRepository;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;

/**
 * BaseData\Rule\Model\RuleService.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @author chloroplast
 */
class RuleServiceTest extends TestCase
{
    private $ruleService;

    public function setUp()
    {
        $this->ruleService = new MockRuleService();
    }

    public function tearDown()
    {
        unset($this->ruleService);
    }

    //constructor
    public function testConstructor()
    {
        $this->assertEquals(0, $this->ruleService->getId());
        $this->assertInstanceof('BaseData\Template\Model\Template', $this->ruleService->getTransformationTemplate());
        $this->assertInstanceof('BaseData\Template\Model\Template', $this->ruleService->getSourceTemplate());
        $this->assertEquals([], $this->ruleService->getRules());
        $this->assertEquals(0, $this->ruleService->getVersion());
        $this->assertEquals(0, $this->ruleService->getDataTotal());
        $this->assertInstanceof('BaseData\Crew\Model\Crew', $this->ruleService->getCrew());
        $this->assertInstanceof('BaseData\UserGroup\Model\UserGroup', $this->ruleService->getUserGroup());
        $this->assertEquals(Template::CATEGORY['BJ'], $this->ruleService->getTransformationCategory());
        $this->assertEquals(Template::CATEGORY['WBJ'], $this->ruleService->getSourceCategory());
        $this->assertEquals(RuleService::STATUS['NORMAL'], $this->ruleService->getStatus());
        $this->assertEquals(0, $this->ruleService->getStatusTime());
        $this->assertEquals(Core::$container->get('time'), $this->ruleService->getCreateTime());
        $this->assertEquals(Core::$container->get('time'), $this->ruleService->getUpdateTime());
        $this->assertInstanceof(
            'BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->ruleService->getRepository()
        );
    }

    //id
    public function testSetId()
    {
        $this->ruleService->setId(1);
        $this->assertEquals(1, $this->ruleService->getId());
    }

    //transformationTemplate -- 开始
    /**
     * 设置 RuleService setTransformationTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTransformationTemplateCorrectType()
    {
        $template = new Template();

        $this->ruleService->setTransformationTemplate($template);
        $this->assertEquals($template, $this->ruleService->getTransformationTemplate());
    }
    /**
     * 设置 RuleService setTransformationTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransformationTemplateWrongType()
    {
        $this->ruleService->setTransformationTemplate('template');
    }
    //transformationTemplate -- 结束

    //sourceTemplate -- 开始
    /**
     * 设置 RuleService setSourceTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetSourceTemplateCorrectType()
    {
        $template = new Template();

        $this->ruleService->setSourceTemplate($template);
        $this->assertEquals($template, $this->ruleService->getSourceTemplate());
    }
    /**
     * 设置 RuleService setSourceTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceTemplateWrongType()
    {
        $this->ruleService->setSourceTemplate('template');
    }
    //sourceTemplate -- 结束

    //rules
    /**
     * 设置 RuleService setRules() 正确的传参类型,期望传值正确
     */
    public function testSetDataCorrectType()
    {
        $data = [];

        $this->ruleService->setRules($data);
        $this->assertEquals($data, $this->ruleService->getRules());
    }
    /**
     * 设置 RuleService setRules() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataWrongType()
    {
        $this->ruleService->setRules('data');
    }

    //version
    /**
     * 设置 RuleService setVersion() 正确的传参类型,期望传值正确
     */
    public function testSetNiceCorrectType()
    {
        $version = 1;
        $this->ruleService->setVersion($version);
        $this->assertEquals($version, $this->ruleService->getVersion());
    }
    /**
     * 设置 RuleService setVersion() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNiceWrongType()
    {
        $this->ruleService->setVersion('version');
    }

    //dataTotal
    /**
     * 设置 RuleService setDataTotal() 正确的传参类型,期望传值正确
     */
    public function testSetDataTotalCorrectType()
    {
        $dataTotal = 1;
        $this->ruleService->setDataTotal($dataTotal);
        $this->assertEquals($dataTotal, $this->ruleService->getDataTotal());
    }
    /**
     * 设置 RuleService setDataTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataTotalWrongType()
    {
        $this->ruleService->setDataTotal('dataTotal');
    }

    //crew
    /**
     * 设置 RuleService setCrew() 正确的传参类型,期望传值正确
     */
    public function testSetCrewCorrectType()
    {
        $crew = new Crew();

        $this->ruleService->setCrew($crew);
        $this->assertEquals($crew, $this->ruleService->getCrew());
    }
    /**
     * 设置 RuleService setCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCrewWrongType()
    {
        $this->ruleService->setCrew('crew');
    }
    //userGroup
    /**
     * 设置 RuleService setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $userGroup = new UserGroup();

        $this->ruleService->setUserGroup($userGroup);
        $this->assertEquals($userGroup, $this->ruleService->getUserGroup());
    }
    /**
     * 设置 RuleService setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->ruleService->setUserGroup('userGroup');
    }

    //transformationCategory
    /**
     * 循环测试 RuleService setTransformationCategory() 是否符合预定范围
     *
     * @dataProvider transformationCategoryProvider
     */
    public function testSetTransformationCategory($actual, $expected)
    {
        $this->ruleService->setTransformationCategory($actual);
        $this->assertEquals($expected, $this->ruleService->getTransformationCategory());
    }
    /**
     * 循环测试 RuleService setTransformationCategory() 数据构建器
     */
    public function transformationCategoryProvider()
    {
        return array(
            array(Template::CATEGORY['WBJ'], Template::CATEGORY['WBJ']),
            array(Template::CATEGORY['BJ'], Template::CATEGORY['BJ']),
            array(Template::CATEGORY['GB'], Template::CATEGORY['GB']),
            array(Template::CATEGORY['QZJ_WBJ'], Template::CATEGORY['QZJ_WBJ']),
            array(Template::CATEGORY['QZJ_BJ'], Template::CATEGORY['QZJ_BJ']),
            array(Template::CATEGORY['QZJ_GB'], Template::CATEGORY['QZJ_GB']),
            array(999, Template::CATEGORY['BJ']),
        );
    }
    /**
     * 设置 RuleService setTransformationCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransformationCategoryWrongType()
    {
        $this->ruleService->setTransformationCategory('string');
    }
    //sourceCategory
    /**
     * 循环测试 RuleService setSourceCategory() 是否符合预定范围
     *
     * @dataProvider sourceCategoryProvider
     */
    public function testSetSourceCategory($actual, $expected)
    {
        $this->ruleService->setSourceCategory($actual);
        $this->assertEquals($expected, $this->ruleService->getSourceCategory());
    }
    /**
     * 循环测试 RuleService setSourceCategory() 数据构建器
     */
    public function sourceCategoryProvider()
    {
        return array(
            array(Template::CATEGORY['WBJ'], Template::CATEGORY['WBJ']),
            array(Template::CATEGORY['BJ'], Template::CATEGORY['BJ']),
            array(Template::CATEGORY['GB'], Template::CATEGORY['GB']),
            array(Template::CATEGORY['QZJ_WBJ'], Template::CATEGORY['QZJ_WBJ']),
            array(Template::CATEGORY['QZJ_BJ'], Template::CATEGORY['QZJ_BJ']),
            array(Template::CATEGORY['QZJ_GB'], Template::CATEGORY['QZJ_GB']),
            array(999, Template::CATEGORY['WBJ']),
        );
    }
    /**
     * 设置 RuleService setSourceCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceCategoryWrongType()
    {
        $this->ruleService->setSourceCategory('string');
    }

    //status
    /**
     * 循环测试 RuleService setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->ruleService->setStatus($actual);
        $this->assertEquals($expected, $this->ruleService->getStatus());
    }
    /**
     * 循环测试 RuleService setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(RuleService::STATUS['NORMAL'],RuleService::STATUS['NORMAL']),
            array(RuleService::STATUS['DELETED'],RuleService::STATUS['DELETED']),
            array(999,RuleService::STATUS['NORMAL']),
        );
    }
    /**
     * 设置 RuleService setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->ruleService->setStatus('string');
    }

    //isNormal -- 开始
    public function testIsNormalTrue()
    {
        $this->ruleService->setStatus(RuleService::STATUS['NORMAL']);
        $result = $this->ruleService->isNormal();
        $this->assertTrue($result);
    }

    public function testIsNormalFail()
    {
        $this->ruleService->setStatus(RuleService::STATUS['DELETED']);
        $result = $this->ruleService->isNormal();
        $this->assertFalse($result);
    }
    //isNormal -- 结束

    //delete -- 开始
    public function testDeleteFail()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)->setMethods(['isNormal'])->getMock();

        $ruleService->expects($this->once())->method('isNormal')->willReturn(false);

        $result = $ruleService->delete();
        
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'status'), Core::getLastError()->getSource());
    }

    public function testDeleteSuccess()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
                    ->setMethods([
                        'isNormal',
                        'getRepository'
                    ])
                    ->getMock();

        $ruleService->setStatus(RuleService::STATUS['DELETED']);
        $ruleService->setStatusTime(Core::$container->get('time'));
        $ruleService->setUpdateTime(Core::$container->get('time'));

        $ruleService->expects($this->once())->method('isNormal')->willReturn(true);

        $repository = $this->prophesize(IRuleServiceAdapter::class);

        $repository->edit(
            Argument::exact($ruleService),
            Argument::exact(array(
                'updateTime',
                'status',
                'statusTime',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $ruleService->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        //揭示
        $result = $ruleService->delete();
        
        $this->assertTrue($result);
    }
    //delete -- 结束

    //addAction -- 开始
    public function testAddActionSuccess()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'validate',
                'getRepository',
                'updateTemplateRuleCount'
            ])
            ->getMock();

        $ruleService->expects($this->once())->method('validate')->willReturn(true);
        $ruleService->expects($this->once())->method('updateTemplateRuleCount')->willReturn(true);

        $repository = $this->prophesize(IRuleServiceAdapter::class);
        $repository->add(Argument::exact($ruleService))->shouldBeCalledTimes(1)->willReturn(true);
         //绑定
        $ruleService->expects($this->once()) ->method('getRepository')->willReturn($repository->reveal());

        $result = $ruleService->addAction();
        $this->assertTrue($result);
    }

    public function testAddActionValidateFail()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'validate',
                'getRepository',
                'updateTemplateRuleCount'
            ])
            ->getMock();

        $ruleService->expects($this->once())->method('validate')->willReturn(false);
        $ruleService->expects($this->exactly(0))->method('updateTemplateRuleCount');
         //绑定
        $ruleService->expects($this->exactly(0))->method('getRepository');

        $result = $ruleService->addAction();
        $this->assertFalse($result);
    }
    //addAction -- 结束

    //editAction -- 开始
    //success
    public function testEditActionSuccess()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'validate',
                'getRepository'
            ])
            ->getMock();

        $ruleService->setUpdateTime(Core::$container->get('time'));
        $ruleService->expects($this->once())->method('validate')->willReturn(true);

        $repository = $this->prophesize(IRuleServiceAdapter::class);

        $repository->edit(
            Argument::exact($ruleService),
            Argument::exact(array(
                'rules',
                'crew',
                'version',
                'userGroup',
                'updateTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

         //绑定
        $ruleService->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $ruleService->editAction();
        $this->assertTrue($result);
    }
    //fail
    public function testEditActionValidateFalse()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'validate',
                'getRepository'
            ])
            ->getMock();

        $ruleService->setUpdateTime(Core::$container->get('time'));
        $ruleService->expects($this->once())->method('validate')->willReturn(false);
        $ruleService->expects($this->exactly(0))->method('getRepository');

        $result = $ruleService->editAction();
        $this->assertFalse($result);
    }
    //editAction -- 结束

    //validate -- 开始
    public function testValidateSuccess()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'commonValidate',
                'rulesValidate'
            ])
            ->getMock();

        $ruleService->expects($this->once())->method('commonValidate')->willReturn(true);
        $ruleService->expects($this->once())->method('rulesValidate')->willReturn(true);

        $result = $ruleService->validate();
        $this->assertTrue($result);
    }

    public function testValidateFail()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'commonValidate',
                'rulesValidate'
            ])
            ->getMock();

        $ruleService->expects($this->once())->method('commonValidate')->willReturn(false);
        $ruleService->expects($this->exactly(0))->method('rulesValidate');

        $result = $ruleService->validate();
        $this->assertFalse($result);
    }
    //success
    //fail
    //validate -- 结束

    //commonValidate -- 开始
    //success
    public function testCommonValidateSuccess()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'isCrewExist',
                'isSourceTemplateExist',
                'isTransformationTemplateExist',
                'isRuleNotExist',
                'isRuleFormatCorrect'
            ])
            ->getMock();

        $ruleService->expects($this->once())->method('isCrewExist')->willReturn(true);
        $ruleService->expects($this->once())->method('isSourceTemplateExist')->willReturn(true);
        $ruleService->expects($this->once())->method('isTransformationTemplateExist')->willReturn(true);
        $ruleService->expects($this->once())->method('isRuleNotExist')->willReturn(true);
        $ruleService->expects($this->once())->method('isRuleFormatCorrect')->willReturn(true);

        $result = $ruleService->validate();
        $this->assertTrue($result);
    }

    //faile
    public function testCommonValidateFail()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'isCrewExist',
                'isSourceTemplateExist',
                'isTransformationTemplateExist',
                'isRuleNotExist',
                'isRuleFormatCorrect'
            ])
            ->getMock();

        $ruleService->expects($this->once())->method('isCrewExist')->willReturn(false);
        $ruleService->expects($this->exactly(0))->method('isSourceTemplateExist');
        $ruleService->expects($this->exactly(0))->method('isTransformationTemplateExist');
        $ruleService->expects($this->exactly(0))->method('isRuleNotExist');
        $ruleService->expects($this->exactly(0))->method('isRuleFormatCorrect');

        $result = $ruleService->validate();
        $this->assertFalse($result);
    }
    //commonValidate -- 结束

    //isCrewExist
    public function testIsCrewExistSuccess()
    {
        $crew = new Crew();
        $ruleService = new MockRuleService();

        $ruleService->setCrew($crew);
        $result = $ruleService->isCrewExist();

        $this->assertTrue($result);
    }

    public function testIsCrewExistFail()
    {
        $crew = new NullCrew();
        $ruleService = new MockRuleService();

        $ruleService->setCrew($crew);
        $result = $ruleService->isCrewExist();

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'crewId'), Core::getLastError()->getSource());
    }

    //isSourceTemplateExist
    public function testIsSourceTemplateSuccess()
    {
        $template = new WbjTemplate();
        $ruleService = new MockRuleService();

        $ruleService->setSourceTemplate($template);
        $result = $ruleService->isSourceTemplateExist();

        $this->assertTrue($result);
    }

    public function testIsSourceTemplateFail()
    {
        $template = new NullWbjTemplate();
        $ruleService = new MockRuleService();

        $ruleService->setSourceTemplate($template);
        $result = $ruleService->isSourceTemplateExist();

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'sourceTemplateId'), Core::getLastError()->getSource());
    }

    //isTransformationTemplateExist
    public function testIsTransformationTemplateSuccess()
    {
        $template = new BjTemplate();
        $ruleService = new MockRuleService();

        $ruleService->setTransformationTemplate($template);
        $result = $ruleService->isTransformationTemplateExist();

        $this->assertTrue($result);
    }

    public function testIsTransformationTemplateFail()
    {
        $template = new NullBjTemplate();
        $ruleService = new MockRuleService();

        $ruleService->setTransformationTemplate($template);
        $result = $ruleService->isTransformationTemplateExist();

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_IS_EMPTY, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'transformationTemplateId'), Core::getLastError()->getSource());
    }

    public function testUpdateTemplateRuleCount()
    {
        $ruleService = new MockRuleService();

        $result = $ruleService->updateTemplateRuleCount();

        $this->assertTrue($result);
    }

    //isRuleNotExist
    private function prepareIsRuleNotExist($ruleList, $count)
    {
        //初始化
        $id = 1;
        $filter = array();
        $filter['transformationTemplate'] = 1;
        $filter['sourceTemplate'] = 2;
        $filter['transformationCategory'] = Template::CATEGORY['BJ'];
        $filter['sourceCategory'] = Template::CATEGORY['WBJ'];
        $filter['status'] = RuleService::STATUS['NORMAL'];
        $filter['id'] = $id;

        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'getRepository'
            ])
            ->getMock();

        $ruleService->setId($id);
        $ruleService->getTransformationTemplate()->setId($filter['transformationTemplate']);
        $ruleService->getSourceTemplate()->setId($filter['sourceTemplate']);
        $ruleService->setTransformationCategory(Template::CATEGORY['BJ']);
        $ruleService->setSourceCategory(Template::CATEGORY['WBJ']);

        $repository = $this->prophesize(IRuleServiceAdapter::class);
        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$ruleList, $count]);
        $ruleService->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $ruleService->isRuleNotExist();
        return $result;
    }

    public function testIsRuleNotExistSuccess()
    {
        $result = $this->prepareIsRuleNotExist([], 0);
        $this->assertTrue($result);
    }

    public function testIsRuleNotExistFail()
    {
        $result = $this->prepareIsRuleNotExist(['ruleList'], 5);
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'rule'), Core::getLastError()->getSource());
    }

    //isRuleFormatCorrect
    public function testIsRuleFormatCorrectSuccess()
    {
        //初始化
        $items = [
            ['isNecessary'=>Template::IS_NECESSARY['SHI'], 'identify'=>'identify1'],
            ['isNecessary'=>Template::IS_NECESSARY['SHI'], 'identify'=>'identify2']
        ];
        
        $rules = [];
        $completionRule = new CompletionRule();
        $completionRule->setCondition(['identify1'=>'value']);
        $rules['completionRule'] = $completionRule;

        $transformationRule = new TransformationRule();
        $transformationRule->setCondition(['identify2'=>'value']);
        $rules['transformationRule'] = $transformationRule;

        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'getRules'
            ])
            ->getMock();

        $ruleService->getTransformationTemplate()->setItems($items);

        $ruleService->expects($this->once())->method('getRules')->willReturn($rules);

        $result = $ruleService->isRuleFormatCorrect();
        $this->assertTrue($result);
    }

    public function testIsRuleFormatCorrectFail()
    {
        //初始化
        $items = [
            ['isNecessary'=>Template::IS_NECESSARY['SHI'], 'identify'=>'identify3'],
            ['isNecessary'=>Template::IS_NECESSARY['SHI'], 'identify'=>'identify2']
        ];
        
        $rules = [];
        $completionRule = new CompletionRule();
        $completionRule->setCondition(['identify1'=>'value']);
        $rules['completionRule'] = $completionRule;

        $transformationRule = new TransformationRule();
        $transformationRule->setCondition(['identify4'=>'value']);
        $rules['transformationRule'] = $transformationRule;

        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'getRules'
            ])
            ->getMock();
            
        $ruleService->getTransformationTemplate()->setItems($items);

        $ruleService->expects($this->once())->method('getRules')->willReturn($rules);

        $result = $ruleService->isRuleFormatCorrect();
        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer'=>'requiredItemsMapping'), Core::getLastError()->getSource());
    }

    //rulesValidate -- 开始
    public function testRulesValidateTrue()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'getRules'
            ])
            ->getMock();

        $rules[0] = $this->prophesize(Rule::class);
        $rules[1] = $this->prophesize(Rule::class);
        $rules[0]->validate()->shouldBeCalledTimes(1)->willReturn(true);
        $rules[1]->validate()->shouldBeCalledTimes(1)->willReturn(true);

        $ruleService->expects($this->once())
                   ->method('getRules')
                   ->willReturn(
                       [
                        $rules[0]->reveal(),
                        $rules[1]->reveal()
                        ]
                   );

        $result = $ruleService->rulesValidate();
        $this->assertTrue($result);
    }

    public function testRulesValidateFalse()
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'getRules'
            ])
            ->getMock();

        $rules[0] = $this->prophesize(Rule::class);
        $rules[0]->validate()->shouldBeCalledTimes(1)->willReturn(false);

        $ruleService->expects($this->once())
                   ->method('getRules')
                   ->willReturn(
                       [
                        $rules[0]->reveal(),
                        ]
                   );

        $result = $ruleService->rulesValidate();
        $this->assertFalse($result);
    }
    //rulesValidate -- 结束
    public function testGetSortRules()
    {
        $rules = array(
            'completionRule' => array('completionRule'),
            'comparisonRule' > array('comparisonRule')
        );

        $sortRules = array();
        foreach (IRule::RULE_PRIORITY as $name) {
            if (isset($rules[$name])) {
                $sortRules[$name] = $rules[$name];
            }
        }
        
        $result = $this->ruleService->getSortRules($rules);
        $this->assertEquals($result, $sortRules);
    }

    private function initialTransformation($resultTransformationSearchData)
    {
        $ruleService = $this->getMockBuilder(MockRuleService::class)
            ->setMethods([
                'getTransformationTemplate',
                'initializationTransformationSearchData',
                'getSortRules',
                'getRules',
                'errorReasonFormatConversion'
            ])->getMock();


        $id = 1;
        $sourceSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);
        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData($id);
        $transformationTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate($id);
        $rules = array('rules');

        $ruleService->expects($this->exactly(1))->method(
            'getTransformationTemplate'
        )->willReturn($transformationTemplate);

        $ruleService->expects($this->exactly(1))->method(
            'initializationTransformationSearchData'
        )->with($sourceSearchData, $transformationTemplate)->willReturn($transformationSearchData);

        $ruleService->expects($this->once())->method('getRules')->willReturn($rules);

        $sortRules[0] = $this->prophesize(TransformationRule::class);
        $sortRules[0]->transformation(
            Argument::exact($sourceSearchData),
            Argument::exact($transformationSearchData)
        )->shouldBeCalledTimes(1)->willReturn($resultTransformationSearchData);

        $ruleService->expects($this->exactly(1))->method(
            'getSortRules'
        )->with($rules)->willReturn([$sortRules[0]->reveal()]);

        if ($resultTransformationSearchData instanceof ErrorData) {
            $ruleService->expects($this->exactly(1))->method(
                'errorReasonFormatConversion'
            )->willReturn($resultTransformationSearchData);
        }

        $result = $ruleService->transformation($sourceSearchData);

        $this->assertEquals($result, $resultTransformationSearchData);
    }

    public function testTransformationFailureData()
    {
        $failureData = new FailureData();

        $this->initialTransformation($failureData);
    }

    public function testTransformation()
    {
        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);

        $this->initialTransformation($transformationSearchData);
    }
}

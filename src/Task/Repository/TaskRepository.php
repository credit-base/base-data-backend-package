<?php
namespace BaseData\Task\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Task\Model\Task;
use BaseData\Task\Adapter\Task\ITaskAdapter;
use BaseData\Task\Adapter\Task\TaskDbAdapter;
use BaseData\Task\Adapter\Task\TaskMockAdapter;

class TaskRepository extends Repository implements ITaskAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new TaskDbAdapter();
    }

    protected function getActualAdapter() : ITaskAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ITaskAdapter
    {
        return new TaskMockAdapter();
    }

    public function setAdapter(ITaskAdapter $adapter) : void
    {
        $this->adapter = $adapter;
    }

    public function fetchOne($id) : Task
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function schedule() : array
    {
        return $this->getAdapter()->schedule();
    }

    public function notifySubTasks(Task $parentTask) : bool
    {
        return $this->getAdapter()->notifySubTasks($parentTask);
    }

    public function expire() : array
    {
        return $this->getAdapter()->expire();
    }

    public function add(Task $task) : bool
    {
        return $this->getAdapter()->add($task);
    }

    public function edit(Task $task, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($task, $keys);
    }
}

<?php
namespace BaseData\UserGroup\View;

use PHPUnit\Framework\TestCase;

use BaseData\UserGroup\Model\UserGroup;

class UserGroupViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $userGroup = new UserGroupView(new UserGroup());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $userGroup);
    }
}

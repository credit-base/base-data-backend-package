<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use BaseData\Common\Model\IEnableAble;

class MockEnableWbjSearchDataCommandHandler extends EnableWbjSearchDataCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}

<?php
// $rules = ['transformationRule'=>[],'completionRule'=>[],'comparisonRule'=>'',['deDuplicationRule']=>''];

$rules = ['transformationRule'=>[]];

//转换规则
$rules['transformationRule'] = [
    "XK_XDR_MC" => "XK_XDR_MC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "XK_XDR_SHXYM" => "XK_XDR_SHXYM",//2
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    "BZ" => "BZ"//27
];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    )
);

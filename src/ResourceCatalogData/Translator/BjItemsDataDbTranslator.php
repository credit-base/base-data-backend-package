<?php
namespace BaseData\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\BjItemsData;
use BaseData\ResourceCatalogData\Model\NullBjItemsData;

class BjItemsDataDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $bjItemsData = null) : BjItemsData
    {
        if (!isset($expression['bj_items_data_id'])) {
            return NullBjItemsData::getInstance();
        }

        if ($bjItemsData == null) {
            $bjItemsData = new BjItemsData();
        }

        $bjItemsData->setId($expression['bj_items_data_id']);

        if (isset($expression['data'])) {
            $bjItemsData->setData(unserialize(gzuncompress(base64_decode($expression['data'], true))));
        }

        return $bjItemsData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($bjItemsData, array $keys = array()) : array
    {
        if (!$bjItemsData instanceof BjItemsData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'data'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['bj_items_data_id'] = $bjItemsData->getId();
        }
        if (in_array('data', $keys)) {
            $expression['data'] = base64_encode(gzcompress(serialize($bjItemsData->getData())));
        }

        return $expression;
    }
}

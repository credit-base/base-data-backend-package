<?php
namespace BaseData\Rule\UnAuditedRuleService\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则审核权限,在政务网OA的资源目录规则审核中,
 *           我可以管理委办局提交的资源目录的规则,可以查看审核状态为已驳回或待审核的资源目录规则,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 查看规则审核列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_bj_template');
    }

    /**
     * @Given: 存在规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form_rule' => $this->applyFormRule(),
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_bj_template' => $this->bjTemplate(),
        ]);
    }

    /**
     * @When: 当我查看规则审核数据列表时
     */
    public function fetchRuleList()
    {
        $repository = new UnAuditedRuleServiceRepository();

        list($unAuditedRuleServiceList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedRuleServiceList;
    }

    /**
     * @Then: 我可以看到规则审核数据的全部信息
     */
    public function testViewRuleList()
    {
        $setUnAuditedRuleServiceList = $this->applyFormRule();

        foreach ($setUnAuditedRuleServiceList as $key => $setUnAuditedRuleService) {
            unset($setUnAuditedRuleService);
            $setUnAuditedRuleServiceList[$key]['apply_form_rule_id'] = $key +1;
        }

        $unAuditedRuleServiceList = $this->fetchRuleList();

        $translator = new UnAuditedRuleServiceDbTranslator();
        foreach ($unAuditedRuleServiceList as $rule) {
            $ruleServiceArray[] = $translator->objectToArray($rule);
        }

        $this->assertEquals($ruleServiceArray, $setUnAuditedRuleServiceList);
    }
}

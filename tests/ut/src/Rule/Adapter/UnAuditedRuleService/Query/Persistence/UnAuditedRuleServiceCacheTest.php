<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query\Persistence;

use PHPUnit\Framework\TestCase;

class UnAuditedRuleServiceCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockUnAuditedRuleServiceCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(UnAuditedRuleServiceCache::KEY, $this->cache->getKey());
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\GbSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\ResourceCatalogData\Command\GbSearchData\ConfirmGbSearchDataCommand;

class ConfirmGbSearchDataCommandHandler implements ICommandHandler
{
    use GbSearchDataCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof ConfirmGbSearchDataCommand)) {
            throw new \InvalidArgumentException;
        }

        $gbSearchData = $this->fetchGbSearchData($command->id);

        return $gbSearchData->confirm();
    }
}

# 新闻类控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 新闻类

* `TITLE`: 标题控件规范
* `SOURCE`: 来源控件规范
* `COVER`: 封面控件规范
* `ATTACHMENTS`: 附件控件规范
* `STATUS`: 状态控件规范.
* `STICK`: 置顶状态控件规范.
* `CONTENT`: 内容控件规范
* `PARENT_CATEGORY`: 新闻父级分类控件规范
* `CATEGORY`: 新闻分类控件规范
* `NEWS_TYPE`: 新闻类型控件规范
* `DIMENSION`: 数据纬度控件规范
* `BANNER_STATUS`: 轮播状态控件规范
* `BANNER_IMAGE`: 轮播图图片控件规范
* `HOME_PAGE_SHOW_STATUS`: 首页展示状态控件规范
* `REJECT_REASON`: 驳回原因控件规范

### `TITLE`
* [标题控件规范(TITLE)](common.md)(必填)

### `SOURCE`
* [来源控件规范(SOURCE)](common.md)(必填)

### `COVER`
* [封面控件规范(IMAGE)](common.md)(选填)

### `ATTACHMENTS`
* [附件控件规范(ATTACHMENTS)](common.md)(选填)

### `STATUS`
* [状态控件规范(STATUS)](common.md)(必填)

### `STICK`
* [置顶状态控件规范(STICK)](common.md)(必填)

### `REJECT_REASON`
* [驳回原因控件规范(DESCRIPTION)](common.md)(必填)

### `CONTENT`

**内容** 内容控件规范

#### 规则

* string
* 必填

#### 错误提示

* `ER-0101`: 内容格式不正确

### `PARENT_CATEGORY`

**新闻父级分类** 控件规范,用于表述新闻父级分类

#### 规则

* int positive
* 在新闻父级分类数组中
* 必填

#### 错误提示

* `ER-0010`,新闻父级分类不存在

### `CATEGORY`

**新闻分类** 控件规范,用于表述新闻分类

#### 规则

* int positive
* 在新闻分类数组中
* 必填

#### 错误提示

* `ER-0010`,新闻分类不存在

### `NEWS_TYPE`

**新闻类型** 控件规范,用于表述新闻类型

#### 规则

* int positive
* 在新闻类型数组中
* 必填

#### 错误提示

* `ER-0100`,新闻类型不能为空
* `ER-0101`,新闻类型格式不正确

### `DIMENSION`

**数据纬度** 控件规范,用于表述数据纬度

#### 规则

* int positive
	* SOCIOLOGY | 社会公开 | 1 默认
	* GOVERNMENT_AFFAIRS | 政务共享 | 2
* 必填

#### 错误提示

* `ER-0100`,数据纬度不能为空
* `ER-0101`,数据共享纬度格式不正确

### `BANNER_STATUS`

**轮播状态** 控件规范,用于表述轮播状态

#### 规则

* int positive
* 选填
* 轮播状态在以下范围中
	* BANNER_STATUS_DISABLED | 不轮播 | 0 默认
	* BANNER_STATUS_ENABLED | 轮播 |  2

#### 错误提示

* `ER-0101`,轮播状态格式不正确

### `BANNER_IMAGE`
* [轮播图图片控件规范(IMAGE)](common.md)(轮播状态 0 选填 2 必填)

### `HOME_PAGE_SHOW_STATUS`

**首页展示状态** 控件规范,用于表述首页展示状态

#### 规则

* int positive
* 选填
* 首页展示状态在以下范围中
	* HOME_PAGE_SHOW_STATUS_DISABLED | 不展示 | 0 默认
	* HOME_PAGE_SHOW_STATUS_ENABLED | 展示 |  2

#### 错误提示

* `ER-0101`,首页展示状态格式不正确

<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullTaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = NullTask::getInstance();
    }

    public function tearDown()
    {
        unset($this->task);
    }

    public function testExtendsTask()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\Task', $this->task);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->task);
    }
}

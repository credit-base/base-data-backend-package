<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query;

use Marmot\Framework\Query\RowCacheQuery;

class WorkOrderTaskRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'work_order_task_id',
            new Persistence\WorkOrderTaskCache(),
            new Persistence\WorkOrderTaskDb()
        );
    }
}

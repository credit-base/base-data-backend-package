<?php
namespace BaseData\Enterprise\Adapter\Enterprise;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Enterprise\Model\Enterprise;
use BaseData\Enterprise\Model\NullEnterprise;
use BaseData\Enterprise\Translator\EnterpriseDbTranslator;
use BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery;

class EnterpriseDbAdapter implements IEnterpriseAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new EnterpriseDbTranslator();
        $this->rowCacheQuery = new EnterpriseRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : EnterpriseDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : EnterpriseRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullEnterprise::getInstance();
    }

    public function add(Enterprise $enterprise) : bool
    {
        return $this->addAction($enterprise);
    }

    public function edit(Enterprise $enterprise, array $keys = array()) : bool
    {
        return $this->editAction($enterprise, $keys);
    }

    public function fetchOne($id) : Enterprise
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $enterprise = new Enterprise();
            if (isset($filter['unifiedSocialCreditCode'])) {
                $enterprise->setUnifiedSocialCreditCode($filter['unifiedSocialCreditCode']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('unifiedSocialCreditCode'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['name'])) {
                $enterprise->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['principal'])) {
                $enterprise->setPrincipal($filter['principal']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('principal'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['uniqueName'])) {
                $enterprise->setName($filter['uniqueName']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('name'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['identify'])) {
                $enterprise->setName($filter['identify']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' OR ';
                $enterprise->setUnifiedSocialCreditCode($filter['identify']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('unifiedSocialCreditCode'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['hash'])) {
                $enterprise->setHash($filter['hash']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('hash'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['task'])) {
                $enterprise->getTask()->setId($filter['task']);
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('task'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $enterprise = new Enterprise();
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($enterprise, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }
        
        return $condition;
    }
}

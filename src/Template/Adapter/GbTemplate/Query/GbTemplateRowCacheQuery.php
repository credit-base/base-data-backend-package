<?php
namespace BaseData\Template\Adapter\GbTemplate\Query;

use Marmot\Framework\Query\RowCacheQuery;

class GbTemplateRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'gb_template_id',
            new Persistence\GbTemplateCache(),
            new Persistence\GbTemplateDb()
        );
    }
}

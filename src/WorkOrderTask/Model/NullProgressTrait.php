<?php
namespace BaseData\WorkOrderTask\Model;

use Marmot\Core;

trait NullProgressTrait
{
    public function revoke(string $reason) : bool
    {
        unset($reason);
        return $this->resourceNotExist();
    }

    public function confirm() : bool
    {
        return $this->resourceNotExist();
    }

    public function end(string $reason) : bool
    {
        unset($reason);
        return $this->resourceNotExist();
    }

    public function feedback() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}

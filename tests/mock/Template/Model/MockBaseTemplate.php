<?php
namespace BaseData\Template\Model;

use BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class MockBaseTemplate extends BaseTemplate
{
    public function getRepository() : IBaseTemplateAdapter
    {
        return parent::getRepository();
    }
}

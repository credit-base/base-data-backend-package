<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class MockWbjSearchDataEnableController extends WbjSearchDataEnableController
{
    public function getRepository() : IWbjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}

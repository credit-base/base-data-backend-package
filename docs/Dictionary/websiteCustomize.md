# 网站定制

### 英文名称

**中文名称** 描述信息

---

### crew

**发布人** 发布人的表述

* Crew

### version

**版本号** 版本号的表述.

* int
* 年月日+四位随机数(2108053456)

### category

**定制类型** 定制类型的表述.

* int

	* CATEGORY['HOME_PAGE'] | 首页 | 1

### content

**定制内容** 定制内容的表述.

* array

### status

**发布状态** 发布状态的表述.

* int

	* STATUS['UNPUBLISHED'| 未发布 | 0
	* STATUS['PUBLISHED'] | 发布 | 2

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

-----

## 首页内容

## type

**内容分类** 内容分类的表述.

* int 

	* TYPE['NULL'| 默认 | 0 
	* TYPE['PV'] | 访问量统计 | 1
	* TYPE['FONT'] | 字体    | 2
	* TYPE['ACCESSIBLE_READING'] | 无障碍阅读    | 3
	* TYPE['SIGN_IN'] | 登录    | 4
	* TYPE['SIGN_UP'] | 注册    | 5
	* TYPE['USER_CENTER'] | 用户中心 | 6
	* TYPE['FOR_THE_RECORD'] | 备案号    | 7
	* TYPE['PUBLIC_NETWORK_SECURITY'] | 公网安备    | 8
	* TYPE['WEBSITE_IDENTIFY'] | 网站标识码    | 9

### memorialStatus

**背景置灰** 背景置灰的表述.

* int

	* MEMORIAL_STATUS['NO'| 否 | 0 默认
	* MEMORIAL_STATUS['YES'] | 是 | 1

### fontStatus

**字体** 字体的表述.

* int

	* FONT_STATUS['SIMPLIFIED_CHINESE'| 简体 | 0 默认
	* FONT_STATUS['TRADITIONAL_CHINES'] | 繁体 | 1

### accessibleReading

**无障碍阅读** 无障碍阅读的表述.

* int

	* ACCESSIBLE_READING['YES'| 是 | 0 默认
	* ACCESSIBLE_READING['NO'] | 否 | 1

## status

**状态** 状态的表述.

* int

	* STATUS_ENABLED 0 启用
	* STATUS_DISABLED -2 禁用

### name

**名称** 名称的表述.

* string

### url

**链接** 链接的表述.

* string

### image

**图片** 图片的表述.

* array

### color

**背景颜色** 背景颜色的表述.

* string

### description

**描述** 描述的表述.

* string

### theme

**页面主题标识** 页面主题标识的表述.

* 必填

	```
	array(
		'color' => '#41245',
		'image' => array(
			'name' => '图片名称',
			'identify' => '图片地址'
		)
	)
	```

### headerBarLeft

**头部左侧位置标识页面主题标识** 头部左侧位置标识的表述.

* 必填

	```
	array(
		array(
			'name' => '访问量统计',
			'status' => ,
			'url' => '',
			'type' => 1
		),
		array(
			'name' => '帮助说明',
			'status' => 0,
			'url' => '/about',
			'type' => 0
		),
	)
	```

## headerBarRight

**头部右侧位置标识页面主题标识** 头部右侧位置标识的表述.

* 必填

	```
	array(
		array(
			'name' => '字体',
			'status' => 0,
			'url' => '',
			'type' => 2
		),
		array(
			'name' => '无障碍阅读',
			'status' => 0,
			'url' => '',
			'type' => 3
		),
		array(
			'name' => '登录',
			'status' => 0,
			'url' => '/signIn',
			'type' => 4
		),
		array(
			'name' => '注册',
			'status' => 0,
			'url' => '/signUp',
			'type' => 5
		),
		array(
			'name' => '用户中心',
			'status' => 0,
			'url' => '/members',
			'type' => 6
		),
		array(
			'name' => '网站导航',
			'status' => 0,
			'url' => 'navigations/index',
			'type' => 0
		),
	)
	```

## headerBg

**header背景** header背景的表述.

* 必填

	```
	array(
		'name' => '名称',
		'identify' => '地址'
	)
	```


## logo

**网站logo** 网站logo的表述.

* 必填

	```
	array(
		'name' => 'logo名称',
		'identify' => 'logo地址'
	)
	```

## headerSearch

**头部搜索栏位置标识** 头部搜索栏位置标识的表述.

* creditInformation 信用信息
* personalCreditInformation 个人信用信息
* unifiedSocialCreditCode 统一社会信用代码
* legalPerson 法人
* newsTitle 站内文章

* 必填

	```
	array(
		'creditInformation' => array(
			'status' => 0
		),
		'personalCreditInformation' => array(
			'status' => 0
		),
		'unifiedSocialCreditCode' => array(
			'status' => 0
		),
		'legalPerson' => array(
			'status' => 0
		),
		'newsTitle' => array(
			'status' => 0
		),
	)
	```

## nav(? 高亮)

**头部导航位置标识** 头部导航位置标识的表述.

* 必填

	```
	array(
		array(
			'name' => '首页',
			'url' => '/index',
			'image' => array(
				'name' => '',
				'identify' => ''
			),
			'status' => 0
		),
		array(
			'name' => '组织架构',
			'url' => '/organizations/index',
			'image' => array(
				'name' => '',
				'identify' => ''
			),
			'status' => 0
		),
	)
	```

## centerDialog

**中间弹框位置标识** 中间弹框位置标识的表述.

* 选填

	```
	array(
		'status' =>0,
		'image' => array(
			name=> '',
			identify => ''
		),
		'url'=>''
	)
	```

## animateWindow

**飘窗位置标识** 飘窗位置标识的表述.

* 选填

	```
	array(
		array(
			'status' =>0,
			'image' => array(
				name=> '',
				identify => ''
			),
			'url'=>''
		),
		array(
			'status' =>0,
			'image' => array(
				name=> '',
				identify => ''
			),
			'url'=>''
		),
	)
	```

## leftFloatCard

**左侧浮动卡片位置标识** 左侧浮动卡片位置标识的表述.

* 选填

	```
	array(
		array(
			'status' =>0,
			'image' => array(
				name=> '',
				identify => ''
			),
			'url'=>''
		),
		array(
			'status' =>0,
			'image' => array(
				name=> '',
				identify => ''
			),
			'url'=>''
		),
	)
	```

## frameWindow

**外链窗口** 外链窗口的表述.

* 选填

	```
	array = array(
		'status' => 0,
		'url' => ''
	)
	```

## rightToolBar

**右侧工具栏** 右侧工具栏的表述.

* 必填

### category

**右侧工具栏分类** 右侧工具栏分类的表述.

* int 
	* CATEGORY['IMAGE'] => 1 //图片
	* CATEGORY['LINK'] => 2 //链接
	* CATEGORY['WRITTEN_WORDS'] => 3 //文字

	```
	array(
		array(
			'name' => '',
			'category' => 1,
			'status' =>0,
			'images' => array(
				array(
					'title' => '',
					'image' => array(
						'name' => '',
						'identify' => ''
					)
				),
				array(
					'title' => '',
					'image' => array(
						'name' => '',
						'identify' => ''
					)
				)
			)
		),
		array(
			'name' => '',
			'category' => 2,
			'status' =>0,
			'url'=>''
		),
		array(
			'name' => '',
			'category' => 3,
			'status' =>0,
			'description'=>''
		),
	)
	```

## specialColumn

**专题专栏** 专题专栏的表述.

* 选填

	```
	array(
		array(
			'status' =>0,
			'image' => array(
				name=> '',
				identify => ''
			),
			'url'=>''
		),
		array(
			'status' =>0,
			'image' => array(
				name=> '',
				identify => ''
			),
			'url'=>''
		),
	)
	```

## relatedLinks

**相关链接** 相关链接的表述.

* 选填

	```
	array(
		array(
			'name' => '',
			'status' => '',
			'items' => array(
				array(
					'name' => '',
					'url' => '',
					'status' => 0
				)
			)
		),
		array(
			'name' => '',
			'status' => '',
			'items' => array(
				array(
					'name' => '',
					'url' => '',
					'status' => 0
				)
			)
		),
	)
	```

## footerBanner

**底部轮播** 底部轮播的表述.

* 选填

	```
	array(
		array(
			'status' =>0,
			'image' => array(
				'name'=> '',
				'identify' => ''
			),
			'url'=>''
		),
		array(
			'status' =>0,
			'image' => array(
				'name'=> '',
				'identify' => ''
			),
			'url'=>''
		),
	)
	```

## organizationGroup

**组织列表** 组织列表的表述.

* 选填

	```
	array(
		array(
			'status' =>0,
			'image' => array(
				'name'=> '',
				'identify' => ''
			),
			'url'=>''
		),
		array(
			'status' =>0,
			'image' => array(
				'name'=> '',
				'identify' => ''
			),
			'url'=>''
		),
	)
	```

## silhouette

**剪影** 剪影的表述.

* 选填

	```
	array(
		'image' => array(
			'name' => '',
			'identify' => ''
		),
		'description' => '',
		'status' => 0
	)
	```

## partyAndGovernmentOrgans

**党政机关** 党政机关的表述.

* 选填

	```
	array(
		'status' => 0
		'image' => array(
			'name' => ''
			'identify' => ''
		),
		'url' => ''
	)
	```

## governmentErrorCorrection

**政府纠错** 政府纠错的表述.

* 选填

	```
	array(
		'status' => 0
		'image' => array(
			'name' => ''
			'identify' => ''
		),
		'url' => ''
	)
	```

## footerNav

**页脚导航** 页脚导航的表述.

* 必填

	```
	array(
		array(
			'name' => ''
			'url' => ''
			'status' => ''
		)
	)
	```

## footerTwo

**页脚第二行内容呈现** 页脚第二行内容呈现的表述.

* 选填

	```
	array(
		array(
			'name' => '',
			'description' => '',
			'url' => ''
			'status' => 0
		),
		array(
			'name' => '',
			'description' => '',
			'url' => ''
			'status' => 0
		),
	)
	```

## footerThree

**页脚第三行内容呈现** 页脚第三行内容呈现的表述.

* 必填

	```
	array(
		array(
			'name' => '备案号'
			'description' => ''
			'url' => ''
			'type' => 7,
			'status' => 0
		),
		array(
			'image' => array(
				'name' => '',
				'identify' => ''
			)
			'name' => '公网安备'
			'description' => ''
			'url' => ''
			'type' => 8,
			'status' => 0
		),
		array(
			'name' => '网站标识码'
			'description' => ''
			'url' => ''
			'type' => 9,
			'status' => 0
		),
		array(
			'name' => '随便'
			'description' => ''
			'url' => ''
			'type' => 0,
			'status' => 0
		),
	)
	```

## 首页内容样例

	```
	array(
		'memorialStatus' => 0,
		'theme' => 	array(
			'color' => '#41245',
			'image' => array(
				'name' => '图片名称',
				'identify' => '图片地址'
			)
		),
		'headerBarLeft' => array(
			array(
				'name' => '访问量统计',
				'status' => 0,
				'url' => '',
				'type' => 1
			),
			array(
				'name' => '帮助说明',
				'status' => 0,
				'url' => '/about',
				'type' => 0
			),
		),
		'headerBarRight' => array(
			array(
				'name' => '字体',
				'status' => 0,
				'url' => '',
				'type' => 2
			),
			array(
				'name' => '无障碍阅读',
				'status' => 0,
				'url' => '',
				'type' => 3
			),
			array(
				'name' => '登录',
				'status' => 0,
				'url' => '/signIn',
				'type' => 4
			),
			array(
				'name' => '注册',
				'status' => 0,
				'url' => '/signUp',
				'type' => 5
			),
			array(
				'name' => '用户中心',
				'status' => 0,
				'url' => '/members',
				'type' => 6
			),
			array(
				'name' => '网站导航',
				'status' => 0,
				'url' => 'navigations/index',
				'type' => 0
			),
		),
		'headerBg' => array(
			'name' => 'headerBg名称',
			'identify' => 'headerBg地址'
		),
		'logo' => array(
			'name' => 'logo名称',
			'identify' => 'logo地址'
		),
		'headerSearch' => array(
			'creditInformation' => array(
				'status' => 0
			),
			'personalCreditInformation' => array(
				'status' => 0
			),
			'unifiedSocialCreditCode' => array(
				'status' => 0
			),
			'legalPerson' => array(
				'status' => 0
			),
			'newsTitle' => array(
				'status' => 0
			),
		),
		'nav' => array(
			array(
				'name' => '首页',
				'url' => '/index',
				'image' => array(
					'name' => '',
					'identify' => ''
				),
				'status' => 0
			),
			array(
				'name' => '组织架构',
				'url' => '/organizations/index',
				'image' => array(
					'name' => '',
					'identify' => ''
				),
				'status' => 0
			),
		),
		'centerDialog' => array(
			'status' =>0,
			'image' => array(
				name=> '',
				identify => ''
			),
			'url'=>''
		),
		'animateWindow' => array(
			array(
				'status' =>0,
				'image' => array(
					name=> '',
					identify => ''
				),
				'url'=>''
			),
			array(
				'status' =>0,
				'image' => array(
					name=> '',
					identify => ''
				),
				'url'=>''
			),
		),
		'leftFloatCard' => array(
			array(
				'status' =>0,
				'image' => array(
					name=> '',
					identify => ''
				),
				'url'=>''
			),
			array(
				'status' =>0,
				'image' => array(
					name=> '',
					identify => ''
				),
				'url'=>''
			),
		),
		'frameWindow' => array(
			'status' => 0,
			'url' => ''
		),
		'rightToolBar' => array(
			array(
				'name' => '',
				'category' => 1,
				'status' => 0,
				'images' => array(
					array(
						'title' => '',
						'image' => array(
							'name' => '',
							'identify' => ''
						)
					),
					array(
						'title' => '',
						'image' => array(
							'name' => '',
							'identify' => ''
						)
					)
				)
			),
			array(
				'name' => '',
				'category' => 2,
				'status' => 0,
				'url'=> ''
			),
			array(
				'name' => '',
				'category' => 3,
				'status' => 0,
				'description'=> ''
			),
		),
		'specialColumn' => array(
			array(
				'status' => 0,
				'image' => array(
					name=> '',
					identify => ''
				),
				'url'=>''
			),
			array(
				'status' => 0,
				'image' => array(
					name=> '',
					identify => ''
				),
				'url'=>''
			),
		),
		'relatedLinks' => array(
			array(
				'name' => '',
				'status' => '',
				'items' => array(
					array(
						'name' => '',
						'url' => '',
						'status' => 0
					)
				)
			),
			array(
				'name' => '',
				'status' => '',
				'items' => array(
					array(
						'name' => '',
						'url' => '',
						'status' => 0
					)
				)
			),
		),
		'footerBanner' => array(
			array(
				'status' =>0,
				'image' => array(
					'name'=> '',
					'identify' => ''
				),
				'url'=>''
			),
			array(
				'status' =>0,
				'image' => array(
					'name'=> '',
					'identify' => ''
				),
				'url'=>''
			),
		),
		'organizationGroup' => array(
			array(
				'status' =>0,
				'image' => array(
					'name'=> '',
					'identify' => ''
				),
				'url'=>''
			),
			array(
				'status' =>0,
				'image' => array(
					'name'=> '',
					'identify' => ''
				),
				'url'=>''
			),
		),
		'silhouette' => array(
			'image' => array(
				'name' => '',
				'identify' => ''
			),
			'description' => '',
			'status'=>0
		),
		'partyAndGovernmentOrgans' => array(
			'status' => 0,
			'image' => array(
				'name' => '',
				'identify' => ''
			),
			'url' => ''
		),
		'governmentErrorCorrection' => array(
			'status' => 0,
			'image' => array(
				'name' => '',
				'identify' => ''
			),
			'url' => ''
		),
		'footerNav' => array(
			array(
				'name' => '',
				'url' => '',
				'status' => ''
			)
		),
		'footerTwo' => array(
			array(
				'name' => '',
				'description' => '',
				'url' => '',
				'status' => 0
			),
			array(
				'name' => '',
				'description' => '',
				'url' => '',
				'status' => 0
			),
		),
		'footerThree' => array(
			array(
				'name' => '备案号',
				'description' => '',
				'url' => '',
				'type' => 7,
				'status' => 0
			),
			array(
				'image' => array(
					'name' => '',
					'identify' => ''
				),
				'name' => '公网安备',
				'description' => '',
				'url' => '',
				'type' => 8,
				'status' => 0
			),
			array(
				'name' => '网站标识码',
				'description' => '',
				'url' => '',
				'type' => 9,
				'status' => 0
			),
			array(
				'name' => '随便',
				'description' => '',
				'url' => '',
				'type' => 0,
				'status' => 0
			),
		)
	);
	```
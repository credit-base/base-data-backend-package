<?php
namespace BaseData\Rule\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\IRule;
use BaseData\Template\Model\Template;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\MockTemplate;
use BaseData\Template\Repository\MockBjTemplateRepository;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\MockSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

/**
 * BaseData\Rule\Model\ComparisonRule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class ComparisonRuleTest extends ComparisonCompleteRule
{
    public function setUp()
    {
        parent::setUp();
        $this->rule = $this->getMockBuilder(ComparisonRule::class)
                           ->setMethods(
                               [
                                    'getTransformationTemplate',
                                    'fetchTemplateItems',
                                    'fetchTemplateList',
                                    'getCondition',
                                    'validateBase',
                                    'validateIdentify',
                                    'comparisonRuleComparison'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function testValidateIdentify()
    {
        $transformationItemIdentify = 'transformationItemIdentify';
        $base = ['base'];

        $this->rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'identifyAndBaseDuplicateWithCompanyName',
                                    'identifyAndBaseDuplicateWithIdentify'
                                ]
                           )
                           ->getMock();

        $this->rule->expects($this->exactly(1))
                         ->method('identifyAndBaseDuplicateWithCompanyName')
                         ->with($transformationItemIdentify, $base)
                         ->willReturn(true);

        $this->rule->expects($this->exactly(1))
                         ->method('identifyAndBaseDuplicateWithIdentify')
                         ->with($transformationItemIdentify, $base)
                         ->willReturn(false);

        $result = $this->rule->validateIdentify($transformationItemIdentify, $base);
        $this->assertFalse($result);
    }

    public function testIdentifyAndBaseDuplicateWithCompanyNameNotExistIdentify()
    {
        $transformationItemIdentify = 'NOT_EXIST';
        $base = [];

        $rule = new MockComparisonRule();
        $result = $rule->identifyAndBaseDuplicateWithCompanyName($transformationItemIdentify, $base);
        $this->assertTrue($result);
    }

    public function testIdentifyAndBaseDuplicateWithCompanyNameDuplicateBase()
    {
        $transformationItemIdentify = 'ZTMC';
        $base = IRule::COMPARISON_BASE;
        ;

        $rule = new MockComparisonRule();
        $result = $rule->identifyAndBaseDuplicateWithCompanyName($transformationItemIdentify, $base);
        $this->assertFalse($result);
    }

    public function testValidateIdentifyFalse()
    {
        $this->ruleTemplateList = [1=>new MockTemplate(), 2=>new MockTemplate()];

        $this->condition = array(
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC')
            )
        );
        $base = $this->condition['ZTMC'][0]['base'];
        $comparisonTemplate = $this->ruleTemplateList[1];

        $this->prepareValidate();

        $this->rule->expects($this->exactly(1))
                         ->method('fetchTemplateItems')
                         ->with($this->transformationTemplate)
                         ->willReturn($this->transformationTemplateItems);

        $this->rule->expects($this->exactly(1))
                         ->method('validateBase')
                         ->with($comparisonTemplate, $base)
                         ->willReturn(true);

         $this->rule->expects($this->exactly(1))
                         ->method('validateIdentify')
                         ->with('ZTMC', $base)
                         ->willReturn(false);

        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    public function testValidateIdentifySuccess()
    {
        $transformationItemIdentify = 'NOT_EXIST';
        $base = [];

        $rule = new MockComparisonRule();
        $result = $rule->identifyAndBaseDuplicateWithIdentify($transformationItemIdentify, $base);
        $this->assertTrue($result);
    }

    public function testValidateIdentifyFailNotExistItem()
    {
        $transformationItemIdentify = 'TYSHXYDM';
        $base = IRule::COMPARISON_BASE;
        ;

        $rule = new MockComparisonRule();
        $result = $rule->identifyAndBaseDuplicateWithIdentify($transformationItemIdentify, $base);
        $this->assertFalse($result);
    }

    public function testValidateIdentifyFailDuplicateBase()
    {
        $transformationItemIdentify = 'TYSHXYDM';
        $base = IRule::COMPARISON_BASE;
        ;

        $rule = new MockComparisonRule();
        $result = $rule->identifyAndBaseDuplicateWithIdentify($transformationItemIdentify, $base);
        $this->assertFalse($result);
    }

    private function prepareComparisonRuleComparison(bool $result)
    {
        $this->prepareItemComparison();

        $this->rule->expects($this->exactly(1))
                         ->method('validateIdentify')
                         ->with('ZTMC', $this->condition['ZTMC'][0]['base'])
                         ->willReturn(true);

        $item = $this->condition['ZTMC'][0]['item'];
        $transformationItem = $this->transformationTemplateItems['ZTMC'];
        $sourceItem = $this->ruleTemplateItems[$item];

        $this->rule->expects($this->exactly(1))
                         ->method('comparisonRuleComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn($result);
    }
    public function testValidateComparisonRuleComparisonFalse()
    {
        $this->prepareComparisonRuleComparison(false);
        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    public function testValidateSuccess()
    {
        $this->prepareComparisonRuleComparison(true);
        $result = $this->rule->validate();
        $this->assertTrue($result);
    }

    public function testValidateNotExistComparisonTemplateItem()
    {
        $this->ruleTemplateList = [1=>new MockTemplate(), 2=>new MockTemplate()];

        $this->condition = array(
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'NOT_EXIST')
            )
        );
        $base = $this->condition['ZTMC'][0]['base'];
        $comparisonTemplate = $this->ruleTemplateList[1];

        $this->prepareValidate();

        $this->rule->expects($this->exactly(2))
                         ->method('fetchTemplateItems')
                        ->withConsecutive(
                            [$this->transformationTemplate],
                            [$comparisonTemplate]
                        )
                         ->will($this->onConsecutiveCalls(
                             $this->transformationTemplateItems,
                             $this->ruleTemplateItems
                         ));

        $this->rule->expects($this->exactly(1))
                         ->method('validateBase')
                         ->with($comparisonTemplate, $base)
                         ->willReturn(true);

         $this->rule->expects($this->exactly(1))
                         ->method('validateIdentify')
                         ->with('ZTMC', $base)
                         ->willReturn(true);

        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    //validateTransformation
    public function testValidateTransformation()
    {
        $rule = new MockComparisonRule();
        $transformationSearchData = new ErrorData();
        $result = $rule->validateTransformation($transformationSearchData);
        $this->assertTrue($result);
    }

    //transformationInitialization
    public function testTransformationInitialization()
    {
        //初始化
        $condition = ['condition'];
        $data = ['data'];
        $template = new MockTemplate();
        $templateItems = ['templateItems'];
        $transformationSearchData = new MockSearchData();
        $transformationSearchData->setTemplate($template);
        $transformationSearchData->getItemsData()->setData($data);

        $rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'getCondition',
                                    'fetchTemplateItems'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('getCondition')
                         ->willReturn($condition);

        $rule->expects($this->exactly(1))
                         ->method('fetchTemplateItems')
                         ->with($template)
                         ->willReturn($templateItems);

        $expected = [
            'condition' => $condition,
            'transformationItemsData' => $data,
            'transformationTemplateItems' => $templateItems
        ];

        $result = $rule->transformationInitialization(new MockSearchData(), $transformationSearchData);

        $this->assertEquals($expected, $result);
    }

    //validateInitializationDataFormat
    public function testValidateInitializationDataFormat()
    {
        $data = [
            'condition'=>'condition',
            'transformationItemsData'=>'transformationItemsData',
            'transformationTemplateItems'=>'transformationTemplateItems'
        ];
        $rule = new MockComparisonRule();

        $result = $rule->validateInitializationDataFormat($data);
        $this->assertTrue($result);
    }

    //transformationExecute
    public function testTransformationExecuteValidateFalse()
    {
        $data = ['data'];
        $transformationSearchData = new MockSearchData();

        $rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'validateInitializationDataFormat'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateInitializationDataFormat')
                         ->with($data)
                         ->willReturn(false);

        $result = $rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals([], $result);
    }

    public function testTransformationExecuteEmptyContion()
    {
        $data = [
            'transformationItemsData'=>['transformationItemsData'],
            'transformationTemplateItems'=>['transformationTemplateItems'],
            'condition'=>[]
        ];
        $transformationSearchData = new MockSearchData();

        $rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'validateInitializationDataFormat'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateInitializationDataFormat')
                         ->with($data)
                         ->willReturn(true);

        $result = $rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals(['failureItems'=>[]], $result);
    }

    private function perpareTransformationExecute(
        array $data,
        ISearchDataAble $transformationSearchData,
        string $comparisonItemData = 'comparisonItemData',
        string $transformationIdentify = 'identify'
    ) {
        $searchDataList = ['searchDataList'];

        $this->rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'validateInitializationDataFormat',
                                    'fetchSearchDataList',
                                    'filterExistItemsDataByIdentify'
                                ]
                           )
                           ->getMock();

        $this->rule->expects($this->exactly(1))
                         ->method('validateInitializationDataFormat')
                         ->with($data)
                         ->willReturn(true);

        $this->rule->expects($this->exactly(1))
                         ->method('fetchSearchDataList')
                         ->with($data['condition'][$transformationIdentify][0], $transformationSearchData)
                         ->willReturn($searchDataList);

        $this->rule->expects($this->exactly(1))
                         ->method('filterExistItemsDataByIdentify')
                         ->with($searchDataList, $transformationIdentify)
                         ->willReturn($comparisonItemData);
    }

    public function testTransformationExecuteTransformationTemplateItemsNotExistIdentify()
    {
        $data = [
            'transformationItemsData'=>['identify'=>''],
            'transformationTemplateItems'=>['identifyNotExist'=>['transformationTemplateItems']],
            'condition'=>['identify'=>[['item'=>'data']]],
        ];
        $transformationSearchData = new MockSearchData();

        $this->perpareTransformationExecute($data, $transformationSearchData);

        $failureItems['identify'] = [];

        $result = $this->rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals(['failureItems'=>$failureItems], $result);
    }

    public function testTransformationExecuteComparisonItemDataAndTransformationItemDataEmpty()
    {
        $data = [
            'transformationItemsData'=>['identify'=>''],
            'transformationTemplateItems'=>['identifyNotExist'=>['transformationTemplateItems']],
            'condition'=>['name'=>[['item'=>'data']]],
        ];
        $transformationSearchData = new MockSearchData();
        $comparisonItemData = '';

        $this->perpareTransformationExecute($data, $transformationSearchData, $comparisonItemData, 'name');

        $failureItems['name'] = [];

        $result = $this->rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals(['failureItems'=>$failureItems], $result);
    }

    public function testTransformationExecute()
    {
        $data = [
            'transformationItemsData'=>['identify'=>''],
            'transformationTemplateItems'=>['identify'=>['transformationTemplateItems']],
            'condition'=>['identify'=>[['item'=>'data']]],
        ];
        $transformationSearchData = new MockSearchData();

        $this->perpareTransformationExecute($data, $transformationSearchData);

        $failureItems['identify'] = ['transformationTemplateItems'];

        $result = $this->rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals(['failureItems'=>$failureItems], $result);
    }

    //validateDataFormat
    public function testValidateDataFormat()
    {
        $data = ['failureItems'=>'failureItems'];
        $rule = new MockComparisonRule();

        $result = $rule->validateDataFormat($data);
        $this->assertTrue($result);
    }

    //transformationResult
    public function testTransformationResultValidateDataFormatFalse()
    {
        //初始化
        $data = ['data'];
        $transformationSearchData = new MockSearchData();
        $errorData = new ErrorData();

        $rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'validateDataFormat',
                                    'failureDataException'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateDataFormat')
                         ->willReturn(false);

        $rule->expects($this->exactly(1))
                         ->method('failureDataException')
                         ->with($transformationSearchData, ErrorData::STATUS['PROGRAM_EXCEPTION'])
                         ->willReturn($errorData);

        $result = $rule->transformationResult($data, $transformationSearchData);
        $this->assertEquals($errorData, $result);
    }

    public function testTransformationResultValidateDataFormatTrueEmptyFailureItems()
    {
        //初始化
        $data = ['failureItems'=>''];
        $transformationSearchData = new MockSearchData();

        $rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'validateDataFormat',
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateDataFormat')
                         ->willReturn(true);

        $result = $rule->transformationResult($data, $transformationSearchData);
        $this->assertEquals($transformationSearchData, $result);
    }

    public function testTransformationResultValidateDataFormatTrueExistFailureItems()
    {
        //初始化
        $failureItems = [1=>'item'];
        $data = ['failureItems'=>$failureItems];
        $transformationSearchData = new MockSearchData();
        $errorData = new ErrorData();

        $rule = $this->getMockBuilder(MockComparisonRule::class)
                           ->setMethods(
                               [
                                    'validateDataFormat',
                                    'failureData',
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateDataFormat')
                         ->willReturn(true);

        $rule->expects($this->exactly(1))
                         ->method('failureData')
                         ->with($transformationSearchData, ErrorData::ERROR_TYPE['COMPARISON_FAILED'], $failureItems)
                         ->willReturn($errorData);

        $result = $rule->transformationResult($data, $transformationSearchData);
        $this->assertEquals($errorData, $result);
    }
}

<?php
namespace BaseData\Template\Adapter\WbjTemplate\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class WbjTemplateCache extends Cache
{
    const KEY = 'wbj_template';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

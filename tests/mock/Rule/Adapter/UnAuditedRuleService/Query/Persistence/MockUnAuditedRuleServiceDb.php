<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query\Persistence;

class MockUnAuditedRuleServiceDb extends UnAuditedRuleServiceDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Service;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Repository\SearchDataRepositoryFactory;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\IRule;
use BaseData\Rule\Repository\RuleServiceRepository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\SearchData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\BjItemsData;
use BaseData\ResourceCatalogData\Model\ErrorItemsData;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;

use BaseData\Template\Model\Template;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SyncTaskTraitTest extends TestCase
{
    private $trait;

    private $ruleService;

    private $task;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockSyncTaskTrait::class)
                            ->getMock();

        $this->ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService(1);
        $this->task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->ruleService);
        unset($this->task);
    }

    public function testGetErrorDataRepository()
    {
        $trait = new MockSyncTaskTrait();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\ErrorDataRepository',
            $trait->publicGetErrorDataRepository()
        );
    }

    public function testGetRuleServiceRepository()
    {
        $trait = new MockSyncTaskTrait();
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RuleServiceRepository',
            $trait->publicGetRuleServiceRepository()
        );
    }

    public function testSuccessNumber()
    {
        $trait = $this->getMockBuilder(MockSyncTaskTrait::class)
                            ->setMethods(['getTemplateType', 'getSearchDataRepositoryFactory'])
                            ->getMock();


        $list = array('list');
        $count = 1;
        $filter['task'] = $this->task->getId();
        $type = $this->ruleService->getTransformationCategory();

        $factory = $this->prophesize(SearchDataRepositoryFactory::class);
        $repository = $this->prophesize(WbjSearchDataRepository::class);

        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $factory->getRepository(Argument::exact($type))->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $trait->expects($this->once())->method('getSearchDataRepositoryFactory')->willReturn($factory->reveal());

        $result = $trait->publicSuccessNumber($this->task, $this->ruleService);

        $this->assertEquals($count, $result);
    }

    public function testFailureNumber()
    {
        $trait = $this->getMockBuilder(MockSyncTaskTrait::class)
                            ->setMethods(['getErrorDataRepository'])
                            ->getMock();

        $list = array('list');
        $count = 1;
        $filter['task'] = $this->task->getId();

        $repository = $this->prophesize(ErrorDataRepository::class);

        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $trait->expects($this->once())->method('getErrorDataRepository')->willReturn($repository->reveal());

        $result = $trait->publicFailureNumber($this->task);

        $this->assertEquals($count, $result);
    }

    public function testFetchRuleServiceList()
    {
        $trait = $this->getMockBuilder(MockSyncTaskTrait::class)
                            ->setMethods(['getRuleServiceRepository'])
                            ->getMock();

        $template = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);

        $list = array('list');
        $count = 1;
        $filter['sourceTemplateIds'] = $template->getId();
        $filter['sourceCategory'] = $template->getCategory();
        $filter['status'] = RuleService::STATUS['NORMAL'];

        $repository = $this->prophesize(RuleServiceRepository::class);

        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $trait->expects($this->once())->method('getRuleServiceRepository')->willReturn($repository->reveal());

        $result = $trait->publicFetchRuleServiceList($template);

        $this->assertEquals($list, $result);
    }

    public function testSearchDataTransformationAndAddTrue()
    {
        $sourceSearchDataList = [];
        $sourceSearchDataList[0] = new BjSearchData();
        $searchData = $this->prophesize(SearchData::class);
        $ruleService = $this->prophesize(RuleService::class);
        $task = new Task();

        $trait = new MockSyncTaskTrait();

        $searchData->add()->shouldBeCalledTimes(1)->willReturn(true);
        $searchData->setTask($task)->shouldBeCalledTimes(1);
        $ruleService->transformation($sourceSearchDataList[0])->shouldBeCalledTimes(1)
                                                              ->willReturn($searchData->reveal());

        $trait->publicSearchDataTransformationAndAdd($sourceSearchDataList, $ruleService->reveal(), $task);
    }

    public function testSearchDataTransformationAndAddFailureErrorSearchData()
    {
        $sourceSearchDataList = [];
        $sourceSearchDataList[0] = new FailureData();
        $searchData = $this->prophesize(FailureData::class);
        $ruleService = $this->prophesize(RuleService::class);
        $task = new Task();

        $trait = new MockSyncTaskTrait();

        $searchData->add()->shouldBeCalledTimes(1)->willReturn(true);
        $searchData->setTask($task)->shouldBeCalledTimes(1);
        $ruleService->transformation($sourceSearchDataList[0])->shouldBeCalledTimes(1)
                                                              ->willReturn($searchData->reveal());

        $trait->publicSearchDataTransformationAndAdd($sourceSearchDataList, $ruleService->reveal(), $task);
    }

    public function testSearchDataTransformationAndAddFailure()
    {
        $sourceSearchDataList = [];
        $sourceSearchDataList[0] = new BjSearchData();
        $searchData = $this->prophesize(SearchData::class);
        $errorSearchData = $this->prophesize(FailureData::class);

        $searchDataItemsData = new BjItemsData();
        $searchDataItemsData->setData(array('searchData'));

        $errorItemsData = new ErrorItemsData();
        $errorItemsData->setData($searchDataItemsData->getData());

        $ruleService = $this->prophesize(RuleService::class);
        $task = new Task();

        $trait = $this->getMockBuilder(MockSyncTaskTrait::class)
                            ->setMethods(['failureDataException'])
                            ->getMock();

        $searchData->getItemsData()->shouldBeCalledTimes(1)->willReturn($searchDataItemsData);
        $searchData->add()->shouldBeCalledTimes(1)->willReturn(false);
        $searchData->setTask($task)->shouldBeCalledTimes(1);
        $ruleService->transformation($sourceSearchDataList[0])->shouldBeCalledTimes(1)
                                                              ->willReturn($searchData->reveal());

        $errorSearchData->setTask($task)->shouldBeCalledTimes(1);

        $errorSearchData->getItemsData()->shouldBeCalledTimes(1)->willReturn($errorItemsData);
        $errorSearchData->generateHash()->shouldBeCalledTimes(1);
        $errorSearchData->add()->shouldBeCalledTimes(1);

        $trait->expects($this->once())->method('failureDataException')
                                      ->with($searchData->reveal(), ErrorData::STATUS['STORAGE_EXCEPTION'])
                                      ->willReturn($errorSearchData->reveal());

        $trait->publicSearchDataTransformationAndAdd($sourceSearchDataList, $ruleService->reveal(), $task);
    }

    //updateTask
    public function testUpdateTaskFailure()
    {
        $ruleService = new RuleService();
        $task = $this->prophesize(Task::class);

        $successNumber = 1;
        $failureNumber = 2;


        $trait = $this->getMockBuilder(MockSyncTaskTrait::class)
                            ->setMethods(['successNumber','failureNumber'])
                            ->getMock();

        $trait->expects($this->once())->method('successNumber')
                                      ->with($task->reveal(), $ruleService)
                                      ->willReturn($successNumber);

        $trait->expects($this->once())->method('failureNumber')
                                      ->with($task->reveal())
                                      ->willReturn($failureNumber);

        $task->setSuccessNumber($successNumber)->shouldBeCalledTimes(1);
        $task->setFailureNumber($failureNumber)->shouldBeCalledTimes(1);
        $task->setStatus(Task::STATUS['FAILURE']);
        $task->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $result = $trait->publicUpdateTask($task->reveal(), $ruleService);
        $this->assertTrue($result);
    }

    public function testUpdateTaskSuccess()
    {
        $ruleService = new RuleService();
        $task = $this->prophesize(Task::class);

        $successNumber = 1;
        $failureNumber = 0;

        $trait = $this->getMockBuilder(MockSyncTaskTrait::class)
                            ->setMethods(['successNumber','failureNumber'])
                            ->getMock();

        $trait->expects($this->once())->method('successNumber')
                                      ->with($task->reveal(), $ruleService)
                                      ->willReturn($successNumber);

        $trait->expects($this->once())->method('failureNumber')
                                      ->with($task->reveal())
                                      ->willReturn($failureNumber);

        $task->setSuccessNumber($successNumber)->shouldBeCalledTimes(1);
        $task->setFailureNumber($failureNumber)->shouldBeCalledTimes(1);
        $task->setStatus(Task::STATUS['SUCCESS']);
        $task->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $result = $trait->publicUpdateTask($task->reveal(), $ruleService);
        $this->assertTrue($result);
    }
}

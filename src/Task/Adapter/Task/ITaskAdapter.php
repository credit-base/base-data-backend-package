<?php
namespace BaseData\Task\Adapter\Task;

use BaseData\Task\Model\Task;

interface ITaskAdapter
{
    public function fetchOne($id) : Task;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Task $task) : bool;

    public function edit(Task $task, array $keys = array()) : bool;

    //调度返回可以运行的任务
    public function schedule() : array;

    //超时任务
    public function expire() : array;

    public function notifySubTasks(Task $parentTask) : bool;
}

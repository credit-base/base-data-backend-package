<?php
namespace BaseData\Template\Model;

use Marmot\Core;

use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;

use BaseData\UserGroup\Model\UserGroup;

class BjTemplate extends Template implements IOperate
{
    use OperateTrait;

    /**
     * @var int $sourceUnit 来源单位
     */
    protected $sourceUnit;
    /**
     * @var int $gbTemplate 国标目录
     */
    protected $gbTemplate;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->sourceUnit = new UserGroup();
        $this->gbTemplate = new GbTemplate();
        $this->repository = new BjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->gbTemplate);
        unset($this->repository);
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function setGbTemplate(GbTemplate $gbTemplate) : void
    {
        $this->gbTemplate = $gbTemplate;
    }

    public function getGbTemplate() : gbTemplate
    {
        return $this->gbTemplate;
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['BJ'];
    }

    protected function getRepository() : IBjTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        if (!$this->getRepository()->add($this)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'identify'));
            return false;
        }
        return true;
    }

    /**
     * @todo 编辑时，本次未考虑目录版本化
     */
    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));

        if (!$this->getRepository()->edit(
            $this,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'gbTemplate',
                'updateTime'
            )
        )) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'identify'));
            return false;
        }
        return true;
    }
}

<?php
namespace BaseData\Template\BjTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Translator\BjTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要查看我的本级目录时,在政务网OA中,可以查看到我的所有本级目录,
 *           通过列表和详情的形式查看所有本级目录,以便于我可以看到我的所有本级目录
 * @Scenario: 查看资源目录详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 存在一条资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_gb_template' => $this->gbTemplate(),
        ]);
    }

    /**
     * @When: 当我查看该条资源目录详情时
     */
    public function fetchBjTemplate($id)
    {
        $repository = new BjTemplateRepository();

        $bjTemplate = $repository->fetchOne($id);

        return $bjTemplate;
    }

    /**
     * @Then: 我可以看见该条资源目录的全部信息
     */
    public function testViewBjTemplateList()
    {
        $id = 1;
        
        $setBjTemplate = $this->bjTemplate()[0];
        $setBjTemplate['bj_template_id'] = $id;
        $setBjTemplate['subject_category'] = json_decode($setBjTemplate['subject_category'], true);
        $setBjTemplate['items'] = json_decode($setBjTemplate['items'], true);

        $bjTemplate = $this->fetchBjTemplate($id);
        $translator = new BjTemplateDbTranslator();
        $bjTemplateArray = $translator->objectToArray($bjTemplate);

        $this->assertEquals($bjTemplateArray, $setBjTemplate);
    }
}

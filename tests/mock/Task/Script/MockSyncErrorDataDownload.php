<?php
namespace BaseData\Task\Script;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\TaskRepository;
use BaseData\ResourceCatalogData\Service\SyncErrorDataDownloadService;

class MockSyncErrorDataDownload extends SyncErrorDataDownload
{
    public function getTaskRepository() : TaskRepository
    {
        return parent::getTaskRepository();
    }

    public function fetchTask(int $id) : Task
    {
        return parent::fetchTask($id);
    }

    public function getSyncErrorDataDownloadService(Task $task) : SyncErrorDataDownloadService
    {
        return parent::getSyncErrorDataDownloadService($task);
    }
}

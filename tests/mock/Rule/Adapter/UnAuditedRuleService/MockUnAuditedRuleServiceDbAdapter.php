<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService;

use Marmot\Interfaces\INull;

use Marmot\Interfaces\ITranslator;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Adapter\UnAuditedRuleService\Query\UnAuditedRuleServiceRowCacheQuery;

class MockUnAuditedRuleServiceDbAdapter extends UnAuditedRuleServiceDbAdapter
{
    public function getDbTranslator() : ITranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : UnAuditedRuleServiceRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function addAction(UnAuditedRuleService $unAuditedRuleService) : bool
    {
        return parent::addAction($unAuditedRuleService);
    }

    public function editAction(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return parent::editAction($unAuditedRuleService, $keys);
    }

    public function fetchOneAction($id) : UnAuditedRuleService
    {
        return parent::fetchOneAction($id);
    }

    public function fetchListAction(array $ids) : array
    {
        return parent::fetchListAction($ids);
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

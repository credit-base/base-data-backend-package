<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData\Query;

use Marmot\Framework\Interfaces\DbLayer;
use Marmot\Interfaces\CacheLayer;

class MockYsSearchDataRowCacheQuery extends YsSearchDataRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

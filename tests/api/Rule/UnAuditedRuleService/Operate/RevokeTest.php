<?php
namespace BaseData\Rule\UnAuditedRuleService\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Common\Model\IApproveAble;

use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\Model\UnAuditedRuleService;

/**
 * 测试规则接口审核撤销
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * 存在一条规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_apply_form_rule' => $this->applyFormRule()
            ]
        );
    }

    public function testViewUnAuditedRuleService()
    {
        $data = array(
            'data' => array(
                "type"=>"unAuditedRules",
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>2)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'unAuditedRules/1/revoke',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IApproveAble::APPLY_STATUS['REVOKE'], $attributes['applyStatus']);

        $relationships = $contents['data']['relationships'];
        $relationshipsCrew = $relationships['publishCrew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals(2, $relationshipsCrew['id']);
    }
}

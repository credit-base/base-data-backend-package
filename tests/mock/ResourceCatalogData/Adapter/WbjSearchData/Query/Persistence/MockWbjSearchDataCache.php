<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\Persistence;

class MockWbjSearchDataCache extends WbjSearchDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

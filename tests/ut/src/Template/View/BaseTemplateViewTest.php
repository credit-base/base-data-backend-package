<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\BaseTemplate;

class BaseTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $baseTemplate = new BaseTemplateView(new BaseTemplate());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $baseTemplate);
    }
}

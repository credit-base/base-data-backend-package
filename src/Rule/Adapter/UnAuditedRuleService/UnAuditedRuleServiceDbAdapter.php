<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Model\NullUnAuditedRuleService;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;
use BaseData\Rule\Adapter\UnAuditedRuleService\Query\UnAuditedRuleServiceRowCacheQuery;

use BaseData\Rule\Adapter\RuleServiceAdapterTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceDbAdapter implements IUnAuditedRuleServiceAdapter
{
    use DbAdapterTrait, RuleServiceAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new UnAuditedRuleServiceDbTranslator();
        $this->rowCacheQuery = new UnAuditedRuleServiceRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : ITranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : UnAuditedRuleServiceRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullUnAuditedRuleService::getInstance();
    }

    public function add(UnAuditedRuleService $unAuditedRuleService) : bool
    {
        return $this->addAction($unAuditedRuleService);
    }

    protected function addAction(UnAuditedRuleService $unAuditedRuleService) : bool
    {
        $info = array();
        $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService);

        $id = $this->getRowQuery()->add($info);
        if (!$id) {
            return false;
        }

        $unAuditedRuleService->setApplyId($id);
        return true;
    }

    public function edit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        return $this->editAction($unAuditedRuleService, $keys);
    }

    protected function editAction(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool
    {
        $info = array();
        $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService, $keys);

        $rowQuery = $this->getRowQuery();
        $conditionArray[$rowQuery->getPrimaryKey()] = $unAuditedRuleService->getApplyId();

        return $rowQuery->update($info, $conditionArray);
    }

    public function fetchOne($id) : UnAuditedRuleService
    {
        $unAuditedRuleService = $this->fetchOneAction($id);

        $this->fetchRules($unAuditedRuleService);
        $this->fetchSourceTemplate($unAuditedRuleService);
        $this->fetchTransformationTemplate($unAuditedRuleService);

        return $unAuditedRuleService;
    }

    protected function fetchOneAction($id) : UnAuditedRuleService
    {
        $info = array();

        $info = $this->getRowQuery()->fetchOne($id);
        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return $this->getNullObject();
        }

        $applyInfo = unserialize(gzuncompress(base64_decode($info['apply_info'], true)));

        $info = array_merge($applyInfo, $info);

        $translator = $this->getDbTranslator();

        $unAuditedRuleService = $translator->arrayToObject($info);
        $unAuditedRuleService = $translator->arrayToObjects($info, $unAuditedRuleService);

        return $unAuditedRuleService;
    }

    public function fetchList(array $ids) : array
    {
        $unAuditedRuleServiceList = $this->fetchListAction($ids);

        $this->fetchSourceTemplate($unAuditedRuleServiceList);
        $this->fetchTransformationTemplate($unAuditedRuleServiceList);

        return $unAuditedRuleServiceList;
    }

    protected function fetchListAction(array $ids) : array
    {
        $unAuditedRuleServiceList = array();
        
        $unAuditedRuleServiceInfoList = $this->getRowQuery()->fetchList($ids);

        if (empty($unAuditedRuleServiceInfoList)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return array();
        }

        foreach ($unAuditedRuleServiceInfoList as $unAuditedRuleServiceInfo) {
            $translator = $this->getDbTranslator();

            $applyInfo =unserialize(gzuncompress(base64_decode($unAuditedRuleServiceInfo['apply_info'], true)));

            $unAuditedRuleServiceInfo = array_merge($applyInfo, $unAuditedRuleServiceInfo);

            $unAuditedRuleService = $translator->arrayToObject($unAuditedRuleServiceInfo);

            $unAuditedRuleService = $translator->arrayToObjects($unAuditedRuleServiceInfo, $unAuditedRuleService);

            $unAuditedRuleServiceList[$unAuditedRuleService->getApplyId()] = $unAuditedRuleService;
        }

        return $unAuditedRuleServiceList;
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $unAuditedRuleService = new UnAuditedRuleService();
            if (isset($filter['userGroup'])) {
                $unAuditedRuleService->getUserGroup()->setId($filter['userGroup']);
                $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService, array('userGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['relationId'])) {
                $unAuditedRuleService->setRelationId($filter['relationId']);
                $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService, array('relationId'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['transformationTemplate'])) {
                $unAuditedRuleService->getTransformationTemplate()->setId($filter['transformationTemplate']);
                $info = $this->getDbTranslator()->objectToArray(
                    $unAuditedRuleService,
                    array('transformationTemplate')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceTemplate'])) {
                $unAuditedRuleService->getSourceTemplate()->setId($filter['sourceTemplate']);
                $info = $this->getDbTranslator()->objectToArray(
                    $unAuditedRuleService,
                    array('sourceTemplate')
                );
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['applyStatus'])) {
                $unAuditedRuleService->setApplyStatus(0);
                $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService, array('applyStatus'));
                $condition .= $conjection.key($info).' IN ('.$filter['applyStatus'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['transformationCategory'])) {
                $info = $this->getDbTranslator()->objectToArray(
                    $unAuditedRuleService,
                    array('transformationCategory')
                );
                $condition .= $conjection.key($info).' IN ('.$filter['transformationCategory'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['excludeTransformationCategory'])) {
                $unAuditedRuleService->setTransformationCategory($filter['excludeTransformationCategory']);
                $info = $this->getDbTranslator()->objectToArray(
                    $unAuditedRuleService,
                    array('transformationCategory')
                );
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceCategory'])) {
                $info = $this->getDbTranslator()->objectToArray(
                    $unAuditedRuleService,
                    array('sourceCategory')
                );
                $condition .= $conjection.key($info).' IN ('.$filter['sourceCategory'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['operationType'])) {
                $unAuditedRuleService->setOperationType($filter['operationType']);
                $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService, array('operationType'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceTemplateIds'])) {
                $info = $this->getDbTranslator()->objectToArray(
                    $unAuditedRuleService,
                    array('sourceTemplate')
                );
                $condition .= $conjection.key($info).' IN ('.$filter['sourceTemplateIds'].')';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $unAuditedRuleService = new UnAuditedRuleService();

            foreach ($sort as $key => $val) {
                if ($key == 'updateTime') {
                    $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService, array('updateTime'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'applyStatus') {
                    $info = $this->getDbTranslator()->objectToArray($unAuditedRuleService, array('applyStatus'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
            }
        }
        
        return $condition;
    }
}

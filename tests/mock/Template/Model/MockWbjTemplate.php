<?php
namespace BaseData\Template\Model;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;

class MockWbjTemplate extends WbjTemplate
{
    public function getRepository() : IWbjTemplateAdapter
    {
        return parent::getRepository();
    }
}

<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Rule\Command\RuleService\AddRuleServiceCommand;

use BaseData\Template\Model\Template;

class AddRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    const SOURCE_TEMPLATE_HAVE_USER_GROUP_CATEGORY = array(
        Template::CATEGORY['WBJ'], Template::CATEGORY['BJ']
    );
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof AddRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $sourceRepository = $this->getRepository($command->sourceCategory);
        $sourceTemplate = $sourceRepository->fetchOne($command->sourceTemplate);

        $transformationRepository = $this->getRepository($command->transformationCategory);
        $transformationTemplate = $transformationRepository->fetchOne($command->transformationTemplate);

        $unAuditedRuleService = $this->getUnAuditedRuleService();
        
        $unAuditedRuleService->setTransformationTemplate($transformationTemplate);
        $unAuditedRuleService->setSourceTemplate($sourceTemplate);
        if (in_array($sourceTemplate->getCategory(), self::SOURCE_TEMPLATE_HAVE_USER_GROUP_CATEGORY)) {
            $unAuditedRuleService->setUserGroup($sourceTemplate->getSourceUnit());
        }
        $unAuditedRuleService->generateVersion();
        $unAuditedRuleService->setTransformationCategory($command->transformationCategory);
        $unAuditedRuleService->setSourceCategory($command->sourceCategory);
        $unAuditedRuleService->setOperationType($command->operationType);
        $unAuditedRuleService = $this->generateUnAuditedRuleServiceCrew($command->crew, $unAuditedRuleService);
        $unAuditedRuleService = $this->generateUnAuditedRuleServiceRules($command->rules, $unAuditedRuleService);
        $applyInfo = $this->applyInfo($unAuditedRuleService);
        $unAuditedRuleService->setApplyInfo($applyInfo);

        if ($unAuditedRuleService->add()) {
            $command->id = $unAuditedRuleService->getApplyId();
            return true;
        }
        
        return false;
    }
}

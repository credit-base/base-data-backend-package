<?php
namespace BaseData\Template\GbTemplate\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Translator\GbTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我之前的国标目录需要更改时,在目录管理子系统,修改国标目录内容
 *           根据我需要 修改的内容和规范修改原有国标目录
 *           以便于我有国标目录可以使用
 * @Scenario: 编辑资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
        ]);
    }

    /**
     * @When: 获取需要编辑的用户数据
     */
    public function fetchTemplate($id)
    {
        $repository = new GbTemplateRepository();

        $gbTemplate = $repository->fetchOne($id);

        return $gbTemplate;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $gbTemplate = $this->fetchTemplate(1);

        $gbTemplate->setSubjectCategory(array(Template::SUBJECT_CATEGORY['ZRR']));
        $gbTemplate->setDimension(Template::DIMENSION['ZWGX']);
        $gbTemplate->setExchangeFrequency(2);
        $gbTemplate->setInfoClassify(Template::INFO_CLASSIFY['XZXK']);
        $gbTemplate->setInfoCategory(Template::INFO_CATEGORY['SHOUXXX']);
        $gbTemplate->setItems(array('items'));
        
        return $gbTemplate->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $gbTemplate = $this->fetchTemplate(1);

        $this->assertEquals(array(Template::SUBJECT_CATEGORY['ZRR']), $gbTemplate->getSubjectCategory());
        $this->assertEquals(Template::DIMENSION['ZWGX'], $gbTemplate->getDimension());
        $this->assertEquals(2, $gbTemplate->getExchangeFrequency());
        $this->assertEquals(Template::INFO_CLASSIFY['XZXK'], $gbTemplate->getInfoClassify());
        $this->assertEquals(Template::INFO_CATEGORY['SHOUXXX'], $gbTemplate->getInfoCategory());
        $this->assertEquals(array('items'), $gbTemplate->getItems());
    }
}

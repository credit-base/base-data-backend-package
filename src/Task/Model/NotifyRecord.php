<?php
namespace BaseData\Task\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use BaseData\Task\Repository\NotifyRecordRepository;
use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;

use BaseData\Task\Script\SyncTask;

/**
 * notify 触发记录表，用于记录 notify 的触发记录信息
 * @todo 设置错误原因
 */
class NotifyRecord implements IObject
{
    const STATUS = [
        'PENDING' => 0,
        'SUCCESS'=>2
    ];

    const ALLOW_FILE_EXTS = array(
        'xls', 'xlsx'
    );

    use Object;

    private $id;
    /**
     * @param string $name 文件名字
     */
    private $name;
    /**
     * @param string $hash 文件hash
     */
    private $hash;

    private $syncTask;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->hash = '';
        $this->status = self::STATUS['PENDING'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new NotifyRecordRepository();
        $this->syncTask = new SyncTask();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->hash);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
        unset($this->syncTask);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setHash(string $hash) : void
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    protected function generateHash() : string
    {
        $name = explode('.', $this->getName());
        $fileName = $name[0];
        $fileNameArray = explode('_', $fileName);

        $hash = end($fileNameArray);

        $this->setHash($hash);
        return $hash;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            self::STATUS
        ) ? $status : self::STATUS['PENDING'];
    }

    protected function getRepository() : INotifyRecordAdapter
    {
        return $this->repository;
    }

    protected function getSyncTask() : SyncTask
    {
        return $this->syncTask;
    }
    
    public function add() : bool
    {
        return $this->generateHash() &&
               $this->validate() &&
               $this->addAction() &&
               $this->notify();
    }

    protected function validate() : bool
    {
        $name = $this->getName();
        $name = explode('.', $name);
        $fileName = isset($name[0]) ? $name[0] : '';
        $extension = isset($name[1]) ? $name[1] : '';

        return $this->validateFileName($fileName) && $this->validateExtension($extension);
    }

    /**
     * 验证文件名是否正确，是否资源目录标识, 类型, 员工id, hash 均不为空
     */
    protected function validateFileName(string $fileName) : bool
    {
        $fileName = explode('_', $fileName);
        $templateName = isset($fileName[0]) ? $fileName[0] : '';
        $templateCategory = isset($fileName[1]) ? $fileName[1] : 0;
        $crewId = isset($fileName[2]) ? $fileName[2] : 0;
        $hash = isset($fileName[3]) ? $fileName[3] : 0;

        return !empty($templateName) &&
               !empty($templateCategory) &&
               !empty($crewId) &&
               !empty($hash);
    }

    protected function validateExtension(string $extension) : bool
    {
        if (!in_array($extension, self::ALLOW_FILE_EXTS)) {
            return false;
        }

        return true;
    }

    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    /**
     * 根据文件名触发生成子任务
     */
    public function notify() : bool
    {
        $parentTask = $this->getSyncTask();
        $parentTask->setData(array('name' => $this->getName()));

        return $parentTask->add();
    }
}

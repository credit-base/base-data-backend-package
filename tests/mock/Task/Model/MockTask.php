<?php
namespace BaseData\Task\Model;

use BaseData\Task\Adapter\Task\ITaskAdapter;

class MockTask extends Task
{
    public function executeAction() : bool
    {
        return true;
    }

    public function getRepository() : ITaskAdapter
    {
        return parent::getRepository();
    }

    public function beforeExecute() : bool
    {
        return parent::beforeExecute();
    }

    public function afterExecute() : bool
    {
        return parent::afterExecute();
    }

    public function updateProcessId() : bool
    {
        return parent::updateProcessId();
    }

    public function notifySubTasks() : bool
    {
        return parent::notifySubTasks();
    }
    
    public function addSubTasks(array $subTasks = array()) : bool
    {
        return parent::addSubTasks($subTasks);
    }
}

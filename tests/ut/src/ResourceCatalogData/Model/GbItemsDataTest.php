<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter;

class GbItemsDataTest extends TestCase
{
    private $gbItemsData;

    public function setUp()
    {
        $this->gbItemsData = new GbItemsData();
    }

    public function tearDown()
    {
        unset($this->gbItemsData);
    }

    public function testGetRepository()
    {
        $gbItemsData = new MockGbItemsData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter',
            $gbItemsData->getRepository()
        );
    }

    public function testAddSuccess()
    {
        $gbItemsData = $this->getMockBuilder(GbItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IGbItemsDataAdapter::class);
        $repository->add(Argument::exact($gbItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $gbItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $gbItemsData->add();
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        $gbItemsData = $this->getMockBuilder(GbItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IGbItemsDataAdapter::class);
        $repository->add(Argument::exact($gbItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        $gbItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $gbItemsData->add();
        $this->assertFalse($result);
    }
}

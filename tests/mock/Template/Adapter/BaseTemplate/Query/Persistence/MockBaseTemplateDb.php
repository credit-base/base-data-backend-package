<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query\Persistence;

class MockBaseTemplateDb extends BaseTemplateDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

<?php
namespace BaseData\Rule\UnAuditedRuleService\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;

use BaseData\Template\Model\Template;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有目录规则审核权限，当我想要查看某个资源目录的规则时,
 *           在政务网OA中的目录规则审核模块,我可以搜索规则,通过列表形式查看我搜索到的规则审核数据,以便于我快速定位规则并进行下一步操作
 * @Scenario: 通过来源资源目录类型搜索
 */
class SourceCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * @Given: 存在规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form_rule' => $this->applyFormRule()
        ]);
    }
    /**
     * @When: 当我查看规则数据列表时
     */
    public function fetchUnAuditedRuleServiceList()
    {
        $repository = new UnAuditedRuleServiceRepository();
 
        $filter['sourceCategory'] = Template::CATEGORY['WBJ'];

        list($unAuditedRuleServiceList, $count) = $repository->filter($filter);
        
        unset($count);

        return $unAuditedRuleServiceList;
    }

    /**
     * @Then: 我可以看到规则来源资源目录类型为"委办局"的规则数据
     */
    public function testViewRuleList()
    {
        $setRuleList = $this->applyFormRule();

        foreach ($setRuleList as $key => $setRule) {
            unset($setRule);
            $setRuleList[$key]['apply_form_rule_id'] = $key +1;
        }

        $unAuditedRuleServiceList = $this->fetchUnAuditedRuleServiceList();
        $translator = new UnAuditedRuleServiceDbTranslator();
        foreach ($unAuditedRuleServiceList as $rule) {
            $ruleArray[] = $translator->objectToArray($rule);
        }

        $this->assertEquals($ruleArray, $setRuleList);

        foreach ($ruleArray as $rule) {
            $this->assertEquals(Template::CATEGORY['WBJ'], $rule['source_category']);
        }
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\Persistence;

class MockWbjSearchDataDb extends WbjSearchDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

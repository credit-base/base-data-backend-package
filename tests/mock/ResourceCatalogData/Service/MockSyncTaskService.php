<?php
namespace BaseData\ResourceCatalogData\Service;

use BaseData\Template\Model\Template;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\FormatValidationRule;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Repository\CrewRepository;

use BaseData\ResourceCatalogData\Model\Task;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockSyncTaskService extends SyncTaskService
{
    public function getFileName() : string
    {
        return parent::getFileName();
    }

    public function getCrewRepository() : CrewRepository
    {
        return parent::getCrewRepository();
    }

    public function fetchCrew(int $id) : Crew
    {
        return parent::fetchCrew($id);
    }

    public function getRuleService() : RuleService
    {
        return parent::getRuleService();
    }

    public function getFormatValidationRule() : FormatValidationRule
    {
        return parent::getFormatValidationRule();
    }
    
    public function getTask() : Task
    {
        return parent::getTask();
    }

    public function validateFileFormat(string $fileName) : bool
    {
        return parent::validateFileFormat($fileName);
    }

    public function isFileExist(string $fileName) : bool
    {
        return parent::isFileExist($fileName);
    }

    public function isFileNameFormatCorrect(string $fileName) : bool
    {
        return parent::isFileNameFormatCorrect($fileName);
    }

    public function initializationData(string $fileName) : array
    {
        return parent::initializationData($fileName);
    }

    public function fetchSourceTemplate(int $type, string $identify, int $sourceTemplateId) : Template
    {
        return parent::fetchSourceTemplate($type, $identify, $sourceTemplateId);
    }

    public function fetchFileContent(string $fileName) : array
    {
        return parent::fetchFileContent($fileName);
    }

    public function validateDataCorrect(array $data) : bool
    {
        return parent::validateDataCorrect($data);
    }

    public function isCrewExist(Crew $crew) : bool
    {
        return parent::isCrewExist($crew);
    }

    public function isSourceTemplateExist(Template $sourceTemplate) : bool
    {
        return parent::isSourceTemplateExist($sourceTemplate);
    }

    public function isFileContentEmpty(array $fileContent) : bool
    {
        return parent::isFileContentEmpty($fileContent);
    }

    public function addTask(array $data) : Task
    {
        return parent::addTask($data);
    }

    public function creditRuleService(Template $sourceTemplate) : RuleService
    {
        return parent::creditRuleService($sourceTemplate);
    }

    public function fileContentToSearchDataObjectArray(array $data) : array
    {
        return parent::fileContentToSearchDataObjectArray($data);
    }

    public function generateSourceSearchData(array $data) : ISearchDataAble
    {
        return parent::generateSourceSearchData($data);
    }

    public function getSourceSearchData(int $type) : ISearchDataAble
    {
        return parent::getSourceSearchData($type);
    }

    public function fileDataToItemsData(array $fileData, array $items) : array
    {
        return parent::fileDataToItemsData($fileData, $items);
    }

    public function generateSubjectCategory(array $itemsData) : int
    {
        return parent::generateSubjectCategory($itemsData);
    }

    public function getReader(string $fileName) : \PhpOffice\PhpSpreadsheet\Reader\BaseReader
    {
        return parent::getReader($fileName);
    }
}

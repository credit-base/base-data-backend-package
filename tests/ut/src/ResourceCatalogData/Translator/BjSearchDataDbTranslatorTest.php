<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\BjSearchDataUtils;
use BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory;

class BjSearchDataDbTranslatorTest extends TestCase
{
    use BjSearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjSearchDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullBjSearchData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData(1);

        $expression['bj_search_data_id'] = $bjSearchData->getId();
        $expression['info_classify'] = $bjSearchData->getInfoClassify();
        $expression['info_category'] = $bjSearchData->getInfoCategory();
        $expression['crew_id'] = $bjSearchData->getCrew()->getId();
        $expression['usergroup_id'] = $bjSearchData->getSourceUnit()->getId();
        $expression['subject_category'] = $bjSearchData->getSubjectCategory();
        $expression['dimension'] = $bjSearchData->getDimension();
        $expression['name'] = $bjSearchData->getName();
        $expression['identify'] = $bjSearchData->getIdentify();
        $expression['expiration_date'] = $bjSearchData->getExpirationDate();
        $expression['bj_template_id'] = $bjSearchData->getTemplate()->getId();
        $expression['bj_items_data_id'] = $bjSearchData->getItemsData()->getId();
        $expression['status'] = $bjSearchData->getStatus();
        $expression['hash'] = $bjSearchData->getHash();
        $expression['status_time'] = $bjSearchData->getStatusTime();
        $expression['create_time'] = $bjSearchData->getCreateTime();
        $expression['update_time'] = $bjSearchData->getUpdateTime();
        $expression['task_id'] = $bjSearchData->getTask()->getId();
        $expression['description'] = $bjSearchData->getDescription();
        $expression['front_end_processor_status'] = $bjSearchData->getFrontEndProcessorStatus();

        $bjSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\BjSearchData', $bjSearchData);

        $this->compareArrayAndObject($expression, $bjSearchData);
    }

    public function testObjectToArray()
    {
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData(1);

        $expression = $this->translator->objectToArray($bjSearchData);

        $this->compareArrayAndObject($expression, $bjSearchData);
    }
}

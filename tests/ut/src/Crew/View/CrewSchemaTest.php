<?php
namespace BaseData\Crew\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class CrewSchemaTest extends TestCase
{
    private $crewSchema;

    private $crew;

    public function setUp()
    {
        $this->crewSchema = new CrewSchema(new Factory());

        $this->crew = \BaseData\Crew\Utils\MockFactory::generateCrew(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->crewSchema);
        unset($this->crew);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->crewSchema);
    }

    public function testGetId()
    {
        $result = $this->crewSchema->getId($this->crew);

        $this->assertEquals($result, $this->crew->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->crewSchema->getAttributes($this->crew);

        $this->assertEquals($result['realName'], $this->crew->getRealName());
        $this->assertEquals($result['cellphone'], $this->crew->getCellphone());
        $this->assertEquals($result['status'], $this->crew->getStatus());
        $this->assertEquals($result['createTime'], $this->crew->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->crew->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->crew->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->crewSchema->getRelationships($this->crew, 0, array());

        $this->assertEquals($result['userGroup'], ['data' => $this->crew->getUserGroup()]);
    }
}

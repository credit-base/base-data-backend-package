<?php
namespace BaseData\Task\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullNotifyRecordTest extends TestCase
{
    private $notifyRecord;

    public function setUp()
    {
        $this->notifyRecord = $this->getMockBuilder(NullNotifyRecord::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->notifyRecord);
    }

    public function testExtendsNotifyRecord()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Model\NotifyRecord',
            $this->notifyRecord
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->notifyRecord
        );
    }

    public function testAdd()
    {
        $this->notifyRecord->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->notifyRecord->add());
    }

    public function testResourceNotExist()
    {
        $notifyRecord = new MockNullNotifyRecord();
        $result = $notifyRecord->resourceNotExist();
        
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}

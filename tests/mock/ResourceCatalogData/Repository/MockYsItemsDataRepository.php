<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;

class MockYsItemsDataRepository extends YsItemsDataRepository
{
    public function getAdapter() : IYsItemsDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IYsItemsDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IYsItemsDataAdapter
    {
        return parent::getMockAdapter();
    }
}

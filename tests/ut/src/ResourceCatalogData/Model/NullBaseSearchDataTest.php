<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullBaseSearchDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullBaseSearchData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsBaseSearchData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\BaseSearchData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

<?php
namespace BaseData\WorkOrderTask\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskDbAdapter;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\WorkOrderTaskMockAdapter;

class WorkOrderTaskRepository extends Repository implements IWorkOrderTaskAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WorkOrderTaskDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWorkOrderTaskAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWorkOrderTaskAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWorkOrderTaskAdapter
    {
        return new WorkOrderTaskMockAdapter();
    }

    public function fetchOne($id) : WorkOrderTask
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(WorkOrderTask $workOrderTask) : bool
    {
        return $this->getAdapter()->add($workOrderTask);
    }

    public function edit(WorkOrderTask $workOrderTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($workOrderTask, $keys);
    }
}

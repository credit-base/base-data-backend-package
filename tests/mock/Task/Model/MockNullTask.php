<?php
namespace BaseData\Task\Model;

class MockNullTask extends NullTask
{
    public function executeAction() : bool
    {
        return parent::executeAction();
    }

    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}

<?php
namespace BaseData\WorkOrderTask\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

use BaseData\WorkOrderTask\Model\ParentTask;

class ParentTaskSchema extends SchemaProvider
{
    protected $resourceType = 'parentTasks';

    public function getId($parentTask) : int
    {
        return $parentTask->getId();
    }

    public function getAttributes($parentTask) : array
    {
        return [
            'templateType' => $parentTask->getTemplateType(),
            'title' => $parentTask->getTitle(),
            'description' => $parentTask->getDescription(),
            'endTime' => $parentTask->getEndTime(),
            'attachment' => $parentTask->getAttachment(),
            'ratio' => $this->calRatio($parentTask),
            'reason' => $parentTask->getReason(),

            'status' => $parentTask->getStatus(),
            'createTime' => $parentTask->getCreateTime(),
            'updateTime' => $parentTask->getUpdateTime(),
            'statusTime' => $parentTask->getStatusTime(),
        ];
    }

    public function getRelationships($parentTask, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'template' => [self::DATA => $parentTask->getTemplate()],
            'assignObjects' => [self::DATA => $parentTask->getAssignObjects()]
        ];
    }

    protected function calRatio(ParentTask $parentTask) : int
    {
        $ratio = 0;
        if (count($parentTask->getAssignObjects())) {
            $ratio = intval(($parentTask->getFinishCount()/count($parentTask->getAssignObjects()))*100);
        }
        if ($ratio > 100) {
            $ratio = 100;
        }
        return $ratio;
    }
}

<?php
namespace BaseData\Template\Model;

use PHPUnit\Framework\TestCase;

class NullBjTemplateTest extends TestCase
{
    private $bjTemplate;

    public function setUp()
    {
        $this->bjTemplate = NullBjTemplate::getInstance();
    }

    public function tearDown()
    {
        unset($this->bjTemplate);
    }

    public function testExtendsBjTemplate()
    {
        $this->assertInstanceof('BaseData\Template\Model\BjTemplate', $this->bjTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->bjTemplate);
    }
}

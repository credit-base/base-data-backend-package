<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class BaseTemplateCache extends Cache
{
    const KEY = 'base_template';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

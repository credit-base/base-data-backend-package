<?php
namespace BaseData\Template;

use Marmot\Core;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\QzjTemplate;

trait SetDataApiTrait
{
    private function items(
        array $subjectCategoryOptions = [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]],
        array $dimensionOptions = [Template::DIMENSION_CN[Template::DIMENSION['SHGK']]],
        array $exchangeFrequencyOptions = [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]],
        array $infoClassifyOptions = [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['QT']]],
        array $infoCategoryOptions = [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['QTXX']]]
    ) : array {
        return
        [
            [
                "name" => '主体名称',    //信息项名称
                "identify" => 'ZTMC',    //数据标识
                "type" => 1,    //数据类型
                "length" => 200,    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则
                "remarks" => '主体名称',    //备注
            ],
            [
                "name" => '统一社会信用代码',    //信息项名称
                "identify" => 'TYSHXYDM',    //数据标识
                "type" => 1,    //数据类型
                "length" => 50,    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '主体代码',    //备注
            ],
            [
                "name" => '主体类别',    //信息项名称
                "identify" => 'ZTLB',    //数据标识
                "type" => 6,    //数据类型
                "length" => 50,    //数据长度
                "options" => $subjectCategoryOptions,    //可选范围
                "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '法人及非法人组织;自然人;个体工商户，支持多选',    //备注
            ],
            [
                "name" => '公开范围',    //信息项名称
                "identify" => 'GKFW',    //数据标识
                "type" => 5,    //数据类型
                "length" => 20,    //数据长度
                "options" => $dimensionOptions,    //可选范围
                "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '支持单选',    //备注
            ],
            [
                "name" => '更新频率',    //信息项名称
                "identify" => 'GXPL',    //数据标识
                "type" => 5,    //数据类型
                "length" => 20,    //数据长度
                "options" => $exchangeFrequencyOptions,    //可选范围
                "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '支持单选',    //备注
            ],
            [
                "name" => '信息分类',    //信息项名称
                "identify" => 'XXFL',    //数据标识
                "type" => 5,    //数据类型
                "length" => 50,    //数据长度
                "options" => $infoClassifyOptions,    //可选范围
                "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '支持单选',    //备注
            ],
            [
                "name" => '信息类别',    //信息项名称
                "identify" => 'XXLB',    //数据标识
                "type" => 5,    //数据类型
                "length" => 50,    //数据长度
                "options" => $infoCategoryOptions,    //可选范围
                "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '信息性质类型，支持单选',    //备注
            ],
        ];
    }

    protected function wbjTemplate() : array
    {
        $itemsOne = $this->items();
        $itemsTwo = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]],
            [Template::DIMENSION_CN[Template::DIMENSION['SHGK']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['SHOUXXX']]]
        );

        return
        [
            [
                'wbj_template_id' => 1,
                'name' => '委办局企业基本信息',
                'identify' => 'WBJQYJBXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'委办局企业基本信息',
                'items' => json_encode($itemsOne, true),
                'source_unit_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'wbj_template_id' => 2,
                'name' => '委办局行政许可信息',
                'identify' => 'WBJXZXKXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                'description' =>'委办局行政许可信息',
                'items' => json_encode($itemsTwo, true),
                'source_unit_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function bjTemplate() : array
    {
        $itemsOne = $this->items();
        $itemsTwo = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]],
            [Template::DIMENSION_CN[Template::DIMENSION['SHGK']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['SHOUXXX']]]
        );

        return
        [
            [
                'bj_template_id' => 1,
                'name' => '本级企业基本信息',
                'identify' => 'BJQYJBXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'本级企业基本信息',
                'items' => json_encode($itemsOne, true),
                'source_unit_id' => 1,
                'gb_template_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'bj_template_id' => 2,
                'name' => '本级行政许可信息',
                'identify' => 'BJXZXKXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                'description' =>'本级行政许可信息',
                'items' => json_encode($itemsTwo, true),
                'source_unit_id' => 1,
                'gb_template_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function gbTemplate() : array
    {
        $itemsOne = $this->items();
        $itemsTwo = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]],
            [Template::DIMENSION_CN[Template::DIMENSION['SHGK']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['SHOUXXX']]]
        );

        return
        [
            [
                'gb_template_id' => 1,
                'name' => '国标企业基本信息',
                'identify' => 'GBQYJBXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'国标企业基本信息',
                'items' => json_encode($itemsOne, true),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'gb_template_id' => 2,
                'name' => '国标行政许可信息',
                'identify' => 'GBXZXKXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                'description' =>'国标行政许可信息',
                'items' => json_encode($itemsTwo, true),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function baseTemplate() : array
    {
        $itemsOne = $this->items();
        $itemsTwo = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]],
            [Template::DIMENSION_CN[Template::DIMENSION['SHGK']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['SHOUXXX']]]
        );

        return
        [
            [
                'base_template_id' => 1,
                'name' => '基础企业基本信息',
                'identify' => 'JCQYJBXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'基础企业基本信息',
                'items' => json_encode($itemsOne, true),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'base_template_id' => 2,
                'name' => '基础行政许可信息',
                'identify' => 'JCXZXKXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                'description' =>'基础行政许可信息',
                'items' => json_encode($itemsTwo, true),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function qzjTemplate() : array
    {
        $itemsOne = $this->items();
        $itemsTwo = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['FRJFFRZZ']]],
            [Template::DIMENSION_CN[Template::DIMENSION['SHGK']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['SHOUXXX']]]
        );

        return
        [
            [
                'qzj_template_id' => 1,
                'name' => '前置机企业基本信息',
                'identify' => 'QZJQYJBXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'前置机企业基本信息',
                'items' => json_encode($itemsOne, true),
                'source_unit_id' => 1,
                'category' => QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'],
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'qzj_template_id' => 2,
                'name' => '前置机行政许可信息',
                'identify' => 'QZJXZXKXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => Template::EXCHANGE_FREQUENCY['SS'],
                'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                'description' =>'前置机行政许可信息',
                'items' => json_encode($itemsTwo, true),
                'source_unit_id' => 1,
                'category' => QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'],
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }
}

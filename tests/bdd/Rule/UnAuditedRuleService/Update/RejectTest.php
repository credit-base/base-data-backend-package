<?php
namespace BaseData\Rule\UnAuditedRuleService\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Common\Model\IApproveAble;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,当委办局需要将数据导入到自己选择的资源目录时,在政务网OA中,可以对委办局设置的规则进行审核
 *           通过审核委办局提交的规则申请，查看委办局通过规则导入数据的情况进行管理,以便于我可以通过规则管理委办局导入的数据
 * @Scenario: 审核驳回
 */
class RejectTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * @Given: 存在需要审核驳回的规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form_rule' => $this->applyFormRule(IApproveAble::APPLY_STATUS['PENDING'])
        ]);
    }

    /**
     * @When: 获取需要审核驳回的规则数据
     */
    public function fetchUnAuditedRuleService($id)
    {
        $repository = new UnAuditedRuleServiceRepository();

        $unAuditedRuleService = $repository->fetchOne($id);

        return $unAuditedRuleService;
    }

    /**
     * @And: 当我调用审核驳回函数,期待返回true
     */
    public function reject()
    {
        $unAuditedRuleService = $this->fetchUnAuditedRuleService(1);
        $unAuditedRuleService->setRejectReason('测试驳回原因');
        $unAuditedRuleService->getApplyCrew()->setId(1);
        
        return $unAuditedRuleService->reject();
    }

    /**
     * @Then: 数据已经被审核驳回
     */
    public function testValidate()
    {
        $result = $this->reject();

        $this->assertTrue($result);

        $unAuditedRuleService = $this->fetchUnAuditedRuleService(1);

        $this->assertEquals(IApproveAble::APPLY_STATUS['REJECT'], $unAuditedRuleService->getApplyStatus());
        $this->assertEquals('测试驳回原因', $unAuditedRuleService->getRejectReason());
        $this->assertEquals(Core::$container->get('time'), $unAuditedRuleService->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $unAuditedRuleService->getStatusTime());
        $this->assertEquals(1, $unAuditedRuleService->getApplyCrew()->getId());
    }
}

<?php
namespace BaseData\Template\Adapter\BjTemplate;

use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Utils\BjTemplateMockFactory;

class BjTemplateMockAdapter implements IBjTemplateAdapter
{
    public function fetchOne($id) : BjTemplate
    {
        return BjTemplateMockFactory::generateBjTemplate($id);
    }

    public function fetchList(array $ids) : array
    {
        $bjTemplateList = array();

        foreach ($ids as $id) {
            $bjTemplateList[$id] = BjTemplateMockFactory::generateBjTemplate($id);
        }

        return $bjTemplateList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(BjTemplate $bjTemplate) : bool
    {
        unset($bjTemplate);
        return true;
    }

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool
    {
        unset($bjTemplate);
        unset($keys);
        return true;
    }
}

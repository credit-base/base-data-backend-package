<?php
namespace BaseData\Template\Controller;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use Marmot\Interfaces\IView;

class MockBjTemplateFetchController extends BjTemplateFetchController
{
    public function getRepository() : IBjTemplateAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

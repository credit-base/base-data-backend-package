<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class DisableWbjSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDisableWbjSearchDataCommandHandler::class)
                       ->setMethods(['fetchWbjSearchData'])
                       ->getMock();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'BaseData\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $wbjSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);

        $this->stub->expects($this->once())
             ->method('fetchWbjSearchData')
             ->with($id)
             ->willReturn($wbjSearchData);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $wbjSearchData);
    }
}

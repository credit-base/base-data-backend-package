<?php
namespace BaseData\Rule\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Utils\MockFactory;
use BaseData\Rule\Model\ModelFactory;
use BaseData\Rule\Utils\RuleServiceUtils;

class RuleServiceDbTranslatorTest extends TestCase
{
    use RuleServiceUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new RuleServiceDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Rule\Model\NullRuleService', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    protected function getModelFactory() : ModelFactory
    {
        return new ModelFactory();
    }

    public function testArrayToObject()
    {
        $ruleService = MockFactory::generateRuleService(1);


        $expression['rule_id'] = $ruleService->getId();
        $expression['transformation_template_id'] = $ruleService->getTransformationTemplate()->getId();
        $expression['source_template_id'] = $ruleService->getSourceTemplate()->getId();
        $expression['rules'] = base64_encode(gzcompress(serialize($ruleService->getRules())));
        $expression['version'] = $ruleService->getVersion();
        $expression['data_total'] = $ruleService->getDataTotal();
        $expression['crew_id'] = $ruleService->getCrew()->getId();
        $expression['usergroup_id'] = $ruleService->getUserGroup()->getId();
        $expression['transformation_category'] = $ruleService->getTransformationCategory();
        $expression['source_category'] = $ruleService->getSourceCategory();

        $expression['status'] = $ruleService->getStatus();
        $expression['status_time'] = $ruleService->getStatusTime();
        $expression['create_time'] = $ruleService->getCreateTime();
        $expression['update_time'] = $ruleService->getUpdateTime();

        $ruleService = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Rule\Model\RuleService', $ruleService);
        $this->compareArrayAndObjectRuleService($expression, $ruleService);
    }

    public function testObjectToArray()
    {
        $ruleService = MockFactory::generateRuleService(1);

        foreach ($ruleService->getRules() as $key => $condition) {
            $model = $this->getModelFactory()->getModel($key);
            $model->setCondition($condition);
            $rules[$key] = $model;
        }

        $ruleService->setRules($rules);

        $expression = $this->translator->objectToArray($ruleService);

        $this->compareArrayAndObjectRuleService($expression, $ruleService);
    }
}

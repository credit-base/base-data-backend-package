<?php
namespace BaseData\Task\Adapter\NotifyRecord;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\Model\NullNotifyRecord;
use BaseData\Task\Translator\NotifyRecordDbTranslator;
use BaseData\Task\Adapter\NotifyRecord\Query\NotifyRecordRowCacheQuery;

class NotifyRecordDbAdapter implements INotifyRecordAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new NotifyRecordDbTranslator();
        $this->rowCacheQuery = new NotifyRecordRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : NotifyRecordDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : NotifyRecordRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullNotifyRecord::getInstance();
    }

    public function fetchOne($id) : NotifyRecord
    {
        return $this->fetchOneAction($id);
    }

    public function add(NotifyRecord $notifyRecord) : bool
    {
        return $this->addAction($notifyRecord);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $notifyRecord = new NotifyRecord();

            if (isset($filter['hash'])) {
                $notifyRecord->setHash($filter['hash']);
                $info = $this->getDbTranslator()->objectToArray($notifyRecord, array('hash'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        unset($sort);
        unset($conjection);
        return $condition;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class BjSearchDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'bj_search_data_id',
            new Persistence\BjSearchDataCache(),
            new Persistence\BjSearchDataDb()
        );
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Repository\YsItemsDataRepository;
use BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\YsSearchDataRowCacheQuery;

use BaseData\Template\Repository\WbjTemplateRepository;

class MockYsSearchDataDbAdapter extends YsSearchDataDbAdapter
{
    public function getDbTranslator() : YsSearchDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : YsSearchDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getItemsDataRepository() : YsItemsDataRepository
    {
        return parent::getItemsDataRepository();
    }

    public function getTemplateRepository() : WbjTemplateRepository
    {
        return parent::getTemplateRepository();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Utils;

trait YsSearchDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $ysSearchData
    ) {
        $this->assertEquals($expectedArray['ys_search_data_id'], $ysSearchData->getId());
        $this->assertEquals($expectedArray['info_classify'], $ysSearchData->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $ysSearchData->getInfoCategory());
        $this->assertEquals($expectedArray['crew_id'], $ysSearchData->getCrew()->getId());
        $this->assertEquals($expectedArray['usergroup_id'], $ysSearchData->getSourceUnit()->getId());
        $this->assertEquals($expectedArray['subject_category'], $ysSearchData->getSubjectCategory());
        $this->assertEquals($expectedArray['dimension'], $ysSearchData->getDimension());
        $this->assertEquals($expectedArray['name'], $ysSearchData->getName());
        $this->assertEquals($expectedArray['identify'], $ysSearchData->getIdentify());
        $this->assertEquals($expectedArray['expiration_date'], $ysSearchData->getExpirationDate());
        $this->assertEquals($expectedArray['wbj_template_id'], $ysSearchData->getTemplate()->getId());
        $this->assertEquals($expectedArray['ys_items_data_id'], $ysSearchData->getItemsData()->getId());
        $this->assertEquals($expectedArray['status'], $ysSearchData->getStatus());
        $this->assertEquals($expectedArray['hash'], $ysSearchData->getHash());
        $this->assertEquals($expectedArray['create_time'], $ysSearchData->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $ysSearchData->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $ysSearchData->getStatusTime());
        $this->assertEquals($expectedArray['task_id'], $ysSearchData->getTask()->getId());
    }
}

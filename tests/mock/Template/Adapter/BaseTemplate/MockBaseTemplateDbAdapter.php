<?php
namespace BaseData\Template\Adapter\BaseTemplate;

use Marmot\Interfaces\INull;

use BaseData\Template\Translator\BaseTemplateDbTranslator;
use BaseData\Template\Adapter\BaseTemplate\Query\BaseTemplateRowCacheQuery;

class MockBaseTemplateDbAdapter extends BaseTemplateDbAdapter
{
    public function getDbTranslator() : BaseTemplateDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : BaseTemplateRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

<?php
namespace BaseData\WorkOrderTask\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Utils\ParentTaskUtils;
use BaseData\WorkOrderTask\Utils\ParentTaskMockFactory;

class ParentTaskDbTranslatorTest extends TestCase
{
    use ParentTaskUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ParentTaskDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\WorkOrderTask\Model\NullParentTask', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObjectGbTemplate()
    {
        $value['templateType']  = ParentTask::TEMPLATE_TYPE['GB'];
        $parentTask = ParentTaskMockFactory::generateParentTask(1, 1, $value);

        $this->initArrayToObject($parentTask);
    }

    public function testArrayToObjectBjTemplate()
    {
        $value['templateType']  = ParentTask::TEMPLATE_TYPE['BJ'];
        $parentTask = ParentTaskMockFactory::generateParentTask(1, 1, $value);

        $this->initArrayToObject($parentTask);
    }

    protected function initArrayToObject(ParentTask $parentTask)
    {

        $expression['parent_task_id'] = $parentTask->getId();

        $expression['template_type'] = $parentTask->getTemplateType();
        $expression['template_id'] = $parentTask->getTemplate()->getId();
        $expression['template_name'] = $parentTask->getTemplate()->getName();

        $expression['title'] = $parentTask->getTitle();
        $expression['description'] = $parentTask->getDescription();
        $expression['end_time'] = $parentTask->getEndTime();
        $expression['attachment'] = json_encode($parentTask->getAttachment());

        $assignObjects = $parentTask->getAssignObjects();
        $assignObjectIds = array();
        foreach ($assignObjects as $assignObject) {
            $assignObjectIds[] = $assignObject->getId();
        }
        $expression['assign_objects'] = json_encode($assignObjectIds);

        $expression['finish_count'] = $parentTask->getFinishCount();
        $expression['reason'] = $parentTask->getReason();

        $expression['status'] = $parentTask->getStatus();
        $expression['status_time'] = $parentTask->getStatusTime();
        $expression['create_time'] = $parentTask->getCreateTime();
        $expression['update_time'] = $parentTask->getUpdateTime();

        $parentTask = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\WorkOrderTask\Model\ParentTask', $parentTask);
        $this->compareArrayAndObject($expression, $parentTask);
    }

    public function testObjectToArray()
    {
        $parentTask = ParentTaskMockFactory::generateParentTask(2);

        $expression = $this->translator->objectToArray($parentTask);

        $this->compareArrayAndObject($expression, $parentTask);
    }
}

<?php
namespace BaseData\Common\CommandHandler;

use PHPUnit\Framework\TestCase;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\Command\EnableCommand;

class EnableCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(EnableCommandHandler::class)
                    ->setMethods(['fetchIEnableObject'])
                    ->getMockForAbstractClass();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->stub
        );
    }

    public function testExecute()
    {
        $id =1;

        $command = new class($id) extends EnableCommand
        {

        };

        //预言
        $enableAble = $this->prophesize(IEnableAble::class);
        $enableAble->enable()->shouldBeCalledTimes(1)->willReturn(true);

        //揭示
        $this->stub->expects($this->exactly(1))
            ->method('fetchIEnableObject')
            ->with($id)
            ->willReturn($enableAble->reveal());

        //验证
        $result = $this->stub->execute($command);
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Task\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\Model\Template;

/**
 * 测试任务接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
    }

    /**
     * 存在多条任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_task' => $this->task()
            ]
        );
    }

    public function testViewTask()
    {
        $setTaskList = $this->task();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);

        $response = $client->request(
            'GET',
            'tasks?filter[crew]=1&filter[userGroup]=1&filter[pid]=0&filter[sourceTemplate]=1
            &filter[targetTemplate]=1&filter[targetRule]=1&filter[sourceCategory]='.Template::CATEGORY['WBJ'].
            '&filter[targetCategory]='.Template::CATEGORY['WBJ'].'&filter[status]='.Task::STATUS['FAILURE'],
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];

        foreach ($data as $key => $task) {
            $this->taskCompareData($setTaskList[$key], $task);
        }
    }
}

<?php
namespace BaseData\ResourceCatalogData\Translator\Strategy;

use BaseData\ResourceCatalogData\Model\BaseSearchData;

class EnterpriserDbTranslatorStrategy implements IDbTranslatorStrategy
{
    const ENTERPRISE_PARAMETERS_MAPPING = array(
        'name' => array(
            'identify' => 'ZTMC',
            'defaultValue' => ''
        ),
        'unified_social_credit_code' => array(
            'identify' => 'TYSHXYDM',
            'defaultValue' => ''
        ),
        'establishment_date' => array(
            'identify' => 'CLRQ',
            'defaultValue' => 0
        ),
        'approval_date' => array(
            'identify' => 'HZRQ',
            'defaultValue' => 0
        ),
        'address' => array(
            'identify' => 'JYCS',
            'defaultValue' => ''
        ),
        'registration_capital' => array(
            'identify' => 'ZCZB',
            'defaultValue' => ''
        ),
        'business_term_start' => array(
            'identify' => 'YYQXQ',
            'defaultValue' => 0
        ),
        'business_term_to' => array(
            'identify' => 'YYQXZ',
            'defaultValue' => 0
        ),
        'business_scope' => array(
            'identify' => 'JYFW',
            'defaultValue' => ''
        ),
        'registration_authority' => array(
            'identify' => 'DJJG',
            'defaultValue' => ''
        ),
        'principal' => array(
            'identify' => 'FDDBR',
            'defaultValue' => ''
        ),
        'principal_card_id' => array(
            'identify' => 'FRZJHM',
            'defaultValue' => ''
        ),
        'registration_status' => array(
            'identify' => 'DJZT',
            'defaultValue' => ''
        ),
        'enterprise_type_code' => array(
            'identify' => 'QYLXDM',
            'defaultValue' => ''
        ),
        'enterprise_type' => array(
            'identify' => 'QYLX',
            'defaultValue' => ''
        ),
        'industry_category' => array(
            'identify' => 'HYML',
            'defaultValue' => ''
        ),
        'industry_code' => array(
            'identify' => 'HYDM',
            'defaultValue' => ''
        ),
        'administrative_area' => array(
            'identify' => 'SSQY',
            'defaultValue' => 0
        ),
    );

    public function objectToArray($baseSearchData, array $keys = array())
    {
        if (!$baseSearchData instanceof BaseSearchData) {
            return [];
        }

        $itemsData = $baseSearchData->getItemsData()->getData();

        foreach (self::ENTERPRISE_PARAMETERS_MAPPING as $key => $value) {
            $expression[$key] = isset($itemsData[$value['identify']]) ?
                                $itemsData[$value['identify']] :
                                $value['defaultValue'];
        }

        $expression['data'] = base64_encode(gzcompress(serialize($itemsData)));
        $expression['status'] = $baseSearchData->getStatus();
        $expression['hash'] = $baseSearchData->getHash();
        $expression['task_id'] = $baseSearchData->getTask()->getId();
        $expression['create_time'] = $baseSearchData->getCreateTime();
        $expression['update_time'] = $baseSearchData->getUpdateTime();
        $expression['status_time'] = $baseSearchData->getStatusTime();

        return $expression;
    }
}

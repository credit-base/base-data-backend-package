<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query\Persistence;

class MockBaseTemplateCache extends BaseTemplateCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

<?php
namespace BaseData\Task\Adapter\Task\Query;

use Marmot\Framework\Query\RowQuery;

class TaskRowQuery extends RowQuery
{
    public function __construct()
    {
        parent::__construct(
            'task_id',
            new Persistence\TaskDb()
        );
    }
}

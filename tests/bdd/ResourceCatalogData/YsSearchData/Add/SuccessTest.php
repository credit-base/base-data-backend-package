<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\YsItemsData;
use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要上传资源目录数据时,在资源目录数据子系统下的原始数据管理,上传对应的资源目录数据
 *           根据我归集到的资源目录数据上传,以便于我实现资源目录数据的报送和平台资源目录数据的呈现
 * @Scenario: 正常新增资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_ys_items_data');
        $this->clear('pcore_ys_search_data');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate()
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $templateRepository = new WbjTemplateRepository();
        $wbjTemplate = $templateRepository->fetchOne(1);

        $ysItemsDataArray = $this->ysSearchData()['pcore_ys_items_data'][0];
        $ysSearchDataArray = $this->ysSearchData()['pcore_ys_search_data'][0];

        $ysItemsData = new YsItemsData();
        $ysItemsData->setData(unserialize(gzuncompress(base64_decode($ysItemsDataArray['data'], true))));

        $ysSearchData = new YsSearchData();
        $ysSearchData->getCrew()->setId($ysSearchDataArray['crew_id']);
        $ysSearchData->getSourceUnit()->setId($ysSearchDataArray['usergroup_id']);
        $ysSearchData->setSubjectCategory($ysSearchDataArray['subject_category']);
        $ysSearchData->setExpirationDate($ysSearchDataArray['expiration_date']);
        $ysSearchData->setDimension($ysSearchDataArray['dimension']);
        $ysSearchData->setTemplate($wbjTemplate);
        $ysSearchData->setItemsData($ysItemsData);
        $ysSearchData->setHash($ysSearchDataArray['hash']);
        
        return $ysSearchData->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setYsSearchData = $this->ysSearchData()['pcore_ys_search_data'][0];
        $setYsSearchData['ys_search_data_id'] = 1;
        
        $repository = new YsSearchDataRepository();

        $ysSearchData = $repository->fetchOne($result);

        $translator = new YsSearchDataDbTranslator();
        $ysSearchDataArray = $translator->objectToArray($ysSearchData);

        $this->assertEquals($ysSearchDataArray, $setYsSearchData);
    }
}

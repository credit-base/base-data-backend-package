<?php
namespace BaseData\Enterprise\Model;

use Marmot\Interfaces\INull;

use BaseData\Common\Model\NullOperateTrait;

class NullEnterprise extends Enterprise implements INull
{
    use NullOperateTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function isNameExist() : bool
    {
        return $this->resourceNotExist();
    }

    public function isUnifiedSocialCreditCodeExist() : bool
    {
        return $this->resourceNotExist();
    }
}

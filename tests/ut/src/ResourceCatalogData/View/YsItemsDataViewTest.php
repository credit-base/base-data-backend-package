<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\YsItemsData;

class YsItemsDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $ysItemsData = new YsItemsDataView(new YsItemsData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $ysItemsData);
    }
}

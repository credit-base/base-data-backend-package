<?php
namespace BaseData\ResourceCatalogData\Translator\Strategy;

interface IDbTranslatorStrategy
{
    public function objectToArray($baseSearchData, array $keys = array());
}

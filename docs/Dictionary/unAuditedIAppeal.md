# 异议申诉审核字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [互动类通用字典](./docs/Dictionary/interaction.md "互动类通用字典")
* [异议申诉字典](./docs/Dictionary/appeal.md "异议申诉字典")
* [互动类审核通用字典](./docs/Dictionary/unAuditedInteraction.md "互动类审核通用字典")

### [类型](appeal.md)
### [姓名/企业名称](appeal.md)
### [统一社会信用代码/身份证号](appeal.md)
### [联系方式](appeal.md)
### [上传身份证/营业执照](appeal.md)
### [图片](appeal.md)
### [标题](interaction.md)
### [内容](interaction.md)
### [前台用户](interaction.md)
### [受理委办局](interaction.md)
### [受理状态](interaction.md)
### [回复信息](interaction.md)
### [状态](interaction.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
### [操作类型](common.md)
### [审核状态](common.md)
### [驳回原因](common.md)
### [可以申请的信息分类](common.md)
### [审核委办局](common.md)
### [审核人](common.md)
### [修改人](common.md)

### applyInfoType

**可以申请的最小信息分类** 可以申请的最小信息分类的表述(该字段为预留字段预防出现二级三级分类的情况).

* int
	* APPLY_INFO_TYPE['INTERACTION'] = array(
		'APPEAL' => 1
	)

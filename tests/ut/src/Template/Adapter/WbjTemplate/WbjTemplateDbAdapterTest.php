<?php
namespace BaseData\Template\Adapter\WbjTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\NullWbjTemplate;
use BaseData\Template\Translator\WbjTemplateDbTranslator;
use BaseData\Template\Adapter\WbjTemplate\Query\WbjTemplateRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD)
 * @author chloroplast
 */
class WbjTemplateDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(WbjTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IWbjTemplateAdapter
     */
    public function testImplementsIWbjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 WbjTemplateDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockWbjTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Translator\WbjTemplateDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 WbjTemplateRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockWbjTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\Query\WbjTemplateRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockWbjTemplateDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedWbjTemplate = new WbjTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedWbjTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedWbjTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedWbjTemplate = new WbjTemplate();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedWbjTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedWbjTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedWbjTemplate = new WbjTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWbjTemplate);

        //验证
        $wbjTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWbjTemplate, $wbjTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $wbjTemplateOne = new WbjTemplate(1);
        $wbjTemplateTwo = new WbjTemplate(2);

        $ids = [1, 2];

        $expectedWbjTemplateList = [];
        $expectedWbjTemplateList[$wbjTemplateOne->getId()] = $wbjTemplateOne;
        $expectedWbjTemplateList[$wbjTemplateTwo->getId()] = $wbjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWbjTemplateList);

        //验证
        $wbjTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWbjTemplateList, $wbjTemplateList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 'name',
        );
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';
        
        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterIdentify()
    {
        $filter = array(
            'identify' => 'identify',
        );
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = 'identify = \''.$filter['identify'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterDimension()
    {
        $filter = array(
            'dimension' => WbjTemplate::DIMENSION['SHGK']
        );
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = 'dimension = '.$filter['dimension'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoClassify()
    {
        $filter = array(
            'infoClassify' => 1
        );
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = 'info_classify = '.$filter['infoClassify'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoCategory()
    {
        $filter = array(
            'infoCategory' => 1
        );
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = 'info_category = '.$filter['infoCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterSourceUnit()
    {
        $filter = array(
            'sourceUnit' => 1
        );
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = 'source_unit_id = '.$filter['sourceUnit'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY wbj_template_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockWbjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY wbj_template_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

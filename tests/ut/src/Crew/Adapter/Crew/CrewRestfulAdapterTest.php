<?php
namespace BaseData\Crew\Adapter\Crew;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Model\NullCrew;

class CrewRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(CrewRestfulAdapter::class)
                           ->setMethods([
                                'getTranslator',
                                'getResource',
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends CrewRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsICrewAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Crew\Adapter\Crew\ICrewAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('crews', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'CREW_LIST',
                CrewRestfulAdapter::SCENARIOS['CREW_LIST']
            ],
            [
                'CREW_FETCH_ONE',
                CrewRestfulAdapter::SCENARIOS['CREW_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOneSuccess()
    {
        $adapter = $this->getMockBuilder(CrewRestfulAdapter::class)
                           ->setMethods([
                                'get',
                                'getResource',
                                'isSuccess',
                                'translateToObject'
                            ])->getMock();

        $id = 1;

        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew(1);

        $adapter->expects($this->exactly(1))->method('getResource')->willReturn('crews');
        $adapter->expects($this->exactly(1))->method('get');
        $adapter->expects($this->exactly(1))->method('isSuccess')->willReturn(true);
        $adapter->expects($this->exactly(1))->method('translateToObject')->willReturn($crew);
            
        $result = $adapter->fetchOne($id);
        $this->assertEquals($crew, $result);
    }

    public function testFetchOneFail()
    {
        $adapter = $this->getMockBuilder(CrewRestfulAdapter::class)
                           ->setMethods([
                                'get',
                                'getResource',
                                'isSuccess'
                            ])->getMock();

        $id = 1;

        $adapter->expects($this->exactly(1))->method('getResource')->willReturn('crews');
        $adapter->expects($this->exactly(1))->method('get');
        $adapter->expects($this->exactly(1))->method('isSuccess')->willReturn(false);
            
        $result = $adapter->fetchOne($id);
        $this->assertEquals(NullCrew::getInstance(), $result);
    }
}

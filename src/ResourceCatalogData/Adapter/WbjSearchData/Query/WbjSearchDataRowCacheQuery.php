<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class WbjSearchDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'wbj_search_data_id',
            new Persistence\WbjSearchDataCache(),
            new Persistence\WbjSearchDataDb()
        );
    }
}

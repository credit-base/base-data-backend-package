<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\Crew\Model\Crew;
use BaseData\Template\Model\Template;
use BaseData\UserGroup\Model\UserGroup;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
abstract class SearchData implements IObject, ISearchDataAble, IOperate
{
    const EXPIRATION_DATE_DEFAULT = '4102329600'; //20991231

    use Object, OperateTrait;

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var int $infoClassify 信息分类
     */
    protected $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    protected $infoCategory;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $sourceUnit 来源单位
     */
    protected $sourceUnit;
    /**
     * @var int $subjectCategory 主体类别
     */
    protected $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    protected $dimension;
    /**
     * @var int $expirationDate 有效期限
     */
    protected $expirationDate;
    /**
     * @var string $hash 摘要hash
     */
    protected $hash;
    /**
     * @var Template $template 资源目录
     */
    protected $template;
    /**
     * @var ItemsData $itemsData 资源目录数据
     */
    protected $itemsData;
    /**
     * @var Task $task 任务
     */
    protected $task;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->infoClassify = 0;
        $this->infoCategory = 0;
        $this->crew = new Crew();
        $this->sourceUnit = new UserGroup();
        $this->subjectCategory = 0;
        $this->dimension = 0;
        $this->expirationDate = self::EXPIRATION_DATE_DEFAULT;
        $this->hash = '';
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->task = new Task();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->infoClassify);
        unset($this->infoCategory);
        unset($this->crew);
        unset($this->sourceUnit);
        unset($this->subjectCategory);
        unset($this->dimension);
        unset($this->expirationDate);
        unset($this->hash);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->task);
    }

    /**
     * @marmot-set-id
     */
    public function setId($id) : void
    {
        $this->id = $id;
    }
    /**
     * @marmot-get-id
     */
    public function getId()
    {
        return $this->id;
    }

    public function setInfoClassify(int $infoClassify) : void
    {
        $this->infoClassify = $infoClassify;
    }

    public function getInfoClassify() : int
    {
        return $this->infoClassify;
    }

    public function setInfoCategory(int $infoCategory) : void
    {
        $this->infoCategory = $infoCategory;
    }

    public function getInfoCategory() : int
    {
        return $this->infoCategory;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function setSubjectCategory(int $subjectCategory) : void
    {
        $this->subjectCategory = $subjectCategory;
    }

    public function getSubjectCategory() : int
    {
        return $this->subjectCategory;
    }

    public function setDimension(int $dimension) : void
    {
        $this->dimension = $dimension;
    }

    public function getDimension() : int
    {
        return $this->dimension;
    }

    public function getName() : string
    {
        $itemsData = $this->getItemsData()->getData();

        return isset($itemsData['ZTMC']) ? $itemsData['ZTMC'] : '';
    }

    public function getIdentify() : string
    {
        $identify = '';
        $itemsData = $this->getItemsData()->getData();

        if ($this->getSubjectCategory() == self::SUBJECT_CATEGORY['FRJFFRZZ'] ||
        $this->getSubjectCategory() == self::SUBJECT_CATEGORY['GTGSH']) {
            $identify = isset($itemsData['TYSHXYDM']) ? $itemsData['TYSHXYDM'] : '';
        }
    
        if ($this->getSubjectCategory() == self::SUBJECT_CATEGORY['ZRR']) {
            $identify = isset($itemsData['ZJHM']) ? $itemsData['ZJHM'] : '';
        }
        return $identify;
    }

    public function setExpirationDate(int $expirationDate) : void
    {
        $this->expirationDate = $expirationDate;
    }

    public function getExpirationDate() : int
    {
        return $this->expirationDate;
    }

    public function generateHash(array $itemsData = array())
    {
        if (empty($itemsData)) {
            $itemsData = $this->getItemsData()->getData();
        }

        $hash = md5(base64_encode(gzcompress(serialize($itemsData))));

        $this->setHash($hash);
    }

    public function setHash(string $hash) : void
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function setTask(Task $task) : void
    {
        $this->task = $task;
    }

    public function getTask() : Task
    {
        return $this->task;
    }

    public function setTemplate(Template $template) : void
    {
        $this->template = $template;
        $this->generateDefaultItemsData();
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    private function generateDefaultItemsData()
    {
        $items = $this->getTemplate()->getItems();

        $itemsData = array();

        foreach ($items as $item) {
            if (isset($item['identify']) && isset($item['type'])) {
                $itemsData[$item['identify']] = isset(
                    Template::TEMPLATE_TYPE_DEFAULT[$item['type']]
                ) ? Template::TEMPLATE_TYPE_DEFAULT[$item['type']] : '';
            }
        }
        
        $this->getItemsData()->setData($itemsData);
    }

    abstract public function setStatus(int $status) : void;

    abstract public function getItemsData();

    abstract protected function getRepository();

    protected function isCrewNull() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crew'));
            return false;
        }
        
        return true;
    }

    protected function isSourceUnitNull() : bool
    {
        if ($this->getSourceUnit() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'sourceUnit'));
            return false;
        }
        
        return true;
    }

    protected function isTemplateNull() : bool
    {
        if ($this->getTemplate() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'template'));
            return false;
        }
        
        return true;
    }

    protected function isHashExist() : bool
    {
        $filter['hash'] = $this->getHash();

        list($list, $count) = $this->getRepository()->filter($filter);
        unset($list);

        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'hash'));
            return false;
        }
        
        return true;
    }

    protected function isSubjectCategoryExist() : bool
    {
        if (!in_array($this->getSubjectCategory(), $this->getTemplate()->getSubjectCategory())) {
            Core::setLastError(RESOURCE_NOT_EXIST, array('pointer'=>'subjectCategory'));
            return false;
        }
        
        return true;
    }


    protected function validate() : bool
    {
        if (!$this->isSourceUnitNull() ||
           !$this->isTemplateNull() ||
           !$this->isHashExist() ||
           !$this->isSubjectCategoryExist()) {
            return false;
        }

        return true;
    }

    //赋值: 从资源目录获取信息类别和信息分类,从资源目录数据获取主体名称和主体标识
    protected function assignment()
    {
        $this->setInfoClassify($this->getTemplate()->getInfoClassify());

        $this->setInfoCategory($this->getTemplate()->getInfoCategory());
    }

    protected function addAction() : bool
    {
        return false;
    }

    protected function editAction() : bool
    {
        return false;
    }
}

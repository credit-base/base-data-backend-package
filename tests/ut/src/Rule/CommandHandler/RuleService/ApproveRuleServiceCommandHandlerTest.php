<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Rule\Command\RuleService\ApproveRuleServiceCommand;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Model\TransformationRule;

use BaseData\Crew\Model\Crew;

class ApproveRuleServiceCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(MockApproveRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         ['fetchUnAuditedRuleService','fetchCrew']
                                     )
                                     ->getMock();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $this->command = new class implements ICommand {
        };
        $this->commandHandler->execute($this->command);
    }

    public function testExecuteAction()
    {
        //初始化
        $id = 1;
        $applyCrew = 2;
        $command = new ApproveRuleServiceCommand(
            $applyCrew,
            $id
        );
        $rules = array(
            'transformationRule'=> new TransformationRule()
        );

        $transformationTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $sourceTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);

        $crew = new Crew();

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->getRules()->shouldBeCalledTimes(1)->willReturn($rules);
        $unAuditedRuleService->getTransformationTemplate()->shouldBeCalledTimes(1)->willReturn(
            $transformationTemplate
        );
        $unAuditedRuleService->getSourceTemplate()->shouldBeCalledTimes(1)->willReturn($sourceTemplate);
        $unAuditedRuleService->setApplyCrew(Argument::exact($crew))->shouldBeCalledTimes(1);

        foreach ($rules as $key => $condition) {
            $condition->setTransformationTemplate($transformationTemplate);
            $condition->setSourceTemplate($sourceTemplate);
            $rules[$key] = $condition;
        }
        $unAuditedRuleService->setRules(Argument::exact($rules))->shouldBeCalledTimes(1);
                             
        $unAuditedRuleService->approve()
                             ->shouldBeCalledTimes(1)
                             ->willReturn(true);

        $this->commandHandler->expects($this->once())
                       ->method('fetchUnAuditedRuleService')
                       ->with($command->id)
                       ->willReturn($unAuditedRuleService->reveal());

        $this->commandHandler->expects($this->once())
                       ->method('fetchCrew')
                       ->with($command->applyCrew)
                       ->willReturn($crew);

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Task\Adapter\NotifyRecord\Query\Persistence;

class MockNotifyRecordCache extends NotifyRecordCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

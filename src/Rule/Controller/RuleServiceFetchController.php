<?php
namespace BaseData\Rule\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Rule\View\RuleServiceView;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter;

class RuleServiceFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new RuleServiceRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IRuleServiceAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new RuleServiceView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'rules';
    }
}

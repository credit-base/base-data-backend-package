<?php
namespace BaseData\ResourceCatalogData\Service;

use Marmot\Core;
use Marmot\Interfaces\INull;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\NullWbjTemplate;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\FormatValidationRule;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Repository\CrewRepository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;

use BaseData\ResourceCatalogData\Model\SearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class SyncTaskService
{
    const SOURCE_TEMPLATE_COUNT = 1;
    const FILE_CONTENT_MIN_COUNT = 1;

    use SyncTaskTrait;

    protected $crewRepository;

    protected $task;

    protected $ruleService;

    protected $formatValidationRule;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
        $this->crewRepository = new CrewRepository();
        $this->task = new Task();
        $this->ruleService = new RuleService();
        $this->formatValidationRule = new FormatValidationRule();
    }

    public function __destruct()
    {
        unset($this->fileName);
        unset($this->crewRepository);
        unset($this->task);
        unset($this->ruleService);
        unset($this->formatValidationRule);
    }

    protected function getFileName() : string
    {
        return $this->fileName;
    }

    protected function getCrewRepository() : CrewRepository
    {
        return $this->crewRepository;
    }

    protected function getTask() : Task
    {
        return $this->task;
    }

    protected function getRuleService() : RuleService
    {
        return $this->ruleService;
    }

    protected function getFormatValidationRule() : FormatValidationRule
    {
        return $this->formatValidationRule;
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    /**
     * 通过excel,获取到文件内容,并入委办局数据库,将文件内容入到委办局数据库需要进行格式验证,此处可以将格式验证也当成一种规则验证,所以在处理这部分内容时,将文件内容设置为委办局资源目录数据,通过格式验证规则,转换为正确数据的委办局资源目录数据并入库
        * 获取文件名,通过文件获取文件内容
        * 新增子任务
        * 将文件内容通过规则进行转换,入目标库
            * 此处的规则需要自己创建,通过资源目录模板信息对数据进行格式验证
        * 修改任务信息,成功数,失败数,状态
        * 根据来源资源目录获取其下的转换规则
     * @return array 返回规则列表和任务
     */
    public function execute() : array
    {
        //获取文件名
        $fileName = $this->getFileName();

        //验证文件格式是否正确
        if (!$this->validateFileFormat($fileName)) {
            return array(array(), new NullTask());
        }

        //初始化数据
        $data = $this->initializationData($fileName);

        //验证数据是否正确
        if (!$this->validateDataCorrect($data)) {
            return array(array(), new NullTask());
        }

        //新增父任务
        $task = $this->addTask($data);

        if ($task instanceof NullTask) {
            return array(array(), new NullTask());
        }

        //创建规则
        $ruleService = $this->creditRuleService($data['sourceTemplate']);

        //将获取到的文件内容转成数组对象
        $sourceSearchDataList = $this->fileContentToSearchDataObjectArray($data);

        //循环数组,通过规则对数组进行转换,并入库
        $this->searchDataTransformationAndAdd($sourceSearchDataList, $ruleService, $task);

        //修改任务信息
        $this->updateTask($task, $ruleService);

        //根据来源资源目录获取其下的转换规则
        $ruleServiceList = $this->fetchRuleServiceList($ruleService->getSourceTemplate());

        return array($ruleServiceList, $task);
    }

    /**
     * 验证文件格式是否正确
        * 验证文件是否存在
        * 验证文件名格式正确
     * @param string $fileName 文件名称
     * @return bool
     */
    protected function validateFileFormat(string $fileName) : bool
    {
        return $this->isFileExist($fileName) && $this->isFileNameFormatCorrect($fileName);
    }
    
    /**
     * 验证文件存在
     * @param string $fileName 文件名称
     * @return bool
     */
    protected function isFileExist(string $fileName) : bool
    {
        if (!file_exists(Core::$container->get('attachment.upload.path').$fileName)) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer'=>'file'));
            return false;
        }

        return true;
    }
    /**
     * 验证文件名格式正确
        * 文件名称由五部分组成: 来源资源目录标识,来源资源目录id,来源资源目录类型,员工id,文件内容hash
        * 来源资源目录标识,文件内容hash类型为 sting,来源资源目录id,来源资源目录类型,员工id类型为 int
        * 来源资源目录类型在资源目录类型范围内
     * @param string $fileName 文件名称
     * @return bool
     */
    protected function isFileNameFormatCorrect(string $fileName) : bool
    {
        $fileNameInfo = explode('_', $fileName);

        return $this->validateFileNameFormatCorrect($fileNameInfo)
            && $this->validateFileNameTypeCorrect($fileNameInfo)
            && $this->validateFileNameCategoryExist($fileNameInfo[2]);
    }

    /**
     * 验证文件名是否由五部分组成
     * @param array $fileNameInfo 分割文件名生成的数组
     * @return bool
     */
    private function validateFileNameFormatCorrect($fileNameInfo) : bool
    {
        if (!isset($fileNameInfo[0]) ||
            !isset($fileNameInfo[1]) ||
            !isset($fileNameInfo[2]) ||
            !isset($fileNameInfo[3]) ||
            !isset($fileNameInfo[4])
        ) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'fileName'));
            return false;
        }

        return true;
    }

    /**
     * 验证文件名称五部分的数据格式是否正确
     * @param array $fileNameInfo 分割文件名生成的数组
     * @return bool
     */
    private function validateFileNameTypeCorrect($fileNameInfo) : bool
    {
        if (!is_string($fileNameInfo[0]) ||
            !is_numeric($fileNameInfo[1]) ||
            !is_numeric($fileNameInfo[2]) ||
            !is_numeric($fileNameInfo[3]) ||
            !is_string($fileNameInfo[4])
        ) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'fileName'));
            return false;
        }

        return true;
    }

    /**
     * 验证文件名称中的来源资源目录类型是否在范围内
     * @param array $sourceCategory 来源资源目录类型
     * @return bool
     */
    private function validateFileNameCategoryExist(int $sourceCategory) : bool
    {
        if (!in_array($sourceCategory, Template::CATEGORY)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'fileName'));
            return false;
        }

        return true;
    }

    /**
     * 初始化数据
     * @param string $fileName 文件名称
     * @return array
        * crew 员工信息
        * sourceTemplate 来源资源目录信息
        * fileContent 文件类型
        * sourceCategory 来源资源目录类型
      */
    protected function initializationData(string $fileName) : array
    {
        $fileNameInfo = explode('_', $fileName);
        $identify = $fileNameInfo[0];
        $sourceTemplateId = $fileNameInfo[1];
        $sourceCategory = $fileNameInfo[2];
        $crewId = $fileNameInfo[3];

        //获取员工信息
        $crew = $this->fetchCrew($crewId);
        //获取来源资源目录
        $sourceTemplate = $this->fetchSourceTemplate($sourceCategory, $identify, $sourceTemplateId);
        //获取文件内容
        $fileContent = $this->fetchFileContent($fileName);

        return array(
            'crew' => $crew,
            'sourceTemplate' => $sourceTemplate,
            'fileContent' => $fileContent,
            'sourceCategory' => $sourceCategory
        );
    }
    /**
     * 获取来源资源目录
        * 如果文件中的来源资源目录标识和获取到的资源目录标识不一致则返回空的Template
     * @param string $identify 来源资源目录标识
     * @param int sourceCategory 来源资源目录类型
     * @param int sourceTemplateId 来源资源目录id
     * @return Template 来源资源目录信息
      */
    protected function fetchSourceTemplate(int $sourceCategory, string $identify, int $sourceTemplateId) : Template
    {
        $repository = $this->getRepositoryFactory()->getRepository($sourceCategory);

        $sourceTemplate = $repository->fetchOne($sourceTemplateId);

        return ($sourceTemplate->getIdentify()==$identify) ? $sourceTemplate : NullWbjTemplate::getInstance();
    }

    /**
     * 返回一个reader，具体返回什么reader，是根据文件名来的
     * @param string $fileName 文件名
     * @return BaseReader
    */
    protected function getReader(string $fileName) : \PhpOffice\PhpSpreadsheet\Reader\BaseReader
    {
        try {
            $pool = new \Cache\Adapter\Doctrine\DoctrineCachePool(\Marmot\Core::$cacheDriver);
            $simpleCache = new \Cache\Bridge\SimpleCache\SimpleCacheBridge($pool);
            \PhpOffice\PhpSpreadsheet\Settings::setCache($simpleCache);

            \PhpOffice\PhpSpreadsheet\Settings::setLibXmlLoaderOptions(
                LIBXML_DTDLOAD | LIBXML_DTDATTR | LIBXML_PARSEHUGE | LIBXML_COMPACT
            );
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($fileName);

            return $reader;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * 获取文件内容
     * @param string $fileName 文件名
     * @return array
    */
    protected function fetchFileContent(string $fileName) : array
    {
        $file = Core::$container->get('attachment.upload.path').$fileName;

        $data = array();
        try {
            $reader = $this->getReader($file);
            //仅读取数据
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($file);
            $data = $spreadsheet->getActiveSheet()->toArray();
        } catch (\Exception $e) {
            //处理异常数据
        }

        return $data;
    }

    //验证数据是否正确
    protected function validateDataCorrect(array $data) : bool
    {
        return isset($data['crew'])
            && isset($data['sourceTemplate'])
            && isset($data['fileContent'])
            && isset($data['sourceCategory'])
            && $this->isCrewExist($data['crew'])
            && $this->isSourceTemplateExist($data['sourceTemplate'])
            && $this->isFileContentEmpty($data['fileContent']);
    }
    //验证员工是否存在
    protected function isCrewExist(Crew $crew) : bool
    {
        if ($crew instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crewId'));
            return false;
        }

        return true;
    }
    //验证来源资源目录是否存在
    protected function isSourceTemplateExist(Template $sourceTemplate) : bool
    {
        if ($sourceTemplate instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'sourceTemplateId'));
            return false;
        }

        return true;
    }
    //验证文件内容是否为空
    protected function isFileContentEmpty(array $fileContent) : bool
    {
        if (count($fileContent) <= self::FILE_CONTENT_MIN_COUNT) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'file'));
            return false;
        }

        return true;
    }

    /**
     * 新增父任务
     * @param array $data 初始化后的数据
     * @return Task 返回任务对象,失败返回空的任务对象是为了避免报错,能使流程继续执行
     */
    protected function addTask(array $data) : Task
    {
        $crew = $data['crew'];
        $sourceTemplate = $data['sourceTemplate'];
        $fileName = $this->getFileName();

        $task = $this->getTask();
        $task->setCrew($crew);
        $task->setUserGroup($crew->getUserGroup());
        $task->setScheduleTask(posix_getpid());
        $task->setTotal(count($data['fileContent'])-1);
        $task->setSourceCategory($data['sourceCategory']);
        $task->setSourceTemplate($sourceTemplate->getId());
        $task->setTargetCategory($data['sourceCategory']);
        $task->setTargetTemplate($sourceTemplate->getId());
        $task->setTargetRule($sourceTemplate->getId());
        $task->setFileName($fileName);

        return $task->add() ? $task : NullTask::getInstance();
    }

    /**
     * 创建规则
        * 格式验证规则
        * excel文件上传入委办局库,需要通过委办局资源目录的模板信息进行格式验证,为了保持代码统一和后期方便维护,所以将此处的格式验证与清晰比对规则验证保持一致,创建委办局资源目录数据-委办局资源目录数据的规则,所以需要将文件内容转为资源目录数据
     * @param Template $sourceTemplate 来源资源目录
     * @return RuleService 返回规则对象
     */
    protected function creditRuleService(Template $sourceTemplate) : RuleService
    {
        $items = $this->fetchTemplateItems($sourceTemplate);
        $formatValidationRule = $this->getFormatValidationRule();
        $formatValidationRule->setCondition($items);
        $formatValidationRule->setTransformationTemplate($sourceTemplate);
        $formatValidationRule->setSourceTemplate($sourceTemplate);

        $rules = array('formatValidationRule' => $formatValidationRule);
        $ruleService = $this->getRuleService();
        $ruleService->setRules($rules);
        $ruleService->setTransformationTemplate($sourceTemplate);
        $ruleService->setSourceTemplate($sourceTemplate);
        $ruleService->setTransformationCategory($sourceTemplate->getCategory());
        $ruleService->setSourceCategory($sourceTemplate->getCategory());

        return $ruleService;
    }

    /**
     * 将获取到的文件内容转成数组对象,将数组转成来源委办局资源目录数据并返回数据列表
     * @param array $data 文件内容
     * @return array 返回数组对象
     */
    protected function fileContentToSearchDataObjectArray(array $data) : array
    {
        $fileContent = $data['fileContent'];
        $sourceTemplate = $data['sourceTemplate'];

        $items = $sourceTemplate->getItems();
        $sourceSearchDataList = array();

        foreach ($fileContent as $key => $fileData) {
            if ($key != 0) {
                //将文件内容转成资源目录数据格式
                $itemsDataData = $this->fileDataToItemsData($fileData, $items);
                //生成主体类别
                $subjectCategory = $this->generateSubjectCategory($itemsDataData);
                $expirationDate = isset($itemsDataData['GQSJ']) ?
                                    strtotime($itemsDataData['GQSJ']) :
                                    SearchData::EXPIRATION_DATE_DEFAULT;
                //生成来源资源目录数据
                $sourceSearchData = $this->generateSourceSearchData($data);
                $sourceSearchData->setId($key);
                $sourceSearchData->setExpirationDate($expirationDate);
                $sourceSearchData->setSubjectCategory($subjectCategory);
                $sourceSearchData->getItemsData()->setData($itemsDataData);
                $sourceSearchData->generateHash();
                $sourceSearchDataList[] = $sourceSearchData;
            }
        }

        return $sourceSearchDataList;
    }

    //生成来源资源目录数据
    protected function generateSourceSearchData(array $data) : ISearchDataAble
    {
        $sourceTemplate = $data['sourceTemplate'];
        $crew = $data['crew'];

        $sourceSearchData = $this->getSourceSearchData($data['sourceCategory']);
        $sourceSearchData->setCrew($crew);
        $sourceSearchData->setSourceUnit($crew->getUserGroup());
        $sourceSearchData->setTemplate($sourceTemplate);
        $sourceSearchData->setInfoClassify($sourceTemplate->getInfoClassify());
        $sourceSearchData->setInfoCategory($sourceTemplate->getInfoCategory());
        $sourceSearchData->setDimension($sourceTemplate->getDimension());

        return $sourceSearchData;
    }
    //获取来源资源目录数据模型
    protected function getSourceSearchData(int $sourceCategory) : ISearchDataAble
    {
        return $this->getSearchDataModelFactory()->getModel($sourceCategory);
    }
    //将文件内容转成资源目录数据格式
    protected function fileDataToItemsData(array $fileData, array $items) : array
    {
        $itemsData = array();

        foreach ($items as $key => $item) {
            $defaultValue = isset(Template::TEMPLATE_TYPE_DEFAULT[$item['type']]) ?
                            Template::TEMPLATE_TYPE_DEFAULT[$item['type']] :
                            '';

            $itemsData[$item['identify']] = isset($fileData[$key]) ? trim($fileData[$key]) : $defaultValue;
        }

        return $itemsData;
    }

    //生成主体类别
    protected function generateSubjectCategory(array $itemsData) : int
    {
        $subjectCategory = isset($itemsData['ZTLB']) ? $itemsData['ZTLB'] : 0;

        if (!is_numeric($subjectCategory)) {
            $subjectCategory = isset(ISearchDataAble::SUBJECT_CATEGORY_CN[$subjectCategory]) ?
                            ISearchDataAble::SUBJECT_CATEGORY_CN[$subjectCategory] :
                            0;
        }

        return $subjectCategory;
    }
}

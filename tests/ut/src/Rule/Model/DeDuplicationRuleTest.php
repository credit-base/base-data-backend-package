<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;

/**
 * BaseData\Rule\Model\DeDuplicationRule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class DeDuplicationRuleTest extends TestCase
{
    private $rule;

    private $transformationTemplate;

    private $sourceSearchData;

    private $transformationSearchData;

    private $initializationData;

    private $data;

    public function setUp()
    {
        $this->transformationTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(2);

        $this->sourceSearchData =
            \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(2);
        $this->transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(2);

        $condition = array('result' => 1, 'items' => array("ZTMC"));

        $this->initializationData = array(
            'condition' => $condition
        );

        $this->data = array(
            'hash' => md5('hash'),
            'deDuplicationItems' => array('ZTMC'=>array('ZTMC'))
        );

        $this->rule = new MockDeDuplicationRule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\Rule", $this->rule);
    }

    public function testImplementsIRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\IRule", $this->rule);
    }

    private function initialValidate($condition)
    {
        $this->rule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods(['getTransformationTemplate', 'getCondition'])
            ->getMock();
        
        $transformationTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $items = array(
            array(
                "name" => '主体名称',    //信息项名称
                "identify" => 'ZTMC',    //数据标识
                "type" => 1,    //数据类型
                "length" => '200',    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则
                "remarks" => '信用主体名称',    //备注
            ),
        );
        $transformationTemplate->setItems($items);

        $this->rule->expects($this->exactly(1))->method('getTransformationTemplate')->willReturn(
            $transformationTemplate
        );
        
        $this->rule->expects($this->exactly(1))->method('getCondition')->willReturn(
            $condition
        );
    }

    public function testValidateFalse()
    {
        $condition = array('result' => 1, 'items' => array("TYSHXYDM"));
        $this->initialValidate($condition);

        $result = $this->rule->validate();

        $this->assertEquals('deDuplicationRulesItemsNotExit', Core::getLastError()->getSource()['pointer']);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testValidateTrue()
    {
        $condition = array('result' => 1, 'items' => array("ZTMC"));
        $this->initialValidate($condition);
        
        $result = $this->rule->validate();

        $this->assertTrue($result);
    }

    public function testValidateTransformation()
    {
        $result = $this->rule->validateTransformation($this->transformationSearchData);

        $this->assertTrue($result);
    }

    public function testTransformationInitialization()
    {
        $rule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'getCondition'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('getCondition')
             ->willReturn($this->initializationData['condition']);

        $result = $rule->transformationInitialization($this->sourceSearchData, $this->transformationSearchData);

        $this->assertEquals($result, $this->initializationData);
    }

    public function testValidateInitializationDataFormat()
    {
        $result = $this->rule->validateInitializationDataFormat($this->initializationData);

        $this->assertTrue($result);
    }

    public function testTransformationExecuteFailure()
    {
        $rule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'validateInitializationDataFormat'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(false);

        $result = $rule->transformationExecute($this->initializationData, $this->transformationSearchData);

        $this->assertEquals($result, array());
    }

    public function testTransformationExecute()
    {
        $rule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'validateInitializationDataFormat',
                'generateDeDuplicationItemsHash',
                'fetchDeDuplicationItems'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(true);

        $rule->expects($this->exactly(1))
             ->method('generateDeDuplicationItemsHash')
             ->with(
                 $this->initializationData['condition']['items'],
                 $this->transformationSearchData
             )->willReturn($this->data['hash']);

        $rule->expects($this->exactly(1))
             ->method('fetchDeDuplicationItems')
             ->with(
                 $this->initializationData['condition']['items'],
                 $this->transformationSearchData
             )->willReturn($this->data['deDuplicationItems']);

        $result = $rule->transformationExecute($this->initializationData, $this->transformationSearchData);

        $this->assertEquals($result, $this->data);
    }
    
    public function testFetchDeDuplicationItems()
    {
        $rule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'fetchTemplateItems',
            ])->getMock();

        $deDuplicationItemsIdentify = $this->initializationData['condition']['items'];
        $transformationTemplateItems = $this->data['deDuplicationItems'];
        
        $rule->expects($this->exactly(1))
             ->method('fetchTemplateItems')
             ->with(
                 $this->transformationSearchData->getTemplate()
             )->willReturn($transformationTemplateItems);
        
        $result = $rule->fetchDeDuplicationItems($deDuplicationItemsIdentify, $this->transformationSearchData);

        $this->assertEquals($result, $transformationTemplateItems);
    }

    public function testFetchDeDuplicationItemsEmpty()
    {
        $rule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'fetchTemplateItems',
            ])->getMock();

        $deDuplicationItemsIdentify = array();
        $transformationTemplateItems = $this->data['deDuplicationItems'];
        
        $rule->expects($this->exactly(1))
             ->method('fetchTemplateItems')
             ->with(
                 $this->transformationSearchData->getTemplate()
             )->willReturn($transformationTemplateItems);
        
        $result = $rule->fetchDeDuplicationItems($deDuplicationItemsIdentify, $this->transformationSearchData);

        $this->assertEquals($result, $transformationTemplateItems);
    }

    public function testValidateDataFormat()
    {
        $result = $this->rule->validateDataFormat($this->data);

        $this->assertTrue($result);
    }

    public function testTransformationResultFailure()
    {
        $deDuplicationRule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'validateDataFormat',
                'failureDataException',
                'isSearchDataExist',
                'failureData'
            ])->getMock();

        $failureData = new FailureData();
        $hash = $this->data['hash'];
        $deDuplicationItems = isset($this->data['deDuplicationItems']) ?
                                $this->data['deDuplicationItems'] :
                                array();
        $failureData->setTemplate($this->transformationTemplate);
        
        $deDuplicationRule->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(false);
             
        $deDuplicationRule->expects($this->exactly(1))
             ->method('failureDataException')
             ->with(
                 $this->transformationSearchData,
                 ErrorData::STATUS['PROGRAM_EXCEPTION']
             )
             ->willReturn($failureData);

        $deDuplicationRule->expects($this->exactly(1))
             ->method('isSearchDataExist')
             ->with($hash, $failureData)
             ->willReturn(false);

        $deDuplicationRule->expects($this->exactly(1))
             ->method('failureData')
             ->with(
                 $failureData,
                 ErrorData::ERROR_TYPE['DUPLICATION_DATA'],
                 $deDuplicationItems
             )
             ->willReturn($failureData);

         $result = $deDuplicationRule->transformationResult($this->data, $this->transformationSearchData);

        $this->assertEquals($result, $failureData);
    }
    
    public function testTransformationResult()
    {
        $deDuplicationRule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'validateDataFormat',
                'isSearchDataExist'
            ])->getMock();
            
        $deDuplicationRule->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(true);
        
        $hash = $this->data['hash'];
        $deDuplicationRule->expects($this->exactly(1))
             ->method('isSearchDataExist')
             ->with($hash, $this->transformationSearchData)
             ->willReturn(true);
             
        $result = $deDuplicationRule->transformationResult($this->data, $this->transformationSearchData);
        $this->transformationSearchData->setHash($hash);
          
        $this->assertEquals($result, $this->transformationSearchData);
    }
    
    public function testTransformationResultFailureData()
    {
        $deDuplicationRule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'validateDataFormat',
                'isSearchDataExist',
                'failureData'
            ])->getMock();

        $failureData = new FailureData();

        
        $deDuplicationRule->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(true);
        
        $hash = $this->data['hash'];
        $deDuplicationRule->expects($this->exactly(1))
             ->method('isSearchDataExist')
             ->with($hash, $this->transformationSearchData)
             ->willReturn(false);

        $deDuplicationRule->expects($this->exactly(1))
             ->method('failureData')
             ->with(
                 $this->transformationSearchData,
                 ErrorData::ERROR_TYPE['DUPLICATION_DATA'],
                 $this->data['deDuplicationItems']
             )
             ->willReturn($failureData);
        
        $failureData->setHash($hash);

        $result = $deDuplicationRule->transformationResult($this->data, $this->transformationSearchData);

        $this->assertEquals($result, $failureData);
    }

    public function testGenerateDeDuplicationItemsHashEmpty()
    {
        $deDuplicationItems = array();
        $deDuplicationItemsData = $this->transformationSearchData->getItemsData()->getData();
        $hash = md5(base64_encode(gzcompress(serialize($deDuplicationItemsData))));

        $result = $this->rule->generateDeDuplicationItemsHash($deDuplicationItems, $this->transformationSearchData);

        $this->assertEquals($result, $hash);
    }

    public function testGenerateDeDuplicationItemsHash()
    {
        $deDuplicationItems = array('ZTMC');
        $deDuplicationItemsData = array('ZTMC'=>'企业名称');
        $this->transformationSearchData->getItemsData()->setData($deDuplicationItemsData);

        $hash = md5(base64_encode(gzcompress(serialize($deDuplicationItemsData))));

        $result = $this->rule->generateDeDuplicationItemsHash($deDuplicationItems, $this->transformationSearchData);

        $this->assertEquals($result, $hash);
    }

    public function testIsSearchDataExist()
    {
        $deDuplicationRule = $this->getMockBuilder(MockDeDuplicationRule::class)
            ->setMethods([
                'getSearchDataRepository',
                'filterFormatChange'
            ])->getMock();

        $hash = md5('hash');
        $list = array('list');
        $count = 1;
        $filter['hash'] = $hash;
        $filter['template'] = $this->transformationTemplate->getId();
        
        $deDuplicationRule->expects($this->exactly(1))
             ->method('filterFormatChange')
             ->with($hash, $this->transformationSearchData)
             ->willReturn($filter);
        
        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $deDuplicationRule->expects($this->exactly(1))
             ->method('getSearchDataRepository')
             ->with($this->transformationSearchData)
             ->willReturn($repository->reveal());

        $result = $deDuplicationRule->isSearchDataExist($hash, $this->transformationSearchData);

        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        $hash = md5('hash');
        $filter['hash'] = $hash;
        $filter['template'] = $this->transformationSearchData->getTemplate()->getId();

        $result = $this->rule->filterFormatChange($hash, $this->transformationSearchData);

        $this->assertEquals($result, $filter);
    }

    public function testFilterFormatChangeBase()
    {
        $transformationSearchData = $this->transformationSearchData;
        $baseTemplate = \BaseData\Template\Utils\BaseTemplateMockFactory::generateBaseTemplate(1);
        $transformationSearchData->setTemplate($baseTemplate);

        $hash = md5('hash');
        $filter['hash'] = $hash;
        $filter['template'] = $this->transformationSearchData->getTemplate()->getId();
        $filter['baseSubjectCategory'] = $transformationSearchData->getSubjectCategory();

        $result = $this->rule->filterFormatChange($hash, $transformationSearchData);

        $this->assertEquals($result, $filter);
    }
}

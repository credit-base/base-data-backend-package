<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class WbjSearchDataDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockWbjSearchDataDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(WbjSearchDataDb::TABLE, $this->database->getTable());
    }
}

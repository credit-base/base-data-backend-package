<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullBjItemsDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullBjItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsBjItemsData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\BjItemsData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\WorkOrderTaskUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试工单任务接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WorkOrderTaskUtils, TemplateSetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_work_order_task');
    }

    /**
     * 存在一条工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_work_order_task' => $this->workOrderTask(),
                'pcore_parent_task' => $this->parentTask(),
                'pcore_bj_template' => $this->bjTemplate()
            ]
        );
    }

    public function testViewWorkOrderTask()
    {
        $setWorkOrderTask = $this->workOrderTask()[0];
        $setTemplate = $this->bjTemplate()[0];
        $setParentTask = $this->parentTask()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'workOrderTasks/1?include=template,parentTask',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->workOrderTaskCompareData($setWorkOrderTask, $contents['data']);

        $bjTemplate = $contents['included'][0];
        $this->assertEquals($setTemplate['bj_template_id'], $bjTemplate['id']);
        $this->assertEquals('bjTemplates', $bjTemplate['type']);
        $attributes = $bjTemplate['attributes'];
        $this->assertEquals($setTemplate['name'], $attributes['name']);

        $parentTask = $contents['included'][1];
        $this->assertEquals($setParentTask['parent_task_id'], $parentTask['id']);
        $this->assertEquals('parentTasks', $parentTask['type']);
        $attributes = $parentTask['attributes'];
        $this->assertEquals($setParentTask['template_type'], $attributes['templateType']);
        $this->assertEquals($setParentTask['title'], $attributes['title']);
        $this->assertEquals($setParentTask['end_time'], $attributes['endTime']);
    }
}

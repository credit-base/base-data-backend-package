<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\Persistence;

use Marmot\Framework\Classes\Db;

class WorkOrderTaskDb extends Db
{
    const TABLE = 'work_order_task';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

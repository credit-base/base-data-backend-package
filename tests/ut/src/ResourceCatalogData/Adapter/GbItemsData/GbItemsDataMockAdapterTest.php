<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\GbItemsData;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class GbItemsDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new GbItemsDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new GbItemsData()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\GbItemsData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\GbItemsData',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\GbItemsData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

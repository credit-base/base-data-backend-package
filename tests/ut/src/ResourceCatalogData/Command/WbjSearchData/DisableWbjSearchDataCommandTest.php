<?php
namespace BaseData\ResourceCatalogData\Command\WbjSearchData;

use PHPUnit\Framework\TestCase;

class DisableWbjSearchDataCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new DisableWbjSearchDataCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsDisableCommand()
    {
        $this->assertInstanceof('BaseData\Common\Command\DisableCommand', $this->stub);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class BjItemsDataCache extends Cache
{
    const KEY = 'bj_items_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use BaseData\Template\Command\QzjTemplate\AddQzjTemplateCommand;
use BaseData\Template\Command\QzjTemplate\EditQzjTemplateCommand;

class QzjTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new QzjTemplateCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddQzjTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddQzjTemplateCommand(
                $this->faker->word(),
                $this->faker->regexify('[A-Z_]{1,100}'),
                $this->faker->randomElements(array(1,2,3,4), 3),
                $this->faker->randomElement(array(1,2,3,4)),
                $this->faker->randomDigitNotNull(),
                $this->faker->randomDigitNotNull(),
                $this->faker->randomDigitNotNull(),
                $this->faker->sentence(),
                $this->faker->randomElements(array(1,2,3,4), 2),
                $this->faker->randomDigitNotNull(),
                $this->faker->randomDigitNotNull()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\Template\CommandHandler\QzjTemplate\AddQzjTemplateCommandHandler',
            $commandHandler
        );
    }

    public function testEditQzjTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditQzjTemplateCommand(
                $this->faker->word,
                $this->faker->regexify('[A-Z_]{1,100}'),
                $this->faker->randomElements(array(1,2,3,4), 2),
                $this->faker->randomElement(array(1,2,3,4)),
                $this->faker->randomDigitNot(0),
                $this->faker->randomDigit(),
                $this->faker->randomDigitNot(0),
                $this->faker->sentence(),
                $this->faker->randomElements(array(1,2,3,4), 2),
                $this->faker->randomDigitNot(0),
                $this->faker->randomDigitNot(0),
                $this->faker->randomDigitNot(0)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\Template\CommandHandler\QzjTemplate\EditQzjTemplateCommandHandler',
            $commandHandler
        );
    }
}

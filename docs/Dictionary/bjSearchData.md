# 本级资源目录数据搜索字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [资源目录字典](./docs/Dictionary/searchData.md "资源目录数据搜索字典")

### template

**资源目录** 

* BjTemplate
* 必填项

### itemsData

**资源目录数据** 

* BjItemsData
* 必填项

### description

**待确认规则描述** 

* string
* 选填项

### status

**状态** 状态的表述.

* int

	* STATUS_CONFIRM  待确认
	* STATUS_ENABLED 2 已确认
	* STATUS_DISABLED -2 屏蔽
	* STATUS_DELETED -4 封存
    
### frontEndProcessorStatus

**前置机状态** 前置机状态的表述.

* int

	* STATUS_NOT_IMPORT 0 未导入前置机
	* STATUS_IMPORT 2 已经导入前置机

### [信息分类](searchData.md)
### [信息类别](searchData.md)
### [发布人](searchData.md)
### [来源单位](searchData.md)
### [主体类别](searchData.md)
### [公开范围](searchData.md)
### [主体名称](searchData.md)
### [主体标识](searchData.md)
### [有效期限](searchData.md)
### [父任务](searchData.md)
### [子任务](searchData.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

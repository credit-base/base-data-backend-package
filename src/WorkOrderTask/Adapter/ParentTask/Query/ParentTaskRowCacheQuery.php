<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ParentTaskRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'parent_task_id',
            new Persistence\ParentTaskCache(),
            new Persistence\ParentTaskDb()
        );
    }
}

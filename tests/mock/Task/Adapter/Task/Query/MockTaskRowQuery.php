<?php
namespace BaseData\Task\Adapter\Task\Query;

use Marmot\Framework\Interfaces\DbLayer;

class MockTaskRowQuery extends TaskRowQuery
{
    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

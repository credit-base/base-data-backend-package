<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\WbjSearchDataMockAdapter;

class WbjSearchDataRepository extends Repository implements IWbjSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WbjSearchDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWbjSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWbjSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWbjSearchDataAdapter
    {
        return new WbjSearchDataMockAdapter();
    }

    public function fetchOne($id) : WbjSearchData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(WbjSearchData $wbjSearchData) : bool
    {
        return $this->getAdapter()->add($wbjSearchData);
    }

    public function edit(WbjSearchData $wbjSearchData, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($wbjSearchData, $keys);
    }
}

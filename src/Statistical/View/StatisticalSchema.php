<?php
namespace BaseData\Statistical\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class StatisticalSchema extends SchemaProvider
{
    protected $resourceType = 'statisticals';

    public function getId($statistical) : int
    {
        return $statistical->getId();
    }

    public function getAttributes($statistical) : array
    {
        return [
            'result'  => $statistical->getResult()
        ];
    }
}

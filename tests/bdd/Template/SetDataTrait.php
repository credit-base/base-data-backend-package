<?php
namespace BaseData\Template;

use Marmot\Core;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\QzjTemplate;

trait SetDataTrait
{
    private function items() : array
    {
        return
        [
            [
                "name" => '统一社会信用代码',    //信息项名称
                "identify" => 'TYSHXYDM',    //数据标识
                "type" => 1,    //数据类型
                "length" => '50',    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                "remarks" => '信用主体代码',    //备注
            ],
            [
                "name" => '主体名称',    //信息项名称
                "identify" => 'ZTMC',    //数据标识
                "type" => 1,    //数据类型
                "length" => '200',    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则
                "remarks" => '信用主体名称',    //备注
            ],
        ];
    }

    protected function wbjTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '委办局登记信息',
                'identify' => 'WBJDJXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 1,
                'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                'description' =>'委办局登记信息',
                'items' => json_encode($items, true),
                'source_unit_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function bjTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '本级登记信息',
                'identify' => 'BJDJXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 1,
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'本级登记信息',
                'items' => json_encode($items, true),
                'source_unit_id' => 1,
                'gb_template_id' => 1,
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function gbTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '国标登记信息',
                'identify' => 'QSXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 7,
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'国标登记信息',
                'items' => json_encode($items, true),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function baseTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '基础登记信息',
                'identify' => 'QSXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 7,
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'基础登记信息',
                'items' => json_encode($items, true),
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function qzjTemplate() : array
    {
        $items = $this->items();

        return
        [
            [
                'name' => '前置机登记信息',
                'identify' => 'QZJDJXX',
                'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                'dimension' => Template::DIMENSION['SHGK'],
                'exchange_frequency' => 1,
                'info_classify' => Template::INFO_CLASSIFY['QT'],
                'info_category' => Template::INFO_CATEGORY['QTXX'],
                'description' =>'前置机登记信息',
                'items' => json_encode($items, true),
                'source_unit_id' => 1,
                'category' => QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'],
                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }
}

<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class ParentTaskCache extends Cache
{
    const KEY = 'parent_task';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

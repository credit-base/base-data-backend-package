<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class AdapterFactoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new AdapterFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullAdapter()
    {
        $stub = $this->stub->getAdapter(0);
            $this->assertInstanceOf(
                'BaseData\Enterprise\Adapter\Enterprise\EnterpriseDbAdapter',
                $stub
            );
    }

    public function testGetAdapter()
    {
        foreach (AdapterFactory::MAPS as $key => $adapter) {
            $this->assertInstanceOf(
                $adapter,
                $this->stub->getAdapter($key)
            );
        }
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Model\NullWbjSearchData;
use BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\WbjSearchDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class WbjSearchDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockWbjSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchTemplate',
                                'fetchItemsData',
                                'searchDataFormatFilter'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IWbjSearchDataAdapter
     */
    public function testImplementsIWbjSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 WbjSearchDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockWbjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 WbjSearchDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockWbjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\WbjSearchDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetItemsDataRepository()
    {
        $adapter = new MockWbjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\WbjItemsDataRepository',
            $adapter->getItemsDataRepository()
        );
    }

    public function testGetTemplateRepository()
    {
        $adapter = new MockWbjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\WbjTemplateRepository',
            $adapter->getTemplateRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockWbjSearchDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedWbjSearchData = new WbjSearchData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedWbjSearchData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedWbjSearchData);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedWbjSearchData = new WbjSearchData();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedWbjSearchData, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedWbjSearchData, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedWbjSearchData = new WbjSearchData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWbjSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedWbjSearchData);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedWbjSearchData);

        //验证
        $wbjSearchData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWbjSearchData, $wbjSearchData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $wbjSearchDataOne = new WbjSearchData(1);
        $wbjSearchDataTwo = new WbjSearchData(2);

        $ids = [1, 2];

        $expectedWbjSearchDataList = [];
        $expectedWbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $expectedWbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWbjSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchTemplate')
                        ->with($expectedWbjSearchDataList);

        $this->adapter->expects($this->exactly(1))
                        ->method('fetchItemsData')
                        ->with($expectedWbjSearchDataList);
        //验证
        $wbjSearchDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWbjSearchDataList, $wbjSearchDataList);
    }

    public function testFormatFilter()
    {
        $filter = array();
        $wbjSearchData = new WbjSearchData();
        $expectedCondition = ' 1 ';

        $this->adapter->expects($this->exactly(1))
                        ->method('searchDataFormatFilter')
                        ->with($filter, $wbjSearchData)
                        ->willReturn($expectedCondition);

        $condition = $this->adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter/template
    public function testFormatFilterTemplate()
    {
        $filter = array(
            'template' => 1
        );
        $adapter = new MockWbjSearchDataDbAdapter();

        $expectedCondition = 'wbj_template_id = '.$filter['template'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockWbjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY wbj_search_data_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockWbjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY wbj_search_data_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatSort-updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockWbjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockWbjSearchDataDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

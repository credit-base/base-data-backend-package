<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory;

class ErrorDataMockAdapter implements IErrorDataAdapter
{
    public function fetchOne($id) : ErrorData
    {
        return ErrorDataMockFactory::generateErrorData($id);
    }

    public function fetchList(array $ids) : array
    {
        $errorDataList = array();

        foreach ($ids as $id) {
            $errorDataList[$id] = ErrorDataMockFactory::generateErrorData($id);
        }

        return $errorDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(ErrorData $errorData) : bool
    {
        unset($errorData);
        return true;
    }

    public function edit(ErrorData $errorData, array $keys = array()) : bool
    {
        unset($errorData);
        unset($keys);
        return true;
    }
}

<?php
namespace BaseData\Rule\View;

use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\UnAuditedRuleService;

class UnAuditedRuleServiceViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $unAuditedRuleService = new UnAuditedRuleServiceView(new UnAuditedRuleService());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $unAuditedRuleService);
    }
}

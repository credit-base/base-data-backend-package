<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Search;

use PHPUnit\DbUnit\DataSet\ArrayDataSet;

trait SearchTrait
{
    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
            'pcore_bj_search_data' => $this->bjSearchData()['pcore_bj_search_data']
        ]);
    }
}

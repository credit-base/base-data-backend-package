<?php
namespace BaseData\Rule;

use Marmot\Core;

use BaseData\Template\Model\Template;

use BaseData\Common\Model\IApproveAble;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Model\RuleService;

trait SetDataApiTrait
{
    protected function rule() : array
    {
        $rules = array(IRule::RULE_NAME['TRANSFORMATION_RULE']=>array(
            "ZTMC" => "ZTMC",
            "TYSHXYDM" => "TYSHXYDM",
            "ZTLB" => "ZTLB",
            "GKFW" => "GKFW",
            "GXPL" => "GXPL",
            "XXFL" => "XXFL",
            "XXLB" => "XXLB",
        ));
        return
        [
            [
                'rule_id' => 1,
                'transformation_category' => Template::CATEGORY['GB'],
                'transformation_template_id' => 1,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 1,
                'crew_id' => 1,
                'usergroup_id' => 1,
                'rules' => base64_encode(gzcompress(serialize($rules))),
                'version' => Core::$container->get('time'),
                'data_total' => 0,
                'status' => RuleService::STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'rule_id' => 2,
                'transformation_category' => Template::CATEGORY['GB'],
                'transformation_template_id' => 2,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 2,
                'crew_id' => 1,
                'usergroup_id' => 1,
                'rules' => base64_encode(gzcompress(serialize($rules))),
                'version' => Core::$container->get('time'),
                'data_total' => 0,
                'status' => RuleService::STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }

    protected function applyFormRule(
        $applyStatus = IApproveAble::APPLY_STATUS['PENDING'],
        $relationId = 0,
        $operationType = IApproveAble::OPERATION_TYPE['ADD']
    ) : array {
        $applyInfoOne = $this->rule()[0];
        $applyInfoTwo = $this->rule()[1];
        
        return
        [
            [
                'apply_form_rule_id' => 1,
                'publish_crew_id' => 1,
                'usergroup_id' => 1,
                'apply_usergroup_id' => 0,
                'apply_crew_id' => 0,
                'operation_type' => $operationType,
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoOne))),
                'reject_reason' => '',
                'transformation_category' => Template::CATEGORY['GB'],
                'transformation_template_id' => 1,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 1,
                'relation_id' => $relationId,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'apply_status' => $applyStatus,
                'status_time' => 0,
            ],
            [
                'apply_form_rule_id' => 2,
                'publish_crew_id' => 2,
                'usergroup_id' => 2,
                'apply_usergroup_id' => 2,
                'apply_crew_id' => 2,
                'operation_type' => $operationType,
                'apply_info' => base64_encode(gzcompress(serialize($applyInfoTwo))),
                'reject_reason' => '规则驳回原因',
                'transformation_category' => Template::CATEGORY['GB'],
                'transformation_template_id' => 2,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 2,
                'relation_id' => $relationId,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'apply_status' => $applyStatus,
                'status_time' => 0,
            ]
        ];
    }
}

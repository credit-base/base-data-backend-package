<?php
namespace BaseData\ResourceCatalogData\Adapter\Task\Query;

use Marmot\Framework\Query\RowCacheQuery;

class TaskRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'task_id',
            new Persistence\TaskCache(),
            new Persistence\TaskDb()
        );
    }
}

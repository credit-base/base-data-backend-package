<?php
namespace BaseData\Template\CommandHandler\WbjTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;
use BaseData\Template\Command\WbjTemplate\EditWbjTemplateCommand;
use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\UserGroup\Model\UserGroup;

class EditWbjTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo;
    
    private $repository;

    public function __construct()
    {
        $this->repository = new WbjTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getWbjTemplateRepository() : IWbjTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditWbjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getWbjTemplateRepository();
        $wbjTemplate = $repository->fetchOne($command->id);

        $wbjTemplate->setName($command->name);
        $wbjTemplate->setIdentify($command->identify);
        $wbjTemplate->setSubjectCategory($command->subjectCategory);
        $wbjTemplate->setDimension($command->dimension);
        $wbjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $wbjTemplate->setInfoClassify($command->infoClassify);
        $wbjTemplate->setInfoCategory($command->infoCategory);
        $wbjTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $wbjTemplate->setItems($items);
        $wbjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));

        return $wbjTemplate->edit();
    }
}

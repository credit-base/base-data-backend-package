<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Crew\Model\Crew;
use BaseData\UserGroup\Model\UserGroup;

interface ISearchDataAble
{
    /**
     * 信息分类
     * @var INFO_CLASSIFY['XZXK']  行政许可 1
     * @var INFO_CLASSIFY['XZCF']  行政处罚 2
     * @var INFO_CLASSIFY['HONGMD']  红名单 3
     * @var INFO_CLASSIFY['HEIMD']  黑名单 4
     * @var INFO_CLASSIFY['QT']  其他 5
     */
    const INFO_CLASSIFY = array(
        'XZXK' => 1,
        'XZCF' => 2,
        'HONGMD' => 3,
        'HEIMD' => 4,
        'QT' => 5
    );

    /**
     * 信息类别
     * @var INFO_CATEGORY['JCXX']  基础信息 1
     * @var INFO_CATEGORY['SHOUXXX']  守信信息 2
     * @var INFO_CATEGORY['SHIXXX']  失信信息 3
     * @var INFO_CATEGORY['QTXX']  其他信息 4
     */
    const INFO_CATEGORY = array(
        'JCXX' => 1,
        'SHOUXXX' => 2,
        'SHIXXX' => 3,
        'QTXX' => 4
    );

    /**
     * 主体类别
     * @var SUBJECT_CATEGORY['FRJFFRZZ']  法人及非法人组织 1
     * @var SUBJECT_CATEGORY['ZRR']  自然人 2
     * @var SUBJECT_CATEGORY['GTGSH']  个体工商户 3
     */
    const SUBJECT_CATEGORY = array(
        'FRJFFRZZ' => 1,
        'ZRR' => 2,
        'GTGSH' => 3
    );

    const SUBJECT_CATEGORY_CN = array(
        '法人及非法人组织' => self::SUBJECT_CATEGORY['FRJFFRZZ'],
        '自然人' => self::SUBJECT_CATEGORY['ZRR'],
        '个体工商户' => self::SUBJECT_CATEGORY['GTGSH']
    );

    /**
     * 公开范围
     * @var DIMENSION['SHGK']  社会公开 1
     * @var DIMENSION['ZWGX']  政务共享 2
     * @var DIMENSION['SQCX']  授权查询 3
     */
    const DIMENSION = array(
        'SHGK' => 1,
        'ZWGX' => 2,
        'SQCX' => 3
    );

    const DIMENSION_CN = array(
        '社会公开' => self::DIMENSION['SHGK'],
        '政务共享' => self::DIMENSION['ZWGX'],
        '授权查询' => self::DIMENSION['SQCX']
    );

    public function getInfoClassify() : int;

    public function getInfoCategory() : int;

    public function getCrew() : Crew;

    public function getSourceUnit() : UserGroup;

    public function getSubjectCategory() : int;

    public function getDimension() : int;

    public function getName() : string;

    public function getIdentify() : string;

    public function getExpirationDate() : int;

    public function getTemplate();

    public function getItemsData();
}

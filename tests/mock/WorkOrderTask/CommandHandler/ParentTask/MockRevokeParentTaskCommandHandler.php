<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;

class MockRevokeParentTaskCommandHandler extends RevokeParentTaskCommandHandler
{
    public function getParentTaskRepository() : IParentTaskAdapter
    {
        return parent::getParentTaskRepository();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\NullWbjTemplate;

use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class YsSearchDataTest extends TestCase
{
    private $ysSearchData;

    private $faker;

    public function setUp()
    {
        $this->ysSearchData = new MockYsSearchData();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->ysSearchData);
        unset($this->faker);
    }

    public function testImplementsIEnableAble()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IEnableAble',
            $this->ysSearchData
        );
    }
    
    public function testSearchDataConstructor()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\WbjTemplate',
            $this->ysSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\YsItemsData',
            $this->ysSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new YsItemsData();

        $this->ysSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->ysSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->ysSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end
    
    public function testGetYsItemsDataRepository()
    {
        $ysSearchData = new MockYsSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter',
            $ysSearchData->getYsItemsDataRepository()
        );
    }

    public function testGetRepository()
    {
        $ysSearchData = new MockYsSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter',
            $ysSearchData->getRepository()
        );
    }

    public function testAddItemsData()
    {
        $ysSearchData = $this->getMockBuilder(MockYsSearchData::class)
                           ->setMethods(['getYsItemsDataRepository'])
                           ->getMock();

        $repository = $this->prophesize(IYsItemsDataAdapter::class);
        $repository->add(Argument::exact($ysSearchData->getItemsData()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $ysSearchData->expects($this->once())
             ->method('getYsItemsDataRepository')
             ->willReturn($repository->reveal());

        $result = $ysSearchData->addItemsData();
        $this->assertTrue($result);
    }

    public function testAdd()
    {
        $ysSearchData = $this->getMockBuilder(MockYsSearchData::class)
                           ->setMethods(['assignment', 'validate', 'addItemsData', 'getRepository'])
                           ->getMock();

        $ysSearchData->expects($this->once())
            ->method('assignment');

        $ysSearchData->expects($this->once())
            ->method('validate')
            ->willReturn(true);

        $ysSearchData->expects($this->once())
            ->method('addItemsData')
            ->willReturn(true);

        $repository = $this->prophesize(IYsSearchDataAdapter::class);
        $repository->add(Argument::exact($ysSearchData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $ysSearchData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $ysSearchData->add();
        $this->assertTrue($result);
    }
}

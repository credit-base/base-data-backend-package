<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\WbjItemsData;
use BaseData\ResourceCatalogData\Model\NullWbjItemsData;
use BaseData\ResourceCatalogData\Translator\WbjItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\WbjItemsDataRowCacheQuery;

class WbjItemsDataDbAdapter implements IWbjItemsDataAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new WbjItemsDataDbTranslator();
        $this->rowCacheQuery = new WbjItemsDataRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : WbjItemsDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : WbjItemsDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullWbjItemsData::getInstance();
    }

    public function add(WbjItemsData $wbjItemsData) : bool
    {
        return $this->addAction($wbjItemsData);
    }

    public function fetchOne($id) : WbjItemsData
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        unset($filter);
        unset($conjection);

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        unset($sort);
        unset($conjection);

        return $condition;
    }
}

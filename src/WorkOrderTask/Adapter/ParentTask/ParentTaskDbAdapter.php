<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Model\NullParentTask;
use BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator;
use BaseData\WorkOrderTask\Adapter\ParentTask\Query\ParentTaskRowCacheQuery;

class ParentTaskDbAdapter implements IParentTaskAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new ParentTaskDbTranslator();
        $this->rowCacheQuery = new ParentTaskRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : ParentTaskDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : ParentTaskRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullParentTask::getInstance();
    }

    public function add(ParentTask $parentTask) : bool
    {
        return $this->addAction($parentTask);
    }

    public function edit(ParentTask $parentTask, array $keys = array()) : bool
    {
        return $this->editAction($parentTask, $keys);
    }

    public function fetchOne($id) : ParentTask
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $parentTask = new ParentTask();

            if (isset($filter['title'])) {
                $parentTask->setTitle($filter['title']);
                $info = $this->getDbTranslator()->objectToArray($parentTask, array('title'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['endTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new ParentTask(), array('endTime'));
                $condition .= $conjection.key($info).' '.($sort['endTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

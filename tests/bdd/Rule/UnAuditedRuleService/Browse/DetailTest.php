<?php
namespace BaseData\Rule\UnAuditedRuleService\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则审核权限,在政务网OA的资源目录规则审核中,
 *           我可以管理委办局提交的资源目录的规则,可以查看审核状态为已驳回或待审核的资源目录规则的内容信息和审核信息,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 查看规则审核详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_wbj_template');
    }

    /**
     * @Given: 存在一条规则审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form_rule' => $this->applyFormRule(),
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_wbj_template' => $this->wbjTemplate()
        ]);
    }

    /**
     * @When: 当我查看该条规则数据详情时
     */
    public function fetchUnAuditedRuleService($id)
    {
        $repository = new UnAuditedRuleServiceRepository();

        $unAuditedRuleService = $repository->fetchOne($id);

        return $unAuditedRuleService;
    }

    /**
     * @Then: 我可以看见该条规则数据的全部信息
     */
    public function testViewUnAuditedRuleServiceList()
    {
        $id = 1;
        
        $setUnAuditedRuleService = $this->applyFormRule()[0];
        $setUnAuditedRuleService['apply_form_rule_id'] = $id;
        $unAuditedRuleService = $this->fetchUnAuditedRuleService($id);

        $translator = new UnAuditedRuleServiceDbTranslator();
        $unAuditedRuleServiceArray = $translator->objectToArray($unAuditedRuleService);

        $this->assertEquals($unAuditedRuleServiceArray, $setUnAuditedRuleService);
    }
}

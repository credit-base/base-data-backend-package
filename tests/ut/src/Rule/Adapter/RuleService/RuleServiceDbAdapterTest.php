<?php
namespace BaseData\Rule\Adapter\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\NullRuleService;
use BaseData\Rule\Translator\RuleServiceDbTranslator;
use BaseData\Rule\Adapter\RuleService\Query\RuleServiceRowCacheQuery;

use BaseData\Template\Repository\WbjTemplateRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(RuleServiceDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                'fetchSourceTemplate',
                                'fetchTransformationTemplate',
                                'fetchRules'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IRuleServiceAdapter
     */
    public function testImplementsIRuleServiceAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 RuleServiceDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockRuleServiceDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Rule\Translator\RuleServiceDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 RuleServiceRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockRuleServiceDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Rule\Adapter\RuleService\Query\RuleServiceRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockRuleServiceDbAdapter();
        $this->assertInstanceOf('Marmot\Interfaces\INull', $adapter->getNullObject());
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedRuleService = new RuleService();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedRuleService)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedRuleService);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedRuleService = new RuleService();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedRuleService, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedRuleService, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedRuleService = new RuleService();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedRuleService);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceTemplate')
                         ->with($expectedRuleService);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTransformationTemplate')
                         ->with($expectedRuleService);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchRules')
                         ->with($expectedRuleService);
        //验证
        $ruleService = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedRuleService, $ruleService);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $ruleServiceOne = new RuleService(1);
        $ruleServiceTwo = new RuleService(2);

        $ids = [1, 2];

        $expectedRuleServiceList = [];
        $expectedRuleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $expectedRuleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedRuleServiceList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchSourceTemplate')
                         ->with($expectedRuleServiceList);

        $this->adapter->expects($this->exactly(1))
                         ->method('fetchTransformationTemplate')
                         ->with($expectedRuleServiceList);
        //验证
        $ruleServiceList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedRuleServiceList, $ruleServiceList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-userGroup
    public function testFormatFilterUserGroup()
    {
        $filter = array(
            'userGroup' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'usergroup_id = '.$filter['userGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-transformationTemplate
    public function testFormatFilterTransformationTemplate()
    {
        $filter = array(
            'transformationTemplate' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'transformation_template_id = '.$filter['transformationTemplate'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-sourceTemplate
    public function testFormatFilterSourceTemplate()
    {
        $filter = array(
            'sourceTemplate' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'source_template_id = '.$filter['sourceTemplate'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-transformationCategory
    public function testFormatFilterTransformationCategory()
    {
        $filter = array(
            'transformationCategory' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'transformation_category IN ('.$filter['transformationCategory'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-sourceCategory
    public function testFormatFilterSourceCategory()
    {
        $filter = array(
            'sourceCategory' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'source_category IN ('.$filter['sourceCategory'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-status
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => -2
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'status = '.$filter['status'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-sourceTemplateIds
    public function testFormatFilterSourceTemplateIds()
    {
        $filter = array(
            'sourceTemplateIds' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'source_template_id IN ('.$filter['sourceTemplateIds'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-excludeTransformationCategory
    public function testFormatFilterExcludeTransformationCategory()
    {
        $filter = array(
            'excludeTransformationCategory' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'transformation_category <> '.$filter['excludeTransformationCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-id
    public function testFormatFilterId()
    {
        $filter = array(
            'id' => 1
        );
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = 'rule_id <> '.$filter['id'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockRuleServiceDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $sortAdapter = new MockRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $sortAdapter = new MockRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--status
    public function testFormatSortStatusDesc()
    {
        $sort = array('status' => -1);
        $sortAdapter = new MockRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY status DESC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-status
    public function testFormatSortStatusAsc()
    {
        $sort = array('status' => 1);
        $sortAdapter = new MockRuleServiceDbAdapter();

        $expectedCondition = ' ORDER BY status ASC';

        //验证
        $condition = $sortAdapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

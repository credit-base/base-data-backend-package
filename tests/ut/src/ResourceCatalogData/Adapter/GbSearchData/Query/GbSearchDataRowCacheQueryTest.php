<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData\Query;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class GbSearchDataRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockGbSearchDataRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\Persistence\GbSearchDataCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\Persistence\GbSearchDataDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}

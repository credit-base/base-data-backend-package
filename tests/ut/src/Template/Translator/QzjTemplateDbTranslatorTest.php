<?php
namespace BaseData\Template\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Utils\QzjTemplateUtils;
use BaseData\Template\Utils\QzjTemplateMockFactory;

class QzjTemplateDbTranslatorTest extends TestCase
{
    use QzjTemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new QzjTemplateDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Template\Model\NullQzjTemplate', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $qzjTemplate = QzjTemplateMockFactory::generateQzjTemplate(1);

        $expression['qzj_template_id'] = $qzjTemplate->getId();
        $expression['name'] = $qzjTemplate->getName();
        $expression['identify'] = $qzjTemplate->getIdentify();
        $expression['category'] = $qzjTemplate->getCategory();
        $expression['subject_category'] = json_encode($qzjTemplate->getSubjectCategory());
        $expression['dimension'] = $qzjTemplate->getDimension();
        $expression['exchange_frequency'] = $qzjTemplate->getExchangeFrequency();
        $expression['info_classify'] = $qzjTemplate->getInfoClassify();
        $expression['info_category'] = $qzjTemplate->getInfoCategory();
        $expression['description'] = $qzjTemplate->getDescription();
        $expression['items'] = json_encode($qzjTemplate->getItems());
        $expression['source_unit_id'] = $qzjTemplate->getSourceUnit()->getId();

        $expression['status'] = $qzjTemplate->getStatus();
        $expression['status_time'] = $qzjTemplate->getStatusTime();
        $expression['create_time'] = $qzjTemplate->getCreateTime();
        $expression['update_time'] = $qzjTemplate->getUpdateTime();

        $qzjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Template\Model\QzjTemplate', $qzjTemplate);
        $this->compareArrayAndObject($expression, $qzjTemplate);
    }

    public function testObjectToArray()
    {
        $qzjTemplate = QzjTemplateMockFactory::generateQzjTemplate(1);

        $expression = $this->translator->objectToArray($qzjTemplate);

        $this->compareArrayAndObject($expression, $qzjTemplate);
    }
}

<?php
namespace BaseData\Crew\Utils;

trait CrewRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $crew
    ) {
        $this->assertEquals($expectedArray['data']['id'], $crew->getId());
        $this->assertEquals($expectedArray['data']['attributes']['cellphone'], $crew->getCellphone());
        $this->assertEquals($expectedArray['data']['attributes']['realName'], $crew->getRealName());
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $crew->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $crew->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $crew->getStatus());
        }
        if (isset($expectedArray['data']['relationships'])) {
            $relationships = $expectedArray['data']['relationships'];
            if (isset($relationships['userGroup']['data'])) {
                $this->assertEquals(
                    $relationships['userGroup']['data'][0]['type'],
                    'userGroups'
                );
                $this->assertEquals(
                    $relationships['userGroup']['data'][0]['id'],
                    $crew->getUserGroup()->getId()
                );
            }
        }
    }
}

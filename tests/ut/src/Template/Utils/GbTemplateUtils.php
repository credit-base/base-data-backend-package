<?php
namespace BaseData\Template\Utils;

trait GbTemplateUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbTemplate
    ) {
        $this->assertEquals($expectedArray['gb_template_id'], $gbTemplate->getId());
        $this->assertEquals($expectedArray['name'], $gbTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $gbTemplate->getIdentify());

        $subjectCategory = is_string($expectedArray['subject_category']) ?
        json_decode($expectedArray['subject_category'], true) :
        $expectedArray['subject_category'];
        $this->assertEquals($subjectCategory, $gbTemplate->getSubjectCategory());

        $this->assertEquals($expectedArray['dimension'], $gbTemplate->getDimension());
        $this->assertEquals($expectedArray['exchange_frequency'], $gbTemplate->getExchangeFrequency());
        $this->assertEquals($expectedArray['info_classify'], $gbTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $gbTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['description'], $gbTemplate->getDescription());

        $items = is_string($expectedArray['items']) ?
        json_decode($expectedArray['items'], true) :
        $expectedArray['items'];
        $this->assertEquals($items, $gbTemplate->getItems());

        $this->assertEquals($expectedArray['status'], $gbTemplate->getStatus());
        $this->assertEquals($expectedArray['create_time'], $gbTemplate->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $gbTemplate->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $gbTemplate->getStatusTime());
    }
}

<?php
namespace BaseData\ResourceCatalogData;

use BaseData\ResourceCatalogData\Model\ResourceCatalogData;

trait ResourceCatalogDataUtils
{
    private function wbjSearchDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->resourceCatalogDataCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['wbj_search_data_id'], $responseDataArray['id']);
        $this->assertEquals('wbjSearchData', $responseDataArray['type']);

        $relationshipsTemplate = $responseDataArray['relationships']['template']['data'];
        $this->assertEquals('templates', $relationshipsTemplate['type']);
        $this->assertEquals($setDataArray['wbj_template_id'], $relationshipsTemplate['id']);

        $relationshipsItemsData = $responseDataArray['relationships']['itemsData']['data'];
        $this->assertEquals('wbjItemsData', $relationshipsItemsData['type']);
        $this->assertEquals($setDataArray['wbj_items_data_id'], $relationshipsItemsData['id']);

        $relationshipsSourceUnit = $responseDataArray['relationships']['sourceUnit']['data'];
        $this->assertEquals('userGroups', $relationshipsSourceUnit['type']);
        $this->assertEquals($setDataArray['usergroup_id'], $relationshipsSourceUnit['id']);
    }

    private function wbjItemsDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['wbj_items_data_id'], $responseDataArray['id']);
        $this->assertEquals('wbjItemsData', $responseDataArray['type']);
        
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals(
            unserialize(gzuncompress(base64_decode($setDataArray['data'], true))),
            $attributes['data']
        );
    }

    private function bjSearchDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->resourceCatalogDataCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['bj_search_data_id'], $responseDataArray['id']);
        $this->assertEquals('bjSearchData', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['description'], $attributes['description']);
        $this->assertEquals($setDataArray['front_end_processor_status'], $attributes['frontEndProcessorStatus']);

        $relationshipsSourceUnit = $responseDataArray['relationships']['sourceUnit']['data'];
        $this->assertEquals('userGroups', $relationshipsSourceUnit['type']);
        $this->assertEquals($setDataArray['usergroup_id'], $relationshipsSourceUnit['id']);

        $relationshipsTemplate = $responseDataArray['relationships']['template']['data'];
        $this->assertEquals('bjTemplates', $relationshipsTemplate['type']);
        $this->assertEquals($setDataArray['bj_template_id'], $relationshipsTemplate['id']);

        $relationshipsItemsData = $responseDataArray['relationships']['itemsData']['data'];
        $this->assertEquals('bjItemsData', $relationshipsItemsData['type']);
        $this->assertEquals($setDataArray['bj_items_data_id'], $relationshipsItemsData['id']);
    }

    private function bjItemsDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['bj_items_data_id'], $responseDataArray['id']);
        $this->assertEquals('bjItemsData', $responseDataArray['type']);
        
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals(
            unserialize(gzuncompress(base64_decode($setDataArray['data'], true))),
            $attributes['data']
        );
    }

    private function gbSearchDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->resourceCatalogDataCompareData($setDataArray, $responseDataArray);

        $this->assertEquals($setDataArray['gb_search_data_id'], $responseDataArray['id']);
        $this->assertEquals('gbSearchData', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['description'], $attributes['description']);
        $this->assertEquals($setDataArray['front_end_processor_status'], $attributes['frontEndProcessorStatus']);

        $relationshipsTemplate = $responseDataArray['relationships']['template']['data'];
        $this->assertEquals('gbTemplates', $relationshipsTemplate['type']);
        $this->assertEquals($setDataArray['gb_template_id'], $relationshipsTemplate['id']);

        $relationshipsItemsData = $responseDataArray['relationships']['itemsData']['data'];
        $this->assertEquals('gbItemsData', $relationshipsItemsData['type']);
        $this->assertEquals($setDataArray['gb_items_data_id'], $relationshipsItemsData['id']);
    }

    private function gbItemsDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['gb_items_data_id'], $responseDataArray['id']);
        $this->assertEquals('gbItemsData', $responseDataArray['type']);
        
        $attributes = $responseDataArray['attributes'];
        $this->assertEquals(
            unserialize(gzuncompress(base64_decode($setDataArray['data'], true))),
            $attributes['data']
        );
    }

    private function errorDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['error_data_id'], $responseDataArray['id']);
        $this->assertEquals('errorDatas', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['category'], $attributes['category']);
        $this->assertEquals($setDataArray['error_type'], $attributes['errorType']);
        $this->assertEquals(json_decode($setDataArray['error_reason'], true), $attributes['errorReason']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);
        $this->assertEquals(
            unserialize(gzuncompress(base64_decode($setDataArray['items_data'], true))),
            $attributes['itemsData']
        );

        $relationshipsTask = $responseDataArray['relationships']['task']['data'];
        $this->assertEquals('tasks', $relationshipsTask['type']);
        $this->assertEquals($setDataArray['task_id'], $relationshipsTask['id']);

        $relationshipsTemplate = $responseDataArray['relationships']['template']['data'];
        $this->assertEquals('bjTemplates', $relationshipsTemplate['type']);
        $this->assertEquals($setDataArray['template_id'], $relationshipsTemplate['id']);
    }

    private function resourceCatalogDataCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $attributes = $responseDataArray['attributes'];
        
        $this->assertEquals($setDataArray['name'], $attributes['name']);
        $this->assertEquals($setDataArray['identify'], $attributes['identify']);
        $this->assertEquals($setDataArray['info_classify'], $attributes['infoClassify']);
        $this->assertEquals($setDataArray['info_category'], $attributes['infoCategory']);
        $this->assertEquals($setDataArray['subject_category'], $attributes['subjectCategory']);
        $this->assertEquals($setDataArray['dimension'], $attributes['dimension']);
        $this->assertEquals($setDataArray['expiration_date'], $attributes['expirationDate']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);
        
        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($setDataArray['crew_id'], $relationshipsCrew['id']);
    }

    private function taskCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['task_id'], $responseDataArray['id']);
        $this->assertEquals('tasks', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['pid'], $attributes['pid']);
        $this->assertEquals($setDataArray['total'], $attributes['total']);
        $this->assertEquals($setDataArray['success_number'], $attributes['successNumber']);
        $this->assertEquals($setDataArray['failure_number'], $attributes['failureNumber']);
        $this->assertEquals($setDataArray['source_category'], $attributes['sourceCategory']);
        $this->assertEquals($setDataArray['source_template_id'], $attributes['sourceTemplate']);
        $this->assertEquals($setDataArray['target_category'], $attributes['targetCategory']);
        $this->assertEquals($setDataArray['target_template_id'], $attributes['targetTemplate']);
        $this->assertEquals($setDataArray['target_rule_id'], $attributes['targetRule']);
        $this->assertEquals($setDataArray['schedule_task_id'], $attributes['scheduleTask']);
        $this->assertEquals($setDataArray['error_number'], $attributes['errorNumber']);
        $this->assertEquals($setDataArray['file_name'], $attributes['fileName']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsUserGroup = $responseDataArray['relationships']['userGroup']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals($setDataArray['user_group_id'], $relationshipsUserGroup['id']);

        $relationshipsCrew = $responseDataArray['relationships']['crew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals($setDataArray['crew_id'], $relationshipsCrew['id']);
    }
}

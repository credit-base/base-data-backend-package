<?php
namespace BaseData\ResourceCatalogData\Translator\Strategy;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\BaseSearchData;

class NullSearchDataDbTranslatorStrategyTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new NullSearchDataDbTranslatorStrategy();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\Strategy\IDbTranslatorStrategy',
            $this->translator
        );
    }

    public function testObjectToArray()
    {
        $baseSearchData = new BaseSearchData();

        $result = $this->translator->objectToArray($baseSearchData);

        $this->assertFalse($result);
    }
}

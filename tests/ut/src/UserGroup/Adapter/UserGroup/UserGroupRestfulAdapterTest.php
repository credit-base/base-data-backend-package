<?php
namespace BaseData\UserGroup\Adapter\UserGroup;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\IRestfulTranslator;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Model\NullUserGroup;

class UserGroupRestfulAdapterTest extends TestCase
{
    private $adapter;
    
    private $childAdapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(UserGroupRestfulAdapter::class)
                           ->setMethods([
                                'getTranslator',
                                'getResource',
                            ])
                           ->getMock();
                           
        $this->childAdapter = new class extends UserGroupRestfulAdapter
        {
            public function getTranslator() : IRestfulTranslator
            {
                return parent::getTranslator();
            }
            public function getResource() : string
            {
                return parent::getResource();
            }
            public function getScenario() : array
            {
                return parent::getScenario();
            }
        };
    }

    public function tearDown()
    {
        unset($this->adapter);
        unset($this->childAdapter);
    }

    public function testExtendsGuzzleAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Basecode\Adapter\Restful\GuzzleAdapter',
            $this->adapter
        );
    }

    public function testImplementsIUserGroupAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $this->adapter
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->childAdapter->getTranslator()
        );
    }

    public function testGetResource()
    {
        $this->assertEquals('userGroups', $this->childAdapter->getResource());
    }

    /**
     * 循环测试 scenario() 是否符合预定范围
     *
     * @dataProvider scenarioDataProvider
     */
    public function testScenario($expect, $actual)
    {
        $this->childAdapter->scenario($expect);
        $this->assertEquals($actual, $this->childAdapter->getScenario());
    }
     /**
     * 循环测试 testScenario() 数据构建器
     */
    public function scenarioDataProvider()
    {
        return [
            [
                'USER_GROUP_LIST',
                UserGroupRestfulAdapter::SCENARIOS['USER_GROUP_LIST']
            ],
            [
                'USER_GROUP_FETCH_ONE',
                UserGroupRestfulAdapter::SCENARIOS['USER_GROUP_FETCH_ONE']
            ],
            ['NULL', array()]
        ];
    }

    public function testFetchOneSuccess()
    {
        $adapter = $this->getMockBuilder(UserGroupRestfulAdapter::class)
                           ->setMethods([
                                'get',
                                'getResource',
                                'isSuccess',
                                'translateToObject'
                            ])->getMock();

        $id = 1;

        $userGroup = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $adapter->expects($this->exactly(1))->method('getResource')->willReturn('userGroups');
        $adapter->expects($this->exactly(1))->method('get');
        $adapter->expects($this->exactly(1))->method('isSuccess')->willReturn(true);
        $adapter->expects($this->exactly(1))->method('translateToObject')->willReturn($userGroup);
            
        $result = $adapter->fetchOne($id);
        $this->assertEquals($userGroup, $result);
    }

    public function testFetchOneFail()
    {
        $adapter = $this->getMockBuilder(UserGroupRestfulAdapter::class)
                           ->setMethods([
                                'get',
                                'getResource',
                                'isSuccess'
                            ])->getMock();

        $id = 1;

        $adapter->expects($this->exactly(1))->method('getResource')->willReturn('userGroups');
        $adapter->expects($this->exactly(1))->method('get');
        $adapter->expects($this->exactly(1))->method('isSuccess')->willReturn(false);
            
        $result = $adapter->fetchOne($id);
        $this->assertEquals(NullUserGroup::getInstance(), $result);
    }
}

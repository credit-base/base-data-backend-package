<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\YsItemsData;
use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要上传资源目录数据时,在资源目录数据子系统下的原始数据管理,上传对应的资源目录数据
 *           根据我归集到的资源目录数据上传,以便于我实现资源目录数据的报送和平台资源目录数据的呈现
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_ys_items_data');
        $this->clear('pcore_ys_search_data');
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_ys_items_data' => $this->ysSearchData()['pcore_ys_items_data'],
            'pcore_ys_search_data' => $this->ysSearchData()['pcore_ys_search_data']
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回false
     */
    public function add()
    {
        $templateRepository = new WbjTemplateRepository();
        $wbjTemplate = $templateRepository->fetchOne(1);

        $ysItemsDataArray = $this->ysSearchData()['pcore_ys_items_data'][0];
        $ysSearchDataArray = $this->ysSearchData()['pcore_ys_search_data'][0];

        $ysItemsData = new YsItemsData();
        $ysItemsData->setData(unserialize(gzuncompress(base64_decode($ysItemsDataArray['data'], true))));

        $ysSearchData = new YsSearchData();
        $ysSearchData->getCrew()->setId($ysSearchDataArray['crew_id']);
        $ysSearchData->getSourceUnit()->setId($ysSearchDataArray['usergroup_id']);
        $ysSearchData->setSubjectCategory($ysSearchDataArray['subject_category']);
        $ysSearchData->setDimension($ysSearchDataArray['dimension']);
        $ysSearchData->setExpirationDate($ysSearchDataArray['expiration_date']);
        $ysSearchData->setTemplate($wbjTemplate);
        $ysSearchData->setItemsData($ysItemsData);
        $ysSearchData->setHash($ysSearchDataArray['hash']);
        
        return $ysSearchData->add();
    }

    /**
     * @Then: 数据已经存在
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\Task;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Translator\TaskDbTranslator;
use BaseData\ResourceCatalogData\Adapter\Task\Query\TaskRowCacheQuery;

class MockTaskDbAdapter extends TaskDbAdapter
{
    public function getDbTranslator() : TaskDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : TaskRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

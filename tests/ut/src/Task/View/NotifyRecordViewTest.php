<?php
namespace BaseData\Task\View;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class NotifyRecordViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockNotifyRecordView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testSetData()
    {
        $data = 'data';
        $this->view->setData($data);

        $this->assertEquals($data, $this->view->getData());
    }

    public function testDisplay()
    {
        $expect = 'expect';
        $data = 'data';
        $encodingParameters = null;
        $rules = array(
            \BaseData\Task\Model\NotifyRecord::class =>
            \BaseData\Task\View\NotifyRecordSchema::class
        );

        $notifyRecord = $this->getMockBuilder(NotifyRecordView::class)
                           ->setMethods(['jsonApiFormat'])
                           ->setConstructorArgs([$data, $encodingParameters])
                           ->getMock();


        $notifyRecord->expects($this->once())
                     ->method('jsonApiFormat')
                     ->with(
                         $data,
                         $rules,
                         $encodingParameters
                     )->willReturn($expect);

        $result = $notifyRecord->display();
        $this->assertEquals($expect, $result);
    }
}

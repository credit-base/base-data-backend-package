<?php
namespace BaseData\Enterprise\Enterprise\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Enterprise\SetDataTrait;
use BaseData\Enterprise\Repository\EnterpriseRepository;
use BaseData\Enterprise\Translator\EnterpriseDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看企业数据时,在信用信息查询应用系统下的企业管理中
 *           可以查看到所有的企业数据,通过列表和详情的形式查看我所有的企业, 以便于我可以精确的查看到不同的企业信息
 * @Scenario: 查看企业数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_enterprise');
    }

    /**
     * @Given: 存在一条企业数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_enterprise' => $this->enterprise()
        ]);
    }

    /**
     * @When: 当我查看该条企业数据详情时
     */
    public function fetchEnterprise($id)
    {
        $repository = new EnterpriseRepository();

        $enterprise = $repository->fetchOne($id);

        return $enterprise;
    }

    /**
     * @Then: 我可以看见该条企业数据的全部信息
     */
    public function testViewEnterpriseList()
    {
        $id = 1;
        
        $setEnterprise = $this->enterprise()[0];
        $setEnterprise['enterprise_id'] = $id;

        $enterprise = $this->fetchEnterprise($id);
        $translator = new EnterpriseDbTranslator();
        $enterpriseArray = $translator->objectToArray($enterprise);

        $this->assertEquals($enterpriseArray, $setEnterprise);
    }
}

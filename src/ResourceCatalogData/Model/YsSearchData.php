<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\Model\EnableAbleTrait;

use BaseData\Template\Model\WbjTemplate;

use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Repository\YsItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;

class YsSearchData extends SearchData implements IEnableAble
{
    use EnableAbleTrait;

    protected $repository;

    protected $ysItemsDataRepository;

    public function __construct()
    {
        parent::__construct();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->template = new WbjTemplate();
        $this->itemsData = new YsItemsData();
        $this->repository = new YsSearchDataRepository();
        $this->ysItemsDataRepository = new YsItemsDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->template);
        unset($this->itemsData);
        unset($this->repository);
        unset($this->ysItemsDataRepository);
    }

    public function setItemsData(YsItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : YsItemsData
    {
        return $this->itemsData;
    }

    protected function getRepository() : IYsSearchDataAdapter
    {
        return $this->repository;
    }
    
    protected function getYsItemsDataRepository() : IYsItemsDataAdapter
    {
        return $this->ysItemsDataRepository;
    }

    protected function addItemsData() : bool
    {
        return $this->getYsItemsDataRepository()->add($this->getItemsData());
    }

    protected function addAction() : bool
    {
        $this->assignment();
        
        return $this->validate() && $this->addItemsData() && $this->getRepository()->add($this);
    }
}

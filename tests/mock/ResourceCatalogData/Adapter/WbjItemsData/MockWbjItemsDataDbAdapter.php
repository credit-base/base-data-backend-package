<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Translator\WbjItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\WbjItemsDataRowCacheQuery;

class MockWbjItemsDataDbAdapter extends WbjItemsDataDbAdapter
{
    public function getDbTranslator() : WbjItemsDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : WbjItemsDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

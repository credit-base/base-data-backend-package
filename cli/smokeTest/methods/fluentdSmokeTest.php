<?php

use Fluent\Logger\FluentLogger;

function fluentdSmokeTest(string $address, int $port)
{
    $url = FluentLogger::getTransportUri($address, $port);
    $socket = @stream_socket_client($url);
    if(!$socket) {
        return false;
    }

    return true;
}

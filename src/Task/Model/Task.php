<?php
namespace BaseData\Task\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use BaseData\Task\Adapter\Task\ITaskAdapter;
use BaseData\Task\Repository\TaskRepository;

abstract class Task implements IObject, IScheduleAble
{
    /**
     * UNDEFINED: 未定义任务
     * ONCE: 一次性任务
     * PERIOD: 周期性任务
     */
    const TYPE = ['UNDEFINED'=>-4, 'ONCE'=>1, 'PERIOD'=>2];

    /**
     * UNDEFINED: 未定义状态
     * FAIL: 失败状态，任务未执行成功
     * UNUSUAL: 异常状态，任务异常终止
     * SLEEP: 睡眠状态，任务未被唤醒
     * PENDING: 待执行状态，任务已经准备好执行
     * EXECUTING: 运行中
     * SUCCESS: 成功状态
     */
    const STATUS = [
        'UNDEFINED'=>-4,
        'FAIL'=>-3,
        'UNUSUAL'=>-2,
        'SLEEP' => 0,
        'PENDING'=>2,
        'EXECUTING'=>4,
        'SUCCESS'=>6
    ];

    use Object;

    protected $id;

    protected $parentId;

    /**
     * string 任务标识，自动获取任务脚本的 class 名字
     */
    protected $identify;

    protected $type;

    /**
     * array 任务数据，任务运行时所需数据
     */
    protected $data;

    /**
     * 时间戳, 任务下次调度时间
     */
    protected $nextScheduleTime;

    /**
     * int nice值, 任务优先级. 值越小优先级越高
     */
    protected $nice;

    protected $repository;

    protected $pid;

    public function __construct(int $id = 0, array $data = array())
    {
        $this->id = !empty($id) ? $id : 0;
        $this->parentId = 0;
        $this->identify = get_class($this);
        $this->type = self::TYPE['UNDEFINED'];
        $this->data = $data;
        $this->nextScheduleTime = 0;
        $this->nice = 0;
        $this->status = self::STATUS['PENDING'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new TaskRepository();
        $this->pid = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->parentId);
        unset($this->identify);
        unset($this->type);
        unset($this->data);
        unset($this->nextScheduleTime);
        unset($this->nice);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->taskRepository);
        unset($this->pid);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * 进程id
     */
    public function setPid(int $pid) : void
    {
        $this->pid = $pid;
    }

    public function getPid() : int
    {
        return $this->pid;
    }

    public function setParentId(int $parentId) : void
    {
        $this->parentId = $parentId;
    }

    public function getParentId() : int
    {
        return $this->parentId;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setData(array $data) : void
    {
        $this->data = $data;
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function setNextScheduleTime(int $nextScheduleTime) : void
    {
        $this->nextScheduleTime = $nextScheduleTime;
    }

    public function getNextScheduleTime() : int
    {
        return $this->nextScheduleTime;
    }

    public function setNice(int $nice) : void
    {
        $this->nice = $nice;
    }

    public function getNice() : int
    {
        return $this->nice;
    }

    //type
    public function setType(int $type) : void
    {
        $this->type = in_array(
            $type,
            self::TYPE
        ) ? $type : self::TYPE['UNDEFINED'];
    }

    public function getType() : int
    {
        return $this->type;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            self::STATUS
        ) ? $status : self::STATUS['UNDEFINED'];
    }

    protected function getRepository() : ITaskAdapter
    {
        return $this->repository;
    }

    /**
     * @param subTasks array<Task> 任务对象数组
     *
     * 添加任务，如果子任务传参不为空则会同时添加子任务
     */
    public function add(array $subTasks = array()) : bool
    {
        if (!$this->getRepository()->add($this)) {
            return false;
        }

        if (!empty($subTasks)) {
            $this->addSubTasks($subTasks);
        }

        return true;
    }

    /**
     * 添加子任务，子任务默认为 SLEEP 状态
     * 父任务执行完成后，子任务从 SLEEP 状态转为 PENDDING 状态
     */
    protected function addSubTasks(array $subTasks = array()) : bool
    {
        if (empty($subTasks)) {
            return false;
        }

        foreach ($subTasks as $subTask) {
            $subTask->setParentId($this->getId());
            $subTask->setStatus(self::STATUS['SLEEP']);

            if (!$subTask->add()) {
                return false;
            }
        }

        return true;
    }

    public function edit() : bool
    {
        return false;
    }

    public function fail() : bool
    {
        $this->setStatus(Task::STATUS['FAIL']);
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'statusTime'
            )
        );
    }

    public function unusual() : bool
    {
        $this->setStatus(Task::STATUS['UNUSUAL']);
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'statusTime'
            )
        );
    }
    
    /**
     * 更新进程id
     */
    protected function updateProcessId() : bool
    {
        $this->pid = posix_getpid();
        return $this->getRepository()->edit(
            $this,
            array(
                'pid'
            )
        );
    }

    //把所有子任务从睡眠状态触发为PENDING
    protected function notifySubTasks() : bool
    {
        return $this->getRepository()->notifySubTasks(
            $this
        );
    }

    public function execute() : bool
    {
        $this->beforeExecute();
        
        //任务执行时，被分配一个独立进程，更新进程id
        if ($this->updateProcessId() && $this->executeAction()) {
            //触发子任务
            $this->notifySubTasks();

            $this->afterExecute();
            return true;
        }

        return false;
    }

    public function scheduled() : bool
    {
        $this->setStatus(Task::STATUS['EXECUTING']);
        $this->setStatusTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'statusTime'
            )
        );
    }

    public function refresh()
    {
        $this->repository = new TaskRepository();
        $this->pid = posix_getpid();
    }
    
    abstract protected function executeAction() : bool;

    protected function beforeExecute() : bool
    {
        return true;
    }

    protected function afterExecute() : bool
    {
        return true;
    }
}

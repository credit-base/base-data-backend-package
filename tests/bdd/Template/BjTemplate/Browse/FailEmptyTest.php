<?php
namespace BaseData\Template\BjTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\BjTemplateRepository;

/**
 * @Feature: 我是发改委领导,当我需要查看我的本级目录时,在政务网OA中,可以查看到我的所有本级目录,
 *           通过列表和详情的形式查看所有本级目录,以便于我可以看到我的所有本级目录
 * @Scenario: 查看资源目录列表-异常数据为空
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
    }

    /**
     * @Given: 不存在资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBjTemplateList()
    {
        $repository = new BjTemplateRepository();

        list($bjTemplateList, $count) = $repository->filter([]);
        
        unset($count);

        return $bjTemplateList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewBjTemplate()
    {
        $bjTemplateList = $this->fetchBjTemplateList();

        $this->assertEmpty($bjTemplateList);
    }
}

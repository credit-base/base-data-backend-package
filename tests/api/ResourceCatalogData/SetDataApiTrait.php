<?php
namespace BaseData\ResourceCatalogData;

use Marmot\Core;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

use BaseData\Common\Model\IEnableAble;

trait SetDataApiTrait
{
    protected function wbjSearchData()  : array
    {
        $dataOne = array(
            'ZTMC' => '中研社食品有限公司',
            'TYSHXYDM' => '91120104MA06FBJY36',
        );

        $dataTwo = array(
            'ZTMC' => '中研社食品有限分公司',
            'TYSHXYDM' => '91120104MA06FBJY37',
        );

        $hashOne = md5(base64_encode(gzcompress(serialize($dataOne))));
        $hashTwo = md5(base64_encode(gzcompress(serialize($dataTwo))));

        return [
            'pcore_wbj_search_data' =>
                [
                    [
                        'wbj_search_data_id' => 1,
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['HEIMD'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['SHIXXX'],
                        'wbj_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['ZWGX'],
                        'name' => $dataOne['ZTMC'],
                        'identify' => $dataOne['TYSHXYDM'],
                        'expiration_date' => 2147483647,
                        'wbj_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'hash' => $hashOne,
                        'task_id' => 0
                    ],
                    [
                        'wbj_search_data_id' => 2,
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['XZCF'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['SHIXXX'],
                        'wbj_template_id' => 2,
                        'usergroup_id' => 2,
                        'crew_id' => 2,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['SHGK'],
                        'name' => $dataTwo['ZTMC'],
                        'identify' => $dataTwo['TYSHXYDM'],
                        'expiration_date' => 2147483648,
                        'wbj_items_data_id' => 2,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => IEnableAble::STATUS['ENABLED'],
                        'hash' => $hashTwo,
                        'task_id' => 0
                    ]
                ],
            'pcore_wbj_items_data' =>
                [
                    [
                        'wbj_items_data_id' => 1,
                        'data' => base64_encode(gzcompress(serialize($dataOne)))
                    ],
                    [
                        'wbj_items_data_id' => 2,
                        'data' => base64_encode(gzcompress(serialize($dataTwo)))
                    ]
                ],
        ];
    }

    protected function bjSearchData()  : array
    {
        $dataOne = array(
            'ZTMC' => '陕西大药房',
            'TYSHXYDM' => '91120104MA06FBJY39',
        );

        $dataTwo = array(
            'ZTMC' => '陕西大药房分店',
            'TYSHXYDM' => '91120104MA06FBJY49',
        );

        $hashOne = md5(base64_encode(gzcompress(serialize($dataOne))));
        $hashTwo = md5(base64_encode(gzcompress(serialize($dataTwo))));

        return [
            'pcore_bj_search_data' =>
                [
                    [
                        'bj_search_data_id' => 1,
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['HONGMD'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['SHOUXXX'],
                        'bj_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['SHGK'],
                        'name' => $dataOne['ZTMC'],
                        'identify' => $dataOne['TYSHXYDM'],
                        'expiration_date' => 2147483657,
                        'bj_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => BjSearchData::STATUS['CONFIRM'],
                        'hash' => $hashOne,
                        'task_id' => 0,
                        'description' => '',
                        'front_end_processor_status' => BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
                    ],
                    [
                        'bj_search_data_id' => 2,
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['SHOUXXX'],
                        'bj_template_id' => 2,
                        'usergroup_id' => 2,
                        'crew_id' => 2,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'],
                        'dimension' => ISearchDataAble::DIMENSION['SHGK'],
                        'name' => $dataTwo['ZTMC'],
                        'identify' => $dataTwo['TYSHXYDM'],
                        'expiration_date' => 2147483648,
                        'bj_items_data_id' => 2,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => BjSearchData::STATUS['ENABLED'],
                        'hash' => $hashTwo,
                        'task_id' => 0,
                        'description' => '',
                        'front_end_processor_status' => BjSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
                    ]
                ],
            'pcore_bj_items_data' =>
                [
                    [
                        'bj_items_data_id' => 1,
                        'data' => base64_encode(gzcompress(serialize($dataOne)))
                    ],
                    [
                        'bj_items_data_id' => 2,
                        'data' => base64_encode(gzcompress(serialize($dataTwo)))
                    ]
                ],
        ];
    }

    protected function gbSearchData()  : array
    {
        $dataOne = array(
            'ZTMC' => '陕西出租公司',
            'TYSHXYDM' => '91220104MA06FBJY39',
        );

        $dataTwo = array(
            'ZTMC' => '陕西出租分公司',
            'TYSHXYDM' => '91220114MA06FBJY39',
        );

        $hashOne = md5(base64_encode(gzcompress(serialize($dataOne))));
        $hashTwo = md5(base64_encode(gzcompress(serialize($dataTwo))));

        return [
            'pcore_gb_search_data' =>
                [
                    [
                        'gb_search_data_id' => 1,
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['JCXX'],
                        'gb_template_id' => 1,
                        'usergroup_id' => 1,
                        'crew_id' => 1,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['GTGSH'],
                        'dimension' => ISearchDataAble::DIMENSION['SQCX'],
                        'name' => $dataOne['ZTMC'],
                        'identify' => $dataOne['TYSHXYDM'],
                        'expiration_date' => 2147483646,
                        'gb_items_data_id' => 1,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => GbSearchData::STATUS['CONFIRM'],
                        'hash' => $hashOne,
                        'task_id' => 0,
                        'description' => '',
                        'front_end_processor_status' => GbSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
                    ],
                    [
                        'gb_search_data_id' => 2,
                        'info_classify' => ISearchDataAble::INFO_CLASSIFY['QT'],
                        'info_category' => ISearchDataAble::INFO_CATEGORY['JCXX'],
                        'gb_template_id' => 2,
                        'usergroup_id' => 2,
                        'crew_id' => 2,
                        'subject_category' => ISearchDataAble::SUBJECT_CATEGORY['GTGSH'],
                        'dimension' => ISearchDataAble::DIMENSION['ZWGX'],
                        'name' => $dataTwo['ZTMC'],
                        'identify' => $dataTwo['TYSHXYDM'],
                        'expiration_date' => 2147483647,
                        'gb_items_data_id' => 2,
                        'create_time' => Core::$container->get('time'),
                        'update_time' => Core::$container->get('time'),
                        'status_time' => 0,
                        'status' => GbSearchData::STATUS['ENABLED'],
                        'hash' => $hashTwo,
                        'task_id' => 0,
                        'description' => '',
                        'front_end_processor_status' => GbSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT']
                    ]
                ],
            'pcore_gb_items_data' =>
                [
                    [
                        'gb_items_data_id' => 1,
                        'data' => base64_encode(gzcompress(serialize($dataOne)))
                    ],
                    [
                        'gb_items_data_id' => 2,
                        'data' => base64_encode(gzcompress(serialize($dataTwo)))
                    ]
                ],
        ];
    }

    protected function task() : array
    {
        return
        [
            [
                'task_id' => 1,
                'crew_id' => 1,
                'user_group_id' => 1,
                'pid' => 0,
                'total' => 200,
                'success_number' => 60,
                'failure_number' => 140,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 1,
                'target_category' => Template::CATEGORY['WBJ'],
                'target_template_id' => 1,
                'target_rule_id' => 1,
                'schedule_task_id' => 1,
                'error_number' => 1,
                'file_name' => 'file_name_parent',

                'status' => Task::STATUS['FAILURE'],
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ],
            [
                'task_id' => 2,
                'crew_id' => 1,
                'user_group_id' => 1,
                'pid' => 1,
                'total' => 60,
                'success_number' => 40,
                'failure_number' => 20,
                'source_category' => Template::CATEGORY['WBJ'],
                'source_template_id' => 1,
                'target_category' => Template::CATEGORY['BJ'],
                'target_template_id' => 1,
                'target_rule_id' => 1,
                'schedule_task_id' => 1,
                'error_number' => 0,
                'file_name' => 'file_name_sub',

                'status' => Task::STATUS['FAILURE'],
                'status_time' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time')
            ]
        ];
    }

    protected function errorData()  : array
    {
        $dataOne = array(
            'ZTMC' => '陕西出租公司',
            'TYSHXYDM' => '91220104MA06FBJY39',
        );

        $dataTwo = array(
            'ZTMC' => '陕西出租分公司',
            'TYSHXYDM' => '91221104MA06FBJY39',
        );

        return
        [
            [
                'error_data_id' => 1,
                'task_id' => 1,
                'category' => Template::CATEGORY['BJ'],
                'template_id' => 1,
                'items_data' => base64_encode(gzcompress(serialize($dataOne))),
                'error_type' => ErrorData::ERROR_TYPE['DUPLICATION_DATA'],
                'error_reason' => json_encode(array('ZTMC'=>array(4))),
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'status' => ErrorData::STATUS['NORMAL']
            ],
            [
                'error_data_id' => 2,
                'task_id' => 2,
                'category' => Template::CATEGORY['BJ'],
                'template_id' => 2,
                'items_data' => base64_encode(gzcompress(serialize($dataTwo))),
                'error_type' => ErrorData::ERROR_TYPE['COMPARISON_FAILED'],
                'error_reason' => json_encode(array('ZTMC'=>array(2))),
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'status' => ErrorData::STATUS['NORMAL']
            ]
        ];
    }
}

<?php
namespace BaseData\Template\CommandHandler\BjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Command\BjTemplate\EditBjTemplateCommand;

use BaseData\UserGroup\Model\UserGroup;

class EditBjTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditBjTemplateCommandHandler::class)
                                     ->setMethods(['getBjTemplateRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetBjTemplateRepository()
    {
        $commandHandler = new MockEditBjTemplateCommandHandler();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\BjTemplateRepository',
            $commandHandler->getBjTemplateRepository()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $bjTemplate = $this->initBjTemplate($command, true);

        $bjTemplateRepository = $this->prophesize(BjTemplateRepository::class);
        $bjTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($bjTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getBjTemplateRepository')
            ->willReturn($bjTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $bjTemplate = $this->initBjTemplate($command, false);

        $bjTemplateRepository = $this->prophesize(BjTemplateRepository::class);
        $bjTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($bjTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getBjTemplateRepository')
            ->willReturn($bjTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new EditBjTemplateCommand(
            $this->faker->word,
            $this->faker->regexify('[A-Z_]{1,100}'),
            $this->faker->randomElements(Template::SUBJECT_CATEGORY, 2),
            $this->faker->randomElement(Template::DIMENSION),
            $this->faker->randomElement(Template::EXCHANGE_FREQUENCY),
            $this->faker->randomElement(Template::INFO_CLASSIFY),
            $this->faker->randomElement(Template::INFO_CATEGORY),
            $this->faker->sentence,
            $this->faker->randomElements(
                array(
                    array(
                        "name" => '统一社会信用代码',    //信息项名称
                        "identify" => 'TYSHXYDM',    //数据标识
                        "type" => Template::TYPE['ZFX'],    //数据类型
                        "length" => '18',    //数据长度
                    )
                ),
                1
            ),
            $this->faker->randomDigit,
            $this->faker->randomDigit,
            $this->faker->randomDigit
        );
        return $command;
    }

    private function initBjTemplate(ICommand $command, bool $result)
    {
        $bjTemplate = $this->prophesize(BjTemplate::class);
        $bjTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $bjTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $bjTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $bjTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $bjTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $bjTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $bjTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);

        $items = $this->commandHandler->setDefaultInfo($command);
        $bjTemplate->setItems(Argument::exact($items))->shouldBeCalledTimes(1);
        $bjTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $bjTemplate->setSourceUnit(new UserGroup($command->sourceUnit))->shouldBeCalledTimes(1);
        $bjTemplate->setGbTemplate(new GbTemplate($command->gbTemplate))->shouldBeCalledTimes(1);
        $bjTemplate->edit()->shouldBeCalledTimes(1)->willReturn($result);

        return $bjTemplate;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class RowQueryFactoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new RowQueryFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullRowQuery()
    {
        $stub = $this->stub->getRowQuery(0);
            $this->assertInstanceOf(
                'BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery',
                $stub
            );
    }

    public function testGetRowQuery()
    {
        foreach (RowQueryFactory::MAPS as $key => $rowQuery) {
            $this->assertInstanceOf(
                $rowQuery,
                $this->stub->getRowQuery($key)
            );
        }
    }
}

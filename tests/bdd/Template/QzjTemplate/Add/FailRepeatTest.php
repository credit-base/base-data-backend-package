<?php
namespace BaseData\Template\QzjTemplate\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要向前置机报送数据时,我需要相关的资源目录对应,以便于我生成规则进行数据的导入,
 *           在目录管理子系统下的前置机资源目录管理中,新增前置机资源目录
 *           根据我需要报送的资源目录数据新增对应的前置机资源目录
 *           以便于实现资源目录数据的报送
 * @Scenario: 异常流程-数据重复
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qzj_template' => $this->qzjTemplate()
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $qzjTemplateArray = $this->qzjTemplate()[0];

        $qzjTemplate = new QzjTemplate();

        $qzjTemplate->getSourceUnit()->setId($qzjTemplateArray['source_unit_id']);
        $qzjTemplate->setName($qzjTemplateArray['name']);
        $qzjTemplate->setIdentify($qzjTemplateArray['identify']);
        $qzjTemplate->setCategory($qzjTemplateArray['category']);
        $qzjTemplate->setDimension($qzjTemplateArray['dimension']);
        $qzjTemplate->setDescription($qzjTemplateArray['description']);
        $qzjTemplate->setInfoClassify($qzjTemplateArray['info_classify']);
        $qzjTemplate->setInfoCategory($qzjTemplateArray['info_category']);
        $qzjTemplate->setItems(json_decode($qzjTemplateArray['items'], true));
        $qzjTemplate->setExchangeFrequency($qzjTemplateArray['exchange_frequency']);
        $qzjTemplate->setSubjectCategory(json_decode($qzjTemplateArray['subject_category'], true));

        return $qzjTemplate->add();
    }

    /**
     * @Then: 期望数据返回失败
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}

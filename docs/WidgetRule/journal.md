# 信用刊物类控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 信用刊物类

* `TITLE`: 标题控件规范
* `SOURCE`: 来源控件规范
* `COVER`: 封面控件规范
* `DESCRIPTION`: 简介控件规范.
* `STATUS`: 状态控件规范.
* `ATTACHMENT`: 附件控件规范
* `AUTH_IMAGES`: 授权图片控件规范
* `YEAR`: 年份控件规范.
* `REJECT_REASON`: 驳回原因控件规范

### `TITLE`
* [标题控件规范(TITLE)](common.md)(必填)

### `SOURCE`
* [来源控件规范(SOURCE)](common.md)(必填)

### `COVER`
* [封面控件规范(IMAGE)](common.md)(必填,因现在文件服务器还没部署好所以该字段先为选填,服务器部署完要改为必填)

### `STATUS`
* [状态控件规范(STATUS)](common.md)(必填)

### `DESCRIPTION`
* [简介控件规范(DESCRIPTION)](common.md)(必填)

### `REJECT_REASON`
* [驳回原因控件规范(DESCRIPTION)](common.md)(必填)

### `ATTACHMENT`

**附件** 控件规范，用于上传附件

#### 规则

* 格式为pdf
* 附件不大于200M
* 数量为1

#### 错误提示

* `ER-0100`,附件不能为空,因现在文件服务器还没部署好所以该字段先为选填,服务器部署完要改为必填
* `ER-0101`:附件格式不正确

### `AUTH_IMAGES`

**授权图片** 控件规范，用于上传授权图片

#### 规则

* 格式为jpg,jpeg,png
* 授权图片不大于2M
* 数量不能大于5
* 选填

#### 错误提示

* `ER-0101`:授权图片格式不正确

### `YEAR`

**年份** 控件规范,用于表述年份

#### 规则

* int 
* 4位数字
* 1901-2155

#### 错误提示

* `ER-0100`, 年份不能为空
* `ER-0101`, 年份格式不正确


<?php
namespace BaseData\Crew\Adapter\Crew;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Utils\MockFactory;

class CrewMockAdapter implements ICrewAdapter
{
    public function fetchOne(int $id) : Crew
    {
        return MockFactory::generateCrew($id);
    }

    public function fetchList(array $ids) : array
    {
        $crewList = array();

        foreach ($ids as $id) {
            $crewList[$id] = MockFactory::generateCrew($id);
        }

        return $crewList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }
}

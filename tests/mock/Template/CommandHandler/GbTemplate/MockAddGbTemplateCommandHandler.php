<?php
namespace BaseData\Template\CommandHandler\GbTemplate;

use BaseData\Template\Model\GbTemplate;

class MockAddGbTemplateCommandHandler extends AddGbTemplateCommandHandler
{
    public function getGbTemplate() : GbTemplate
    {
        return parent::getGbTemplate();
    }
}

<?php
namespace BaseData\Statistical\Adapter\Statistical;

use BaseData\Statistical\Model\Statistical;

interface IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical;
}

<?php
namespace BaseData\ResourceCatalogData\Controller;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;
use Marmot\Interfaces\IView;

class MockErrorDataFetchController extends ErrorDataFetchController
{
    public function getRepository() : IErrorDataAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

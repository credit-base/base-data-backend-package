<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\GbSearchDataUtils;
use BaseData\ResourceCatalogData\Utils\GbSearchDataMockFactory;

class GbSearchDataDbTranslatorTest extends TestCase
{
    use GbSearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbSearchDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullGbSearchData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData(1);

        $expression['gb_search_data_id'] = $gbSearchData->getId();
        $expression['info_classify'] = $gbSearchData->getInfoClassify();
        $expression['info_category'] = $gbSearchData->getInfoCategory();
        $expression['crew_id'] = $gbSearchData->getCrew()->getId();
        $expression['usergroup_id'] = $gbSearchData->getSourceUnit()->getId();
        $expression['subject_category'] = $gbSearchData->getSubjectCategory();
        $expression['dimension'] = $gbSearchData->getDimension();
        $expression['name'] = $gbSearchData->getName();
        $expression['identify'] = $gbSearchData->getIdentify();
        $expression['expiration_date'] = $gbSearchData->getExpirationDate();
        $expression['gb_template_id'] = $gbSearchData->getTemplate()->getId();
        $expression['gb_items_data_id'] = $gbSearchData->getItemsData()->getId();
        $expression['status'] = $gbSearchData->getStatus();
        $expression['hash'] = $gbSearchData->getHash();
        $expression['status_time'] = $gbSearchData->getStatusTime();
        $expression['create_time'] = $gbSearchData->getCreateTime();
        $expression['update_time'] = $gbSearchData->getUpdateTime();
        $expression['task_id'] = $gbSearchData->getTask()->getId();
        $expression['description'] = $gbSearchData->getDescription();
        $expression['front_end_processor_status'] = $gbSearchData->getFrontEndProcessorStatus();

        $gbSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\GbSearchData', $gbSearchData);
        $this->compareArrayAndObject($expression, $gbSearchData);
    }

    public function testObjectToArray()
    {
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData(1);

        $expression = $this->translator->objectToArray($gbSearchData);

        $this->compareArrayAndObject($expression, $gbSearchData);
    }
}

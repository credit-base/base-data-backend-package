<?php
namespace BaseData\ResourceCatalogData\BaseSearchData\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\Repository\BaseTemplateRepository;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\BaseItemsData;
use BaseData\ResourceCatalogData\Model\BaseSearchData;

use BaseData\Enterprise\Repository\EnterpriseRepository;
use BaseData\Enterprise\Translator\EnterpriseDbTranslator;
use BaseData\Enterprise\SetDataTrait as EnterpriseSetDataTrait;

/**
 * @Feature: 我是开发人员,当委办局工作人员上传资源目录数据后,在资源目录数据子系统下的基础数据管理,开发定时任务新增基础数据
 *           根据委办局数据库的资源目录数据通过基础规则验证新增基础数据
 *           以便于我实现资源目录数据的报送和平台资源目录数据的呈现
 * @Scenario: 正常新增资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, EnterpriseSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
        $this->clear('pcore_enterprise');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_base_template' => $this->baseTemplate()
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $templateRepository = new BaseTemplateRepository();
        $baseTemplate = $templateRepository->fetchOne(1);

        $baseItemsDataArray = $this->baseSearchData()['pcore_base_items_data'][0];
        $baseSearchDataArray = $this->baseSearchData()['pcore_base_search_data'][0];

        $baseItemsData = new BaseItemsData();
        $baseItemsData->setData(unserialize(gzuncompress(base64_decode($baseItemsDataArray['data'], true))));

        $baseSearchData = new BaseSearchData();
        $baseSearchData->setTemplate($baseTemplate);
        $baseSearchData->setItemsData($baseItemsData);
        $baseSearchData->setHash($baseSearchDataArray['hash']);
        $baseSearchData->setStatus(BaseSearchData::STATUS['ENABLED']);
        $baseSearchData->setDimension($baseSearchDataArray['dimension']);
        $baseSearchData->getCrew()->setId($baseSearchDataArray['crew_id']);
        $baseSearchData->setExpirationDate($baseSearchDataArray['expiration_date']);
        $baseSearchData->getSourceUnit()->setId($baseSearchDataArray['usergroup_id']);
        $baseSearchData->setSubjectCategory($baseSearchDataArray['subject_category']);
        
        return $baseSearchData->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setEnterprise = $this->enterprise()[0];
        $setEnterprise['enterprise_id'] = 1;
        
        $repository = new EnterpriseRepository();

        $enterprise = $repository->fetchOne($result);

        $translator = new EnterpriseDbTranslator();
        $enterpriseArray = $translator->objectToArray($enterprise);

        $this->assertEquals($enterpriseArray['enterprise_id'], $setEnterprise['enterprise_id']);
        $this->assertEquals($enterpriseArray['name'], $setEnterprise['name']);
        $this->assertEquals(
            $enterpriseArray['unified_social_credit_code'],
            $setEnterprise['unified_social_credit_code']
        );
    }
}

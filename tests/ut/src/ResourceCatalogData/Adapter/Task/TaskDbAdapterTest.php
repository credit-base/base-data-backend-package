<?php
namespace BaseData\ResourceCatalogData\Adapter\Task;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;
use BaseData\ResourceCatalogData\Translator\TaskDbTranslator;
use BaseData\ResourceCatalogData\Adapter\Task\Query\TaskRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class TaskDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(TaskDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 ITaskAdapter
     */
    public function testImplementsITaskAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 TaskDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\TaskDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 TaskRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\Task\Query\TaskRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockTaskDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedTask = new Task();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedTask)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedTask);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedTask = new Task();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedTask, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedTask, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedTask = new Task();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedTask);

        //验证
        $task = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedTask, $task);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $taskOne = new Task(1);
        $taskTwo = new Task(2);

        $ids = [1, 2];

        $expectedTaskList = [];
        $expectedTaskList[$taskOne->getId()] = $taskOne;
        $expectedTaskList[$taskTwo->getId()] = $taskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedTaskList);

        //验证
        $taskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedTaskList, $taskList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterCrew()
    {
        $filter = array(
            'crew' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'crew_id = '.$filter['crew'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterUserGroup()
    {
        $filter = array(
            'userGroup' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'user_group_id = '.$filter['userGroup'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterPid()
    {
        $filter = array(
            'pid' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'pid = '.$filter['pid'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterSourceCategory()
    {
        $filter = array(
            'sourceCategory' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'source_category = '.$filter['sourceCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterSourceTemplate()
    {
        $filter = array(
            'sourceTemplate' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'source_template_id = '.$filter['sourceTemplate'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterTargetCategory()
    {
        $filter = array(
            'targetCategory' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'target_category = '.$filter['targetCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterTargetTemplate()
    {
        $filter = array(
            'targetTemplate' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'target_template_id = '.$filter['targetTemplate'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterTargetRule()
    {
        $filter = array(
            'targetRule' => 1
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'target_rule_id = '.$filter['targetRule'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => 0
        );
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = 'status IN ('.$filter['status'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortCreateTimeDesc()
    {
        $sort = array('createTime' => -1);
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = ' ORDER BY create_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortCreateTimeAsc()
    {
        $sort = array('createTime' => 1);
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = ' ORDER BY create_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class QzjTemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\Template\Command\QzjTemplate\AddQzjTemplateCommand' =>
        'BaseData\Template\CommandHandler\QzjTemplate\AddQzjTemplateCommandHandler',
        'BaseData\Template\Command\QzjTemplate\EditQzjTemplateCommand' =>
        'BaseData\Template\CommandHandler\QzjTemplate\EditQzjTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

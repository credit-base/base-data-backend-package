<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\YsSearchDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\YsSearchDataMockAdapter;

class YsSearchDataRepository extends Repository implements IYsSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new YsSearchDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IYsSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IYsSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IYsSearchDataAdapter
    {
        return new YsSearchDataMockAdapter();
    }

    public function fetchOne($id) : YsSearchData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(YsSearchData $ysSearchData) : bool
    {
        return $this->getAdapter()->add($ysSearchData);
    }

    public function edit(YsSearchData $ysSearchData, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($ysSearchData, $keys);
    }
}

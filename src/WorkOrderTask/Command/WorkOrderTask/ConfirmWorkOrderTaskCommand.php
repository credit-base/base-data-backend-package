<?php
namespace BaseData\WorkOrderTask\Command\WorkOrderTask;

use Marmot\Interfaces\ICommand;

class ConfirmWorkOrderTaskCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}

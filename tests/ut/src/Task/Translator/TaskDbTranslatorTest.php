<?php
namespace BaseData\Task\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Task\Utils\TaskUtils;

class TaskDbTranslatorTest extends TestCase
{
    use TaskUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new TaskDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Task\Model\NullTask', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObjectClassNotExist()
    {
        $expression['identify'] =  'identify';
        $expression['task_id'] = 1;

        $result = $this->translator->arrayToObject($expression);
        $this->assertInstanceOf('BaseData\Task\Model\NullTask', $result);
    }

    public function testArrayToObject()
    {
        $task = \BaseData\Task\Utils\MockFactory::generateTask(1);

        $expression['task_id'] = $task->getId();
        $expression['parent_id'] = $task->getParentId();
        $expression['identify'] =  marmot_encode($task->getIdentify());
        $expression['type'] = $task->getType();
        $expression['data'] = serialize($task->getData());
        $expression['nice'] = $task->getNice();
        $expression['pid'] = $task->getPid();
        $expression['next_schedule_time'] = $task->getNextScheduleTime();

        $expression['status'] = $task->getStatus();
        $expression['status_time'] = $task->getStatusTime();
        $expression['create_time'] = $task->getCreateTime();
        $expression['update_time'] = $task->getUpdateTime();

        $task = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Task\Model\Task', $task);
        $this->compareArrayAndObject($expression, $task);
    }

    public function testObjectToArray()
    {
        $task = \BaseData\Task\Utils\MockFactory::generateTask(1);

        $expression = $this->translator->objectToArray($task);

        $this->compareArrayAndObject($expression, $task);
    }
}

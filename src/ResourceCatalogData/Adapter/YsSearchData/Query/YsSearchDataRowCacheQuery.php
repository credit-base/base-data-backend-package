<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class YsSearchDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'ys_search_data_id',
            new Persistence\YsSearchDataCache(),
            new Persistence\YsSearchDataDb()
        );
    }
}

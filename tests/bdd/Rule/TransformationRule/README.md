# 转换规则BDD测试

## 测试用例

* 完全转换成功
* 非完全转换成功

### 1. 完全转换成功

#### 1.1 场景说明

设定转换规则，从委办局的行政许可信息到国标表的行政许可信息，所有<font color="red">映射项可以全部对应</font>.

#### 1.2 数据准备

#### 1.2.1 规则

委办局行政许可转换<font color="red">完全转换</font>国标行政许可

```php
$rules = ['transformationRule'=>[]];
//转换规则
$rules['transformationRule'] = [
    "ZTMC" => "ZTMC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "TYSHXYDM" => "TYSHXYDM",//2
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    "BZ" => "BZ"//27
];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    );

//测试结果-GbSearchData
$transformationSearchData = array(
    'infoClassify' => 1,
    'infoCategory' => 1,
    'crewId' => 1,
    'sourceUnitId' => 1,
    'subjectCategory' => 1,
    'dimension' => 1,
    'name' => '濮阳市嘉业运输有限公司',
    'identify' => '91410900MA46184E7Q',
    'expirationDate' => 20991231,
    'hash' => 'f7008574848bca7580350fbcc913ddf6',
    'templateId' => 1,
    'itemsData' => array(
        'ZTMC' => "濮阳市嘉业运输有限公司",
        'XK_XDR_LB' => "法人及非法人组织",
        'TYSHXYDM' => "91410900MA46184E7Q",
        'XK_XDR_GSZC' => "91410900MA46184E7Q",
        'XK_XDR_ZZJG' => "91410900MA46184E7Q",
        'XK_XDR_SWDJ' => "91410900MA46184E7Q",
        'XK_XDR_SYDW' => "91410900MA46184E7Q",
        'XK_XDR_SHZZ' => "MA46184E-7",
        'XK_FRDB' => "张晓苹",
        'XK_FR_ZJLX' => "身份证号",
        'XK_FR_ZJHM' => "412825199809097654",
        'XK_XDR_ZJLX' => "身份证号",
        'XK_XDR_ZJHM' => "412825199809097654",
        'XK_XKWS' => "高速公路联网管理中心交通行政许可决定书",
        'XK_WSH' => "豫高速公路许 字第20210705000090号",
        'XK_XKLB' => "普通",
        'XK_XKZS' => "高速公路超限运输车辆通行证",
        'XK_XKBH' => "410000017210705A000090",
        'XK_NR' => "高速公路超限运输车辆通行证",
        'XK_JDRQ' => "20210714",
        'XK_YXQZ' => "20210714",
        'XK_YXQZI' => "20210807",
        'XK_XKJG' => "河南省高速公路联网管理中心",
        'XK_XKJGDM' => "12410000MB14527979",
        'XK_ZT' => "1",
        'XK_LYDW' => "河南省高速公路联网管理中心",
        'XK_LYDWDM' => "12410000MB14527979",
        'BZ' => ""
    ),
    'statusTime' => 0,
    'status' => 0,
    'createTime' => '1626506618',
    'updateTime' => '1626506618'
);
```

##### 1.2.2 委办局行政许可信息

**默认数据**

### 1.3 返回

返回<font color="red">完整</font>数据

### 2. 非完全转换成功

#### 2.1 背景说明

委办局行政许可转换<font color="red">不完全转换</font>国标行政许可

去掉统一社会信用代码映射`TYSHXYDM `

#### 2.2.1 规则

```php
$rules = ['transformationRule'=>[]];
//转换规则
$rules['transformationRule'] = [
    "ZTMC" => "ZTMC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    "BZ" => "BZ"//27
];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    );

//测试结果-IncompleteSearchData
array(
	'errorType'=>1,
	'errorReason' => '行政相对人代码_1(统一社会信用代码),行政相对人代码_3(工商登记码)转换失败',
	'type'=>2,
	'infoClassify' => 1,
	'infoCategory' => 1,
	'crewId' => 1,
	'sourceUnitId' => 1,
	'subjectCategory' => 1,
	'dimension' => 1,
	'name' => '濮阳市嘉业运输有限公司',
	'identify' => '91410900MA46184E7Q',
	'expirationDate' => 20991231,
	'hash' => '2b6fad2dee00211363461bcfa874486f',
	'templateId' => 1,
	'itemsData' => array(
		'ZTMC' => "濮阳市嘉业运输有限公司",
		'XK_XDR_LB' => "法人及非法人组织",
		'TYSHXYDM' => "",
		'XK_XDR_GSZC' => "",
		'XK_XDR_ZZJG' => "91410900MA46184E7Q",
		'XK_XDR_SWDJ' => "91410900MA46184E7Q",
		'XK_XDR_SYDW' => "91410900MA46184E7Q",
		'XK_XDR_SHZZ' => "MA46184E-7",
		'XK_FRDB' => "张晓苹",
		'XK_FR_ZJLX' => "身份证号",
		'XK_FR_ZJHM' => "412825199809097654",
		'XK_XDR_ZJLX' => "身份证号",
		'XK_XDR_ZJHM' => "412825199809097654",
		'XK_XKWS' => "高速公路联网管理中心交通行政许可决定书",
		'XK_WSH' => "豫高速公路许 字第20210705000090号",
		'XK_XKLB' => "普通",
		'XK_XKZS' => "高速公路超限运输车辆通行证",
		'XK_XKBH' => "410000017210705A000090",
		'XK_NR' => "高速公路超限运输车辆通行证",
		'XK_JDRQ' => "20210714",
		'XK_YXQZ' => "20210714",
		'XK_YXQZI' => "20210807",
		'XK_XKJG' => "河南省高速公路联网管理中心",
		'XK_XKJGDM' => "12410000MB14527979",
		'XK_ZT' => "1",
		'XK_LYDW' => "河南省高速公路联网管理中心",
		'XK_LYDWDM' => "12410000MB14527979",
		'BZ' => ""
	),
	'statusTime' => 0,
	'status' => 0,
	'createTime' => '1626506618',
	'updateTime' => '1626506618'
);
```

##### 2.2.2 委办局行政许可信息

**默认数据**

### 2.3 返回

返回<font color="red">不完整</font>数据
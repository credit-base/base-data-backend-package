<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Repository\UserGroupRepository;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;

trait QzjTemplateCommandHandlerTrait
{
    protected function getUserGroupRepository() : UserGroupRepository
    {
        return new UserGroupRepository();
    }

    protected function fetchUserGroup(int $id) : UserGroup
    {
        return $this->getUserGroupRepository()->fetchOne($id);
    }

    protected function getQzjTemplate() : QzjTemplate
    {
        return new QzjTemplate();
    }

    protected function getQzjTemplateRepository() : QzjTemplateRepository
    {
        return new QzjTemplateRepository();
    }

    protected function fetchQzjTemplate(int $id) : QzjTemplate
    {
        return $this->getQzjTemplateRepository()->fetchOne($id);
    }
}

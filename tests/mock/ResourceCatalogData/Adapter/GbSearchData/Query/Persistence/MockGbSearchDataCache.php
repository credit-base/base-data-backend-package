<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\Persistence;

class MockGbSearchDataCache extends GbSearchDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

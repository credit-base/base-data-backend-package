<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Core;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;

use BaseData\Crew\Model\Crew;
use BaseData\UserGroup\Model\UserGroup;

class FeedbackWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(FeedbackWorkOrderTaskCommandHandler::class)
                                     ->setMethods([
                                         'getWorkOrderTaskRepository',
                                         'fetchOneCrew',
                                         'fetchOneUserGroup',
                                         'fetchOneWbjTemplate'
                                    ])->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetWorkOrderTaskRepository()
    {
        $commandHandler = new MockFeedbackWorkOrderTaskCommandHandler();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository',
            $commandHandler->getWorkOrderTaskRepository()
        );
    }

    public function testExecute()
    {
        $feedbackRecords = array(
            array(
                'crew' => 1,
                'userGroup' => 1,
                'templateId' => 1
            )
        );

        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew($feedbackRecords[0]['crew']);
        $userGroup = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup($feedbackRecords[0]['userGroup']);
        $wbjTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(
            $feedbackRecords[0]['templateId']
        );

        $command = new FeedbackWorkOrderTaskCommand(
            $feedbackRecords,
            $this->faker->randomNumber()
        );
        $command->feedbackRecords[0]['crewName'] = $crew->getRealName();
        $command->feedbackRecords[0]['userGroupName'] = $userGroup->getName();
        $command->feedbackRecords[0]['feedbackTime'] = Core::$container->get('time');
        $command->feedbackRecords[0]['items'] = $wbjTemplate->getItems();
        
        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->feedback()->shouldBeCalledTimes(1)->willReturn(true);
        $workOrderTask->getFeedbackRecords()->shouldBeCalledTimes(1)->willReturn(array());
        $workOrderTask->setFeedbackRecords(Argument::exact($command->feedbackRecords))->shouldBeCalledTimes(1);

        $workOrderTaskRepository = $this->prophesize(WorkOrderTaskRepository::class);
        $workOrderTaskRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($workOrderTask->reveal());

        $this->commandHandler->expects($this->any())
            ->method('getWorkOrderTaskRepository')
            ->willReturn($workOrderTaskRepository->reveal());

        $this->commandHandler->expects($this->exactly(1))
                ->method('fetchOneCrew')
                ->with($command->feedbackRecords[0]['crew'])
                ->willReturn($crew);

        $this->commandHandler->expects($this->exactly(1))
                ->method('fetchOneUserGroup')
                ->with($command->feedbackRecords[0]['userGroup'])
                ->willReturn($userGroup);

        $this->commandHandler->expects($this->exactly(1))
                ->method('fetchOneWbjTemplate')
                ->with($command->feedbackRecords[0]['templateId'])
                ->willReturn($wbjTemplate);

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Common\Model;

class MockNullResubmitAbleTrait
{
    use NullResubmitAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }
}

<?php
namespace BaseData\Statistical\Adapter\Statistical;

use Marmot\Interfaces\INull;

use BaseData\Statistical\Model\Statistical;
use BaseData\Statistical\Model\NullStatistical;

class NullStatisticalAdapter implements INull, IStatisticalAdapter
{
    public function analyse(array $filter = array()) : Statistical
    {
        return NullStatistical::getInstance();
    }
}

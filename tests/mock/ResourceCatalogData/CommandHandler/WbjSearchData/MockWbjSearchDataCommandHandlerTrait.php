<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

class MockWbjSearchDataCommandHandlerTrait
{
    use WbjSearchDataCommandHandlerTrait;

    public function publicGetRepository() : WbjSearchDataRepository
    {
        return $this->getRepository();
    }

    public function publicFetchWbjSearchData(int $id) : WbjSearchData
    {
        return $this->fetchWbjSearchData($id);
    }
}

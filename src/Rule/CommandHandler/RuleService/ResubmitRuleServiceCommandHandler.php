<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Rule\Command\RuleService\ResubmitRuleServiceCommand;

class ResubmitRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof ResubmitRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $unAuditedRuleService = $this->fetchUnAuditedRuleService($command->id);

        $unAuditedRuleService = $this->generateUnAuditedRuleServiceCrew($command->crew, $unAuditedRuleService);
        $unAuditedRuleService = $this->generateUnAuditedRuleServiceRules($command->rules, $unAuditedRuleService);

        $applyInfo = $this->applyInfo($unAuditedRuleService);
        $unAuditedRuleService->setApplyInfo($applyInfo);

        return $unAuditedRuleService->resubmit();
    }
}

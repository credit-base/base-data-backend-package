# 通用错误（100-500）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

**status**

状态码


---

* `ER-100`: 数据为空
* `ER-101`: 数据格式不正确
* `ER-102`: 资源不能操作
* `ER-103`: 资源已存在
* `ER-104`: 数据不正确
* `ER-105` - `ER-500`: 预留

---

### <a name="ER-100">ER-100</a>

**id**

`100`

**code**

`PARAMETER_IS_EMPTY`

**title**

数据为空

**detail**

表述传输了一个空的数据

**links**

待补充

**status**

403

### <a name="ER-101">ER-101</a>

**id**

`101`

**code**

`PARAMETER_FORMAT_INCORRECT`

**title**

数据格式不正确

**detail**

表述传输了一个格式不正确的字段

**links**

待补充

**status**

403

### <a name="ER-102">ER-102</a>

**id**

`102`

**code**

`RESOURCE_CAN_NOT_MODIFY`

**title**

资源不能操作

**detail**

表述不能操作一个不具备操作权限的资源

**links**

待补充

**status**

403

### <a name="ER-103">ER-103</a>

**id**

`103`

**code**

`RESOURCE_ALREADY_EXIST`

**title**

资源已存在

**detail**

表述传输了一个已存在的资源

**links**

待补充

**status**

403

### <a name="ER-104">ER-104</a>

**id**

`104`

**code**

`PARAMETER_INCORRECT`

**title**

数据不正确

**detail**

表述传输了一个不正确的字段

**links**

待补充

**status**

403

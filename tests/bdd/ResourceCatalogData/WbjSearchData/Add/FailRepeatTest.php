<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\WbjItemsData;
use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

/**
 * @Feature: 我是开发人员,当委办局工作人员上传资源目录数据后,在资源目录数据子系统下的委办局数据管理,开发定时任务新增委办局数据
 *           根据委办局工作人员上传到原始数据库的资源目录数据通过资源目录模板格式验证新增委办局数据
 *           以便于我实现资源目录数据的报送和平台资源目录数据的呈现
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_wbj_items_data');
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data'],
            'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data']
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回false
     */
    public function add()
    {
        $templateRepository = new WbjTemplateRepository();
        $wbjTemplate = $templateRepository->fetchOne(1);

        $wbjItemsDataArray = $this->wbjSearchData()['pcore_wbj_items_data'][0];
        $wbjSearchDataArray = $this->wbjSearchData()['pcore_wbj_search_data'][0];

        $wbjItemsData = new WbjItemsData();
        $wbjItemsData->setData(unserialize(gzuncompress(base64_decode($wbjItemsDataArray['data'], true))));

        $wbjSearchData = new WbjSearchData();

        $wbjSearchData->getCrew()->setId($wbjSearchDataArray['crew_id']);
        $wbjSearchData->getSourceUnit()->setId($wbjSearchDataArray['usergroup_id']);
        $wbjSearchData->setSubjectCategory($wbjSearchDataArray['subject_category']);
        $wbjSearchData->setDimension($wbjSearchDataArray['dimension']);
        $wbjSearchData->setExpirationDate($wbjSearchDataArray['expiration_date']);
        $wbjSearchData->setTemplate($wbjTemplate);
        $wbjSearchData->setItemsData($wbjItemsData);
        $wbjSearchData->setHash($wbjSearchDataArray['hash']);
        
        return $wbjSearchData->add();
    }

    /**
     * @Then: 数据已经存在
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}

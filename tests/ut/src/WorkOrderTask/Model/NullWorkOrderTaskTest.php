<?php
namespace BaseData\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

class NullWorkOrderTaskTest extends TestCase
{
    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTask = NullWorkOrderTask::getInstance();
    }

    public function tearDown()
    {
        unset($this->workOrderTask);
    }

    public function testExtendsWorkOrderTask()
    {
        $this->assertInstanceof('BaseData\WorkOrderTask\Model\WorkOrderTask', $this->workOrderTask);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->workOrderTask);
    }
}

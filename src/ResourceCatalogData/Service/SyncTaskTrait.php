<?php
namespace BaseData\ResourceCatalogData\Service;

use BaseData\Rule\Model\RuleTrait;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RuleServiceRepository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;

use BaseData\Template\Model\Template;

trait SyncTaskTrait
{
    use RuleTrait;

    protected function getErrorDataRepository() : ErrorDataRepository
    {
        return new ErrorDataRepository();
    }
    
    protected function getRuleServiceRepository() : RuleServiceRepository
    {
        return new RuleServiceRepository();
    }

    /**
     * 将来源资源目录数据转换为目标资源目录,并新增,如果新增失败则入到失败库
     * @param array $sourceSearchDataList 来源资源目录数据
     * @param RuleService $ruleService 规则信息,通过规则进行数据转换
     * @param Task $task 任务信息
     * @return 没有返回值
     */
    protected function searchDataTransformationAndAdd(
        array $sourceSearchDataList,
        RuleService $ruleService,
        Task $task
    ) : void {
        foreach ($sourceSearchDataList as $sourceSearchData) {
            $transformationSearchData = $ruleService->transformation($sourceSearchData);

            $transformationSearchData->setTask($task);

            if (!$transformationSearchData->add()) {
                if (!$transformationSearchData instanceof ErrorData) {
                    $errorData = $this->failureDataException(
                        $transformationSearchData,
                        ErrorData::STATUS['STORAGE_EXCEPTION']
                    );

                    $errorData->setTask($task);
                    $errorData->getItemsData()->setData($transformationSearchData->getItemsData()->getData());
                    $errorData->generateHash();

                    $errorData->add();
                }
            }
        }
    }

    /**
     * 获取成功数,通过目标id查询入目标库的数量
     * @param Task $task 任务信息,搜索条件
     * @param RuleService $ruleService 规则信息,获取规则信息获取目标类型,通过目标类型获取目标资源目录数据仓库
     * @return int 返回成功数
     */
    protected function successNumber(Task $task, RuleService $ruleService) : int
    {
        $transformationCategory = $ruleService->getTransformationCategory();
        $repository = $this->getSearchDataRepositoryFactory()->getRepository($transformationCategory);
        $filter['task'] = $task->getId();
        list($list, $count) = $repository->filter($filter);
        unset($list);
        
        return $count;
    }

    /**
     * 获取失败数,通过目标id查询入失败库的数量
     * @param Task $task 任务信息,搜索条件
     * @return int 返回失败数
     */
    protected function failureNumber(Task $task) : int
    {
        $filter['task'] = $task->getId();
        list($list, $count) = $this->getErrorDataRepository()->filter($filter);
        unset($list);
        
        return $count;
    }

    /**
     * 修改任务信息,数据入库后,需要修改任务的成功数,失败数,任务状态
     * @param Task $task 任务信息
     * @param RuleService $ruleService 规则信息,通过规则获取成功数
     * @return bool
     */
    protected function updateTask(Task $task, RuleService $ruleService) : bool
    {
        $successNumber = $this->successNumber($task, $ruleService);
        $failureNumber = $this->failureNumber($task);
        $status = empty($failureNumber) ? Task::STATUS['SUCCESS'] : Task::STATUS['FAILURE'];

        $task->setSuccessNumber($successNumber);
        $task->setFailureNumber($failureNumber);
        $task->setStatus($status);

        return $task->edit();
    }
    
    //根据来源资源目录获取其下的转换规则
    protected function fetchRuleServiceList(Template $template) : array
    {
        $filter['sourceTemplateIds'] = $template->getId();
        $filter['sourceCategory'] = $template->getCategory();
        $filter['status'] = RuleService::STATUS['NORMAL'];

        list($list, $count) = $this->getRuleServiceRepository()->filter($filter);
        unset($count);

        return $list;
    }
}

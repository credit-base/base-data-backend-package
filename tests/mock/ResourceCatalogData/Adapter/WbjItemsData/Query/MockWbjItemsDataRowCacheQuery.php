<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query;

use Marmot\Framework\Interfaces\DbLayer;
use Marmot\Interfaces\CacheLayer;

class MockWbjItemsDataRowCacheQuery extends WbjItemsDataRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

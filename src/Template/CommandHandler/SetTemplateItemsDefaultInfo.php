<?php
namespace BaseData\Template\CommandHandler;

use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Strategy\DateStrategy;
use BaseData\ResourceCatalogData\Strategy\FloatStrategy;

trait SetTemplateItemsDefaultInfo
{
    public function setDefaultInfo(ICommand $command) : array
    {
        $items = $this->removeLastFiveItems($command->items);
        $itemsDefaultLength = $this->setDefaultLength($items);
        $itemsResult = $this->setDefaultLastFiveItems($command, $itemsDefaultLength);

        return $itemsResult;
    }

    protected function removeLastFiveItems(array $items) : array
    {
        $itemsResult = array();
        foreach ($items as $value) {
            if (!in_array($value['identify'], Template::DEFAULT_LAST_ITEMS_IDENTIFY)) {
                $itemsResult[] = $value;
            }
        }
        return $itemsResult;
    }

    protected function setDefaultLength(array $items) : array
    {
        foreach ($items as $item) {
            if ($item['type'] == Template::TYPE['RQX']) {
                $item['length'] = DateStrategy::DEFAULT_LENGTH;
            }
            if ($item['type'] == Template::TYPE['FDX']) {
                $item['length'] = FloatStrategy::DEFAULT_LENGTH;
            }

            $itemsResult[] = $item;
        }
        return $itemsResult;
    }

    protected function setDefaultLastFiveItems(ICommand $command, array $items) : array
    {
        foreach (Template::DEFAULT_LAST_ITEMS as $value) {
            if ($value['identify'] == Template::DEFAULT_LAST_ITEMS_IDENTIFY['ZTLB']) {
                foreach ($command->subjectCategory as $subjectCategory) {
                    $subjectCategoryOptions[] = Template::SUBJECT_CATEGORY_CN[$subjectCategory];
                }
                $value['options'] = $subjectCategoryOptions;
            }
            if ($value['identify'] == Template::DEFAULT_LAST_ITEMS_IDENTIFY['GKFW']) {
                $value['options'] = array(Template::DIMENSION_CN[$command->dimension]);
            }
            if ($value['identify'] == Template::DEFAULT_LAST_ITEMS_IDENTIFY['GXPL']) {
                $value['options'] = array(Template::EXCHANGE_FREQUENCY_CN[$command->exchangeFrequency]);
            }
            if ($value['identify'] == Template::DEFAULT_LAST_ITEMS_IDENTIFY['XXFL']) {
                $value['options'] = array(Template::INFO_CLASSIFY_CN[$command->infoClassify]);
            }
            if ($value['identify'] == Template::DEFAULT_LAST_ITEMS_IDENTIFY['XXLB']) {
                $value['options'] = array(Template::INFO_CATEGORY_CN[$command->infoCategory]);
            }

            $itemsResult[] = $value;
        }
        return array_merge($items, $itemsResult);
    }
}

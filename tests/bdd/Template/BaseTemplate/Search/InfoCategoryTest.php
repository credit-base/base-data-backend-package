<?php
namespace BaseData\Template\BaseTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Translator\BaseTemplateDbTranslator;

/**
 * @Feature: 我是超级管理员,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 通过资源信息类别搜索
 */
class InfoCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBaseTemplateList()
    {
        $repository = new BaseTemplateRepository();
 
        $filter['infoCategory'] = Template::INFO_CATEGORY['QTXX'];

        list($baseTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $baseTemplateList;
    }

    /**
     * @Then: 我可以看到信息分类为其他信息的所有资源目录
     */
    public function testViewBaseTemplateList()
    {
        $setBaseTemplateList = $this->baseTemplate();

        foreach ($setBaseTemplateList as $key => $setBaseTemplate) {
            $setBaseTemplateList[$key]['base_template_id'] = $key +1;
            $setBaseTemplateList[$key]['subject_category'] = json_decode($setBaseTemplate['subject_category'], true);
            $setBaseTemplateList[$key]['items'] = json_decode($setBaseTemplate['items'], true);
        }

        $baseTemplateList = $this->fetchBaseTemplateList();
        $translator = new BaseTemplateDbTranslator();
        foreach ($baseTemplateList as $baseTemplate) {
            $baseTemplateArray[] = $translator->objectToArray($baseTemplate);
        }

        $this->assertEquals($baseTemplateArray, $setBaseTemplateList);

        foreach ($baseTemplateArray as $baseTemplate) {
            $this->assertEquals(Template::INFO_CATEGORY['QTXX'], $baseTemplate['info_category']);
        }
    }
}

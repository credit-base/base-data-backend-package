<?php
namespace BaseData\ResourceCatalogData\Translator;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\NullGbSearchData;

class GbSearchDataDbTranslator extends SearchDataDbTranslator
{
    public function arrayToObject(array $expression, $gbSearchData = null)
    {
        if (!isset($expression['gb_search_data_id'])) {
            return NullGbSearchData::getInstance();
        }

        if ($gbSearchData == null) {
            $gbSearchData = new GbSearchData();
        }

        $gbSearchData->setId($expression['gb_search_data_id']);

        if (isset($expression['gb_template_id'])) {
            $gbSearchData->getTemplate()->setId($expression['gb_template_id']);
        }
        if (isset($expression['gb_items_data_id'])) {
            $gbSearchData->getItemsData()->setId($expression['gb_items_data_id']);
        }
        if (isset($expression['description'])) {
            $gbSearchData->setDescription($expression['description']);
        }
        if (isset($expression['front_end_processor_status'])) {
            $gbSearchData->setFrontEndProcessorStatus($expression['front_end_processor_status']);
        }

        $gbSearchData = parent::arrayToObject($expression, $gbSearchData);
        
        return $gbSearchData;
    }

    public function objectToArray($gbSearchData, array $keys = array())
    {
        $searchDataExpression = parent::objectToArray($gbSearchData, $keys);

        if (!$gbSearchData instanceof GbSearchData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'template',
                'itemsData',
                'description',
                'frontEndProcessorStatus'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['gb_search_data_id'] = $gbSearchData->getId();
        }
        if (in_array('template', $keys)) {
            $expression['gb_template_id'] = $gbSearchData->getTemplate()->getId();
        }
        if (in_array('itemsData', $keys)) {
            $expression['gb_items_data_id'] = $gbSearchData->getItemsData()->getId();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $gbSearchData->getDescription();
        }
        if (in_array('frontEndProcessorStatus', $keys)) {
            $expression['front_end_processor_status'] = $gbSearchData->getFrontEndProcessorStatus();
        }

        $expression = array_merge($expression, $searchDataExpression);

        return $expression;
    }
}

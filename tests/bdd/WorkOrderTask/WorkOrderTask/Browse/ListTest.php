<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Translator\WorkOrderTaskDbTranslator;

use BaseData\Template\SetDataTrait as TemplateSetDataTrait;

/**
 * @Feature: 我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务,
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 查看工单任务列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, TemplateSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_work_order_task');
    }

    /**
     * @Given: 存在工单任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_parent_task' => $this->parentTask(),
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_work_order_task' => $this->workOrderTask()
        ]);
    }

    /**
     * @When: 当我查看工单任务列表时
     */
    public function fetchWorkOrderTaskList()
    {
        $repository = new WorkOrderTaskRepository();

        list($workOrderTaskList, $count) = $repository->filter([]);
        
        unset($count);

        return $workOrderTaskList;
    }

    /**
     * @Then: 我可以看到工单任务的全部信息
     */
    public function testViewWorkOrderTaskList()
    {
        $setWorkOrderTaskList = $this->workOrderTask();

        foreach ($setWorkOrderTaskList as $key => $setWorkOrderTask) {
            $setWorkOrderTaskList[$key]['work_order_task_id'] = $key +1;
            $setWorkOrderTaskList[$key]['feedback_records'] = json_decode($setWorkOrderTask['feedback_records'], true);
        }

        $workOrderTaskList = $this->fetchWorkOrderTaskList();

        $translator = new WorkOrderTaskDbTranslator();
        foreach ($workOrderTaskList as $workOrderTask) {
            $workOrderTaskArray[] = $translator->objectToArray($workOrderTask);
        }

        $this->assertEquals($workOrderTaskArray, $setWorkOrderTaskList);
    }
}

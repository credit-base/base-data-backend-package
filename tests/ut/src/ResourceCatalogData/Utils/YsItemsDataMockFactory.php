<?php
namespace BaseData\ResourceCatalogData\Utils;

use BaseData\ResourceCatalogData\Model\YsItemsData;

class YsItemsDataMockFactory
{
    public static function generateYsItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : YsItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $ysItemsData = new YsItemsData($id);
        $ysItemsData->setId($id);

        self::generateData($ysItemsData, $faker, $value);

        return $ysItemsData;
    }

    private static function generateData($ysItemsData, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            $faker->words(3, false);
        
        $ysItemsData->setData($data);
    }
}

<?php
namespace BaseData\Template\CommandHandler\WbjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class WbjTemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\Template\Command\WbjTemplate\AddWbjTemplateCommand' =>
        'BaseData\Template\CommandHandler\WbjTemplate\AddWbjTemplateCommandHandler',
        'BaseData\Template\Command\WbjTemplate\EditWbjTemplateCommand' =>
        'BaseData\Template\CommandHandler\WbjTemplate\EditWbjTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

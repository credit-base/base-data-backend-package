<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ErrorDataDb extends Db
{
    const TABLE = 'error_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

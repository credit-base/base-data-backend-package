<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullBaseItemsDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullBaseItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsBaseItemsData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\BaseItemsData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

<?php
namespace BaseData\ResourceCatalogData\ErrorData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;
use BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要了解我数据上传数据的原因时,在资源目录数据子系统下的上传任务列表中,点击失败数据可以查看到我的所有上传失败的数据,
 *           通过列表形式呈现,以便于我可以更快的修复错误数据并重新上传
 * @Scenario: 查看失败资源目录数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_error_data');
    }

    /**
     * @Given: 存在失败资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_task' => $this->task(),
            'pcore_error_data' => $this->errorData(),
            'pcore_bj_template' => $this->bjTemplate()
        ]);
    }

    /**
     * @When: 当我查看失败资源目录数据列表时
     */
    public function fetchErrorDataList()
    {
        $repository = new ErrorDataRepository();

        list($errorDataList, $count) = $repository->filter([]);
        
        unset($count);

        return $errorDataList;
    }

    /**
     * @Then: 我可以看到失败资源目录数据的全部信息
     */
    public function testViewErrorDataList()
    {
        $setErrorDataList = $this->errorData();

        foreach ($setErrorDataList as $key => $setErrorData) {
            $setErrorDataList[$key]['error_data_id'] = $key +1;
            $setErrorDataList[$key]['error_reason'] = json_decode($setErrorData['error_reason'], true);
        }

        $errorDataList = $this->fetchErrorDataList();

        $translator = new ErrorDataDbTranslator();
        foreach ($errorDataList as $errorData) {
            $errorDataArray[] = $translator->objectToArray($errorData);
        }

        $this->assertEquals($errorDataArray, $setErrorDataList);
    }
}

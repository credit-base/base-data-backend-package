<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\BjTemplate;

class BjTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $bjTemplate = new BjTemplateView(new BjTemplate());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $bjTemplate);
    }
}

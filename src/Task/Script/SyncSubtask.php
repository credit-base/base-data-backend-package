<?php
namespace BaseData\Task\Script;

use BaseData\Task\Model\OnceTask;

use BaseData\ResourceCatalogData\Service\SyncSubtaskService;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RuleServiceRepository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\TaskRepository;

class SyncSubtask extends OnceTask
{
    protected function getTaskRepository() : TaskRepository
    {
        return new TaskRepository();
    }

    protected function fetchTask(int $id) : Task
    {
        return $this->getTaskRepository()->fetchOne($id);
    }

    protected function getRuleServiceRepository() : RuleServiceRepository
    {
        return new RuleServiceRepository();
    }

    protected function fetchRuleService(int $id) : RuleService
    {
        return $this->getRuleServiceRepository()->fetchOne($id);
    }

    protected function getSyncSubtaskService(RuleService $ruleService, Task $task) : SyncSubtaskService
    {
        return new SyncSubtaskService($ruleService, $task);
    }

    protected function getSyncSubtask() : SyncSubtask
    {
        return new SyncSubtask();
    }

    protected function getSyncErrorDataDownload() : SyncErrorDataDownload
    {
        return new SyncErrorDataDownload();
    }

    public function executeAction() : bool
    {
        $data = $this->getData();

        $ruleServiceId = isset($data['ruleService']) ? $data['ruleService'] : 0;
        $taskId = isset($data['task']) ? $data['task'] : 0;

        $task = $this->fetchTask($taskId);
        $ruleService = $this->fetchRuleService($ruleServiceId);

        $syncSubtaskService = $this->getSyncSubtaskService($ruleService, $task);

        list($ruleServiceList, $task) = $syncSubtaskService->execute();

        if ($task->isFailureStatus()) {
            $syncErrorDataDownload = $this->getSyncErrorDataDownload();
            $syncErrorDataDownload->setData(array('task' => $task->getId()));
            $syncErrorDataDownload->add();
        }

        foreach ($ruleServiceList as $ruleService) {
            $syncSubtask = $this->getSyncSubtask();
            $syncSubtask->setData(array('task' => $task->getId(), 'ruleService' => $ruleService->getId()));
            $syncSubtask->add();
        }

        return true;
    }
}

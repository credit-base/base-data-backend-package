<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\BjSearchData;

class BjSearchDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $bjSearchData = new BjSearchDataView(new BjSearchData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $bjSearchData);
    }
}

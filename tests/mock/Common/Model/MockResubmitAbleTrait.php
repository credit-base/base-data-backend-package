<?php
namespace BaseData\Common\Model;

class MockResubmitAbleTrait implements IResubmitAble
{
    use ResubmitAbleTrait;

    public function isReject() : bool
    {
        return false;
    }

    public function resubmitAction() : bool
    {
        return false;
    }
}

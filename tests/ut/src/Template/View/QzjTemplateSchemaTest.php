<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class QzjTemplateSchemaTest extends TestCase
{
    private $qzjTemplateSchema;

    private $qzjTemplate;

    public function setUp()
    {
        $this->qzjTemplateSchema = new QzjTemplateSchema(new Factory());

        $this->qzjTemplate = \BaseData\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->qzjTemplateSchema);
        unset($this->qzjTemplate);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->qzjTemplateSchema);
    }

    public function testGetId()
    {
        $result = $this->qzjTemplateSchema->getId($this->qzjTemplate);

        $this->assertEquals($result, $this->qzjTemplate->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->qzjTemplateSchema->getAttributes($this->qzjTemplate);

        $this->assertEquals($result['name'], $this->qzjTemplate->getName());
        $this->assertEquals($result['identify'], $this->qzjTemplate->getIdentify());
        $this->assertEquals($result['subjectCategory'], $this->qzjTemplate->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->qzjTemplate->getDimension());
        $this->assertEquals($result['exchangeFrequency'], $this->qzjTemplate->getExchangeFrequency());
        $this->assertEquals($result['infoClassify'], $this->qzjTemplate->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->qzjTemplate->getInfoCategory());
        $this->assertEquals($result['description'], $this->qzjTemplate->getDescription());
        $this->assertEquals($result['category'], $this->qzjTemplate->getCategory());
        $this->assertEquals($result['items'], $this->qzjTemplate->getItems());
        $this->assertEquals($result['status'], $this->qzjTemplate->getStatus());
        $this->assertEquals($result['createTime'], $this->qzjTemplate->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->qzjTemplate->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->qzjTemplate->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->qzjTemplateSchema->getRelationships($this->qzjTemplate, 0, array());

        $this->assertEquals($result['sourceUnit'], ['data' => $this->qzjTemplate->getSourceUnit()]);
    }
}

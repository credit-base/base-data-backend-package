<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Repository\BjItemsDataRepository;
use BaseData\ResourceCatalogData\Translator\BjSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\BjSearchDataRowCacheQuery;

use BaseData\Template\Repository\BjTemplateRepository;

class MockBjSearchDataDbAdapter extends BjSearchDataDbAdapter
{
    public function getDbTranslator() : BjSearchDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : BjSearchDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getItemsDataRepository() : BjItemsDataRepository
    {
        return parent::getItemsDataRepository();
    }

    public function getTemplateRepository() : BjTemplateRepository
    {
        return parent::getTemplateRepository();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

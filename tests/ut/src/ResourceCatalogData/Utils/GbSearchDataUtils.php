<?php
namespace BaseData\ResourceCatalogData\Utils;

trait GbSearchDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbSearchData
    ) {
        $this->assertEquals($expectedArray['gb_search_data_id'], $gbSearchData->getId());
        $this->assertEquals($expectedArray['info_classify'], $gbSearchData->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $gbSearchData->getInfoCategory());
        $this->assertEquals($expectedArray['crew_id'], $gbSearchData->getCrew()->getId());
        $this->assertEquals($expectedArray['usergroup_id'], $gbSearchData->getSourceUnit()->getId());
        $this->assertEquals($expectedArray['subject_category'], $gbSearchData->getSubjectCategory());
        $this->assertEquals($expectedArray['dimension'], $gbSearchData->getDimension());
        $this->assertEquals($expectedArray['name'], $gbSearchData->getName());
        $this->assertEquals($expectedArray['identify'], $gbSearchData->getIdentify());
        $this->assertEquals($expectedArray['expiration_date'], $gbSearchData->getExpirationDate());
        $this->assertEquals($expectedArray['gb_template_id'], $gbSearchData->getTemplate()->getId());
        $this->assertEquals($expectedArray['gb_items_data_id'], $gbSearchData->getItemsData()->getId());
        $this->assertEquals($expectedArray['status'], $gbSearchData->getStatus());
        $this->assertEquals($expectedArray['hash'], $gbSearchData->getHash());
        $this->assertEquals($expectedArray['create_time'], $gbSearchData->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $gbSearchData->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $gbSearchData->getStatusTime());
        $this->assertEquals($expectedArray['task_id'], $gbSearchData->getTask()->getId());
        $this->assertEquals($expectedArray['description'], $gbSearchData->getDescription());
        $this->assertEquals($expectedArray['front_end_processor_status'], $gbSearchData->getFrontEndProcessorStatus());
    }
}

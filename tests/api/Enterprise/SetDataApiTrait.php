<?php
namespace BaseData\Enterprise;

use Marmot\Core;

use BaseData\Enterprise\Model\Enterprise;

trait SetDataApiTrait
{
    protected function enterprise() : array
    {
        return
        [
            [
                'enterprise_id' => 1,
                'name' => '企业名称',
                'unified_social_credit_code' => '912908Q21903E74AX2',
                'establishment_date' => 20210831,
                'approval_date' => 20210831,
                'address' => '陕西省西安市雁塔区长延堡街道',
                'registration_capital' => '1000',
                'business_term_start' => 2018930,
                'business_term_to' => 20210930,
                'business_scope' => '经营范围',
                'registration_authority' => '发展和改革委员会',
                'principal' => '张萍',
                'principal_card_id' => '412825199009094352',
                'registration_status' => '存续(在营、开业、在册)',
                'enterprise_type_code' => '100',
                'enterprise_type' => '内资企业',
                'data' => base64_encode(gzcompress(serialize(array('其他数据')))),
                'industry_category' => 'A',
                'industry_code' => '0112',
                'administrative_area' => 001,
                'hash' => md5(base64_encode(gzcompress(serialize(array('其他数据'))))),
                'task_id' => 1,
                'status' => Enterprise::STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ],
            [
                'enterprise_id' => 2,
                'name' => '宏远园林工程有限公司',
                'unified_social_credit_code' => '610527199403165500',
                'establishment_date' => 20210930,
                'approval_date' => 20210930,
                'address' => '黑牛城道298号',
                'registration_capital' => '1000',
                'business_term_start' => 20180930,
                'business_term_to' => 20220930,
                'business_scope' => '园林绿化工程；园林绿化工程设计',
                'registration_authority' => '市场和质量监督管理局',
                'principal' => '张萍',
                'principal_card_id' => '412825199009094352',
                'registration_status' => '存续(在营、开业、在册)',
                'enterprise_type_code' => '100',
                'enterprise_type' => '内资企业',
                'data' => base64_encode(gzcompress(serialize(array('其他数据')))),
                'industry_category' => 'A',
                'industry_code' => '0111',
                'administrative_area' => 001,
                'hash' => md5(base64_encode(gzcompress(serialize(array('其他数据'))))),
                'task_id' => 1,
                'status' => Enterprise::STATUS['NORMAL'],
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
            ]
        ];
    }
}

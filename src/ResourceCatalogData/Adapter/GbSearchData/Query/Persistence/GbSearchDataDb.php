<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class GbSearchDataDb extends Db
{
    const TABLE = 'gb_search_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

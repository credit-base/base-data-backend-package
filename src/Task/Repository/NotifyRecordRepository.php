<?php
namespace BaseData\Task\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;
use BaseData\Task\Adapter\NotifyRecord\NotifyRecordDbAdapter;
use BaseData\Task\Adapter\NotifyRecord\NotifyRecordMockAdapter;

class NotifyRecordRepository extends Repository implements INotifyRecordAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new NotifyRecordDbAdapter();
    }

    protected function getActualAdapter() : INotifyRecordAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : INotifyRecordAdapter
    {
        return new NotifyRecordMockAdapter();
    }

    public function setAdapter(INotifyRecordAdapter $adapter) : void
    {
        $this->adapter = $adapter;
    }

    public function fetchOne($id) : NotifyRecord
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(NotifyRecord $notifyRecord) : bool
    {
        return $this->getAdapter()->add($notifyRecord);
    }
}

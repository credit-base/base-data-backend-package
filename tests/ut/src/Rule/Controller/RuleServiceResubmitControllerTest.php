<?php
namespace BaseData\Rule\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Command\RuleService\ResubmitRuleServiceCommand;

class RuleServiceResubmitControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new RuleServiceResubmitController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIResubmitAbleController()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Controller\Interfaces\IResubmitAbleController',
            $this->controller
        );
    }

    private function rulesData() : array
    {
        return array(
            "type"=>"rules",
            "attributes"=>array(
                "rules"=>array(
                    'transformationRule' => array("ZTMC" => "ZTMC",),
                    'completionRule' =>  array(
                        'ZTMC' => array(
                            array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                            array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
                        )
                    ),
                    'comparisonRule' =>  array(
                        'TYSHXYDM' => array(
                            array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                            array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
                        ),
                    ),
                    'deDuplicationRule' => array('result' => 1, "items"=>"ZTMC"),
                )
            ),
            "relationships"=>array(
                "crew"=>array(
                    "data"=>array(
                        array("type"=>"crews","id"=>1)
                    )
                )
            )
        );
    }
    
    /**
     * 测试 resubmit 成功
     * 1. 为 RuleServiceResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getRuleServiceWidgetRule、getCommandBus、getUnAuditedRuleServiceRepository、render方法
     * 2. 调用 $this->initResubmit(), 期望结果为 true
     * 3. 为 RuleService 类建立预言
     * 4. 为 UnAuditedRuleServiceRepository 类建立预言, UnAuditedRuleServiceRepository->fetchOne 方法被调用一次,
     *    且 返回结果为 预言的RuleService, getUnAuditedRuleServiceRepository 方法被调用一次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->resubmit 方法被调用一次, 且返回结果为 true
     */
    public function testResubmit()
    {
        // 为 RuleServiceResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getRuleServiceWidgetRule、getCommandBus、getUnAuditedRuleServiceRepository、render方法
        $controller = $this->getMockBuilder(RuleServiceResubmitController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateCommonScenario',
                    'getCommandBus',
                    'getUnAuditedRuleServiceRepository',
                    'render'
                ]
            )->getMock();

        $id = 1;

        $this->initResubmit($controller, $id, true);

        // 为 RuleService 类建立预言
        $unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($id);

        // 为 UnAuditedRuleServiceRepository 类建立预言, UnAuditedRuleServiceRepository->fetchOne 方法被调用一次,
        // 且 返回结果为 预言的RuleService, getUnAuditedRuleServiceRepository 方法被调用一次
        $repository = $this->prophesize(UnAuditedRuleServiceRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($unAuditedRuleService);
        $controller->expects($this->once())
                             ->method('getUnAuditedRuleServiceRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->resubmit 方法被调用一次, 且返回结果为 true
        $result = $controller->resubmit($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 resubmit 失败
     * 1. 为 RuleServiceResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getRuleServiceWidgetRule、getCommandBus、displayError 方法
     * 2. 调用 $this->initResubmit(), 期望结果为 false
     * 3. displayError 方法被调用一次, 且controller返回结果为 false
     * 4. controller->resubmit 方法被调用一次, 且返回结果为 false
     */
    public function testResubmitFail()
    {
        // 为 RuleServiceResubmitController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getRuleServiceWidgetRule、getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(RuleServiceResubmitController::class)
            ->setMethods(
                [
                    'getRequest',
                    'validateCommonScenario',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();

        $id = 1;

        // 调用 $this->initResubmit(), 期望结果为 false
        $this->initResubmit($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->resubmit 方法被调用一次, 且返回结果为 false
        $result = $controller->resubmit($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 resubmit 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
     * 3. 为 RuleServiceWidgetRule 类建立预言, 验证请求参数, getRuleServiceWidgetRule 方法被调用一次
     * 4. 为 CommandBus 类建立预言, 传入 ResubmitRuleServiceCommand参数, 且 send 方法被调用一次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initResubmit(RuleServiceResubmitController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->rulesData();

        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rules = $attributes['rules'];

        $crew = $relationships['crew']['data'][0]['id'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用一次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $controller->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->willReturn(true);

        $command = new ResubmitRuleServiceCommand(
            $rules,
            $crew,
            $id
        );
        // 为 CommandBus 类建立预言, 传入 ResubmitRuleServiceCommand参数, 且 send 方法被调用一次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用一次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}

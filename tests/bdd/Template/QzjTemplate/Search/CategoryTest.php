<?php
namespace BaseData\Template\QzjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件资源目录时,在目录管理子系统下的前置机资源目录管理中,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录数
 * @Scenario: 通过目录类别搜索
 */
class CategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchQzjTemplateList()
    {
        $repository = new QzjTemplateRepository();
        $filter['category'] = QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'];

        list($qzjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $qzjTemplateList;
    }

    /**
     * @Then: 我可以看到目录类别为前置机委办局的所有资源目录
     */
    public function testViewQzjTemplateList()
    {
        $categoryQzjTemplateList = $this->qzjTemplate();

        foreach ($categoryQzjTemplateList as $key => $categoryQzjTemplate) {
            $categoryQzjTemplateList[$key]['qzj_template_id'] = $key +1;
            $categoryQzjTemplateList[$key]['subject_category'] = json_decode(
                $categoryQzjTemplate['subject_category'],
                true
            );
            $categoryQzjTemplateList[$key]['items'] = json_decode($categoryQzjTemplate['items'], true);
        }

        $qzjTemplateList = $this->fetchQzjTemplateList();
        $translator = new QzjTemplateDbTranslator();
        foreach ($qzjTemplateList as $qzjTemplate) {
            $qzjTemplateArray[] = $translator->objectToArray($qzjTemplate);
        }

        $this->assertEquals($qzjTemplateArray, $categoryQzjTemplateList);

        foreach ($qzjTemplateArray as $qzjTemplate) {
            $this->assertEquals(QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'], $qzjTemplate['category']);
        }
    }
}

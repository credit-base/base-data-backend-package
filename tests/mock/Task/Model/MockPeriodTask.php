<?php
namespace BaseData\Task\Model;

class MockPeriodTask extends PeriodTask
{
    protected function executeAction() : bool
    {
        return true;
    }

    public function afterExecute() : bool
    {
        return parent::afterExecute();
    }

    public function getPeriod() : int
    {
        return 5;
    }
}

<?php
namespace BaseData\Rule\Model;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

use BaseData\Template\Model\Template;

class DeDuplicationRule extends Rule implements IRule
{
    use RuleTrait;
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 验证去重规则格式
        * 规则格式说明及样例
            * array('result'=>1, items=>array('ZTMC', 'TYSHXYDM'))
        * 验证流程
            * 1. 获取去重规则 $condition = $this->getCondition();
            * 2. 获取目标资源目录
            * 3. 验证去重规则中的items项是否存在与目标资源目录
     * @return bool
     * @todo 后续需要对返回错误输出信息或日志，方便调试
     */
    public function validate() : bool
    {
        //获取目标资源目录的模板信息项中的模板标识
        $transformationTemplate = $this->getTransformationTemplate();
        
        $items = $transformationTemplate->getItems();
        $itemIdentifies = array();

        foreach ($items as $item) {
            $itemIdentifies[] = $item['identify'];
        }

        //获取去重规则中的去重信息项
        $condition = $this->getCondition();
        $deDuplicationItems = $condition['items'];

        //循环去重信息项验证是否存在与目标资源目录
        foreach ($deDuplicationItems as $deDuplicationItem) {
            if (!in_array($deDuplicationItem, $itemIdentifies)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'deDuplicationRulesItemsNotExit'));
                return false;
            }
        }

        return true;
    }

    //实现父类的抽象方法
    protected function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        unset($transformationSearchData);
        return true;
    }

    /**
     * 初始化数据(实现父类的抽象方法)
     * @param ISearchDataAble $sourceSearchData 来源资源目录数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * condition 去重条件
     */
    protected function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {
        unset($sourceSearchData);
        unset($transformationSearchData);
        //获取去重条件
        $condition = $this->getCondition();
        return array(
            'condition' => $condition
        );
    }

    //验证初始化数据格式
    protected function validateInitializationDataFormat(array $data) : bool
    {
        return isset($data['condition']);
    }

    /**
     * 执行转换过程(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * deDuplicationItems 去重失败的资源目录模板信息项
        * hash 去重项hash值
     */
    protected function transformationExecute(array $data, ISearchDataAble $transformationSearchData) : array
    {
        //验证初始化数据格式,false返回空数组
        if (!$this->validateInitializationDataFormat($data)) {
            return array();
        }
        //获取去重信息项
        $deDuplicationItemsIdentify = isset($data['condition']['items']) ? $data['condition']['items'] : array();
        //获取去重项hash值
        $hash = $this->generateDeDuplicationItemsHash(
            $deDuplicationItemsIdentify,
            $transformationSearchData
        );

        $deDuplicationItems = $this->fetchDeDuplicationItems($deDuplicationItemsIdentify, $transformationSearchData);

        return array(
            'deDuplicationItems' => $deDuplicationItems,
            'hash' => $hash
        );
    }

    //获取去重资源目录模板信息项
    protected function fetchDeDuplicationItems(
        array $deDuplicationItemsIdentify,
        ISearchDataAble $transformationSearchData
    ) : array {
        $deDuplicationItems = array();
        //获取目标资源目录的模板信息项
        $transformationTemplateItems = $this->fetchTemplateItems($transformationSearchData->getTemplate());

        if (empty($deDuplicationItemsIdentify)) {
            $deDuplicationItems = $transformationTemplateItems;
        }
        foreach ($deDuplicationItemsIdentify as $identify) {
            $deDuplicationItems[$identify] = isset($transformationTemplateItems[$identify]) ?
                                             $transformationTemplateItems[$identify] :
                                             array();
        }

        return $deDuplicationItems;
    }

    //验证执行后返回的数据格式
    protected function validateDataFormat(array $data) : bool
    {
        return (isset($data['deDuplicationItems']) && isset($data['hash']));
    }

    /**
     * 判断并返回结果(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return ISearchDataAble
     */
    protected function transformationResult(array $data, ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        //验证数据格式不正确,直接返回异常失败的目标资源目录数据
        if (!$this->validateDataFormat($data)) {
            $transformationSearchData = $this->failureDataException(
                $transformationSearchData,
                ErrorData::STATUS['PROGRAM_EXCEPTION']
            );
        }
        
        $hash = $data['hash'];
        $deDuplicationItems = isset($data['deDuplicationItems']) ? $data['deDuplicationItems'] : array();

        //通过hash验证数据是否存在,不存在则返回完善的资源目录数据,存在则返回失败的资源目录数据
        $transformationSearchData = ($this->isSearchDataExist($hash, $transformationSearchData)) ?
                                    $transformationSearchData :
                                    $this->failureData(
                                        $transformationSearchData,
                                        ErrorData::ERROR_TYPE['DUPLICATION_DATA'],
                                        $deDuplicationItems
                                    );
                                   
        $transformationSearchData->setHash($hash);

        return $transformationSearchData;
    }

    //生成hash
    protected function generateDeDuplicationItemsHash(
        array $deDuplicationItemsIdentify,
        ISearchDataAble $transformationSearchData
    ) : string {
        $deDuplicationItemsData = array();
        //获取目标资源目录信息项数据
        $transformationItemsData = $transformationSearchData->getItemsData()->getData();
        //获取去重数据
        foreach ($deDuplicationItemsIdentify as $identify) {
            if (isset($transformationItemsData[$identify])) {
                $deDuplicationItemsData[$identify] = $transformationItemsData[$identify];
            }
        }
        //如果去重信息项为空,默认为全部资源目录信息项数据
        if (empty($deDuplicationItemsIdentify)) {
            $deDuplicationItemsData = $transformationItemsData;
        }

        return md5(base64_encode(gzcompress(serialize($deDuplicationItemsData))));
    }

    //通过hash验证数据是否存在
    protected function isSearchDataExist(string $hash, ISearchDataAble $transformationSearchData) : bool
    {
        //获取资源目录数据repository
        $repository =  $this->getSearchDataRepository($transformationSearchData);
        //通过hash条件获取资源目录数据
        $filter = $this->filterFormatChange($hash, $transformationSearchData);
        
        list($list, $count) = $repository->filter($filter);
        unset($list);

        return empty($count);
    }

    protected function filterFormatChange(string $hash, ISearchDataAble $transformationSearchData) : array
    {
        $filter = array();
        $filter['hash'] = $hash;

        $filter['template'] = $transformationSearchData->getTemplate()->getId();

        if ($transformationSearchData->getTemplate()->getCategory() == Template::CATEGORY['BASE']) {
            $filter['baseSubjectCategory'] = $transformationSearchData->getSubjectCategory();
        }
        
        return $filter;
    }
}

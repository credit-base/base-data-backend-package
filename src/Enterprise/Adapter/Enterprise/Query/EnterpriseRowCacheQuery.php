<?php
namespace BaseData\Enterprise\Adapter\Enterprise\Query;

use Marmot\Framework\Query\RowCacheQuery;

class EnterpriseRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'enterprise_id',
            new Persistence\EnterpriseCache(),
            new Persistence\EnterpriseDb()
        );
    }
}

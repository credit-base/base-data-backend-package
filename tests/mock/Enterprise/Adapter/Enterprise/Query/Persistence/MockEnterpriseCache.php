<?php
namespace BaseData\Enterprise\Adapter\Enterprise\Query\Persistence;

class MockEnterpriseCache extends EnterpriseCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

<?php
namespace BaseData\Rule\Command\RuleService;

use PHPUnit\Framework\TestCase;

/**
 * @author chloroplast
 */
class ApproveRuleServiceCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'applyCrew' => $faker->randomDigit(),
            'id' => $faker->randomDigit(),
        );

        $this->command = new ApproveRuleServiceCommand(
            $this->fakerData['applyCrew'],
            $this->fakerData['id']
        );
    }

    public function tearDown()
    {
        unset($this->command);
    }
    
    public function testExtendsApproveCommand()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Command\ApproveCommand',
            $this->command
        );
    }
}

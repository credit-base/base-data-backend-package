<?php
namespace BaseData\Rule\UnAuditedRuleService\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;

use BaseData\Common\Model\IApproveAble;

use BaseData\Template\Model\Template;

/**
 * @Feature: 超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则编辑权限,在政务网OA的资源目录规则管理中,可以对设置的规则进行编辑
 *           可以查看使用规则的情况或编辑已经设置好的规则并重新提交,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 正常编辑规则数据
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_rule' => $this->rule(),
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_wbj_template' => $this->wbjTemplate()
        ]);
    }

    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $applyFormArray = $this->applyFormRule(
            IApproveAble::APPLY_STATUS['PENDING'],
            1,
            IApproveAble::OPERATION_TYPE['EDIT']
        )[0];

        $unAuditedRuleService = new UnAuditedRuleService();

        $unAuditedRuleService->setId(1);
        $unAuditedRuleService->setRelationId(1);
        $unAuditedRuleService->setRejectReason($applyFormArray['reject_reason']);
        $unAuditedRuleService->setOperationType($applyFormArray['operation_type']);
        $unAuditedRuleService->setSourceCategory($applyFormArray['source_category']);
        $unAuditedRuleService->getUserGroup()->setId($applyFormArray['usergroup_id']);
        $unAuditedRuleService->getApplyCrew()->setId($applyFormArray['apply_crew_id']);
        $unAuditedRuleService->getPublishCrew()->setId($applyFormArray['publish_crew_id']);
        $unAuditedRuleService->getApplyUserGroup()->setId($applyFormArray['apply_usergroup_id']);
        $unAuditedRuleService->getSourceTemplate()->setId($applyFormArray['source_template_id']);
        $unAuditedRuleService->setTransformationCategory($applyFormArray['transformation_category']);
        $unAuditedRuleService->getTransformationTemplate()->setId($applyFormArray['transformation_template_id']);
        $unAuditedRuleService->setApplyInfo(
            unserialize(gzuncompress(base64_decode($applyFormArray['apply_info'], true)))
        );

        return $unAuditedRuleService->edit();
    }

    /**
     * @Then: 可以查到编辑的审核数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);

        $setUnAuditedRuleService = $this->applyFormRule(
            IApproveAble::APPLY_STATUS['PENDING'],
            1,
            IApproveAble::OPERATION_TYPE['EDIT']
        )[0];
        $setUnAuditedRuleService['apply_form_rule_id'] = 1;
        
        $repository = new UnAuditedRuleServiceRepository();

        $unAuditedRuleService = $repository->fetchOne($result);

        $translator = new UnAuditedRuleServiceDbTranslator();
        $unAuditedRuleServiceArray = $translator->objectToArray($unAuditedRuleService);

        $this->assertEquals($unAuditedRuleServiceArray, $setUnAuditedRuleService);
    }
}

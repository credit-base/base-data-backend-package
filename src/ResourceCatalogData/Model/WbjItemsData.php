<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Repository\WbjItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;

class WbjItemsData extends ItemsData
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjItemsDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IWbjItemsDataAdapter
    {
        return $this->repository;
    }
}

<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockConfirmWorkOrderTaskCommandHandler extends ConfirmWorkOrderTaskCommandHandler
{
    public function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return parent::getWorkOrderTaskRepository();
    }
}

<?php
namespace BaseData\Template\Repository;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;

class MockGbTemplateRepository extends GbTemplateRepository
{
    public function getAdapter() : IGbTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IGbTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IGbTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}

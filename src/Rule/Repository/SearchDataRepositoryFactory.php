<?php
namespace BaseData\Rule\Repository;

use BaseData\Template\Model\Template;

use BaseData\Common\Repository\NullRepository;

class SearchDataRepositoryFactory
{
    const MAPS = array(
        Template::CATEGORY['BJ'] =>
        'BaseData\ResourceCatalogData\Repository\BjSearchDataRepository',
        Template::CATEGORY['GB'] =>
        'BaseData\ResourceCatalogData\Repository\GbSearchDataRepository',
        Template::CATEGORY['BASE'] =>
        'BaseData\ResourceCatalogData\Repository\BaseSearchDataRepository',
        Template::CATEGORY['WBJ'] =>
        'BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository',
    );

    public function getRepository(int $category)
    {
        $repository = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($repository) ? new $repository : NullRepository::getInstance();
    }
}

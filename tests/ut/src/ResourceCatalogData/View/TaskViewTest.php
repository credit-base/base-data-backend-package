<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\Task;

class TaskViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $task = new TaskView(new Task());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $task);
    }
}

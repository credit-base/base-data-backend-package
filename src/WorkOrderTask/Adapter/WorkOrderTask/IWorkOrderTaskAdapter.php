<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask;

use BaseData\WorkOrderTask\Model\WorkOrderTask;

interface IWorkOrderTaskAdapter
{
    public function fetchOne($id) : WorkOrderTask;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(WorkOrderTask $workOrderTask) : bool;

    public function edit(WorkOrderTask $workOrderTask, array $keys = array()) : bool;
}

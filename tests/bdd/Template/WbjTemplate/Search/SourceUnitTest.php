<?php
namespace BaseData\Template\WbjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Translator\WbjTemplateDbTranslator;

/**
 * @Feature: 我是委办局领导,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 通过来源单位搜索
 */
class SourceUnitTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchWbjTemplateList()
    {
        $repository = new WbjTemplateRepository();
 
        $filter['userGroup'] = 1;

        list($wbjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $wbjTemplateList;
    }

    /**
     * @Then: 我可以看到来源单位Id为1的所有资源目录
     */
    public function testViewWbjTemplateList()
    {
        $userGroupWbjTemplateList = $this->wbjTemplate();

        foreach ($userGroupWbjTemplateList as $key => $userGroupWbjTemplate) {
            $userGroupWbjTemplateList[$key]['wbj_template_id'] = $key +1;
            $userGroupWbjTemplateList[$key]['subject_category'] = json_decode(
                $userGroupWbjTemplate['subject_category'],
                true
            );
            $userGroupWbjTemplateList[$key]['items'] = json_decode($userGroupWbjTemplate['items'], true);
        }

        $wbjTemplateList = $this->fetchWbjTemplateList();
        $translator = new WbjTemplateDbTranslator();
        foreach ($wbjTemplateList as $wbjTemplate) {
            $wbjTemplateArray[] = $translator->objectToArray($wbjTemplate);
        }

        $this->assertEquals($wbjTemplateArray, $userGroupWbjTemplateList);

        foreach ($wbjTemplateArray as $wbjTemplate) {
            $this->assertEquals(1, $wbjTemplate['source_unit_id']);
        }
    }
}

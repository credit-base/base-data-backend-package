<?php
namespace BaseData\Template\Command\WbjTemplate;

use BaseData\Template\Command\Template\EditTemplateCommand;

class EditWbjTemplateCommand extends EditTemplateCommand
{
    /**
     * @var int $sourceUnit 来源单位
     */
    public $sourceUnit;

    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    public function __construct(
        string $name,
        string $identify,
        array $subjectCategory,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        string $description,
        array $items,
        int $sourceUnit,
        int $id
    ) {
        parent::__construct(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $id
        );
        $this->sourceUnit = $sourceUnit;
    }
}

<?php
namespace BaseData\Rule\Adapter\RuleService\Query\Persistence;

use Marmot\Framework\Classes\Db;

class RuleServiceDb extends Db
{
    const TABLE = 'rule';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

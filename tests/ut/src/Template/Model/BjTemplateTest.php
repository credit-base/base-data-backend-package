<?php
namespace BaseData\Template\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;

class BjTemplateTest extends TestCase
{
    private $bjTemplate;

    public function setUp()
    {
        $this->bjTemplate = new BjTemplate();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->bjTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\Template',
            $this->bjTemplate
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IOperate',
            $this->bjTemplate
        );
    }

    public function testGetRepository()
    {
        $bjTemplate = new MockBjTemplate();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $bjTemplate->getRepository()
        );
    }

    public function testGetCategory()
    {
        $result = $this->bjTemplate->getCategory();
        $this->assertEquals($result, Template::CATEGORY['BJ']);
    }

    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IBjTemplateAdapter 调用 add 并且传参 $bjTemplate 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $bjTemplate = $this->getMockBuilder(BjTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IBjTemplateAdapter
        $repository = $this->prophesize(IBjTemplateAdapter::class);
        $repository->add(Argument::exact($bjTemplate))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $bjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $bjTemplate->add();
        $this->assertTrue($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IBjTemplateAdapter 调用 add 并且传参 $bjTemplate 对象, 返回 false
     */
    public function testAddFail()
    {
        //初始化
        $bjTemplate = $this->getMockBuilder(BjTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IBjTemplateAdapter
        $repository = $this->prophesize(IBjTemplateAdapter::class);
        $repository->add(Argument::exact($bjTemplate))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        //绑定
        $bjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $bjTemplate->add();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IBjTemplateAdapter 调用 edit 并且传参 $bjTemplate 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $bjTemplate = $this->getMockBuilder(BjTemplate::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        //预言修改updateTime
        $bjTemplate->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IBjTemplateAdapter
        $repository = $this->prophesize(IBjTemplateAdapter::class);
        $repository->edit(
            Argument::exact($bjTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items',
                    'sourceUnit',
                    'gbTemplate',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $bjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $bjTemplate->edit();
        $this->assertTrue($result);
    }

    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IBjTemplateAdapter 调用 edit 并且传参 $bjTemplate 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $bjTemplate = $this->getMockBuilder(BjTemplate::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        //预言修改updateTime
        $bjTemplate->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IBjTemplateAdapter
        $repository = $this->prophesize(IBjTemplateAdapter::class);
        $repository->edit(
            Argument::exact($bjTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items',
                    'sourceUnit',
                    'gbTemplate',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $bjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $bjTemplate->edit();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
    }
}

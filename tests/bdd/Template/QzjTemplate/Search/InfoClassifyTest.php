<?php
namespace BaseData\Template\QzjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件资源目录时,在目录管理子系统下的前置机资源目录管理中,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录数
 * @Scenario: 通过信息分类搜索
 */
class InfoClassifyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchQzjTemplateList()
    {
        $repository = new QzjTemplateRepository();
 
        $filter['infoClassify'] = Template::INFO_CLASSIFY['QT'];

        list($qzjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $qzjTemplateList;
    }

    /**
     * @Then: 我可以看到信息分类为其他的所有资源目录
     */
    public function testViewQzjTemplateList()
    {
        $classifyQzjTemplateList = $this->qzjTemplate();

        foreach ($classifyQzjTemplateList as $key => $classifyQzjTemplate) {
            $classifyQzjTemplateList[$key]['qzj_template_id'] = $key +1;
            $classifyQzjTemplateList[$key]['subject_category'] = json_decode(
                $classifyQzjTemplate['subject_category'],
                true
            );
            $classifyQzjTemplateList[$key]['items'] = json_decode($classifyQzjTemplate['items'], true);
        }

        $qzjTemplateList = $this->fetchQzjTemplateList();
        $translator = new QzjTemplateDbTranslator();
        foreach ($qzjTemplateList as $qzjTemplate) {
            $qzjTemplateArray[] = $translator->objectToArray($qzjTemplate);
        }

        $this->assertEquals($qzjTemplateArray, $classifyQzjTemplateList);

        foreach ($qzjTemplateArray as $qzjTemplate) {
            $this->assertEquals(Template::INFO_CLASSIFY['QT'], $qzjTemplate['info_classify']);
        }
    }
}

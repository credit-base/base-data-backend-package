<?php
namespace BaseData\ResourceCatalogData\CommandHandler\GbSearchData;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;

class MockGbSearchDataCommandHandlerTrait
{
    use GbSearchDataCommandHandlerTrait;

    public function publicGetRepository() : GbSearchDataRepository
    {
        return $this->getRepository();
    }

    public function publicFetchGbSearchData(int $id) : GbSearchData
    {
        return $this->fetchGbSearchData($id);
    }
}

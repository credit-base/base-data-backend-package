<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use BaseData\WorkOrderTask\Model\ParentTask;

class MockAddParentTaskCommandHandler extends AddParentTaskCommandHandler
{
    public function getParentTask() : ParentTask
    {
        return parent::getParentTask();
    }
}

<?php
namespace BaseData\Enterprise\View;

use PHPUnit\Framework\TestCase;

use BaseData\Enterprise\Model\Enterprise;

class EnterpriseViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $enterprise = new EnterpriseView(new Enterprise());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $enterprise);
    }
}

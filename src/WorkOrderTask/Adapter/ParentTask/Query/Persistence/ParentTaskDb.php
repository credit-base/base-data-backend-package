<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask\Query\Persistence;

use Marmot\Framework\Classes\Db;

class ParentTaskDb extends Db
{
    const TABLE = 'parent_task';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Model\NullWbjSearchData;
use BaseData\ResourceCatalogData\Repository\WbjItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\SearchDataDbAdapterTrait;
use BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\WbjSearchDataRowCacheQuery;

use BaseData\Template\Repository\WbjTemplateRepository;

class WbjSearchDataDbAdapter implements IWbjSearchDataAdapter
{
    use DbAdapterTrait, SearchDataDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $wbjItemsDataRepository;

    private $wbjTemplateRepository;

    public function __construct()
    {
        $this->dbTranslator = new WbjSearchDataDbTranslator();
        $this->rowCacheQuery = new WbjSearchDataRowCacheQuery();
        $this->wbjItemsDataRepository = new WbjItemsDataRepository();
        $this->wbjTemplateRepository = new WbjTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->wbjItemsDataRepository);
        unset($this->wbjTemplateRepository);
    }
    
    protected function getDbTranslator() : WbjSearchDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : WbjSearchDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullWbjSearchData::getInstance();
    }

    protected function getItemsDataRepository() : WbjItemsDataRepository
    {
        return $this->wbjItemsDataRepository;
    }

    protected function getTemplateRepository() : WbjTemplateRepository
    {
        return $this->wbjTemplateRepository;
    }

    public function add(WbjSearchData $wbjSearchData) : bool
    {
        return $this->addAction($wbjSearchData);
    }

    public function edit(WbjSearchData $wbjSearchData, array $keys = array()) : bool
    {
        return $this->editAction($wbjSearchData, $keys);
    }

    public function fetchOne($id) : WbjSearchData
    {
        $wbjSearchData = $this->fetchOneAction($id);
        $this->fetchTemplate($wbjSearchData);
        $this->fetchItemsData($wbjSearchData);
        
        return $wbjSearchData;
    }

    public function fetchList(array $ids) : array
    {
        $wbjSearchDataList = $this->fetchListAction($ids);
        $this->fetchTemplate($wbjSearchDataList);
        $this->fetchItemsData($wbjSearchDataList);

        return $wbjSearchDataList;
    }

    protected function formatFilter(array $filter) : string
    {
        $wbjSearchData = new WbjSearchData();

        return $this->searchDataFormatFilter($filter, $wbjSearchData);
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $wbjSearchData = new WbjSearchData();
 
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray($wbjSearchData, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($wbjSearchData, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

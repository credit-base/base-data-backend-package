<?php
namespace BaseData\Template\Utils;

trait WbjTemplateUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $wbjTemplate
    ) {
        $this->assertEquals($expectedArray['wbj_template_id'], $wbjTemplate->getId());
        $this->assertEquals($expectedArray['name'], $wbjTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $wbjTemplate->getIdentify());

        $subjectCategory = is_string($expectedArray['subject_category']) ?
        json_decode($expectedArray['subject_category'], true) :
        $expectedArray['subject_category'];
        $this->assertEquals($subjectCategory, $wbjTemplate->getSubjectCategory());

        $this->assertEquals($expectedArray['dimension'], $wbjTemplate->getDimension());
        $this->assertEquals($expectedArray['exchange_frequency'], $wbjTemplate->getExchangeFrequency());
        $this->assertEquals($expectedArray['info_classify'], $wbjTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $wbjTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['description'], $wbjTemplate->getDescription());

        $items = is_string($expectedArray['items']) ?
        json_decode($expectedArray['items'], true) :
        $expectedArray['items'];
        $this->assertEquals($items, $wbjTemplate->getItems());
        $this->assertEquals($expectedArray['source_unit_id'], $wbjTemplate->getSourceUnit()->getId());

        $this->assertEquals($expectedArray['status'], $wbjTemplate->getStatus());
        $this->assertEquals($expectedArray['create_time'], $wbjTemplate->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $wbjTemplate->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $wbjTemplate->getStatusTime());
    }
}

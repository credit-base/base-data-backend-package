<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\ResourceCatalogData\View\TaskView;
use BaseData\ResourceCatalogData\Repository\TaskRepository;
use BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter;

class TaskFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new TaskRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : ITaskAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new TaskView($data);
    }

    protected function getResourceName() : string
    {
        return 'tasks';
    }
}

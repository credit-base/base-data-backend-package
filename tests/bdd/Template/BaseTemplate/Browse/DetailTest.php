<?php
namespace BaseData\Template\BaseTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Translator\BaseTemplateDbTranslator;

/**
 * @Feature: 我是超级管理员,当我需要查看我的基础目录时,在政务网OA中,可以查看到我的所有基础目录,
 *           通过列表的形式查看所有基础目录,以便于我可以快速找到我的所有基础目录
 * @Scenario: 查看资源目录详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
    }

    /**
     * @Given: 存在一条资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_base_template' => $this->baseTemplate(),
        ]);
    }

    /**
     * @When: 当我查看该条资源目录详情时
     */
    public function fetchBaseTemplate($id)
    {
        $repository = new BaseTemplateRepository();

        $baseTemplate = $repository->fetchOne($id);

        return $baseTemplate;
    }

    /**
     * @Then: 我可以看见该条资源目录的全部信息
     */
    public function testViewBaseTemplateList()
    {
        $id = 1;
        
        $setBaseTemplate = $this->baseTemplate()[0];
        $setBaseTemplate['base_template_id'] = $id;
        $setBaseTemplate['subject_category'] = json_decode($setBaseTemplate['subject_category'], true);
        $setBaseTemplate['items'] = json_decode($setBaseTemplate['items'], true);

        $baseTemplate = $this->fetchBaseTemplate($id);
        $translator = new BaseTemplateDbTranslator();
        $baseTemplateArray = $translator->objectToArray($baseTemplate);

        $this->assertEquals($baseTemplateArray, $setBaseTemplate);
    }
}

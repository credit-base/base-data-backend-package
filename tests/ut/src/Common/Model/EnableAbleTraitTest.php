<?php
namespace BaseData\Common\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Repository\MockEnableRepository;

class EnableAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockEnableAbleTrait::class)
                            ->setMethods(['updateStatus'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    /**
     * @dataProvider statusDataProvider
     */
    public function testSetStatus($actural, $expected)
    {
        $this->trait->setStatus($actural);
        
        $result = $this->trait->getStatus();
        $this->assertEquals($expected, $result);
    }

    public function statusDataProvider()
    {
        return [
            [IEnableAble::STATUS['ENABLED'], IEnableAble::STATUS['ENABLED']],
            [IEnableAble::STATUS['DISABLED'], IEnableAble::STATUS['DISABLED']],
            [999, IEnableAble::STATUS['ENABLED']]
        ];
    }

    public function testEnableSuccess()
    {
        $this->trait->setStatus(IEnableAble::STATUS['DISABLED']);

        $this->trait->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(IEnableAble::STATUS['ENABLED'])
                    ->willReturn(true);

        $result = $this->trait->enable();
        $this->assertTrue($result);
    }

    public function testEnableFail()
    {
        $this->trait->setStatus(IEnableAble::STATUS['ENABLED']);

        $result = $this->trait->enable();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $this->trait->setStatus(IEnableAble::STATUS['ENABLED']);

        $this->trait->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(IEnableAble::STATUS['DISABLED'])
                    ->willReturn(true);

        $result = $this->trait->disable();
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $this->trait->setStatus(IEnableAble::STATUS['DISABLED']);

        $result = $this->trait->disable();
        $this->assertEquals(Core::getLastError()->getId(), RESOURCE_CAN_NOT_MODIFY);
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $trait = $this->getMockBuilder(MockEnableAbleTrait::class)
                            ->setMethods(['getRepository'])
                            ->getMock();
            
        $status = IEnableAble::STATUS['ENABLED'];
        $trait->setStatus($status);
        $trait->setUpdateTime(Core::$container->get('time'));
        $trait->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(MockEnableRepository::class);

        $repository->edit(
            Argument::exact($trait),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $trait->publicUpdateStatus($status);
        
        $this->assertTrue($result);
    }
}

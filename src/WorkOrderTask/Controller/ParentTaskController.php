<?php
namespace BaseData\WorkOrderTask\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\View\ParentTaskView;
use BaseData\WorkOrderTask\Command\ParentTask\AddParentTaskCommand;
use BaseData\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;

class ParentTaskController extends Controller
{
    use JsonApiTrait, ParentTaskControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 指派父任务,通过POST传参
     * 对应路由 /parentTasks
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $templateType = $attributes['templateType'];
        $title = $attributes['title'];
        $description = $attributes['description'];
        $endTime = $attributes['endTime'];
        $attachment = $attributes['attachment'];
        $template = $relationships['template']['data'][0]['id'];
        foreach ($relationships['assignObjects']['data'] as $value) {
            $assignObjects[] = $value['id'];
        }

        //验证
        if ($this->validateAddScenario(
            $templateType,
            $title,
            $description,
            $endTime,
            $attachment,
            $template,
            $assignObjects
        )) {
            //初始化命令
            $commandBus = $this->getCommandBus();
            $command = new AddParentTaskCommand(
                $templateType,
                $title,
                $description,
                $endTime,
                $attachment,
                $template,
                $assignObjects
            );

            //执行命令
            if ($commandBus->send($command)) {
                //获取最新数据
                $repository = $this->getRepository();
                $parentTask = $repository->fetchOne($command->id);
                if ($parentTask instanceof ParentTask) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new ParentTaskView($parentTask));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateAddScenario(
        $templateType,
        $title,
        $description,
        $endTime,
        $attachment,
        $template,
        $assignObjects
    ) : bool {
        return $this->getParentTaskWidgetRule()->templateType($templateType)
        && $this->getParentTaskWidgetRule()->title($title)
        && $this->getParentTaskWidgetRule()->description($description)
        && $this->getParentTaskWidgetRule()->endTime($endTime)
        && $this->getParentTaskWidgetRule()->pdf($attachment)
        && $this->getCommonWidgetRule()->formatNumeric($template, 'template')
        && $this->getCommonWidgetRule()->formatArray($assignObjects, 'assignObjects');
    }

    /**
     * 撤销父任务,通过PATCH传参
     * 对应路由 /parentTasks/{id:\d+}/revoke
     * @param int id 父任务 id
     * @return jsonApi
     */
    public function revoke(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        
        $reason = $attributes['reason'];

        //验证
        if ($this->validateRevokeScenario(
            $reason
        )) {
            $commandBus = $this->getCommandBus();

            $command = new RevokeParentTaskCommand(
                $reason,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $parentTask = $repository->fetchOne($id);
                if ($parentTask instanceof ParentTask) {
                    $this->render(new ParentTaskView($parentTask));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateRevokeScenario(
        $reason
    ) : bool {
        return $this->getCommonWidgetRule()->reason($reason);
    }
}

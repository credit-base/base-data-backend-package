<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData;

use BaseData\ResourceCatalogData\Model\WbjItemsData;

interface IWbjItemsDataAdapter
{
    public function fetchOne($id) : WbjItemsData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(WbjItemsData $wbjItemsData) : bool;
}

<?php
namespace BaseData\ResourceCatalogData\ErrorData\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\Repository\BjTemplateRepository;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ErrorItemsData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;
use BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要上传资源目录数据时,在资源目录数据子系统下的委办局数据管理,上传对应的资源目录数据
 *           根据我归集到的资源目录数据上传
 *           以便于我实现资源目录数据的报送和平台资源目录数据的呈现
 * @Scenario: 上传失败生成失败数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_error_data');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_task' => $this->task(),
            'pcore_bj_template' => $this->bjTemplate(),
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $templateRepository = new BjTemplateRepository();
        $bjTemplate = $templateRepository->fetchOne(1);

        $errorDataArray = $this->errorData()[0];

        $errorItemsData = new ErrorItemsData();
        $errorItemsData->setData(unserialize(gzuncompress(base64_decode($errorDataArray['items_data'], true))));

        $errorData = new ErrorData();

        $errorData->getTask()->setId($errorDataArray['task_id']);
        $errorData->setCategory($errorDataArray['category']);
        $errorData->setTemplate($bjTemplate);
        $errorData->setItemsData($errorItemsData);
        $errorData->setErrorType($errorDataArray['error_type']);
        $errorData->setErrorReason(json_decode($errorDataArray['error_reason'], true));
        $errorData->setStatus($errorDataArray['status']);

        return $errorData->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setErrorData = $this->errorData()[0];
        $setErrorData['error_data_id'] = 1;

        $setErrorData['error_reason'] = json_decode($setErrorData['error_reason'], true);
        
        $repository = new ErrorDataRepository();

        $errorData = $repository->fetchOne($result);

        $translator = new ErrorDataDbTranslator();
        $errorDataArray = $translator->objectToArray($errorData);

        $this->assertEquals($errorDataArray, $setErrorData);
    }
}

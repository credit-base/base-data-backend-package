<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class BjItemsDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'bj_items_data_id',
            new Persistence\BjItemsDataCache(),
            new Persistence\BjItemsDataDb()
        );
    }
}

<?php
namespace BaseData\Task\Adapter\NotifyRecord\Query\Persistence;

use Marmot\Framework\Classes\Db;

class NotifyRecordDb extends Db
{
    const TABLE = 'schedule_task_notify_record';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

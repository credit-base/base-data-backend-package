<?php
namespace BaseData\Template\QzjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件资源目录时,在目录管理子系统下的前置机资源目录管理中,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录数
 * @Scenario: 通过公开范围搜索
 */
class DimensionTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchQzjTemplateList()
    {
        $repository = new QzjTemplateRepository();
        $filter['dimension'] = Template::DIMENSION['SHGK'];

        list($qzjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $qzjTemplateList;
    }

    /**
     * @Then: 我可以看到公开范围为社会公开的所有资源目录
     */
    public function testViewQzjTemplateList()
    {
        $dimensionQzjTemplateList = $this->qzjTemplate();

        foreach ($dimensionQzjTemplateList as $key => $dimensionQzjTemplate) {
            $dimensionQzjTemplateList[$key]['qzj_template_id'] = $key +1;
            $dimensionQzjTemplateList[$key]['subject_category'] = json_decode(
                $dimensionQzjTemplate['subject_category'],
                true
            );
            $dimensionQzjTemplateList[$key]['items'] = json_decode($dimensionQzjTemplate['items'], true);
        }

        $qzjTemplateList = $this->fetchQzjTemplateList();
        $translator = new QzjTemplateDbTranslator();
        foreach ($qzjTemplateList as $qzjTemplate) {
            $qzjTemplateArray[] = $translator->objectToArray($qzjTemplate);
        }

        $this->assertEquals($qzjTemplateArray, $dimensionQzjTemplateList);

        foreach ($qzjTemplateArray as $qzjTemplate) {
            $this->assertEquals(Template::DIMENSION['SHGK'], $qzjTemplate['dimension']);
        }
    }
}

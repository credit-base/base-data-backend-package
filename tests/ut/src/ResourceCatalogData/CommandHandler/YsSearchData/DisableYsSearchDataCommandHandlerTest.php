<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

class DisableYsSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDisableYsSearchDataCommandHandler::class)
                       ->setMethods(['fetchYsSearchData'])
                       ->getMock();
    }

    public function testExtendsDisableCommandHandler()
    {
        $this->assertInstanceOf(
            'BaseData\Common\CommandHandler\DisableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $ysSearchData = \BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory::generateYsSearchData($id);

        $this->stub->expects($this->once())
             ->method('fetchYsSearchData')
             ->with($id)
             ->willReturn($ysSearchData);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $ysSearchData);
    }
}

<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query\Persistence;

use Marmot\Framework\Classes\Db;

class BaseTemplateDb extends Db
{
    const TABLE = 'base_template';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

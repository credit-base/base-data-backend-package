<?php
namespace BaseData\Rule\Model;

class MockCompletionRule extends CompletionRule
{
    use MockConcretenessRuleTrait;
    
    public function validate() : bool
    {
        return parent::validate();
    }
}

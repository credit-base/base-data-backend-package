<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要将待确认的资源目录数据确认时,在资源目录数据子系统下的本级数据管理,确认对应的资源目录数据
 *           根据我核实需要确认的数据确认,以便于我能更好的实现数据的报送和平台的呈现
 * @Scenario: 确认资源目录数据
 */
class ConfirmTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * @Given: 存在需要确认的资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
            'pcore_bj_search_data' => $this->bjSearchData(BjSearchData::STATUS['CONFIRM'])['pcore_bj_search_data']
        ]);
    }

    /**
     * @When: 获取需要确认的资源目录数据
     */
    public function fetchBjSearchData($id)
    {
        $repository = new BjSearchDataRepository();

        $bjSearchData = $repository->fetchOne($id);

        return $bjSearchData;
    }

    /**
     * @And: 当我调用确认函数,期待返回true
     */
    public function confirm()
    {
        $bjSearchData = $this->fetchBjSearchData(1);
        
        return $bjSearchData->confirm();
    }

    /**
     * @Then: 数据已经被确认
     */
    public function testValidate()
    {
        $result = $this->confirm();

        $this->assertTrue($result);

        $bjSearchData = $this->fetchBjSearchData(1);

        $this->assertEquals(BjSearchData::STATUS['ENABLED'], $bjSearchData->getStatus());
        $this->assertEquals(Core::$container->get('time'), $bjSearchData->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $bjSearchData->getStatusTime());
    }
}

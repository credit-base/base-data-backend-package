<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class YsSearchDataSchemaTest extends TestCase
{
    private $ysSearchDataSchema;

    private $ysSearchData;

    public function setUp()
    {
        $this->ysSearchDataSchema = new YsSearchDataSchema(new Factory());

        $this->ysSearchData = \BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory::generateYsSearchData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->ysSearchDataSchema);
        unset($this->ysSearchData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->ysSearchDataSchema);
    }

    public function testGetId()
    {
        $result = $this->ysSearchDataSchema->getId($this->ysSearchData);

        $this->assertEquals($result, $this->ysSearchData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->ysSearchDataSchema->getAttributes($this->ysSearchData);

        $this->assertEquals($result['infoClassify'], $this->ysSearchData->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->ysSearchData->getInfoCategory());
        $this->assertEquals($result['subjectCategory'], $this->ysSearchData->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->ysSearchData->getDimension());
        $this->assertEquals($result['name'], $this->ysSearchData->getName());
        $this->assertEquals($result['identify'], $this->ysSearchData->getIdentify());
        $this->assertEquals($result['expirationDate'], $this->ysSearchData->getExpirationDate());
        $this->assertEquals($result['status'], $this->ysSearchData->getStatus());
        $this->assertEquals($result['createTime'], $this->ysSearchData->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->ysSearchData->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->ysSearchData->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->ysSearchDataSchema->getRelationships($this->ysSearchData, 0, array());

        $this->assertEquals($result['itemsData'], ['data' => $this->ysSearchData->getItemsData()]);
        $this->assertEquals($result['template'], ['data' => $this->ysSearchData->getTemplate()]);
        $this->assertEquals($result['sourceUnit'], ['data' => $this->ysSearchData->getSourceUnit()]);
        $this->assertEquals($result['crew'], ['data' => $this->ysSearchData->getCrew()]);
    }
}

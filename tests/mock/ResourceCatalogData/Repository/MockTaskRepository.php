<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter;

class MockTaskRepository extends TaskRepository
{
    public function getAdapter() : ITaskAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : ITaskAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : ITaskAdapter
    {
        return parent::getMockAdapter();
    }
}

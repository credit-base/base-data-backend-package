<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

class WbjSearchDataCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockWbjSearchDataCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository',
            $this->trait->publicGetRepository()
        );
    }

    public function testFetchWbjSearchData()
    {
        $trait = $this->getMockBuilder(
            MockWbjSearchDataCommandHandlerTrait::class
        )->setMethods(['getRepository'])->getMock();

        $id = 1;
        $wbjSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);

        $repository = $this->prophesize(WbjSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjSearchData);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchWbjSearchData($id);

        $this->assertEquals($result, $wbjSearchData);
    }
}

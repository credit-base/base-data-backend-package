<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService;

use BaseData\Rule\Model\UnAuditedRuleService;

interface IUnAuditedRuleServiceAdapter
{
    public function fetchOne($id) : UnAuditedRuleService;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(UnAuditedRuleService $unAuditedRuleService) : bool;

    public function edit(UnAuditedRuleService $unAuditedRuleService, array $keys = array()) : bool;
}

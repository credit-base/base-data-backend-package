<?php
namespace BaseData\Task\Script;

use BaseData\Task\Model\OnceTask;

use BaseData\ResourceCatalogData\Service\SyncErrorDataDownloadService;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\TaskRepository;

class SyncErrorDataDownload extends OnceTask
{
    protected function getTaskRepository() : TaskRepository
    {
        return new TaskRepository();
    }

    protected function fetchTask(int $id) : Task
    {
        return $this->getTaskRepository()->fetchOne($id);
    }

    protected function getSyncErrorDataDownloadService(Task $task) : SyncErrorDataDownloadService
    {
        return new SyncErrorDataDownloadService($task);
    }

    public function executeAction() : bool
    {
        $data = $this->getData();

        $taskId = isset($data['task']) ? $data['task'] : 0;

        $task = $this->fetchTask($taskId);

        $syncErrorDataDownloadService = $this->getSyncErrorDataDownloadService($task);

        return $syncErrorDataDownloadService->execute();
    }
}

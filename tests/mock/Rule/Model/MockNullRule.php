<?php
namespace BaseData\Rule\Model;

class MockNullRule extends NullRule
{
    public function validate() : bool
    {
        return parent::validate();
    }
}

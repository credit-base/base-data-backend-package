<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Command\RuleService\RejectRuleServiceCommand;

use BaseData\Crew\Model\Crew;

class RejectRuleServiceCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new RejectRuleServiceCommandHandler();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectExtendsApproveCommandHandler()
    {
        $this->assertInstanceOf(
            'BaseData\Common\CommandHandler\RejectCommandHandler',
            $this->commandHandler
        );
    }

    public function testFetchIApplyObject()
    {
        $id = 1;
        $unAuditedRuleService = new UnAuditedRuleService();

        $commandHandler = $this->getMockBuilder(MockRejectRuleServiceCommandHandler::class)
                                ->setMethods(['fetchUnAuditedRuleService'])
                                ->getMock();

        $commandHandler->expects($this->once())
                       ->method('fetchUnAuditedRuleService')
                       ->with($id)
                       ->willReturn($unAuditedRuleService);

        $result = $commandHandler->fetchIApplyObject($id);
        $this->assertEquals($unAuditedRuleService, $result);
    }

    public function testExecuteAction()
    {
        //初始化
        $id = 1;
        $rejectReason = 'rejectReason';
        $applyCrew = 2;
        $command = new RejectRuleServiceCommand(
            $applyCrew,
            $rejectReason,
            $id
        );

        $crew = new Crew();

        $commandHandler = $this->getMockBuilder(MockRejectRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         ['fetchIApplyObject','fetchCrew']
                                     )
                                     ->getMock();

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->setRejectReason(Argument::exact($rejectReason))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->setApplyCrew(Argument::exact($crew))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->reject()
                             ->shouldBeCalledTimes(1)
                             ->willReturn(true);

        $commandHandler->expects($this->once())
                       ->method('fetchIApplyObject')
                       ->with($command->id)
                       ->willReturn($unAuditedRuleService->reveal());

        $commandHandler->expects($this->once())
                       ->method('fetchCrew')
                       ->with($command->applyCrew)
                       ->willReturn($crew);

        $result = $commandHandler->execute($command);
        $this->assertTrue($result);
    }
}

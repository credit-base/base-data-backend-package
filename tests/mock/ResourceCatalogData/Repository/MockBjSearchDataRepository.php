<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;

class MockBjSearchDataRepository extends BjSearchDataRepository
{
    public function getAdapter() : IBjSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBjSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBjSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\Persistence;

class MockBjSearchDataCache extends BjSearchDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

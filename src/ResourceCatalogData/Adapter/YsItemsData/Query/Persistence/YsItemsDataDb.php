<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class YsItemsDataDb extends Db
{
    const TABLE = 'ys_items_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

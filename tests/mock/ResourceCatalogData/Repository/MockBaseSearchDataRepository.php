<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter;

class MockBaseSearchDataRepository extends BaseSearchDataRepository
{
    public function getAdapter() : IBaseSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBaseSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBaseSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}

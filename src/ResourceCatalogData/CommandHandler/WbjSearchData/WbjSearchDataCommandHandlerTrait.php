<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

trait WbjSearchDataCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new WbjSearchDataRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : WbjSearchDataRepository
    {
        return $this->repository;
    }

    protected function fetchWbjSearchData(int $id) : WbjSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}

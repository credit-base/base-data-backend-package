<?php
namespace BaseData\Rule\RuleService\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RuleServiceRepository;

/**
 * @Feature: 超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则删除权限,在政务网OA的资源目录规则管理中,可以对设置的规则进行删除
 *           可以查看使用规则的情况或删除已经设置好的规则,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 删除规则数据
 */
class DeleteTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
    }

    /**
     * @Given: 存在需要删除的规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_rule' => $this->rule(),
        ]);
    }

    /**
     * @When: 获取需要删除的规则数据
     */
    public function fetchRule($id)
    {
        $repository = new RuleServiceRepository();

        $rule = $repository->fetchOne($id);

        return $rule;
    }

    /**
     * @And: 当我调用删除函数,期待返回true
     */
    public function delete()
    {
        $rule = $this->fetchRule(1);
        
        return $rule->delete();
    }

    /**
     * @Then: 数据已经被删除
     */
    public function testValidate()
    {
        $result = $this->delete();

        $this->assertTrue($result);

        $rule = $this->fetchRule(1);

        $this->assertEquals(RuleService::STATUS['DELETED'], $rule->getStatus());
        $this->assertEquals(Core::$container->get('time'), $rule->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $rule->getStatusTime());
    }
}

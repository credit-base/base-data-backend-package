<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\Persistence;

class MockGbItemsDataCache extends GbItemsDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

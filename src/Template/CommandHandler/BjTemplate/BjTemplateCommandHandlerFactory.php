<?php
namespace BaseData\Template\CommandHandler\BjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class BjTemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\Template\Command\BjTemplate\AddBjTemplateCommand' =>
        'BaseData\Template\CommandHandler\BjTemplate\AddBjTemplateCommandHandler',
        'BaseData\Template\Command\BjTemplate\EditBjTemplateCommand' =>
        'BaseData\Template\CommandHandler\BjTemplate\EditBjTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

# 本级目录接口文档

---

## 简介

本文档主要描述本级资源目录模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [控件规范](#控件规范)
    * [错误规范](#错误规范)
    * [接口错误返回说明](#接口错误返回说明) 
    * [mock接口请求说明](#mock接口请求说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [新增](#新增)
    * [编辑](#编辑)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [通用项目字典](./docs/Dictionary/common.md "通用项目字典")
    * [目录项目字典](./docs/Dictionary/template.md "目录项目字典")
    * [本级目录项目字典](./docs/Dictionary/bjTemplate.md "本级目录项目字典")
* <a name="控件规范">控件规范</a>
* <a name="错误规范">错误规范</a>
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 
* <a name="mock接口请求说明">mock接口请求说明</a>
    * [mock接口请求说明](./docs/Api/mockApi.md "mock接口请求说明") 

## <a name="参数说明">参数说明</a>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=sourceUnit
    1.2 include=gbTemplate
2、fields[TYPE]请求参数
    2.1 fields[bjTemplates]
    2.2 fields[userGroups]
    2.3 fields[gbTemplates]
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request(
    'GET', 
    'bjTemplates/1?fields[bjTemplates]=name,identify',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/bjTemplates/{id:\d+}
```

示例

```php
$response = $client->request(
    'GET', 
    'bjTemplates/1',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/bjTemplates/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request(
    'GET', 
    'bjTemplates/1,2,3',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/bjTemplates

1、检索条件
    1.1 filter[name] | 根据 目录名称 搜索
    1.2 filter[identify] | 根据 目录标识 搜索
    1.3 filter[sourceUnit] | 根据 来源单位 搜索 | 委办局id
    1.4 filter[subjectCategory] | 根据 主体类别 搜索 | 法人及非法人组织 1 | 自然人 2 | 个体工商户 3
    1.5 filter[dimension] | 根据 公开范围 搜索 | 社会公开 1 | 政务共享 2 | 授权查询 3
    1.6 filter[infoClassify] | 根据 信息分类 搜索
    1.7 filter[infoCategory] | 根据 信息类别 搜索
2、排序
    2.1 sort=-id | -id 根据更新时间倒序 | id 根据更新时间正序
    2.2 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
```

示例

```php
$response = $client->request(
    'GET', 
    'bjTemplates?filter[sourceUnit]=3&sort=-id',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="新增">新增示例</a>

路由

```
通过POST传参
/bjTemplates
```

示例

```php
$data = array(
    "data" => array(
        "type" => "bjTemplates",
        "attributes" => array(
            "name" => '登记信息',    //目录名称
            "identify" => 'DJXX',    //目录标识
            "subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "exchangeFrequency" => 1,    //更新频率
            "infoClassify" => 1,    //信息分类
            "infoCategory" => 1,    //信息类别
            "description" => "目录描述信息",    //目录描述
            "items" => array(
                array(
                    "name" => '主体名称',    //信息项名称
                    "identify" => 'ZTMC',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '200',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(),    //脱敏规则
                    "remarks" => '信用主体名称',    //备注
                ),
                array(
                    "name" => '统一社会信用代码',    //信息项名称
                    "identify" => 'TYSHXYDM',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                    "remarks" => '信用主体代码',    //备注
                ),
                array(
                    "name" => '信息类别',    //信息项名称
                    "identify" => 'XXLB',    //数据标识
                    "type" => 5,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(
                        "基础信息",
                        "守信信息",
                        "失信信息",
                        "其他信息"
                    ),    //可选范围
                    "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(1, 2),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                    "remarks" => '信息性质类型，支持单选',    //备注
                )
            )
        ),
        "relationships" => array(
            "sourceUnit" => array( // 来源单位
                "data" => array(
                    array("type" => "userGroups", "id" => 1)
                )
            ),
            "gbTemplate" => array( // 国标目录
                "data" => array(
                    array("type" => "gbTemplates", "id" => 1)
                )
            )
        )
    )
);
$response = $client->request(
    'POST',
    'bjTemplates',
    [
        'headers'=>['Content-Type' => 'application/vnd.api+json'],
        'json' => $data
    ]
);
```

### <a name="编辑">编辑示例</a>

路由

```
通过PATCH传参
/bjTemplates/{id:\d+}
```

示例

```php
$data = array(
    "data" => array(
        "type" => "bjTemplates",
        "attributes" => array(
            "name" => '登记信息',    //目录名称
            "identify" => 'DJXX',    //目录标识
            "subjectCategory" => array(1, 3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "exchangeFrequency" => 1,    //更新频率
            "infoClassify" => 1,    //信息分类
            "infoCategory" => 1,    //信息类别
            "description" => "目录描述信息",    //目录描述
            "items" => array(
                array(
                    "name" => '主体名称',    //信息项名称
                    "identify" => 'ZTMC',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '200',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(),    //脱敏规则
                    "remarks" => '信用主体名称',    //备注
                ),
                array(
                    "name" => '统一社会信用代码',    //信息项名称
                    "identify" => 'TYSHXYDM',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                    "remarks" => '信用主体代码',    //备注
                ),
                array(
                    "name" => '信息类别',    //信息项名称
                    "identify" => 'XXLB',    //数据标识
                    "type" => 5,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(
                        "基础信息",
                        "守信信息",
                        "失信信息",
                        "其他信息"
                    ),    //可选范围
                    "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(1, 2),    //脱敏规则，即左边保留1位字符，右边保留2位字符
                    "remarks" => '信息性质类型，支持单选',    //备注
                )
            )
        ),
        "relationships" => array(
            "sourceUnit" => array( // 来源单位
                "data" => array(
                    array("type" => "userGroups", "id" => 1)
                )
            ),
            "gbTemplate" => array( // 国标目录
                "data" => array(
                    array("type" => "gbTemplates", "id" => 1)
                )
            )
        )
    )
);
$response = $client->request(
    'PATCH',
    'bjTemplates/1',
    [
        'headers'=>['Content-Type' => 'application/vnd.api+json'],
        'json' => $data
    ]
);
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

```json
{
    "meta": [],
    "data": {
        "type": "bjTemplates",
        "id": "1",
        "attributes": {
            "name": "登记信息",
            "identify": "DJXX",
            "subjectCategory": [
                1,
                3
            ],
            "dimension": 1,
            "exchangeFrequency": 1,
            "infoClassify": 1,
            "infoCategory": 1,
            "description": "目录描述信息",
            "category": 1,
            "items": [{
                    "name": "主体名称",
                    "identify": "ZTMC",
                    "type": 1,
                    "length": 200,
                    "options": [],
                    "dimension": 1,
                    "isNecessary": 1,
                    "isMasked": 0,
                    "maskRule": [],
                    "remarks": "信用主体名称"
                },
                {
                    "name": "统一社会信用代码",
                    "identify": "TYSHXYDM",
                    "type": 1,
                    "length": 50,
                    "options": [],
                    "dimension": 1,
                    "isNecessary": 1,
                    "isMasked": 1,
                    "maskRule": [3, 4],
                    "remarks": "信用主体代码"
                },
                {
                    "name": "信息类别",
                    "identify": "XXLB",
                    "type": 5,
                    "length": 50,
                    "options": [
                        "基础信息",
                        "守信信息",
                        "失信信息",
                        "其他信息"
                    ],
                    "dimension": 2,
                    "isNecessary": 1,
                    "isMasked": 1,
                    "maskRule": [1, 2],
                    "remarks": "信息性质类型，支持单选"
                }
            ],
            "status": 0,
            "createTime": 1531897790,
            "updateTime": 1531897790,
            "statusTime": 0
        },
        "relationships": {
            "sourceUnit": {
                "data": {
                    "type": "userGroups",
                    "id": "1"
                }
            },
            "gbTemplate": {
                "data": {
                    "type": "gbTemplates",
                    "id": "1"
                }
            }
        },
        "links": {
            "self": "127.0.0.1:8080/bjTemplates/1"
        }
    }
}
```

#### <a name="多条示例">多条示例</a>

```json
{
    "meta": {
        "count": 2,
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        }
    },
    "links": {
        "first": null,
        "last": null,
        "prev": null,
        "next": null
    },
    "data": [{
            "type": "bjTemplates",
            "id": "1",
            "attributes": {
                "name": "登记信息",
                "identify": "DJXX",
                "subjectCategory": [
                    1,
                    3
                ],
                "dimension": 1,
                "exchangeFrequency": 1,
                "infoClassify": 1,
                "infoCategory": 1,
                "description": "目录描述信息",
                "category": 1,
                "items": [{
                        "name": "主体名称",
                        "identify": "ZTMC",
                        "type": 1,
                        "length": 200,
                        "options": [],
                        "dimension": 1,
                        "isNecessary": 1,
                        "isMasked": 0,
                        "maskRule": [],
                        "remarks": "信用主体名称"
                    },
                    {
                        "name": "统一社会信用代码",
                        "identify": "TYSHXYDM",
                        "type": 1,
                        "length": 50,
                        "options": [],
                        "dimension": 1,
                        "isNecessary": 1,
                        "isMasked": 1,
                        "maskRule": [3, 4],
                        "remarks": "信用主体代码"
                    },
                    {
                        "name": "信息类别",
                        "identify": "XXLB",
                        "type": 5,
                        "length": 50,
                        "options": [
                            "基础信息",
                            "守信信息",
                            "失信信息",
                            "其他信息"
                        ],
                        "dimension": 2,
                        "isNecessary": 1,
                        "isMasked": 1,
                        "maskRule": [1, 2],
                        "remarks": "信息性质类型，支持单选"
                    }
                ],
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "relationships": {
                "sourceUnit": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                },
                "gbTemplate": {
                    "data": {
                        "type": "gbTemplates",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8080/bjTemplates/1"
            }
        },
        {
            "type": "bjTemplates",
            "id": "2",
            "attributes": {
                "name": "纳税信息",
                "identify": "NSXX",
                "subjectCategory": [
                    1,
                    3
                ],
                "dimension": 1,
                "exchangeFrequency": 1,
                "infoClassify": 1,
                "infoCategory": 1,
                "description": "目录描述信息",
                "category": 1,
                "items": [{
                        "name": "主体名称",
                        "identify": "ZTMC",
                        "type": 1,
                        "length": 200,
                        "options": [],
                        "dimension": 1,
                        "isNecessary": 1,
                        "isMasked": 0,
                        "maskRule": [],
                        "remarks": "信用主体名称"
                    },
                    {
                        "name": "统一社会信用代码",
                        "identify": "TYSHXYDM",
                        "type": 1,
                        "length": 50,
                        "options": [],
                        "dimension": 1,
                        "isNecessary": 1,
                        "isMasked": 1,
                        "maskRule": [3, 4],
                        "remarks": "信用主体代码"
                    },
                    {
                        "name": "信息类别",
                        "identify": "XXLB",
                        "type": 5,
                        "length": 50,
                        "options": [
                            "基础信息",
                            "守信信息",
                            "失信信息",
                            "其他信息"
                        ],
                        "dimension": 2,
                        "isNecessary": 1,
                        "isMasked": 1,
                        "maskRule": [1, 2],
                        "remarks": "信息性质类型，支持单选"
                    }
                ],
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "relationships": {
                "sourceUnit": {
                    "data": {
                        "type": "userGroups",
                        "id": "1"
                    }
                },
                "gbTemplate": {
                    "data": {
                        "type": "gbTemplates",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8080/bjTemplates/2"
            }
        }
    ]
}
```
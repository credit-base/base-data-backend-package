<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\Model\IProgress;
use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\WorkOrderTask\Model\WorkOrderTask;

/**
 * 测试工单任务接口确认
 */
class ConfirmTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_work_order_task');
    }

    /**
     * 存在一条工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_parent_task' => $this->parentTask(),
                'pcore_work_order_task' => $this->workOrderTask(),
            ]
        );
    }

    public function testViewWorkOrderTask()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'workOrderTasks/1/confirm',
            ['headers' => Core::$container->get('client.headers')]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IProgress::STATUS['YQR'], $attributes['status']);
    }
}

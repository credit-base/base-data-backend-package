<?php
namespace BaseData\ResourceCatalogData\Strategy;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class FloatStrategyTest extends TestCase
{
    private $floatStub;
    
    public function setUp()
    {
        $this->floatStub = new FloatStrategy();
    }

    public function tearDown()
    {
        unset($this->floatStub);
    }

    public function testImplementsIDataStrategy()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Strategy\IDataStrategy',
            $this->floatStub
        );
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($actual, $expected)
    {
        $result = $this->floatStub->validate($actual);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals('data', Core::getLastError()->getSource()['pointer']);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomFloat(2, 0, 99999999), true),
            array([['value'=>$faker->randomFloat(2, 0, 99999999)]], false)
        );
    }

    public function testMock()
    {
        $result = $this->floatStub->mock();
        
        $this->assertIsFloat($result);
    }
}

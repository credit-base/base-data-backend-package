<?php
namespace BaseData\ResourceCatalogData\Adapter;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

use BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockSearchDataDbAdapterTrait
{
    use SearchDataDbAdapterTrait;

    protected function getDbTranslator() : ITranslator
    {
        return new WbjSearchDataDbTranslator();
    }

    protected function getTemplateRepository()
    {
    }

    protected function getItemsDataRepository()
    {
    }

    public function publicFetchTemplate($searchData)
    {
        return $this->fetchTemplate($searchData);
    }

    public function publicFetchTemplateByObject(ISearchDataAble $searchData)
    {
        return $this->fetchTemplateByObject($searchData);
    }

    public function publicFetchTemplateByList(array $searchDataList)
    {
        return $this->fetchTemplateByList($searchDataList);
    }

    public function publicFetchItemsData($searchData)
    {
        return $this->fetchItemsData($searchData);
    }

    public function publicFetchItemsDataByObject(ISearchDataAble $searchData)
    {
        return $this->fetchItemsDataByObject($searchData);
    }

    public function publicFetchItemsDataByList(array $searchDataList)
    {
        return $this->fetchItemsDataByList($searchDataList);
    }

    public function publicSearchDataFormatFilter(array $filter, ISearchDataAble $searchData)
    {
        return $this->searchDataFormatFilter($filter, $searchData);
    }
}

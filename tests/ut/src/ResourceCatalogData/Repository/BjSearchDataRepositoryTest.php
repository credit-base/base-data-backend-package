<?php
namespace BaseData\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use BaseData\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataDbAdapter;

use BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory;

class BjSearchDataRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(BjSearchDataRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockBjSearchDataRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new BjSearchDataDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\BjSearchDataMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData($id);
        $keys = array();
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->add(Argument::exact($bjSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($bjSearchData, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $bjSearchData = BjSearchDataMockFactory::generateBjSearchData($id);
        $keys = array();
        
        $adapter = $this->prophesize(IBjSearchDataAdapter::class);
        $adapter->edit(Argument::exact($bjSearchData), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($bjSearchData, $keys);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\BjItemsData;
use BaseData\ResourceCatalogData\Model\NullBjItemsData;
use BaseData\ResourceCatalogData\Translator\BjItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\BjItemsDataRowCacheQuery;

class BjItemsDataDbAdapter implements IBjItemsDataAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new BjItemsDataDbTranslator();
        $this->rowCacheQuery = new BjItemsDataRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : BjItemsDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : BjItemsDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullBjItemsData::getInstance();
    }

    public function add(BjItemsData $bjItemsData) : bool
    {
        return $this->addAction($bjItemsData);
    }
    
    public function fetchOne($id) : BjItemsData
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        unset($filter);
        unset($conjection);
        
        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        unset($sort);
        unset($conjection);

        return $condition;
    }
}

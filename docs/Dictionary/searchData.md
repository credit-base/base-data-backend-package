# 资源目录数据搜索字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### infoClassify

**信息分类** 

* int
	* XZXK => 1 | 行政许可
	* XZCF => 2 | 行政处罚
	* HONGMD => 3 | 红名单
	* HEIMD => 4 | 黑名单
	* QT => 5 | 其他
* 必填项

### infoCategory

**信息类别** 

* int
	* JCXX => 1 | 基础信息
	* SHOUXXX => 2 | 守信信息
	* SHIXXX => 3 | 失信信息
	* QTXX => 4 | 其他信息

* 必填项

### crew

**发布人** 

* Crew
* 必填项

### sourceUnit

**来源单位** 

* UserGroup
* 必填项

### subjectCategory

**主体类别** 

* int
	* FRJFFRZZ => 1 | 法人及非法人组织
	* ZRR => 2 | 自然人
	* GTGSH => 3 | 个体工商户
* 必填项

### dimension

**公开范围** 

* int
	* SHGK => 1 | 社会公开
	* ZWGX => 2 | 政务共享
	* SQCX => 3 | 授权查询
* 必填项

### name

**主体名称** 

* string
* 必填项

### identify

**主体标识** 

* string
* 必填项

### expirationDate

**有效期限** 

* int
* 选填项

### task

**父任务** 父任务的表述.

* Task
* 选填项

### subtask

**子任务** 子任务的表述.

* SubTask
* 选填项

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

## 资源目录数据

### data

**资源目录数据** 

* array
* 必填项
<?php
namespace BaseData\Rule\Model;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

abstract class Rule
{
    protected $condition;

    protected $transformationTemplate;

    protected $sourceTemplate;

    public function __construct()
    {
        $this->condition = array();
        $this->transformationTemplate = new Template();
        $this->sourceTemplate = new Template();
    }

    public function __destruct()
    {
        unset($this->condition);
        unset($this->transformationTemplate);
        unset($this->sourceTemplate);
    }

    public function setCondition(array $condition) : void
    {
        $this->condition = $condition;
    }

    public function getCondition()
    {
        return $this->condition;
    }

    public function setTransformationTemplate(Template $transformationTemplate)
    {
        $this->transformationTemplate = $transformationTemplate;
    }

    public function getTransformationTemplate() : Template
    {
        return $this->transformationTemplate;
    }

    public function setSourceTemplate(Template $sourceTemplate)
    {
        $this->sourceTemplate = $sourceTemplate;
    }

    public function getSourceTemplate() : Template
    {
        return $this->sourceTemplate;
    }

    abstract public function validate() : bool;

    /**
     * 数据转换
     * @param ISearchDataAble $sourceSearchData 来源资源目录数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return ISearchDataAble
     */
    public function transformation(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        //验证
        if (!$this->validateTransformation($transformationSearchData)) {
            return $transformationSearchData;
        }
        //初始化数据
        $initializationData = $this->transformationInitialization($sourceSearchData, $transformationSearchData);
        //执行转换过程
        $data = $this->transformationExecute($initializationData, $transformationSearchData);
        //判断并返回结果
        return $this->transformationResult($data, $transformationSearchData);
    }

    abstract protected function validateTransformation(ISearchDataAble $transformationSearchData) : bool;
    abstract protected function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array;
    abstract protected function transformationExecute(array $data, ISearchDataAble $transformationSearchData) : array;
    abstract protected function transformationResult(
        array $data,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble;
}

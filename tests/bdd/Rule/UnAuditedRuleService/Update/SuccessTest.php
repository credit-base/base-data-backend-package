<?php
namespace BaseData\Rule\UnAuditedRuleService\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;

use BaseData\Common\Model\IApproveAble;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限,在政务网OA的资源目录规则管理审核表中,我可以管理我提交的资源目录的规则
 *           可以重新编辑审核状态为已驳回的资源目录规则,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 正常编辑规则审核数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form_rule' => $this->applyFormRule(IApproveAble::APPLY_STATUS['REJECT'])
        ]);
    }

    /**
     * @When: 获取需要编辑的规则数据
     */
    public function fetchUnAuditedRuleService($id)
    {
        $repository = new UnAuditedRuleServiceRepository();

        $unAuditedRuleService = $repository->fetchOne($id);

        return $unAuditedRuleService;
    }
    /**
     * @When: 当我调用重新编辑函数,期待返回true
     */
    public function resubmit()
    {
        $unAuditedRuleService = $this->fetchUnAuditedRuleService(1);

        $unAuditedRuleService->setApplyInfo(array('测试重新编辑'));

        return $unAuditedRuleService->resubmit();
    }

    /**
     * @Then: 可以查到重新编辑的数据
     */
    public function testValidate()
    {
        $result = $this->resubmit();

        $this->assertTrue($result);
        
        $unAuditedRuleService = $this->fetchUnAuditedRuleService(1);

        $this->assertEquals(array('测试重新编辑'), $unAuditedRuleService->getApplyInfo());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $unAuditedRuleService->getApplyStatus());
    }
}

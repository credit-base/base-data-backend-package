<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\GbSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的国标数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_items_data');
        $this->clear('pcore_gb_search_data');
    }

    /**
     * @Given: 存在一条资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data'],
            'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data']
        ]);
    }

    /**
     * @When: 当我查看该条资源目录数据详情时
     */
    public function fetchGbSearchData($id)
    {
        $repository = new GbSearchDataRepository();

        $gbSearchData = $repository->fetchOne($id);

        return $gbSearchData;
    }

    /**
     * @Then: 我可以看见该条资源目录数据的全部信息
     */
    public function testViewGbSearchDataList()
    {
        $id = 1;
        
        $setGbSearchData = $this->gbSearchData()['pcore_gb_search_data'][0];
        $setGbSearchData['gb_search_data_id'] = $id;

        $gbSearchData = $this->fetchGbSearchData($id);
        $translator = new GbSearchDataDbTranslator();
        $gbSearchDataArray = $translator->objectToArray($gbSearchData);

        $this->assertEquals($gbSearchDataArray, $setGbSearchData);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\Model\EnableAbleTrait;

use BaseData\Template\Model\BaseTemplate;

use BaseData\ResourceCatalogData\Repository\BaseSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter;

class BaseSearchData extends SearchData implements IEnableAble
{
    use EnableAbleTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->template = new BaseTemplate();
        $this->itemsData = new BaseItemsData();
        $this->repository = new BaseSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->template);
        unset($this->itemsData);
        unset($this->repository);
    }

    public function setItemsData(BaseItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : BaseItemsData
    {
        return $this->itemsData;
    }

    protected function getRepository() : IBaseSearchDataAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        $this->assignment();
        return $this->validate() && $this->getRepository()->add($this);
    }
}

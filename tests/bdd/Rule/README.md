# 规则BDD测试说明

## 背景

为了测试规则数据转换，需要针对各项数据做好规则准备。这里会存放各项数据的准备<font color="red">元数据</font>

## 数据

### 1. 行政许可 <font color="red">国标</font>

#### 1.1 数据

数据项 | 数据 | 备注
----- | ---- | -----
行政相对人名称 | | |
行政相对人类别 | | |
行政相对人代码_1(统一社会信用代码) | | |
行政相对人代码_3(工商登记码) | | |
行政相对人代码_2(组织机构代码) | | |
行政相对人代码_4(税务登记号) | | |
事业单位证书号 | | |
社会组织登记证号 | | |
法定代表人 | | |
法定代表人证件类型 | | |
法定代表人证件号码 | | |
证件类型 | | |
证件号码 | | |
行政许可决定文书名称 | | |
行政许可决定文书号 | | |
许可类别 | | |
许可证书名称 | | |
许可编号 | | |
许可内容 | | |
许可决定日期 | | |
有效期自 | | |
有效期至 | | |
许可机关 | | |
许可机关统一社会信用代码 | | |
当前状态 | | |
数据来源单位 | | |
数据来源单位统一社会信用代码 | | |
备注 | | |

#### 1.2 接口

##### 1.2.1 模板

模板`id`为<font color='red'>1</font>

```php
//行政许可模板信息
$template = array(
    "data" => array(
        "type" => "gbTemplates",
        "attributes" => array(
            "name" => '行政许可信息',    //目录名称
            "identify" => 'XZXK',    //目录标识
            "subjectCategory" => array(1,3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "exchangeFrequency" => 1,    //更新频率
            "infoClassify" => 1,    //信息分类
            "infoCategory" => 1,    //信息类别
            "description" => "行政许可信息",    //目录描述
            "items" => array(
            )
        ),
        "relationships" => array(
            "sourceUnit" => array( // 来源单位
                "data" => array(
                    array("type" => "userGroups", "id" => 1)
                )
            )
        )
    )
);

//type 1.字符型 2.日期型 3.整数型 4.浮点型 5.枚举型 6.集合型
$items = [];

$itemsTemplate = [
	'name' => '',
	'identify' => '',
	'type' => '1',//字符型
	'length' => '',
	'options' => [],
	'dimension' => 1,//社会公开
	'isNecessary' => 1,//必填
	'isMasked' => 0,//不脱敏
	'maskRule' => [],
	'remarks' => ''
];

//行政相对人名称
$items[0] = $itemsTemplate;
$items[0]['name'] = '行政相对人名称';
$items[0]['identify'] = 'ZTMC';
$items[0]['length'] = '200';
$items[0]['remarks'] = '信用主体的法定名称';

//行政相对人类别
$items[1] = $itemsTemplate;
$items[1]['name'] = '行政相对人类别';
$items[1]['identify'] = 'XK_XDR_LB';
$items[1]['length'] = '16';

//行政相对人代码_1(统一社会信用代码)
$items[2] = $itemsTemplate;
$items[2]['name'] = '行政相对人代码_1(统一社会信用代码)';
$items[2]['identify'] = 'TYSHXYDM';
$items[2]['length'] = '18';

//行政相对人代码_3(工商登记码)
$items[3] = $itemsTemplate;
$items[3]['name'] = '行政相对人代码_3(工商登记码)';
$items[3]['identify'] = 'XK_XDR_GSZC';
$items[3]['length'] = '50';

//行政相对人代码_2(组织机构代码)
$items[4] = $itemsTemplate;
$items[4]['name'] = '行政相对人代码_2(组织机构代码)';
$items[4]['identify'] = 'XK_XDR_ZZJG';
$items[4]['length'] = '9';

//行政相对人代码_4(税务登记号)
$items[5] = $itemsTemplate;
$items[5]['name'] = '行政相对人代码_4(税务登记号)';
$items[5]['identify'] = 'XK_XDR_SWDJ';
$items[5]['length'] = '15';

//事业单位证书号
$items[6] = $itemsTemplate;
$items[6]['name'] = '事业单位证书号';
$items[6]['identify'] = 'XK_XDR_SYDW';
$items[6]['length'] = '12';

//社会组织登记证号
$items[7] = $itemsTemplate;
$items[7]['name'] = '社会组织登记证号';
$items[7]['identify'] = 'XK_XDR_SHZZ';
$items[7]['length'] = '50';

//法定代表人
$items[8] = $itemsTemplate;
$items[8]['name'] = '法定代表人';
$items[8]['identify'] = 'XK_FRDB';
$items[8]['length'] = '50';

//法定代表人证件类型
$items[9] = $itemsTemplate;
$items[9]['name'] = '法定代表人证件类型';
$items[9]['identify'] = 'XK_FR_ZJLX';
$items[9]['length'] = '64';

//法定代表人证件号码
$items[10] = $itemsTemplate;
$items[10]['name'] = '法定代表人证件号码';
$items[10]['identify'] = 'XK_FR_ZJHM';
$items[10]['length'] = '64';

//证件类型
$items[11] = $itemsTemplate;
$items[11]['name'] = '证件类型';
$items[11]['identify'] = 'XK_XDR_ZJLX';
$items[11]['length'] = '64';

//证件号码
$items[12] = $itemsTemplate;
$items[12]['name'] = '证件号码';
$items[12]['identify'] = 'XK_XDR_ZJHM';
$items[12]['length'] = '64';

//行政许可决定文书名称
$items[13] = $itemsTemplate;
$items[13]['name'] = '行政许可决定文书名称';
$items[13]['identify'] = 'XK_XKWS';
$items[13]['length'] = '64';

//行政许可决定文书号
$items[14] = $itemsTemplate;
$items[14]['name'] = '行政许可决定文书号';
$items[14]['identify'] = 'XK_WSH';
$items[14]['length'] = '64';

//许可类别
$items[15] = $itemsTemplate;
$items[15]['name'] = '许可类别';
$items[15]['identify'] = 'XK_XKLB';
$items[15]['length'] = '256';

//许可证书名称
$items[16] = $itemsTemplate;
$items[16]['name'] = '许可证书名称';
$items[16]['identify'] = 'XK_XKZS';
$items[16]['length'] = '64';

//许可编号
$items[17] = $itemsTemplate;
$items[17]['name'] = '许可编号';
$items[17]['identify'] = 'XK_XKBH';
$items[17]['length'] = '64';

//许可内容
$items[18] = $itemsTemplate;
$items[18]['name'] = '许可内容';
$items[18]['identify'] = 'XK_NR';
$items[18]['length'] = '4000';

//许可决定日期
$items[19] = $itemsTemplate;
$items[19]['name'] = '许可决定日期';
$items[19]['type'] = 2;
$items[19]['identify'] = 'XK_JDRQ';

//有效期自
$items[20] = $itemsTemplate;
$items[20]['name'] = '有效期自';
$items[20]['type'] = 2;
$items[20]['identify'] = 'XK_YXQZ';

//有效期至
$items[21] = $itemsTemplate;
$items[21]['name'] = '有效期至';
$items[21]['type'] = 2;
$items[21]['identify'] = 'XK_YXQZI';

//许可机关
$items[22] = $itemsTemplate;
$items[22]['name'] = '许可机关';
$items[22]['identify'] = 'XK_XKJG';
$items[22]['length'] = '200';

//许可机关统一社会信用代码
$items[23] = $itemsTemplate;
$items[23]['name'] = '许可机关统一社会信用代码';
$items[23]['identify'] = 'XK_XKJGDM';
$items[23]['length'] = '18';

//当前状态
$items[24] = $itemsTemplate;
$items[24]['name'] = '当前状态';
$items[24]['type'] = 3;
$items[24]['identify'] = 'XK_ZT';
$items[24]['length'] = '1';

//数据来源单位
$items[25] = $itemsTemplate;
$items[25]['name'] = '数据来源单位';
$items[25]['identify'] = 'XK_LYDW';
$items[25]['length'] = '200';

//数据来源单位统一社会信用代码
$items[26] = $itemsTemplate;
$items[26]['name'] = '数据来源单位统一社会信用代码';
$items[26]['identify'] = 'XK_LYDWDM';
$items[26]['length'] = '18';

//备注
$items[27] = $itemsTemplate;
$items[27]['name'] = '备注';
$items[27]['isNecessary'] = 0;
$items[27]['identify'] = 'BZ';
$items[27]['length'] = '4000';

$template['items'] = $items;
```

##### 1.2.1 数据

目标数据**无**

### 2. 法人登记信息  <font color="red">国标</font>

#### 2.1 数据

数据项 | 数据 | 备注
----- | ---- | -----
主体名称| 濮阳市嘉业运输有限公司| |
主体类别 | 企业法人| |
统一社会信用代码 | 91410900MA46184E7Q | |
法定代表人（负责人） | 张晓苹 | |
法定代表人（负责人）证件类型 | 身份证 |
法定代表人（负责人） 证件号码 |612301197209212055 |
成立日期 | 20171013| |
有效期 |20501013 | |
地址 |四川省彭州市桂花镇衡州村7组 | |
登记机关 |彭州市市场监督管理局 | |
国别(地区) |A24 | |
注册资本 |500 | |
注册资本币种 | 156| |
行业代码 |80 | |
类型 | N| |
经营范围 | 水利、环境和公共设施管理业| |
经营状态 | 1| |
经营范围描述 |旅游景区规划设计、开发、管理；健身休闲活动；餐饮服务；蔬菜、水果、花卉、苗木种植；住宿服务；百货零售；会议及展览服务；游乐园活动；室内娱乐活动；景区内游船出租活动；景区内的小动物拉车、骑马、钓鱼活动；歌舞厅娱乐活动[依法须经批准的项目，经相关部门批准后方可开展经营活动]。 | |

#### 2.2 接口

##### 2.2.1 模板

模板`id`为<font color='red'>2</font>

```php
//法人登记信息模板信息
$template = array(
    "data" => array(
        "type" => "gbTemplates",
        "attributes" => array(
            "name" => '法人登记信息',    //目录名称
            "identify" => 'FR_DJXX',    //目录标识
            "subjectCategory" => array(1,3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "exchangeFrequency" => 1,    //更新频率
            "infoClassify" => 1,    //信息分类
            "infoCategory" => 1,    //信息类别
            "description" => "行政许可信息",    //目录描述
            "items" => array(
            )
        ),
        "relationships" => array(
            "sourceUnit" => array( // 来源单位
                "data" => array(
                    array("type" => "userGroups", "id" => 1)
                )
            )
        )
    )
);

//type 1.字符型 2.日期型 3.整数型 4.浮点型 5.枚举型 6.集合型
$items = [];

$itemsTemplate = [
	'name' => '',
	'identify' => '',
	'type' => '1',//字符型
	'length' => '',
	'options' => [],
	'dimension' => 1,//社会公开
	'isNecessary' => 1,//必填
	'isMasked' => 0,//不脱敏
	'maskRule' => [],
	'remarks' => ''
];

//主体名称
$items[0] = $itemsTemplate;
$items[0]['name'] = '主体名称';
$items[0]['identify'] = 'ZTMC';
$items[0]['length'] = '200';
$items[0]['remarks'] = '在国家机关办理登记的主 体名称，以下同';

//主体类别
$items[1] = $itemsTemplate;
$items[1]['name'] = '主体类别';
$items[1]['identify'] = 'ZTLB';
$items[1]['length'] = '2';
$items[1]['remarks'] = '01=企业法人；02=机关法 人；03=事业单位法人； 04=社会团体法人；97= 其他法人；98=个体工商户；99=其他组织';

//统一社会信用代码
$items[2] = $itemsTemplate;
$items[2]['name'] = '统一社会信用代码';
$items[2]['identify'] = 'TYSHXYDM';
$items[2]['length'] = '18';
$items[2]['remarks'] = '采用 GB 32100-2015';

//法定代表人（负责人）
$items[3] = $itemsTemplate;
$items[3]['name'] = '法定代表人（负责人）';
$items[3]['identify'] = 'FDDBR';
$items[3]['length'] = '100';

//法定代表人（负责人）证件类型
$items[4] = $itemsTemplate;
$items[4]['name'] = '法定代表人（负责人）证件类型';
$items[4]['identify'] = 'FDDBRZJLX';
$items[4]['length'] = '3';
$items[4]['remarks'] = '采用 GA/T517《常用证件代码》';

//法定代表人（负责人）证件号码
$items[5] = $itemsTemplate;
$items[5]['name'] = '法定代表人（负责人）证件号码';
$items[5]['identify'] = 'FDDBRZJHM';
$items[5]['length'] = '18';

//成立日期
$items[6] = $itemsTemplate;
$items[6]['name'] = '成立日期';
$items[6]['identify'] = 'CLRQ';
$items[6]['type'] = 2;
$items[6]['remarks'] = '采用 GB/T7408，YYYYMMDD 格式';

//有效期
$items[7] = $itemsTemplate;
$items[7]['name'] = '有效期';
$items[7]['identify'] = 'YXQ';
$items[7]['type'] = 2;
$items[7]['remarks'] ='采用 GB/T7408，YYYYMMDD 格式';

//地址
$items[8] = $itemsTemplate;
$items[8]['name'] = '地址';
$items[8]['identify'] = 'DZ';
$items[8]['length'] = '200';
$items[8]['remarks'] = '按“___省___市___县(市、 区)_____”格式填报；直辖 市按照“___市___区 (县)___”格式填报。';

//登记机关
$items[9] = $itemsTemplate;
$items[9]['name'] = '登记机关';
$items[9]['identify'] = 'DJJG';
$items[9]['length'] = '200';
$items[8]['remarks'] = '登记机关的全称';

//国别(地区)
$items[10] = $itemsTemplate;
$items[10]['name'] = '国别(地区)';
$items[10]['identify'] = 'GB';
$items[10]['length'] = '3';
$items[10]['remarks'] = '采用 GB/T2659';

//注册资本
$items[11] = $itemsTemplate;
$items[11]['name'] = '注册资本';
$items[11]['identify'] = 'ZCZB';
$items[11]['type'] = 4;
$items[11]['remarks'] = '单位：万元';

//注册资本币种
$items[12] = $itemsTemplate;
$items[12]['name'] = '注册资本币种';
$items[12]['identify'] = 'ZCZBBZ';
$items[12]['length'] = '3';
$items[12]['remarks'] = '采用 GB/T12406';

//行业代码
$items[13] = $itemsTemplate;
$items[13]['name'] = '行业代码';
$items[13]['identify'] = 'HYDM';
$items[13]['length'] = '5';
$items[13]['remarks'] = '采用 GB/T12406';

//类型
$items[14] = $itemsTemplate;
$items[14]['name'] = '类型';
$items[14]['identify'] = 'LX';
$items[14]['length'] = '3';
$items[14]['remarks'] = '市场主体类型代码，参照 国家工商行政管理总局相 关标准';

//经营范围
$items[15] = $itemsTemplate;
$items[15]['name'] = '经营范围';
$items[15]['identify'] = 'JYFW';
$items[15]['length'] = '2000';

//经营状态
$items[16] = $itemsTemplate;
$items[16]['name'] = '经营状态';
$items[16]['identify'] = 'JYZT';
$items[16]['length'] = '1';
$items[16]['remarks'] = '1=存续(在营、开业、在册);2=吊销，未注销;3=吊 销，已注销;4=注销;5=撤 销;6=迁出;9=其他';

//经营范围描述
$items[17] = $itemsTemplate;
$items[17]['name'] = '经营范围描述';
$items[17]['identify'] = 'JYFWMS';
$items[17]['length'] = '2000';
```

##### 2.2.2 数据

```php
$itemsData = [
	'ZTMC' => '濮阳市嘉业运输有限公司',
	'ZTLB' => '企业法人',
	'TYSHXYDM' => '91410900MA46184E7Q',
	'FDDBR' => '张晓苹',
	'FDDBRZJLX' => '身份证',
	'FDDBRZJHM' => '612301197209212055',
	'CLRQ' => '20171013',
	'YXQ' => '20501013',
	'DZ' => '四川省彭州市桂花镇衡州村7组',
	'DJJG' => '彭州市市场监督管理局',
	'GB' => 'A24',
	'ZCZB' => '500',
	'ZCZBBZ' => '156',
	'HYDM' => '80',
	'LX' => 'N',
	'JYFW' => '水利、环境和公共设施管理业',
	'JYZT' => '1',
	'JYFWMS' => '旅游景区规划设计、开发、管理；健身休闲活动；餐饮服务；蔬菜、水果、花卉、苗木种植；住宿服务；百货零售；会议及展览服务；游乐园活动；室内娱乐活动；景区内游船出租活动；景区内的小动物拉车、骑马、钓鱼活动；歌舞厅娱乐活动[依法须经批准的项目，经相关部门批准后方可开展经营活动]。',
];

$data = array(
    "data" => array(
        "type" => "gbSearchData",
        "attributes" => array(
            "subjectCategory" => 1,    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "expirationDate" => 20991231,    //有效期限
        ),
        "relationships" => array(
            "crew" => array( // 来源单位
                "data" => array(
                    array("type" => "crews", "id" => 1)
                )
            ),
            "sourceUnit" => array( // 来源单位
                "data" => array(
                    array("type" => "userGroups", "id" => 1)
                )
            ),
            "template" => array( // 委办局资源目录
                "data" => array(
                    array("type" => "gbTemplates", "id" => 2)
                )
            ),
            "itemsData" => array( //资源目录数据
                "data" => array(
                    array(
                        "type" => "itemsData", 
                        "attributes" => array(
                            "data" => $itemsData;
                        )
                    )
                )
            )
        )
    )
);

```

### 3. 行政许可信息  <font color="red">委办局</font>

#### 3.1 数据

数据项 | 数据 | 备注
----- | ---- | -----
行政相对人名称 |濮阳市嘉业运输有限公司 | |
行政相对人类别 |法人及非法人组织 | |
行政相对人代码_1(统一社会信用代码) |91410900MA46184E7Q | |
行政相对人代码_2 (工商注册号) | | |
行政相对人代码_3(组织机构代码) | | |
行政相对人代码_4(税务登记号) | | |
行政相对人代码_5(事业单位证书号)| | |
行政相对人代码_6((社会组织登记证号) | MA46184E-7| |
法定代表人 | 张晓苹| |
法定代表人证件类型 | | |
法定代表人证件号码 | | |
证件类型 | | |
证件号码 | | |
行政许可决定文书名称 |高速公路联网管理中心交通行政许可决定书 | |
行政许可决定文书号 |豫高速公路许 字第20210705000090号 | |
许可类别 |普通 | |
许可证书名称 |高速公路超限运输车辆通行证 | |
许可编号 |410000017210705A000090 | |
许可内容 |高速公路超限运输车辆通行证 | |
许可决定日期 |20210714 | |
有效期自 | 20210714 | |
有效期至 | 20210807 | |
许可机关 |河南省高速公路联网管理中心 | |
许可机关统一社会信用代码 |12410000MB14527979 | |
当前状态 |1 | |
数据来源单位 | 河南省高速公路联网管理中心| |
数据来源单位统一社会信用代码 |12410000MB14527979 | |
备注 | | |

#### 3.2 接口

##### 3.2.1 模板

模板`id`为<font color='red'>1</font>

```php
//行政许可模板信息
$template = array(
    "data" => array(
        "type" => "templates",
        "attributes" => array(
            "name" => '行政许可信息',    //目录名称
            "identify" => 'XZXK',    //目录标识
            "subjectCategory" => array(1,3),    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "exchangeFrequency" => 1,    //更新频率
            "infoClassify" => 1,    //信息分类
            "infoCategory" => 1,    //信息类别
            "description" => "行政许可信息",    //目录描述
            "items" => array(
            )
        ),
        "relationships" => array(
            "sourceUnit" => array( // 来源单位
                "data" => array(
                    array("type" => "userGroups", "id" => 1)
                )
            )
        )
    )
);

//type 1.字符型 2.日期型 3.整数型 4.浮点型 5.枚举型 6.集合型
$items = [];

$itemsTemplate = [
	'name' => '',
	'identify' => '',
	'type' => '1',//字符型
	'length' => '',
	'options' => [],
	'dimension' => 1,//社会公开
	'isNecessary' => 1,//必填
	'isMasked' => 0,//不脱敏
	'maskRule' => [],
	'remarks' => ''
];

//行政相对人名称
$items[0] = $itemsTemplate;
$items[0]['name'] = '行政相对人名称';
$items[0]['identify'] = 'ZTMC';
$items[0]['length'] = '200';
$items[0]['remarks'] = '信用主体的法定名称';

//行政相对人类别
$items[1] = $itemsTemplate;
$items[1]['name'] = '行政相对人类别';
$items[1]['identify'] = 'XK_XDR_LB';
$items[1]['length'] = '16';

//行政相对人代码_1(统一社会信用代码)
$items[2] = $itemsTemplate;
$items[2]['name'] = '行政相对人代码_1(统一社会信用代码)';
$items[2]['identify'] = 'TYSHXYDM';
$items[2]['length'] = '18';

//行政相对人代码_3(工商登记码)
$items[3] = $itemsTemplate;
$items[3]['name'] = '行政相对人代码_2 (工商注册号)';
$items[3]['identify'] = 'XK_XDR_GSZC';
$items[3]['length'] = '50';

//行政相对人代码_2(组织机构代码)
$items[4] = $itemsTemplate;
$items[4]['name'] = '行政相对人代码_3(组织机构代码)';
$items[4]['identify'] = 'XK_XDR_ZZJG';
$items[4]['length'] = '9';

//行政相对人代码_4(税务登记号)
$items[5] = $itemsTemplate;
$items[5]['name'] = '行政相对人代码_4(税务登记号)';
$items[5]['identify'] = 'XK_XDR_SWDJ';
$items[5]['length'] = '15';

//事业单位证书号
$items[6] = $itemsTemplate;
$items[6]['name'] = '行政相对人代码_5(事业单位证书号)';
$items[6]['identify'] = 'XK_XDR_SYDW';
$items[6]['length'] = '12';

//社会组织登记证号
$items[7] = $itemsTemplate;
$items[7]['name'] = '行政相对人代码_6(社会组织登记证号)';
$items[7]['identify'] = 'XK_XDR_SHZZ';
$items[7]['length'] = '50';

//法定代表人
$items[8] = $itemsTemplate;
$items[8]['name'] = '法定代表人';
$items[8]['identify'] = 'XK_FRDB';
$items[8]['length'] = '50';

//法定代表人证件类型
$items[9] = $itemsTemplate;
$items[9]['name'] = '法定代表人证件类型';
$items[9]['identify'] = 'XK_FR_ZJLX';
$items[9]['length'] = '64';

//法定代表人证件号码
$items[10] = $itemsTemplate;
$items[10]['name'] = '法定代表人证件号码';
$items[10]['identify'] = 'XK_FR_ZJHM';
$items[10]['length'] = '64';

//证件类型
$items[11] = $itemsTemplate;
$items[11]['name'] = '证件类型';
$items[11]['identify'] = 'XK_XDR_ZJLX';
$items[11]['length'] = '64';

//证件号码
$items[12] = $itemsTemplate;
$items[12]['name'] = '证件号码';
$items[12]['identify'] = 'XK_XDR_ZJHM';
$items[12]['length'] = '64';

//行政许可决定文书名称
$items[13] = $itemsTemplate;
$items[13]['name'] = '行政许可决定文书名称';
$items[13]['identify'] = 'XK_XKWS';
$items[13]['length'] = '64';

//行政许可决定文书号
$items[14] = $itemsTemplate;
$items[14]['name'] = '行政许可决定文书号';
$items[14]['identify'] = 'XK_WSH';
$items[14]['length'] = '64';

//许可类别
$items[15] = $itemsTemplate;
$items[15]['name'] = '许可类别';
$items[15]['identify'] = 'XK_XKLB';
$items[15]['length'] = '256';

//许可证书名称
$items[16] = $itemsTemplate;
$items[16]['name'] = '许可证书名称';
$items[16]['identify'] = 'XK_XKZS';
$items[16]['length'] = '64';

//许可编号
$items[17] = $itemsTemplate;
$items[17]['name'] = '许可编号';
$items[17]['identify'] = 'XK_XKBH';
$items[17]['length'] = '64';

//许可内容
$items[18] = $itemsTemplate;
$items[18]['name'] = '许可内容';
$items[18]['identify'] = 'XK_NR';
$items[18]['length'] = '4000';

//许可决定日期
$items[19] = $itemsTemplate;
$items[19]['name'] = '许可决定日期';
$items[19]['type'] = 2;
$items[19]['identify'] = 'XK_JDRQ';

//有效期自
$items[20] = $itemsTemplate;
$items[20]['name'] = '有效期自';
$items[20]['type'] = 2;
$items[20]['identify'] = 'XK_YXQZ';

//有效期至
$items[21] = $itemsTemplate;
$items[21]['name'] = '有效期至';
$items[21]['type'] = 2;
$items[21]['identify'] = 'XK_YXQZI';

//许可机关
$items[22] = $itemsTemplate;
$items[22]['name'] = '许可机关';
$items[22]['identify'] = 'XK_XKJG';
$items[22]['length'] = '200';

//许可机关统一社会信用代码
$items[23] = $itemsTemplate;
$items[23]['name'] = '许可机关统一社会信用代码';
$items[23]['identify'] = 'XK_XKJGDM';
$items[23]['length'] = '18';

//当前状态
$items[24] = $itemsTemplate;
$items[24]['name'] = '当前状态';
$items[24]['type'] = 3;
$items[24]['identify'] = 'XK_ZT';
$items[24]['length'] = '1';

//数据来源单位
$items[25] = $itemsTemplate;
$items[25]['name'] = '数据来源单位';
$items[25]['identify'] = 'XK_LYDW';
$items[25]['length'] = '200';

//数据来源单位统一社会信用代码
$items[26] = $itemsTemplate;
$items[26]['name'] = '数据来源单位统一社会信用代码';
$items[26]['identify'] = 'XK_LYDWDM';
$items[26]['length'] = '18';

//备注
$items[27] = $itemsTemplate;
$items[27]['name'] = '备注';
$items[27]['identify'] = 'BZ';
$items[27]['length'] = '4000';

$template['items'] = $items;
```

#### 3.2.2 数据

```php
$itemsData = [
	'ZTMC' => '濮阳市嘉业运输有限公司',
	'XK_XDR_LB' => '法人及非法人组织',
	'TYSHXYDM' => '91410900MA46184E7Q',
	'XK_XDR_GSZC' => '',
	'XK_XDR_ZZJG' => '',
	'XK_XDR_SWDJ' => '',,
	'XK_XDR_SYDW' => '',,
	'XK_XDR_SHZZ' => 'MA46184E-7',,
	'XK_FRDB' => '张晓苹',
	'XK_FR_ZJLX' => '',
	'XK_FR_ZJHM' => '',
	'XK_XDR_ZJLX' => '',
	'XK_XDR_ZJHM' => '',
	'XK_XKWS' => '高速公路联网管理中心交通行政许可决定书',
	'XK_WSH' => '豫高速公路许 字第20210705000090号',
	'XK_XKLB' => '普通',
	'XK_XKZS' => '高速公路超限运输车辆通行证',
	'XK_XKBH' => '410000017210705A000090',
	'XK_NR' => '高速公路超限运输车辆通行证',
	'XK_JDRQ' => '20210714',
	'XK_YXQZ' => '20210714',
	'XK_YXQZI' => '20210807',
	'XK_XKJG' => '河南省高速公路联网管理中心',
	'XK_XKJGDM' => '12410000MB14527979',
	'XK_ZT' => '1',
	'XK_LYDW' => '河南省高速公路联网管理中心',
	'XK_LYDWDM' => '12410000MB14527979',
	'BZ'  => ''
];

$data = array(
    "data" => array(
        "type" => "wbjSearchData",
        "attributes" => array(
            "subjectCategory" => 1,    //主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "expirationDate" => 20991231,    //有效期限
        ),
        "relationships" => array(
            "crew" => array( // 来源单位
                "data" => array(
                    array("type" => "crews", "id" => 1)
                )
            ),
            "sourceUnit" => array( // 来源单位
                "data" => array(
                    array("type" => "userGroups", "id" => 1)
                )
            ),
            "template" => array( // 委办局资源目录
                "data" => array(
                    array("type" => "wbjTemplates", "id" => 1)
                )
            ),
            "itemsData" => array( //资源目录数据
                "data" => array(
                    array(
                        "type" => "itemsData", 
                        "attributes" => array(
                            "data" => $itemsData;
                        )
                    )
                )
            )
        )
    )
);
```
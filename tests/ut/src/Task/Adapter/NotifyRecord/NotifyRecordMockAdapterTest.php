<?php
namespace BaseData\Task\Adapter\NotifyRecord;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Task\Model\NotifyRecord;

class NotifyRecordMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new NotifyRecordMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new NotifyRecord()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Model\NotifyRecord',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Task\Model\NotifyRecord',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Task\Model\NotifyRecord',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

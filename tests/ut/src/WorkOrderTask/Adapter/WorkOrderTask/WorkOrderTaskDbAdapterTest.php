<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Model\NullWorkOrderTask;
use BaseData\WorkOrderTask\Translator\WorkOrderTaskDbTranslator;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\WorkOrderTaskRowCacheQuery;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\UserGroup\Repository\UserGroupRepository;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 * @author chloroplast
 */
class WorkOrderTaskDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(WorkOrderTaskDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction',
                                    'fetchGbTemplateByObject',
                                    'fetchBjTemplateByObject',
                                    'fetchParentTaskByObject'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IWorkOrderTaskAdapter
     */
    public function testImplementsIWorkOrderTaskAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 WorkOrderTaskDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Translator\WorkOrderTaskDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 WorkOrderTaskRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\WorkOrderTaskRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    public function testGetGbTemplateRepository()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\GbTemplateRepository',
            $adapter->getGbTemplateRepository()
        );
    }

    public function testGetBjTemplateRepository()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\BjTemplateRepository',
            $adapter->getBjTemplateRepository()
        );
    }

    public function testGetParentTaskRepository()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Repository\ParentTaskRepository',
            $adapter->getParentTaskRepository()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedWorkOrderTask = new WorkOrderTask();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedWorkOrderTask)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedWorkOrderTask);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedWorkOrderTask = new WorkOrderTask();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedWorkOrderTask, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedWorkOrderTask, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOneGbTemplate()
    {
        //初始化
        $expectedWorkOrderTask = new WorkOrderTask();
        $expectedWorkOrderTask->getParentTask()->setTemplateType(ParentTask::TEMPLATE_TYPE['GB']);
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWorkOrderTask);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchGbTemplateByObject')
                         ->with($expectedWorkOrderTask);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchParentTaskByObject')
                         ->with($expectedWorkOrderTask);

        //验证
        $workOrderTask = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWorkOrderTask, $workOrderTask);
    }

    public function testFetchOneBjTemplate()
    {
        //初始化
        $expectedWorkOrderTask = new WorkOrderTask();
        $expectedWorkOrderTask->getParentTask()->setTemplateType(ParentTask::TEMPLATE_TYPE['BJ']);
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWorkOrderTask);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchBjTemplateByObject')
                         ->with($expectedWorkOrderTask);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchParentTaskByObject')
                         ->with($expectedWorkOrderTask);

        //验证
        $workOrderTask = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWorkOrderTask, $workOrderTask);
    }

    //fetchGbTemplateByObject
    public function testFetchGbTemplateByObject()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskDbAdapter::class)
                           ->setMethods(
                               [
                                    'getGbTemplateRepository'
                                ]
                           )
                           ->getMock();

        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setTemplate(new GbTemplate(1));
        
        $gbTemplateId = 1;

        // 为 GbTemplate 类建立预言
        $gbTemplate  = $this->prophesize(GbTemplate::class);
        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchOne(Argument::exact($gbTemplateId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($gbTemplate);

        $adapter->expects($this->exactly(1))
                         ->method('getGbTemplateRepository')
                         ->willReturn($gbTemplateRepository->reveal());

        $adapter->fetchGbTemplateByObject($workOrderTask);
    }

    //fetchBjTemplateByObject
    public function testFetchBjTemplateByObject()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskDbAdapter::class)
                           ->setMethods(
                               [
                                    'getBjTemplateRepository'
                                ]
                           )
                           ->getMock();

        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setTemplate(new BjTemplate(1));
        
        $bjTemplateId = 1;

        // 为 BjTemplate 类建立预言
        $bjTemplate  = $this->prophesize(BjTemplate::class);
        $bjTemplateRepository = $this->prophesize(BjTemplateRepository::class);
        $bjTemplateRepository->fetchOne(Argument::exact($bjTemplateId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($bjTemplate);

        $adapter->expects($this->exactly(1))
                         ->method('getBjTemplateRepository')
                         ->willReturn($bjTemplateRepository->reveal());

        $adapter->fetchBjTemplateByObject($workOrderTask);
    }

    //fetchParentTaskByObject
    public function testFetchParentTaskByObject()
    {
        $adapter = $this->getMockBuilder(MockWorkOrderTaskDbAdapter::class)
                           ->setMethods(
                               [
                                    'getParentTaskRepository'
                                ]
                           )
                           ->getMock();

        $workOrderTask = new WorkOrderTask();
        $workOrderTask->setParentTask(new ParentTask(1));
        
        $parentTaskId = 1;

        // 为 ParentTask 类建立预言
        $parentTask  = $this->prophesize(ParentTask::class);
        $parentTaskRepository = $this->prophesize(ParentTaskRepository::class);
        $parentTaskRepository->fetchOne(Argument::exact($parentTaskId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($parentTask);

        $adapter->expects($this->exactly(1))
                         ->method('getParentTaskRepository')
                         ->willReturn($parentTaskRepository->reveal());

        $adapter->fetchParentTaskByObject($workOrderTask);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $workOrderTaskOne = new WorkOrderTask(1);
        $workOrderTaskTwo = new WorkOrderTask(2);

        $ids = [1, 2];

        $expectedWorkOrderTaskList = [];
        $expectedWorkOrderTaskList[$workOrderTaskOne->getId()] = $workOrderTaskOne;
        $expectedWorkOrderTaskList[$workOrderTaskTwo->getId()] = $workOrderTaskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWorkOrderTaskList);

        //验证
        $workOrderTaskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWorkOrderTaskList, $workOrderTaskList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterTitle()
    {
        $filter = array(
            'title' => 'title'
        );
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = 'title LIKE \'%'.$filter['title'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterAssignObject()
    {
        $filter = array(
            'assignObject' => 1
        );
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = 'assign_object_id = '.$filter['assignObject'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterStatus()
    {
        $filter = array(
            'status' => '1,2'
        );
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = 'status IN ('.$filter['status'].')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterParentTask()
    {
        $filter = array(
            'parentTask' => 1
        );
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = 'parent_task_id = '.$filter['parentTask'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortEndTimeDesc()
    {
        $sort = array('endTime' => -1);
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = ' ORDER BY end_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortEndTimeAsc()
    {
        $sort = array('endTime' => 1);
        $adapter = new MockWorkOrderTaskDbAdapter();

        $expectedCondition = ' ORDER BY end_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

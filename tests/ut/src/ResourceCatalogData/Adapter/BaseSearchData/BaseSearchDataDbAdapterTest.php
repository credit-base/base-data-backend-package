<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Template\Model\Template;

use BaseData\Enterprise\Adapter\Enterprise\EnterpriseDbAdapter;
use BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery;

use BaseData\ResourceCatalogData\Model\BaseSearchData;
use BaseData\ResourceCatalogData\Translator\Strategy\EnterpriserDbTranslatorStrategy;
use BaseData\ResourceCatalogData\Translator\Strategy\SearchDataDbTranslatorStrategyFactory;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class BaseSearchDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(MockBaseSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'getTranslatorStrategyFactory',
                                'getRowQueryFactory',
                                'getDbTranslator',
                                'getRowQuery',
                                'getAdapterFactory',
                                'getAdapter'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IBaseSearchDataAdapter
     */
    public function testImplementsIBaseSearchDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 SearchDataDbTranslatorStrategyFactory
     */
    public function testGetTranslatorStrategyFactory()
    {
        $adapter = new MockBaseSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\Strategy\SearchDataDbTranslatorStrategyFactory',
            $adapter->getTranslatorStrategyFactory()
        );
    }

    public function testGetAdapterFactory()
    {
        $adapter = new MockBaseSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BaseSearchData\AdapterFactory',
            $adapter->getAdapterFactory()
        );
    }

    /**
     * 测试是否初始化 RowQueryFactory
     */
    public function testGetRowQueryFactory()
    {
        $adapter = new MockBaseSearchDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BaseSearchData\RowQueryFactory',
            $adapter->getRowQueryFactory()
        );
    }

    public function testGetDbTranslator()
    {
        $this->adapter = $this->getMockBuilder(MockBaseSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'getTranslatorStrategyFactory',
                                ]
                           )
                           ->getMock();

        $category = 1;
        $translatorStrategy = new EnterpriserDbTranslatorStrategy();

        $factory = $this->prophesize(SearchDataDbTranslatorStrategyFactory::class);
        $factory->getStrategy(
            Argument::exact($category)
        )->shouldBeCalledTimes(1)->willReturn($translatorStrategy);

        $this->adapter->expects($this->once())->method('getTranslatorStrategyFactory')->willReturn($factory->reveal());

        $result = $this->adapter->getDbTranslator($category);

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\Strategy\EnterpriserDbTranslatorStrategy',
            $result
        );
    }

    public function testGetRowQuery()
    {
        $this->adapter = $this->getMockBuilder(MockBaseSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'getRowQueryFactory',
                                ]
                           )
                           ->getMock();

        $category = 1;
        $enterpriseRowCacheQuery = new EnterpriseRowCacheQuery();

        $factory = $this->prophesize(RowQueryFactory::class);
        $factory->getRowQuery(
            Argument::exact($category)
        )->shouldBeCalledTimes(1)->willReturn($enterpriseRowCacheQuery);

        $this->adapter->expects($this->once())->method('getRowQueryFactory')->willReturn($factory->reveal());

        $result = $this->adapter->getRowQuery($category);

        $this->assertInstanceOf(
            'BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery',
            $result
        );
    }

    public function testGetAdapter()
    {
        $this->adapter = $this->getMockBuilder(MockBaseSearchDataDbAdapter::class)
                           ->setMethods(
                               [
                                'getAdapterFactory',
                                ]
                           )
                           ->getMock();

        $category = 1;
        $enterpriseDbAdapter = new EnterpriseDbAdapter();

        $factory = $this->prophesize(AdapterFactory::class);
        $factory->getAdapter(
            Argument::exact($category)
        )->shouldBeCalledTimes(1)->willReturn($enterpriseDbAdapter);

        $this->adapter->expects($this->once())->method('getAdapterFactory')->willReturn($factory->reveal());

        $result = $this->adapter->getAdapter($category);

        $this->assertInstanceOf(
            'BaseData\Enterprise\Adapter\Enterprise\EnterpriseDbAdapter',
            $result
        );
    }

    //filter
    public function testFilterEmpty()
    {
        $list = array('list');

        $adapter = $this->prophesize(EnterpriseDbAdapter::class);
        $adapter->filter(array(), array(), 0, 20)->shouldBeCalledTimes(1)->willReturn($list);

        $this->adapter->expects($this->once())->method('getAdapter')->willReturn($adapter->reveal());

        //验证
        $result = $this->adapter->filter();

        $this->assertEquals($result, $list);
    }

    public function testFilter()
    {
        $filter['baseSubjectCategory'] = 1;

        $list = array('list');

        $adapter = $this->prophesize(EnterpriseDbAdapter::class);
        $adapter->filter($filter, array(), 0, 20)->shouldBeCalledTimes(1)->willReturn($list);

        $this->adapter->expects($this->once())->method('getAdapter')->willReturn($adapter->reveal());

        //验证
        $result = $this->adapter->filter($filter);

        $this->assertEquals($result, $list);
    }
    //add
    public function testAdd()
    {
        //初始化
        $baseSearchData = new BaseSearchData();
        $baseSearchData->setSubjectCategory(Template::SUBJECT_CATEGORY['FRJFFRZZ']);
        $info = array('info');

        $translator = $this->prophesize(EnterpriserDbTranslatorStrategy::class);
        $translator->objectToArray(Argument::exact($baseSearchData))->shouldBeCalledTimes(1)->willReturn($info);
        $this->adapter->expects($this->once())->method('getDbTranslator')->willReturn($translator->reveal());

        $rowCacheQuery = $this->prophesize(EnterpriseRowCacheQuery::class);
        $rowCacheQuery->add(Argument::exact($info))->shouldBeCalledTimes(1)->willReturn(true);
        $this->adapter->expects($this->once())->method('getRowQuery')->willReturn($rowCacheQuery->reveal());

        //验证
        $result = $this->adapter->add($baseSearchData);
        $this->assertTrue($result);
    }

    //add
    public function testAddFalse()
    {
        //初始化
        $baseSearchData = new BaseSearchData();
        $baseSearchData->setSubjectCategory(Template::SUBJECT_CATEGORY['FRJFFRZZ']);
        $info = array('baseSearchData');

        $translator = $this->prophesize(EnterpriserDbTranslatorStrategy::class);
        $translator->objectToArray(Argument::exact($baseSearchData))->shouldBeCalledTimes(1)->willReturn($info);
        $this->adapter->expects($this->once())->method('getDbTranslator')->willReturn($translator->reveal());

        $rowCacheQuery = $this->prophesize(EnterpriseRowCacheQuery::class);
        $rowCacheQuery->add(Argument::exact($info))->shouldBeCalledTimes(1)->willReturn(false);
        $this->adapter->expects($this->once())->method('getRowQuery')->willReturn($rowCacheQuery->reveal());

        //验证
        $result = $this->adapter->add($baseSearchData);
        $this->assertFalse($result);
    }
}

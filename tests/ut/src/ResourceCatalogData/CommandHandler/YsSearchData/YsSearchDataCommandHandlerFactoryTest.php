<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use BaseData\ResourceCatalogData\Command\YsSearchData\EnableYsSearchDataCommand;
use BaseData\ResourceCatalogData\Command\YsSearchData\DisableYsSearchDataCommand;

class YsSearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new YsSearchDataCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testDisableYsSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableYsSearchDataCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\CommandHandler\YsSearchData\DisableYsSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testEnableYsSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableYsSearchDataCommand(
                $this->faker->randomNumber(3)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\CommandHandler\YsSearchData\EnableYsSearchDataCommandHandler',
            $commandHandler
        );
    }
}

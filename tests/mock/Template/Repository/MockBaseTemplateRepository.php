<?php
namespace BaseData\Template\Repository;

use BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class MockBaseTemplateRepository extends BaseTemplateRepository
{
    public function getAdapter() : IBaseTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBaseTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBaseTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}

# 问题反馈接口示例

---

## 目录

* [参考文档](#参考文档)
* [参数说明](#参数说明)
* [接口示例](#接口示例)
	* [获取数据支持include、fields请求参数](#获取数据支持include、fields请求参数)
	* [获取单条数据](#获取单条数据)
	* [获取多条数据](#获取多条数据)
	* [根据检索条件查询数据](#根据检索条件查询数据)	
	* [新增](#新增)
	* [受理](#受理)
	* [撤销](#撤销)
	* [接口返回示例](#接口返回示例)
		* [单条数据接口返回示例](#单条数据接口返回示例)
		* [多条数据接口返回示例](#多条数据接口返回示例)

## <a name="参考文档">参考文档</a>

* 项目字典
	* [通用项目字典](../Dictionary/common.md "通用项目字典")
	* [互动类项目字典](../Dictionary/interaction.md "互动类项目字典")
	* [前台用户项目字典](../Dictionary/member.md "前台用户项目字典")
	* [受理委办局项目字典](../Dictionary/UserGroup.md "受理委办局项目字典")
	* [问题反馈项目字典](../Dictionary/feedback.md "问题反馈项目字典")
*  控件规范
	* [通用控件规范](../WidgetRule/common.md "通用控件规范")
	* [互动类通用控件规范](../WidgetRule/interaction.md "互动类通用控件规范")
	* [问题反馈控件规范](../WidgetRule/feedback.md "问题反馈控件规范")

* 错误规范
	* [通用错误规范](../ErrorRule/common.md "通用错误规范")
    * 错误映射
    ```
	100=>array(
		'memberId'=>前台用户不能为空
		'acceptUserGroupId'=>受理委办局不能为空
		'crewId' => 受理人不能为空
	)
	101=>array(
		'title'=>标题格式不正确
		'content'=>内容格式不正确
		'memberId'=>前台用户格式不正确
		'acceptUserGroupId'=>受理委办局格式不正确
		'replyContent'=>回复内容格式不正确
		'replyImages'=>回复图片格式不正确
		'replyImagesCount'=>回复图片数量超出限制
		'admissibility'=>受理情况格式不正确
		'replyCrew'=>受理人格式不正确
	),
	102=>array(
		'applyStatus'=>审核状态不能操作
		'status'=>状态不能操作
	)
    ```
	
## <a name="参数说明">参数说明</a>
     
### feedback

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| title           | string     | 是            | 个人反馈                                       | 标题            |
| content         | string     | 是            | 反馈内容                                       | 内容            |
| member          | int        | 是            | 1                                            | 前台用户         |
| acceptUserGroup | int        | 是            | 1                                            | 受理委办局        |
| acceptStatus    | int        | 是            | 0                                            | 受理状态(0 待受理, 1 受理中, 2 受理完成)|
| reply           | int        | 是            | 1                                            | 回复信息        |
| status          | int        | 是            | 0                                            | 状态(0 正常, -2 撤销)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### reply

| 英文名称         | 类型        |请求参数是否必填  |  示例                                        | 描述            |
| :---:           | :----:     | :------:      |:------------:                                |:-------:       |
| content         | string     | 是            | 回复内容                                       | 回复内容        |
| images          | array      | 否            | array(array('name'=>'图片', 'identify'=>'图片.jpg')) | 回复图片  |
| crew            | int        | 是            | 1                                            | 受理人         |
| admissibility   | int        | 是            | 0                                            | 受理情况(1 予以受理, 2 不予受理)|
| status          | int        | 是            | 0                                            | 状态(0 默认)|
| updateTime      | int        |               | 1535444931                                   | 更新时间         |
| creditTime      | int        |               | 1535444931                                   | 创建时间         |
| statusTime      | int        |               | 1535444931                                   | 状态更新时间      |

### <a name="获取数据支持include、fields请求参数">获取数据支持include、fields请求参数</a>

	1、fields[TYPE]请求参数
	    1.1 fields[members]
	    1.2 fields[userGroups]
	    1.3 fields[crews]
	    1.4 fields[replies]
	    1.5 fields[feedbacks]
		1.6 include=member,acceptUserGroup,reply,reply.crew
	2、page请求参数
		2.1 page[number]=1 | 当前页
		2.2 page[size]=20 | 获取每页的数量

示例

	$response = $client->request('GET', 'feedbacks/1?fields[feedbacks]=name',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取单条数据">获取单条数据</a>

路由

	通过GET传参
	/feedbacks/{id:\d+}

示例

	$response = $client->request('GET', 'feedbacks/1',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="获取多条数据">获取多条数据</a>

路由

	通过GET传参
	/feedbacks/{ids:\d+,[\d,]+}

示例

	$response = $client->request('GET', 'feedbacks/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="根据检索条件查询数据">根据检索条件查询数据</a>

路由

	通过GET传参
	/feedbacks

	1、检索条件
	    1.1 filter[title] | 根据标题搜索
	    1.2 filter[member] | 根据前台用户搜索,前台用户id
	    1.3 filter[acceptUserGroup] | 根据受理委办局搜索,委办局id
	    1.4 filter[status] | 根据状态搜索 | 0 正常 | -2 撤销
	    1.5 filter[acceptStatus] | 根据受理状态搜索 | 0 待受理 | 1 受理中 | 2 受理完成

	2、排序
		2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | 更新时间 根据更新时间正序
		2.2 sort=-status | -status 根据状态倒序 | 状态 根据状态正序
		2.3 sort=-acceptStatus | -acceptStatus 根据受理状态倒序 | 受理状态 根据受理状态正序

示例

	$response = $client->request('GET', 'feedbacks?sort=-id&page[number]=1&page[size]=20',['headers'=>['Content-' => 'application/vnd.api+json']]);

### <a name="新增">新增</a>
	
路由

	通过POST传参
	/feedbacks

示例

	$data = array(
		'data' => array(
			"type"=>"feedbacks",
			"attributes"=>array(
				"title"=>"标题",
				"content"=>"内容"
			),
			"relationships"=>array(
				"member"=>array(
					"data"=>array(
						array("type"=>"members","id"=>前台用户id)
					)
				),
				"acceptUserGroup"=>array(
				m	"data"=>array(
						array("type"=>"userGroups","id"=>受理委办局id)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'POST',
	                'feedbacks',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="受理">受理</a>

路由

	通过PATCH传参
	/feedbacks/{id:\d+}/accept

示例

	$data = array(
		'data' => array(
			"type"=>"feedbacks",
			"relationships"=>array(
				"reply"=>array(
					"data"=>array(
						array(
							"type"=>"replies",
							"attributes"=>array(
								"content"=>"回复内容",
								"images"=>array(
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg'),
									array('name' => '回复图片', 'identify' => 'identify.jpg')
								),
								"admissibility"=>0
							),
							"relationships"=>array(
								"crew"=>array(
									"data"=>array(
										array("type"=>"crews","id"=>受理人id)
									)
								)
							),
						)
					)
				)
			)
		)
	);

	$response = $client->request(
	                'PATCH',
	                'feedbacks/1/accept',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json'],
						'json' => $data
	                ]
	            );

### <a name="撤销">撤销</a>

路由

	通过PATCH传参
	/feedbacks/{id:\d+}/revoke

示例

	$response = $client->request(
	                'PATCH',
	                'feedbacks/1/revoke',
	                [
						'headers'=>['Content-Type' => 'application/vnd.api+json']
	                ]
	            );

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条数据接口返回示例">单条数据接口返回示例</a>

	{
		"meta": [],
		"data": {
			"type": "feedbacks",
			"id": "1",
			"attributes": {
				"title": "标题",
				"content": "内容",
				"acceptStatus": 2,
				"status": 0,
				"createTime": 1635562956,
				"updateTime": 1635576541,
				"statusTime": 0
			},
			"relationships": {
				"acceptUserGroup": {
					"data": {
						"type": "userGroups",
						"id": "1"
					}
				},
				"member": {
					"data": {
						"type": "members",
						"id": "1"
					}
				},
				"reply": {
					"data": {
						"type": "replies",
						"id": "14"
					}
				}
			},
			"links": {
				"self": "127.0.0.1:8089/feedbacks/1"
			}
		},
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "14",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635576489,
					"updateTime": 1635576489,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}

#### <a name="多条数据接口返回示例">多条数据接口返回示例</a>

	{
		"meta": {
			"count": 1,
			"links": {
				"first": null,
				"last": null,
				"prev": null,
				"next": null
			}
		},
		"links": {
			"first": null,
			"last": null,
			"prev": null,
			"next": null
		},
		"data": [
			{
				"type": "feedbacks",
				"id": "1",
				"attributes": {
					"title": "标题",
					"content": "内容",
					"acceptStatus": 2,
					"status": 0,
					"createTime": 1635562956,
					"updateTime": 1635576541,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"member": {
						"data": {
							"type": "members",
							"id": "1"
						}
					},
					"reply": {
						"data": {
							"type": "replies",
							"id": "14"
						}
					}
				},
				"links": {
					"self": "127.0.0.1:8089/feedbacks/1"
				}
			}
		],
		"included": [
			{
				"type": "userGroups",
				"id": "1",
				"attributes": {
					"name": "萍乡市发展和改革委员会",
					"shortName": "发改委",
					"status": 0,
					"createTime": 1516168970,
					"updateTime": 1516168970,
					"statusTime": 0
				}
			},
			{
				"type": "members",
				"id": "1",
				"attributes": {
					"userName": "用户名",
					"realName": "姓名",
					"cellphone": "18800000000",
					"email": "997809098@qq.com",
					"cardId": "412825199009094567",
					"contactAddress": "雁塔区长延堡街道",
					"securityQuestion": 1,
					"gender": 1,
					"status": 0,
					"createTime": 1621434208,
					"updateTime": 1621435225,
					"statusTime": 1621434736
				}
			},
			{
				"type": "crews",
				"id": "1",
				"attributes": {
					"realName": "张科",
					"cardId": "412825199009094532",
					"userName": "18800000000",
					"cellphone": "18800000000",
					"category": 1,
					"purview": [
						"1",
						"2",
						"3"
					],
					"status": 0,
					"createTime": 1618284031,
					"updateTime": 1619578455,
					"statusTime": 0
				},
				"relationships": {
					"userGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"department": {
						"data": {
							"type": "departments",
							"id": "3"
						}
					}
				}
			},
			{
				"type": "replies",
				"id": "14",
				"attributes": {
					"content": "予以受理",
					"images": [
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						},
						{
							"name": "回复图片",
							"identify": "identify.jpg"
						}
					],
					"admissibility": 1,
					"status": 0,
					"createTime": 1635576489,
					"updateTime": 1635576489,
					"statusTime": 0
				},
				"relationships": {
					"acceptUserGroup": {
						"data": {
							"type": "userGroups",
							"id": "1"
						}
					},
					"crew": {
						"data": {
							"type": "crews",
							"id": "1"
						}
					}
				}
			}
		]
	}
<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use BaseData\Common\Controller\Interfaces\IEnableAbleController;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\View\WbjSearchDataView;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use BaseData\ResourceCatalogData\Command\WbjSearchData\EnableWbjSearchDataCommand;
use BaseData\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand;
use BaseData\ResourceCatalogData\CommandHandler\WbjSearchData\WbjSearchDataCommandHandlerFactory;

class WbjSearchDataEnableController extends Controller implements IEnableAbleController
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new WbjSearchDataRepository();
        $this->commandBus = new CommandBus(new WbjSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : IWbjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /wbjSearchData/{id:\d+}/enable
     * 启用, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function enable(int $id)
    {
        if (!empty($id)) {
            $command = new EnableWbjSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $wbjSearchData  = $this->getRepository()->fetchOne($id);
                if ($wbjSearchData instanceof WbjSearchData) {
                    $this->render(new WbjSearchDataView($wbjSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /wbjSearchData/{id:\d+}/disable
     * 禁用, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableWbjSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $wbjSearchData  = $this->getRepository()->fetchOne($id);
                if ($wbjSearchData instanceof WbjSearchData) {
                    $this->render(new WbjSearchDataView($wbjSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}

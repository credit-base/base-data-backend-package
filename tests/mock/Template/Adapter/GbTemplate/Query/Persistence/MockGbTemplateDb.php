<?php
namespace BaseData\Template\Adapter\GbTemplate\Query\Persistence;

class MockGbTemplateDb extends GbTemplateDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

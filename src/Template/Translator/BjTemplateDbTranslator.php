<?php
namespace BaseData\Template\Translator;

use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\NullBjTemplate;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class BjTemplateDbTranslator extends TemplateDbTranslator
{
    public function arrayToObject(array $expression, $bjTemplate = null) : BjTemplate
    {
        if (!isset($expression['bj_template_id'])) {
            return NullBjTemplate::getInstance();
        }

        if ($bjTemplate == null) {
            $bjTemplate = new BjTemplate();
        }

        $bjTemplate->setId($expression['bj_template_id']);

        if (isset($expression['source_unit_id'])) {
            $bjTemplate->getSourceUnit()->setId($expression['source_unit_id']);
        }
        if (isset($expression['gb_template_id'])) {
            $bjTemplate->getGbTemplate()->setId($expression['gb_template_id']);
        }

        $bjTemplate = parent::arrayToObject($expression, $bjTemplate);
        
        return $bjTemplate;
    }

    public function objectToArray($bjTemplate, array $keys = array()) : array
    {
        if (!$bjTemplate instanceof BjTemplate) {
            return [];
        }

        $templateExpression = parent::objectToArray($bjTemplate, $keys);
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'sourceUnit',
                'gbTemplate'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['bj_template_id'] = $bjTemplate->getId();
        }
        if (in_array('sourceUnit', $keys)) {
            $expression['source_unit_id'] = $bjTemplate->getSourceUnit()->getId();
        }
        if (in_array('gbTemplate', $keys)) {
            $expression['gb_template_id'] = $bjTemplate->getGbTemplate()->getId();
        }

        $expression = array_merge($expression, $templateExpression);
        
        return $expression;
    }
}

<?php
namespace BaseData\Rule\Adapter;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Model\MockRule;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RepositoryFactory;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\MockTemplate;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Repository\WbjTemplateRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceAdapterTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockRuleServiceAdapterTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetGbTemplateRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Repository\GbTemplateRepository',
            $this->trait->publicGetGbTemplateRepository()
        );
    }

    public function testGetRepositoryFactory()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RepositoryFactory',
            $this->trait->publicGetRepositoryFactory()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Repository\BjTemplateRepository',
            $this->trait->publicGetRepository(Template::CATEGORY['BJ'])
        );
    }

    //fetchTransformationTemplate
    public function testFetchTransformationTemplateIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchTransformationTemplateByObject'
                                ]
                           )
                           ->getMock();
        
        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))
                         ->method('fetchTransformationTemplateByObject')
                         ->with($ruleService);

        $adapter->publicFetchTransformationTemplate($ruleService);
    }

    public function testFetchTransformationTemplateIsArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchTransformationTemplateByList'
                                ]
                           )
                           ->getMock();
                
        $ruleServiceOne = \BaseData\Rule\Utils\MockFactory::generateRuleService(
            $this->faker->randomNumber(1)
        );

        $ruleServiceTwo = \BaseData\Rule\Utils\MockFactory::generateRuleService(
            $this->faker->randomNumber(2)
        );

        $ruleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchTransformationTemplateByList')
                         ->with($ruleServiceList);

        $adapter->publicFetchTransformationTemplate($ruleServiceList);
    }

    //fetchTransformationTemplateByObject
    public function testFetchTransformationTemplateByObject()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getRepositoryFactory'
                                ]
                           )
                           ->getMock();

        $id = $transformationTemplateId = $this->faker->randomNumber();
        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService($id);
        $ruleService->setTransformationCategory(Template::CATEGORY['BJ']);
        $transformationTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $transformationTemplateId
        );
        $ruleService->setTransformationTemplate($transformationTemplate);

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $transformationTemplateRepository = $this->prophesize(BjTemplateRepository::class);
        $transformationTemplateRepository->fetchOne(Argument::exact($transformationTemplateId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($transformationTemplate);
        $repositoryFactory->getRepository(Argument::exact($ruleService->getTransformationCategory()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($transformationTemplateRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->publicFetchTransformationTemplateByObject($ruleService);
    }

    public function testFetchTransformationTemplateByList()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getRepositoryFactory'
                                ]
                           )
                           ->getMock();

        $id = $transformationTemplateId = $this->faker->randomNumber();
        $ruleServiceOne = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($id);
        $transformationTemplateOne = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $transformationTemplateId
        );
        $ruleServiceOne->setTransformationCategory(Template::CATEGORY['BJ']);
        $ruleServiceOne->setTransformationTemplate($transformationTemplateOne);

        $idTwo = $transformationTemplateIdTwo = $this->faker->randomNumber(2);
        $ruleServiceTwo = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($idTwo);
        $transformationTemplateTwo = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(
            $transformationTemplateIdTwo
        );
        $ruleServiceTwo->setTransformationCategory(Template::CATEGORY['BJ']);
        $ruleServiceTwo->setTransformationTemplate($transformationTemplateTwo);

        $ruleServiceTwo->setTransformationTemplate($transformationTemplateTwo);
        $transformationTemplateIds = [$transformationTemplateId, $transformationTemplateIdTwo];
        
        $transformationTemplateList[$transformationTemplateOne->getId()] = $transformationTemplateOne;
        $transformationTemplateList[$transformationTemplateTwo->getId()] = $transformationTemplateTwo;
        
        $ruleServiceList[$ruleServiceOne->getApplyId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getApplyId()] = $ruleServiceTwo;

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $transformationTemplateRepository = $this->prophesize(BjTemplateRepository::class);
        $transformationTemplateRepository->fetchList(Argument::exact($transformationTemplateIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($transformationTemplateList);
        $repositoryFactory->getRepository(Argument::exact(Template::CATEGORY['BJ']))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($transformationTemplateRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->publicFetchTransformationTemplateByList($ruleServiceList);
    }

    //fetchSourceTemplate
    public function testFetchSourceTemplateIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchSourceTemplateByObject'
                                ]
                           )
                           ->getMock();
        
        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceTemplateByObject')
                         ->with($ruleService);

        $adapter->publicFetchSourceTemplate($ruleService);
    }

    public function testFetchSourceTemplateIsArray()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchSourceTemplateByList'
                                ]
                           )
                           ->getMock();
                
        $ruleServiceOne = \BaseData\Rule\Utils\MockFactory::generateRuleService(
            $this->faker->randomNumber(1)
        );

        $ruleServiceTwo = \BaseData\Rule\Utils\MockFactory::generateRuleService(
            $this->faker->randomNumber(2)
        );

        $ruleServiceList[$ruleServiceOne->getId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getId()] = $ruleServiceTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchSourceTemplateByList')
                         ->with($ruleServiceList);

        $adapter->publicFetchSourceTemplate($ruleServiceList);
    }

    //fetchSourceTemplateByObject
    public function testFetchSourceTemplateByObject()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getRepositoryFactory'
                                ]
                           )
                           ->getMock();

        $id = $sourceTemplateId = $this->faker->randomNumber();
        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService($id);
        $ruleService->setSourceCategory(Template::CATEGORY['WBJ']);
        $sourceTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(
            $sourceTemplateId
        );
        $ruleService->setSourceTemplate($sourceTemplate);

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $sourceTemplateRepository = $this->prophesize(WbjTemplateRepository::class);
        $sourceTemplateRepository->fetchOne(Argument::exact($sourceTemplateId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceTemplate);
        $repositoryFactory->getRepository(Argument::exact($ruleService->getSourceCategory()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceTemplateRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->publicFetchSourceTemplateByObject($ruleService);
    }

    public function testFetchSourceTemplateByList()
    {
        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getRepositoryFactory'
                                ]
                           )
                           ->getMock();

        $id = $sourceTemplateId = $this->faker->randomNumber();
        $ruleServiceOne = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($id);
        $sourceTemplateOne = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(
            $sourceTemplateId
        );
        $ruleServiceOne->setSourceCategory(Template::CATEGORY['WBJ']);
        $ruleServiceOne->setSourceTemplate($sourceTemplateOne);

        $idTwo = $sourceTemplateIdTwo = $this->faker->randomNumber(2);
        $ruleServiceTwo = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService($idTwo);
        $sourceTemplateTwo = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(
            $sourceTemplateIdTwo
        );
        $ruleServiceTwo->setSourceCategory(Template::CATEGORY['WBJ']);
        $ruleServiceTwo->setSourceTemplate($sourceTemplateTwo);

        $ruleServiceTwo->setSourceTemplate($sourceTemplateTwo);
        $sourceTemplateIds = [$sourceTemplateId, $sourceTemplateIdTwo];
        
        $sourceTemplateList[$sourceTemplateOne->getId()] = $sourceTemplateOne;
        $sourceTemplateList[$sourceTemplateTwo->getId()] = $sourceTemplateTwo;
        
        $ruleServiceList[$ruleServiceOne->getApplyId()] = $ruleServiceOne;
        $ruleServiceList[$ruleServiceTwo->getApplyId()] = $ruleServiceTwo;

        $repositoryFactory = $this->prophesize(RepositoryFactory::class);
        $sourceTemplateRepository = $this->prophesize(WbjTemplateRepository::class);
        $sourceTemplateRepository->fetchList(Argument::exact($sourceTemplateIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceTemplateList);
        $repositoryFactory->getRepository(Argument::exact(Template::CATEGORY['WBJ']))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($sourceTemplateRepository->reveal());

        $adapter->expects($this->exactly(1))
                         ->method('getRepositoryFactory')
                         ->willReturn($repositoryFactory->reveal());

        $adapter->publicFetchSourceTemplateByList($ruleServiceList);
    }

    //fetchRules
    public function testFetchRules()
    {
        $ruleService = new RuleService();
        $condition = ['condition'];

        $rules['completionRule'] = new MockRule();
        $rules['comparisonRule'] = new MockRule();

        $ruleService->setRules($rules);

        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchCondition'
                                ]
                           )
                           ->getMock();

        $adapter->expects($this->exactly(2))
                         ->method('fetchCondition')
                         ->withConsecutive(
                             [$rules['completionRule']],
                             [$rules['comparisonRule']]
                         )
                         ->willReturn($condition);

        $adapter->publicFetchRules($ruleService);

        $rules = $ruleService->getRules();

        $this->assertEquals($condition, $rules['completionRule']->getCondition());
        $this->assertEquals($condition, $rules['comparisonRule']->getCondition());
    }

    //fetchCondition
    public function testFetchCondition()
    {
        $temlateList = ['temlateList'];
        $templateName = 'templateName';
        $itemName = 'itemName';

        $condition = array(
            'completionRule' =>  array(
                'ZTMC' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                    array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC')
                ),
                'TYSHXYDM' => array(
                    array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM')
                ),
            )
        );

        $expection = array(
            'completionRule' =>  array(
                'ZTMC' => array(
                    array(
                        'id'=>1,
                        'base'=> array(1,2),
                        'item'=>'ZTMC',
                        'itemName'=>'itemName',
                        'name'=>'templateName'
                    ),
                    array(
                        'id'=>2,
                        'base'=> array(1),
                        'item'=>'ZTMC',
                        'itemName'=>'itemName',
                        'name'=>'templateName'
                    ),
                ),
                'TYSHXYDM' => array(
                    array(
                        'id'=>1,
                        'base'=> array(1,2),
                        'item'=>'TYSHXYDM',
                        'itemName'=>'itemName',
                        'name'=>'templateName'
                    )
                ),
            )
        );

        $rule = new MockRule();
        $rule->setCondition($condition['completionRule']);

        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchRuleTemplate',
                                    'fetchTemplateName',
                                    'fetchItemName'
                                ]
                           )
                           ->getMock();

        $adapter->expects($this->exactly(1))
                         ->method('fetchRuleTemplate')
                         ->with($condition['completionRule'])
                         ->willReturn($temlateList);

        $adapter->expects($this->exactly(3))
                         ->method('fetchTemplateName')
                         ->withConsecutive(
                             [$temlateList,  $condition['completionRule']['ZTMC'][0]],
                             [$temlateList,  $condition['completionRule']['ZTMC'][1]],
                             [$temlateList,  $condition['completionRule']['TYSHXYDM'][0]]
                         )->willReturn($templateName);

        $adapter->expects($this->exactly(3))
                         ->method('fetchItemName')
                         ->withConsecutive(
                             [$temlateList,  $condition['completionRule']['ZTMC'][0]],
                             [$temlateList,  $condition['completionRule']['ZTMC'][1]],
                             [$temlateList,  $condition['completionRule']['TYSHXYDM'][0]]
                         )->willReturn($itemName);

        $result = $adapter->publicFetchCondition($rule);
        $this->assertEquals($expection['completionRule'], $result);
    }

    public function testFetchConditionEmptyTemplate()
    {
        $condition = ['condition'];
        $temlateList = [];

        $rule = new MockRule();
        $rule->setCondition($condition);

        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchRuleTemplate'
                                ]
                           )
                           ->getMock();

        $adapter->expects($this->exactly(1))
                         ->method('fetchRuleTemplate')
                         ->with($condition)
                         ->willReturn($temlateList);

        $result = $adapter->publicFetchCondition($rule);

        $this->assertEquals($condition, $result);
    }

    //fetchTemplateName
    public function testFetchTemplateNameExist()
    {
        //初始化
        $templateList[0] = new MockTemplate();
        $templateList[1] = new MockTemplate();
        $templateList[0]->setName('template0');
        $templateList[1]->setName('template1');

        $rule['id'] = 1;

        $adapter = new MockRuleServiceAdapterTrait();

        $result = $adapter->publicFetchTemplateName($templateList, $rule);
        $this->assertEquals('template1', $result);
    }

    public function testFetchTemplateNameNotExist()
    {
        //初始化
        $templateList[0] = new MockTemplate();
        $templateList[1] = new MockTemplate();
        $templateList[0]->setName('template1');
        $templateList[1]->setName('template1');

        $rule['id'] = 3;

        $adapter = new MockRuleServiceAdapterTrait();

        $result = $adapter->publicFetchTemplateName($templateList, $rule);
        $this->assertEquals('', $result);
    }

    //fetchRuleTemplate
    public function testFetchRuleTemplate()
    {
        //初始化
        $templateList = ['templateList'];

        $rules = [
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            ),
            'TYSHXYDM' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
                array('id'=>2, 'base'=> array(1), 'item'=>'TYSHXYDM'),
            )
        ];
        $ids = [1,2];

        $adapter = $this->getMockBuilder(MockRuleServiceAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getGbTemplateRepository'
                                ]
                           )
                           ->getMock();

        $repository = $this->prophesize(gbTemplateRepository::class);
        $repository->fetchList(Argument::exact($ids))
                                         ->shouldBeCalledTimes(1)
                                         ->willReturn($templateList);

        $adapter->expects($this->exactly(1))->method('getGbTemplateRepository')->willReturn($repository->reveal());

        $result = $adapter->publicFetchRuleTemplate($rules);
        $this->assertEquals($templateList, $result);
    }

    //fetchItemName
    private function prepareFetchItemName($rule)
    {
        $items = array(
                array(
                    "name" => '主体名称',    //信息项名称
                    "identify" => 'ZTMC',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '200',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(),    //脱敏规则
                    "remarks" => '信用主体名称',    //备注
                ),
                array(
                    "name" => '统一社会信用代码',    //信息项名称
                    "identify" => 'TYSHXYDM',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                    "remarks" => '信用主体代码',    //备注
                )
        );

        $template = new MockTemplate();
        $template->setItems($items);

        $templateList = [1=>$template];

        $adapter = new MockRuleServiceAdapterTrait();

        return $adapter->publicFetchItemName($templateList, $rule);
    }

    public function testFetchItemName()
    {
        $rule = array('id'=>1, 'item'=>'ZTMC',);
        $itemName = $this->prepareFetchItemName($rule);
        $this->assertEquals('主体名称', $itemName);
    }

    public function testFetchItemNameNotExist()
    {
        $rule = array('id'=>1, 'item'=>'NOT_EXIST',);
        $itemName = $this->prepareFetchItemName($rule);
        $this->assertEquals('', $itemName);
    }
}

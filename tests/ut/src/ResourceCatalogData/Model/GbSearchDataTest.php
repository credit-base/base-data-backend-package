<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\NullGbTemplate;

use BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class GbSearchDataTest extends TestCase
{
    private $gbSearchData;

    private $faker;

    public function setUp()
    {
        $this->gbSearchData = new MockGbSearchData();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->gbSearchData);
        unset($this->faker);
    }

    public function testSearchDataConstructor()
    {
        $this->assertEquals('', $this->gbSearchData->getDescription());
        $this->assertEquals(
            GbSearchData::FRONT_END_PROCESSOR_STATUS['NOT_IMPORT'],
            $this->gbSearchData->getFrontEndProcessorStatus()
        );
        $this->assertInstanceOf(
            'BaseData\Template\Model\GbTemplate',
            $this->gbSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\GbItemsData',
            $this->gbSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new GbItemsData();

        $this->gbSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->gbSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->gbSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end
    
    public function testGetGbItemsDataRepository()
    {
        $gbSearchData = new MockGbSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter',
            $gbSearchData->getGbItemsDataRepository()
        );
    }

    public function testGetRepository()
    {
        $gbSearchData = new MockGbSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $gbSearchData->getRepository()
        );
    }

    public function testAddItemsData()
    {
        $gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
                           ->setMethods(['getGbItemsDataRepository'])
                           ->getMock();

        $repository = $this->prophesize(IGbItemsDataAdapter::class);
        $repository->add(Argument::exact($gbSearchData->getItemsData()))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $gbSearchData->expects($this->once())
             ->method('getGbItemsDataRepository')
             ->willReturn($repository->reveal());

        $result = $gbSearchData->addItemsData();
        $this->assertTrue($result);
    }

    public function testAdd()
    {
        $gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
                           ->setMethods(['assignment', 'validate', 'addItemsData', 'getRepository'])
                           ->getMock();

        $gbSearchData->expects($this->once())
            ->method('assignment');

        $gbSearchData->expects($this->once())
            ->method('validate')
            ->willReturn(true);

        $gbSearchData->expects($this->once())
            ->method('addItemsData')
            ->willReturn(true);

        $repository = $this->prophesize(IGbSearchDataAdapter::class);
        $repository->add(Argument::exact($gbSearchData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $gbSearchData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $gbSearchData->add();
        $this->assertTrue($result);
    }

    public function testDelete()
    {
        $gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $status = GbSearchData::STATUS['CONFIRM'];
        $gbSearchData->setStatus($status);

        $gbSearchData->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(GbSearchData::STATUS['DELETED'])
                    ->willReturn(true);

        $result = $gbSearchData->delete();
        
        $this->assertTrue($result);
    }

    public function testDeleteFail()
    {
        $status = GbSearchData::STATUS['DELETED'];
        $this->gbSearchData->setStatus($status);

        $result = $this->gbSearchData->delete();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testIsConfirm()
    {
        $this->gbSearchData->setStatus(GbSearchData::STATUS['CONFIRM']);

        $result = $this->gbSearchData->isConfirm();
        $this->assertTrue($result);
    }

    public function testIsEnabled()
    {
        $this->gbSearchData->setStatus(GbSearchData::STATUS['ENABLED']);

        $result = $this->gbSearchData->isEnabled();
        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $status = GbSearchData::STATUS['CONFIRM'];
        $gbSearchData->setStatus($status);

        $gbSearchData->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(GbSearchData::STATUS['ENABLED'])
                    ->willReturn(true);

        $result = $gbSearchData->confirm();
        
        $this->assertTrue($result);
    }

    public function testConfirmFail()
    {
        $status = GbSearchData::STATUS['DELETED'];
        $this->gbSearchData->setStatus($status);

        $result = $this->gbSearchData->confirm();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['updateStatus'])
            ->getMock();
            
        $status = GbSearchData::STATUS['ENABLED'];
        $gbSearchData->setStatus($status);

        $gbSearchData->expects($this->exactly(1))
                    ->method('updateStatus')
                    ->with(GbSearchData::STATUS['DISABLED'])
                    ->willReturn(true);

        $result = $gbSearchData->disable();
        
        $this->assertTrue($result);
    }

    public function testDisableFail()
    {
        $status = GbSearchData::STATUS['DELETED'];
        $this->gbSearchData->setStatus($status);

        $result = $this->gbSearchData->disable();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('status', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $this->gbSearchData = $this->getMockBuilder(MockGbSearchData::class)
            ->setMethods(['getRepository'])
            ->getMock();
            
        $status = GbSearchData::STATUS['CONFIRM'];
        $this->gbSearchData->setStatus($status);
        $this->gbSearchData->setUpdateTime(Core::$container->get('time'));
        $this->gbSearchData->setStatusTime(Core::$container->get('time'));

        $repository = $this->prophesize(IGbSearchDataAdapter::class);

        $repository->edit(
            Argument::exact($this->gbSearchData),
            Argument::exact(array(
                'statusTime',
                'status',
                'updateTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->gbSearchData->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->gbSearchData->updateStatus($status);
        
        $this->assertTrue($result);
    }
}

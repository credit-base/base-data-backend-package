<?php
namespace BaseData\Template\GbTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Translator\GbTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要查看我的国标目录时,在政务网OA中,可以查看到我的所有国标目录,
 *           通过列表的形式查看所有国标目录,以便于我可以快速找到我的所有国标目录
 * @Scenario: 查看资源目录详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 存在一条资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
        ]);
    }

    /**
     * @When: 当我查看该条资源目录详情时
     */
    public function fetchGbTemplate($id)
    {
        $repository = new GbTemplateRepository();

        $gbTemplate = $repository->fetchOne($id);

        return $gbTemplate;
    }

    /**
     * @Then: 我可以看见该条资源目录的全部信息
     */
    public function testViewGbTemplateList()
    {
        $id = 1;
        
        $setGbTemplate = $this->gbTemplate()[0];
        $setGbTemplate['gb_template_id'] = $id;
        $setGbTemplate['subject_category'] = json_decode($setGbTemplate['subject_category'], true);
        $setGbTemplate['items'] = json_decode($setGbTemplate['items'], true);

        $gbTemplate = $this->fetchGbTemplate($id);
        $translator = new GbTemplateDbTranslator();
        $gbTemplateArray = $translator->objectToArray($gbTemplate);

        $this->assertEquals($gbTemplateArray, $setGbTemplate);
    }
}

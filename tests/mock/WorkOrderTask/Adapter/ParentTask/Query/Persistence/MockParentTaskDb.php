<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask\Query\Persistence;

class MockParentTaskDb extends ParentTaskDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Service;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use BaseData\Template\Model\Template;

class MockSyncErrorDataDownloadService extends SyncErrorDataDownloadService
{
    public function getTask() : Task
    {
        return parent::getTask();
    }

    public function getErrorDataRepository() : ErrorDataRepository
    {
        return parent::getErrorDataRepository();
    }

    public function getWriter(Spreadsheet $spreadsheet) : Writer
    {
        return parent::getWriter($spreadsheet);
    }

    public function fetchErrorDataList(Task $task) : array
    {
        return parent::fetchErrorDataList($task);
    }

    public function download(array $errorDataList, Task $task) : bool
    {
        return parent::download($errorDataList, $task);
    }

    public function splicingFilePath(Task $task) : string
    {
        return parent::splicingFilePath($task);
    }

    public function getSpreadsheet() : Spreadsheet
    {
        return parent::getSpreadsheet();
    }

    public function fillWorksheet(
        Task $task,
        array $errorDataList,
        Spreadsheet $spreadsheet
    ) : Spreadsheet {
        return parent::fillWorksheet($task, $errorDataList, $spreadsheet);
    }

    public function fillWorksheetStyle(Worksheet $sheet)
    {
        return parent::fillWorksheetStyle($sheet);
    }

    public function fillWorksheetHeader(Worksheet $sheet, Task $task)
    {
        return parent::fillWorksheetHeader($sheet, $task);
    }

    public function fillWorksheetContent(Worksheet $sheet, array $errorDataList)
    {
        return parent::fillWorksheetContent($sheet, $errorDataList);
    }

    public function splicingErrorReason(ErrorData $errorData)
    {
        return parent::splicingErrorReason($errorData);
    }
    
    public function fetchTargetTemplate(Task $task) : Template
    {
        return parent::fetchTargetTemplate($task);
    }

    public function updateTaskStatus(Task $task) : bool
    {
        return parent::updateTaskStatus($task);
    }
}

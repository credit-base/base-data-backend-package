<?php
namespace BaseData\Rule\Translator;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\ModelFactory;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait RuleServiceTranslatorTrait
{
    protected function getModelFactory() : ModelFactory
    {
        return new ModelFactory();
    }
    
    public function arrayToObjectUtils(RuleService $ruleService, array $expression)
    {
        if (isset($expression['transformation_template_id'])) {
            $ruleService->getTransformationTemplate()->setId($expression['transformation_template_id']);
        }
        if (isset($expression['source_template_id'])) {
            $ruleService->getSourceTemplate()->setId($expression['source_template_id']);
        }
        if (isset($expression['rules'])) {
            $rules = unserialize(gzuncompress(base64_decode($expression['rules'], true)));

            foreach ($rules as $name => $condition) {
                $model = $this->getModelFactory()->getModel($name);
                $model->setCondition($condition);
                $model->setTransformationTemplate($ruleService->getTransformationTemplate());
                $model->setSourceTemplate($ruleService->getSourceTemplate());
                $rules[$name] = $model;
            }

            $ruleService->setRules($rules);
        }
        if (isset($expression['version'])) {
            $ruleService->setVersion($expression['version']);
        }
        if (isset($expression['data_total'])) {
            $ruleService->setDataTotal($expression['data_total']);
        }
        if (isset($expression['crew_id'])) {
            $ruleService->getCrew()->setId($expression['crew_id']);
        }
        if (isset($expression['usergroup_id'])) {
            $ruleService->getUserGroup()->setId($expression['usergroup_id']);
        }
        if (isset($expression['transformation_category'])) {
            $ruleService->setTransformationCategory($expression['transformation_category']);
        }
        if (isset($expression['source_category'])) {
            $ruleService->setSourceCategory($expression['source_category']);
        }
        if (isset($expression['status'])) {
            $ruleService->setStatus($expression['status']);
        }

        $ruleService->setCreateTime($expression['create_time']);
        $ruleService->setUpdateTime($expression['update_time']);
        $ruleService->setStatusTime($expression['status_time']);
    }
    
    public function objectToArrayUtils(RuleService $ruleService, array $keys)
    {
        $expression = array();

        if (in_array('transformationTemplate', $keys)) {
            $expression['transformation_template_id'] = $ruleService->getTransformationTemplate()->getId();
        }
        if (in_array('sourceTemplate', $keys)) {
            $expression['source_template_id'] = $ruleService->getSourceTemplate()->getId();
        }
        if (in_array('rules', $keys)) {
            $rules = $ruleService->getRules();
            foreach ($rules as $name => $rule) {
                if ($rule instanceof IRule) {
                    $rules[$name] = $rule->getCondition();
                }
            }

            foreach ($rules as $name => $rule) {
                if ($name == IRule::RULE_NAME['COMPLETION_RULE'] || $name == IRule::RULE_NAME['COMPARISON_RULE']) {
                    foreach ($rule as $identify => $condition) {
                        foreach ($condition as $key => $value) {
                            $rule[$identify][$key] = array(
                                'id' => $value['id'],
                                'base' => $value['base'],
                                'item' => $value['item']
                            );
                        }
                    }
                    $rules[$name] = $rule;
                }
            }

            $expression['rules'] = base64_encode(gzcompress(serialize($rules)));
        }

        if (in_array('version', $keys)) {
            $expression['version'] = $ruleService->getVersion();
        }
        if (in_array('dataTotal', $keys)) {
            $expression['data_total'] = $ruleService->getDataTotal();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $ruleService->getCrew()->getId();
        }
        if (in_array('userGroup', $keys)) {
            $expression['usergroup_id'] = $ruleService->getUserGroup()->getId();
        }
        if (in_array('transformationCategory', $keys)) {
            $expression['transformation_category'] = $ruleService->getTransformationCategory();
        }
        if (in_array('sourceCategory', $keys)) {
            $expression['source_category'] = $ruleService->getSourceCategory();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $ruleService->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $ruleService->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $ruleService->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $ruleService->getStatusTime();
        }
        
        return $expression;
    }
}

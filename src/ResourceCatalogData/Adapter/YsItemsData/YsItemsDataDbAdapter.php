<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\YsItemsData;
use BaseData\ResourceCatalogData\Model\NullYsItemsData;
use BaseData\ResourceCatalogData\Translator\YsItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\YsItemsDataRowCacheQuery;

class YsItemsDataDbAdapter implements IYsItemsDataAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new YsItemsDataDbTranslator();
        $this->rowCacheQuery = new YsItemsDataRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : YsItemsDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : YsItemsDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullYsItemsData::getInstance();
    }

    public function add(YsItemsData $ysItemsData) : bool
    {
        return $this->addAction($ysItemsData);
    }

    public function fetchOne($id) : YsItemsData
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        unset($filter);
        unset($conjection);

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        unset($sort);
        unset($conjection);

        return $condition;
    }
}

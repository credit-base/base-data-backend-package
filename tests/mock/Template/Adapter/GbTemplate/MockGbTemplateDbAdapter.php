<?php
namespace BaseData\Template\Adapter\GbTemplate;

use Marmot\Interfaces\INull;

use BaseData\Template\Translator\GbTemplateDbTranslator;
use BaseData\Template\Adapter\GbTemplate\Query\GbTemplateRowCacheQuery;

class MockGbTemplateDbAdapter extends GbTemplateDbAdapter
{
    public function getDbTranslator() : GbTemplateDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : GbTemplateRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

<?php
namespace BaseData\WorkOrderTask\Command\WorkOrderTask;

use PHPUnit\Framework\TestCase;

class RevokeWorkOrderTaskCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = array(
            'reason' => 'reason',
            'id' => 10,
        );

        $this->command = new RevokeWorkOrderTaskCommand(
            $this->fakerData['reason'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testReasonParameter()
    {
        $this->assertEquals($this->fakerData['reason'], $this->command->reason);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}

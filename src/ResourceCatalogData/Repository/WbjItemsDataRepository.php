<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\WbjItemsData;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\WbjItemsDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\WbjItemsDataMockAdapter;

class WbjItemsDataRepository extends Repository implements IWbjItemsDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WbjItemsDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWbjItemsDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWbjItemsDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWbjItemsDataAdapter
    {
        return new WbjItemsDataMockAdapter();
    }

    public function fetchOne($id) : WbjItemsData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(WbjItemsData $wbjItemsData) : bool
    {
        return $this->getAdapter()->add($wbjItemsData);
    }
}

<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter;

use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\Crew\Model\Crew;

use BaseData\Template\Model\Template;

use BaseData\UserGroup\Model\UserGroup;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class RuleService implements IObject, IOperate
{
    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );

    const NOT_COMPLETION_COMPARISON_CATEGORY = array(
        Template::CATEGORY['BASE'],
        Template::CATEGORY['QZJ_WBJ'],
        Template::CATEGORY['QZJ_BJ'],
        Template::CATEGORY['QZJ_GB']
    );

    use Object, OperateTrait, RuleTrait;

    protected $id;
    /**
     * @var Template $transformationTemplate 目标资源目录
     */
    protected $transformationTemplate;

    /**
     * @var int $transformationCategory 目标资源目录类型
     */
    protected $transformationCategory;

    /**
     * @var Template $sourceTemplate 来源资源目录
     */
    protected $sourceTemplate;

    /**
     * @var int $sourceCategory 来源资源目录类型
     */
    protected $sourceCategory;

    /**
     * @var array $rules 规则集合
     */
    protected $rules;

    /**
     * @var int $version 版本号
     */
    protected $version;

    /**
     * @var int $dataTotal 已转换数据量
     */
    protected $dataTotal;

    /**
     * @var Crew $crew 发布人
     */
    protected $crew;

    /**
     * @var UserGroup $userGroup 来源委办局
     */
    protected $userGroup;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->sourceCategory = Template::CATEGORY['WBJ'];
        $this->sourceTemplate = new Template();
        $this->rules = array();
        $this->version = 0;
        $this->dataTotal = 0;
        $this->crew = new Crew();
        $this->userGroup = new UserGroup();
        $this->transformationCategory = Template::CATEGORY['BJ'];
        $this->transformationTemplate = new Template();
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new RuleServiceRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->transformationCategory);
        unset($this->transformationTemplate);
        unset($this->sourceCategory);
        unset($this->sourceTemplate);
        unset($this->rules);
        unset($this->version);
        unset($this->dataTotal);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTransformationCategory(int $transformationCategory) : void
    {
        $this->transformationCategory = in_array($transformationCategory, Template::CATEGORY)
        ? $transformationCategory
        : Template::CATEGORY['BJ'];
    }

    public function getTransformationCategory() : int
    {
        return $this->transformationCategory;
    }

    public function setTransformationTemplate(Template $transformationTemplate)
    {
        $this->transformationTemplate = $transformationTemplate;
    }

    public function getTransformationTemplate() : Template
    {
        return $this->transformationTemplate;
    }

    public function setSourceCategory(int $sourceCategory) : void
    {
        $this->sourceCategory = in_array($sourceCategory, Template::CATEGORY)
        ? $sourceCategory
        : Template::CATEGORY['WBJ'];
    }

    public function getSourceCategory() : int
    {
        return $this->sourceCategory;
    }

    public function setSourceTemplate(Template $sourceTemplate)
    {
        $this->sourceTemplate = $sourceTemplate;
    }

    public function getSourceTemplate() : Template
    {
        return $this->sourceTemplate;
    }

    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    public function getRules() : array
    {
        return $this->rules;
    }

    public function setVersion(int $version)
    {
        $this->version = $version;
    }

    public function getVersion() : int
    {
        return $this->version;
    }

    public function setDataTotal(int $dataTotal)
    {
        $this->dataTotal = $dataTotal;
    }

    public function getDataTotal() : int
    {
        return $this->dataTotal;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : IRuleServiceAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        return $this->validate() &&
        $this->getRepository()->add($this) &&
        $this->updateTemplateRuleCount();
    }

    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));
        
        return $this->validate() && $this->getRepository()->edit(
            $this,
            array(
                'rules',
                'crew',
                'version',
                'userGroup',
                'updateTime'
            )
        );
    }

    protected function validate() : bool
    {
        return $this->commonValidate() && $this->rulesValidate();
    }

    protected function commonValidate() : bool
    {
        return $this->isCrewExist() &&
               $this->isSourceTemplateExist() &&
               $this->isTransformationTemplateExist() &&
               $this->isRuleNotExist() &&
               $this->isRuleFormatCorrect();
    }

    //验证发布人不为空
    protected function isCrewExist() : bool
    {
        if ($this->getCrew() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'crewId'));
            return false;
        }

        return true;
    }

    //验证来源资源目录不为空
    protected function isSourceTemplateExist() : bool
    {
        if ($this->getSourceTemplate() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'sourceTemplateId'));
            return false;
        }

        return true;
    }

    //验证目标资源目录不为空
    protected function isTransformationTemplateExist() : bool
    {
        if ($this->getTransformationTemplate() instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'transformationTemplateId'));
            return false;
        }

        return true;
    }

    //验证规则是否存在
    protected function isRuleNotExist() : bool
    {
        $filter = array();
        $filter['transformationTemplate'] = $this->getTransformationTemplate()->getId();
        $filter['sourceTemplate'] = $this->getSourceTemplate()->getId();
        $filter['transformationCategory'] = $this->getTransformationCategory();
        $filter['sourceCategory'] = $this->getSourceCategory();
        $filter['status'] = self::STATUS['NORMAL'];
        if (!empty($this->getId())) {
            $filter['id'] = $this->getId();
        }

        list($ruleList, $count) = $this->getRepository()->filter($filter);
        unset($ruleList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'rule'));
            return false;
        }

        return true;
    }

    //验证规则格式
    protected function isRuleFormatCorrect() : bool
    {
        $rules = $this->getRules();
        
        //获取转换资源目录的必填项字段
        $items = $this->getTransformationTemplate()->getItems();
        $requiredItems = array();

        foreach ($items as $item) {
            if ($item['isNecessary'] == Template::IS_NECESSARY['SHI']) {
                $requiredItems[] = $item['identify'];
            }
        }

        //获取转换规则和补全规则的信息项
        $mappingItems = array();

        $completionRule = isset($rules['completionRule']) ? $rules['completionRule']->getCondition() : array();
        $transformationRule = isset($rules['transformationRule']) ?
                              $rules['transformationRule']->getCondition() :
                              array();

        foreach ($transformationRule as $key => $value) {
            unset($value);
            $mappingItems[] = $key;
        }

        foreach ($completionRule as $key => $value) {
            unset($value);
            $mappingItems[] = $key;
        }
        
        foreach ($requiredItems as $item) {
            if (!in_array($item, $mappingItems)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'requiredItemsMapping'));
                return false;
            }
        }

        return true;
    }

    protected function rulesValidate() : bool
    {
        $rules = $this->getRules();

        foreach ($rules as $rule) {
            if (!$rule->validate()) {
                return false;
            }
        }

        return true;
    }

    /** @todo 修改模板规则数*/
    protected function updateTemplateRuleCount() : bool
    {
        return true;
    }

    public function isNormal() : bool
    {
        return $this->getStatus() == self::STATUS['NORMAL'];
    }

    public function delete() : bool
    {
        if (!$this->isNormal()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        $this->setStatus(self::STATUS['DELETED']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array('updateTime','status','statusTime','crew')
        );
    }

    /**
     * 数据转换
     * @param ISearchDataAble $sourceSearchData 来源资源目录数据
     * @return ISearchDataAble
     */
    public function transformation(ISearchDataAble $sourceSearchData) : ISearchDataAble
    {
        //获取目标资源目录数据
        $transformationTemplate = $this->getTransformationTemplate();
        //初始化目标资源目录-默认为待补全资源目录数据
        $transformationSearchData = $this->initializationTransformationSearchData(
            $sourceSearchData,
            $transformationTemplate
        );
        
        //获取规则集合
        $rules = $this->getRules();
        //根据规则的优先级对数组进行重新排序
        $rules = $this->getSortRules($rules);
        //根据规则转换数据
        foreach ($rules as $rule) {
            $transformationSearchData = $rule->transformation($sourceSearchData, $transformationSearchData);
        }

        return ($transformationSearchData instanceof ErrorData) ?
                $this->errorReasonFormatConversion($transformationSearchData) :
                $transformationSearchData;
    }

    /**
     * 根据优先级对规则进行排序
     * @param array $rules 规则集合
     * @return array
     */
    protected function getSortRules(array $rules) : array
    {
        $sortRules = array();
        foreach (IRule::RULE_PRIORITY as $name) {
            if (isset($rules[$name])) {
                $sortRules[$name] = $rules[$name];
            }
        }

        return $sortRules;
    }
}

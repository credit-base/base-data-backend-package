<?php
namespace BaseData\WorkOrderTask\WidgetRule;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Common\Utils\StringGenerate;

use BaseData\WorkOrderTask\Model\ParentTask;

/**
 * @SuppressWarnings(PHPMD)
 */
class ParentTaskWidgetRuleTest extends TestCase
{
    private $widgetRule;

    public function setUp()
    {
        $this->widgetRule = new ParentTaskWidgetRule();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->widgetRule);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    //templateType -- start
    /**
     * @dataProvider invalidTemplateTypeProvider
     */
    public function testTemplateTypeInvalid($actual, $expected)
    {
        $result = $this->widgetRule->templateType($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidTemplateTypeProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->word, false),
            array($faker->regexify('[4-9]{1}'), false),
            array($faker->randomElements(ParentTask::TEMPLATE_TYPE, 1), false),
            array($faker->randomElement(ParentTask::TEMPLATE_TYPE, 1), true),
        );
    }
    //templateType -- end

    //title -- start
    /**
     * @dataProvider invalidTitleProvider
     */
    public function testTitleInvalid($actual, $expected)
    {
        $result = $this->widgetRule->title($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidTitleProvider()
    {
        return array(
            array('', false),
            array(StringGenerate::generate(ParentTaskWidgetRule::TITLE_MIN_LENGTH-1), false),
            array(StringGenerate::generate(ParentTaskWidgetRule::TITLE_MAX_LENGTH+1), false),
            array(StringGenerate::generate(ParentTaskWidgetRule::TITLE_MIN_LENGTH), true),
            array(StringGenerate::generate(ParentTaskWidgetRule::TITLE_MIN_LENGTH+1), true),
            array(StringGenerate::generate(ParentTaskWidgetRule::TITLE_MAX_LENGTH), true),
            array(StringGenerate::generate(ParentTaskWidgetRule::TITLE_MAX_LENGTH-1), true)
        );
    }
    //title -- end

    //description -- start
    /**
     * @dataProvider invalidDescriptionProvider
     */
    public function testDescriptionInvalid($actual, $expected)
    {
        $result = $this->widgetRule->description($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidDescriptionProvider()
    {
        return array(
            array(StringGenerate::generate(ParentTaskWidgetRule::DESCRIPTION_MAX_LENGTH+1), false),
            array(StringGenerate::generate(ParentTaskWidgetRule::DESCRIPTION_MIN_LENGTH), true),
            array(StringGenerate::generate(ParentTaskWidgetRule::DESCRIPTION_MIN_LENGTH+1), true),
            array(StringGenerate::generate(ParentTaskWidgetRule::DESCRIPTION_MAX_LENGTH), true),
            array(StringGenerate::generate(ParentTaskWidgetRule::DESCRIPTION_MAX_LENGTH-1), true),
            array('', true)
        );
    }
    //description -- end

    //endTime -- start
    /**
     * @dataProvider invalidEndTimeProvider
     */
    public function testEndTimeInvalid($actual, $expected)
    {
        $result = $this->widgetRule->endTime($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidEndTimeProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->word, false),
            array($faker->regexify('\d{3}-\d{2}-\d{2}'), false),
            array($faker->regexify('2021-01-33'), false),
            array($faker->regexify('2021-01-01'), true),
        );
    }
    //endTime -- end

    //pdf -- start
    /**
     * @dataProvider invalidPdfProvider
     */
    public function testPdfInvalid($actual, $expected)
    {
        $result = $this->widgetRule->pdf($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
    }

    public function invalidPdfProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array($faker->word, false),
            array(array($faker->word), false),
            array(array('identify' => $faker->word.'.doc'), false),
            array(array('name' => 'name', 'identify' => 'identify.pdf'), true),
            array(array(), true),
        );
    }
    //pdf -- end
}

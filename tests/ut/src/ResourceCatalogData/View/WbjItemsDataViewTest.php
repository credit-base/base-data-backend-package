<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\WbjItemsData;

class WbjItemsDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $wbjItemsData = new WbjItemsDataView(new WbjItemsData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $wbjItemsData);
    }
}

<?php
namespace BaseData\Template\Adapter\WbjTemplate\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class WbjTemplateCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockWbjTemplateCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(WbjTemplateCache::KEY, $this->cache->getKey());
    }
}

<?php
namespace BaseData\Template\BjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\BjTemplate;

/**
 * 测试本级资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    private $bjTemplate;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->bjTemplate);
        $this->clear('pcore_gb_template');
        $this->clear('pcore_bj_template');
    }

    /**
     * 存在一条本级资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_bj_template' => $this->bjTemplate()
            ]
        );
    }

    public function testViewBjTemplate()
    {
        $setBjTemplate = $this->bjTemplate()[0];
        $setGbTemplate = $this->gbTemplate()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'bjTemplates/1?include=gbTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->bjTemplateCompareData($setBjTemplate, $contents['data']);
        $this->gbTemplateCompareData($setGbTemplate, $contents['included'][0]);
    }
}

<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\YsSearchData;

class YsSearchDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $ysSearchData = new YsSearchDataView(new YsSearchData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $ysSearchData);
    }
}

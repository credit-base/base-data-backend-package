<?php
namespace BaseData\ResourceCatalogData\ErrorData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试错误资源目录接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_error_data');
    }

    /**
     * 存在多条错误资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_task' => $this->task(),
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_error_data' => $this->errorData(),
            ]
        );
    }

    public function testViewErrorData()
    {
        $setErrorDataList = $this->errorData();
        $setTaskList = $this->task();
        foreach ($setTaskList as $setTask) {
            $setTaskList[$setTask['task_id']] = $setTask;
        }
        $setTemplateList = $this->bjTemplate();
        foreach ($setTemplateList as $setTemplate) {
            $setTemplateList[$setTemplate['bj_template_id']] = $setTemplate;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'errorDatas/1,2?include=task,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $errorData) {
            $this->errorDataCompareData($setErrorDataList[$key], $errorData);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'task') {
                $this->taskCompareData($setTaskList[$value['id']], $value);
            }
            if ($value['type'] == 'bjTemplates') {
                $this->bjTemplateCompareData($setTemplateList[$value['id']], $value);
            }
        }
    }
}

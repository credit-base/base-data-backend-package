<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class NullRule extends Rule implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    public function validate() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        unset($transformationSearchData);
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {
        unset($sourceSearchData);
        unset($transformationSearchData);
        return array();
    }

    public function transformationExecute(
        array $data,
        ISearchDataAble $transformationSearchData
    ) : array {
        unset($data);
        unset($transformationSearchData);
        return array();
    }

    public function transformationResult(
        array $data,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        unset($data);
        return $transformationSearchData;
    }

    public function transformation(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        unset($sourceSearchData);
        return $transformationSearchData;
    }
}

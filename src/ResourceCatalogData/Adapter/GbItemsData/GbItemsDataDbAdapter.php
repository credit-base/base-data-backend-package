<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\GbItemsData;
use BaseData\ResourceCatalogData\Model\NullGbItemsData;
use BaseData\ResourceCatalogData\Translator\GbItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\GbItemsDataRowCacheQuery;

class GbItemsDataDbAdapter implements IGbItemsDataAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new GbItemsDataDbTranslator();
        $this->rowCacheQuery = new GbItemsDataRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : GbItemsDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : GbItemsDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullGbItemsData::getInstance();
    }

    public function add(GbItemsData $gbItemsData) : bool
    {
        return $this->addAction($gbItemsData);
    }

    public function fetchOne($id) : GbItemsData
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        unset($filter);
        unset($conjection);

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        unset($sort);
        unset($conjection);

        return $condition;
    }
}

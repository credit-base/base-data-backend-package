<?php
namespace BaseData\Template\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Template\Model\Template;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
abstract class TemplateDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $template = null)
    {
        if (isset($expression['name'])) {
            $template->setName($expression['name']);
        }
        if (isset($expression['identify'])) {
            $template->setIdentify($expression['identify']);
        }
        if (isset($expression['subject_category'])) {
            $template->setSubjectCategory(json_decode($expression['subject_category'], true));
        }
        if (isset($expression['dimension'])) {
            $template->setDimension($expression['dimension']);
        }
        if (isset($expression['exchange_frequency'])) {
            $template->setExchangeFrequency($expression['exchange_frequency']);
        }
        if (isset($expression['info_classify'])) {
            $template->setInfoClassify($expression['info_classify']);
        }
        if (isset($expression['info_category'])) {
            $template->setInfoCategory($expression['info_category']);
        }
        if (isset($expression['description'])) {
            $template->setDescription($expression['description']);
        }
        if (isset($expression['items'])) {
            $template->setItems(json_decode($expression['items'], true));
        }

        if (isset($expression['create_time'])) {
            $template->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $template->setUpdateTime($expression['update_time']);
        }
        if (isset($expression['status'])) {
            $template->setStatus($expression['status']);
        }
        if (isset($expression['status_time'])) {
            $template->setStatusTime($expression['status_time']);
        }

        return $template;
    }

    public function objectToArray($template, array $keys = array())
    {
        if (!$template instanceof Template) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('name', $keys)) {
            $expression['name'] = $template->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $template->getIdentify();
        }
        if (in_array('subjectCategory', $keys)) {
            $expression['subject_category'] = $template->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = $template->getDimension();
        }
        if (in_array('exchangeFrequency', $keys)) {
            $expression['exchange_frequency'] = $template->getExchangeFrequency();
        }
        if (in_array('infoClassify', $keys)) {
            $expression['info_classify'] = $template->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $expression['info_category'] = $template->getInfoCategory();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $template->getDescription();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $template->getItems();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $template->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $template->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $template->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $template->getStatusTime();
        }
        
        return $expression;
    }
}

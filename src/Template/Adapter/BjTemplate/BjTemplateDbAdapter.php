<?php
namespace BaseData\Template\Adapter\BjTemplate;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\NullBjTemplate;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Translator\BjTemplateDbTranslator;
use BaseData\Template\Adapter\BjTemplate\Query\BjTemplateRowCacheQuery;

class BjTemplateDbAdapter implements IBjTemplateAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $gbTemplateRepository;

    public function __construct()
    {
        $this->dbTranslator = new BjTemplateDbTranslator();
        $this->rowCacheQuery = new BjTemplateRowCacheQuery();
        $this->gbTemplateRepository = new GbTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->gbTemplateRepository);
    }
    
    protected function getDbTranslator() : BjTemplateDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : BjTemplateRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getGbTemplateRepository() : GbTemplateRepository
    {
        return $this->gbTemplateRepository;
    }

    protected function getNullObject() : INull
    {
        return NullBjTemplate::getInstance();
    }

    public function add(BjTemplate $bjTemplate) : bool
    {
        return $this->addAction($bjTemplate);
    }

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool
    {
        return $this->editAction($bjTemplate, $keys);
    }

    public function fetchOne($id) : BjTemplate
    {
        $bjTemplate = $this->fetchOneAction($id);
        $this->fetchGbTemplate($bjTemplate);

        return $bjTemplate;
    }

    public function fetchList(array $ids) : array
    {
        $bjTemplateList = $this->fetchListAction($ids);
        $this->fetchGbTemplate($bjTemplateList);

        return $bjTemplateList;
    }

    protected function fetchGbTemplate($bjTemplate)
    {
        return is_array($bjTemplate) ?
        $this->fetchGbTemplateByList($bjTemplate) :
        $this->fetchGbTemplateByObject($bjTemplate);
    }

    protected function fetchGbTemplateByObject(BjTemplate $bjTemplate)
    {
        $gbTemplateId = $bjTemplate->getGbTemplate()->getId();
        $gbTemplate = $this->getGbTemplateRepository()->fetchOne($gbTemplateId);
        $bjTemplate->setGbTemplate($gbTemplate);
    }

    protected function fetchGbTemplateByList(array $bjTemplateList)
    {
        $gbTemplateIds = array();
        foreach ($bjTemplateList as $bjTemplate) {
            $gbTemplateIds[] = $bjTemplate->getGbTemplate()->getId();
        }

        $gbTemplateList = $this->getGbTemplateRepository()->fetchList($gbTemplateIds);
        if (!empty($gbTemplateList)) {
            foreach ($bjTemplateList as $bjTemplate) {
                $gbTemplateId = $bjTemplate->getGbTemplate()->getId();
                if (isset($gbTemplateList[$gbTemplateId])
                    && ($gbTemplateList[$gbTemplateId]->getId() == $gbTemplateId)) {
                    $bjTemplate->setGbTemplate($gbTemplateList[$gbTemplateId]);
                }
            }
        }
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $bjTemplate = new BjTemplate();

            if (isset($filter['name'])) {
                $bjTemplate->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($bjTemplate, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['identify'])) {
                $bjTemplate->setIdentify($filter['identify']);
                $info = $this->getDbTranslator()->objectToArray($bjTemplate, array('identify'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['dimension'])) {
                $bjTemplate->setDimension($filter['dimension']);
                $info = $this->getDbTranslator()->objectToArray($bjTemplate, array('dimension'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoClassify'])) {
                $bjTemplate->setInfoClassify($filter['infoClassify']);
                $info = $this->getDbTranslator()->objectToArray($bjTemplate, array('infoClassify'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoCategory'])) {
                $bjTemplate->setInfoCategory($filter['infoCategory']);
                $info = $this->getDbTranslator()->objectToArray($bjTemplate, array('infoCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceUnit'])) {
                $bjTemplate->getSourceUnit()->setId($filter['sourceUnit']);
                $info = $this->getDbTranslator()->objectToArray($bjTemplate, array('sourceUnit'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new BjTemplate(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray(new BjTemplate(), array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

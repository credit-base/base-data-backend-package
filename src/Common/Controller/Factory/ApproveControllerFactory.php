<?php
namespace BaseData\Common\Controller\Factory;

use BaseData\Common\Controller\NullApproveController;
use BaseData\Common\Controller\Interfaces\IApproveAbleController;

class ApproveControllerFactory
{
    const MAPS = array(
        'unAuditedRules'=>'\BaseData\Rule\Controller\RuleServiceApproveController'
    );

    public static function getController(string $resource) : IApproveAbleController
    {
        $approveController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        return class_exists($approveController) ? new $approveController : NullApproveController::getInstance();
    }
}

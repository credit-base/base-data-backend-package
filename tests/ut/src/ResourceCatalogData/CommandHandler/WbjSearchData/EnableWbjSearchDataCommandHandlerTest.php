<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use PHPUnit\Framework\TestCase;

class EnableWbjSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnableWbjSearchDataCommandHandler::class)
                       ->setMethods(['fetchWbjSearchData'])
                       ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'BaseData\Common\CommandHandler\EnableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $wbjSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);

        $this->stub->expects($this->once())
             ->method('fetchWbjSearchData')
             ->with($id)
             ->willReturn($wbjSearchData);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $wbjSearchData);
    }
}

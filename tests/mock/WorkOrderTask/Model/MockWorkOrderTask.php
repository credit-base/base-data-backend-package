<?php
namespace BaseData\WorkOrderTask\Model;

use BaseData\Template\Model\WbjTemplate;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockWorkOrderTask extends WorkOrderTask
{
    public function getRepository() : IWorkOrderTaskAdapter
    {
        return parent::getRepository();
    }

    public function isDqr() : bool
    {
        return parent::isDqr();
    }

    public function isGjz() : bool
    {
        return parent::isGjz();
    }

    public function isTemplateExist() : bool
    {
        return parent::isTemplateExist();
    }

    public function getItems() : array
    {
        return parent::getItems();
    }

    public function getWbjTemplate() : WbjTemplate
    {
        return parent::getWbjTemplate();
    }

    public function addWbjTemplate(array $items) : bool
    {
        return parent::addWbjTemplate($items);
    }
}

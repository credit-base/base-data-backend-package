<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator;

/**
 * @Feature: 我是前台用户,当我需要查看特定条件资源目录数据时,在门户网portal中,可以搜索我需要的所有资源目录数据
 *           通过列表的形式展现我搜索的所有资源目录数据,以便于我可以快速查看到我需要的资源目录数据
 * @Scenario: 前台用户通过信息分类搜索
 */
class InfoClassifyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_ys_items_data');
        $this->clear('pcore_ys_search_data');
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchYsSearchDataList()
    {
        $repository = new YsSearchDataRepository();
 
        $filter['infoClassify'] = ISearchDataAble::INFO_CLASSIFY['XZXK'];

        list($ysSearchDataList, $count) = $repository->filter($filter);
        
        unset($count);

        return $ysSearchDataList;
    }

    /**
     * @Then: 我可以看到信息分类为行政许可的所有资源目录数据
     */
    public function testViewYsSearchDataList()
    {
        $setYsSearchDataList = $this->ysSearchData()['pcore_ys_search_data'];

        foreach ($setYsSearchDataList as $key => $setYsSearchData) {
            unset($setYsSearchData);
            $setYsSearchDataList[$key]['ys_search_data_id'] = $key +1;
        }

        $ysSearchDataList = $this->fetchYsSearchDataList();
        $translator = new YsSearchDataDbTranslator();
        foreach ($ysSearchDataList as $ysSearchData) {
            $ysSearchDataArray[] = $translator->objectToArray($ysSearchData);
        }

        $this->assertEquals($ysSearchDataArray, $setYsSearchDataList);

        foreach ($ysSearchDataArray as $ysSearchData) {
            $this->assertEquals(ISearchDataAble::INFO_CLASSIFY['XZXK'], $ysSearchData['info_classify']);
        }
    }
}

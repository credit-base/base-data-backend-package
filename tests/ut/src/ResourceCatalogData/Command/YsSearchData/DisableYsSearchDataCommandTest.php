<?php
namespace BaseData\ResourceCatalogData\Command\YsSearchData;

use PHPUnit\Framework\TestCase;

class DisableYsSearchDataCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new DisableYsSearchDataCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsDisableCommand()
    {
        $this->assertInstanceof('BaseData\Common\Command\DisableCommand', $this->stub);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\Task;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\Task;

class TaskMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new TaskMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new Task()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new Task(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\Task',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\Task',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\Task',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

<?php
namespace BaseData\Rule\Model;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class CompletionRule extends Rule implements IRule
{
    use RuleTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 验证补全规则格式
        * 规则格式说明及样例
            * array(
            *     '补全信息项标识' => array(
            *         array('id'=>补全信息项来源id, 'base'=> array(补全依据), 'item'=>'被补全信息项标识'),
            *     ),
            *     'ZTMC' => array(
            *         array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
            *         array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            *     ),
            *     'TYSHXYDM' => array(
            *         array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
            *         array('id'=>2, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
            *     ),
            * )
        * 验证流程
            * 1. 获取补全规则 $condition = $this->getCondition();
            * 2. 获取目标资源目录和根据获取到的补全规则获取补全资源目录id集合
            * 3. 通过获取到的资源目录id集合获取资源目录
            * 4. 通过获取到的资源目录验证补全信息项存在,且验证补全信息项的字段格式
            * 5. 对比目标和补全信息项的模板字段的类型:
                * 数据类型对应关系(目标:来源): 日期:日期,字符串:全部,集合:集合,枚举:枚举,整型:整型,浮点:浮点-整型
                * 数据长度: 目标>=来源(全部),目标项包含来源(集合-枚举)
                * 公开范围: 目标>=来源
                * 是否脱敏: 不脱敏>脱敏
                *  脱敏规则: 目标>=来源
            * 6. 验证补全依据的合理性: 如果补全资源目录为自然人,补全依据必须包含身份证号
     * @return bool
     * @todo 后续需要对返回错误输出信息或日志，方便调试
     */
    public function validate() : bool
    {
        //获取目标资源目录
        $transformationTemplate = $this->getTransformationTemplate();

        //获取目标资源目录的模板信息项
        $transformationTemplateItems = $this->fetchTemplateItems($transformationTemplate);

        //获取补全规则
        $condition = $this->getCondition();

        //获取补全的资源目录集合
        $completionTemplateList = $this->fetchTemplateList($condition);

        foreach ($condition as $transformationItemIdentify => $patterns) {
            //验证目标资源目录模板信息项存在
            if (!isset($transformationTemplateItems[$transformationItemIdentify])) {
                Core::setLastError(
                    PARAMETER_FORMAT_ERROR,
                    array('pointer'=>'completionRulesTransformationItemNotExist')
                );
                return false;
            }

            foreach ($patterns as $pattern) {
                //验证补全资源目录存在
                if (!isset($completionTemplateList[$pattern['id']])) {
                    Core::setLastError(
                        PARAMETER_FORMAT_ERROR,
                        array('pointer'=>'completionRulesCompletionTemplateNotExist')
                    );
                    return false;
                }

                //如果补全资源目录为自然人,依据必须包含身份证号
                $completionTemplate = $completionTemplateList[$pattern['id']];
                if (!$this->validateBase($completionTemplate, $pattern['base'])) {
                    return false;
                }

                //获取补全资源目录模板信息项
                $completionTemplateItems = $this->fetchTemplateItems($completionTemplate);

                //验证补全资源目录模板信息项存在
                if (!isset($completionTemplateItems[$pattern['item']])) {
                    Core::setLastError(
                        PARAMETER_FORMAT_ERROR,
                        array('pointer'=>'completionRulesCompletionTemplateItemNotExist')
                    );
                    return false;
                }

                //验证信息项格式
                $transformationItem = $transformationTemplateItems[$transformationItemIdentify];
                $sourceItem = $completionTemplateItems[$pattern['item']];

                if (!$this->templateItemComparison($transformationItem, $sourceItem)) {
                    return false;
                }
            }
        }

        return true;
    }

    //实现父类的抽象方法
    protected function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        unset($transformationSearchData);
        return true;
    }
    /**
     * 初始化数据(实现父类的抽象方法)
     * @param ISearchDataAble $sourceSearchData 来源资源目录数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * condition 补全条件
        * transformationItemsData 目标资源目录数据
        * requiredItems 必填资源目录模板信息项
     */
    protected function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {
        unset($sourceSearchData);
        
        //获取补全条件
        $condition = $this->getCondition();
        //获取目标资源目录
        $transformationTemplate = $transformationSearchData->getTemplate();
        //获取目标资源目录数据信息项
        $transformationItemsData = $transformationSearchData->getItemsData()->getData();
        //获取目标资源目录必填项
        $requiredItems = $this->transformationTemplateRequired($transformationTemplate);

        return array(
            'condition' => $condition,
            'transformationItemsData' => $transformationItemsData,
            'requiredItems' => $requiredItems
        );
    }

    //验证初始化数据格式
    protected function validateInitializationDataFormat(array $data) : bool
    {
        return (isset($data['condition']) &&
                isset($data['transformationItemsData']) &&
                isset($data['requiredItems']));
    }
    /**
     * 执行转换过程(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * transformationItemsData 目标资源目录数据模板数据
        * incompleteItems 待补全的数据信息项
     */
    protected function transformationExecute(array $data, ISearchDataAble $transformationSearchData) : array
    {
        //验证初始化数据格式,false返回空数组
        if (!$this->validateInitializationDataFormat($data)) {
            return array();
        }
        
        $transformationItemsData = $data['transformationItemsData'];
        
        foreach ($data['condition'] as $transformationIdentify => $patterns) {
            foreach ($patterns as $pattern) {
                //判断补全信息数据项值是否为空,为空则进行补全
                if (empty($transformationItemsData[$transformationIdentify])) {
                    //获取补全的资源目录数据列表
                    $searchDataList = $this->fetchSearchDataList($pattern, $transformationSearchData);

                    //获取补全信息项的数据
                    $completionItemData = $this->filterExistItemsDataByIdentify($searchDataList, $pattern['item']);

                    //赋值
                    $transformationItemsData[$transformationIdentify] = $completionItemData;
                }
            }
        }

        //获取待补全的数据信息项
        $incompleteItems = $this->fetchIncompleteItems($transformationItemsData, $data['requiredItems']);
        
        return array(
            'transformationItemsData' => $transformationItemsData,
            'incompleteItems' => $incompleteItems
        );
    }

    //验证执行后返回的数据格式
    protected function validateDataFormat(array $data) : bool
    {
        return (isset($data['transformationItemsData']) && isset($data['incompleteItems']));
    }

    /**
     * 判断并返回结果(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return ISearchDataAble
     */
    protected function transformationResult(array $data, ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        //验证数据格式不正确,直接返回异常失败的目标资源目录数据
        if (!$this->validateDataFormat($data)) {
            $transformationSearchData = $this->failureDataException(
                $transformationSearchData,
                ErrorData::STATUS['PROGRAM_EXCEPTION']
            );
        }

        $transformationItemsData = isset($data['transformationItemsData']) ? $data['transformationItemsData'] : array();
        $incompleteItems = isset($data['incompleteItems']) ? $data['incompleteItems'] : array();

        //判断待补全信息项是否为空,空则返回完善的资源目录数据,不为空则返回待补全的资源目录数据
        $transformationSearchData = $this->transformationDataResult($incompleteItems, $transformationSearchData);
        $transformationSearchData->getItemsData()->setData($transformationItemsData);
        $transformationSearchData->generateHash();
        
        return $transformationSearchData;
    }
}

<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Model\IApproveAble;
use BaseData\Common\Model\IResubmitAble;
use BaseData\Common\Model\ApproveAbleTrait;
use BaseData\Common\Model\ResubmitAbleTrait;

use BaseData\Crew\Model\Crew;

use BaseData\UserGroup\Model\UserGroup;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\WbjTemplate;

use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class UnAuditedRuleServiceTest extends TestCase
{
    private $unAuditedRuleService;

    public function setUp()
    {
        $this->unAuditedRuleService = new MockUnAuditedRuleService();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->unAuditedRuleService);
    }

    public function testImplementsIApproveAble()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IApproveAble',
            $this->unAuditedRuleService
        );
    }

    public function testImplementsIResubmitAble()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IResubmitAble',
            $this->unAuditedRuleService
        );
    }

    public function testGetUnAuditedRuleServiceRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\UnAuditedRuleServiceRepository',
            $this->unAuditedRuleService->getUnAuditedRuleServiceRepository()
        );
    }

    public function testUnAuditedRuleServiceConstructor()
    {
        $this->assertEquals(0, $this->unAuditedRuleService->getApplyId());
        $this->assertInstanceOf(
            'BaseData\Crew\Model\Crew',
            $this->unAuditedRuleService->getPublishCrew()
        );
        $this->assertInstanceOf(
            'BaseData\UserGroup\Model\UserGroup',
            $this->unAuditedRuleService->getUserGroup()
        );
        $this->assertInstanceOf(
            'BaseData\Crew\Model\Crew',
            $this->unAuditedRuleService->getApplyCrew()
        );
        $this->assertInstanceOf(
            'BaseData\UserGroup\Model\UserGroup',
            $this->unAuditedRuleService->getApplyUserGroup()
        );
        $this->assertEquals(
            IApproveAble::OPERATION_TYPE['NULL'],
            $this->unAuditedRuleService->getOperationType()
        );
        $this->assertEquals(array(), $this->unAuditedRuleService->getApplyInfo());
        $this->assertEquals(
            Template::CATEGORY['BJ'],
            $this->unAuditedRuleService->getTransformationCategory()
        );
        $this->assertEquals(
            Template::CATEGORY['WBJ'],
            $this->unAuditedRuleService->getSourceCategory()
        );
        $this->assertInstanceOf(
            'BaseData\Template\Model\Template',
            $this->unAuditedRuleService->getTransformationTemplate()
        );
        $this->assertInstanceOf(
            'BaseData\Template\Model\Template',
            $this->unAuditedRuleService->getSourceTemplate()
        );
        $this->assertEquals(0, $this->unAuditedRuleService->getRelationId());
        $this->assertEmpty($this->unAuditedRuleService->getRejectReason());
        $this->assertEquals(IApproveAble::APPLY_STATUS['PENDING'], $this->unAuditedRuleService->getApplyStatus());
    }
    
    public function testSetApplyId()
    {
        $this->unAuditedRuleService->setApplyId(1);
        $this->assertEquals(1, $this->unAuditedRuleService->getApplyId());
    }
    
    //relationId 测试 ------------------------------------------------------- start
    /**
     * 设置 News setRelationId() 正确的传参类型,期望传值正确
     */
    public function testSetRelationIdCorrectType()
    {
        $this->unAuditedRuleService->setRelationId(1);
        $this->assertEquals(1, $this->unAuditedRuleService->getRelationId());
    }

    /**
     * 设置 News setRelationId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRelationIdWrongType()
    {
        $this->unAuditedRuleService->setRelationId(array(1, 2, 3));
    }
    //relationId 测试 -------------------------------------------------------   end

    //operationType 测试 ------------------------------------------------------ start
    /**
     * 循环测试 News setOperationType() 是否符合预定范围
     *
     * @dataProvider operationTypeProvider
     */
    public function testSetOperationType($actual, $expected)
    {
        $this->unAuditedRuleService->setOperationType($actual);
        $this->assertEquals($expected, $this->unAuditedRuleService->getOperationType());
    }

    /**
     * 循环测试 News setOperationType() 数据构建器
     */
    public function operationTypeProvider()
    {
        return array(
            array(IApproveAble::OPERATION_TYPE['ADD'],IApproveAble::OPERATION_TYPE['ADD']),
            array(IApproveAble::OPERATION_TYPE['EDIT'],IApproveAble::OPERATION_TYPE['EDIT']),
            array(999,IApproveAble::OPERATION_TYPE['NULL']),
        );
    }

    /**
     * 设置 News setOperationType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperationTypeWrongType()
    {
        $this->unAuditedRuleService->setOperationType('string');
    }
    //operationType 测试 ------------------------------------------------------   end

    //applyInfo 测试 ------------------------------------------------------- start
    /**
     * 设置 News setApplyInfo() 正确的传参类型,期望传值正确
     */
    public function testSetApplyInfoCorrectType()
    {
        $this->unAuditedRuleService->setApplyInfo(array('applyInfo'));
        $this->assertEquals(array('applyInfo'), $this->unAuditedRuleService->getApplyInfo());
    }

    /**
     * 设置 News setApplyInfo() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyInfoWrongType()
    {
        $this->unAuditedRuleService->setApplyInfo('applyInfo');
    }
    //applyInfo 测试 -------------------------------------------------------   end
    
    //publishCrew 测试 --------------------------------------------- start
    /**
     * 设置 News setPublishCrew() 正确的传参类型,期望传值正确
     */
    public function testSetPublishCrewCorrectType()
    {
        $crew = new Crew();
        $this->unAuditedRuleService->setPublishCrew($crew);
        $this->assertSame($crew, $this->unAuditedRuleService->getPublishCrew());
    }

    /**
     * 设置 News setPublishCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublishCrewWrongType()
    {
        $this->unAuditedRuleService->setPublishCrew('string');
    }
    //publishCrew 测试 ---------------------------------------------   end

    //applyCrew 测试 --------------------------------------------- start
    /**
     * 设置 News setApplyCrew() 正确的传参类型,期望传值正确
     */
    public function testSetApplyCrewCorrectType()
    {
        $crew = new Crew();
        $this->unAuditedRuleService->setApplyCrew($crew);
        $this->assertSame($crew, $this->unAuditedRuleService->getApplyCrew());
    }

    /**
     * 设置 News setApplyCrew() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyCrewWrongType()
    {
        $this->unAuditedRuleService->setApplyCrew('string');
    }
    //applyCrew 测试 ---------------------------------------------   end

    //userGroup 测试 --------------------------------------------- start
    /**
     * 设置 News setUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetUserGroupCorrectType()
    {
        $userGroup = new UserGroup();
        $this->unAuditedRuleService->setUserGroup($userGroup);
        $this->assertSame($userGroup, $this->unAuditedRuleService->getUserGroup());
    }

    /**
     * 设置 News setUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserGroupWrongType()
    {
        $this->unAuditedRuleService->setUserGroup('string');
    }
    //userGroup 测试 ---------------------------------------------   end

    //applyUserGroup 测试 --------------------------------------------- start
    /**
     * 设置 News setApplyUserGroup() 正确的传参类型,期望传值正确
     */
    public function testSetApplyUserGroupCorrectType()
    {
        $userGroup = new UserGroup();
        $this->unAuditedRuleService->setApplyUserGroup($userGroup);
        $this->assertSame($userGroup, $this->unAuditedRuleService->getApplyUserGroup());
    }

    /**
     * 设置 News setApplyUserGroup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApplyUserGroupWrongType()
    {
        $this->unAuditedRuleService->setApplyUserGroup('string');
    }
    //applyUserGroup 测试 ---------------------------------------------   end
    
    //transformationTemplate 测试 --------------------------------------------- start
    /**
     * 设置 News setTransformationTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTransformationTemplateCorrectType()
    {
        $template = new Template();
        $this->unAuditedRuleService->setTransformationTemplate($template);
        $this->assertSame($template, $this->unAuditedRuleService->getTransformationTemplate());
    }

    /**
     * 设置 News setTransformationTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransformationTemplateWrongType()
    {
        $this->unAuditedRuleService->setTransformationTemplate('string');
    }
    //transformationTemplate 测试 ---------------------------------------------   end

    //sourceTemplate 测试 --------------------------------------------- start
    /**
     * 设置 News setSourceTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetSourceTemplateCorrectType()
    {
        $wbjTemplate = new WbjTemplate();
        $this->unAuditedRuleService->setSourceTemplate($wbjTemplate);
        $this->assertSame($wbjTemplate, $this->unAuditedRuleService->getSourceTemplate());
    }

    /**
     * 设置 News setSourceTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceTemplateWrongType()
    {
        $this->unAuditedRuleService->setSourceTemplate('string');
    }
    //sourceTemplate 测试 ---------------------------------------------   end

    public function testGenerateVersion()
    {
        //验证
        $this->unAuditedRuleService->generateVersion();
        $this->assertEquals($this->unAuditedRuleService->getVersion(), $this->unAuditedRuleService->getCreateTime());
    }
    
    public function testAdd()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['apply'])
                           ->getMock();


        $unAuditedRuleService->expects($this->exactly(1))
             ->method('apply')
             ->willReturn(true);
        //验证
        $result = $unAuditedRuleService->add();
        $this->assertTrue($result);
    }

    public function testEdit()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['apply'])
                           ->getMock();


        $unAuditedRuleService->expects($this->exactly(1))
             ->method('apply')
             ->willReturn(true);
        //验证
        $result = $unAuditedRuleService->edit();
        $this->assertTrue($result);
    }

    public function testApply()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['validate', 'getUnAuditedRuleServiceRepository'])
                           ->getMock();

        $unAuditedRuleService->expects($this->exactly(1))
             ->method('validate')
             ->willReturn(true);

        $repository = $this->prophesize(UnAuditedRuleServiceRepository::class);
        $repository->add(Argument::exact($unAuditedRuleService))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $unAuditedRuleService->expects($this->once())
             ->method('getUnAuditedRuleServiceRepository')
             ->willReturn($repository->reveal());


        //验证
        $result = $unAuditedRuleService->apply();
        $this->assertTrue($result);
    }

    public function testResubmitAction()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['setUpdateTime', 'getUnAuditedRuleServiceRepository', 'setStatusTime'])
                           ->getMock();

        //预言修改updateTime
        $unAuditedRuleService->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));
        $unAuditedRuleService->expects($this->exactly(1))
             ->method('setStatusTime')
             ->with(Core::$container->get('time'));

        //预言 INewsAdapter
        $repository = $this->prophesize(UnAuditedRuleServiceRepository::class);
        $repository->edit(
            Argument::exact($unAuditedRuleService),
            Argument::exact(
                [
                    'publishCrew',
                    'applyInfo',
                    'applyStatus',
                    'updateTime',
                    'statusTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $unAuditedRuleService->expects($this->once())
             ->method('getUnAuditedRuleServiceRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $unAuditedRuleService->resubmitAction();
        $this->assertTrue($result);
    }

    public function testApproveAction()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['updateApplyStatus', 'approveApplyInfo', 'updateRelationId'])
                           ->getMock();

        $unAuditedRuleService->expects($this->exactly(1))
             ->method('updateApplyStatus')
             ->with(IApproveAble::APPLY_STATUS['APPROVE'])
             ->willReturn(true);
        
        $unAuditedRuleService->expects($this->exactly(1))
             ->method('approveApplyInfo')
             ->with($unAuditedRuleService->getOperationType())
             ->willReturn(true);
        
        $unAuditedRuleService->expects($this->exactly(1))
             ->method('updateRelationId')
             ->willReturn(true);
        //验证
        $result = $unAuditedRuleService->approveAction();
        $this->assertTrue($result);
    }

    public function testRejectAction()
    {
        //初始化
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
                           ->setMethods(['updateApplyStatus'])
                           ->getMock();

        $unAuditedRuleService->expects($this->exactly(1))
             ->method('updateApplyStatus')
             ->with(IApproveAble::APPLY_STATUS['REJECT'])
             ->willReturn(true);
        
        //验证
        $result = $unAuditedRuleService->rejectAction();
        $this->assertTrue($result);
    }

    public function testUpdateApplyStatus()
    {
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
            ->setMethods(['getUnAuditedRuleServiceRepository', 'setUpdateTime', 'setStatusTime'])
            ->getMock();
            
        $applyStatus = IApproveAble::APPLY_STATUS['PENDING'];

        $unAuditedRuleService->setApplyStatus($applyStatus);
        $unAuditedRuleService->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));
        $unAuditedRuleService->expects($this->exactly(1))
             ->method('setStatusTime')
             ->with(Core::$container->get('time'));

        $repository = $this->prophesize(UnAuditedRuleServiceRepository::class);

        $repository->edit(
            Argument::exact($unAuditedRuleService),
            Argument::exact(array(
                'updateTime',
                'applyStatus',
                'statusTime',
                'applyCrew',
                'applyUserGroup',
                'rejectReason',
                'publishCrew',
                'crew'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditedRuleService->expects($this->any())
                   ->method('getUnAuditedRuleServiceRepository')
                   ->willReturn($repository->reveal());

        $result = $unAuditedRuleService->updateApplyStatus($applyStatus);
        
        $this->assertTrue($result);
    }

    public function testUpdateApplyStatusFail()
    {
        $applyStatus = IApproveAble::APPLY_STATUS['REJECT'];
        $this->unAuditedRuleService->setApplyStatus($applyStatus);

        $result = $this->unAuditedRuleService->updateApplyStatus($applyStatus);
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('applyStatus', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testRevokeSuccess()
    {
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
            ->setMethods(['updateApplyStatus'])
            ->getMock();
            
        $applyStatus = IApproveAble::APPLY_STATUS['PENDING'];

        $unAuditedRuleService->expects($this->exactly(1))
             ->method('updateApplyStatus')
             ->with(IApproveAble::APPLY_STATUS['REVOKE'])
             ->willReturn(true);

        $result = $unAuditedRuleService->revoke();
        
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $applyStatus = IApproveAble::APPLY_STATUS['REJECT'];
        $this->unAuditedRuleService->setApplyStatus($applyStatus);

        $result = $this->unAuditedRuleService->revoke();
        
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
        $this->assertEquals('applyStatus', Core::getLastError()->getSource()['pointer']);
        $this->assertFalse($result);
    }

    public function testUpdateRelationId()
    {
        $unAuditedRuleService = $this->getMockBuilder(MockUnAuditedRuleService::class)
            ->setMethods(['getUnAuditedRuleServiceRepository'])
            ->getMock();
        
        $unAuditedRuleService->setOperationType(IApproveAble::OPERATION_TYPE['ADD']);

        $repository = $this->prophesize(UnAuditedRuleServiceRepository::class);

        $repository->edit(
            Argument::exact($unAuditedRuleService),
            Argument::exact(array('relationId'))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $unAuditedRuleService->expects($this->any())
                   ->method('getUnAuditedRuleServiceRepository')
                   ->willReturn($repository->reveal());

        $result = $unAuditedRuleService->updateRelationId();
        
        $this->assertTrue($result);
    }

    public function testUpdateRelationIdEdit()
    {
        $this->unAuditedRuleService->setOperationType(IApproveAble::OPERATION_TYPE['EDIT']);

        $result = $this->unAuditedRuleService->updateRelationId();
        
        $this->assertTrue($result);
    }
}

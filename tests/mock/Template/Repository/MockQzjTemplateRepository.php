<?php
namespace BaseData\Template\Repository;

use BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;

class MockQzjTemplateRepository extends QzjTemplateRepository
{
    public function getAdapter() : IQzjTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IQzjTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IQzjTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}

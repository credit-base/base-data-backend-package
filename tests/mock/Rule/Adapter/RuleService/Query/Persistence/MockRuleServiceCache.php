<?php
namespace BaseData\Rule\Adapter\RuleService\Query\Persistence;

class MockRuleServiceCache extends RuleServiceCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData;

use BaseData\ResourceCatalogData\Model\GbItemsData;

interface IGbItemsDataAdapter
{
    public function fetchOne($id) : GbItemsData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(GbItemsData $gbItemsData) : bool;
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;

class MockYsSearchDataCommandHandlerTrait
{
    use YsSearchDataCommandHandlerTrait;

    public function publicGetRepository() : YsSearchDataRepository
    {
        return $this->getRepository();
    }

    public function publicFetchYsSearchData(int $id) : YsSearchData
    {
        return $this->fetchYsSearchData($id);
    }
}

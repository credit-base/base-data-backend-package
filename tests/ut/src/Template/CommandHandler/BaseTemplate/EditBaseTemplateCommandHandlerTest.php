<?php
namespace BaseData\Template\CommandHandler\BaseTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\BaseTemplate;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class EditBaseTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditBaseTemplateCommandHandler::class)
                                     ->setMethods(['getBaseTemplateRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetBaseTemplateRepository()
    {
        $commandHandler = new MockEditBaseTemplateCommandHandler();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\BaseTemplateRepository',
            $commandHandler->getBaseTemplateRepository()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $baseTemplate = $this->initBaseTemplate($command, true);

        $baseTemplateRepository = $this->prophesize(BaseTemplateRepository::class);
        $baseTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($baseTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getBaseTemplateRepository')
            ->willReturn($baseTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $baseTemplate = $this->initBaseTemplate($command, false);

        $baseTemplateRepository = $this->prophesize(BaseTemplateRepository::class);
        $baseTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($baseTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getBaseTemplateRepository')
            ->willReturn($baseTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new EditBaseTemplateCommand(
            $this->faker->randomElements(Template::SUBJECT_CATEGORY, 2),
            $this->faker->randomElements(
                array(
                    array(
                        "name" => '日期',    //信息项名称
                        "identify" => 'RQ',    //数据标识
                        "type" => Template::TYPE['RQX'],    //数据类型
                        "length" => '8',    //数据长度
                    )
                ),
                1
            ),
            $this->faker->randomElement(Template::DIMENSION),
            $this->faker->randomElement(Template::EXCHANGE_FREQUENCY),
            $this->faker->randomElement(Template::INFO_CLASSIFY),
            $this->faker->randomElement(Template::INFO_CATEGORY),
            $this->faker->randomDigit()
        );
        return $command;
    }

    private function initBaseTemplate(ICommand $command, bool $result)
    {
        $baseTemplate = $this->prophesize(BaseTemplate::class);
        $baseTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $baseTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $baseTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $baseTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $baseTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $items = $this->commandHandler->setDefaultInfo($command);
        $baseTemplate->setItems(Argument::exact($items))->shouldBeCalledTimes(1);
        $baseTemplate->edit()->shouldBeCalledTimes(1)->willReturn($result);

        return $baseTemplate;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Template\Model\WbjTemplate;

use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

class MockSearchData extends SearchData
{
    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->itemsData = new WbjItemsData();
        $this->repository = new WbjSearchDataRepository();
    }

    public function isCrewNull() : bool
    {
        return parent::isCrewNull();
    }

    public function isSourceUnitNull() : bool
    {
        return parent::isSourceUnitNull();
    }

    public function isTemplateNull() : bool
    {
        return parent::isTemplateNull();
    }

    public function isHashExist() : bool
    {
        return parent::isHashExist();
    }

    public function isSubjectCategoryExist() : bool
    {
        return parent::isSubjectCategoryExist();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function assignment()
    {
        return parent::assignment();
    }

    public function generateHash(array $itemsData = array())
    {
        return parent::generateHash($itemsData);
    }

    public function setStatus(int $status) : void
    {
        unset($status);
    }

    public function getItemsData()
    {
        return $this->itemsData;
    }

    public function getRepository()
    {
        return $this->repository;
    }
}

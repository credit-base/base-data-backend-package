<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\YsItemsData;
use BaseData\ResourceCatalogData\Model\NullYsItemsData;
use BaseData\ResourceCatalogData\Translator\YsItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\YsItemsDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class YsItemsDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(YsItemsDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'fetchOneAction',
                                'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IYsItemsDataAdapter
     */
    public function testImplementsIYsItemsDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 YsItemsDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockYsItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\YsItemsDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 YsItemsDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockYsItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\YsItemsDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockYsItemsDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedYsItemsData = new YsItemsData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedYsItemsData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedYsItemsData);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedYsItemsData = new YsItemsData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedYsItemsData);

        //验证
        $ysItemsData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedYsItemsData, $ysItemsData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $ysItemsDataOne = new YsItemsData(1);
        $ysItemsDataTwo = new YsItemsData(2);

        $ids = [1, 2];

        $expectedYsItemsDataList = [];
        $expectedYsItemsDataList[$ysItemsDataOne->getId()] = $ysItemsDataOne;
        $expectedYsItemsDataList[$ysItemsDataTwo->getId()] = $ysItemsDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedYsItemsDataList);

        //验证
        $ysItemsDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedYsItemsDataList, $ysItemsDataList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockYsItemsDataDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockYsItemsDataDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }
}

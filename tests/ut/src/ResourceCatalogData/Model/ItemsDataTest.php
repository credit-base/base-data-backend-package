<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ItemsDataTest extends TestCase
{
    private $itemsData;

    private $faker;

    public function setUp()
    {
        $this->itemsData = $this->getMockBuilder(ItemsData::class)
                            ->getMockForAbstractClass();
                            
        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->itemsData);
        unset($this->faker);
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IOperate',
            $this->itemsData
        );
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->itemsData
        );
    }

    public function testItemsDataConstructor()
    {
        $this->assertEquals(0, $this->itemsData->getId());
        $this->assertEquals(array(), $this->itemsData->getData());
    }

    public function testSetId()
    {
        $id = $this->faker->randomNumber();
        $this->itemsData->setId($id);
        $this->assertEquals($id, $this->itemsData->getId());
    }

    //data 测试 ---------------------------------------------------- start
    /**
     * 设置 ItemsData setData() 正确的传参类型,期望传值正确
     */
    public function testSetDataCorrectType()
    {
        $data = $this->faker->words(3, false);

        $this->itemsData->setData($data);
        $this->assertEquals($data, $this->itemsData->getData());
    }

    /**
     * 设置 ItemsData setData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataWrongType()
    {
        $this->itemsData->setData($this->faker->word());
    }
    //data 测试 ----------------------------------------------------   end

    //status 测试 ---------------------------------------------------- start
    /**
     * 设置 ItemsData setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $status = $this->faker->randomDigit();

        $this->itemsData->setStatus($status);
        $this->assertEquals($status, $this->itemsData->getStatus());
    }

    /**
     * 设置 ItemsData setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->itemsData->setStatus($this->faker->words(3, false));
    }
    //status 测试 ----------------------------------------------------   end

    public function testEdit()
    {
        $result = $this->itemsData->edit();
        $this->assertFalse($result);
    }
}

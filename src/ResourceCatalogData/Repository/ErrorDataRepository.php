<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;
use BaseData\ResourceCatalogData\Adapter\ErrorData\ErrorDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\ErrorData\ErrorDataMockAdapter;

class ErrorDataRepository extends Repository implements IErrorDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new ErrorDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IErrorDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IErrorDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IErrorDataAdapter
    {
        return new ErrorDataMockAdapter();
    }

    public function fetchOne($id) : ErrorData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(ErrorData $errorData) : bool
    {
        return $this->getAdapter()->add($errorData);
    }

    public function edit(ErrorData $errorData, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($errorData, $keys);
    }
}

<?php
namespace BaseData\Rule\Repository;

use BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter;

class MockRuleServiceRepository extends RuleServiceRepository
{
    public function getAdapter() : IRuleServiceAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IRuleServiceAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IRuleServiceAdapter
    {
        return parent::getMockAdapter();
    }
}

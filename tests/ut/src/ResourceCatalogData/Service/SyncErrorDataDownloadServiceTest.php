<?php
namespace BaseData\ResourceCatalogData\Service;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;

use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;

use BaseData\Rule\Repository\RepositoryFactory;

use BaseData\Template\Repository\WbjTemplateRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 */
class SyncErrorDataDownloadServiceTest extends TestCase
{
    private $stub;

    private $task;

    private $errorDataList;

    private $spreadsheet;

    private $sheet;

    public function setUp()
    {
        $this->stub = new MockSyncErrorDataDownloadService(new Task());

        $this->task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask(1);

        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
        $data = array('ZTMC'=>'主体名称');
        $errorData->getItemsData()->setData($data);

        $this->errorDataList = array($errorData);
        $this->spreadsheet = new Spreadsheet();
        $this->sheet = $this->spreadsheet->getActiveSheet();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->stub);
        unset($this->task);
        unset($this->errorDataList);
        unset($this->spreadsheet);
        unset($this->sheet);
    }

    public function testGetTask()
    {
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\Task',
            $this->stub->getTask()
        );
    }

    public function testGetErrorDataRepository()
    {
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Repository\ErrorDataRepository',
            $this->stub->getErrorDataRepository()
        );
    }

    public function testGetWriter()
    {
        $this->assertInstanceOf(
            'PhpOffice\PhpSpreadsheet\Writer\Xlsx',
            $this->stub->getWriter(new Spreadsheet())
        );
    }

    public function testGetSpreadsheet()
    {
        $this->assertInstanceOf(
            'PhpOffice\PhpSpreadsheet\Spreadsheet',
            $this->stub->getSpreadsheet()
        );
    }

    private function initialExecute($result)
    {
        $this->stub = $this->getMockBuilder(MockSyncErrorDataDownloadService::class)
                        ->setConstructorArgs(array(new Task()))
                        ->setMethods(['getTask', 'fetchErrorDataList', 'download', 'updateTaskStatus'])
                        ->getMock();

        $this->stub->expects($this->once())->method('getTask')->willReturn($this->task);
        $this->stub->expects($this->once())->method('fetchErrorDataList')->willReturn($this->errorDataList);
        $this->stub->expects($this->once())->method('download')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->once())->method('updateTaskStatus')->willReturn($result);
        }
    }

    public function testExecuteTrue()
    {
        $this->initialExecute(true);

        $result = $this->stub->execute();

        $this->assertTrue($result);
    }

    public function testExecuteFalse()
    {
        $this->initialExecute(false);

        $result = $this->stub->execute();

        $this->assertFalse($result);
    }

    public function testFetchErrorDataList()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownloadService::class)
                        ->setConstructorArgs(array(new Task()))
                        ->setMethods(['getErrorDataRepository'])
                        ->getMock();

        $count = 1;
        $filter['task'] = $this->task->getId();

        $repository = $this->prophesize(ErrorDataRepository::class);

        $repository->filter(
            Argument::exact($filter)
        )->shouldBeCalledTimes(1)->willReturn([$this->errorDataList, $count]);

        $stub->expects($this->once())->method('getErrorDataRepository')->willReturn($repository->reveal());

        $result = $stub->fetchErrorDataList($this->task);

        $this->assertEquals($this->errorDataList, $result);
    }

    public function testDownload()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownloadService::class)
                        ->setConstructorArgs(array(new Task()))
                        ->setMethods(['getSpreadsheet', 'fillWorksheet', 'getWriter', 'splicingFilePath'])
                        ->getMock();

        $filePath = Core::$container->get('attachment.download.path');

        $stub->expects($this->once())->method('getSpreadsheet')->willReturn($this->spreadsheet);
        $stub->expects($this->once())->method('fillWorksheet')->willReturn($this->spreadsheet);
        $stub->expects($this->once())->method('splicingFilePath')->willReturn($filePath);
        
        $writer = $this->prophesize(Writer::class);
        $writer->save(Argument::exact($filePath))->shouldBeCalledTimes(1)->willReturn(true);
        $stub->expects($this->once())->method('getWriter')->willReturn($writer->reveal());

        $result = $stub->download($this->errorDataList, $this->task);

        $this->assertTrue($result);
    }

    public function testSplicingFilePath()
    {
        $task = $this->task;
        $fileName = Task::ERROR_FILE_NAME_PREFIX.'_'.$task->getSourceCategory().
        '_'.$task->getTargetTemplate().'_'.$task->getTargetCategory().'_'.$task->getFileName();
        $filePath = Core::$container->get('attachment.download.path').$fileName;

        $result = $this->stub->splicingFilePath($task);

        $this->assertEquals($filePath, $result);
    }

    public function testFillWorksheet()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownloadService::class)
                        ->setConstructorArgs(array(new Task()))
                        ->setMethods(['fillWorksheetStyle', 'fillWorksheetHeader', 'fillWorksheetContent'])
                        ->getMock();

        $stub->expects($this->once())->method('fillWorksheetStyle');
        $stub->expects($this->once())->method('fillWorksheetHeader');
        $stub->expects($this->once())->method('fillWorksheetContent');
        
        $result = $stub->fillWorksheet($this->task, $this->errorDataList, $this->spreadsheet);

        $this->assertEquals($this->spreadsheet, $result);
    }
    
    public function testFillWorksheetStyle()
    {
        $numberFormat = $this->prophesize(NumberFormat::class);
        $numberFormat->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT
        )->shouldBeCalledTimes(1);

        $style = $this->prophesize(Style::class);
        $style->getNumberFormat()->shouldBeCalledTimes(1)->willReturn($numberFormat->reveal());

        $sheet = $this->prophesize(Worksheet::class);
        $sheet->getStyle('A:BZ')->shouldBeCalledTimes(1)->willReturn($style->reveal());
    
        $this->stub->fillWorksheetStyle($sheet->reveal());
    }

    public function testFillWorksheetHeader()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownloadService::class)
                        ->setConstructorArgs(array(new Task()))
                        ->setMethods(['fetchTargetTemplate'])
                        ->getMock();

        $targetTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $items = array(
            array('name'=>'主体名称')
        );
        $targetTemplate->setItems($items);

        $stub->expects($this->once())->method('fetchTargetTemplate')->willReturn($targetTemplate);

        $sheet = $this->prophesize(Worksheet::class);

        foreach ($items as $key => $item) {
            $sheet->setCellValue(
                SyncErrorDataDownloadService::EXCEL_LETTER[$key].SyncErrorDataDownloadService::EXCEL_HEADER,
                $item['name']
            )->shouldBeCalledTimes(1);
        }

        $count = count($items);
        $sheet->setCellValue(
            SyncErrorDataDownloadService::EXCEL_LETTER[$count].SyncErrorDataDownloadService::EXCEL_HEADER,
            SyncErrorDataDownloadService::EXCEL_HEADER_ERROR_REASON_CN_NAME
        )->shouldBeCalledTimes(1);

        $stub->fillWorksheetHeader($sheet->reveal(), $this->task);
    }

    public function testFillWorksheetContent()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownloadService::class)
                        ->setConstructorArgs(array(new Task()))
                        ->setMethods(['splicingErrorReason'])
                        ->getMock();

        $excelKey = SyncErrorDataDownloadService::EXCEL_CONTENT_ROWS_NUMBER;
        $errorReason = 'errorReason';

        $stub->expects($this->once())->method('splicingErrorReason')->willReturn($errorReason);

        $sheet = $this->prophesize(Worksheet::class);

        $sheet->setCellValueExplicit(
            SyncErrorDataDownloadService::EXCEL_LETTER[0].$excelKey,
            '主体名称',
            DataType::TYPE_STRING
        )->shouldBeCalledTimes(1);

        $sheet->setCellValue(
            SyncErrorDataDownloadService::EXCEL_LETTER[1].$excelKey,
            $errorReason
        )->shouldBeCalledTimes(1);

        $stub->fillWorksheetContent($sheet->reveal(), $this->errorDataList);
    }

    public function testSplicingErrorReason()
    {
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
        $errorReason = array(
            'ZTMC' => array(ErrorData::ERROR_TYPE['MISSING_DATA'])
        );

        $items = array(
            array('identify' => 'ZTMC', 'name'=>'主体名称')
        );

        $errorReasonResult = '主体名称'.ErrorData::ERROR_REASON[ErrorData::ERROR_TYPE['MISSING_DATA']];

        $errorData->setErrorReason($errorReason);
        $errorData->getTemplate()->setItems($items);

        $result = $this->stub->splicingErrorReason($errorData);

        $this->assertEquals($errorReasonResult, $result);
    }

    public function testFetchTargetTemplate()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownloadService::class)
                        ->setConstructorArgs(array(new Task()))
                        ->setMethods(['getRepositoryFactory'])
                        ->getMock();

        $targetTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);

        $factory = $this->prophesize(RepositoryFactory::class);
        $repository = $this->prophesize(WbjTemplateRepository::class);

        $repository->fetchOne(
            Argument::exact($this->task->getTargetTemplate())
        )->shouldBeCalledTimes(1)->willReturn($targetTemplate);

        $factory->getRepository(
            Argument::exact($this->task->getTargetCategory())
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $stub->expects($this->once())->method('getRepositoryFactory')->willReturn($factory->reveal());

        $result = $stub->fetchTargetTemplate($this->task);

        $this->assertEquals($targetTemplate, $result);
    }

    public function testUpdateTaskStatus()
    {
        $task = $this->prophesize(Task::class);

        $task->setStatus(Task::STATUS['FAILURE_FILE_DOWNLOAD'])->shouldBeCalledTimes(1);
        $task->setStatusTime(Core::$container->get('time'))->shouldBeCalledTimes(1);
        $task->edit()->shouldBeCalledTimes(1)->willReturn(true);

        $result = $this->stub->updateTaskStatus($task->reveal());
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Task\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\Model\NullNotifyRecord;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class NotifyRecordDbTranslator implements ITranslator
{
    
    public function arrayToObject(array $expression, $notifyRecord = null) : NotifyRecord
    {
        if (!isset($expression['record_id'])) {
            return NullNotifyRecord::getInstance();
        }

        if ($notifyRecord == null) {
            $notifyRecord = new NotifyRecord($expression['record_id']);
        }

        $notifyRecord->setId($expression['record_id']);

        if (isset($expression['name'])) {
            $notifyRecord->setName($expression['name']);
        }

        if (isset($expression['hash'])) {
            $notifyRecord->setHash($expression['hash']);
        }

        if (isset($expression['status'])) {
            $notifyRecord->setStatus($expression['status']);
        }

        $notifyRecord->setCreateTime($expression['create_time']);
        $notifyRecord->setUpdateTime($expression['update_time']);
        $notifyRecord->setStatusTime($expression['status_time']);

        return $notifyRecord;
    }

    public function objectToArray($notifyRecord, array $keys = array()) : array
    {
        if (!$notifyRecord instanceof NotifyRecord) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'hash',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['record_id'] = $notifyRecord->getId();
        }

        if (in_array('name', $keys)) {
            $expression['name'] = $notifyRecord->getName();
        }

        if (in_array('hash', $keys)) {
            $expression['hash'] = $notifyRecord->getHash();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $notifyRecord->getStatus();
        }

        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $notifyRecord->getCreateTime();
        }

        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $notifyRecord->getUpdateTime();
        }
        
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $notifyRecord->getStatusTime();
        }
        
        return $expression;
    }
}

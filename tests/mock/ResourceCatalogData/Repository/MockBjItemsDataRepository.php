<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter;

class MockBjItemsDataRepository extends BjItemsDataRepository
{
    public function getAdapter() : IBjItemsDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBjItemsDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBjItemsDataAdapter
    {
        return parent::getMockAdapter();
    }
}

<?php
namespace BaseData\Enterprise\Adapter\Enterprise;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Enterprise\Model\Enterprise;

class EnterpriseMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new EnterpriseMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new Enterprise()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new Enterprise(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\Enterprise\Model\Enterprise',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Enterprise\Model\Enterprise',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Enterprise\Model\Enterprise',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Repository;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Repository\CrewRepository;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\ModelFactory;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Translator\RuleServiceDbTranslator;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

class MockRuleServiceCommandHandlerTrait
{
    use RuleServiceCommandHandlerTrait;

    public function getCrewRepositoryPublic() : CrewRepository
    {
        return $this->getCrewRepository();
    }

    public function fetchCrewPublic(int $id) : Crew
    {
        return $this->fetchCrew($id);
    }

    public function getRuleServiceRepositoryPublic() : RuleServiceRepository
    {
        return $this->getRuleServiceRepository();
    }

    public function fetchRuleServicePublic(int $id) : RuleService
    {
        return $this->fetchRuleService($id);
    }

    public function getRuleServiceDbTranslatorPublic() : RuleServiceDbTranslator
    {
        return $this->getRuleServiceDbTranslator();
    }

    public function getUnAuditedRuleServiceRepositoryPublic() : UnAuditedRuleServiceRepository
    {
        return $this->getUnAuditedRuleServiceRepository();
    }

    public function fetchUnAuditedRuleServicePublic(int $id) : UnAuditedRuleService
    {
        return $this->fetchUnAuditedRuleService($id);
    }

    public function getUnAuditedRuleServicePublic() : UnAuditedRuleService
    {
        return $this->getUnAuditedRuleService();
    }

    public function getRepositoryFactoryPublic() : RepositoryFactory
    {
        return $this->getRepositoryFactory();
    }

    public function getRepositoryPublic(int $category) : Repository
    {
        return $this->getRepository($category);
    }

    public function getModelFactoryPublic() : ModelFactory
    {
        return $this->getModelFactory();
    }

    public function applyInfoPublic(UnAuditedRuleService $unAuditedRuleService) : array
    {
        return $this->applyInfo($unAuditedRuleService);
    }

    public function generateUnAuditedRuleServicePublic(
        ICommand $command,
        UnAuditedRuleService $unAuditedRuleService
    ) : UnAuditedRuleService {
        return $this->generateUnAuditedRuleService($command, $unAuditedRuleService);
    }

    public function generateUnAuditedRuleServiceCrewPublic(
        int $crewId,
        UnAuditedRuleService $unAuditedRuleService
    ) : UnAuditedRuleService {
        return $this->generateUnAuditedRuleServiceCrew($crewId, $unAuditedRuleService);
    }

    public function generateUnAuditedRuleServiceRulesPublic(
        array $commandRules,
        UnAuditedRuleService $unAuditedRuleService
    ) : UnAuditedRuleService {
        return $this->generateUnAuditedRuleServiceRules($commandRules, $unAuditedRuleService);
    }
}

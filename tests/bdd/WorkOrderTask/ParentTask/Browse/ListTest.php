<?php
namespace BaseData\WorkOrderTask\ParentTask\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator;

use BaseData\Template\SetDataTrait as TemplateSetDataTrait;

/**
 * @Feature: 我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务,
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 查看工单任务列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, TemplateSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_parent_task');
    }

    /**
     * @Given: 存在工单任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_parent_task' => $this->parentTask()
        ]);
    }

    /**
     * @When: 当我查看工单任务列表时
     */
    public function fetchParentTaskList()
    {
        $repository = new ParentTaskRepository();

        list($parentTaskList, $count) = $repository->filter([]);
        
        unset($count);

        return $parentTaskList;
    }

    /**
     * @Then: 我可以看到工单任务的全部信息
     */
    public function testViewParentTaskList()
    {
        $setParentTaskList = $this->parentTask();

        foreach ($setParentTaskList as $key => $setParentTask) {
            $setParentTaskList[$key]['parent_task_id'] = $key +1;
            $setParentTaskList[$key]['attachment'] = json_decode($setParentTask['attachment'], true);
            $setParentTaskList[$key]['assign_objects'] = json_decode($setParentTask['assign_objects'], true);
        }

        $parentTaskList = $this->fetchParentTaskList();

        $translator = new ParentTaskDbTranslator();
        foreach ($parentTaskList as $parentTask) {
            $parentTaskArray[] = $translator->objectToArray($parentTask);
        }

        $this->assertEquals($parentTaskArray, $setParentTaskList);
    }
}

<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\Model\IProgress;
use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\WorkOrderTask\Model\WorkOrderTask;

/**
 * 测试工单任务接口反馈
 */
class FeedbackTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_work_order_task');
    }

    /**
     * 存在一条工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_parent_task' => $this->parentTask(),
                'pcore_work_order_task' => $this->workOrderTask(IProgress::STATUS['GJZ']),
            ]
        );
    }

    public function testViewWorkOrderTask()
    {
        $workOrderTask = $this->workOrderTask()[0];
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $data = array(
            "data" => array(
                "type" => "workOrderTasks",
                "attributes" => array(
                    "feedbackRecords" => array(  //反馈记录
                        array(
                            'crew' => 1,  //反馈人
                            'userGroup' => 1,  //反馈委办局
                            'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                            'templateId' => 0,  //目录id
                            'items' => array(
                                [
                                    "name" => "主体名称",
                                    "identify" => "ZTMC",
                                    "type" => 1,
                                    "options" => [],
                                    "length" => 200,
                                    "isMasked" => 0,
                                    "maskRule" => [],
                                    "dimension" => 1,
                                    "isNecessary" => 1,
                                    "remarks" => "主体名称"
                                ],
                                [
                                    "name" => "统一社会信用代码",
                                    "identify" => "TYSHXYDM",
                                    "type" => 1,
                                    "length" => 20,
                                    "options" => [],
                                    "isMasked" => 0,
                                    "maskRule" => [],
                                    "dimension" => 1,
                                    "isNecessary" => 1,
                                    "remarks" => "统一社会信用代码"
                                ],
                            ),  //信息项
                            'reason' => '测试反馈信息',  //反馈原因
                        )
                    )
                )
            )
        );

        $response = $client->request(
            'PATCH',
            'workOrderTasks/2/feedback',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $feedbackRecords = json_decode($workOrderTask['feedback_records'], true);

        $feedbackRecords[0]['crewName'] = $attributes['feedbackRecords'][0]['crewName'];
        $feedbackRecords[0]['userGroupName'] = $attributes['feedbackRecords'][0]['userGroupName'];

        $this->assertEquals($feedbackRecords, $attributes['feedbackRecords']);
        $this->assertEquals('测试反馈信息', $attributes['reason']);
        $this->assertEquals(IProgress::STATUS['GJZ'], $attributes['status']);
    }
}

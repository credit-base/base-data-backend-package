<?php
namespace BaseData\Enterprise\Enterprise\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Enterprise\SetDataTrait;
use BaseData\Enterprise\Repository\EnterpriseRepository;
use BaseData\Enterprise\Translator\EnterpriseDbTranslator;

 /**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件企业数据时,在政务网OA中,
 *           可以搜索我需要的所有企业数据,通过列表形式查看我搜索的所有企业数据 ,以便于我可以快速查看到我需要的企业数据
 * @Scenario: 通过法定代表人搜索
 */
class PrincipalTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_enterprise');
    }

    /**
     * @Given: 存在企业数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_enterprise' => $this->enterprise()
        ]);
    }
    /**
     * @When: 当我查看企业数据列表时
     */
    public function fetchEnterpriseList()
    {
        $repository = new EnterpriseRepository();
 
        $filter['principal'] = '张萍';

        list($enterpriseList, $count) = $repository->filter($filter);
        
        unset($count);

        return $enterpriseList;
    }

    /**
     * @Then: 我可以看到法定代表人为"张萍"的企业数据
     */
    public function testViewEnterpriseList()
    {
        $setEnterpriseList = $this->enterprise();

        foreach ($setEnterpriseList as $key => $setEnterprise) {
            unset($setEnterprise);
            $setEnterpriseList[$key]['enterprise_id'] = $key +1;
        }

        $enterpriseList = $this->fetchEnterpriseList();
        $translator = new EnterpriseDbTranslator();
        foreach ($enterpriseList as $enterprise) {
            $enterpriseArray[] = $translator->objectToArray($enterprise);
        }

        $this->assertEquals($enterpriseArray, $setEnterpriseList);

        foreach ($enterpriseArray as $enterprise) {
            $this->assertEquals('张萍', $enterprise['principal']);
        }
    }
}

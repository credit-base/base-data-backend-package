<?php
namespace BaseData\Task\Controller;

use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;
use Marmot\Interfaces\IView;

class MockNotifyRecordFetchController extends NotifyRecordFetchController
{
    public function getRepository() : INotifyRecordAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

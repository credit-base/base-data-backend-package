<?php
namespace BaseData\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Rule\WidgetRule\RuleServiceWidgetRule;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

class MockRuleServiceControllerTrait extends Controller
{
    use RuleServiceControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getRuleServiceWidgetRulePublic() : RuleServiceWidgetRule
    {
        return $this->getRuleServiceWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : RuleServiceRepository
    {
        return $this->getRepository();
    }

    public function getUnAuditedRuleServiceRepositoryPublic() : UnAuditedRuleServiceRepository
    {
        return $this->getUnAuditedRuleServiceRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateCommonScenarioPublic($rules, $crew)
    {
        return $this->validateCommonScenario($rules, $crew);
    }
    
    public function validateRejectScenarioPublic(
        $rejectReason,
        $applyCrew
    ) {
        return $this->validateRejectScenario(
            $rejectReason,
            $applyCrew
        );
    }

    public function validateApproveScenarioPublic($applyCrew)
    {
        return $this->validateApproveScenario($applyCrew);
    }

    public function validateStatusScenarioPublic($status)
    {
        return $this->validateStatusScenario($status);
    }
    
    public function validateAddScenarioPublic(
        $transformationTemplate,
        $sourceTemplate,
        $transformationCategory,
        $sourceCategory
    ) {
        return $this->validateAddScenario(
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory
        );
    }
}

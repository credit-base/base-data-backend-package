<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class WbjSearchDataDb extends Db
{
    const TABLE = 'wbj_search_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

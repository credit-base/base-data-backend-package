<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Common\Model\IEnableAble;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要禁用资源目录数据时,在资源目录数据子系统下的委办局数据管理,禁用对应的资源目录数据
 *           根据我核实需要禁用的数据禁用,以便于我实现平台资源目录数据的呈现
 * @Scenario: 禁用资源目录数据
 */
class DisableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_items_data');
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * @Given: 存在需要禁用的资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data'],
            'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data']
        ]);
    }

    /**
     * @When: 获取需要禁用的资源目录数据
     */
    public function fetchWbjSearchData($id)
    {
        $repository = new WbjSearchDataRepository();

        $wbjSearchData = $repository->fetchOne($id);

        return $wbjSearchData;
    }

    /**
     * @And: 当我调用禁用函数,期待返回true
     */
    public function disable()
    {
        $wbjSearchData = $this->fetchWbjSearchData(1);
        
        return $wbjSearchData->disable();
    }

    /**
     * @Then: 数据已经被禁用
     */
    public function testValidate()
    {
        $result = $this->disable();

        $this->assertTrue($result);

        $wbjSearchData = $this->fetchWbjSearchData(1);

        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $wbjSearchData->getStatus());
        $this->assertEquals(Core::$container->get('time'), $wbjSearchData->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $wbjSearchData->getStatusTime());
    }
}

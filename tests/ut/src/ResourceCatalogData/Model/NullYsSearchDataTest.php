<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullYsSearchDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullYsSearchData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsYsSearchData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\YsSearchData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;
use BaseData\ResourceCatalogData\Model\NullWbjSearchData;
use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\View\WbjSearchDataView;

class WbjSearchDataFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WbjSearchDataFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockWbjSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockWbjSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\View\WbjSearchDataView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockWbjSearchDataFetchController();

        $this->assertEquals(
            'wbjSearchData',
            $controller->getResourceName()
        );
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use BaseData\Common\Model\IEnableAble;

class MockDisableYsSearchDataCommandHandler extends DisableYsSearchDataCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}

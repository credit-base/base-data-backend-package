<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Command\RuleService\ResubmitRuleServiceCommand;

use BaseData\Crew\Model\Crew;

class ResubmitRuleServiceCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new ResubmitRuleServiceCommandHandler();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        //初始化
        $id = 1;
        $crew = 2;
        $rules = ['rules'];
        $applyInfo = ['applyInfo'];

        $command = new ResubmitRuleServiceCommand(
            $rules,
            $crew,
            $id
        );

        $crew = new Crew();

        $commandHandler = $this->getMockBuilder(ResubmitRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         [
                                            'fetchUnAuditedRuleService',
                                            'generateUnAuditedRuleServiceCrew',
                                            'generateUnAuditedRuleServiceRules',
                                            'applyInfo'
                                         ]
                                     )
                                     ->getMock();

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->setApplyInfo(Argument::exact($applyInfo))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->resubmit()
                             ->shouldBeCalledTimes(1)
                             ->willReturn(true);

        $commandHandler->expects($this->once())
                       ->method('fetchUnAuditedRuleService')
                       ->with($command->id)
                       ->willReturn($unAuditedRuleService->reveal());

        $commandHandler->expects($this->once())
                       ->method('applyInfo')
                       ->with(
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($applyInfo);

        $commandHandler->expects($this->once())
                       ->method('generateUnAuditedRuleServiceCrew')
                       ->with(
                           $command->crew,
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($unAuditedRuleService->reveal());

        $commandHandler->expects($this->once())
                       ->method('generateUnAuditedRuleServiceRules')
                       ->with(
                           $command->rules,
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($unAuditedRuleService->reveal());

        $result = $commandHandler->execute($command);
        $this->assertTrue($result);
    }
}

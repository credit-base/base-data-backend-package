<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockEndWorkOrderTaskCommandHandler extends EndWorkOrderTaskCommandHandler
{
    public function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return parent::getWorkOrderTaskRepository();
    }
}

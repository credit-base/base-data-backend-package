<?php
namespace BaseData\ResourceCatalogData\ErrorData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试错误资源目录接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_error_data');
    }

    /**
     * 存在多条错误资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_task' => $this->task(),
                'pcore_error_data' => $this->errorData(),
                'pcore_bj_template' => $this->bjTemplate()
            ]
        );
    }

    public function testViewErrorData()
    {
        $setErrorDataList = $this->errorData();
        $setTaskList = $this->task();
        $setTemplateList = $this->bjTemplate();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);

        $response = $client->request(
            'GET',
            'errorDatas?filter[task]=1&filter[template]=1&filter[category]='.Template::CATEGORY['BJ'].
            '&include=task,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $errorData) {
            $this->errorDataCompareData($setErrorDataList[$key], $errorData);
        }

        $this->taskCompareData($setTaskList[0], $included[0]);
        $this->bjTemplateCompareData($setTemplateList[0], $included[1]);
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;

class BjSearchDataCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockBjSearchDataCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\BjSearchDataRepository',
            $this->trait->publicGetRepository()
        );
    }

    public function testFetchBjSearchData()
    {
        $trait = $this->getMockBuilder(
            MockBjSearchDataCommandHandlerTrait::class
        )->setMethods(['getRepository'])->getMock();

        $id = 1;
        $bjSearchData = \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData($id);

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($bjSearchData);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchBjSearchData($id);

        $this->assertEquals($result, $bjSearchData);
    }
}


CREATE TABLE `pcore_base_template` (
  `base_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='基础资源目录表';

ALTER TABLE `pcore_base_template`
  ADD PRIMARY KEY (`base_template_id`),
  ADD UNIQUE KEY `identify` (`identify`);

ALTER TABLE `pcore_base_template`
  MODIFY `base_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

INSERT INTO `pcore_base_template` (`base_template_id`, `name`, `identify`, `subject_category`, `dimension`, `exchange_frequency`, `info_classify`, `info_category`, `description`, `items`, `status`, `create_time`, `update_time`, `status_time`) VALUES
(1, '企业基本信息', 'QYJBXX', '["1", "3"]', 1, 4, 5, 1, '企业基本信息', '[
  {
      "name": "主体名称",
      "type": "1",
      "length": "200",
      "options": [],
      "remarks": "",
      "identify": "ZTMC",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "统一社会信用代码",
      "type": "1",
      "length": "18",
      "options": [],
      "remarks": "",
      "identify": "TYSHXYDM",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "成立日期",
      "type": "2",
      "length": "8",
      "options": [],
      "remarks": "",
      "identify": "CLRQ",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "核准日期",
      "type": "2",
      "length": "8",
      "options": [],
      "remarks": "",
      "identify": "HZRQ",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "经营场所",
      "type": "1",
      "length": "255",
      "options": [],
      "remarks": "",
      "identify": "JYCS",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "注册资本（万）",
      "type": "1",
      "length": "255",
      "options": [],
      "remarks": "",
      "identify": "ZCZB",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "营业期限自",
      "type": "2",
      "length": "8",
      "options": [],
      "remarks": "",
      "identify": "YYQXQ",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "营业期限至",
      "type": "2",
      "length": "8",
      "options": [],
      "remarks": "",
      "identify": "YYQXZ",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "经营范围",
      "type": "1",
      "length": "2000",
      "options": [],
      "remarks": "",
      "identify": "JYFW",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "登记机关",
      "type": "1",
      "length": "255",
      "options": [],
      "remarks": "",
      "identify": "DJJG",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "法定代表人",
      "type": "1",
      "length": "255",
      "options": [],
      "remarks": "",
      "identify": "FDDBR",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "法人身份证号",
      "type": "1",
      "length": "200",
      "options": [],
      "remarks": "",
      "identify": "FRZJHM",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "登记状态",
      "type": "1",
      "length": "200",
      "options": [],
      "remarks": "",
      "identify": "DJZT",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "企业类型",
      "type": "1",
      "length": "200",
      "options": [],
      "remarks": "",
      "identify": "QYLX",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "企业类型代码",
      "type": "1",
      "length": "200",
      "options": [],
      "remarks": "",
      "identify": "QYLXDM",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "行业门类",
      "type": "1",
      "length": "4",
      "options": [],
      "remarks": "",
      "identify": "HYML",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "行业代码",
      "type": "1",
      "length": "200",
      "options": [],
      "remarks": "",
      "identify": "HYDM",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "所属区域",
      "type": "1",
      "length": "50",
      "options": [],
      "remarks": "",
      "identify": "SSQY",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "1",
      "isNecessary": "1"
  },
  {
      "name": "主体类别",
      "type": "6",
      "length": "50",
      "options" : [
          "法人及非法人组织",
          "个体工商户"
      ],
      "remarks": "法人及非法人组织;自然人;个体工商户，支持多选",
      "identify": "ZTLB",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "2",
      "isNecessary": "1"
  },
  {
      "name": "公开范围",
      "type": "5",
      "length": "20",
      "options": [
          "社会公开"
      ],
      "remarks": "支持单选",
      "identify": "GKFW",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "2",
      "isNecessary": "1"
  },
  {
      "name": "更新频率",
      "type": "5",
      "length": "20",
      "options": [
          "每月"
      ],
      "remarks": "支持单选",
      "identify": "GXPL",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "2",
      "isNecessary": "1"
  },
  {
      "name": "信息分类",
      "type": "5",
      "length": "50",
      "options": [
          "其他"
      ],
      "remarks": "支持单选",
      "identify": "XXFL",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "2",
      "isNecessary": "1"
  },
  {
      "name": "信息类别",
      "type": "5",
      "length": "50",
      "options": [
          "基础信息"
      ],
      "remarks": "信息性质类型，支持单选",
      "identify": "XXLB",
      "isMasked": "0",
      "maskRule": [],
      "dimension": "2",
      "isNecessary": "1"
  }
]', 0, 1626504022, 1629172736, 0);

ALTER TABLE `pcore_enterprise` ADD COLUMN `hash` char(32) NOT NULL COMMENT '摘要hash';

ALTER TABLE `pcore_enterprise` ADD COLUMN `task_id` int(10) NOT NULL COMMENT '任务id';

ALTER  TABLE  `pcore_enterprise`  ADD  UNIQUE (`name` );

ALTER  TABLE  `pcore_enterprise`  ADD  UNIQUE (`unified_social_credit_code` );
<?php
namespace BaseData\Rule\RuleService\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Translator\RuleServiceDbTranslator;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限，当我想要查看的某个资源目录的规则详情时,
 *           在政务网OA中的资源目录规则管理模块中的资源目录规则列表正式表中,我可以查看所有通过审核的规则,通过点击资源目录名称或是查看,以便于了解该资源目录规则的相关信息
 * @Scenario: 查看规则数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_wbj_template');
    }

    /**
     * @Given: 存在一条规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_rule' => $this->rule(),
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_wbj_template' => $this->wbjTemplate()
        ]);
    }

    /**
     * @When: 当我查看该条规则数据详情时
     */
    public function fetchRule($id)
    {
        $repository = new RuleServiceRepository();

        $rule = $repository->fetchOne($id);

        return $rule;
    }

    /**
     * @Then: 我可以看见该条规则数据的全部信息
     */
    public function testViewRuleList()
    {
        $id = 1;
        
        $setRule = $this->rule()[0];
        $setRule['rule_id'] = $id;
        $rule = $this->fetchRule($id);

        $translator = new RuleServiceDbTranslator();
        $ruleArray = $translator->objectToArray($rule);

        $this->assertEquals($ruleArray, $setRule);
    }
}

<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\CommandHandler\GbTemplate\GbTemplateCommandHandlerFactory;

trait GbTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : GbTemplateRepository
    {
        return new GbTemplateRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new GbTemplateCommandHandlerFactory());
    }
}

<?php
namespace BaseData\Rule\RuleService\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\RuleUtils;
use BaseData\Rule\SetDataApiTrait;

use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

use BaseData\Common\Model\IApproveAble;

/**
 * 测试规则接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, RuleUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_rule');
    }

    /**
     * 不存在规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_rule' => $this->rule(),
            ]
        );
    }

    public function testViewRuleService()
    {
        $setRuleService = $this->applyFormRule(
            IApproveAble::APPLY_STATUS['PENDING'],
            1,
            IApproveAble::OPERATION_TYPE['EDIT']
        )[0];

        $data = array(
            'data' => array(
                "type"=>"rules",
                "attributes"=>array(
                    "rules"=>array(
                        'transformationRule' => array(
                            "ZTMC" => "ZTMC",
                            "ZTLB" => "ZTLB",
                            "GKFW" => "GKFW",
                            "GXPL" => "GXPL",
                            "XXFL" => "XXFL",
                            "XXLB" => "XXLB",
                            "TYSHXYDM" => "TYSHXYDM",
                        ),
                    )
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'rules/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedRuleCompareData($setRuleService, $contents['data']);
    }
}

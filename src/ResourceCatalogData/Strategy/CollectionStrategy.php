<?php
namespace BaseData\ResourceCatalogData\Strategy;

use Marmot\Core;

use Respect\Validation\Validator as V;

class CollectionStrategy implements IDataStrategy
{
    public function validate($value, $rule = array()) : bool
    {
        if (!is_array($value)) {
            $value = explode(',', $value);
        }
        
        foreach ($value as $each) {
            if (!V::stringType()->validate($each) || !in_array($each, $rule['options'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'data'));
                return false;
            }
        }

        return true;
    }

    public function mock($rule = array())
    {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(mt_rand());

        return $faker->randomElements($rule['options'], 1);
    }
}

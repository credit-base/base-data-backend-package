<?php
namespace BaseData\Template\GbTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\GbTemplate;

/**
 * 测试国标资源目录接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在多条国标资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate()
            ]
        );
    }

    public function testViewGbTemplate()
    {
        $setGbTemplateList = $this->gbTemplate();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'gbTemplates/1,2',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $gbTemplate) {
            $this->gbTemplateCompareData($setGbTemplateList[$key], $gbTemplate);
        }
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;

trait BjSearchDataCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new BjSearchDataRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : BjSearchDataRepository
    {
        return $this->repository;
    }

    protected function fetchBjSearchData(int $id) : BjSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}

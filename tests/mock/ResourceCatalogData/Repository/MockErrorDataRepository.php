<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class MockErrorDataRepository extends ErrorDataRepository
{
    public function getAdapter() : IErrorDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IErrorDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IErrorDataAdapter
    {
        return parent::getMockAdapter();
    }
}

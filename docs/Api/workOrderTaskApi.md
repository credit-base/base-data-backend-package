# 工单任务接口文档

---

## 简介

本文档主要描述工单任务模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [控件规范](#控件规范)
    * [错误规范](#错误规范)
    * [接口错误返回说明](#接口错误返回说明) 
    * [mock接口请求说明](#mock接口请求说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [根据检索条件获取父任务](#根据检索条件获取父任务)
    * [指派父任务](#指派父任务)
    * [撤销父任务](#撤销父任务)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [撤销](#撤销)
    * [确认](#确认)
    * [终结](#终结)
    * [反馈](#反馈)
    * [接口返回示例](#接口返回示例)
        * [父任务接口示例](#父任务接口示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [通用项目字典](./docs/Dictionary/common.md "通用项目字典")
    * [工单任务项目字典](./docs/Dictionary/workOrderTask.md "工单任务项目字典")
* <a name="控件规范">控件规范</a>
* <a name="错误规范">错误规范</a>
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 
* <a name="mock接口请求说明">mock接口请求说明</a>
    * [mock接口请求说明](./docs/Api/mockApi.md "mock接口请求说明") 

## <a name="参数说明">参数说明</a>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=template
2、fields[TYPE]请求参数
    2.1 fields[parentTasks]
    2.2 fields[gbTemplates]  //仅支持name字段数据
    2.3 fields[bjTemplates]  //仅支持name字段数据
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request(
    'GET', 
    'parentTasks?include=template&fields[gbTemplates]=name&fields[bjTemplates]=name',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="根据检索条件获取父任务">根据检索条件获取父任务示例</a>

路由

```
通过GET传参
/parentTasks

1、检索条件
    1.1 filter[title] | 根据 任务标题 搜索
2、排序
    2.1 sort=-endTime | -endTime 根据终结时间倒序 | endTime 根据终结时间正序
```

示例

```php
$response = $client->request(
    'GET', 
    'parentTasks?filter[title]=任务标题&sort=-endTime',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="指派父任务">指派父任务示例</a>

路由

```
通过POST传参
/parentTasks
```

示例

```php
$data = array(
    "data" => array(
        "type" => "parentTasks",
        "attributes" => array(
            "templateType" => 1,    //基础目录，国标目录 1 | 本级目录 2
            "title" => '归集地方性红名单信息',    //任务标题
            "description" => '依据国家要求，现需要按要求归集地方性红名单信息',    //任务描述
            "endTime" => '2020-01-01',    //终结时间
            "attachment" => array(  //依据附件
                'name' => '国203号文', 
                'identify' => 'b900638af896a26de13bc89ce4f64124.pdf'
            )    
        ),
        "relationships" => array(
            "template" => array( // 指派目录，根据所选择基础目录，在以下示例二选一
                "data" => array(
                    array("type" => "gbTemplate", "id" => 1), //国标目录
                    // array("type" => "bjTemplate", "id" => 1)  //本级目录
                )
            ),
            "assignObjects" => array( // 指派对象
                "data" => array(
                    array("type" => "userGroups", "id" => 1),
                    array("type" => "userGroups", "id" => 2),
                    array("type" => "userGroups", "id" => 3)
                )
            )
        )
    )
);
$response = $client->request(
    'POST',
    'parentTasks',
    [
        'headers'=>['Content-Type' => 'application/vnd.api+json'],
        'json' => $data
    ]
);
```

### <a name="撤销父任务">撤销父任务示例</a>

路由

```
通过PATCH传参
/parentTasks/{id:\d+}/revoke
```

示例

```php
$data = array("data"=>array(
                    "type"=>"parentTasks",
                    "attributes"=>array(
                        "reason"=>'撤销原因'
                    )
                )
        );
$response = $client->request(
                'PATCH',
                'parentTasks/1/revoke',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

---

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=template
    1.2 include=assignObject
    1.3 include=parentTask
2、fields[TYPE]请求参数
    2.1 fields[workOrderTasks]
    2.2 fields[gbTemplates] //获取多条数据时，仅支持name字段数据
    2.3 fields[bjTemplates] //获取多条数据时，仅支持name字段数据
    2.4 fields[userGroups]  //仅支持name字段数据
    2.5 fields[parentTasks] //获取多条数据时，仅支持templateType、title、endTime字段数据
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request(
    'GET', 
    'workOrderTasks/1?fields[workOrderTasks]=reason,status',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/workOrderTasks/{id:\d+}
```

示例

```php
$response = $client->request(
    'GET', 
    'workOrderTasks/1',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/workOrderTasks/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request(
    'GET', 
    'workOrderTasks/1,2,3',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/workOrderTasks

1、检索条件
    1.1 filter[title] | 根据 任务标题 搜索
    1.2 filter[assignObject] | 根据 指派对象 搜索 | 委办局id
    1.3 filter[status] | 根据 状态 搜索，支持多条件搜索
    1.4 filter[parentTask] | 根据 父任务id 搜索
2、排序
    2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
    2.2 sort=-endTime | -endTime 根据终结时间倒序 | endTime 根据终结时间正序
```

示例

```php
$response = $client->request(
    'GET', 
    'workOrderTasks?filter[status]=1,2&sort=-updateTime',
    [
        'headers'=>['Content-' => 'application/vnd.api+json']
    ]
);
```

### <a name="撤销">撤销示例</a>

路由

```
通过PATCH传参
/workOrderTasks/{id:\d+}/revoke
```

示例

```php
$data = array("data"=>array(
                    "type"=>"workOrderTasks",
                    "attributes"=>array(
                        "reason"=>'撤销原因'
                    )
                )
        );
$response = $client->request(
                'PATCH',
                'workOrderTasks/1/revoke',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="确认">确认示例</a>

路由

```
通过PATCH传参
/workOrderTasks/{id:\d+}/confirm
```

示例

```php
$response = $client->request(
                'PATCH',
                'workOrderTasks/1/confirm',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                ]
            );
```

### <a name="终结">终结示例</a>

路由

```
通过PATCH传参
/workOrderTasks/{id:\d+}/end
```

示例

```php
$data = array("data"=>array(
                    "type"=>"workOrderTasks",
                    "attributes"=>array(
                        "reason"=>'终结原因'
                    )
                )
        );
$response = $client->request(
                'PATCH',
                'workOrderTasks/1/end',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="反馈">反馈示例</a>

路由

```
通过PATCH传参
/workOrderTasks/{id:\d+}/feedback
```

示例

```php
$data = array(
    "data" => array(
        "type" => "workOrderTasks",
        "attributes" => array(
            "feedbackRecords" => array(  //反馈记录
                array(
                    'crew' => 1,  //反馈人
                    'userGroup' => 1,  //反馈委办局
                    'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                    'templateId' => '',  //目录id
                    'items' => array(
                        array(
                            "name" => '主体名称',    //信息项名称
                            "identify" => 'ZTMC',    //数据标识
                            "type" => 1,    //数据类型
                            "length" => '200',    //数据长度
                            "options" => array(),    //可选范围
                            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                            "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                            "maskRule" => array(),    //脱敏规则
                            "remarks" => '信用主体名称',    //备注
                        ),
                        array(
                            "name" => '统一社会信用代码',    //信息项名称
                            "identify" => 'TYSHXYDM',    //数据标识
                            "type" => 1,    //数据类型
                            "length" => '50',    //数据长度
                            "options" => array(),    //可选范围
                            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                            "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                            "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                            "remarks" => '信用主体代码',    //备注
                        )
                    ),  //信息项
                    'reason' => '反馈原因',  //反馈原因
                )
            )    
        )
    )
);
$response = $client->request(
                'PATCH',
                'workOrderTasks/1/feedback',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="父任务接口示例">父任务接口示例</a>

```json
{
    "meta": {
        "count": 2,
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        }
    },
    "links": {
        "first": null,
        "last": null,
        "prev": null,
        "next": null
    },
    "data": [
        {
            "type": "parentTasks",
            "id": "1",
            "attributes": {
                "templateType": 1,
                "title": "归集地方性红名单信息",
                "description": "依据国家要求，现需要按要求归集地方性红名单信息",
                "endTime": "2020-01-01",
                "attachment": {
                    "name": "国203号文",
                    "identify": "b900638af896a26de13bc89ce4f64124.pdf"
                },
                "ratio": 100,
                "reason": "撤销原因",
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "relationships": {
                "template": {
                    "data": {
                        "type": "gbTemplates",
                        "id": "1"
                    }
                },
                "assignObjects": {
                    "data": [
                        {
                            "type": "userGroups",
                            "id": "1"
                        },
                        {
                            "type": "userGroups",
                            "id": "2"
                        },
                        {
                            "type": "userGroups",
                            "id": "3"
                        }
                    ]
                }
            },
            "links": {
                "self": "127.0.0.1:8080/parentTasks/1"
            }
        },
        {
            "type": "parentTasks",
            "id": "2",
            "attributes": {
                "templateType": 2,
                "title": "归集地方性黑名单信息",
                "description": "依据省平台要求，现需要按要求归集地方性黑名单信息",
                "endTime": "2020-01-01",
                "attachment": {
                    "name": "省203号文",
                    "identify": "b900638af896a26de13bc89ce4f64127.pdf"
                },
                "ratio": 50,
                "reason": "",
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "relationships": {
                "template": {
                    "data": {
                        "type": "bjTemplates",
                        "id": "2"
                    }
                },
                "assignObjects": {
                    "data": [
                        {
                            "type": "userGroups",
                            "id": "1"
                        },
                        {
                            "type": "userGroups",
                            "id": "2"
                        },
                        {
                            "type": "userGroups",
                            "id": "3"
                        }
                    ]
                }
            },
            "links": {
                "self": "127.0.0.1:8080/parentTasks/2"
            }
        }
    ],
    "included": [
        {
            "type": "gbTemplates",
            "id": "1",
            "attributes": {
                "name": "地方性红名单"
            }
        },
        {
            "type": "bjTemplates",
            "id": "2",
            "attributes": {
                "name": "地方性黑名单"
            }
        }
    ]
}
```

#### <a name="单条示例">单条示例</a>

```json
{
    "meta": [],
    "data": {
        "type": "workOrderTasks",
        "id": "3",
        "attributes": {
            "reason": "",
            "feedbackRecords": [
                {
                    "crew": 2,
                    "crewName": "人社局管理员",
                    "userGroup": 3,
                    "userGroupName": "人力和社会资源保障局",
                    "isExistedTemplate": 1,
                    "templateId": 1,
                    "items": [
                        {
                            "name": "主体名称",
                            "identify": "ZTMC",
                            "type": 1,
                            "length": 200,
                            "options": [],
                            "dimension": 1,
                            "isNecessary": 1,
                            "isMasked": 0,
                            "maskRule": [],
                            "remarks": "信用主体名称"
                        },
                        {
                            "name": "统一社会信用代码",
                            "identify": "TYSHXYDM",
                            "type": 1,
                            "length": 50,
                            "options": [],
                            "dimension": 1,
                            "isNecessary": 1,
                            "isMasked": 1,
                            "maskRule": [
                                3,
                                4
                            ],
                            "remarks": "信用主体代码"
                        },
                        {
                            "name": "信息类别",
                            "identify": "XXLB",
                            "type": 5,
                            "length": 50,
                            "options": [
                                "基础信息",
                                "守信信息",
                                "失信信息",
                                "其他信息"
                            ],
                            "dimension": 2,
                            "isNecessary": 1,
                            "isMasked": 1,
                            "maskRule": [
                                1,
                                2
                            ],
                            "remarks": "信息性质类型，支持单选"
                        }
                    ],
                    "reason": "人力和社会资源保障局的反馈原因",
                    "feedbackTime": 1614422868
                },
                {
                    "crew": 1,
                    "crewName": "发改委管理员",
                    "userGroup": 1,
                    "userGroupName": "发展和改革委员会",
                    "isExistedTemplate": 1,
                    "templateId": 1,
                    "items": [
                        {
                            "name": "主体名称",
                            "identify": "ZTMC",
                            "type": 1,
                            "length": 200,
                            "options": [],
                            "dimension": 1,
                            "isNecessary": 1,
                            "isMasked": 0,
                            "maskRule": [],
                            "remarks": "信用主体名称"
                        },
                        {
                            "name": "统一社会信用代码",
                            "identify": "TYSHXYDM",
                            "type": 1,
                            "length": 50,
                            "options": [],
                            "dimension": 1,
                            "isNecessary": 1,
                            "isMasked": 1,
                            "maskRule": [
                                3,
                                4
                            ],
                            "remarks": "信用主体代码"
                        },
                        {
                            "name": "信息类别",
                            "identify": "XXLB",
                            "type": 5,
                            "length": 50,
                            "options": [
                                "基础信息",
                                "守信信息",
                                "失信信息",
                                "其他信息"
                            ],
                            "dimension": 2,
                            "isNecessary": 1,
                            "isMasked": 1,
                            "maskRule": [
                                1,
                                2
                            ],
                            "remarks": "信息性质类型，支持单选"
                        }
                    ],
                    "reason": "发展和改革委员会的反馈原因",
                    "feedbackTime": 1614509268
                }
            ],
            "status": 1,
            "createTime": 1531897790,
            "updateTime": 1531897790,
            "statusTime": 0
        },
        "relationships": {
            "parentTask": {
                "data": {
                    "type": "parentTasks",
                    "id": "2"
                }
            },
            "assignObject": {
                "data": {
                    "type": "userGroups",
                    "id": "3"
                }
            },
            "template": {
                "data": {
                    "type": "gbTemplates",
                    "id": "2"
                }
            }
        },
        "links": {
            "self": "127.0.0.1:8080/workOrderTasks/3"
        }
    },
    "included": [
        {
            "type": "gbTemplates",
            "id": "2",
            "attributes": {
                "name": "地方性黑名单",
                "identify": "DFXHMD",
                "subjectCategory": [
                    1,
                    3
                ],
                "dimension": 1,
                "exchangeFrequency": 1,
                "infoClassify": 1,
                "infoCategory": 1,
                "description": "目录描述信息",
                "items": [
                    {
                        "name": "主体名称",
                        "identify": "ZTMC",
                        "type": 1,
                        "length": 200,
                        "options": [],
                        "dimension": 1,
                        "isNecessary": 1,
                        "isMasked": 0,
                        "maskRule": [],
                        "remarks": "信用主体名称"
                    },
                    {
                        "name": "统一社会信用代码",
                        "identify": "TYSHXYDM",
                        "type": 1,
                        "length": 50,
                        "options": [],
                        "dimension": 1,
                        "isNecessary": 1,
                        "isMasked": 1,
                        "maskRule": [
                            3,
                            4
                        ],
                        "remarks": "信用主体代码"
                    },
                    {
                        "name": "信息类别",
                        "identify": "XXLB",
                        "type": 5,
                        "length": 50,
                        "options": [
                            "基础信息",
                            "守信信息",
                            "失信信息",
                            "其他信息"
                        ],
                        "dimension": 2,
                        "isNecessary": 1,
                        "isMasked": 1,
                        "maskRule": [
                            1,
                            2
                        ],
                        "remarks": "信息性质类型，支持单选"
                    }
                ],
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            }
        },
        {
            "type": "userGroups",
            "id": "3",
            "attributes": {
                "name": "人力资源和社会保障局",
                "shortName": "人社局",
                "purview": [],
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            }
        },
        {
            "type": "parentTasks",
            "id": "2",
            "attributes": {
                "templateType": 1,
                "title": "归集地方性黑名单信息",
                "description": "依据国家要求，现需要按要求归集地方性黑名单信息",
                "endTime": "2020-01-01",
                "attachment": {
                    "name": "国203号文",
                    "identify": "b900638af896a26de13bc89ce4f64124.pdf"
                },
                "ratio": 50,
                "reason": "",
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            }
        }
    ]
}
```

#### <a name="多条示例">多条示例</a>

```json
{
    "meta": {
        "count": 3,
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        }
    },
    "links": {
        "first": null,
        "last": null,
        "prev": null,
        "next": null
    },
    "data": [
        {
            "type": "workOrderTasks",
            "id": "1",
            "attributes": {
                "reason": "撤销原因",
                "feedbackRecords": [],
                "status": 1,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "relationships": {
                "parentTask": {
                    "data": {
                        "type": "parentTasks",
                        "id": "1"
                    }
                },
                "assignObject": {
                    "data": {
                        "type": "userGroups",
                        "id": "2"
                    }
                },
                "template": {
                    "data": {
                        "type": "gbTemplates",
                        "id": "1"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8080/workOrderTasks/1"
            }
        },
        {
            "type": "workOrderTasks",
            "id": "2",
            "attributes": {
                "reason": "终结原因",
                "feedbackRecords": [],
                "status": 4,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "relationships": {
                "parentTask": {
                    "data": {
                        "type": "parentTasks",
                        "id": "2"
                    }
                },
                "assignObject": {
                    "data": {
                        "type": "userGroups",
                        "id": "2"
                    }
                },
                "template": {
                    "data": {
                        "type": "bjTemplates",
                        "id": "2"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8080/workOrderTasks/2"
            }
        },
        {
            "type": "workOrderTasks",
            "id": "3",
            "attributes": {
                "reason": "",
                "feedbackRecords": [
                    {
                        "crew": 2,
                        "crewName": "市监局管理员",
                        "userGroup": 2,
                        "userGroupName": "市场监督管理局",
                        "isExistedTemplate": 1,
                        "templateId": 1,
                        "items": [
                            {
                                "name": "主体名称",
                                "identify": "ZTMC",
                                "type": 1,
                                "length": 200,
                                "options": [],
                                "dimension": 1,
                                "isNecessary": 1,
                                "isMasked": 0,
                                "maskRule": [],
                                "remarks": "信用主体名称"
                            },
                            {
                                "name": "统一社会信用代码",
                                "identify": "TYSHXYDM",
                                "type": 1,
                                "length": 50,
                                "options": [],
                                "dimension": 1,
                                "isNecessary": 1,
                                "isMasked": 1,
                                "maskRule": [
                                    3,
                                    4
                                ],
                                "remarks": "信用主体代码"
                            },
                            {
                                "name": "信息类别",
                                "identify": "XXLB",
                                "type": 5,
                                "length": 50,
                                "options": [
                                    "基础信息",
                                    "守信信息",
                                    "失信信息",
                                    "其他信息"
                                ],
                                "dimension": 2,
                                "isNecessary": 1,
                                "isMasked": 1,
                                "maskRule": [
                                    1,
                                    2
                                ],
                                "remarks": "信息性质类型，支持单选"
                            }
                        ],
                        "reason": "市场监督管理局的反馈原因",
                        "feedbackTime": 1614422868
                    },
                    {
                        "crew": 1,
                        "crewName": "发改委管理员",
                        "userGroup": 1,
                        "userGroupName": "发展和改革委员会",
                        "isExistedTemplate": 1,
                        "templateId": 1,
                        "items": [
                            {
                                "name": "主体名称",
                                "identify": "ZTMC",
                                "type": 1,
                                "length": 200,
                                "options": [],
                                "dimension": 1,
                                "isNecessary": 1,
                                "isMasked": 0,
                                "maskRule": [],
                                "remarks": "信用主体名称"
                            },
                            {
                                "name": "统一社会信用代码",
                                "identify": "TYSHXYDM",
                                "type": 1,
                                "length": 50,
                                "options": [],
                                "dimension": 1,
                                "isNecessary": 1,
                                "isMasked": 1,
                                "maskRule": [
                                    3,
                                    4
                                ],
                                "remarks": "信用主体代码"
                            },
                            {
                                "name": "信息类别",
                                "identify": "XXLB",
                                "type": 5,
                                "length": 50,
                                "options": [
                                    "基础信息",
                                    "守信信息",
                                    "失信信息",
                                    "其他信息"
                                ],
                                "dimension": 2,
                                "isNecessary": 1,
                                "isMasked": 1,
                                "maskRule": [
                                    1,
                                    2
                                ],
                                "remarks": "信息性质类型，支持单选"
                            }
                        ],
                        "reason": "发展和改革委员会的反馈原因",
                        "feedbackTime": 1614509268
                    }
                ],
                "status": 0,
                "createTime": 1531897790,
                "updateTime": 1531897790,
                "statusTime": 0
            },
            "relationships": {
                "parentTask": {
                    "data": {
                        "type": "parentTasks",
                        "id": "2"
                    }
                },
                "assignObject": {
                    "data": {
                        "type": "userGroups",
                        "id": "3"
                    }
                },
                "template": {
                    "data": {
                        "type": "bjTemplates",
                        "id": "2"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8080/workOrderTasks/2"
            }
        }
    ],
    "included": [
        {
            "type": "gbTemplates",
            "id": "1",
            "attributes": {
                "name": "地方性红名单"
            }
        },
        {
            "type": "bjTemplates",
            "id": "2",
            "attributes": {
                "name": "地方性黑名单"
            }
        },
        {
            "type": "userGroups",
            "id": "2",
            "attributes": {
                "name": "市场监督管理局"
            }
        },
        {
            "type": "userGroups",
            "id": "3",
            "attributes": {
                "name": "人力资源和社会保障局"
            }
        },
        {
            "type": "parentTasks",
            "id": "1",
            "attributes": {
                "templateType": 1,
                "title": "归集地方性红名单信息",
                "endTime": "2020-01-01"
            }
        },
        {
            "type": "parentTasks",
            "id": "2",
            "attributes": {
                "templateType": 2,
                "title": "归集地方性黑名单信息",
                "endTime": "2020-01-01"
            }
        }
    ]
}
```
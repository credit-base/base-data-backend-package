<?php
namespace BaseData\Rule\Adapter\RuleService;

use BaseData\Rule\Model\RuleService;

interface IRuleServiceAdapter
{
    public function fetchOne($id) : RuleService;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(RuleService $ruleService) : bool;

    public function edit(RuleService $ruleService, array $keys = array()) : bool;
}

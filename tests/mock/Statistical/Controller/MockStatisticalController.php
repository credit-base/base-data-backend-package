<?php
namespace BaseData\Statistical\Controller;

use BaseData\Statistical\Repository\StatisticalRepository;

class MockStatisticalController extends StatisticalController
{
    public function getStatisticalRepository(string $type) : StatisticalRepository
    {
        return parent::getStatisticalRepository($type);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter;
use BaseData\ResourceCatalogData\Adapter\Task\TaskDbAdapter;

class TaskRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(TaskRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends TaskRepository
        {
            public function getAdapter() : ITaskAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter() : ITaskAdapter
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : ITaskAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new TaskDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->childRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter',
            $this->childRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\Task\TaskDbAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter',
            $this->childRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\Task\TaskMockAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testAdd()
    {
        $id = 1;
        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask($id);
        $keys = array();
        
        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->add(Argument::exact($task))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($task, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask($id);
        $keys = array();
        
        $adapter = $this->prophesize(ITaskAdapter::class);
        $adapter->edit(Argument::exact($task), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($task, $keys);
    }

    public function testFetchOne()
    {
        $id = 1;

        $taskAdapter = $this->prophesize(ITaskAdapter::class);
        $taskAdapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($taskAdapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $taskAdapter = $this->prophesize(ITaskAdapter::class);
        $taskAdapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($taskAdapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $taskAdapter = $this->prophesize(ITaskAdapter::class);
        $taskAdapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($taskAdapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }
}

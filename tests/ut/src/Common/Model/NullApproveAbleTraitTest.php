<?php
namespace BaseData\Common\Model;

use PHPUnit\Framework\TestCase;

class NullApproveAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullApproveAbleTrait::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testApprove()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->approve();
        $this->assertFalse($result);
    }

    public function testReject()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->reject();
        $this->assertFalse($result);
    }

    public function testApproveAction()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->approveAction();
        $this->assertFalse($result);
    }

    public function testRejectAction()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->rejectAction();
        $this->assertFalse($result);
    }

    public function testIsPending()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->isPending();
        $this->assertFalse($result);
    }

    public function testIsApprove()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->isApprove();
        $this->assertFalse($result);
    }

    public function testIsReject()
    {
        $this->mockResourceNotExist();
        
        $result = $this->trait->isReject();
        $this->assertFalse($result);
    }

    private function mockResourceNotExist()
    {
        $this->trait->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);
    }
}

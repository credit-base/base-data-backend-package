<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullErrorDataTest extends TestCase
{
    private $errorData;

    public function setUp()
    {
        $this->errorData = NullErrorData::getInstance();
    }

    public function tearDown()
    {
        unset($this->errorData);
    }

    public function testExtendsErrorData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\ErrorData', $this->errorData);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->errorData);
    }
}

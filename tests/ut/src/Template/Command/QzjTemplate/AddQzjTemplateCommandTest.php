<?php
namespace BaseData\Template\Command\QzjTemplate;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class AddQzjTemplateCommandTest extends TestCase
{
    private $fakerData;

    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $this->fakerData = array(
            'sourceUnit' => $faker->randomDigit(),
            'category' => $faker->randomDigit()
        );

        $this->command = new AddQzjTemplateCommand(
            $faker->word(),
            $faker->regexify('[A-Z_]{1,100}'),
            $faker->randomElements(array(1,2,3), 2),
            $faker->randomElement(array(1,2,3)),
            $faker->randomDigit(),
            $faker->randomDigit(),
            $faker->randomDigit(),
            $faker->sentence(),
            $faker->randomElements(array('1','2','3'), 2),
            $this->fakerData['sourceUnit'],
            $this->fakerData['category']
        );
    }
    
    public function testExtendsCommand()
    {
        $this->assertInstanceOf('BaseData\Template\Command\Template\AddTemplateCommand', $this->command);
    }

    public function testSourceUnitParameter()
    {
        $this->assertEquals($this->fakerData['sourceUnit'], $this->command->sourceUnit);
    }

    public function testCategoryParameter()
    {
        $this->assertEquals($this->fakerData['category'], $this->command->category);
    }
}

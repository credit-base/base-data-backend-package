<?php
namespace BaseData\Template\Adapter\GbTemplate\Query\Persistence;

use Marmot\Framework\Classes\Db;

class GbTemplateDb extends Db
{
    const TABLE = 'gb_template';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

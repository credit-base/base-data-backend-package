<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class WbjSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\ResourceCatalogData\Command\WbjSearchData\EnableWbjSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\WbjSearchData\EnableWbjSearchDataCommandHandler',
        'BaseData\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\WbjSearchData\DisableWbjSearchDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

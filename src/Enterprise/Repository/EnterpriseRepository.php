<?php
namespace BaseData\Enterprise\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Enterprise\Model\Enterprise;
use BaseData\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;
use BaseData\Enterprise\Adapter\Enterprise\EnterpriseDbAdapter;
use BaseData\Enterprise\Adapter\Enterprise\EnterpriseMockAdapter;

class EnterpriseRepository extends Repository implements IEnterpriseAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new EnterpriseDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IEnterpriseAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IEnterpriseAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IEnterpriseAdapter
    {
        return new EnterpriseMockAdapter();
    }

    public function fetchOne($id) : Enterprise
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Enterprise $enterprise) : bool
    {
        return $this->getAdapter()->add($enterprise);
    }

    public function edit(Enterprise $enterprise, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($enterprise, $keys);
    }
}

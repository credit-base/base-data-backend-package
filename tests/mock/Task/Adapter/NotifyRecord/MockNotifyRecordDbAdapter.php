<?php
namespace BaseData\Task\Adapter\NotifyRecord;

use Marmot\Interfaces\INull;

use BaseData\Task\Translator\NotifyRecordDbTranslator;
use BaseData\Task\Adapter\NotifyRecord\Query\NotifyRecordRowCacheQuery;

class MockNotifyRecordDbAdapter extends NotifyRecordDbAdapter
{
    public function getDbTranslator() : NotifyRecordDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : NotifyRecordRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

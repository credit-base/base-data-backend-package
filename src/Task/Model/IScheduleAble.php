<?php
namespace BaseData\Task\Model;

interface IScheduleAble
{
    public function execute() : bool;
}

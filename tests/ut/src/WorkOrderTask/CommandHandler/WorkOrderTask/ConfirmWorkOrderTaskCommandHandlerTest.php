<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;

class ConfirmWorkOrderTaskCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(ConfirmWorkOrderTaskCommandHandler::class)
                                     ->setMethods(['getWorkOrderTaskRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetWorkOrderTaskRepository()
    {
        $commandHandler = new MockConfirmWorkOrderTaskCommandHandler();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository',
            $commandHandler->getWorkOrderTaskRepository()
        );
    }

    public function testExecute()
    {
        $command = new ConfirmWorkOrderTaskCommand(
            $this->faker->randomNumber()
        );

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->confirm()->shouldBeCalledTimes(1)->willReturn(true);

        $workOrderTaskRepository = $this->prophesize(WorkOrderTaskRepository::class);
        $workOrderTaskRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($workOrderTask->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getWorkOrderTaskRepository')
            ->willReturn($workOrderTaskRepository->reveal());

        $result = $this->commandHandler->execute($command);
        
        $this->assertTrue($result);
    }
}

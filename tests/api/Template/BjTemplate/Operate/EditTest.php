<?php
namespace BaseData\Template\BjTemplate\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\BjTemplate;

/**
 * 测试本级资源目录接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在一条本级资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_gb_template' => $this->gbTemplate(),
            ]
        );
    }

    public function testViewBjTemplate()
    {
        $items = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['GTGSH']]],
            [Template::DIMENSION_CN[Template::DIMENSION['ZWGX']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['SS']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['XZXK']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['QTXX']]]
        );

        $data = array(
            "data" => array(
                "type" => "bjTemplates",
                "attributes" => array(
                    "name" => '本级企业基本信息编辑',
                    "identify" => 'BJQYJBXXBJ',
                    "subjectCategory" => array(Template::SUBJECT_CATEGORY['GTGSH']),
                    "dimension" => Template::DIMENSION['ZWGX'],
                    "exchangeFrequency" => Template::EXCHANGE_FREQUENCY['SS'],
                    "infoClassify" => Template::INFO_CLASSIFY['XZXK'],
                    "infoCategory" => Template::INFO_CATEGORY['QTXX'],
                    "description" => "本级企业基本信息编辑",
                    'items' => $items
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 2)
                        )
                    ),
                    "gbTemplate" => array(
                        "data" => array(
                            array("type" => "gbTemplates", "id" => 2)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'bjTemplates/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals('本级企业基本信息编辑', $attributes['name']);
        $this->assertEquals('BJQYJBXXBJ', $attributes['identify']);
        $this->assertEquals(array(Template::SUBJECT_CATEGORY['GTGSH']), $attributes['subjectCategory']);
        $this->assertEquals(Template::DIMENSION['ZWGX'], $attributes['dimension']);
        $this->assertEquals(Template::EXCHANGE_FREQUENCY['SS'], $attributes['exchangeFrequency']);
        $this->assertEquals(Template::INFO_CLASSIFY['XZXK'], $attributes['infoClassify']);
        $this->assertEquals(Template::INFO_CATEGORY['QTXX'], $attributes['infoCategory']);
        $this->assertEquals($items, $attributes['items']);

        $this->assertEquals('本级企业基本信息编辑', $attributes['description']);

        $relationships = $contents['data']['relationships'];
        $relationshipsUserGroup = $relationships['sourceUnit']['data'];
        $this->assertEquals('userGroups', $relationshipsUserGroup['type']);
        $this->assertEquals(2, $relationshipsUserGroup['id']);

        $relationshipsGbTemplate = $relationships['gbTemplate']['data'];
        $this->assertEquals('gbTemplates', $relationshipsGbTemplate['type']);
        $this->assertEquals(2, $relationshipsGbTemplate['id']);
    }
}

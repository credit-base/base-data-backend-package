<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\Repository;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Repository\CrewRepository;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\ModelFactory;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Translator\RuleServiceDbTranslator;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

trait RuleServiceCommandHandlerTrait
{
    protected function getCrewRepository() : CrewRepository
    {
        return new CrewRepository();
    }

    protected function fetchCrew(int $id) : Crew
    {
        return $this->getCrewRepository()->fetchOne($id);
    }

    protected function getRuleServiceRepository() : RuleServiceRepository
    {
        return new RuleServiceRepository();
    }

    protected function fetchRuleService(int $id) : RuleService
    {
        return $this->getRuleServiceRepository()->fetchOne($id);
    }

    protected function getRuleServiceDbTranslator() : RuleServiceDbTranslator
    {
        return new RuleServiceDbTranslator();
    }

    protected function applyInfo(UnAuditedRuleService $unAuditedRuleService) : array
    {
        $applyInfo = $this->getRuleServiceDbTranslator()->objectToArray($unAuditedRuleService);

        return $applyInfo;
    }

    protected function getUnAuditedRuleServiceRepository() : UnAuditedRuleServiceRepository
    {
        return new UnAuditedRuleServiceRepository();
    }

    protected function fetchUnAuditedRuleService(int $id) : UnAuditedRuleService
    {
        return $this->getUnAuditedRuleServiceRepository()->fetchOne($id);
    }
    
    protected function getUnAuditedRuleService() : UnAuditedRuleService
    {
        return new UnAuditedRuleService();
    }

    protected function getRepositoryFactory() : RepositoryFactory
    {
        return new RepositoryFactory();
    }

    protected function getRepository(int $category) : Repository
    {
        return $this->getRepositoryFactory()->getRepository($category);
    }

    protected function getModelFactory() : ModelFactory
    {
        return new ModelFactory();
    }

    protected function generateUnAuditedRuleServiceCrew(
        int $crewId,
        UnAuditedRuleService $unAuditedRuleService
    ) : UnAuditedRuleService {
        $crew = $this->fetchCrew($crewId);

        $unAuditedRuleService->setCrew($crew);
        $unAuditedRuleService->setPublishCrew($crew);

        return $unAuditedRuleService;
    }

    protected function generateUnAuditedRuleServiceRules(
        array $commandRules,
        UnAuditedRuleService $unAuditedRuleService
    ) : UnAuditedRuleService {
        $rules = array();
        foreach ($commandRules as $key => $condition) {
            $model = $this->getModelFactory()->getModel($key);
            $model->setTransformationTemplate($unAuditedRuleService->getTransformationTemplate());
            $model->setSourceTemplate($unAuditedRuleService->getSourceTemplate());
            $model->setCondition($condition);
            $rules[$key] = $model;
        }

        $unAuditedRuleService->setRules($rules);

        return $unAuditedRuleService;
    }
}

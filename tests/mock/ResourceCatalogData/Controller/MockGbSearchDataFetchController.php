<?php
namespace BaseData\ResourceCatalogData\Controller;

use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use Marmot\Interfaces\IView;

class MockGbSearchDataFetchController extends GbSearchDataFetchController
{
    public function getRepository() : IGbSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

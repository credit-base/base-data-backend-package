<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Model\IncompleteData;

/**
 * BaseData\Rule\Model\TransformationRule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class TransformationRuleTest extends TestCase
{
    private $rule;

    private $sourceSearchData;

    private $transformationSearchData;

    private $initializationData;

    private $data;

    public function setUp()
    {
        $this->sourceSearchData =
            \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);
        $this->transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);

        $condition = array(
            'ZTMC' => 'ZTMC'
        );

        $requiredItems = array(
            'ZTMC' => array('ZTMC')
        );

        $this->initializationData = array(
            'condition' => $condition,
            'sourceItemsData' => $this->sourceSearchData->getItemsData()->getData(),
            'transformationItemsData' => $this->transformationSearchData->getItemsData()->getData(),
            'requiredItems' => $requiredItems
        );

        $this->data = array(
            'transformationItemsData' => $this->transformationSearchData->getItemsData()->getData(),
            'incompleteItems' => array()
        );

        $this->rule = new MockTransformationRule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\Rule", $this->rule);
    }

    public function testImplementsIRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\IRule", $this->rule);
    }

    private function initialValidate($condition)
    {
        $this->rule = $this->getMockBuilder(MockTransformationRule::class)
            ->setMethods([
                'getTransformationTemplate',
                'getSourceTemplate',
                'fetchTemplateItems',
                'templateItemComparison',
                'getCondition'
            ])
            ->getMock();
        
        $transformationTemplate = \BaseData\Template\Utils\BjTemplateMockFactory::generateBjTemplate(1);
        $transformationTemplateItems['ZTMC'] = array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => 1,    //数据类型
            "length" => '255',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        );

        $sourceTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $sourceTemplateItems['ZTMC'] = array(
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => 1,    //数据类型
            "length" => '255',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 2,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        );

        $this->rule->expects($this->exactly(1))->method('getTransformationTemplate')->willReturn(
            $transformationTemplate
        );
        
        $this->rule->expects($this->exactly(1))->method('getSourceTemplate')->willReturn(
            $sourceTemplate
        );

        $this->rule->expects($this->exactly(2))
                   ->method('fetchTemplateItems')
                   ->will($this->returnValueMap(
                       [
                        [$transformationTemplate,$transformationTemplateItems],
                        [$sourceTemplate,$sourceTemplateItems]
                       ]
                   ));

        $this->rule->expects($this->exactly(1))->method('getCondition')->willReturn(
            $condition
        );
    }

    public function testValidateTransformationIdentifyFalse()
    {
        $condition = array("TYSHXYDM"=>"TYSHXYDM");
        $this->initialValidate($condition);

        $result = $this->rule->validate();

        $this->assertEquals('transformationRulesTransformationNotExist', Core::getLastError()->getSource()['pointer']);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testValidateSourceIdentifyFalse()
    {
        $condition = array("ZTMC"=>"XZXK");
        $this->initialValidate($condition);

        $result = $this->rule->validate();

        $this->assertEquals('transformationRulesSourceNotExist', Core::getLastError()->getSource()['pointer']);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testValidateFalse()
    {
        $condition = array("ZTMC"=>"ZTMC");
        $this->initialValidate($condition);

        $this->rule->expects($this->exactly(1))->method('templateItemComparison')->willReturn(false);

        $result = $this->rule->validate();

        $this->assertFalse($result);
    }

    public function testValidateTrue()
    {
        $condition = array("ZTMC"=>"ZTMC");
        $this->initialValidate($condition);

        $this->rule->expects($this->exactly(1))->method('templateItemComparison')->willReturn(true);

        $result = $this->rule->validate();

        $this->assertTrue($result);
    }

    public function testValidateTransformation()
    {
        $result = $this->rule->validateTransformation($this->transformationSearchData);

        $this->assertTrue($result);
    }

    public function testTransformationInitialization()
    {
        $rule = $this->getMockBuilder(MockTransformationRule::class)
            ->setMethods([
                'getCondition',
                'transformationTemplateRequired'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('getCondition')
             ->willReturn($this->initializationData['condition']);

        $rule->expects($this->exactly(1))
             ->method('transformationTemplateRequired')
             ->with($this->transformationSearchData->getTemplate())
             ->willReturn($this->initializationData['requiredItems']);

        $result = $rule->transformationInitialization($this->sourceSearchData, $this->transformationSearchData);

        $this->assertEquals($result, $this->initializationData);
    }

    public function testValidateInitializationDataFormat()
    {
        $result = $this->rule->validateInitializationDataFormat($this->initializationData);

        $this->assertTrue($result);
    }

    public function testTransformationExecuteFailure()
    {
        $rule = $this->getMockBuilder(MockTransformationRule::class)
            ->setMethods([
                'validateInitializationDataFormat'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(false);

        $result = $rule->transformationExecute($this->initializationData, $this->transformationSearchData);

        $this->assertEquals($result, array());
    }

    public function testTransformationExecute()
    {
        $rule = $this->getMockBuilder(MockTransformationRule::class)
            ->setMethods([
                'validateInitializationDataFormat',
                'generateItemsData',
                'fetchIncompleteItems'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(true);

        $initializationData = $this->initializationData;
    
        $rule->expects($this->exactly(1))
             ->method('generateItemsData')
             ->with(
                 $initializationData['sourceItemsData'],
                 $initializationData['transformationItemsData'],
                 $initializationData['condition']
             )->willReturn($initializationData['transformationItemsData']);

        $rule->expects($this->exactly(1))
             ->method('fetchIncompleteItems')
             ->with($initializationData['transformationItemsData'], $initializationData['requiredItems'])
             ->willReturn($this->data['incompleteItems']);

        $result = $rule->transformationExecute($this->initializationData, $this->transformationSearchData);

        $this->assertEquals($result, $this->data);
    }
    
    public function testValidateDataFormat()
    {
        $result = $this->rule->validateDataFormat($this->data);

        $this->assertTrue($result);
    }

    public function testTransformationResultFailure()
    {
        $rule = $this->getMockBuilder(MockTransformationRule::class)
            ->setMethods([
                'validateDataFormat',
                'failureDataException'
            ])->getMock();

        $failureData = new FailureData();
        $rule->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(false);
             
        $rule->expects($this->exactly(1))
             ->method('failureDataException')
             ->with(
                 $this->transformationSearchData,
                 ErrorData::STATUS['PROGRAM_EXCEPTION']
             )
             ->willReturn($failureData);
             
         $result = $rule->transformationResult($this->data, $this->transformationSearchData);

        $this->assertEquals($result, $failureData);
    }
    
    public function testTransformationResultIncomplete()
    {
        $rule = $this->getMockBuilder(MockTransformationRule::class)
            ->setMethods([
                'validateDataFormat',
                'transformationDataResult'
            ])->getMock();
            
        $data = $this->data;
        $data['incompleteItems'] = array('ZTMC'=>array('ZTMC'));
        $incompleteData = new IncompleteData();
          
        $rule->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(true);
             
        $rule->expects($this->exactly(1))
             ->method('transformationDataResult')
             ->with(
                 $data['incompleteItems'],
                 $this->transformationSearchData
             )->willReturn($incompleteData);
        $incompleteData->getItemsData()->setData($this->data['transformationItemsData']);
        $hash = md5(base64_encode(gzcompress(serialize($this->data['transformationItemsData']))));
        $incompleteData->setHash($hash);
             
        $result = $rule->transformationResult($data, $this->transformationSearchData);

        $this->assertEquals($result, $incompleteData);
    }

    public function testGenerateItemsData()
    {
        $sourceItemsData = array('ZTMC' => '企业名称');
        $transformationItemsData = array('ZTMC' => '');
        $condition = array('ZTMC' => 'ZTMC');
        
        $result = $this->rule->generateItemsData($sourceItemsData, $transformationItemsData, $condition);
          
        $this->assertEquals($result, array('ZTMC' => '企业名称'));
    }
}

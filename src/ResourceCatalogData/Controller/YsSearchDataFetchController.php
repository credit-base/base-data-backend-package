<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\ResourceCatalogData\View\YsSearchDataView;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;

class YsSearchDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new YsSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IYsSearchDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new YsSearchDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'ysSearchData';
    }
}

<?php
namespace BaseData\Rule\UnAuditedRuleService\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\RuleUtils;
use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\Model\UnAuditedRuleService;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试规则审核接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, RuleUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在一条规则审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_apply_form_rule' => $this->applyFormRule(),
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_gb_template' => $this->gbTemplate()
            ]
        );
    }

    public function testViewUnAuditedRuleService()
    {
        $setUnAuditedRuleService = $this->applyFormRule()[0];
        $setGbTemplate = $this->gbTemplate()[0];
        $setWbjTemplate = $this->wbjTemplate()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedRules/1?include=transformationTemplate,sourceTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedRuleCompareData($setUnAuditedRuleService, $contents['data']);
        $this->gbTemplateCompareData($setGbTemplate, $contents['included'][0]);
        $this->wbjTemplateCompareData($setWbjTemplate, $contents['included'][1]);
    }
}

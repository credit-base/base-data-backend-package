<?php
namespace BaseData\WorkOrderTask\View;

use PHPUnit\Framework\TestCase;

use BaseData\WorkOrderTask\Model\WorkOrderTask;

class WorkOrderTaskViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $workOrderTask = new WorkOrderTaskView(new WorkOrderTask());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $workOrderTask);
    }
}

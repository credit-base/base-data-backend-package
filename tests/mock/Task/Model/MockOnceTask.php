<?php
namespace BaseData\Task\Model;

class MockOnceTask extends OnceTask
{
    protected function executeAction() : bool
    {
        return true;
    }

    public function afterExecute() : bool
    {
        return parent::afterExecute();
    }
}

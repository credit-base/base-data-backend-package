<?php
namespace BaseData\ResourceCatalogData\Utils;

trait WbjSearchDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $wbjSearchData
    ) {
        $this->assertEquals($expectedArray['wbj_search_data_id'], $wbjSearchData->getId());
        $this->assertEquals($expectedArray['info_classify'], $wbjSearchData->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $wbjSearchData->getInfoCategory());
        $this->assertEquals($expectedArray['crew_id'], $wbjSearchData->getCrew()->getId());
        $this->assertEquals($expectedArray['usergroup_id'], $wbjSearchData->getSourceUnit()->getId());
        $this->assertEquals($expectedArray['subject_category'], $wbjSearchData->getSubjectCategory());
        $this->assertEquals($expectedArray['dimension'], $wbjSearchData->getDimension());
        $this->assertEquals($expectedArray['name'], $wbjSearchData->getName());
        $this->assertEquals($expectedArray['identify'], $wbjSearchData->getIdentify());
        $this->assertEquals($expectedArray['expiration_date'], $wbjSearchData->getExpirationDate());
        $this->assertEquals($expectedArray['wbj_template_id'], $wbjSearchData->getTemplate()->getId());
        $this->assertEquals($expectedArray['wbj_items_data_id'], $wbjSearchData->getItemsData()->getId());
        $this->assertEquals($expectedArray['status'], $wbjSearchData->getStatus());
        $this->assertEquals($expectedArray['hash'], $wbjSearchData->getHash());
        $this->assertEquals($expectedArray['create_time'], $wbjSearchData->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $wbjSearchData->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $wbjSearchData->getStatusTime());
        $this->assertEquals($expectedArray['task_id'], $wbjSearchData->getTask()->getId());
    }
}

<?php
namespace BaseData\Template\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Utils\GbTemplateUtils;
use BaseData\Template\Utils\GbTemplateMockFactory;

class GbTemplateDbTranslatorTest extends TestCase
{
    use GbTemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbTemplateDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Template\Model\NullGbTemplate', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $gbTemplate = GbTemplateMockFactory::generateGbTemplate(1);

        $expression['gb_template_id'] = $gbTemplate->getId();
        $expression['name'] = $gbTemplate->getName();
        $expression['identify'] = $gbTemplate->getIdentify();
        $expression['subject_category'] = json_encode($gbTemplate->getSubjectCategory());
        $expression['dimension'] = $gbTemplate->getDimension();
        $expression['exchange_frequency'] = $gbTemplate->getExchangeFrequency();
        $expression['info_classify'] = $gbTemplate->getInfoClassify();
        $expression['info_category'] = $gbTemplate->getInfoCategory();
        $expression['description'] = $gbTemplate->getDescription();
        $expression['items'] = json_encode($gbTemplate->getItems());

        $expression['status'] = $gbTemplate->getStatus();
        $expression['status_time'] = $gbTemplate->getStatusTime();
        $expression['create_time'] = $gbTemplate->getCreateTime();
        $expression['update_time'] = $gbTemplate->getUpdateTime();

        $gbTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Template\Model\GbTemplate', $gbTemplate);
        $this->compareArrayAndObject($expression, $gbTemplate);
    }

    public function testObjectToArray()
    {
        $gbTemplate = GbTemplateMockFactory::generateGbTemplate(1);

        $expression = $this->translator->objectToArray($gbTemplate);

        $this->compareArrayAndObject($expression, $gbTemplate);
    }
}

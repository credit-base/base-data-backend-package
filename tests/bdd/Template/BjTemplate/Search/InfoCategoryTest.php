<?php
namespace BaseData\Template\BjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Translator\BjTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 通过资源信息类别搜索
 */
class InfoCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBjTemplateList()
    {
        $repository = new BjTemplateRepository();
 
        $filter['infoCategory'] = Template::INFO_CATEGORY['QTXX'];

        list($bjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $bjTemplateList;
    }

    /**
     * @Then: 我可以看到信息分类为其他信息的所有资源目录
     */
    public function testViewBjTemplateList()
    {
        $setBjTemplateList = $this->bjTemplate();

        foreach ($setBjTemplateList as $key => $setBjTemplate) {
            $setBjTemplateList[$key]['bj_template_id'] = $key +1;
            $setBjTemplateList[$key]['subject_category'] = json_decode($setBjTemplate['subject_category'], true);
            $setBjTemplateList[$key]['items'] = json_decode($setBjTemplate['items'], true);
        }

        $bjTemplateList = $this->fetchBjTemplateList();
        $translator = new BjTemplateDbTranslator();
        foreach ($bjTemplateList as $bjTemplate) {
            $bjTemplateArray[] = $translator->objectToArray($bjTemplate);
        }

        $this->assertEquals($bjTemplateArray, $setBjTemplateList);

        foreach ($bjTemplateArray as $bjTemplate) {
            $this->assertEquals(Template::INFO_CATEGORY['QTXX'], $bjTemplate['info_category']);
        }
    }
}

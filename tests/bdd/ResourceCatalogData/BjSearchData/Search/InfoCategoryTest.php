<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\BjSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件资源目录数据时,在政务网OA中,可以搜索我需要的所有资源目录数据,
 *           通过列表形式查看我搜索的所有资源目录数据 ,以便于我可以快速查看到我需要的资源目录数据
 * @Scenario: 委办局工作人员通过资源信息类别搜索
 */
class InfoCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchBjSearchDataList()
    {
        $repository = new BjSearchDataRepository();
 
        $filter['infoCategory'] = ISearchDataAble::INFO_CATEGORY['QTXX'];

        list($bjSearchDataList, $count) = $repository->filter($filter);
        
        unset($count);

        return $bjSearchDataList;
    }

    /**
     * @Then: 我可以看到信息分类为守信信息的所有资源目录数据
     */
    public function testViewBjSearchDataList()
    {
        $setBjSearchDataList = $this->bjSearchData()['pcore_bj_search_data'];

        foreach ($setBjSearchDataList as $key => $setBjSearchData) {
            unset($setBjSearchData);
            $setBjSearchDataList[$key]['bj_search_data_id'] = $key +1;
        }

        $bjSearchDataList = $this->fetchBjSearchDataList();
        $translator = new BjSearchDataDbTranslator();
        foreach ($bjSearchDataList as $bjSearchData) {
            $bjSearchDataArray[] = $translator->objectToArray($bjSearchData);
        }

        $this->assertEquals($bjSearchDataArray, $setBjSearchDataList);

        foreach ($bjSearchDataArray as $bjSearchData) {
            $this->assertEquals(ISearchDataAble::INFO_CATEGORY['QTXX'], $bjSearchData['info_category']);
        }
    }
}

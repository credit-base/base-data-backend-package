<?php
namespace BaseData\UserGroup\Controller;

use PHPUnit\Framework\TestCase;

class UserGroupFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(UserGroupFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockUserGroupFetchController();

        $this->assertInstanceOf(
            'BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockUserGroupFetchController();

        $this->assertInstanceOf(
            'BaseData\UserGroup\View\UserGroupView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockUserGroupFetchController();

        $this->assertEquals(
            'userGroups',
            $controller->getResourceName()
        );
    }
}

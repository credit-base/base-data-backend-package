<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class GbItemsDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'gb_items_data_id',
            new Persistence\GbItemsDataCache(),
            new Persistence\GbItemsDataDb()
        );
    }
}

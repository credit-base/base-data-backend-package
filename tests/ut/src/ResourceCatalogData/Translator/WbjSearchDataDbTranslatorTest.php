<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\WbjSearchDataUtils;
use BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory;

class WbjSearchDataDbTranslatorTest extends TestCase
{
    use WbjSearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjSearchDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullWbjSearchData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $wbjSearchData = WbjSearchDataMockFactory::generateWbjSearchData(1);

        $expression['wbj_search_data_id'] = $wbjSearchData->getId();
        $expression['info_classify'] = $wbjSearchData->getInfoClassify();
        $expression['info_category'] = $wbjSearchData->getInfoCategory();
        $expression['crew_id'] = $wbjSearchData->getCrew()->getId();
        $expression['usergroup_id'] = $wbjSearchData->getSourceUnit()->getId();
        $expression['subject_category'] = $wbjSearchData->getSubjectCategory();
        $expression['dimension'] = $wbjSearchData->getDimension();
        $expression['name'] = $wbjSearchData->getName();
        $expression['identify'] = $wbjSearchData->getIdentify();
        $expression['expiration_date'] = $wbjSearchData->getExpirationDate();
        $expression['wbj_template_id'] = $wbjSearchData->getTemplate()->getId();
        $expression['wbj_items_data_id'] = $wbjSearchData->getItemsData()->getId();
        $expression['status'] = $wbjSearchData->getStatus();
        $expression['hash'] = $wbjSearchData->getHash();
        $expression['status_time'] = $wbjSearchData->getStatusTime();
        $expression['create_time'] = $wbjSearchData->getCreateTime();
        $expression['update_time'] = $wbjSearchData->getUpdateTime();
        $expression['task_id'] = $wbjSearchData->getTask()->getId();

        $wbjSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\WbjSearchData', $wbjSearchData);
        $this->compareArrayAndObject($expression, $wbjSearchData);
    }

    public function testObjectToArray()
    {
        $wbjSearchData = WbjSearchDataMockFactory::generateWbjSearchData(1);

        $expression = $this->translator->objectToArray($wbjSearchData);

        $this->compareArrayAndObject($expression, $wbjSearchData);
    }
}

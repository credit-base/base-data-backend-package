<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use BaseData\Common\Model\IEnableAble;

class MockEnableYsSearchDataCommandHandler extends EnableYsSearchDataCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}

<?php
namespace BaseData\Template\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseData\Template\Adapter\GbTemplate\GbTemplateDbAdapter;
use BaseData\Template\Adapter\GbTemplate\GbTemplateMockAdapter;

class GbTemplateRepository extends Repository implements IGbTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new GbTemplateDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IGbTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IGbTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IGbTemplateAdapter
    {
        return new GbTemplateMockAdapter();
    }

    public function fetchOne($id) : GbTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(GbTemplate $gbTemplate) : bool
    {
        return $this->getAdapter()->add($gbTemplate);
    }

    public function edit(GbTemplate $gbTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($gbTemplate, $keys);
    }
}

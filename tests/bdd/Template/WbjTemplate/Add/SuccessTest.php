<?php
namespace BaseData\Template\WbjTemplate\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Translator\WbjTemplateDbTranslator;

/**
 * @Feature: 我是委办局领导,当我需要使用委办局目录但又没有对应委办局目录时,在目录管理子系统,新增对应的委办局目录
 *           根据我需要归集的内容和规范新增委办局目录
 *           以便于我有委办局目录可以使用
 * @Scenario: 新增资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $wbjTemplateArray = $this->wbjTemplate()[0];

        $wbjTemplate = new WbjTemplate();

        $wbjTemplate->getSourceUnit()->setId($wbjTemplateArray['source_unit_id']);
        $wbjTemplate->setName($wbjTemplateArray['name']);
        $wbjTemplate->setIdentify($wbjTemplateArray['identify']);
        $wbjTemplate->setSubjectCategory(json_decode($wbjTemplateArray['subject_category'], true));
        $wbjTemplate->setDimension($wbjTemplateArray['dimension']);
        $wbjTemplate->setExchangeFrequency($wbjTemplateArray['exchange_frequency']);
        $wbjTemplate->setInfoClassify($wbjTemplateArray['info_classify']);
        $wbjTemplate->setInfoCategory($wbjTemplateArray['info_category']);
        $wbjTemplate->setDescription($wbjTemplateArray['description']);
        $wbjTemplate->setItems(json_decode($wbjTemplateArray['items'], true));

        return $wbjTemplate->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setWbjTemplate = $this->wbjTemplate()[0];
        $setWbjTemplate['wbj_template_id'] = 1;
        $setWbjTemplate['subject_category'] = json_decode($setWbjTemplate['subject_category'], true);
        $setWbjTemplate['items'] = json_decode($setWbjTemplate['items'], true);
        
        $repository = new WbjTemplateRepository();

        $wbjTemplate = $repository->fetchOne($result);

        $translator = new WbjTemplateDbTranslator();
        $wbjTemplateArray = $translator->objectToArray($wbjTemplate);

        $this->assertEquals($wbjTemplateArray, $setWbjTemplate);
    }
}

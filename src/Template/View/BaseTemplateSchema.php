<?php
namespace BaseData\Template\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class BaseTemplateSchema extends SchemaProvider
{
    protected $resourceType = 'baseTemplates';

    public function getId($baseTemplate) : int
    {
        return $baseTemplate->getId();
    }

    public function getAttributes($baseTemplate) : array
    {
        return [
            'name' => $baseTemplate->getName(),
            'identify' => $baseTemplate->getIdentify(),
            'subjectCategory' => $baseTemplate->getSubjectCategory(),
            'dimension' => $baseTemplate->getDimension(),
            'exchangeFrequency' => $baseTemplate->getExchangeFrequency(),
            'infoClassify' => $baseTemplate->getInfoClassify(),
            'infoCategory' => $baseTemplate->getInfoCategory(),
            'description' => $baseTemplate->getDescription(),
            'category' => $baseTemplate->getCategory(),
            'items' => $baseTemplate->getItems(),

            'status' => $baseTemplate->getStatus(),
            'createTime' => $baseTemplate->getCreateTime(),
            'updateTime' => $baseTemplate->getUpdateTime(),
            'statusTime' => $baseTemplate->getStatusTime(),
        ];
    }
}

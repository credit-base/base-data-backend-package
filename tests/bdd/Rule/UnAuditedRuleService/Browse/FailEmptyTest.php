<?php
namespace BaseData\Rule\UnAuditedRuleService\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则审核权限,在政务网OA的资源目录规则审核中,
 *           我可以管理委办局提交的资源目录的规则,可以查看审核状态为已驳回或待审核的资源目录规则,以便于我可以了解我设置的规则并进行管理
 * @Scenario: 查看规则审核列表-异常列表不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在规则
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看规则审核列表时
     */
    public function fetchUnAuditedRuleServiceList()
    {
        $repository = new UnAuditedRuleServiceRepository();

        list($unAuditedRuleServiceList, $count) = $repository->filter([]);
        
        unset($count);

        return $unAuditedRuleServiceList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewRule()
    {
        $unAuditedRuleServiceList = $this->fetchUnAuditedRuleServiceList();

        $this->assertEmpty($unAuditedRuleServiceList);
    }
}

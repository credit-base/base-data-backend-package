<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Rule\Command\RuleService\DeleteRuleServiceCommand;

class DeleteRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof DeleteRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->fetchCrew($command->crew);

        $ruleService = $this->fetchRuleService($command->id);
        $ruleService->setCrew($crew);

        return $ruleService->delete();
    }
}

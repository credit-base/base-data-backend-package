<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Rule\Command\RuleService\RevokeRuleServiceCommand;

class RevokeRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;
    
    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $crew = $this->fetchCrew($command->crew);

        $unAuditedRuleService = $this->fetchUnAuditedRuleService($command->id);
        $unAuditedRuleService->setPublishCrew($crew);
        $unAuditedRuleService->setCrew($crew);

        return $unAuditedRuleService->revoke();
    }
}

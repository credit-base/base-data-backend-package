<?php
namespace BaseData\Common\Model;

use Marmot\Core;

trait EnableAbleTrait
{
    /**
     * 设置状态
     * @param int $status 状态
     */
    public function setStatus(int $status) : void
    {
        $this->status = in_array(
            $status,
            array(
                IEnableAble::STATUS['ENABLED'],
                IEnableAble::STATUS['DISABLED']
            )
        ) ? $status : IEnableAble::STATUS['ENABLED'];
    }
    
    /**
     * 启用
     * @return bool 是否启用成功
     */
    public function enable() : bool
    {
        if (!$this->isDisabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->updateStatus(IEnableAble::STATUS['ENABLED']);
    }

    /**
     * 禁用
     * @return bool 是否禁用成功
     */
    public function disable() : bool
    {
        if (!$this->isEnabled()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }
        
        return $this->updateStatus(IEnableAble::STATUS['DISABLED']);
    }

    public function isEnabled() : bool
    {
        return $this->getStatus() == IEnableAble::STATUS['ENABLED'];
    }

    public function isDisabled() : bool
    {
        return $this->getStatus() == IEnableAble::STATUS['DISABLED'];
    }

    protected function updateStatus(int $status) : bool
    {
        $this->setStatus($status);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit($this, array(
                    'statusTime',
                    'status',
                    'updateTime'
                ));
    }
}

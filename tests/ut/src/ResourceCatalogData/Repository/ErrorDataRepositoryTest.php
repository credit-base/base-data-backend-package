<?php
namespace BaseData\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;
use BaseData\ResourceCatalogData\Adapter\ErrorData\ErrorDataDbAdapter;

class ErrorDataRepositoryTest extends TestCase
{
    private $repository;
    
    private $childRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(ErrorDataRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->childRepository = new class extends ErrorDataRepository
        {
            public function getAdapter() : IErrorDataAdapter
            {
                return parent::getAdapter();
            }

            public function getActualAdapter() : IErrorDataAdapter
            {
                return parent::getActualAdapter();
            }

            public function getMockAdapter() : IErrorDataAdapter
            {
                return parent::getMockAdapter();
            }
        };
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new ErrorDataDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->childRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $this->childRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\ErrorDataDbAdapter',
            $this->childRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $this->childRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\ErrorDataMockAdapter',
            $this->childRepository->getMockAdapter()
        );
    }

    public function testAdd()
    {
        $id = 1;
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData($id);
        $keys = array();
        
        $adapter = $this->prophesize(IErrorDataAdapter::class);
        $adapter->add(Argument::exact($errorData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($errorData, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData($id);
        $keys = array();
        
        $adapter = $this->prophesize(IErrorDataAdapter::class);
        $adapter->edit(Argument::exact($errorData), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($errorData, $keys);
    }

    public function testFetchOne()
    {
        $id = 1;

        $errorDataAdapter = $this->prophesize(IErrorDataAdapter::class);
        $errorDataAdapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($errorDataAdapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $errorDataAdapter = $this->prophesize(IErrorDataAdapter::class);
        $errorDataAdapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($errorDataAdapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $errorDataAdapter = $this->prophesize(IErrorDataAdapter::class);
        $errorDataAdapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($errorDataAdapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }
}

<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;

use BaseData\WorkOrderTask\Command\ParentTask\AddParentTaskCommand;
use BaseData\WorkOrderTask\Model\ParentTask;

class AddParentTaskCommandHandler implements ICommandHandler
{
    use ParentTaskCommandHandlerTrait;

    protected function getParentTask() : ParentTask
    {
        return new ParentTask();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddParentTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $parentTask = $this->getParentTask();

        $parentTask->setTemplateType($command->templateType);
        $parentTask->setTitle($command->title);
        $parentTask->setDescription($command->description);
        $parentTask->setEndTime($command->endTime);
        $parentTask->setAttachment($command->attachment);
        if ($command->templateType == ParentTask::TEMPLATE_TYPE['GB']) {
            $template = $this->fetchOneGbTemplate($command->template);
        }
        if ($command->templateType == ParentTask::TEMPLATE_TYPE['BJ']) {
            $template = $this->fetchOneBjTemplate($command->template);
        }
        $parentTask->setTemplate($template);

        $assignObjects = $this->fetchListUserGroup($command->assignObjects);
        $parentTask->setAssignObjects($assignObjects);

        if ($parentTask->add()) {
            $command->id = $parentTask->getId();
            return true;
        }
        return false;
    }
}

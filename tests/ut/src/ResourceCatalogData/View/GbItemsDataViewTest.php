<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\GbItemsData;

class GbItemsDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $gbItemsData = new GbItemsDataView(new GbItemsData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $gbItemsData);
    }
}

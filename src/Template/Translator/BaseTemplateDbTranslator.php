<?php
namespace BaseData\Template\Translator;

use BaseData\Template\Model\BaseTemplate;
use BaseData\Template\Model\NullBaseTemplate;

class BaseTemplateDbTranslator extends TemplateDbTranslator
{
    public function arrayToObject(array $expression, $baseTemplate = null) : BaseTemplate
    {
        if (!isset($expression['base_template_id'])) {
            return NullBaseTemplate::getInstance();
        }

        if ($baseTemplate == null) {
            $baseTemplate = new BaseTemplate();
        }

        $baseTemplate->setId($expression['base_template_id']);
        $baseTemplate = parent::arrayToObject($expression, $baseTemplate);

        return $baseTemplate;
    }

    public function objectToArray($baseTemplate, array $keys = array()) : array
    {
        $templateExpression = parent::objectToArray($baseTemplate, $keys);
        
        if (!$baseTemplate instanceof BaseTemplate) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['base_template_id'] = $baseTemplate->getId();
        }

        $expression = array_merge($expression, $templateExpression);

        return $expression;
    }
}

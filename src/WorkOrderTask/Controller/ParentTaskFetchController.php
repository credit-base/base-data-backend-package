<?php
namespace BaseData\WorkOrderTask\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\WorkOrderTask\View\ParentTaskView;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;

class ParentTaskFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ParentTaskRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IParentTaskAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new ParentTaskView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'parentTasks';
    }
}

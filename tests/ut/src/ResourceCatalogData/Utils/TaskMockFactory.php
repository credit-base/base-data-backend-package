<?php
namespace BaseData\ResourceCatalogData\Utils;

use BaseData\Template\Model\Template;
use BaseData\ResourceCatalogData\Model\Task;

class TaskMockFactory
{
    public static function generateTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Task {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $task = new Task($id);
        $task->setId($id);

        //crew
        self::generateCrew($task, $faker, $value);
        //userGroup
        self::generateUserGroup($task, $faker, $value);
        //pid
        self::generatePid($task, $faker, $value);
        //total
        self::generateTotal($task, $faker, $value);
        //successNumber
        self::generateSuccessNumber($task, $faker, $value);
        //failureNumber
        self::generateFailureNumber($task, $faker, $value);
        //sourceCategory
        self::generateSourceCategory($task, $faker, $value);
        //sourceTemplate
        self::generateSourceTemplate($task, $faker, $value);
        //targetCategory
        self::generateTargetCategory($task, $faker, $value);
        //targetTemplate
        self::generateTargetTemplate($task, $faker, $value);
        //targetRule
        self::generateTargetRule($task, $faker, $value);
        //scheduleTask
        self::generateScheduleTask($task, $faker, $value);
        //errorNumber
        self::generateErrorNumber($task, $faker, $value);
        //fileName
        self::generateFileName($task, $faker, $value);
        
        self::generateStatus($task, $faker, $value);

        $task->setCreateTime($faker->unixTime());
        $task->setUpdateTime($faker->unixTime());
        $task->setStatusTime($faker->unixTime());

        return $task;
    }

    protected static function generateFileName($task, $faker, $value) : void
    {
        $fileName = isset($value['fileName']) ?
        $value['fileName'] :
        $faker->name().'xls';
        
        $task->setFileName($fileName);
    }

    protected static function generateCrew($task, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \BaseData\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $task->setCrew($crew);
    }

    protected static function generateUserGroup($task, $faker, $value) : void
    {
        $userGroup = isset($value['userGroup']) ?
        $value['userGroup'] :
        \BaseData\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
        
        $task->setUserGroup($userGroup);
    }

    protected static function generatePid($task, $faker, $value) : void
    {
        $pid = isset($value['pid']) ?
        $value['pid'] :
        $faker->randomDigit();
        
        $task->setPid($pid);
    }

    protected static function generateTotal($task, $faker, $value) : void
    {
        $total = isset($value['total']) ?
        $value['total'] :
        $faker->randomDigit();
        
        $task->setTotal($total);
    }

    protected static function generateSuccessNumber($task, $faker, $value) : void
    {
        $successNumber = isset($value['successNumber']) ?
        $value['successNumber'] :
        $faker->randomDigit();
        
        $task->setSuccessNumber($successNumber);
    }

    protected static function generateFailureNumber($task, $faker, $value) : void
    {
        $failureNumber = isset($value['failureNumber']) ?
        $value['failureNumber'] :
        $faker->randomDigit();
        
        $task->setFailureNumber($failureNumber);
    }

    protected static function generateSourceCategory($task, $faker, $value) : void
    {
        $sourceCategory = isset($value['sourceCategory']) ?
            $value['sourceCategory'] :
            $faker->randomElement(
                Template::CATEGORY
            );
        
        $task->setSourceCategory($sourceCategory);
    }

    protected static function generateSourceTemplate($task, $faker, $value) : void
    {
        $sourceTemplate = isset($value['sourceTemplate']) ?
        $value['sourceTemplate'] :
        $faker->randomDigit();
        
        $task->setSourceTemplate($sourceTemplate);
    }
    
    protected static function generateTargetCategory($task, $faker, $value) : void
    {
        $targetCategory = isset($value['targetCategory']) ?
            $value['targetCategory'] :
            $faker->randomElement(
                Template::CATEGORY
            );
        
        $task->setTargetCategory($targetCategory);
    }

    protected static function generateTargetTemplate($task, $faker, $value) : void
    {
        $targetTemplate = isset($value['targetTemplate']) ?
        $value['targetTemplate'] :
        $faker->randomDigit();
        
        $task->setTargetTemplate($targetTemplate);
    }

    protected static function generateTargetRule($task, $faker, $value) : void
    {
        $targetRule = isset($value['targetRule']) ?
        $value['targetRule'] :
        $faker->randomDigit();
        
        $task->setTargetRule($targetRule);
    }

    protected static function generateScheduleTask($task, $faker, $value) : void
    {
        $scheduleTask = isset($value['scheduleTask']) ?
        $value['scheduleTask'] :
        $faker->randomDigit();
        
        $task->setScheduleTask($scheduleTask);
    }

    protected static function generateErrorNumber($task, $faker, $value) : void
    {
        $errorNumber = isset($value['errorNumber']) ?
        $value['errorNumber'] :
        $faker->randomDigit();
        
        $task->setErrorNumber($errorNumber);
    }

    protected static function generateStatus($task, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                Task::STATUS
            );
        
        $task->setStatus($status);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Repository\YsItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;

class YsItemsData extends ItemsData
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new YsItemsDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IYsItemsDataAdapter
    {
        return $this->repository;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter;

class MockTask extends Task
{
    public function getRepository() : ITaskAdapter
    {
        return parent::getRepository();
    }
}

<?php
namespace BaseData\Template\CommandHandler\BaseTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;
use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;
use BaseData\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class EditBaseTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo;
    
    private $repository;

    public function __construct()
    {
        $this->repository = new BaseTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getBaseTemplateRepository() : IBaseTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditBaseTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getBaseTemplateRepository();
        $baseTemplate = $repository->fetchOne($command->id);

        $baseTemplate->setSubjectCategory($command->subjectCategory);
        $baseTemplate->setDimension($command->dimension);
        $baseTemplate->setExchangeFrequency($command->exchangeFrequency);
        $baseTemplate->setInfoClassify($command->infoClassify);
        $baseTemplate->setInfoCategory($command->infoCategory);
        $items = $this->setDefaultInfo($command);
        $baseTemplate->setItems($items);

        return $baseTemplate->edit();
    }
}

<?php
namespace BaseData\Task\Adapter\NotifyRecord;

use BaseData\Task\Model\NotifyRecord;

interface INotifyRecordAdapter
{
    public function fetchOne($id) : NotifyRecord;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(NotifyRecord $notifyRecord) : bool;
}

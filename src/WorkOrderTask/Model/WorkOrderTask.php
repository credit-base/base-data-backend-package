<?php
namespace BaseData\WorkOrderTask\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use BaseData\Template\Model\Template;
use BaseData\UserGroup\Model\UserGroup;

use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class WorkOrderTask implements IObject, IProgress
{
    use Object, ProgressTrait;

    /**
     * @var IS_EXISTED_TEMPLATE['FOU']  否，默认
     * @var IS_EXISTED_TEMPLATE['SHI']  是
     */
    const IS_EXISTED_TEMPLATE = array(
        'FOU' => 0,
        'SHI' => 1
    );

    /**
     * @var $id
     */
    protected $id;
    /**
     * @var ParentTask $parentTask 父任务
     */
    protected $parentTask;
    /**
     * @var Template $template 指派目录
     */
    protected $template;
    /**
     * @var UserGroup $assignObject 指派对象
     */
    protected $assignObject;
    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->parentTask = new ParentTask();
        $this->template = new Template();
        $this->assignObject = new UserGroup();
        $this->reason = '';
        $this->feedbackRecords = array();

        $this->status = self::STATUS['DQR'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new WorkOrderTaskRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->parentTask);
        unset($this->template);
        unset($this->assignObject);
        unset($this->reason);
        unset($this->feedbackRecords);

        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setParentTask(ParentTask $parentTask) : void
    {
        $this->parentTask = $parentTask;
    }

    public function getParentTask() : ParentTask
    {
        return $this->parentTask;
    }

    public function setTemplate(Template $template) : void
    {
        $this->template = $template;
    }

    public function getTemplate() : Template
    {
        return $this->template;
    }

    public function setAssignObject(UserGroup $assignObject) : void
    {
        $this->assignObject = $assignObject;
    }

    public function getAssignObject() : UserGroup
    {
        return $this->assignObject;
    }

    protected function getRepository() : IWorkOrderTaskAdapter
    {
        return $this->repository;
    }
}

<?php
namespace BaseData\Task\Adapter\NotifyRecord;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\Model\NullNotifyRecord;
use BaseData\Task\Translator\NotifyRecordDbTranslator;
use BaseData\Task\Adapter\NotifyRecord\Query\NotifyRecordRowCacheQuery;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class NotifyRecordDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(NotifyRecordDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction'
                               ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 ITaskAdapter
     */
    public function testImplementsITaskAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 TaskDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockNotifyRecordDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Task\Translator\NotifyRecordDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 TaskRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockNotifyRecordDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\NotifyRecord\Query\NotifyRecordRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockNotifyRecordDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedNotifyRecord = new NotifyRecord();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedNotifyRecord)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedNotifyRecord);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedNotifyRecord = new NotifyRecord();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedNotifyRecord);
        //验证
        $task = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedNotifyRecord, $task);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $taskOne = new NotifyRecord(1);
        $taskTwo = new NotifyRecord(2);

        $ids = [1, 2];

        $expectedNotifyRecordList = [];
        $expectedNotifyRecordList[$taskOne->getId()] = $taskOne;
        $expectedNotifyRecordList[$taskTwo->getId()] = $taskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedNotifyRecordList);

        //验证
        $taskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedNotifyRecordList, $taskList);
    }

    public function testFormatEmptyFilter()
    {
        $adapter = new MockNotifyRecordDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatFilterHash()
    {
        $filter = array(
            'hash' => 'hash'
        );
        $adapter = new MockNotifyRecordDbAdapter();

        $expectedCondition = 'hash = \''.$filter['hash'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockNotifyRecordDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }
}

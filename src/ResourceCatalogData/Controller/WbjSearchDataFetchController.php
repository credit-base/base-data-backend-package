<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\ResourceCatalogData\View\WbjSearchDataView;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class WbjSearchDataFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WbjSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IWbjSearchDataAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new WbjSearchDataView($data);
    }

    protected function getResourceName() : string
    {
        return 'wbjSearchData';
    }
}

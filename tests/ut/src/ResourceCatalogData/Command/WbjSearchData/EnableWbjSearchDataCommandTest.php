<?php
namespace BaseData\ResourceCatalogData\Command\WbjSearchData;

use PHPUnit\Framework\TestCase;

class EnableWbjSearchDataCommandTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new EnableWbjSearchDataCommand(1);
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsEnableCommand()
    {
        $this->assertInstanceof('BaseData\Common\Command\EnableCommand', $this->stub);
    }
}

<?php
namespace BaseData\WorkOrderTask\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullParentTask extends ParentTask implements INull
{
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function add() : bool
    {
        return $this->resourceNotExist();
    }

    public function revoke(string $reason) : bool
    {
        unset($reason);
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use BaseData\ResourceCatalogData\Model\NullGbSearchData;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\View\GbSearchDataView;

class GbSearchDataFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(GbSearchDataFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockGbSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockGbSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\View\GbSearchDataView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockGbSearchDataFetchController();

        $this->assertEquals(
            'gbSearchData',
            $controller->getResourceName()
        );
    }
}

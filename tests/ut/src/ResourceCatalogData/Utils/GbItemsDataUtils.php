<?php
namespace BaseData\ResourceCatalogData\Utils;

trait GbItemsDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $gbItemsData
    ) {
        $this->assertEquals($expectedArray['gb_items_data_id'], $gbItemsData->getId());

        $data = array();
        if (is_string($expectedArray['data'])) {
            $data = unserialize(gzuncompress(base64_decode($expectedArray['data'], true)));
        }
        if (is_array($expectedArray['data'])) {
            $data = $expectedArray['data'];
        }

        $this->assertEquals($data, $gbItemsData->getData());
    }
}

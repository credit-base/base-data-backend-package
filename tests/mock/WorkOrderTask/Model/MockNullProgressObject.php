<?php
namespace BaseData\WorkOrderTask\Model;

class MockNullProgressObject
{
    use NullProgressTrait;

    public function publicResourceNotExist() : bool
    {
        return $this->resourceNotExist();
    }
}

<?php
namespace BaseData\Template\GbTemplate\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\GbTemplate;

/**
 * 测试国标资源目录接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在一条国标资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate()
            ]
        );
    }

    public function testViewGbTemplate()
    {
        $items = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['GTGSH']]],
            [Template::DIMENSION_CN[Template::DIMENSION['SHGK']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['MR']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['QT']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['JCXX']]]
        );

        $data = array(
            "data" => array(
                "type" => "gbTemplates",
                "attributes" => array(
                    "name" => '国标企业基本信息编辑',
                    "identify" => 'GBQYJBXXBJ',
                    "subjectCategory" => array(Template::SUBJECT_CATEGORY['GTGSH']),
                    "dimension" => Template::DIMENSION['SHGK'],
                    "exchangeFrequency" => Template::EXCHANGE_FREQUENCY['MR'],
                    "infoClassify" => Template::INFO_CLASSIFY['QT'],
                    "infoCategory" => Template::INFO_CATEGORY['JCXX'],
                    "description" => "国标企业基本信息编辑",
                    'items' => $items
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 2)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'gbTemplates/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals('国标企业基本信息编辑', $attributes['name']);
        $this->assertEquals('GBQYJBXXBJ', $attributes['identify']);
        $this->assertEquals(array(Template::SUBJECT_CATEGORY['GTGSH']), $attributes['subjectCategory']);
        $this->assertEquals(Template::DIMENSION['SHGK'], $attributes['dimension']);
        $this->assertEquals(Template::EXCHANGE_FREQUENCY['MR'], $attributes['exchangeFrequency']);
        $this->assertEquals(Template::INFO_CLASSIFY['QT'], $attributes['infoClassify']);
        $this->assertEquals(Template::INFO_CATEGORY['JCXX'], $attributes['infoCategory']);
        $this->assertEquals($items, $attributes['items']);

        $this->assertEquals('国标企业基本信息编辑', $attributes['description']);
    }
}

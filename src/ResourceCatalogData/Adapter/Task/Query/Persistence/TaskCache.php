<?php
namespace BaseData\ResourceCatalogData\Adapter\Task\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class TaskCache extends Cache
{
    const KEY = 'task';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\Rule\Adapter\RuleService;

use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\RuleService;

class RuleServiceMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new RuleServiceMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new RuleService()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new RuleService(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\RuleService',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Rule\Model\RuleService',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Rule\Model\RuleService',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

<?php
namespace BaseData\Rule\RuleService\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\TransformationRule;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Translator\RuleServiceDbTranslator;

use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Repository\WBjTemplateRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限，且当我们需要把数据从某一个资源目录导入到另一个资源目录时,
 *           在政务网OA的资源目录规则管理中,可以设置导入的规则
 *           设置转换、补全、比对、去重、确认规则提交给发改委审核，审核通过将可以将数据依照规则导入到目标资源目录中,以便于我给发改委的数据是符合发改委要求的
 * @Scenario: 添加资源目录的规则
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_wbj_template');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_wbj_template' => $this->wbjTemplate()
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $ruleArray = $this->rule()[0];

        $bjTemplateRepository = new BjTemplateRepository();
        $bjTemplate = $bjTemplateRepository->fetchOne($ruleArray['transformation_template_id']);

        $wbjTemplateRepository = new WbjTemplateRepository();
        $wbjTemplate = $wbjTemplateRepository->fetchOne($ruleArray['source_template_id']);

        $transformationRule = new TransformationRule();
        $transformationRule->setCondition(array('ZTMC'=>'ZTMC', 'TYSHXYDM' => 'TYSHXYDM'));
        $transformationRule->setTransformationTemplate($bjTemplate);
        $transformationRule->setSourceTemplate($wbjTemplate);
        $rules = array(IRule::RULE_NAME['TRANSFORMATION_RULE'] => $transformationRule);

        $rule = new RuleService();
        $rule->setSourceCategory($ruleArray['source_category']);
        $rule->setTransformationCategory($ruleArray['transformation_category']);
        $rule->setTransformationTemplate($bjTemplate);
        $rule->setSourceTemplate($wbjTemplate);
        $rule->getCrew()->setId($ruleArray['crew_id']);
        $rule->getUserGroup()->setId($ruleArray['usergroup_id']);
        $rule->setVersion($ruleArray['version']);
        $rule->setDataTotal($ruleArray['data_total']);
        $rule->setStatus($ruleArray['status']);
        $rule->setRules($rules);

        return $rule->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setRule = $this->rule()[0];
        $setRule['rule_id'] = 1;
        
        $repository = new RuleServiceRepository();

        $rule = $repository->fetchOne($result);

        $translator = new RuleServiceDbTranslator();
        $ruleArray = $translator->objectToArray($rule);

        $this->assertEquals($ruleArray, $setRule);
    }
}

<?php
namespace BaseData\Template\Repository;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;

class MockBjTemplateRepository extends BjTemplateRepository
{
    public function getAdapter() : IBjTemplateAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IBjTemplateAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IBjTemplateAdapter
    {
        return parent::getMockAdapter();
    }
}

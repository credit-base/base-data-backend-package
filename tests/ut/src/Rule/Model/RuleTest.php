<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\Template;

/**
 * BaseData\Rule\Model\Rule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class RuleTest extends TestCase
{
    private $rule;

    public function setUp()
    {
        $this->rule = new MockRule();
    }

    public function tearDown()
    {
        unset($this->rule);
    }

    /**
     * 测试构造函数
     */
    public function testConstructor()
    {
        $this->assertEquals([], $this->rule->getCondition());

        $this->assertInstanceof(
            'BaseData\Template\Model\Template',
            $this->rule->getTransformationTemplate()
        );

        $this->assertInstanceof(
            'BaseData\Template\Model\Template',
            $this->rule->getSourceTemplate()
        );
    }

    //condition -- 开始
    /**
     * 设置 Task setCondition() 正确的传参类型,期望传值正确
     */
    public function testSetConditionCorrectType()
    {
        $condition = [];

        $this->rule->setCondition($condition);
        $this->assertEquals($condition, $this->rule->getCondition());
    }
    /**
     * 设置 Task setCondition() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetConditionWrongType()
    {
        $this->rule->setCondition('condition');
    }
    //condition -- 结束

    //transformationTemplate -- 开始
    /**
     * 设置 Task setTransformationTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetTransformationTemplateCorrectType()
    {
        $template = new Template();

        $this->rule->setTransformationTemplate($template);
        $this->assertEquals($template, $this->rule->getTransformationTemplate());
    }
    /**
     * 设置 Task setTransformationTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTransformationTemplateWrongType()
    {
        $this->rule->setTransformationTemplate('template');
    }
    //transformationTemplate -- 结束

    //sourceTemplate -- 开始
    /**
     * 设置 Task setSourceTemplate() 正确的传参类型,期望传值正确
     */
    public function testSetSourceTemplateCorrectType()
    {
        $template = new Template();

        $this->rule->setSourceTemplate($template);
        $this->assertEquals($template, $this->rule->getSourceTemplate());
    }
    /**
     * 设置 Task setSourceTemplate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceTemplateWrongType()
    {
        $this->rule->setSourceTemplate('template');
    }
    //sourceTemplate -- 结束

    public function testTransformationValidateFalse()
    {
        $this->rule = $this->getMockBuilder(MockRule::class)
            ->setMethods([
                'validateTransformation'
            ])
            ->getMock();
            
        $sourceSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);

        $this->rule->expects($this->once())
            ->method('validateTransformation')
            ->with($transformationSearchData)
            ->willReturn(false);

        $result = $this->rule->transformation($sourceSearchData, $transformationSearchData);

        $this->assertEquals($result, $transformationSearchData);
    }
    
    public function testTransformation()
    {
        $this->rule = $this->getMockBuilder(MockRule::class)
            ->setMethods([
                'validateTransformation',
                'transformationInitialization',
                'transformationExecute',
                'transformationResult'
            ])
            ->getMock();
            
        $sourceSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);

        $transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory::generateBjSearchData(1);
        
        $initializationData = array('initializationData');
        $data = array('data');
        
        $this->rule->expects($this->once())
            ->method('validateTransformation')
            ->with($transformationSearchData)
            ->willReturn(true);

        $this->rule->expects($this->once())
            ->method('transformationInitialization')
            ->with($sourceSearchData, $transformationSearchData)
            ->willReturn($initializationData);

        $this->rule->expects($this->once())
            ->method('transformationExecute')
            ->with($initializationData, $transformationSearchData)
            ->willReturn($data);

        $this->rule->expects($this->once())
            ->method('transformationResult')
            ->with($data, $transformationSearchData)
            ->willReturn($transformationSearchData);

        $result = $this->rule->transformation($sourceSearchData, $transformationSearchData);

        $this->assertEquals($result, $transformationSearchData);
    }
}

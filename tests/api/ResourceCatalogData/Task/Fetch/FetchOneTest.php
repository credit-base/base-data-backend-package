<?php
namespace BaseData\ResourceCatalogData\Task\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

/**
 * 测试任务接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
    }

    /**
     * 存在一条任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_task' => $this->task()
            ]
        );
    }

    public function testViewTask()
    {
        $setTask = $this->task()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'tasks/1',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->taskCompareData($setTask, $contents['data']);
    }
}

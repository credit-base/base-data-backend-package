<?php
namespace BaseData\Template\Controller;

use Marmot\Core;
use Marmot\Framework\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\View\WbjTemplateView;
use BaseData\Template\Command\WbjTemplate\AddWbjTemplateCommand;
use BaseData\Template\Command\WbjTemplate\EditWbjTemplateCommand;

class WbjTemplateOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, WbjTemplateControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 委办局资源目录新增功能,通过POST传参
     * 对应路由 /templates
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];
        $description = $attributes['description'];
        $items = $attributes['items'];
        $sourceUnit = $relationships['sourceUnit']['data'][0]['id'];

        //验证
        if ($this->validateOperateScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit
        )) {
            //初始化命令
            $commandBus = $this->getCommandBus();
            $command = new AddWbjTemplateCommand(
                $name,
                $identify,
                $subjectCategory,
                $dimension,
                $exchangeFrequency,
                $infoClassify,
                $infoCategory,
                $description,
                $items,
                $sourceUnit
            );

            //执行命令
            if ($commandBus->send($command)) {
                //获取最新数据
                $repository = $this->getRepository();
                $wbjTemplate = $repository->fetchOne($command->id);
                if ($wbjTemplate instanceof WbjTemplate) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new WbjTemplateView($wbjTemplate));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 委办局资源目录编辑功能,通过PATCH传参
     * 对应路由 /templates/{id:\d+}
     * @param int id 委办局资源目录 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];
        $description = $attributes['description'];
        $items = $attributes['items'];
        $sourceUnit = $relationships['sourceUnit']['data'][0]['id'];

        //验证
        if ($this->validateOperateScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $sourceUnit
        )) {
            $commandBus = $this->getCommandBus();

            $command = new EditWbjTemplateCommand(
                $name,
                $identify,
                $subjectCategory,
                $dimension,
                $exchangeFrequency,
                $infoClassify,
                $infoCategory,
                $description,
                $items,
                $sourceUnit,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $wbjTemplate = $repository->fetchOne($id);
                if ($wbjTemplate instanceof WbjTemplate) {
                    $this->render(new WbjTemplateView($wbjTemplate));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function validateOperateScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items,
        $sourceUnit
    ) : bool {
        return $this->getTemplateWidgetRule()->name($name)
        && $this->getCommonWidgetRule()->formatNumeric($sourceUnit, 'sourceUnit')
        && $this->getTemplateWidgetRule()->identify($identify)
        && $this->getTemplateWidgetRule()->subjectCategory($subjectCategory)
        && $this->getTemplateWidgetRule()->dimension($dimension)
        && $this->getCommonWidgetRule()->formatNumeric($exchangeFrequency, 'exchangeFrequency')
        && $this->getCommonWidgetRule()->formatNumeric($infoClassify, 'infoClassify')
        && $this->getCommonWidgetRule()->formatNumeric($infoCategory, 'infoCategory')
        && $this->getTemplateWidgetRule()->description($description)
        && $this->getTemplateWidgetRule()->items($subjectCategory, $items);
    }
}

<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\Repository\BjTemplateRepository;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\BjItemsData;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;

/**
 * @Feature: 我是开发人员,当委办局工作人员上传资源目录数据后,在资源目录数据子系统下的本级数据管理,开发定时任务新增本级数据
 *           根据委办局数据库的资源目录数据通过本级规则验证新增本级数据
 *           以便于我实现资源目录数据的报送和平台资源目录数据的呈现
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
            'pcore_bj_search_data' => $this->bjSearchData()['pcore_bj_search_data']
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回false
     */
    public function add()
    {
        $templateRepository = new BjTemplateRepository();
        $bjTemplate = $templateRepository->fetchOne(1);

        $bjItemsDataArray = $this->bjSearchData()['pcore_bj_items_data'][0];
        $bjSearchDataArray = $this->bjSearchData()['pcore_bj_search_data'][0];

        $bjItemsData = new BjItemsData();
        $bjItemsData->setData(unserialize(gzuncompress(base64_decode($bjItemsDataArray['data'], true))));

        $bjSearchData = new BjSearchData();

        $bjSearchData->getCrew()->setId($bjSearchDataArray['crew_id']);
        $bjSearchData->getSourceUnit()->setId($bjSearchDataArray['usergroup_id']);
        $bjSearchData->setSubjectCategory($bjSearchDataArray['subject_category']);
        $bjSearchData->setDimension($bjSearchDataArray['dimension']);
        $bjSearchData->setExpirationDate($bjSearchDataArray['expiration_date']);
        $bjSearchData->setTemplate($bjTemplate);
        $bjSearchData->setItemsData($bjItemsData);
        $bjSearchData->setHash($bjSearchDataArray['hash']);
        
        return $bjSearchData->add();
    }

    /**
     * @Then: 数据已经存在
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}

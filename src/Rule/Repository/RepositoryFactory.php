<?php
namespace BaseData\Rule\Repository;

use BaseData\Template\Model\Template;

use BaseData\Common\Repository\NullRepository;

class RepositoryFactory
{
    const MAPS = array(
        Template::CATEGORY['BJ'] =>
        'BaseData\Template\Repository\BjTemplateRepository',
        Template::CATEGORY['GB'] =>
        'BaseData\Template\Repository\GbTemplateRepository',
        Template::CATEGORY['BASE'] =>
        'BaseData\Template\Repository\BaseTemplateRepository',
        Template::CATEGORY['WBJ'] =>
        'BaseData\Template\Repository\WbjTemplateRepository',
    );

    public function getRepository(int $category)
    {
        $repository = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($repository) ? new $repository : NullRepository::getInstance();
    }
}

<?php
namespace BaseData\WorkOrderTask\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;
use BaseData\WorkOrderTask\Model\ParentTask;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Model\NullWorkOrderTask;

class WorkOrderTaskDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $workOrderTask = null) : WorkOrderTask
    {
        if (!isset($expression['work_order_task_id'])) {
            return NullWorkOrderTask::getInstance();
        }

        if ($workOrderTask == null) {
            $workOrderTask = new WorkOrderTask($expression['work_order_task_id']);
        }

        $workOrderTask->setParentTask(new ParentTask($expression['parent_task_id']));
        $workOrderTask->getParentTask()->setTitle($expression['title']);
        $workOrderTask->getParentTask()->setEndTime($expression['end_time']);
        $workOrderTask->getParentTask()->setTemplateType($expression['template_type']);

        if ($expression['template_type'] == ParentTask::TEMPLATE_TYPE['GB']) {
            $workOrderTask->setTemplate(new GbTemplate($expression['template_id']));
            $workOrderTask->getParentTask()->setTemplate(new GbTemplate($expression['template_id']));
        }
        if ($expression['template_type'] == ParentTask::TEMPLATE_TYPE['BJ']) {
            $workOrderTask->setTemplate(new BjTemplate($expression['template_id']));
            $workOrderTask->getParentTask()->setTemplate(new BjTemplate($expression['template_id']));
        }
        $workOrderTask->getTemplate()->setName($expression['template_name']);

        $workOrderTask->setAssignObject(new UserGroup($expression['assign_object_id']));
        $workOrderTask->getAssignObject()->setName($expression['assign_object_name']);

        $workOrderTask->setReason($expression['reason']);
        $workOrderTask->setFeedbackRecords(json_decode($expression['feedback_records'], true));

        $workOrderTask->setStatus($expression['status']);
        $workOrderTask->setCreateTime($expression['create_time']);
        $workOrderTask->setUpdateTime($expression['update_time']);
        $workOrderTask->setStatusTime($expression['status_time']);

        return $workOrderTask;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($workOrderTask, array $keys = array()) : array
    {
        if (!$workOrderTask instanceof WorkOrderTask) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'endTime',
                'reason',
                'feedbackRecords',
                'templateType',
                'template',
                'templateName',
                'assignObject',
                'assignObjectName',
                'parentTask',

                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['work_order_task_id'] = $workOrderTask->getId();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $workOrderTask->getParentTask()->getTitle();
        }
        if (in_array('endTime', $keys)) {
            $expression['end_time'] = $workOrderTask->getParentTask()->getEndTime();
        }
        if (in_array('reason', $keys)) {
            $expression['reason'] = $workOrderTask->getReason();
        }
        if (in_array('feedbackRecords', $keys)) {
            $expression['feedback_records'] = $workOrderTask->getFeedbackRecords();
        }
        if (in_array('templateType', $keys)) {
            $expression['template_type'] = $workOrderTask->getParentTask()->getTemplateType();
        }
        if (in_array('template', $keys)) {
            $expression['template_id'] = $workOrderTask->getTemplate()->getId();
        }
        if (in_array('templateName', $keys)) {
            $expression['template_name'] = $workOrderTask->getTemplate()->getName();
        }
        if (in_array('assignObject', $keys)) {
            $expression['assign_object_id'] = $workOrderTask->getAssignObject()->getId();
        }
        if (in_array('assignObjectName', $keys)) {
            $expression['assign_object_name'] = $workOrderTask->getAssignObject()->getName();
        }
        if (in_array('parentTask', $keys)) {
            $expression['parent_task_id'] = $workOrderTask->getParentTask()->getId();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $workOrderTask->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $workOrderTask->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $workOrderTask->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $workOrderTask->getStatusTime();
        }
        
        return $expression;
    }
}

<?php
namespace BaseData\Template\Adapter\BjTemplate\Query;

use Marmot\Framework\Query\RowCacheQuery;

class BjTemplateRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'bj_template_id',
            new Persistence\BjTemplateCache(),
            new Persistence\BjTemplateDb()
        );
    }
}

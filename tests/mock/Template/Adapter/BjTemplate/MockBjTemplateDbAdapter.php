<?php
namespace BaseData\Template\Adapter\BjTemplate;

use Marmot\Interfaces\INull;

use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Translator\BjTemplateDbTranslator;
use BaseData\Template\Adapter\BjTemplate\Query\BjTemplateRowCacheQuery;

use BaseData\UserGroup\Repository\UserGroupRepository;

class MockBjTemplateDbAdapter extends BjTemplateDbAdapter
{
    public function getDbTranslator() : BjTemplateDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : BjTemplateRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getGbTemplateRepository() : GbTemplateRepository
    {
        return parent::getGbTemplateRepository();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function fetchGbTemplate($bjTemplate)
    {
        return parent::fetchGbTemplate($bjTemplate);
    }

    public function fetchGbTemplateByObject(BjTemplate $bjTemplate)
    {
        return parent::fetchGbTemplateByObject($bjTemplate);
    }

    public function fetchGbTemplateByList(array $bjTemplateList)
    {
        return parent::fetchGbTemplateByList($bjTemplateList);
    }

    public function fetchSourceUnit($bjTemplate)
    {
        return parent::fetchSourceUnit($bjTemplate);
    }

    public function fetchSourceUnitByObject(BjTemplate $bjTemplate)
    {
        return parent::fetchSourceUnitByObject($bjTemplate);
    }

    public function fetchSourceUnitByList(array $bjTemplateList)
    {
        return parent::fetchSourceUnitByList($bjTemplateList);
    }
}

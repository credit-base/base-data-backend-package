<?php
namespace BaseData\Template\Model;

use BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;

class MockQzjTemplate extends QzjTemplate
{
    public function getRepository() : IQzjTemplateAdapter
    {
        return parent::getRepository();
    }

    public function isTemplateExist() : bool
    {
        return parent::isTemplateExist();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }
}

<?php
namespace BaseData\WorkOrderTask\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;
use BaseData\UserGroup\Model\UserGroup;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Model\NullParentTask;

class ParentTaskDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $parentTask = null) : ParentTask
    {
        if (!isset($expression['parent_task_id'])) {
            return NullParentTask::getInstance();
        }

        if ($parentTask == null) {
            $parentTask = new ParentTask($expression['parent_task_id']);
        }

        $parentTask->setTemplateType($expression['template_type']);
        if ($expression['template_type'] == ParentTask::TEMPLATE_TYPE['GB']) {
            $parentTask->setTemplate(new GbTemplate($expression['template_id']));
        }
        if ($expression['template_type'] == ParentTask::TEMPLATE_TYPE['BJ']) {
            $parentTask->setTemplate(new BjTemplate($expression['template_id']));
        }
        $parentTask->getTemplate()->setName($expression['template_name']);

        $parentTask->setTitle($expression['title']);
        $parentTask->setDescription($expression['description']);
        $parentTask->setEndTime($expression['end_time']);
        $parentTask->setAttachment(json_decode($expression['attachment'], true));

        $assignObjects = array();
        $assignObjectIds = json_decode($expression['assign_objects'], true);
        foreach ($assignObjectIds as $id) {
            $assignObjects[] = new UserGroup($id);
        }
        $parentTask->setAssignObjects($assignObjects);

        $parentTask->setFinishCount($expression['finish_count']);
        $parentTask->setReason($expression['reason']);

        $parentTask->setStatus($expression['status']);
        $parentTask->setCreateTime($expression['create_time']);
        $parentTask->setUpdateTime($expression['update_time']);
        $parentTask->setStatusTime($expression['status_time']);

        return $parentTask;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($parentTask, array $keys = array()) : array
    {
        if (!$parentTask instanceof ParentTask) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'templateType',
                'title',
                'description',
                'endTime',
                'attachment',
                'template',
                'templateName',
                'assignObjects',
                'finishCount',
                'reason',

                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['parent_task_id'] = $parentTask->getId();
        }
        if (in_array('templateType', $keys)) {
            $expression['template_type'] = $parentTask->getTemplateType();
        }
        if (in_array('title', $keys)) {
            $expression['title'] = $parentTask->getTitle();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $parentTask->getDescription();
        }
        if (in_array('endTime', $keys)) {
            $expression['end_time'] = $parentTask->getEndTime();
        }
        if (in_array('attachment', $keys)) {
            $expression['attachment'] = $parentTask->getAttachment();
        }
        if (in_array('template', $keys)) {
            $expression['template_id'] = $parentTask->getTemplate()->getId();
        }
        if (in_array('templateName', $keys)) {
            $expression['template_name'] = $parentTask->getTemplate()->getName();
        }
        if (in_array('assignObjects', $keys)) {
            $assignObjectIds = array();
            foreach ($parentTask->getAssignObjects() as $assignObject) {
                $assignObjectIds[] = $assignObject->getId();
            }
            $expression['assign_objects'] = $assignObjectIds;
        }
        if (in_array('finishCount', $keys)) {
            $expression['finish_count'] = $parentTask->getFinishCount();
        }
        if (in_array('reason', $keys)) {
            $expression['reason'] = $parentTask->getReason();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $parentTask->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $parentTask->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $parentTask->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $parentTask->getStatusTime();
        }
        
        return $expression;
    }
}

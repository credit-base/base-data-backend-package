<?php
namespace BaseData\ResourceCatalogData\ErrorData\Search;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;
use BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要了解我数据上传数据的原因时,在资源目录数据子系统下的上传任务列表中,点击失败数据可以查看到我的所有上传失败的数据,
 *           通过列表形式呈现 ,以便于我可以更快的修复错误数据并重新上传
 * @Scenario: 委办局工作人员通过资源目录搜索
 */
class TemplateTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_error_data');
    }

    /**
     * @Given: 存在一条失败资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_error_data' => $this->errorData()
        ]);
    }
    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchErrorDataList()
    {
        $repository = new ErrorDataRepository();
 
        $filter['template'] = 1;

        list($errorDataList, $count) = $repository->filter($filter);
        
        unset($count);

        return $errorDataList;
    }

    /**
     * @Then: 我可以看到资源目录Id为1的所有资源目录数据
     */
    public function testViewErrorDataList()
    {
        $list = $this->errorData();

        foreach ($list as $key => $value) {
            $list[$key]['error_data_id'] = $key +1;
            $list[$key]['error_reason'] = json_decode($value['error_reason'], true);
        }

        $errorDataList = $this->fetchErrorDataList();
        $translator = new ErrorDataDbTranslator();
        foreach ($errorDataList as $errorData) {
            $errorDataArray[] = $translator->objectToArray($errorData);
        }
        
        $this->assertEquals($errorDataArray, $list);

        foreach ($errorDataArray as $errorData) {
            $this->assertEquals(1, $errorData['template_id']);
        }
    }
}

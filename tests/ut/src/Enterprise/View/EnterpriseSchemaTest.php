<?php
namespace BaseData\Enterprise\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class EnterpriseSchemaTest extends TestCase
{
    private $enterpriseSchema;

    private $enterprise;

    public function setUp()
    {
        $this->enterpriseSchema = new EnterpriseSchema(new Factory());

        $this->enterprise = \BaseData\Enterprise\Utils\MockFactory::generateEnterprise(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->enterpriseSchema);
        unset($this->enterprise);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->enterpriseSchema);
    }

    public function testGetId()
    {
        $result = $this->enterpriseSchema->getId($this->enterprise);

        $this->assertEquals($result, $this->enterprise->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->enterpriseSchema->getAttributes($this->enterprise);

        $this->assertEquals($result['name'], $this->enterprise->getName());
        $this->assertEquals($result['unifiedSocialCreditCode'], $this->enterprise->getUnifiedSocialCreditCode());
        $this->assertEquals($result['establishmentDate'], $this->enterprise->getEstablishmentDate());
        $this->assertEquals($result['approvalDate'], $this->enterprise->getApprovalDate());
        $this->assertEquals($result['address'], $this->enterprise->getAddress());
        $this->assertEquals($result['registrationCapital'], $this->enterprise->getRegistrationCapital());
        $this->assertEquals($result['businessTermStart'], $this->enterprise->getBusinessTermStart());
        $this->assertEquals($result['businessTermTo'], $this->enterprise->getBusinessTermTo());
        $this->assertEquals($result['businessScope'], $this->enterprise->getBusinessScope());
        $this->assertEquals($result['registrationAuthority'], $this->enterprise->getRegistrationAuthority());
        $this->assertEquals($result['principal'], $this->enterprise->getPrincipal());
        $this->assertEquals($result['principalCardId'], $this->enterprise->getPrincipalCardId());
        $this->assertEquals($result['registrationStatus'], $this->enterprise->getRegistrationStatus());
        $this->assertEquals($result['enterpriseTypeCode'], $this->enterprise->getEnterpriseTypeCode());
        $this->assertEquals($result['enterpriseType'], $this->enterprise->getEnterpriseType());
        $this->assertEquals($result['data'], $this->enterprise->getData());
        $this->assertEquals($result['industryCategory'], $this->enterprise->getIndustryCategory());
        $this->assertEquals($result['industryCode'], $this->enterprise->getIndustryCode());
        $this->assertEquals($result['administrativeArea'], $this->enterprise->getAdministrativeArea());
        $this->assertEquals($result['status'], $this->enterprise->getStatus());
        $this->assertEquals($result['hash'], $this->enterprise->getHash());
        $this->assertEquals($result['taskId'], $this->enterprise->getTask()->getId());
        $this->assertEquals($result['createTime'], $this->enterprise->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->enterprise->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->enterprise->getStatusTime());
    }
}

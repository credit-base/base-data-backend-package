<?php
namespace BaseData\Task\Script;

use BaseData\Task\Model\OnceTask;

class OnceExample extends OnceTask
{
    public function executeAction() : bool
    {
        //echo 'run once', PHP_EOL;

        sleep(2);
        return true;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Model\IEnableAble;

use BaseData\Template\Model\BaseTemplate;
use BaseData\Template\Model\NullBaseTemplate;

use BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class BaseSearchDataTest extends TestCase
{
    private $baseSearchData;

    private $faker;

    public function setUp()
    {
        $this->baseSearchData = new MockBaseSearchData();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->baseSearchData);
        unset($this->faker);
    }

    public function testImplementsIEnableAble()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IEnableAble',
            $this->baseSearchData
        );
    }
    
    public function testSearchDataConstructor()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\BaseTemplate',
            $this->baseSearchData->getTemplate()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\BaseItemsData',
            $this->baseSearchData->getItemsData()
        );
    }

    //itemsData 测试 ---------------------------------------------------- start
    /**
     * 设置 setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $itemsData = new BaseItemsData();

        $this->baseSearchData->setItemsData($itemsData);
        $this->assertEquals($itemsData, $this->baseSearchData->getItemsData());
    }

    /**
     * 设置 setItemsData() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $itemsData = array($this->faker->randomNumber());

        $this->baseSearchData->setItemsData($itemsData);
    }
    //itemsData 测试 ----------------------------------------------------   end

    public function testGetRepository()
    {
        $baseSearchData = new MockBaseSearchData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter',
            $baseSearchData->getRepository()
        );
    }

    public function testAdd()
    {
        $baseSearchData = $this->getMockBuilder(MockBaseSearchData::class)
                           ->setMethods(['assignment', 'validate', 'getRepository'])
                           ->getMock();

        $baseSearchData->expects($this->once())->method('assignment');
        $baseSearchData->expects($this->once())->method('validate') ->willReturn(true);

        $repository = $this->prophesize(IBaseSearchDataAdapter::class);
        $repository->add(Argument::exact($baseSearchData))->shouldBeCalledTimes(1)->willReturn(true);

        $baseSearchData->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $result = $baseSearchData->add();

        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Rule\Model;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

interface IRule
{
    const TYPE = array(
        "BJ" => 1,
        "GB" => 2
    );
    
    const RULE_NAME = array(
        'TRANSFORMATION_RULE' => 'transformationRule', //转换规则
        'COMPLETION_RULE' => 'completionRule',//补全规则
        'COMPARISON_RULE' => 'comparisonRule',//比对规则
        'DE_DUPLICATION_RULE' => 'deDuplicationRule', //去重规则
        'FORMAT_VALIDATION_RULE' => 'formatValidationRule', //格式验证规则
    );
    
    const COMPLETION_BASE = array(
        'NAME' => 1, //企业名称
        'IDENTIFY' => 2, //统一社会信用代码/身份证号
    );
    
    const COMPARISON_BASE = array(
        'NAME' => 1, //企业名称
        'IDENTIFY' => 2, //统一社会信用代码/身份证号
    );

    const DE_DUPLICATION_RESULT = array(
        'DISCARD' => 1, //丢弃
        'COVER' => 2, //覆盖
    );

    const RULE_PRIORITY = array(
        self::RULE_NAME['FORMAT_VALIDATION_RULE'],
        self::RULE_NAME['TRANSFORMATION_RULE'],
        self::RULE_NAME['COMPLETION_RULE'],
        self::RULE_NAME['COMPARISON_RULE'],
        self::RULE_NAME['DE_DUPLICATION_RULE'],
    );

    const TEMPLATE_ITEM_TYPE_MAPPING = array(
        Template::TYPE['ZFX'] => Template::TYPE,
        Template::TYPE['RQX'] => array(Template::TYPE['RQX']),
        Template::TYPE['ZSX'] => array(Template::TYPE['ZSX']),
        Template::TYPE['FDX'] => array(Template::TYPE['FDX'], Template::TYPE['ZSX']),
        Template::TYPE['MJX'] => array(Template::TYPE['MJX']),
        Template::TYPE['JHX'] => array(Template::TYPE['JHX'], Template::TYPE['MJX']),
    );

    public function transformation(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble;
}

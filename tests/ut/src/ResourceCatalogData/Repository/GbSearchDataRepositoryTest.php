<?php
namespace BaseData\ResourceCatalogData\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataDbAdapter;

use BaseData\ResourceCatalogData\Utils\GbSearchDataMockFactory;

class GbSearchDataRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(GbSearchDataRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockGbSearchDataRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new GbSearchDataDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData($id);
        $keys = array();
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->add(Argument::exact($gbSearchData))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($gbSearchData, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $gbSearchData = GbSearchDataMockFactory::generateGbSearchData($id);
        $keys = array();
        
        $adapter = $this->prophesize(IGbSearchDataAdapter::class);
        $adapter->edit(Argument::exact($gbSearchData), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($gbSearchData, $keys);
    }
}

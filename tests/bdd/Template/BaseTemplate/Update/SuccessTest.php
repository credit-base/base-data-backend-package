<?php
namespace BaseData\Template\BaseTemplate\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Translator\BaseTemplateDbTranslator;

/**
 * @Feature: 我是超级管理员,当我之前的基础目录需要更改时,在目录管理子系统,修改基础目录内容
 *           根据我需要 修改的内容和规范修改原有基础目录
 *           以便于我有基础目录可以使用
 * @Scenario: 编辑资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_base_template' => $this->baseTemplate(),
        ]);
    }

    /**
     * @When: 获取需要编辑的用户数据
     */
    public function fetchTemplate($id)
    {
        $repository = new BaseTemplateRepository();

        $baseTemplate = $repository->fetchOne($id);

        return $baseTemplate;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $baseTemplate = $this->fetchTemplate(1);

        $baseTemplate->setItems(array('items'));
        
        return $baseTemplate->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $baseTemplate = $this->fetchTemplate(1);

        $this->assertEquals(array('items'), $baseTemplate->getItems());
    }
}

<?php
namespace BaseData\WorkOrderTask\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\WorkOrderTask\View\WorkOrderTaskView;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class WorkOrderTaskFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WorkOrderTaskRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IWorkOrderTaskAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new WorkOrderTaskView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'workOrderTasks';
    }
}

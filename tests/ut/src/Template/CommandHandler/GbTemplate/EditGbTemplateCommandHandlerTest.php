<?php
namespace BaseData\Template\CommandHandler\GbTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Command\GbTemplate\EditGbTemplateCommand;

class EditGbTemplateCommandHandlerTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->commandHandler = $this->getMockBuilder(EditGbTemplateCommandHandler::class)
                                     ->setMethods(['getGbTemplateRepository'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->commandHandler);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testGetGbTemplateRepository()
    {
        $commandHandler = new MockEditGbTemplateCommandHandler();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\GbTemplateRepository',
            $commandHandler->getGbTemplateRepository()
        );
    }

    public function testExecuteSuccess()
    {
        $command = $this->initCommand();
        $gbTemplate = $this->initGbTemplate($command, true);

        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($gbTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getGbTemplateRepository')
            ->willReturn($gbTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertTrue($result);
    }

    public function testExecuteFail()
    {
        $command = $this->initCommand();
        $gbTemplate = $this->initGbTemplate($command, false);

        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchOne(Argument::exact($command->id))
                                ->shouldBeCalledTimes(1)
                                ->willReturn($gbTemplate->reveal());
        $this->commandHandler->expects($this->any())
            ->method('getGbTemplateRepository')
            ->willReturn($gbTemplateRepository->reveal());

        $result = $this->commandHandler->execute($command);
        $this->assertFalse($result);
    }

    private function initCommand() : ICommand
    {
        $command = new EditGbTemplateCommand(
            $this->faker->word,
            $this->faker->regexify('[A-Z_]{1,100}'),
            $this->faker->randomElements(Template::SUBJECT_CATEGORY, 2),
            $this->faker->randomElement(Template::DIMENSION),
            $this->faker->randomElement(Template::EXCHANGE_FREQUENCY),
            $this->faker->randomElement(Template::INFO_CLASSIFY),
            $this->faker->randomElement(Template::INFO_CATEGORY),
            $this->faker->sentence,
            $this->faker->randomElements(
                array(
                    array(
                        "name" => '日期',    //信息项名称
                        "identify" => 'RQ',    //数据标识
                        "type" => Template::TYPE['RQX'],    //数据类型
                        "length" => '8',    //数据长度
                    )
                ),
                1
            ),
            $this->faker->randomDigit
        );
        return $command;
    }

    private function initGbTemplate(ICommand $command, bool $result)
    {
        $gbTemplate = $this->prophesize(GbTemplate::class);
        $gbTemplate->setName(Argument::exact($command->name))->shouldBeCalledTimes(1);
        $gbTemplate->setIdentify(Argument::exact($command->identify))->shouldBeCalledTimes(1);
        $gbTemplate->setSubjectCategory(Argument::exact($command->subjectCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setDimension(Argument::exact($command->dimension))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoClassify(Argument::exact($command->infoClassify))->shouldBeCalledTimes(1);
        $gbTemplate->setInfoCategory(Argument::exact($command->infoCategory))->shouldBeCalledTimes(1);
        $gbTemplate->setDescription(Argument::exact($command->description))->shouldBeCalledTimes(1);

        $items = $this->commandHandler->setDefaultInfo($command);
        $gbTemplate->setItems(Argument::exact($items))->shouldBeCalledTimes(1);
        $gbTemplate->setExchangeFrequency(Argument::exact($command->exchangeFrequency))->shouldBeCalledTimes(1);
        $gbTemplate->edit()->shouldBeCalledTimes(1)->willReturn($result);

        return $gbTemplate;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter;
use BaseData\ResourceCatalogData\Adapter\Task\TaskDbAdapter;
use BaseData\ResourceCatalogData\Adapter\Task\TaskMockAdapter;

class TaskRepository extends Repository implements ITaskAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new TaskDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(ITaskAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : ITaskAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : ITaskAdapter
    {
        return new TaskMockAdapter();
    }

    public function fetchOne($id) : Task
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(Task $task) : bool
    {
        return $this->getAdapter()->add($task);
    }

    public function edit(Task $task, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($task, $keys);
    }
}

<?php
namespace BaseData\Task\Adapter\Task;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Task\Model\Task;
use BaseData\Task\Model\MockTask;
use BaseData\Task\Model\NullTask;
use BaseData\Task\Translator\TaskDbTranslator;
use BaseData\Task\Adapter\Task\Query\TaskRowQuery;

class TaskDbAdapter implements ITaskAdapter
{
    const PATENT_TASK_SIZE = 10;

    const SUB_TASK_SIZE = 10;

    const TIME_THRESHOLD = 60;

    const NOT_EXIST_ID = 1;

    //异常任务判断时间为 120 秒
    const EXPIRE_TIME = 120;

    const IS_PARENT_TASK = array(
        'NO' => 0,
        'YES' => 1
    );

    use DbAdapterTrait;

    private $dbTranslator;

    private $rowQuery;

    public function __construct()
    {
        $this->dbTranslator = new TaskDbTranslator();
        $this->rowQuery = new TaskRowQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowQuery);
    }
    
    protected function getDbTranslator() : TaskDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : TaskRowQuery
    {
        return $this->rowQuery;
    }

    protected function getNullObject() : INull
    {
        return NullTask::getInstance();
    }

    public function add(Task $task) : bool
    {
        return $this->addAction($task);
    }

    public function edit(Task $task, array $keys = array()) : bool
    {
        return $this->editAction($task, $keys);
    }

    public function fetchOne($id) : Task
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    /**
     * 触发所有子任务, 把睡眠状态改为待运行状态
     */
    public function notifySubTasks(Task $parentTask) : bool
    {
        //获取key
        $status = $this->getDbTranslator()->objectToArray($parentTask, ['status']);
        $statusKey = key($status);

        $parent = $this->getDbTranslator()->objectToArray($parentTask, ['parent']);
        $parentKey = key($parent);

        $rowQuery = $this->getRowQuery();
        $conditionArray[$parentKey] = $parentTask->getId();
        $info[$statusKey] = Task::STATUS['PENDING'];

        return $rowQuery->update($info, $conditionArray);
    }

    public function schedule() : array
    {
        $ids = [];
        
        $parentTasks = $this->schedulParentTasks();
        $subTasks = $this->scheduleSubTaks();

        $tasks = array_merge($parentTasks, $subTasks);

        $primaryKey = $this->getRowQuery()->getPrimaryKey();
        foreach ($tasks as $each) {
            $ids[] = $each[$primaryKey];
        }

        return $this->fetchList($ids);
    }

    public function expire() : array
    {
        //调度执行时间过长任务
        $filter['status'] = Task::STATUS['EXECUTING'];
        $filter['updateTime'] = Core::$container->get('time') - self::EXPIRE_TIME;
        return $this->filter($filter);
    }

    //调度父任务
    //@todo 翻译器优化 key
    protected function schedulParentTasks() : array
    {
        $filter['nextScheduleTime'] = Core::$container->get('time');
        $filter['status'] = Task::STATUS['PENDING'];
        $filter['isParentTask'] = self::IS_PARENT_TASK['YES'];

        $condition = $this->formatFilter($filter);

        $rowQuery = $this->getRowQuery();
        $list = $rowQuery->find(
            $condition,
            0,
            self::PATENT_TASK_SIZE
        );

        return $list;
    }

    //调度子任务
    //@todo 翻译器优化 key
    protected function scheduleSubTaks() : array
    {
        $filter['nextScheduleTime'] = Core::$container->get('time');
        $filter['status'] = Task::STATUS['PENDING'];
        $filter['isParentTask'] = self::IS_PARENT_TASK['NO'];

        $group['parent'] = 1;
        $sort['nice'] = 1;

        $condition = $this->formatFilter($filter);
        $condition .= $this->formatGroup($group);
        $condition .= $this->formatSort($sort);

        $rowQuery = $this->getRowQuery();
        $list = $rowQuery->find(
            $condition,
            0,
            self::SUB_TASK_SIZE
        );

        return $list;
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $task = new MockTask();
            if (isset($filter['pid'])) {
                $pids = empty($filter['pid']) ? self::NOT_EXIST_ID : implode(',', $filter['pid']);
                $info = $this->getDbTranslator()->objectToArray($task, array('pid'));
                $condition .= $conjection.key($info).' IN ('.$pids.')';
                $conjection = ' AND ';
            }

            //updateTime
            if (isset($filter['updateTime'])) {
                $task->setUpdateTime($filter['updateTime']);
                $info = $this->getDbTranslator()->objectToArray($task, array('updateTime'));
                $condition .= $conjection.key($info).' < '.current($info);
                $conjection = ' AND ';
            }

            //nextScheduleTime
            if (isset($filter['nextScheduleTime'])) {
                $task->setNextScheduleTime($filter['nextScheduleTime']);
                $info = $this->getDbTranslator()->objectToArray($task, array('nextScheduleTime'));
                $condition .= $conjection.key($info).' <= '.current($info);
                $conjection = ' AND ';
            }

            //status
            if (isset($filter['status'])) {
                $task->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($task, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }

            //isParentTask
            if (isset($filter['isParentTask'])) {
                $info = $this->getDbTranslator()->objectToArray($task, array('parent'));

                if ($filter['isParentTask'] == self::IS_PARENT_TASK['YES']) {
                    $condition .= $conjection.key($info).' = 0';
                }

                if ($filter['isParentTask'] != self::IS_PARENT_TASK['YES']) {
                    $condition .= $conjection.key($info).' != 0';
                }

                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $task = new MockTask();

            if (isset($sort['nice'])) {
                $info = $this->getDbTranslator()->objectToArray($task, array('nice'));
                $condition .= $conjection.key($info).' '.($sort['nice'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }
        
        return $condition;
    }

    protected function formatGroup(array $group) : string
    {
        $condition = '';
        $conjection = ' GROUP BY ';

        if (!empty($group)) {
            $task = new MockTask();

            if (isset($group['parent'])) {
                $info = $this->getDbTranslator()->objectToArray($task, array('parent'));
                $condition .= $conjection.key($info);
                $conjection = ',';
            }
        }
        
        return $condition;
    }
}

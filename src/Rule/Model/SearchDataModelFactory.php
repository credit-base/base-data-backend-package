<?php
namespace BaseData\Rule\Model;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Model\NullBjSearchData;

class SearchDataModelFactory
{
    const MAPS = array(
        Template::CATEGORY['BJ'] =>
        'BaseData\ResourceCatalogData\Model\BjSearchData',
        Template::CATEGORY['GB'] =>
        'BaseData\ResourceCatalogData\Model\GbSearchData',
        Template::CATEGORY['BASE'] =>
        'BaseData\ResourceCatalogData\Model\BaseSearchData',
        Template::CATEGORY['WBJ'] =>
        'BaseData\ResourceCatalogData\Model\WbjSearchData',
    );

    public function getModel($category)
    {
        $model = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';

        return class_exists($model) ? new $model : NullBjSearchData::getInstance();
    }
}

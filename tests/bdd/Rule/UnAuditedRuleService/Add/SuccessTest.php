<?php
namespace BaseData\Rule\UnAuditedRuleService\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;

use BaseData\Common\Model\IApproveAble;

use BaseData\Template\Model\Template;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限，且当我们需要把数据从某一个资源目录导入到另一个资源目录时,
 *           在政务网OA的资源目录规则管理中,可以设置导入的规则
 *           设置转换、补全、比对、去重、确认规则提交给发改委审核，审核通过将可以将数据依照规则导入到目标资源目录中,以便于我给发改委的数据是符合发改委要求的
 * @Scenario: 新增规则审核数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_apply_form_rule');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_wbj_template' => $this->wbjTemplate()
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $applyFormArray = $this->applyFormRule(IApproveAble::APPLY_STATUS['PENDING'], 0)[0];

        $unAuditedRuleService = new UnAuditedRuleService();

        $unAuditedRuleService->getApplyCrew()->setId($applyFormArray['apply_crew_id']);
        $unAuditedRuleService->getApplyUserGroup()->setId($applyFormArray['apply_usergroup_id']);
        $unAuditedRuleService->setOperationType($applyFormArray['operation_type']);
        $unAuditedRuleService->setApplyInfo(
            unserialize(gzuncompress(base64_decode($applyFormArray['apply_info'], true)))
        );
        $unAuditedRuleService->setRejectReason($applyFormArray['reject_reason']);
        $unAuditedRuleService->setSourceCategory($applyFormArray['source_category']);
        $unAuditedRuleService->setTransformationCategory($applyFormArray['transformation_category']);
        $unAuditedRuleService->getTransformationTemplate()->setId($applyFormArray['transformation_template_id']);
        $unAuditedRuleService->getSourceTemplate()->setId($applyFormArray['source_template_id']);
        $unAuditedRuleService->getPublishCrew()->setId($applyFormArray['publish_crew_id']);
        $unAuditedRuleService->getUserGroup()->setId($applyFormArray['usergroup_id']);

        return $unAuditedRuleService->add();
    }

    /**
     * @Then: 可以查到新增的审核数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setUnAuditedRuleService = $this->applyFormRule(IApproveAble::APPLY_STATUS['PENDING'], 0)[0];
        $setUnAuditedRuleService['apply_form_rule_id'] = 1;
        
        $repository = new UnAuditedRuleServiceRepository();

        $unAuditedRuleService = $repository->fetchOne($result);

        $translator = new UnAuditedRuleServiceDbTranslator();
        $unAuditedRuleServiceArray = $translator->objectToArray($unAuditedRuleService);

        $this->assertEquals($unAuditedRuleServiceArray, $setUnAuditedRuleService);
    }
}

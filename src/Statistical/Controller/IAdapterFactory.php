<?php
namespace BaseData\Statistical\Controller;

use BaseData\Statistical\Adapter\Statistical\IStatisticalAdapter;
use BaseData\Statistical\Adapter\Statistical\NullStatisticalAdapter;

class IAdapterFactory
{
    const MAPS = array(
        'enterpriseRelationInformationCount'=>
        'BaseData\Statistical\Adapter\Statistical\EnterpriseRelationInformationCountAdapter',
    );

    public function getAdapter(string $type) : IStatisticalAdapter
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : new NullStatisticalAdapter();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class MockWbjSearchData extends WbjSearchData
{
    public function getRepository() : IWbjSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getWbjItemsDataRepository() : IWbjItemsDataAdapter
    {
        return parent::getWbjItemsDataRepository();
    }

    public function addItemsData() : bool
    {
        return parent::addItemsData();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }
}

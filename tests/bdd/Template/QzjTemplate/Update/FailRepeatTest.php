<?php
namespace BaseData\Template\QzjTemplate\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当需要报送的资源目录发生变动时,我需要修改自己的前置机资源目录,在目录管理子系统下的前置机资源目录管理中,编辑前置机资源目录
 *           根据需要报送的资源目录修改现有前置机资源目录目录
 *           以便于我能及时报送数据
 * @Scenario: 异常流程-数据重复
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        $items = $this->items();
        return new ArrayDataSet([
            'pcore_qzj_template' => [
                [
                    'qzj_template_id' => 1,
                    'name' => '前置机登记信息测试',
                    'identify' => 'QZJDJXXXS',
                    'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                    'dimension' => Template::DIMENSION['SHGK'],
                    'exchange_frequency' => 1,
                    'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                    'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                    'description' =>'前置机登记信息测试',
                    'items' => json_encode($items, true),
                    'source_unit_id' => 1,
                    'category' => QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'],
                    'status' => 0,
                    'create_time' => Core::$container->get('time'),
                    'update_time' => Core::$container->get('time'),
                    'status_time' => 0
                ],
                [
                    'qzj_template_id' => 2,
                    'name' => '前置机登记信息测试重复',
                    'identify' => 'QZJDJXXXSCF',
                    'subject_category' => json_encode([Template::SUBJECT_CATEGORY['FRJFFRZZ']], true),
                    'dimension' => Template::DIMENSION['SHGK'],
                    'exchange_frequency' => 1,
                    'info_classify' => Template::INFO_CLASSIFY['XZXK'],
                    'info_category' => Template::INFO_CATEGORY['SHOUXXX'],
                    'description' =>'前置机登记信息测试数据重复',
                    'items' => json_encode($items, true),
                    'source_unit_id' => 0,
                    'category' => QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_GB'],
                    'status' => 0,
                    'create_time' => Core::$container->get('time'),
                    'update_time' => Core::$container->get('time'),
                    'status_time' => 0
                ]
            ],
        ]);
    }

    /**
     * @When: 获取需要编辑的用户数据
     */
    public function fetchTemplate($id)
    {
        $repository = new QzjTemplateRepository();

        $qzjTemplate = $repository->fetchOne($id);

        return $qzjTemplate;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $qzjTemplate = $this->fetchTemplate(1);

        $qzjTemplate->setIdentify('QZJDJXXXSCF');
        $qzjTemplate->setSubjectCategory(array(Template::SUBJECT_CATEGORY['ZRR']));
        $qzjTemplate->setDimension(Template::DIMENSION['ZWGX']);
        $qzjTemplate->setExchangeFrequency(2);
        $qzjTemplate->setInfoClassify(Template::INFO_CLASSIFY['QT']);
        $qzjTemplate->setInfoCategory(Template::INFO_CATEGORY['QTXX']);
        $qzjTemplate->setCategory(QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_GB']);
        $qzjTemplate->setItems(array('items'));
        $qzjTemplate->getSourceUnit()->setId(0);
        
        return $qzjTemplate->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertFalse($result);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\BjItemsData\IBjItemsDataAdapter;

class MockBjItemsData extends BjItemsData
{
    public function getRepository() : IBjItemsDataAdapter
    {
        return parent::getRepository();
    }
}

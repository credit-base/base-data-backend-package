<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\WorkOrderTask\Model\WorkOrderTask;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class WorkOrderTaskMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new WorkOrderTaskMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new WorkOrderTask()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new WorkOrderTask(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Model\WorkOrderTask',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\WorkOrderTask\Model\WorkOrderTask',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\WorkOrderTask\Model\WorkOrderTask',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\CommandBus;

use BaseData\ResourceCatalogData\Utils\GbSearchDataMockFactory;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;
use BaseData\ResourceCatalogData\Command\GbSearchData\ConfirmGbSearchDataCommand;
use BaseData\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand;
use BaseData\ResourceCatalogData\Command\GbSearchData\DeleteGbSearchDataCommand;

class GbSearchDataStatusControllerTest extends TestCase
{
    private $gbStub;

    public function setUp()
    {
        $this->gbStub = $this->getMockBuilder(MockGbSearchDataStatusController::class)
        ->setMethods([
            'displayError',
            ])
        ->getMock();
    }

    public function teatDown()
    {
        unset($this->gbStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\GbSearchDataRepository',
            $this->gbStub->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->gbStub->getCommandBus()
        );
    }

    private function initialDisable($result)
    {
        $this->gbStub = $this->getMockBuilder(MockGbSearchDataStatusController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DisableGbSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->gbStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->gbStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->gbStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $crew = GbSearchDataMockFactory::generateGbSearchData($command->id);

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);
        $this->gbStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->gbStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->gbStub->disable($command->id);
        $this->assertTrue($result);
    }

    private function initialConfirm($result)
    {
        $this->gbStub = $this->getMockBuilder(MockGbSearchDataStatusController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new ConfirmGbSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->gbStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testConfirmFailure()
    {
        $command = $this->initialConfirm(false);

        $this->gbStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->gbStub->confirm($command->id);
        $this->assertFalse($result);
    }

    public function testConfirmSuccess()
    {
        $command = $this->initialConfirm(true);

        $crew = GbSearchDataMockFactory::generateGbSearchData($command->id);

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $this->gbStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->gbStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->gbStub->confirm($command->id);
        $this->assertTrue($result);
    }

    private function initialDelete($result)
    {
        $this->gbStub = $this->getMockBuilder(MockGbSearchDataStatusController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DeleteGbSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->gbStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDeleteFailure()
    {
        $command = $this->initialDelete(false);

        $this->gbStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->gbStub->delete($command->id);
        $this->assertFalse($result);
    }

    public function testDeleteSuccess()
    {
        $command = $this->initialDelete(true);

        $crew = GbSearchDataMockFactory::generateGbSearchData($command->id);

        $repository = $this->prophesize(GbSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $this->gbStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->gbStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->gbStub->delete($command->id);
        $this->assertTrue($result);
    }
}

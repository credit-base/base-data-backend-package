<?php
namespace BaseData\Template\GbTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\GbTemplate;

/**
 * 测试国标资源目录接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * 不存在国标资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            []
        );
    }

    public function testViewGbTemplate()
    {
        $setGbTemplate = $this->gbTemplate()[0];
        $items = $this->items();

        $data = array(
            "data" => array(
                "type" => "gbTemplates",
                "attributes" => array(
                    "name" => '国标企业基本信息',
                    "identify" => 'GBQYJBXX',
                    "subjectCategory" => array(Template::SUBJECT_CATEGORY['FRJFFRZZ']),
                    "dimension" => Template::DIMENSION['SHGK'],
                    "exchangeFrequency" => 1,
                    "infoClassify" => Template::INFO_CLASSIFY['QT'],
                    "infoCategory" => Template::INFO_CATEGORY['QTXX'],
                    "description" => "国标企业基本信息",
                    "items" => $items
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'gbTemplates',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->gbTemplateCompareData($setGbTemplate, $contents['data']);
    }
}

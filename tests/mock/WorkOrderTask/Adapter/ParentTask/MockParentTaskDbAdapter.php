<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask;

use Marmot\Interfaces\INull;

use BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator;
use BaseData\WorkOrderTask\Adapter\ParentTask\Query\ParentTaskRowCacheQuery;

class MockParentTaskDbAdapter extends ParentTaskDbAdapter
{
    public function getDbTranslator() : ParentTaskDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : ParentTaskRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

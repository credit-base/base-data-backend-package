<?php
namespace BaseData\Template\Model;

use PHPUnit\Framework\TestCase;

class NullQzjTemplateTest extends TestCase
{
    private $qzjTemplate;

    public function setUp()
    {
        $this->qzjTemplate = $this->getMockBuilder(NullQzjTemplate::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->qzjTemplate);
    }

    public function testExtendsQzjTemplate()
    {
        $this->assertInstanceof('BaseData\Template\Model\QzjTemplate', $this->qzjTemplate);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->qzjTemplate);
    }

    public function testIsTemplateExist()
    {
        $this->qzjTemplate->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);
        
        $result = $this->qzjTemplate->isTemplateExist();
        $this->assertFalse($result);
    }
}

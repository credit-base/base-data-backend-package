<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\BjSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的本级数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * @Given: 存在一条资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
            'pcore_bj_search_data' => $this->bjSearchData()['pcore_bj_search_data']
        ]);
    }

    /**
     * @When: 当我查看该条资源目录数据详情时
     */
    public function fetchBjSearchData($id)
    {
        $repository = new BjSearchDataRepository();

        $bjSearchData = $repository->fetchOne($id);

        return $bjSearchData;
    }

    /**
     * @Then: 我可以看见该条资源目录数据的全部信息
     */
    public function testViewBjSearchDataList()
    {
        $id = 1;
        
        $setBjSearchData = $this->bjSearchData()['pcore_bj_search_data'][0];
        $setBjSearchData['bj_search_data_id'] = $id;

        $bjSearchData = $this->fetchBjSearchData($id);
        $translator = new BjSearchDataDbTranslator();
        $bjSearchDataArray = $translator->objectToArray($bjSearchData);

        $this->assertEquals($bjSearchDataArray, $setBjSearchData);
    }
}

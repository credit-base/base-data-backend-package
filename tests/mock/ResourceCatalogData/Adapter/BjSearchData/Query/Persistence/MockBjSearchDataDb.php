<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\Persistence;

class MockBjSearchDataDb extends BjSearchDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

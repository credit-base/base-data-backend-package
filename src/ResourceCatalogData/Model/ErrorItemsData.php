<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;
use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class ErrorItemsData extends ItemsData
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new ErrorDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IErrorDataAdapter
    {
        return $this->repository;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\CommandBus;

use BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;
use BaseData\ResourceCatalogData\Command\BjSearchData\ConfirmBjSearchDataCommand;
use BaseData\ResourceCatalogData\Command\BjSearchData\DisableBjSearchDataCommand;
use BaseData\ResourceCatalogData\Command\BjSearchData\DeleteBjSearchDataCommand;

class BjSearchDataStatusControllerTest extends TestCase
{
    private $bjStub;

    public function setUp()
    {
        $this->bjStub = $this->getMockBuilder(MockBjSearchDataStatusController::class)
        ->setMethods([
            'displayError',
            ])
        ->getMock();
    }

    public function teatDown()
    {
        unset($this->bjStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\BjSearchDataRepository',
            $this->bjStub->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->bjStub->getCommandBus()
        );
    }

    private function initialDisable($result)
    {
        $this->bjStub = $this->getMockBuilder(MockBjSearchDataStatusController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DisableBjSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->bjStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->bjStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->bjStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $crew = BjSearchDataMockFactory::generateBjSearchData($command->id);

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);
        $this->bjStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->bjStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->bjStub->disable($command->id);
        $this->assertTrue($result);
    }

    private function initialConfirm($result)
    {
        $this->bjStub = $this->getMockBuilder(MockBjSearchDataStatusController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new ConfirmBjSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->bjStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testConfirmFailure()
    {
        $command = $this->initialConfirm(false);

        $this->bjStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->bjStub->confirm($command->id);
        $this->assertFalse($result);
    }

    public function testConfirmSuccess()
    {
        $command = $this->initialConfirm(true);

        $crew = BjSearchDataMockFactory::generateBjSearchData($command->id);

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $this->bjStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->bjStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->bjStub->confirm($command->id);
        $this->assertTrue($result);
    }

    private function initialDelete($result)
    {
        $this->bjStub = $this->getMockBuilder(MockBjSearchDataStatusController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DeleteBjSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->bjStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDeleteFailure()
    {
        $command = $this->initialDelete(false);

        $this->bjStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->bjStub->delete($command->id);
        $this->assertFalse($result);
    }

    public function testDeleteSuccess()
    {
        $command = $this->initialDelete(true);

        $crew = BjSearchDataMockFactory::generateBjSearchData($command->id);

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $this->bjStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->bjStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->bjStub->delete($command->id);
        $this->assertTrue($result);
    }
}

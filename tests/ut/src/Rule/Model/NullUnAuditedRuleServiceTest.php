<?php
namespace BaseData\Rule\Model;

use PHPUnit\Framework\TestCase;

class NullUnAuditedRuleServiceTest extends TestCase
{
    private $ruleService;

    public function setUp()
    {
        $this->ruleService = $this->getMockBuilder(MockNullUnAuditedRuleService::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->ruleService);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\UnAuditedRuleService',
            $this->ruleService
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->ruleService
        );
    }

    public function testApply()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->apply();
        $this->assertFalse($result);
    }

    public function testApproveApplyInfo()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->approveApplyInfo(1);
        $this->assertFalse($result);
    }

    public function testUpdateApplyStatus()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->updateApplyStatus(1);
        $this->assertFalse($result);
    }

    public function testUpdateRelationId()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->updateRelationId();
        $this->assertFalse($result);
    }

    public function testRevoke()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->revoke();
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->ruleService->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}

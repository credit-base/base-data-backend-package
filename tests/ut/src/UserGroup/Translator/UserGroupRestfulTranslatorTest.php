<?php
namespace BaseData\UserGroup\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Utils\UserGroupRestfulUtils;

class UserGroupRestfulTranslatorTest extends TestCase
{
    use UserGroupRestfulUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UserGroupRestfulTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testArrayToObject()
    {
        $userGroup = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $expression['data']['id'] = $userGroup->getId();
        $expression['data']['attributes']['name'] = $userGroup->getName();
        $expression['data']['attributes']['shortName'] = $userGroup->getShortName();
        $expression['data']['attributes']['createTime'] = $userGroup->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $userGroup->getUpdateTime();
        $expression['data']['attributes']['status'] = $userGroup->getStatus();

        $userGroupObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\UserGroup\Model\UserGroup', $userGroupObject);
        $this->compareArrayAndObject($expression, $userGroupObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $department = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\UserGroup\Model\NullUserGroup', $department);
    }

    public function testObjectToArray()
    {
        $userGroup = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup(1);

        $expression = $this->translator->objectToArray($userGroup);

        $this->compareArrayAndObject($expression, $userGroup);
    }

    public function testObjectToArrayFail()
    {
        $userGroup = null;

        $expression = $this->translator->objectToArray($userGroup);
        $this->assertEquals(array(), $expression);
    }
}

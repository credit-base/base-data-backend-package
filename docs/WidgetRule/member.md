# 前台用户管理控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

* `USER_NAME`: 用户名
* `REAL_NAME`: 姓名
* `CELLPHONE`: 手机号码
* `CARDID`: 身份证号
* `EMAIL`: 邮箱
* `CONTACT_ADDRESS`: 联系地址
* `SECURITY_QUESTION`: 密保问题
* `SECURITY_ANSWER`: 密保答案
* `GENDER`: 性别
* `PASSWORD`: 密码
* `STATUS`: 状态

---

### `REAL_NAME`
* [姓名控件规范(REAL_NAME)](user.md)(必填)

### `CELLPHONE`
* [手机号控件规范(CELLPHONE)](user.md)(必填,唯一)

### `CARDID`
* [身份证号控件规范(CARDID)](user.md)(必填)

### `PASSWORD`
* [密码控件规范(PASSWORD)](user.md)(必填)

### `STATUS`
* [状态控件规范(STATUS)](common.md)(必填)

### `USER_NAME`

**用户名** 控件规范,用于表述用户名

#### 规则

* string min:2|max:20
* 唯一
* 必填

#### 错误提示

* `ER-0100`: 用户名不能为空
* `ER-0101`:用户名格式不正确,长度范围为2-20字符
* `ER-0103`:用户名已经存在

### `EMAIL`

**邮箱** 控件规范,用于表述邮箱

#### 规则

* string min:2|max:30
* 必须包含"@"和"."
* 唯一
* 必填

#### 错误提示

* `ER-0100`: 邮箱不能为空
* `ER-0101`:邮箱格式不正确,长度范围为2-30字符,且必须包含"@"和"."
* `ER-0103`:邮箱已经存在

### `CONTACT_ADDRESS`

**联系地址** 控件规范,用于表述联系地址

#### 规则

* string min:1|max:255
* 必填

#### 错误提示

* `ER-0100`: 联系地址不能为空
* `ER-0101`:联系地址格式不正确,长度范围为1-255字符

### `SECURITY_QUESTION`

**密保问题** 密保问题控件规范

#### 规则

* 必填
* int，可选范围1-5
	* SECURITY_QUESTION[QUESTION_ONE] = 1 //你父亲或母亲的姓名
	* SECURITY_QUESTION[QUESTION_TWO] = 2 //你喜欢看的电影
	* SECURITY_QUESTION[QUESTION_THREE] = 3 //你最好朋友的名字
	* SECURITY_QUESTION[QUESTION_FOUR] = 4 //你毕业于哪个高中
	* SECURITY_QUESTION[QUESTION_FIVE] = 5 //你最喜欢的颜色

#### 错误提示

* `ER-0100`: 密保问题不能为空
* `ER-0101`: 密保问题格式不正确

### `SECURITY_ANSWER`

**密保答案** 控件规范,用于表述密保答案

#### 规则

* string min:1|max:30
* 必填

#### 错误提示

* `ER-0100`: 密保答案不能为空
* `ER-0101`:密保答案格式不正确,长度范围为1-30位字符

### `GENDER`

**性别** 性别控件规范

#### 规则

* int，可选范围1,2
	* GENDER[NULL] = 0 //默认
	* GENDER[MALE] = 1 //男
	* GENDER[FEMALE] = 2 //女

#### 错误提示

* `ER-0101`: 性别格式不正确
<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class YsSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\ResourceCatalogData\Command\YsSearchData\EnableYsSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\YsSearchData\EnableYsSearchDataCommandHandler',
        'BaseData\ResourceCatalogData\Command\YsSearchData\DisableYsSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\YsSearchData\DisableYsSearchDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

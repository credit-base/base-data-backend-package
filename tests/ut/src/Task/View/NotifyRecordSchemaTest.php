<?php
namespace BaseData\Task\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class NotifyRecordSchemaTest extends TestCase
{
    private $notifyRecordSchema;

    private $notifyRecord;

    public function setUp()
    {
        $this->notifyRecordSchema = new NotifyRecordSchema(new Factory());

        $this->notifyRecord = \BaseData\Task\Utils\MockFactory::generateNotifyRecord(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->notifyRecordSchema);
        unset($this->notifyRecord);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->notifyRecordSchema);
    }

    public function testGetId()
    {
        $result = $this->notifyRecordSchema->getId($this->notifyRecord);

        $this->assertEquals($result, $this->notifyRecord->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->notifyRecordSchema->getAttributes($this->notifyRecord);

        $this->assertEquals($result['name'], $this->notifyRecord->getName());
        $this->assertEquals($result['hash'], $this->notifyRecord->getHash());
        $this->assertEquals($result['status'], $this->notifyRecord->getStatus());
        $this->assertEquals($result['createTime'], $this->notifyRecord->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->notifyRecord->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->notifyRecord->getStatusTime());
    }
}

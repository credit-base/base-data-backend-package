<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Repository\TaskRepository;
use BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\ErrorData\Query\ErrorDataRowCacheQuery;

use BaseData\Rule\Repository\RepositoryFactory;

class MockErrorDataDbAdapter extends ErrorDataDbAdapter
{
    public function getDbTranslator() : ErrorDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : ErrorDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getTaskRepository() : TaskRepository
    {
        return parent::getTaskRepository();
    }

    public function getRepositoryFactory() : RepositoryFactory
    {
        return parent::getRepositoryFactory();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function fetchTask($errorData)
    {
        return parent::fetchTask($errorData);
    }

    public function fetchTaskByObject($errorData)
    {
        return parent::fetchTaskByObject($errorData);
    }

    public function fetchTaskByList(array $errorDataList)
    {
        return parent::fetchTaskByList($errorDataList);
    }

    public function fetchTemplate($errorData)
    {
        return parent::fetchTemplate($errorData);
    }

    public function fetchTemplateByObject($errorData)
    {
        return parent::fetchTemplateByObject($errorData);
    }

    public function fetchTemplateByList(array $errorDataList)
    {
        return parent::fetchTemplateByList($errorDataList);
    }

    public function fetchUserGroupByObject($errorData)
    {
        return parent::fetchUserGroupByObject($errorData);
    }
}

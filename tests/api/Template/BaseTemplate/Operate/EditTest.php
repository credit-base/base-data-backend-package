<?php
namespace BaseData\Template\BaseTemplate\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\BaseTemplate;

/**
 * 测试基础资源目录接口编辑
 */
class EditTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
    }

    /**
     * 存在一条基础资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_base_template' => $this->baseTemplate()
            ]
        );
    }

    public function testViewBaseTemplate()
    {
        $items = $this->items(
            [Template::SUBJECT_CATEGORY_CN[Template::SUBJECT_CATEGORY['GTGSH']]],
            [Template::DIMENSION_CN[Template::DIMENSION['SQCX']]],
            [Template::EXCHANGE_FREQUENCY_CN[Template::EXCHANGE_FREQUENCY['MZ']]],
            [Template::INFO_CLASSIFY_CN[Template::INFO_CLASSIFY['HONGMD']]],
            [Template::INFO_CATEGORY_CN[Template::INFO_CATEGORY['SHOUXXX']]]
        );

        $data = array(
            "data" => array(
                "type" => "baseTemplates",
                "attributes" => array(
                    "subjectCategory" => array(Template::SUBJECT_CATEGORY['GTGSH']),
                    "dimension" => Template::DIMENSION['SQCX'],
                    "exchangeFrequency" => Template::EXCHANGE_FREQUENCY['MZ'],
                    "infoClassify" => Template::INFO_CLASSIFY['HONGMD'],
                    "infoCategory" => Template::INFO_CATEGORY['SHOUXXX'],
                    "description" => "基础企业基本信息编辑",
                    'items' => $items
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 2)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'baseTemplates/1',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals($items, $attributes['items']);
    }
}

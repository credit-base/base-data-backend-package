<?php
namespace BaseData\Task\Script;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Service\SyncErrorDataDownloadService;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\TaskRepository;

class SyncErrorDataDownloadTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockSyncErrorDataDownload();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsOnceTask()
    {
        $this->assertInstanceof("BaseData\Task\Model\OnceTask", $this->stub);
    }
    
    public function testGetTaskRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\TaskRepository',
            $this->stub->getTaskRepository()
        );
    }

    public function testFetchTask()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownload::class)
                        ->setMethods(['getTaskRepository'])
                        ->getMock();
        $id = 1;

        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask($id);

        $repository = $this->prophesize(TaskRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($task);
        $stub->expects($this->exactly(1))->method('getTaskRepository')->willReturn($repository->reveal());

        $result = $stub->fetchTask($id);

        $this->assertEquals($result, $task);
    }


    public function testGetSyncErrorDataDownloadService()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Service\SyncErrorDataDownloadService',
            $this->stub->getSyncErrorDataDownloadService(new Task())
        );
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockSyncErrorDataDownload::class)
            ->setMethods([
                'getData',
                'fetchTask',
                'getSyncErrorDataDownloadService',
            ])->getMock();

        $data = array('task' => 1);

        $stub->expects($this->exactly(1))
             ->method('getData')
             ->willReturn($data);

        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask($data['task']);
        
        $stub->expects($this->exactly(1))
             ->method('fetchTask')
             ->willReturn($task);

        $syncTaskService = $this->prophesize(SyncErrorDataDownloadService::class);
        $syncTaskService->execute()->shouldBeCalledTimes(1)->willReturn(true);
        $stub->expects($this->once())->method(
            'getSyncErrorDataDownloadService'
        )->willReturn($syncTaskService->reveal());

        $result = $stub->executeAction();

        $this->assertTrue($result);
    }
}

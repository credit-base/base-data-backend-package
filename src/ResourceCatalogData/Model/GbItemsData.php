<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Repository\GbItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter;

class GbItemsData extends ItemsData
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbItemsDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IGbItemsDataAdapter
    {
        return $this->repository;
    }
}

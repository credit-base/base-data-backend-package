<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

/**
 * 测试国标资源目录接口确认
 */
class ConfirmTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_search_data');
    }

    /**
     * 存在一条国标资源目录数据数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data'],
            ]
        );
    }

    public function testViewCrew()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'gbSearchData/1/confirm',
            ['headers' => Core::$container->get('client.headers')]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(GbSearchData::STATUS['ENABLED'], $attributes['status']);
    }
}

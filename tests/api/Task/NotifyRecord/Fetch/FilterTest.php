<?php
namespace BaseData\Task\NotifyRecord\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\NotifyRecord\SetDataApiTrait;
use BaseData\Task\NotifyRecord\NotifyRecordUtils;

/**
 * 测试上传记录接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NotifyRecordUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_schedule_task_notify_record');
    }

    /**
     * 存在多条上传记录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_schedule_task_notify_record' => $this->notifyRecord()
            ]
        );
    }

    public function testViewNotifyRecord()
    {
        $setNotifyRecordList = $this->notifyRecord();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'notifyRecords?filter[hash]=C4CA4238A0B923820DCC509A6F75849B',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];

        foreach ($data as $key => $notifyRecord) {
            $this->notifyRecordCompareData($setNotifyRecordList[$key], $notifyRecord);
        }
    }
}

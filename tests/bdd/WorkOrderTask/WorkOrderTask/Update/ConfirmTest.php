<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Model\IProgress;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;

/**
 * @Feature: 我是委办局领导,当我们有数据但没有对应的资源目录可以上报时,在政务网OA中,可以通过接受发改委领导指派的工单任务生成对应的资源目录
 *           当任务我确认无误时可以同意使用任务中的资源目录上报数据，当任务有问题时我可以进行反馈,以便于我可以把我的数据通过发改委指派的资源目录上报上去
 * @Scenario: 确认工单任务
 */
class ConfirmTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_work_order_task');
    }
    /**
     * @Given: 存在需要撤销的父任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_parent_task' => $this->parentTask(),
            'pcore_work_order_task' => $this->workOrderTask(IProgress::STATUS['DQR'])
        ]);
    }

    protected function getWorkOrderTaskRepository() : WorkOrderTaskRepository
    {
        return new WorkOrderTaskRepository();
    }
    /**
     * @When: 获取需要撤销的父任务
     */
    public function fetchWorkOrderTask($id)
    {
        $repository = $this->getWorkOrderTaskRepository();

        $workOrderTask = $repository->fetchOne($id);

        return $workOrderTask;
    }
    /**
     * @And: 当我调用确认函数,期待返回true
     */
    public function confirm()
    {
        $workOrderTask = $this->fetchWorkOrderTask(1);
        
        return $workOrderTask->confirm();
    }

    /**
     * @Then: 数据已经被确认
     */
    public function testValidate()
    {
        $result = $this->confirm();

        $this->assertTrue($result);

        $workOrderTask = $this->fetchWorkOrderTask(1);

        $this->assertEquals(IProgress::STATUS['YQR'], $workOrderTask->getStatus());
        $this->assertEquals(Core::$container->get('time'), $workOrderTask->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $workOrderTask->getStatusTime());
    }
}

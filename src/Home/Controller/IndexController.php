<?php
namespace Home\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

class IndexController extends Controller
{
    use JsonApiTrait;

    public function index()
    {
        echo "Hello World Base Data Backend Package";
        return true;
    }
}

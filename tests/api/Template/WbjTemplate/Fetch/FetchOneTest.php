<?php
namespace BaseData\Template\WbjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\WbjTemplate;

/**
 * 测试委办局资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    private $wbjTemplate;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->wbjTemplate);
        $this->clear('pcore_wbj_template');
    }

    /**
     * 存在一条委办局资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate()
            ]
        );
    }

    public function testViewWbjTemplate()
    {
        $setWbjTemplate = $this->wbjTemplate()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'templates/1?include=sourceUnit',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->wbjTemplateCompareData($setWbjTemplate, $contents['data']);
    }
}

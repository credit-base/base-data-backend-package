<?php
namespace BaseData\Task\Repository;

use BaseData\Task\Adapter\Task\ITaskAdapter;

class MockTaskRepository extends TaskRepository
{
    public function getAdapter() : ITaskAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : ITaskAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : ITaskAdapter
    {
        return parent::getMockAdapter();
    }
}

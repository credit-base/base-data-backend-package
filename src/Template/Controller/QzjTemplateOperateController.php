<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\View\QzjTemplateView;
use BaseData\Template\Command\QzjTemplate\AddQzjTemplateCommand;
use BaseData\Template\Command\QzjTemplate\EditQzjTemplateCommand;

class QzjTemplateOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, QzjTemplateControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 前置机资源目录新增功能,通过POST传参
     * 对应路由 /qzjTemplates
     * @return jsonApi
     */
    public function add()
    {
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $category = $attributes['category'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];
        $description = $attributes['description'];
        $items = $attributes['items'];
        $sourceUnit = $relationships['sourceUnit']['data'][0]['id'];

        //验证
        if ($this->validateOperateScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $category,
            $sourceUnit
        )) {
            //初始化命令
            $commandBus = $this->getCommandBus();
            $command = new AddQzjTemplateCommand(
                $name,
                $identify,
                $subjectCategory,
                $dimension,
                $exchangeFrequency,
                $infoClassify,
                $infoCategory,
                $description,
                $items,
                $sourceUnit,
                $category
            );

            //执行命令
            if ($commandBus->send($command)) {
                //获取最新数据
                $repository = $this->getRepository();
                $qzjTemplate = $repository->fetchOne($command->id);
                if ($qzjTemplate instanceof QzjTemplate) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new QzjTemplateView($qzjTemplate));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 前置机资源目录编辑功能,通过PATCH传参
     * 对应路由 /qzjTemplates/{id:\d+}
     * @param int id 前置机资源目录 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];
        
        $name = $attributes['name'];
        $identify = $attributes['identify'];
        $category = $attributes['category'];
        $subjectCategory = $attributes['subjectCategory'];
        $dimension = $attributes['dimension'];
        $exchangeFrequency = $attributes['exchangeFrequency'];
        $infoClassify = $attributes['infoClassify'];
        $infoCategory = $attributes['infoCategory'];
        $description = $attributes['description'];
        $items = $attributes['items'];
        $sourceUnit = $relationships['sourceUnit']['data'][0]['id'];

        //验证
        if ($this->validateOperateScenario(
            $name,
            $identify,
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $description,
            $items,
            $category,
            $sourceUnit
        )) {
            $commandBus = $this->getCommandBus();

            $command = new EditQzjTemplateCommand(
                $name,
                $identify,
                $subjectCategory,
                $dimension,
                $exchangeFrequency,
                $infoClassify,
                $infoCategory,
                $description,
                $items,
                $sourceUnit,
                $category,
                $id
            );

            if ($commandBus->send($command)) {
                $repository = $this->getRepository();
                $qzjTemplate = $repository->fetchOne($id);
                if ($qzjTemplate instanceof QzjTemplate) {
                    $this->render(new QzjTemplateView($qzjTemplate));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }
}

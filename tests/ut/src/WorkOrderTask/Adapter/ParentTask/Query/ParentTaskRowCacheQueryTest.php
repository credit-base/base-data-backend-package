<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask\Query;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class ParentTaskRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockParentTaskRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'BaseData\WorkOrderTask\Adapter\ParentTask\Query\Persistence\ParentTaskCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'BaseData\WorkOrderTask\Adapter\ParentTask\Query\Persistence\ParentTaskDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class GbSearchDataCache extends Cache
{
    const KEY = 'gb_search_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

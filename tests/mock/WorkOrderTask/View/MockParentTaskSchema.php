<?php
namespace BaseData\WorkOrderTask\View;

use BaseData\WorkOrderTask\Model\ParentTask;

class MockParentTaskSchema extends ParentTaskSchema
{
    public function calRatio(ParentTask $parentTask) : int
    {
        return parent::calRatio($parentTask);
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;

class YsSearchDataCommandHandlerTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = new MockYsSearchDataCommandHandlerTrait();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\YsSearchDataRepository',
            $this->trait->publicGetRepository()
        );
    }

    public function testFetchYsSearchData()
    {
        $trait = $this->getMockBuilder(
            MockYsSearchDataCommandHandlerTrait::class
        )->setMethods(['getRepository'])->getMock();

        $id = 1;
        $ysSearchData = \BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory::generateYsSearchData($id);

        $repository = $this->prophesize(YsSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($ysSearchData);

        $trait->expects($this->exactly(1))
                         ->method('getRepository')
                         ->willReturn($repository->reveal());

        $result = $trait->publicFetchYsSearchData($id);

        $this->assertEquals($result, $ysSearchData);
    }
}

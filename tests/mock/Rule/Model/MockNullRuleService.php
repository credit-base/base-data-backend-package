<?php
namespace BaseData\Rule\Model;

class MockNullRuleService extends NullRuleService
{
    public function validate() : bool
    {
        return parent::validate();
    }

    public function commonValidate() : bool
    {
        return parent::commonValidate();
    }
    
    public function isCrewNull() : bool
    {
        return parent::isCrewNull();
    }

    public function isUserGroupNull() : bool
    {
        return parent::isUserGroupNull();
    }

    public function isSourceTemplateNull() : bool
    {
        return parent::isSourceTemplateNull();
    }

    public function isRuleExist() : bool
    {
        return parent::isRuleExist();
    }

    public function isTransformationTemplateNull() : bool
    {
        return parent::isTransformationTemplateNull();
    }

    public function requiredItemVerification() : bool
    {
        return parent::requiredItemVerification();
    }

    public function rulesValidate() : bool
    {
        return parent::rulesValidate();
    }

    public function updateTemplateRuleCount() : bool
    {
        return parent::updateTemplateRuleCount();
    }
}

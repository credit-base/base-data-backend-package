<?php
namespace BaseData\Rule\Command\RuleService;

use BaseData\Common\Command\AddApproveCommand;

class AddRuleServiceCommand extends AddApproveCommand
{
    public $rules;

    public $transformationTemplate;

    public $sourceTemplate;

    public $transformationCategory;

    public $sourceCategory;

    public function __construct(
        array $rules,
        int $transformationTemplate,
        int $sourceTemplate,
        int $transformationCategory,
        int $sourceCategory,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $crew,
            $id
        );
        
        $this->rules = $rules;
        $this->transformationTemplate = $transformationTemplate;
        $this->sourceTemplate = $sourceTemplate;
        $this->transformationCategory = $transformationCategory;
        $this->sourceCategory = $sourceCategory;
    }
}

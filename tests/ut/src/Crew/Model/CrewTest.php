<?php
namespace BaseData\Crew\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class CrewTest extends TestCase
{
    private $crew;

    public function setUp()
    {
        $this->crew = new Crew();
    }

    public function tearDown()
    {
        unset($this->crew);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->crew
        );
    }

    public function testConstructor()
    {
        $this->assertEquals(0, $this->crew->getId());
        $this->assertEmpty($this->crew->getCellphone());
        $this->assertEmpty($this->crew->getRealName());
        $this->assertEquals(0, $this->crew->getCreateTime());
        $this->assertEquals(0, $this->crew->getUpdateTime());
        $this->assertEquals(0, $this->crew->getStatusTime());
        $this->assertEquals(Crew::STATUS_NORMAL, $this->crew->getStatus());
        $this->assertInstanceOf('BaseData\UserGroup\Model\UserGroup', $this->crew->getUserGroup());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Crew setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->crew->setId(1);
        $this->assertEquals(1, $this->crew->getId());
    }
    //id 测试 ----------------------------------------------------------   end
    //cellphone 测试 --------------------------------------------------- start
    /**
     * 设置 User setCellphone() 正确的传参类型,期望传值正确
     */
    public function testSetCellphoneCorrectType()
    {
        $this->crew->setCellphone('15202939435');
        $this->assertEquals('15202939435', $this->crew->getCellphone());
    }
    
    /**
     * 设置 User setCellphone() 正确的传参类型,但是不属于手机格式,期望返回空.
     */
    public function testSetCellphoneCorrectTypeButNotCellphone()
    {
        $this->crew->setCellphone('15202939435'.'a');
        $this->assertEquals('', $this->crew->getCellphone());
    }
    //cellphone 测试 ---------------------------------------------------   end

    //realName 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setRealName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->crew->setRealName('string');
        $this->assertEquals('string', $this->crew->getRealName());
    }

    /**
     * 设置 Crew setRealName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->crew->setRealName(array(1,2,3));
    }
    //realName 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 Crew setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->crew->setStatus(0);
        $this->assertEquals(0, $this->crew->getStatus());
    }

    /**
     * 设置 Crew setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->crew->setStatus(array(1,2,3));
    }
    //status 测试 --------------------------------------------------------   end
}

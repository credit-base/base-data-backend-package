<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class ErrorDataDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockErrorDataDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(ErrorDataDb::TABLE, $this->database->getTable());
    }
}

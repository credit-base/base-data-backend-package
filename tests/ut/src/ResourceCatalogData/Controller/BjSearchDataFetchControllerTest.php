<?php
namespace BaseData\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter;
use BaseData\ResourceCatalogData\Model\NullBjSearchData;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\View\BjSearchDataView;

class BjSearchDataFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(BjSearchDataFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockBjSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\BjSearchData\IBjSearchDataAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockBjSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\View\BjSearchDataView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockBjSearchDataFetchController();

        $this->assertEquals(
            'bjSearchData',
            $controller->getResourceName()
        );
    }
}

<?php
namespace BaseData\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\NullErrorData;

class ErrorDataDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $errorData = null)
    {
        if (!isset($expression['error_data_id'])) {
            return NullErrorData::getInstance();
        }

        if ($errorData == null) {
            $errorData = new ErrorData();
        }

        $errorData->setId($expression['error_data_id']);

        $errorData->getTask()->setId($expression['task_id']);
        $errorData->setCategory($expression['category']);
        $errorData->getTemplate()->setId($expression['template_id']);
        $errorData->getItemsData()->setData(
            unserialize(gzuncompress(base64_decode($expression['items_data'], true)))
        );
        $errorData->setErrorType($expression['error_type']);

        if (isset($expression['error_reason'])) {
            $errorReason = array();
            if (is_string($expression['error_reason'])) {
                $errorReason = json_decode($expression['error_reason'], true);
            }
            if (is_array($expression['error_reason'])) {
                $errorReason = $expression['error_reason'];
            }
            $errorData->setErrorReason($errorReason);
        }

        $errorData->setStatus($expression['status']);
        $errorData->setCreateTime($expression['create_time']);
        $errorData->setUpdateTime($expression['update_time']);
        $errorData->setStatusTime($expression['status_time']);
        
        return $errorData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($errorData, array $keys = array())
    {
        if (!$errorData instanceof ErrorData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',

                'task',
                'category',
                'template',
                'itemsData',
                'errorType',
                'errorReason',

                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['error_data_id'] = $errorData->getId();
        }

        if (in_array('task', $keys)) {
            $expression['task_id'] = $errorData->getTask()->getId();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $errorData->getCategory();
        }
        if (in_array('template', $keys)) {
            $expression['template_id'] = $errorData->getTemplate()->getId();
        }
        if (in_array('itemsData', $keys)) {
            $expression['items_data'] =
            base64_encode(gzcompress(serialize($errorData->getItemsData()->getData())));
        }
        if (in_array('errorType', $keys)) {
            $expression['error_type'] = $errorData->getErrorType();
        }
        if (in_array('errorReason', $keys)) {
            $expression['error_reason'] = $errorData->getErrorReason();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $errorData->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $errorData->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $errorData->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $errorData->getStatusTime();
        }

        return $expression;
    }
}

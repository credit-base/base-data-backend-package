<?php
namespace BaseData\Common\Command;

use Marmot\Interfaces\ICommand;

use BaseData\Common\Model\IApproveAble;

class EditApproveCommand implements ICommand
{
    public $crew;

    public $id;
    
    public $operationType;

    public function __construct(
        int $crew,
        int $id = 0,
        int $operationType = IApproveAble::OPERATION_TYPE['EDIT']
    ) {
        $this->crew = $crew;
        $this->id = $id;
        $this->operationType = $operationType;
    }
}

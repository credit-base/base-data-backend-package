<?php
namespace BaseData\Rule\UnAuditedRuleService\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\Translator\UnAuditedRuleServiceDbTranslator;

use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有目录规则审核权限，当我想要查看某个资源目录的规则时,
 *           在政务网OA中的目录规则审核模块,我可以搜索规则,通过列表形式查看我搜索到的规则审核数据,以便于我快速定位规则并进行下一步操作
 * @Scenario: 通过目标资源目录搜索
 */
class TransformationTemplateTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_bj_template');
    }

    /**
     * @Given: 存在规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_apply_form_rule' => $this->applyFormRule(),
            'pcore_bj_template' => $this->bjTemplate()
        ]);
    }
    /**
     * @When: 当我查看规则数据列表时
     */
    public function fetchUnAuditedRuleServiceList()
    {
        $repository = new UnAuditedRuleServiceRepository();
 
        $filter['transformationTemplate'] = 1;

        list($ruleServiceList, $count) = $repository->filter($filter);
        
        unset($count);

        return $ruleServiceList;
    }

    /**
     * @Then: 我可以看到规则目标资源目录id为"1"的规则数据
     */
    public function testViewRuleList()
    {
        $setRuleServiceList = $this->applyFormRule();

        foreach ($setRuleServiceList as $key => $setRuleService) {
            unset($setRuleService);
            $setRuleServiceList[$key]['apply_form_rule_id'] = $key +1;
        }

        $ruleServiceList = $this->fetchUnAuditedRuleServiceList();
        $translator = new UnAuditedRuleServiceDbTranslator();
        foreach ($ruleServiceList as $rule) {
            $ruleServiceArray[] = $translator->objectToArray($rule);
        }

        $this->assertEquals($ruleServiceArray, $setRuleServiceList);

        foreach ($ruleServiceArray as $rule) {
            $this->assertEquals(1, $rule['transformation_template_id']);
        }
    }
}

<?php
namespace BaseData\Rule\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\Template;
use BaseData\Template\Model\MockTemplate;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Model\MockSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Model\SearchData;

/**
 * BaseData\Rule\Model\CompletionRule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class CompletionRuleTest extends ComparisonCompleteRule
{
    public function setUp()
    {
        parent::setUp();
        $this->rule = $this->getMockBuilder(CompletionRule::class)
                           ->setMethods(
                               [
                                    'getTransformationTemplate',
                                    'fetchTemplateItems',
                                    'getTemplateRepository',
                                    'fetchTemplateList',
                                    'getCondition',
                                    'validateBase',
                                    'templateItemComparison'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function testValidateNotExistCompleteTemplateItem()
    {
        $this->ruleTemplateList = [1=>new MockTemplate(), 2=>new MockTemplate()];

        $this->condition = array(
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'NOT_EXIST')
            )
        );
        $base = $this->condition['ZTMC'][0]['base'];
        $completeTemplate = $this->ruleTemplateList[1];


        $this->prepareValidate();

        $this->rule->expects($this->exactly(2))
                         ->method('fetchTemplateItems')
                        ->withConsecutive(
                            [$this->transformationTemplate],
                            [$completeTemplate]
                        )
                         ->will($this->onConsecutiveCalls(
                             $this->transformationTemplateItems,
                             $this->ruleTemplateItems
                         ));

        $this->rule->expects($this->exactly(1))
                         ->method('validateBase')
                         ->with($completeTemplate, $base)
                         ->willReturn(true);

        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    public function testValidateTemplateItemComparisonFalse()
    {
        $result = false;

        $this->prepareItemComparison();
        $item = $this->condition['ZTMC'][0]['item'];
        $transformationItem = $this->transformationTemplateItems['ZTMC'];
        $sourceItem = $this->ruleTemplateItems[$item];

        $this->rule->expects($this->exactly(1))
                         ->method('templateItemComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn($result);

        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    public function testValidateSuccess()
    {
        $result = true;
        
        $this->prepareItemComparison();
        $item = $this->condition['ZTMC'][0]['item'];
        $transformationItem = $this->transformationTemplateItems['ZTMC'];
        $sourceItem = $this->ruleTemplateItems[$item];

        $this->rule->expects($this->exactly(1))
                         ->method('templateItemComparison')
                         ->with($transformationItem, $sourceItem)
                         ->willReturn($result);

        $result = $this->rule->validate();
        $this->assertTrue($result);
    }

    //validateTransformation
    public function testValidateTransformation()
    {
        $rule = new MockCompletionRule();
        $transformationSearchData = new FailureData();
        $result = $rule->validateTransformation($transformationSearchData);
        $this->assertTrue($result);
    }

    //transformationInitialization
    public function testTransformationInitialization()
    {
        //初始化
        $condition = ['condition'];
        $data = ['data'];
        $template = new MockTemplate();
        $requiredItems = ['requiredItems'];
        $transformationSearchData = new MockSearchData();
        $transformationSearchData->setTemplate($template);
        $transformationSearchData->getItemsData()->setData($data);

        $rule = $this->getMockBuilder(MockCompletionRule::class)
                           ->setMethods(
                               [
                                    'getCondition',
                                    'transformationTemplateRequired'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('getCondition')
                         ->willReturn($condition);

        $rule->expects($this->exactly(1))
                         ->method('transformationTemplateRequired')
                         ->with($template)
                         ->willReturn($requiredItems);

        $expected = [
            'condition' => $condition,
            'transformationItemsData' => $data,
            'requiredItems' => $requiredItems
        ];

        $result = $rule->transformationInitialization(new MockSearchData(), $transformationSearchData);

        $this->assertEquals($expected, $result);
    }

    //validateInitializationDataFormat
    public function testValidateInitializationDataFormat()
    {
        $data = [
            'condition'=>'condition',
            'transformationItemsData'=>'transformationItemsData',
            'requiredItems'=>'requiredItems'
        ];
        $rule = new MockCompletionRule();

        $result = $rule->validateInitializationDataFormat($data);
        $this->assertTrue($result);
    }

    //validateDataFormat
    public function testValidateDataFormat()
    {
        $data = ['transformationItemsData'=>'transformationItemsData','incompleteItems'=>'incompleteItems'];
        $rule = new MockCompletionRule();

        $result = $rule->validateDataFormat($data);
        $this->assertTrue($result);
    }

    //transformationExecute
    public function testTransformationExecuteValidateFalse()
    {
        $data = ['data'];
        $transformationSearchData = new MockSearchData();

        $rule = $this->getMockBuilder(MockCompletionRule::class)
                           ->setMethods(
                               [
                                    'validateInitializationDataFormat'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateInitializationDataFormat')
                         ->with($data)
                         ->willReturn(false);

        $result = $rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals([], $result);
    }

    public function testTransformationExecuteValidateNotEmptyIdentify()
    {
        $data = [
            'transformationItemsData'=>['identify'=>'data'],
            'condition' => ['identify'=>array('condition')],
            'requiredItems' => ['requiredItems']
        ];

        $incompleteItems = ['incompleteItems'];

        $transformationSearchData = new MockSearchData();

        $rule = $this->getMockBuilder(MockCompletionRule::class)
                           ->setMethods(
                               [
                                    'validateInitializationDataFormat',
                                    'fetchIncompleteItems'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateInitializationDataFormat')
                         ->with($data)
                         ->willReturn(true);

        $rule->expects($this->exactly(1))
                         ->method('fetchIncompleteItems')
                         ->with($data['transformationItemsData'], $data['requiredItems'])
                         ->willReturn($incompleteItems);

        $expected = [
            'transformationItemsData' => $data['transformationItemsData'],
            'incompleteItems' => $incompleteItems
        ];

        $result = $rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals($expected, $result);
    }

    public function testTransformationExecuteValidateEmptyIdentify()
    {
        $data = [
            'transformationItemsData'=>['identify'=>''],
            'condition' => ['identify'=>[['item'=>'data']]],
            'requiredItems' => ['requiredItems']
        ];

        $searchDataList = ['searchDataList'];
        $incompleteItems = ['incompleteItems'];
        $completionItemData = 'completionItemData';

        $transformationSearchData = new MockSearchData();

        $rule = $this->getMockBuilder(MockCompletionRule::class)
                           ->setMethods(
                               [
                                    'validateInitializationDataFormat',
                                    'fetchIncompleteItems',
                                    'fetchSearchDataList',
                                    'filterExistItemsDataByIdentify'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateInitializationDataFormat')
                         ->with($data)
                         ->willReturn(true);

        $rule->expects($this->exactly(1))
                         ->method('fetchSearchDataList')
                         ->with($data['condition']['identify'][0], $transformationSearchData)
                         ->willReturn($searchDataList);

        $rule->expects($this->exactly(1))
                         ->method('filterExistItemsDataByIdentify')
                         ->with($searchDataList, $data['condition']['identify'][0]['item'])
                         ->willReturn($completionItemData);

        $rule->expects($this->exactly(1))
                         ->method('fetchIncompleteItems')
                         ->with(['identify'=>$completionItemData], $data['requiredItems'])
                         ->willReturn($incompleteItems);

        $expected = [
            'transformationItemsData' => ['identify'=>$completionItemData],
            'incompleteItems' => $incompleteItems
        ];

        $result = $rule->transformationExecute($data, $transformationSearchData);
        $this->assertEquals($expected, $result);
    }

    //transformationResult
    public function testTransformationResultValidateDataFormatFalse()
    {
        //初始化
        $data = ['data'];
        $transformationSearchData = new MockSearchData();
        $errorData = new ErrorData();

        $rule = $this->getMockBuilder(MockCompletionRule::class)
                           ->setMethods(
                               [
                                    'validateDataFormat',
                                    'failureDataException',
                                    'transformationDataResult'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateDataFormat')
                         ->willReturn(false);

        $rule->expects($this->exactly(1))
                         ->method('failureDataException')
                         ->with($transformationSearchData, ErrorData::STATUS['PROGRAM_EXCEPTION'])
                         ->willReturn($errorData);

        $rule->expects($this->exactly(1))
                         ->method('transformationDataResult')
                         ->with(array(), $errorData)
                         ->willReturn($errorData);

        $result = $rule->transformationResult($data, $transformationSearchData);
        $this->assertEquals($errorData, $result);
    }

    public function testTransformationResultValidateDataFormatTrueExistIncompleteItems()
    {
        //初始化
        $data = ['incompleteItems'=>['incompleteItems'],'transformationItemsData'=>['transformationItemsData']];
        $transformationSearchData= new MockSearchData();
        $this->transformationSearchData = $this->prophesize(SearchData::class);

        $this->transformationSearchData->getItemsData()
                                       ->shouldBeCalledTimes(1)->willReturn($transformationSearchData->getItemsData());
        $this->transformationSearchData->generateHash()->shouldBeCalledTimes(1);

        $rule = $this->getMockBuilder(MockCompletionRule::class)
                           ->setMethods(
                               [
                                    'validateDataFormat',
                                    'transformationDataResult'
                                ]
                           )
                           ->getMock();

        $rule->expects($this->exactly(1))
                         ->method('validateDataFormat')
                         ->willReturn(true);

        $rule->expects($this->exactly(1))
                         ->method('transformationDataResult')
                         ->with(
                             $data['incompleteItems'],
                             $transformationSearchData
                         )->willReturn($this->transformationSearchData->reveal());

        $result = $rule->transformationResult($data, $transformationSearchData);
        $this->assertEquals($this->transformationSearchData->reveal(), $result);
    }
}

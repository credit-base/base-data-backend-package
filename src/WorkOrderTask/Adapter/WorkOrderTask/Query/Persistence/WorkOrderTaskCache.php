<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class WorkOrderTaskCache extends Cache
{
    const KEY = 'work_order_task';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\Template\Model;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;

class MockGbTemplate extends GbTemplate
{
    public function getRepository() : IGbTemplateAdapter
    {
        return parent::getRepository();
    }
}

<?php
namespace BaseData\Common\Model;

trait NullApproveAbleTrait
{
    public function approve() : bool
    {
        return $this->resourceNotExist();
    }

    public function reject() : bool
    {
        return $this->resourceNotExist();
    }

    public function approveAction() : bool
    {
        return $this->resourceNotExist();
    }

    public function rejectAction() : bool
    {
        return $this->resourceNotExist();
    }

    public function isPending() : bool
    {
        return $this->resourceNotExist();
    }

    public function isApprove() : bool
    {
        return $this->resourceNotExist();
    }

    public function isReject() : bool
    {
        return $this->resourceNotExist();
    }

    abstract protected function resourceNotExist() : bool;
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;

class YsItemsDataTest extends TestCase
{
    private $ysItemsData;

    public function setUp()
    {
        $this->ysItemsData = new YsItemsData();
    }

    public function tearDown()
    {
        unset($this->ysItemsData);
    }

    public function testGetRepository()
    {
        $ysItemsData = new MockYsItemsData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter',
            $ysItemsData->getRepository()
        );
    }

    public function testAddSuccess()
    {
        $ysItemsData = $this->getMockBuilder(YsItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IYsItemsDataAdapter::class);
        $repository->add(Argument::exact($ysItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $ysItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $ysItemsData->add();
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        $ysItemsData = $this->getMockBuilder(YsItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IYsItemsDataAdapter::class);
        $repository->add(Argument::exact($ysItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        $ysItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $ysItemsData->add();
        $this->assertFalse($result);
    }
}

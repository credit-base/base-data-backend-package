<?php
namespace BaseData\Common\Model;

trait NullResubmitAbleTrait
{
    public function resubmit() : bool
    {
        return $this->resourceNotExist();
    }

    public function resubmitAction() : bool
    {
        return $this->resourceNotExist();
    }

    abstract protected function resourceNotExist() : bool;
}

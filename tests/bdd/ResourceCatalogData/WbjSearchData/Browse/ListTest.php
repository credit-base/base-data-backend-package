<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的委办局数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_wbj_items_data');
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data'],
            'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data']
        ]);
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchWbjSearchDataList()
    {
        $repository = new WbjSearchDataRepository();

        list($wbjSearchDataList, $count) = $repository->filter([]);
        
        unset($count);

        return $wbjSearchDataList;
    }

    /**
     * @Then: 我可以看到资源目录数据的全部信息
     */
    public function testViewWbjSearchDataList()
    {
        $setWbjSearchDataList = $this->wbjSearchData()['pcore_wbj_search_data'];

        foreach ($setWbjSearchDataList as $key => $setWbjSearchData) {
            unset($setWbjSearchData);
            $setWbjSearchDataList[$key]['wbj_search_data_id'] = $key +1;
        }

        $wbjSearchDataList = $this->fetchWbjSearchDataList();

        $translator = new WbjSearchDataDbTranslator();
        foreach ($wbjSearchDataList as $wbjSearchData) {
            $wbjSearchDataArray[] = $translator->objectToArray($wbjSearchData);
        }

        $this->assertEquals($wbjSearchDataArray, $setWbjSearchDataList);
    }
}

<?php
namespace BaseData\Template\CommandHandler\GbTemplate;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;

class MockEditGbTemplateCommandHandler extends EditGbTemplateCommandHandler
{
    public function getGbTemplateRepository() : IGbTemplateAdapter
    {
        return parent::getGbTemplateRepository();
    }
}

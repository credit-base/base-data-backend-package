<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\BjItemsData;

class BjItemsDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $bjItemsData = new BjItemsDataView(new BjItemsData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $bjItemsData);
    }
}

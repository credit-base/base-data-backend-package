<?php
namespace BaseData\Task\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Task\Model\Task;
use BaseData\Task\Model\NullTask;
use BaseData\Task\Model\IScheduleAble;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class TaskDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $task = null) : IScheduleAble
    {
        if (!isset($expression['task_id'])) {
            return NullTask::getInstance();
        }

        if ($task == null) {
            $identify = marmot_decode($expression['identify']);

            if (!class_exists($identify)) {
                return NullTask::getInstance();
            }
            $task = new $identify($expression['task_id']);
        }

        $task->setId($expression['task_id']);

        if (isset($expression['parent_id'])) {
            $task->setParentId($expression['parent_id']);
        }

        if (isset($expression['data'])) {
            $task->setData(unserialize($expression['data']));
        }

        if (isset($expression['next_schedule_time'])) {
            $task->setNextScheduleTime($expression['next_schedule_time']);
        }

        if (isset($expression['nice'])) {
            $task->setNice($expression['nice']);
        }

        if (isset($expression['status'])) {
            $task->setStatus($expression['status']);
        }

        if (isset($expression['pid'])) {
            $task->setPid($expression['pid']);
        }

        $task->setCreateTime($expression['create_time']);
        $task->setUpdateTime($expression['update_time']);
        $task->setStatusTime($expression['status_time']);

        return $task;
    }

    public function objectToArray($task, array $keys = array()) : array
    {
        if (!$task instanceof Task) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'parent',
                'identify',
                'type',
                'data',
                'nextScheduleTime',
                'nice',
                'pid',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['task_id'] = $task->getId();
        }
        
        if (in_array('parent', $keys)) {
            $expression['parent_id'] = $task->getParentId();
        }

        if (in_array('identify', $keys)) {
            $expression['identify'] = marmot_encode($task->getIdentify());
        }

        if (in_array('type', $keys)) {
            $expression['type'] = $task->getType();
        }

        if (in_array('data', $keys)) {
            $expression['data'] = serialize($task->getData());
        }

        if (in_array('nice', $keys)) {
            $expression['nice'] = $task->getNice();
        }

        if (in_array('pid', $keys)) {
            $expression['pid'] = $task->getPid();
        }

        if (in_array('nextScheduleTime', $keys)) {
            $expression['next_schedule_time'] = $task->getNextScheduleTime();
        }

        if (in_array('status', $keys)) {
            $expression['status'] = $task->getStatus();
        }

        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $task->getCreateTime();
        }

        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $task->getUpdateTime();
        }
        
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $task->getStatusTime();
        }

        return $expression;
    }
}

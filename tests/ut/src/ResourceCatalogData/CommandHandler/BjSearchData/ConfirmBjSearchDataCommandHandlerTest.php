<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Command\BjSearchData\ConfirmBjSearchDataCommand;

class ConfirmBjSearchDataCommandHandlerTest extends TestCase
{
    private $confirmStub;

    private $faker;

    public function setUp()
    {
        $this->confirmStub = $this->getMockBuilder(ConfirmBjSearchDataCommandHandler::class)
                                     ->setMethods(['fetchBjSearchData'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->confirmStub);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->confirmStub
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->confirmStub->execute($command);
    }

    public function testExecute()
    {
        $command = new ConfirmBjSearchDataCommand(
            $this->faker->randomNumber(1)
        );

        $bjSearchData = $this->prophesize(BjSearchData::class);
        $bjSearchData->confirm()->shouldBeCalledTimes(1)->willReturn(true);

        $this->confirmStub->expects($this->once())
             ->method('fetchBjSearchData')
             ->with($command->id)
             ->willReturn($bjSearchData->reveal());

        $result = $this->confirmStub->execute($command);
        
        $this->assertTrue($result);
    }
}

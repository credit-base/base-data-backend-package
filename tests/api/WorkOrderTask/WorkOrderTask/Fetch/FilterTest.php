<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\Model\IProgress;
use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\WorkOrderTaskUtils;
use BaseData\WorkOrderTask\Model\WorkOrderTask;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试工单任务接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WorkOrderTaskUtils, TemplateSetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_work_order_task');
    }

    /**
     * 存在多条工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_parent_task' => $this->parentTask(),
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_work_order_task' => $this->workOrderTask(),
            ]
        );
    }

    public function testViewWorkOrderTask()
    {
        $setWorkOrderTaskList = $this->workOrderTask();
        $setTemplateList = $this->bjTemplate();
        $setParentTaskList = $this->parentTask();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'workOrderTasks?filter[title]=归集企业基本信息&filter[assignObject]=1&filter[status]='
            .IProgress::STATUS['DQR'].'&filter[parentTask]=1&include=template,parentTask',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $workOrderTask) {
            $this->workOrderTaskCompareData($setWorkOrderTaskList[$key], $workOrderTask);
        }
        
        $this->assertEquals($setTemplateList[0]['bj_template_id'], $included[0]['id']);
        $this->assertEquals('bjTemplates', $included[0]['type']);
        $attributes = $included[0]['attributes'];
        $this->assertEquals($setTemplateList[0]['name'], $attributes['name']);

        $this->assertEquals($setParentTaskList[0]['parent_task_id'], $included[1]['id']);
        $this->assertEquals('parentTasks', $included[1]['type']);
        $attributes = $included[1]['attributes'];
        $this->assertEquals($setParentTaskList[0]['template_type'], $attributes['templateType']);
        $this->assertEquals($setParentTaskList[0]['title'], $attributes['title']);
        $this->assertEquals($setParentTaskList[0]['end_time'], $attributes['endTime']);
    }
}

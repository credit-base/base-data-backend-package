<?php
namespace BaseData\UserGroup\Adapter\UserGroup;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Model\NullUserGroup;
use BaseData\UserGroup\Translator\UserGroupRestfulTranslator;

use BaseData\Common\Adapter\FetchAbleRestfulAdapterTrait;

class UserGroupRestfulAdapter extends GuzzleAdapter implements IUserGroupAdapter
{
    use FetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'USER_GROUP_LIST'=>[
            'fields' => []
        ],
        'USER_GROUP_FETCH_ONE'=>[
            'fields' => []
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new UserGroupRestfulTranslator();
        $this->resource = 'userGroups';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }
    
    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne(int $id) : UserGroup
    {
        $this->get(
            $this->getResource().'/'.$id
        );

        return $this->isSuccess() ? $this->translateToObject() : NullUserGroup::getInstance();
    }
}

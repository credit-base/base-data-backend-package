<?php
namespace BaseData\Statistical\Adapter\Statistical;

use Marmot\Framework\Classes\MyPdo;

use BaseData\Statistical\Translator\EnterpriseRelationInformationCountTranslator;

class MockEnterpriseRelationInformationCountAdapter extends EnterpriseRelationInformationCountAdapter
{
    public function getEnterpriseRelationInformationCountTranslator() : EnterpriseRelationInformationCountTranslator
    {
        return parent::getEnterpriseRelationInformationCountTranslator();
    }

    public function getDbDriver() : MyPdo
    {
        return parent::getDbDriver();
    }
}

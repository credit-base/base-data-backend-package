<?php
namespace BaseData\Enterprise\Controller;

use PHPUnit\Framework\TestCase;

class EnterpriseFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(EnterpriseFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockEnterpriseFetchController();

        $this->assertInstanceOf(
            'BaseData\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockEnterpriseFetchController();

        $this->assertInstanceOf(
            'BaseData\Enterprise\View\EnterpriseView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockEnterpriseFetchController();

        $this->assertEquals(
            'enterprises',
            $controller->getResourceName()
        );
    }
}

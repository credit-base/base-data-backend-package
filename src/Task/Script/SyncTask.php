<?php
namespace BaseData\Task\Script;

use BaseData\Task\Model\OnceTask;

use BaseData\ResourceCatalogData\Service\SyncTaskService;

class SyncTask extends OnceTask
{
    protected function getSyncTaskService(string $fileName) : SyncTaskService
    {
        return new SyncTaskService($fileName);
    }

    protected function getSyncSubtask() : SyncSubtask
    {
        return new SyncSubtask();
    }

    protected function getSyncErrorDataDownload() : SyncErrorDataDownload
    {
        return new SyncErrorDataDownload();
    }

    public function executeAction() : bool
    {
        //获取文件名
        $fileName = current($this->getData());

        $syncTaskService = $this->getSyncTaskService($fileName);

        list($ruleServiceList, $task) = $syncTaskService->execute();

        if ($task->isFailureStatus()) {
            $syncErrorDataDownload = $this->getSyncErrorDataDownload();
            $syncErrorDataDownload->setData(array('task' => $task->getId()));
            $syncErrorDataDownload->add();
        }
        
        foreach ($ruleServiceList as $ruleService) {
            $syncSubtask = $this->getSyncSubtask();
            $syncSubtask->setData(array('task' => $task->getId(), 'ruleService' => $ruleService->getId()));
            $syncSubtask->add();
        }

        return true;
    }
}

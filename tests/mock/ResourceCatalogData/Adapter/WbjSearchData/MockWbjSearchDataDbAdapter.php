<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Repository\WbjItemsDataRepository;
use BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\WbjSearchDataRowCacheQuery;

use BaseData\Template\Repository\WbjTemplateRepository;

class MockWbjSearchDataDbAdapter extends WbjSearchDataDbAdapter
{
    public function getDbTranslator() : WbjSearchDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : WbjSearchDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getItemsDataRepository() : WbjItemsDataRepository
    {
        return parent::getItemsDataRepository();
    }

    public function getTemplateRepository() : WbjTemplateRepository
    {
        return parent::getTemplateRepository();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

<?php
namespace BaseData\Rule\RuleService\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\ResourceCatalogData\SetDataTrait as ResourceCatalogDataSetDataTrait;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Translator\RuleServiceDbTranslator;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限，当我想要查看目录规则列表时,
 *           在政务网OA中的资源目录规则管理模块中的资源目录规则列表正式表中,我可以查看所有通过审核的规则,通过点击资源目录名称或是查看,以便于我了解所有的目录规则
 * @Scenario: 查看规则列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, ResourceCatalogDataSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_bj_template');
    }

    /**
     * @Given: 存在规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_rule' => $this->rule(),
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_bj_template' => $this->bjTemplate(),
        ]);
    }

    /**
     * @When: 当我查看规则数据列表时
     */
    public function fetchRuleList()
    {
        $repository = new RuleServiceRepository();

        list($ruleList, $count) = $repository->filter([]);
        
        unset($count);

        return $ruleList;
    }

    /**
     * @Then: 我可以看到规则数据的全部信息
     */
    public function testViewRuleList()
    {
        $setRuleList = $this->rule();

        foreach ($setRuleList as $key => $setRule) {
            unset($setRule);
            $setRuleList[$key]['rule_id'] = $key +1;
        }

        $ruleList = $this->fetchRuleList();

        $translator = new RuleServiceDbTranslator();
        foreach ($ruleList as $rule) {
            $ruleArray[] = $translator->objectToArray($rule);
        }

        $this->assertEquals($ruleArray, $setRuleList);
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use PHPUnit\Framework\TestCase;

class EnableYsSearchDataCommandHandlerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnableYsSearchDataCommandHandler::class)
                       ->setMethods(['fetchYsSearchData'])
                       ->getMock();
    }

    public function testExtendsEnableCommandHandler()
    {
        $this->assertInstanceOf(
            'BaseData\Common\CommandHandler\EnableCommandHandler',
            $this->stub
        );
    }

    public function testFetchIEnableObject()
    {
        $id = 1;
        $ysSearchData = \BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory::generateYsSearchData($id);

        $this->stub->expects($this->once())
             ->method('fetchYsSearchData')
             ->with($id)
             ->willReturn($ysSearchData);

        $result = $this->stub->fetchIEnableObject($id);

        $this->assertEquals($result, $ysSearchData);
    }
}

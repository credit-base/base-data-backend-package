<?php
namespace BaseData\Enterprise\Adapter\Enterprise\Query;

use Marmot\Framework\Interfaces\DbLayer;
use Marmot\Interfaces\CacheLayer;

class MockEnterpriseRowCacheQuery extends EnterpriseRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

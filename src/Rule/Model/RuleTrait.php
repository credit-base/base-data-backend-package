<?php
namespace BaseData\Rule\Model;

use Marmot\Core;

use BaseData\Template\Model\Template;
use BaseData\Template\Repository\GbTemplateRepository;

use BaseData\Rule\Model\SearchDataModelFactory;
use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Repository\SearchDataRepositoryFactory;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\IncompleteData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait RuleTrait
{
    protected function getRepositoryFactory() : RepositoryFactory
    {
        return new RepositoryFactory();
    }
    
    protected function getGbTemplateRepository() : GbTemplateRepository
    {
        return new GbTemplateRepository();
    }

    protected function getGbSearchDataRepository() : GbSearchDataRepository
    {
        return new GbSearchDataRepository();
    }

    protected function getSearchDataRepositoryFactory() : SearchDataRepositoryFactory
    {
        return new SearchDataRepositoryFactory();
    }

    protected function getSearchDataModelFactory() : SearchDataModelFactory
    {
        return new SearchDataModelFactory();
    }
    /**
     * @todo 写一个 abstract 抽象方法，每个规则都要实现
     * templateItemComparison 和 comparisonRuleComparison 在对比规则和转换规则调用一致，但是
     * 方法命名差异太大
     */
    protected function templateItemComparison(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataTypeComparison($transformationItem, $sourceItem)
            && $this->dataLengthComparison($transformationItem, $sourceItem)
            && $this->dataDimensionComparison($transformationItem, $sourceItem)
            && $this->dataIsMaskedComparison($transformationItem, $sourceItem);
    }

    //comparisonItemComparison
    protected function comparisonRuleComparison(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataTypeComparison($transformationItem, $sourceItem)
            && $this->dataLengthComparison($transformationItem, $sourceItem);
    }

    //数据类型对应关系(目标:来源): 日期:日期,字符串:全部,集合:集合-枚举,枚举:枚举,整型:整型,浮点:浮点-整型
    protected function dataTypeComparison(array $transformationItem, array $sourceItem) : bool
    {
        $transformationType = isset($transformationItem['type']) ? $transformationItem['type'] : 0;
        $sourceType = isset($sourceItem['type']) ? $sourceItem['type'] : 0;

        //验证目标类型是否存在
        if (!array_key_exists($transformationType, IRule::TEMPLATE_ITEM_TYPE_MAPPING)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'transformationItemTypeNotExist'));
            return false;
        }
        //获取目标类型对应的来源类型
        $typeMapping = IRule::TEMPLATE_ITEM_TYPE_MAPPING[$transformationType];
        //判断来源类型是否在数组范围内
        if (!in_array($sourceType, $typeMapping)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'typeCannotTransformation'));
            return false;
        }

        return true;
    }

    //数据长度: 目标>=来源(全部),目标项包含来源(集合-枚举)
    protected function dataLengthComparison(array $transformationItem, array $sourceItem) : bool
    {
        $transformationLength = isset($transformationItem['length']) ? $transformationItem['length'] : 0;
        $sourceLength = isset($sourceItem['length']) ? $sourceItem['length'] : 0;
        //验证长度
        if ($transformationLength < $sourceLength) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'lengthCannotTransformation'));
            return false;
        }

        //验证包含关系
        $transformationType = isset($transformationItem['type']) ? $transformationItem['type'] : 0;
        if ($transformationType == Template::TYPE['MJX'] || $transformationType == Template::TYPE['JHX']) {
            return $this->dataOptionComparison($transformationItem, $sourceItem);
        }

        return true;
    }

    //验证数据是否在范围内
    protected function dataOptionComparison(array $transformationItem, array $sourceItem) : bool
    {
        $transformationOptions = isset($transformationItem['options']) ? $transformationItem['options'] : array();
        $sourceOptions = isset($sourceItem['options']) ? $sourceItem['options'] : array();
        //循环来源可选范围,验证是否存在
        foreach ($sourceOptions as $sourceOption) {
            if (!in_array($sourceOption, $transformationOptions)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'lengthCannotTransformation'));
                return false;
            }
        }

        return true;
    }

    //公开范围: 目标>=来源,目标:来源 社会公开(1):社会公开(1), 政务共享(2):社会公开(1)-政务共享(2), 授权查询(3):社会公开(1)-政务共享(2)-授权查询(3)
    protected function dataDimensionComparison(array $transformationItem, array $sourceItem) : bool
    {
        $transformationDimension = isset($transformationItem['dimension']) ? $transformationItem['dimension'] : 0;
        $sourceDimension = isset($sourceItem['dimension']) ? $sourceItem['dimension'] : 0;
        
        if ($transformationDimension < $sourceDimension) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'dimensionCannotTransformation'));
            return false;
        }

        return true;
    }

    //是否脱敏: 目标>来源, 目标: 来源  脱敏(1): 脱敏(1),脱敏(1):不脱敏(0), isMasked(0 不脱敏 , 1 脱敏)
    //脱敏规则: 来源左边大于等于目标,来源右边大于等于目标
    protected function dataIsMaskedComparison(array $transformationItem, array $sourceItem) : bool
    {
        $transformationIsMasked = isset($transformationItem['isMasked']) ? $transformationItem['isMasked'] : 0;
        $sourceIsMasked = isset($sourceItem['isMasked']) ? $sourceItem['isMasked'] : 0;
        
        if ($transformationIsMasked < $sourceIsMasked) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'isMaskedCannotTransformation'));
            return false;
        }

        if ($transformationIsMasked == Template::IS_MASKED['SHI'] && $sourceIsMasked == Template::IS_MASKED['SHI']) {
            return $this->dataMaskRuleComparison($transformationItem, $sourceItem);
        }

        return true;
    }
    
    //脱敏规则: 来源左边大于等于目标,来源右边大于等于目标
    protected function dataMaskRuleComparison(array $transformationItem, array $sourceItem) : bool
    {
        $transformationMaskRule = isset($transformationItem['maskRule']) ?
        $transformationItem['maskRule'] :
        array();
        $sourceMaskRule = isset($sourceItem['maskRule']) ? $sourceItem['maskRule'] : array();

        if (current($transformationMaskRule) > current($sourceMaskRule)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'maskRuleCannotTransformation'));
            return false;
        }

        if (end($transformationMaskRule) > end($sourceMaskRule)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'maskRuleCannotTransformation'));
            return false;
        }

        return true;
    }

    //获取资源目录的模板信息项
    protected function fetchTemplateItems(Template $template) : array
    {
        $items = $template->getItems();
        $templateItems = array();

        foreach ($items as $item) {
            $templateItems[$item['identify']] = $item;
        }

        return $templateItems;
    }

    //获取比对/补全的资源目录集合
    protected function fetchTemplateList(array $condition) : array
    {
        $templateIds = array();
        //获取资源目录ids集合
        foreach ($condition as $patterns) {
            foreach ($patterns as $pattern) {
                $templateIds[] = $pattern['id'];
            }
        }

        return $this->getGbTemplateRepository()->fetchList($templateIds);
    }

    //如果比对/补全资源目录为自然人,依据必须包含身份证号
    protected function validateBase(Template $template, array $base) : bool
    {
        $subjectCategory = $template->getSubjectCategory();
        if (in_array(Template::SUBJECT_CATEGORY['ZRR'], $subjectCategory)) {
            if (!in_array(IRule::COMPLETION_BASE['IDENTIFY'], $base)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'zrrNotIncludeIdentify'));
                return false;
            }
        }

        return true;
    }

    //初始化目标资源目录
    protected function initializationTransformationSearchData(
        ISearchDataAble $sourceSearchData,
        Template $transformationTemplate
    ) : ISearchDataAble {
        $transformationSearchData = new IncompleteData();

        $transformationSearchData = $this->generateCommonSearchData(
            $sourceSearchData,
            $transformationSearchData,
            $transformationTemplate
        );
        
        $transformationSearchData->setCategory($transformationTemplate->getCategory());
        $failureItems = $this->transformationTemplateRequired($transformationTemplate);
        
        return $this->errorInfo($transformationSearchData, ErrorData::ERROR_TYPE['MISSING_DATA'], $failureItems);
    }

    //初始化目标资源目录
    protected function generateCommonSearchData(
        ISearchDataAble $searchData,
        ISearchDataAble $transformationSearchData,
        Template $transformationTemplate
    ) : ISearchDataAble {
        $transformationSearchData->setCrew($searchData->getCrew());
        $transformationSearchData->setSourceUnit($searchData->getSourceUnit());
        $transformationSearchData->setExpirationDate($searchData->getExpirationDate());
        $transformationSearchData->setTemplate($transformationTemplate);
        $transformationSearchData->setInfoClassify($transformationTemplate->getInfoClassify());
        $transformationSearchData->setInfoCategory($transformationTemplate->getInfoCategory());
        $transformationSearchData->setSubjectCategory($searchData->getSubjectCategory());
        $transformationSearchData->setDimension($transformationTemplate->getDimension());
       
        return $transformationSearchData;
    }

    //初始化失败资源目录
    protected function generateErrorData(
        ISearchDataAble $errorData,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        $errorData->setStatus($transformationSearchData->getStatus());
        $errorData->setErrorType($transformationSearchData->getErrorType());
        $errorData->setErrorReason($transformationSearchData->getErrorReason());
        $errorData->setCategory($transformationSearchData->getTemplate()->getCategory());

        return $errorData;
    }
    //获取资源目录数据模型
    protected function getSearchData(Template $template)
    {
        return $this->getSearchDataModelFactory()->getModel($template->getCategory());
    }

    //将来源资源目录数据赋值目标资源目录数据
    protected function fetchTransformationSearchData(
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        $perfectSearchData = $this->getSearchData(
            $transformationSearchData->getTemplate()
        );

        $perfectSearchData = $this->generateCommonSearchData(
            $transformationSearchData,
            $perfectSearchData,
            $transformationSearchData->getTemplate()
        );
        return $perfectSearchData;
    }

    //将来源资源目录数据转换为失败资源目录数据
    protected function fetchFailureData(
        ISearchDataAble $transformationSearchData,
        int $errorType,
        array $failureItems
    ) : FailureData {
        $failureData = new FailureData();
        $failureData = $this->generateCommonSearchData(
            $transformationSearchData,
            $failureData,
            $transformationSearchData->getTemplate()
        );
        $failureData->getItemsData()->setData($transformationSearchData->getItemsData()->getData());
        $failureData->setHash($transformationSearchData->getHash());

        if ($transformationSearchData instanceof ErrorData) {
            $failureData = $this->generateErrorData(
                $failureData,
                $transformationSearchData
            );
        }

        $failureData = $this->errorInfo($failureData, $errorType, $failureItems);

        return $failureData;
    }

    //将来源资源目录数据转换为待补全资源目录数据
    protected function fetchIncompleteData(
        ISearchDataAble $transformationSearchData,
        int $errorType,
        array $failureItems
    ) : IncompleteData {
        $incompleteData = new IncompleteData();
        $incompleteData = $this->generateCommonSearchData(
            $transformationSearchData,
            $incompleteData,
            $transformationSearchData->getTemplate()
        );

        if ($transformationSearchData instanceof ErrorData) {
            $incompleteData = $this->generateErrorData(
                $incompleteData,
                $transformationSearchData
            );
        }
        
        $incompleteData = $this->errorInfo($incompleteData, $errorType, $failureItems);

        return $incompleteData;
    }

    //对失败数据赋值失败原因和失败类型
    protected function errorInfo(
        ErrorData $errorData,
        int $errorType,
        array $failureItems
    ) : ErrorData {
        $errorData->setCategory($errorData->getTemplate()->getCategory());

        $errorItems = array();

        foreach ($failureItems as $identify => $failureItem) {
            unset($failureItem);
            $errorItems[] = $identify;
        }

        $errorReason = $errorData->getErrorReason();
        $errorReason[$errorType] = $errorItems;

        $errorData->setErrorReason($errorReason);

        $errorType = array_sum(array_keys($errorReason));

        $errorData->setErrorType($errorType);

        return $errorData;
    }

    //获取目标资源目录必填项
    protected function transformationTemplateRequired(Template $transformationTemplate) : array
    {
        //获取目标资源目录信息项
        $templateItems = $this->fetchTemplateItems($transformationTemplate);
        $requiredItems = array();

        foreach ($templateItems as $item) {
            if ($item['isNecessary'] == Template::IS_NECESSARY['SHI']) {
                $requiredItems[$item['identify']] = $item;
            }
        }

        return $requiredItems;
    }
    //获取资源目录数据仓库
    protected function getSearchDataRepository(ISearchDataAble $searchData)
    {
        return $this->getSearchDataRepositoryFactory()->getRepository($searchData->getTemplate()->getCategory());
    }
    //获取补全/比对资源目录数据
    protected function fetchSearchDataList(array $pattern, ISearchDataAble $transformationSearchData) : array
    {
        //获取仓库
        //获取filter条件
        $filter = $this->filterFormatChange($pattern, $transformationSearchData);

        //获取数据
        list($list, $count) = $this->getGbSearchDataRepository()->filter($filter);

        unset($count);

        return $list;
    }

    protected function filterFormatChange(array $pattern, ISearchDataAble $transformationSearchData) : array
    {
        $filter = array();
        //获取filter条件
        if (in_array(IRule::COMPLETION_BASE['NAME'], $pattern['base'])) {
            $filter['name'] = $transformationSearchData->getName();
        }

        if (in_array(IRule::COMPLETION_BASE['IDENTIFY'], $pattern['base'])) {
            $filter['identify'] = $transformationSearchData->getIdentify();
        }

        $filter['template'] = $pattern['id'];

        return $filter;
    }

    //获取待补全的数据信息项
    protected function fetchIncompleteItems(
        array $transformationItemsData,
        array $requiredItems
    ) : array {
        $incompleteItems = array();

        //获取必填信息项中的标识集合
        $requiredItemsIdentify = $this->fetchItemsIdentify($requiredItems);

        //通过标识返回待补全的信息项
        foreach ($requiredItemsIdentify as $identify) {
            if (!isset($transformationItemsData[$identify]) || empty($transformationItemsData[$identify])) {
                $incompleteItems[$identify] = $requiredItems[$identify];
            }
        }

        return $incompleteItems;
    }

    //获取资源目录信息项标识集合
    protected function fetchItemsIdentify(array $items) : array
    {
        $itemsIdentify = array();
        foreach ($items as $identify => $item) {
            unset($item);

            $itemsIdentify[] = $identify;
        }

        return $itemsIdentify;
    }

    //根据比对/补全的资源目录数据列表获取待补全/带比对资源目录信息项的数据
    protected function filterExistItemsDataByIdentify(array $searchDataList, string $identify)
    {
        foreach ($searchDataList as $searchData) {
            $itemsData = $searchData->getItemsData()->getData();
            if (isset($itemsData[$identify]) && !empty($itemsData[$identify])) {
                return $itemsData[$identify];
            }
        }

        return '';
    }

    protected function generateHash(array $itemsData) : string
    {
        return md5(serialize($itemsData));
    }

    //待补全的资源目录数据
    protected function incompleteData(
        ISearchDataAble $transformationSearchData,
        int $type,
        array $incompleteItems
    ) : ISearchDataAble {
        return $this->fetchIncompleteData(
            $transformationSearchData,
            $type,
            $incompleteItems
        );
    }

    //完善的资源目录数据
    protected function transformationSearchData(ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        return $this->fetchTransformationSearchData($transformationSearchData);
    }

    //失败的资源目录数据
    protected function failureData(
        ISearchDataAble $transformationSearchData,
        int $errorType,
        array $failureItems
    ) : ISearchDataAble {
        return $this->fetchFailureData(
            $transformationSearchData,
            $errorType,
            $failureItems
        );
    }

    //失败的资源目录数据-异常情况
    protected function failureDataException(
        ISearchDataAble $transformationSearchData,
        int $status
    ) : ISearchDataAble {
        $failureData = new FailureData();
        $failureData = $this->generateCommonSearchData(
            $transformationSearchData,
            $failureData,
            $transformationSearchData->getTemplate()
        );
        $failureData->getItemsData()->setData($transformationSearchData->getItemsData()->getData());
        $failureData->setHash($transformationSearchData->getHash());
        $failureData->setCategory($transformationSearchData->getTemplate()->getCategory());

        if ($transformationSearchData instanceof ErrorData) {
            $failureData = $this->generateErrorData(
                $failureData,
                $transformationSearchData
            );
        }

        $failureData->setStatus($status);

        return $failureData;
    }

    /**
     * 对失败原因进行格式的转换
        * 因规则是一个个执行,所以在记录失败原因是"失败类型=>失败资源目录模板信息项标识集合"这样存储更方便,但对于前端使用时,该存储方式并不方便处理, 所以此处将数据格式转换为"失败资源目录模板信息项标识=>[失败类型]"
     * @param ErrorData $errorData 失败数据信息
     * @return ErrorData
     */
    protected function errorReasonFormatConversion(ErrorData $errorData) : ErrorData
    {
        $errorReasonOld = $errorData->getErrorReason();

        $errorReason = array();
        
        foreach ($errorReasonOld as $errorType => $identifies) {
            foreach ($identifies as $identify) {
                $errorReason[$identify][] = $errorType;
            }
        }

        $errorData->setErrorReason($errorReason);

        return $errorData;
    }
    /**
     * 判断并返回结果,此方法只是因为转换和补全规则处理方式一致,提出的公共方法
     * @param array $incompleteItems 待补全的资源目录模板信息项
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return ISearchDataAble
     */
    protected function transformationDataResult(
        array $incompleteItems,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        //如果待补全为空且目标资源目录数据不是失败资源目录数据,则将目标资源目录数据转为完善的资源目录数据
        if (empty($incompleteItems) && !$transformationSearchData instanceof FailureData) {
            $transformationSearchData = $this->transformationSearchData($transformationSearchData);
        }

        //如果待补全为空且目标资源目录数据是失败资源目录数据,则直接返回
        if (empty($incompleteItems) && $transformationSearchData instanceof FailureData) {
            $transformationSearchData = $transformationSearchData;
        }

        //如果存在待补全的资源目录信息项则返回待补全的资源目录数据
        if (!empty($incompleteItems)) {
            $transformationSearchData =  $this->incompleteData(
                $transformationSearchData,
                ErrorData::ERROR_TYPE['MISSING_DATA'],
                $incompleteItems
            );
        }

        return $transformationSearchData;
    }
}

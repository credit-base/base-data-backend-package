<?php
namespace BaseData\WorkOrderTask\ParentTask\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;

/**
 * @Feature: 我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 撤销父任务
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
    }

    /**
     * @Given: 存在需要撤销的父任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_parent_task' => $this->parentTask()
        ]);
    }

    /**
     * @When: 获取需要撤销的父任务
     */
    public function fetchParentTask($id)
    {
        $repository = new ParentTaskRepository();

        $parentTask = $repository->fetchOne($id);

        return $parentTask;
    }

    /**
     * @And: 当我调用撤销函数,期待返回true
     */
    public function revoke()
    {
        $parentTask = $this->fetchParentTask(1);
        
        return $parentTask->revoke('reason');
    }

    /**
     * @Then: 数据已经被撤销
     */
    public function testValidate()
    {
        $result = $this->revoke();

        $this->assertTrue($result);

        $parentTask = $this->fetchParentTask(1);

        $this->assertEquals('reason', $parentTask->getReason());
        $this->assertEquals(1, $parentTask->getFinishCount());
        $this->assertEquals(Core::$container->get('time'), $parentTask->getUpdateTime());
    }
}

<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;

class MockYsSearchDataRepository extends YsSearchDataRepository
{
    public function getAdapter() : IYsSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IYsSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IYsSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}

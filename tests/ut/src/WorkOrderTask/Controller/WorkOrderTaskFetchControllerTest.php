<?php
namespace BaseData\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseData\WorkOrderTask\Model\NullWorkOrderTask;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\View\WorkOrderTaskView;

class WorkOrderTaskFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WorkOrderTaskFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockWorkOrderTaskFetchController();

        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockWorkOrderTaskFetchController();

        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\View\WorkOrderTaskView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockWorkOrderTaskFetchController();

        $this->assertEquals(
            'workOrderTasks',
            $controller->getResourceName()
        );
    }
}

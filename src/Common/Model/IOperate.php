<?php
namespace BaseData\Common\Model;

interface IOperate
{
    public function add() : bool;
    
    public function edit() : bool;
}

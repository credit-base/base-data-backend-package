<?php
namespace BaseData\Rule\Command\RuleService;

use BaseData\Common\Command\ResubmitCommand;

class ResubmitRuleServiceCommand extends ResubmitCommand
{
    public $rules;

    public $crew;

    public function __construct(
        array $rules,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        
        $this->rules = $rules;
        $this->crew = $crew;
    }
}

<?php
namespace BaseData\Template\Translator;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\NullGbTemplate;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class GbTemplateDbTranslator extends TemplateDbTranslator
{
    public function arrayToObject(array $expression, $gbTemplate = null) : GbTemplate
    {
        if (!isset($expression['gb_template_id'])) {
            return NullGbTemplate::getInstance();
        }

        if ($gbTemplate == null) {
            $gbTemplate = new GbTemplate();
        }

        $gbTemplate->setId($expression['gb_template_id']);
        $gbTemplate = parent::arrayToObject($expression, $gbTemplate);

        return $gbTemplate;
    }

    public function objectToArray($gbTemplate, array $keys = array()) : array
    {
        if (!$gbTemplate instanceof GbTemplate) {
            return [];
        }

        $templateExpression = parent::objectToArray($gbTemplate, $keys);
        
        if (empty($keys)) {
            $keys = array(
                'id'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['gb_template_id'] = $gbTemplate->getId();
        }

        $expression = array_merge($expression, $templateExpression);
        
        return $expression;
    }
}

<?php
namespace BaseData\Rule\RuleService\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\Repository\RuleServiceRepository;

/**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有资源目录规则管理权限，当我想要查看目录规则列表时,
 *           在政务网OA中的资源目录规则管理模块中的资源目录规则列表正式表中,我可以查看所有通过审核的规则,通过点击资源目录名称或是查看,以便于我了解所有的目录规则
 * @Scenario: 查看规则列表-异常列表不存在
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在规则
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看规则列表时
     */
    public function fetchRuleList()
    {
        $repository = new RuleServiceRepository();

        list($ruleList, $count) = $repository->filter([]);
        
        unset($count);

        return $ruleList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewRule()
    {
        $ruleList = $this->fetchRuleList();

        $this->assertEmpty($ruleList);
    }
}

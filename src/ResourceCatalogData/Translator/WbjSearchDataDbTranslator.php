<?php
namespace BaseData\ResourceCatalogData\Translator;

use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Model\NullWbjSearchData;

class WbjSearchDataDbTranslator extends SearchDataDbTranslator
{
    public function arrayToObject(array $expression, $wbjSearchData = null)
    {
        if (!isset($expression['wbj_search_data_id'])) {
            return NullWbjSearchData::getInstance();
        }

        if ($wbjSearchData == null) {
            $wbjSearchData = new WbjSearchData();
        }

        $wbjSearchData->setId($expression['wbj_search_data_id']);

        if (isset($expression['wbj_template_id'])) {
            $wbjSearchData->getTemplate()->setId($expression['wbj_template_id']);
        }

        if (isset($expression['wbj_items_data_id'])) {
            $wbjSearchData->getItemsData()->setId($expression['wbj_items_data_id']);
        }

        $wbjSearchData = parent::arrayToObject($expression, $wbjSearchData);
        
        return $wbjSearchData;
    }

    public function objectToArray($wbjSearchData, array $keys = array())
    {
        $searchDataExpression = parent::objectToArray($wbjSearchData, $keys);

        if (!$wbjSearchData instanceof WbjSearchData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'template',
                'itemsData'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['wbj_search_data_id'] = $wbjSearchData->getId();
        }
        if (in_array('template', $keys)) {
            $expression['wbj_template_id'] = $wbjSearchData->getTemplate()->getId();
        }
        if (in_array('itemsData', $keys)) {
            $expression['wbj_items_data_id'] = $wbjSearchData->getItemsData()->getId();
        }

        $expression = array_merge($expression, $searchDataExpression);

        return $expression;
    }
}

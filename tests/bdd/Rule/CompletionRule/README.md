# 补全规则BDD测试

## 测试用例

* 补全成功
	* 补全必填项
	* 补全非必填项
* 补全失败
	* 补全项未匹配

### 1. 补全成功-必填项

#### 1.1 背景

根据统一社会信用代码补全行政相对人名称

#### 1.2 数据准备

#### 1.2.1 规则

* 转换规则没有转换行政相对人名称 **必填**
* 依据"统一社会信用代码"(`base:2`), 从"法人登记信息"(`id:2`)中补全字段`item:ZTMC`
* 默认数据可以匹配

```php
$rules = ['transformationRule'=>[],'completionRule'=>[]];
//转换规则
$rules['transformationRule'] = [
    // "ZTMC" => "ZTMC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "TYSHXYDM" => "TYSHXYDM",//2
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    "BZ" => "BZ"//27
];

$rules['completionRule'] = [
    'ZTMC' => ['id'=>2, 'base'=>[2], 'item'=>'ZTMC']//补全资源目录:id-2-法人登记信息, 依据:统一社会信用代码, 补全字段:主体名称
];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    );
//测试结果-GbSearchData
array(
	'infoClassify' => 1,
	'infoCategory' => 1,
	'crewId' => 1,
	'sourceUnitId' => 1,
	'subjectCategory' => 1,
	'dimension' => 1,
	'name' => '濮阳市嘉业运输有限公司',
	'identify' => '91410900MA46184E7Q',
	'expirationDate' => 20991231,
	'hash' => 'f7008574848bca7580350fbcc913ddf6',
	'templateId' => 1,
	'itemsData' => array(
		'ZTMC' => "濮阳市嘉业运输有限公司",
		'XK_XDR_LB' => "法人及非法人组织",
		'TYSHXYDM' => "91410900MA46184E7Q",
		'XK_XDR_GSZC' => "91410900MA46184E7Q",
		'XK_XDR_ZZJG' => "91410900MA46184E7Q",
		'XK_XDR_SWDJ' => "91410900MA46184E7Q",
		'XK_XDR_SYDW' => "91410900MA46184E7Q",
		'XK_XDR_SHZZ' => "MA46184E-7",
		'XK_FRDB' => "张晓苹",
		'XK_FR_ZJLX' => "身份证号",
		'XK_FR_ZJHM' => "412825199809097654",
		'XK_XDR_ZJLX' => "身份证号",
		'XK_XDR_ZJHM' => "412825199809097654",
		'XK_XKWS' => "高速公路联网管理中心交通行政许可决定书",
		'XK_WSH' => "豫高速公路许 字第20210705000090号",
		'XK_XKLB' => "普通",
		'XK_XKZS' => "高速公路超限运输车辆通行证",
		'XK_XKBH' => "410000017210705A000090",
		'XK_NR' => "高速公路超限运输车辆通行证",
		'XK_JDRQ' => "20210714",
		'XK_YXQZ' => "20210714",
		'XK_YXQZI' => "20210807",
		'XK_XKJG' => "河南省高速公路联网管理中心",
		'XK_XKJGDM' => "12410000MB14527979",
		'XK_ZT' => "1",
		'XK_LYDW' => "河南省高速公路联网管理中心",
		'XK_LYDWDM' => "12410000MB14527979",
		'BZ' => ""
	),
	'statusTime' => 0,
	'status' => 0,
	'createTime' => '1626506618',
	'updateTime' => '1626506618'
);
```

#### 1.2.2 行政许可 <font color='red'>委办局</font>

默认数据

#### 1.2.3 法人登记信息 <font color='red'>国标</font>

默认数据

### 2. 补全成功-非必填项

#### 2.1 背景

行政许可没有**备注**，根据统一社会信用代码从法人登记信息表补全**登记机关**

#### 2.2 数据准备

#### 2.2.1 规则

* 转换规则没有转换备注 **非必填**
* 依据"统一社会信用代码"(`base:2`), 从"法人登记信息"(`id:2`)中补全字段`item:DJJG `
* 默认数据可以匹配

```php
$rules = ['transformationRule'=>[],'completionRule'=>[]];
//转换规则
$rules['transformationRule'] = [
    "ZTMC" => "ZTMC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "TYSHXYDM" => "TYSHXYDM",//2
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    // "BZ" => "BZ"//27
];

$rules['completionRule'] = [
    'BZ' => ['id'=>2, 'base'=>[2], 'item'=>'DJJG']//补全资源目录:id-2-法人登记信息, 依据:统一社会信用代码, 补全字段:主体名称
];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    );
//测试结果-GbSearchData
array(
	'infoClassify' => 1,
	'infoCategory' => 1,
	'crewId' => 1,
	'sourceUnitId' => 1,
	'subjectCategory' => 1,
	'dimension' => 1,
	'name' => '濮阳市嘉业运输有限公司',
	'identify' => '91410900MA46184E7Q',
	'expirationDate' => 20991231,
	'hash' => 'c3c82613915503575122d40a115cf884',
	'templateId' => 1,
	'itemsData' => array(
		'ZTMC' => "濮阳市嘉业运输有限公司",
		'XK_XDR_LB' => "法人及非法人组织",
		'TYSHXYDM' => "91410900MA46184E7Q",
		'XK_XDR_GSZC' => "91410900MA46184E7Q",
		'XK_XDR_ZZJG' => "91410900MA46184E7Q",
		'XK_XDR_SWDJ' => "91410900MA46184E7Q",
		'XK_XDR_SYDW' => "91410900MA46184E7Q",
		'XK_XDR_SHZZ' => "MA46184E-7",
		'XK_FRDB' => "张晓苹",
		'XK_FR_ZJLX' => "身份证号",
		'XK_FR_ZJHM' => "412825199809097654",
		'XK_XDR_ZJLX' => "身份证号",
		'XK_XDR_ZJHM' => "412825199809097654",
		'XK_XKWS' => "高速公路联网管理中心交通行政许可决定书",
		'XK_WSH' => "豫高速公路许 字第20210705000090号",
		'XK_XKLB' => "普通",
		'XK_XKZS' => "高速公路超限运输车辆通行证",
		'XK_XKBH' => "410000017210705A000090",
		'XK_NR' => "高速公路超限运输车辆通行证",
		'XK_JDRQ' => "20210714",
		'XK_YXQZ' => "20210714",
		'XK_YXQZI' => "20210807",
		'XK_XKJG' => "河南省高速公路联网管理中心",
		'XK_XKJGDM' => "12410000MB14527979",
		'XK_ZT' => "1",
		'XK_LYDW' => "河南省高速公路联网管理中心",
		'XK_LYDWDM' => "12410000MB14527979",
		'BZ' => "彭州市市场监督管理局"
	),
	'statusTime' => 0,
	'status' => 0,
	'createTime' => '1626506618',
	'updateTime' => '1626506618'
);
```

#### 2.2.2 行政许可 <font color='red'>委办局</font>

默认数据

#### 2.2.3 法人登记信息 <font color='red'>国标</font>

默认数据

### 3. 补全项未匹配

#### 3.1 背景

根据统一社会信用代码补全行政相对人名称，但是根据统一社会信用代码查询没有匹配项

#### 3.2 数据准备

#### 3.2.1 规则

* 转换规则没有转换行政相对人名称 **必填**
* 依据"统一社会信用代码"(`base:2`), 从"法人登记信息"(`id:2`)中补全字段`item:ZTMC`
* 默认数据不能匹配

```php
$rules = ['transformationRule'=>[],'completionRule'=>[]];
//转换规则
$rules['transformationRule'] = [
    // "ZTMC" => "ZTMC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "TYSHXYDM" => "TYSHXYDM",//2
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    "BZ" => "BZ"//27
];

$rules['completionRule'] = [
    'ZTMC' => ['id'=>2, 'base'=>[2], 'item'=>'ZTMC']//补全资源目录:id-2-法人登记信息, 依据:统一社会信用代码, 补全依据:主体名称
];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    );
//测试结果-IncompleteSearchData
array(
	'errorType'=>2,
	'errorReason'=>'行政相对人名称,法定代表人证件类型比对失败',
	'type'=>2,
	'infoClassify' => 1,
	'infoCategory' => 1,
	'crewId' => 1,
	'sourceUnitId' => 1,
	'subjectCategory' => 1,
	'dimension' => 1,
	'name' => '濮阳市嘉业运输有限公司',
	'identify' => '91410900MA46184E7Q',
	'expirationDate' => 20991231,
	'hash' => '7023004a14b5c690d99c54b0b9bcc22d',
	'templateId' => 1,
	'itemsData' => array(
		'ZTMC' => "",
		'XK_XDR_LB' => "法人及非法人组织",
		'TYSHXYDM' => "91410900MA46184E7Q",
		'XK_XDR_GSZC' => "91410900MA46184E7Q",
		'XK_XDR_ZZJG' => "91410900MA46184E7Q",
		'XK_XDR_SWDJ' => "91410900MA46184E7Q",
		'XK_XDR_SYDW' => "91410900MA46184E7Q",
		'XK_XDR_SHZZ' => "MA46184E-7",
		'XK_FRDB' => "张晓苹",
		'XK_FR_ZJLX' => "",
		'XK_FR_ZJHM' => "412825199809097654",
		'XK_XDR_ZJLX' => "身份证号",
		'XK_XDR_ZJHM' => "412825199809097654",
		'XK_XKWS' => "高速公路联网管理中心交通行政许可决定书",
		'XK_WSH' => "豫高速公路许 字第20210705000090号",
		'XK_XKLB' => "普通",
		'XK_XKZS' => "高速公路超限运输车辆通行证",
		'XK_XKBH' => "410000017210705A000090",
		'XK_NR' => "高速公路超限运输车辆通行证",
		'XK_JDRQ' => "20210714",
		'XK_YXQZ' => "20210714",
		'XK_YXQZI' => "20210807",
		'XK_XKJG' => "河南省高速公路联网管理中心",
		'XK_XKJGDM' => "12410000MB14527979",
		'XK_ZT' => "1",
		'XK_LYDW' => "河南省高速公路联网管理中心",
		'XK_LYDWDM' => "12410000MB14527979",
		'BZ' => ""
	),
	'statusTime' => 0,
	'status' => 0,
	'createTime' => '1626506618',
	'updateTime' => '1626506618'
);
```

#### 3.2.2 行政许可 <font color='red'>委办局</font>

默认数据

#### 3.2.3 法人登记信息 <font color='red'>国标</font>

<font color="red">修改</font>法人登记信息数据

* 主体名称 与 行政许可数据 <font color='red'>不匹配</font>
* 统一社会信用代码 <font color='red'>不匹配</font>

数据项 | 数据 | 备注
----- | ---- | -----
主体名称| 彭州市衡粤旅游发展有限公司| <font color='red'>不匹配</font>|
主体类别 | 企业法人| |
统一社会信用代码 | 91510182MA6C4X1D3B |<font color='red'>不匹配</font> |
法定代表人（负责人） | 张晓苹 | |
法定代表人（负责人）证件类型 | 身份证 |
法定代表人（负责人）证件号码 | 612301197209212055 |
成立日期 | 20171013| |
有效期 |20501013 | |
地址 |四川省彭州市桂花镇衡州村7组 | |
登记机关 |彭州市市场监督管理局 | |
国别(地区) |A24 | |
注册资本 |500 | |
注册资本币种 | 156| |
行业代码 |80 | |
类型 | N| |
经营范围 | 水利、环境和公共设施管理业| |
经营状态 | 1| |
经营范围描述 |旅游景区规划设计、开发、管理；健身休闲活动；餐饮服务；蔬菜、水果、花卉、苗木种植；住宿服务；百货零售；会议及展览服务；游乐园活动；室内娱乐活动；景区内游船出租活动；景区内的小动物拉车、骑马、钓鱼活动；歌舞厅娱乐活动[依法须经批准的项目，经相关部门批准后方可开展经营活动]。 | |

```php
$itemsData = [
	'ZTMC' => '彭州市衡粤旅游发展有限公司',
	'ZTLB' => '企业法人',
	'TYSHXYDM' => '91510182MA6C4X1D3B',
	'FDDBR' => '张晓苹',
	'FDDBRZJLX' => '身份证',
	'FDDBRZJHM' => '612301197209212055',
	'CLRQ' => '20171013',
	'YXQ' => '20501013',
	'DZ' => '四川省彭州市桂花镇衡州村7组',
	'DJJG' => '彭州市市场监督管理局',
	'GB' => 'A24',
	'ZCZB' => '500',
	'ZCZBBZ' => '156',
	'HYDM' => '80',
	'LX' => 'N',
	'JYFW' => '水利、环境和公共设施管理业',
	'JYZT' => '1',
	'JYFWMS' => '旅游景区规划设计、开发、管理；健身休闲活动；餐饮服务；蔬菜、水果、花卉、苗木种植；住宿服务；百货零售；会议及展览服务；游乐园活动；室内娱乐活动；景区内游船出租活动；景区内的小动物拉车、骑马、钓鱼活动；歌舞厅娱乐活动[依法须经批准的项目，经相关部门批准后方可开展经营活动]。',
];
```

<?php
namespace BaseData\ResourceCatalogData\Service;

use Marmot\Core;
use Marmot\Interfaces\INull;

use BaseData\Rule\Model\RuleService;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;

class SyncSubtaskService
{
    use SyncTaskTrait;

    private $subtask;

    public function __construct(RuleService $ruleService, Task $task)
    {
        $this->ruleService = $ruleService;
        $this->task = $task;
        $this->subtask = new Task();
    }

    public function __destruct()
    {
        unset($this->ruleService);
        unset($this->task);
        unset($this->subtask);
    }

    protected function getRuleService() : RuleService
    {
        return $this->ruleService;
    }

    protected function getTask() : Task
    {
        return $this->task;
    }

    protected function getSubtask() : Task
    {
        return $this->subtask;
    }

    /**
     * 将来源资源目录数据通过规则转换,添加到目标资源目录数据库
        * 通过传入的规则和任务获取来源资源目录数据
        * 新增子任务
        * 将来源资源目录数据通过规则进行转换,入目标库
        * 修改任务信息,成功数,失败数,状态
        * 根据来源资源目录获取其下的转换规则
     * @return array 返回规则列表和任务,
     */
    public function execute() : array
    {
        $ruleService = $this->getRuleService();
        $task = $this->getTask();

        //验证数据是否正确
        if (!$this->validateDataCorrect($ruleService, $task)) {
            return array(array(), new NullTask());
        }

        //获取来源资源目录数据
        list($sourceSearchDataList, $count) = $this-> fetchSourceSearchData($ruleService, $task);

        if (empty($count)) {
            return array(array(), new NullTask());
        }

        //新增子任务
        $subTask = $this->addSubtask($ruleService, $task, $count);

        if ($subTask instanceof NullTask) {
            return array(array(), new NullTask());
        }

        //循环数组,通过规则对数组进行转换,并入库
        $this->searchDataTransformationAndAdd($sourceSearchDataList, $ruleService, $subTask);
        
        //修改任务信息
        $this->updateTask($subTask, $ruleService);

        //根据来源资源目录获取其下的转换规则
        $ruleServiceList = $this->fetchRuleServiceList($ruleService->getTransformationTemplate());

        return array($ruleServiceList, $subTask);
    }

    /**
     * 验证数据是否正确
        * 验证规则是否存在
        * 验证任务是否存在
     * @param RuleService $ruleService 规则信息
     * @param Task $task 任务信息
     * @return bool
     */
    protected function validateDataCorrect(RuleService $ruleService, Task $task) : bool
    {
        return $this->isRuleServiceExist($ruleService)
            && $this->isTaskExist($task);
    }

    /**
     * 验证规则是否存在
     * @param RuleService $ruleService 规则信息
     * @return bool
     */
    protected function isRuleServiceExist(RuleService $ruleService) : bool
    {
        if ($ruleService instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'ruleServiceId'));
            return false;
        }

        return true;
    }

    /**
     * 验证任务是否存在
     * @param Task $task 任务信息
     * @return bool
     */
    protected function isTaskExist(Task $task) : bool
    {
        if ($task instanceof INull) {
            Core::setLastError(PARAMETER_IS_EMPTY, array('pointer'=>'taskId'));
            return false;
        }

        return true;
    }

    /**
     * 获取来源资源目录数据
     * @param RuleService $ruleService 规则信息
     * @param Task $task 任务信息
     * @return array $sourceSearchDataList 来源资源目录数据 $count 来源资源目录数据数量
     */
    protected function fetchSourceSearchData(RuleService $ruleService, Task $task) : array
    {
        //获取来源资源目录仓库
        $category = $ruleService->getSourceCategory();
        $repository = $this->getSearchDataRepositoryFactory()->getRepository($category);

        $filter['task'] = $task->getId();
        list($sourceSearchDataList, $count) = $repository->filter($filter);

        return [$sourceSearchDataList, $count];
    }

    /**
     * 新增子任务
     * @param RuleService $ruleService 规则信息
     * @param Task $task 任务信息
     * @param int $count 转换总数
     * @return Task 返回任务对象,失败返回空的任务对象是为了避免报错,能使流程继续执行
     */
    protected function addSubtask(RuleService $ruleService, Task $task, int $count) : Task
    {
        $subtask = $this->getSubtask();
        $subtask->setCrew($task->getCrew());
        $subtask->setUserGroup($task->getUserGroup());
        $subtask->setTotal($count);
        $subtask->setPid($task->getId());
        $subtask->setSourceCategory($ruleService->getSourceCategory());
        $subtask->setSourceTemplate($ruleService->getSourceTemplate()->getId());
        $subtask->setTargetCategory($ruleService->getTransformationCategory());
        $subtask->setTargetTemplate($ruleService->getTransformationTemplate()->getId());
        $subtask->setTargetRule($ruleService->getId());
        $subtask->setScheduleTask(posix_getpid());
        $subtask->setFileName($task->getFileName());
        
        return $subtask->add() ? $subtask : NullTask::getInstance();
    }
}

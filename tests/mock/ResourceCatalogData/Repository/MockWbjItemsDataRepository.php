<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;

class MockWbjItemsDataRepository extends WbjItemsDataRepository
{
    public function getAdapter() : IWbjItemsDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWbjItemsDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWbjItemsDataAdapter
    {
        return parent::getMockAdapter();
    }
}

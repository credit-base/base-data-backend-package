<?php
namespace BaseData\Template\WbjTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Translator\WbjTemplateDbTranslator;

/**
 * @Feature: 我是委办局领导,当我需要查看我的委办局目录时,在政务网OA中,可以查看到我的所有委办局目录,
 *           通过列表和详情的形式查看所有委办局目录,以便于我可以看到我的所有委办局目录
 * @Scenario: 查看资源目录列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * @Given: 存在资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
        ]);
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchWbjTemplateList()
    {
        $repository = new WbjTemplateRepository();

        list($wbjTemplateList, $count) = $repository->filter([]);
        
        unset($count);

        return $wbjTemplateList;
    }

    /**
     * @Then: 我可以看到资源目录的全部信息
     */
    public function testViewWbjTemplateList()
    {
        $setWbjTemplateList = $this->wbjTemplate();

        foreach ($setWbjTemplateList as $key => $setWbjTemplate) {
            $setWbjTemplateList[$key]['wbj_template_id'] = $key +1;
            $setWbjTemplateList[$key]['subject_category'] = json_decode($setWbjTemplate['subject_category'], true);
            $setWbjTemplateList[$key]['items'] = json_decode($setWbjTemplate['items'], true);
        }

        $wbjTemplateList = $this->fetchWbjTemplateList();

        $translator = new WbjTemplateDbTranslator();
        foreach ($wbjTemplateList as $wbjTemplate) {
            $wbjTemplateArray[] = $translator->objectToArray($wbjTemplate);
        }

        $this->assertEquals($wbjTemplateArray, $setWbjTemplateList);
    }
}

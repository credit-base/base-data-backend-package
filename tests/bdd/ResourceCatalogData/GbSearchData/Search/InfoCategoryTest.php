<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\GbSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件资源目录数据时,在政务网OA中,可以搜索我需要的所有资源目录数据,
 *           通过列表形式查看我搜索的所有资源目录数据 ,以便于我可以快速查看到我需要的资源目录数据
 * @Scenario: 委办局工作人员通过资源信息类别搜索
 */
class InfoCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_items_data');
        $this->clear('pcore_gb_search_data');
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchGbSearchDataList()
    {
        $repository = new GbSearchDataRepository();
 
        $filter['infoCategory'] = ISearchDataAble::INFO_CATEGORY['QTXX'];

        list($gbSearchDataList, $count) = $repository->filter($filter);
        
        unset($count);

        return $gbSearchDataList;
    }

    /**
     * @Then: 我可以看到信息分类为守信信息的所有资源目录数据
     */
    public function testViewGbSearchDataList()
    {
        $setGbSearchDataList = $this->gbSearchData()['pcore_gb_search_data'];

        foreach ($setGbSearchDataList as $key => $setGbSearchData) {
            unset($setGbSearchData);
            $setGbSearchDataList[$key]['gb_search_data_id'] = $key +1;
        }

        $gbSearchDataList = $this->fetchGbSearchDataList();
        $translator = new GbSearchDataDbTranslator();
        foreach ($gbSearchDataList as $gbSearchData) {
            $gbSearchDataArray[] = $translator->objectToArray($gbSearchData);
        }

        $this->assertEquals($gbSearchDataArray, $setGbSearchDataList);

        foreach ($gbSearchDataArray as $gbSearchData) {
            $this->assertEquals(ISearchDataAble::INFO_CATEGORY['QTXX'], $gbSearchData['info_category']);
        }
    }
}

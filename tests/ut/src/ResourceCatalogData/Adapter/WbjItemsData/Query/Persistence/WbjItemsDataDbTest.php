<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class WbjItemsDataDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockWbjItemsDataDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(WbjItemsDataDb::TABLE, $this->database->getTable());
    }
}

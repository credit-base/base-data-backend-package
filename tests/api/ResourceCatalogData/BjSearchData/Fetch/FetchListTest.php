<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试本级资源目录接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_bj_search_data');
        $this->clear('pcore_bj_items_data');
    }

    /**
     * 存在多条本级资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_bj_search_data' => $this->bjSearchData()['pcore_bj_search_data'],
                'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data']
            ]
        );
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function testViewBjSearchData()
    {
        $setBjSearchDataList = $this->bjSearchData()['pcore_bj_search_data'];
        $setTemplateList = $this->bjTemplate();
        foreach ($setTemplateList as $setTemplate) {
            $setTemplateList[$setTemplate['bj_template_id']] = $setTemplate;
        }
        $setBjItemsDataList = $this->bjSearchData()['pcore_bj_items_data'];
        foreach ($setBjItemsDataList as $setBjItemsData) {
            $setBjItemsDataList[$setBjItemsData['bj_items_data_id']] = $setBjItemsData;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'bjSearchData/1,2?include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $bjSearchData) {
            $this->bjSearchDataCompareData($setBjSearchDataList[$key], $bjSearchData);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'bjItemsData') {
                $this->bjItemsDataCompareData($setBjItemsDataList[$value['id']], $value);
            }
            if ($value['type'] == 'bjTemplates') {
                $this->bjTemplateCompareData($setTemplateList[$value['id']], $value);
            }
        }
    }
}

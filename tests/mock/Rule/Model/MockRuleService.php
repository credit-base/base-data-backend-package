<?php
namespace BaseData\Rule\Model;

use BaseData\Rule\Adapter\RuleService\IRuleServiceAdapter;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
*/
class MockRuleService extends RuleService
{
    public function getRepository() : IRuleServiceAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function commonValidate() : bool
    {
        return parent::commonValidate();
    }

    public function isCrewExist() : bool
    {
        return parent::isCrewExist();
    }

    public function isSourceTemplateExist() : bool
    {
        return parent::isSourceTemplateExist();
    }

    public function isTransformationTemplateExist() : bool
    {
        return parent::isTransformationTemplateExist();
    }

    public function isRuleNotExist() : bool
    {
        return parent::isRuleNotExist();
    }

    public function isRuleFormatCorrect() : bool
    {
        return parent::isRuleFormatCorrect();
    }

    public function rulesValidate() : bool
    {
        return parent::rulesValidate();
    }

    public function updateTemplateRuleCount() : bool
    {
        return parent::updateTemplateRuleCount();
    }

    public function getSortRules(array $rules) : array
    {
        return parent::getSortRules($rules);
    }
}

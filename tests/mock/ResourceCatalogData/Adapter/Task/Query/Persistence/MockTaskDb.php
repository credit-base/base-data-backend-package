<?php
namespace BaseData\ResourceCatalogData\Adapter\Task\Query\Persistence;

class MockTaskDb extends TaskDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

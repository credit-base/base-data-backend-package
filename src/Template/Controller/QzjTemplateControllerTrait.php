<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\CommandHandler\QzjTemplate\QzjTemplateCommandHandlerFactory;

trait QzjTemplateControllerTrait
{
    protected function getTemplateWidgetRule() : TemplateWidgetRule
    {
        return new TemplateWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : QzjTemplateRepository
    {
        return new QzjTemplateRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new QzjTemplateCommandHandlerFactory());
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function validateOperateScenario(
        $name,
        $identify,
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $description,
        $items,
        $category,
        $sourceUnit
    ) : bool {
        return $this->getTemplateWidgetRule()->name($name)
        && $this->getTemplateWidgetRule()->identify($identify)
        && $this->getTemplateWidgetRule()->subjectCategory($subjectCategory)
        && $this->getTemplateWidgetRule()->dimension($dimension)
        && $this->getCommonWidgetRule()->formatNumeric($infoClassify, 'infoClassify')
        && $this->getCommonWidgetRule()->formatNumeric($infoCategory, 'infoCategory')
        && $this->getCommonWidgetRule()->formatNumeric($exchangeFrequency, 'exchangeFrequency')
        && $this->getTemplateWidgetRule()->description($description)
        && $this->getTemplateWidgetRule()->items($subjectCategory, $items)
        && $this->getTemplateWidgetRule()->qzjCategory($category)
        && $this->getTemplateWidgetRule()->qzjSourceUnit($category, $sourceUnit);
    }
}

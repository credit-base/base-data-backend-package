<?php
namespace BaseData\WorkOrderTask\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\WorkOrderTask\WidgetRule\ParentTaskWidgetRule;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseData\WorkOrderTask\CommandHandler\ParentTask\ParentTaskCommandHandlerFactory;

trait ParentTaskControllerTrait
{
    protected function getParentTaskWidgetRule() : ParentTaskWidgetRule
    {
        return new ParentTaskWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : IParentTaskAdapter
    {
        return new ParentTaskRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new ParentTaskCommandHandlerFactory());
    }
}

<?php
namespace BaseData\Template\WbjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Translator\WbjTemplateDbTranslator;

/**
 * @Feature: 我是委办局领导,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 查看公开范围为社会公开的数据
 */
class DimensionTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchWbjTemplateList()
    {
        $repository = new WbjTemplateRepository();
        $filter['dimension'] = Template::DIMENSION['SHGK'];

        list($wbjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $wbjTemplateList;
    }

    /**
     * @Then: 我可以看到公开范围为社会公开的所有资源目录
     */
    public function testViewWbjTemplateList()
    {
        $dimensionWbjTemplateList = $this->wbjTemplate();

        foreach ($dimensionWbjTemplateList as $key => $dimensionWbjTemplate) {
            $dimensionWbjTemplateList[$key]['wbj_template_id'] = $key +1;
            $dimensionWbjTemplateList[$key]['subject_category'] = json_decode(
                $dimensionWbjTemplate['subject_category'],
                true
            );
            $dimensionWbjTemplateList[$key]['items'] = json_decode($dimensionWbjTemplate['items'], true);
        }

        $wbjTemplateList = $this->fetchWbjTemplateList();
        $translator = new WbjTemplateDbTranslator();
        foreach ($wbjTemplateList as $wbjTemplate) {
            $wbjTemplateArray[] = $translator->objectToArray($wbjTemplate);
        }

        $this->assertEquals($wbjTemplateArray, $dimensionWbjTemplateList);

        foreach ($wbjTemplateArray as $wbjTemplate) {
            $this->assertEquals(Template::DIMENSION['SHGK'], $wbjTemplate['dimension']);
        }
    }
}

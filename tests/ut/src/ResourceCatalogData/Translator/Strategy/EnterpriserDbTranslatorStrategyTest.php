<?php
namespace BaseData\ResourceCatalogData\Translator\Strategy;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\BaseSearchData;
use BaseData\ResourceCatalogData\Model\NullBaseSearchData;

class EnterpriserDbTranslatorStrategyTest extends TestCase
{
    private $translator;

    public function setUp()
    {
        $this->translator = new EnterpriserDbTranslatorStrategy();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\Strategy\IDbTranslatorStrategy',
            $this->translator
        );
    }

    public function testObjectToArrayEmpty()
    {
        $result = $this->translator->objectToArray(array());

        $this->assertEquals(array(), $result);
    }

    public function testObjectToArray()
    {
        $baseSearchData = new BaseSearchData();
        $data = array(
            'ZTMC' => '主体名称'
        );
        $baseSearchData->getItemsData()->setData($data);
        $expression = $this->translator->objectToArray($baseSearchData);

        $this->compareArrayAndObject($expression, $baseSearchData);
    }

    private function compareArrayAndObject(
        array $expectedArray,
        $baseSearchData
    ) {
        $itemsData = $baseSearchData->getItemsData()->getData();
        foreach (EnterpriserDbTranslatorStrategy::ENTERPRISE_PARAMETERS_MAPPING as $key => $value) {
            $data = isset($itemsData[$value['identify']]) ? $itemsData[$value['identify']] : $value['defaultValue'];
            $this->assertEquals($expectedArray[$key], $data);
        }

        $data = array();
        if (is_string($expectedArray['data'])) {
            $data = unserialize(gzuncompress(base64_decode($expectedArray['data'], true)));
        }
        if (is_array($expectedArray['data'])) {
            $data = $expectedArray['data'];
        }

        $this->assertEquals($data, $baseSearchData->getItemsData()->getData());
        $this->assertEquals($expectedArray['hash'], $baseSearchData->getHash());
        $this->assertEquals($expectedArray['task_id'], $baseSearchData->getTask()->getId());
        $this->assertEquals($expectedArray['status'], $baseSearchData->getStatus());
        $this->assertEquals($expectedArray['create_time'], $baseSearchData->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $baseSearchData->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $baseSearchData->getStatusTime());
    }
}

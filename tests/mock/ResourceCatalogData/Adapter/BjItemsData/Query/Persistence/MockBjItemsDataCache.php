<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData\Query\Persistence;

class MockBjItemsDataCache extends BjItemsDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;

class MockYsSearchData extends YsSearchData
{
    public function getRepository() : IYsSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getYsItemsDataRepository() : IYsItemsDataAdapter
    {
        return parent::getYsItemsDataRepository();
    }

    public function addItemsData() : bool
    {
        return parent::addItemsData();
    }
}

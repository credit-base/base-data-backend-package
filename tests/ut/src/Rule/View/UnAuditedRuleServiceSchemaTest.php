<?php
namespace BaseData\Rule\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

use BaseData\Rule\Model\IRule;
use BaseData\Rule\Model\TransformationRule;

class UnAuditedRuleServiceSchemaTest extends TestCase
{
    private $unAuditedRuleServiceSchema;

    private $unAuditedRuleService;

    public function setUp()
    {
        $this->unAuditedRuleServiceSchema = new UnAuditedRuleServiceSchema(new Factory());

        $this->unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->unAuditedRuleServiceSchema);
        unset($this->unAuditedRuleService);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->unAuditedRuleServiceSchema);
    }

    public function testGetId()
    {
        $result = $this->unAuditedRuleServiceSchema->getId($this->unAuditedRuleService);

        $this->assertEquals($result, $this->unAuditedRuleService->getApplyId());
    }

    public function testGetAttributes()
    {
        $unAuditedRuleService = $this->unAuditedRuleService;
        $transformationRule = new TransformationRule();
        $transformationRule->setCondition(array(
            "TYSHXYDM" => "TYSHXYDM",
            "ZTMC" => "ZTMC",
        ));

        $unAuditedRuleService->setRules(array('transformationRule' => $transformationRule));

        $result = $this->unAuditedRuleServiceSchema->getAttributes($unAuditedRuleService);

        $rules = $unAuditedRuleService->getRules();
        foreach ($rules as $key => $rule) {
            if ($rule instanceof IRule) {
                $rules[$key] = $rule->getCondition();
            }
        }

        $this->assertEquals($result['rules'], $rules);
        $this->assertEquals($result['version'], $this->unAuditedRuleService->getVersion());
        $this->assertEquals($result['dataTotal'], $this->unAuditedRuleService->getDataTotal());
        $this->assertEquals(
            $result['transformationCategory'],
            $this->unAuditedRuleService->getTransformationCategory()
        );
        $this->assertEquals($result['sourceCategory'], $this->unAuditedRuleService->getSourceCategory());
        $this->assertEquals($result['applyStatus'], $this->unAuditedRuleService->getApplyStatus());
        $this->assertEquals($result['rejectReason'], $this->unAuditedRuleService->getRejectReason());
        $this->assertEquals($result['operationType'], $this->unAuditedRuleService->getOperationType());
        $this->assertEquals($result['relationId'], $this->unAuditedRuleService->getRelationId());
    }

    public function testGetRelationships()
    {
        $result = $this->unAuditedRuleServiceSchema->getRelationships($this->unAuditedRuleService, 0, array());

        $this->assertEquals(
            $result['transformationTemplate'],
            ['data' => $this->unAuditedRuleService->getTransformationTemplate()]
        );
        $this->assertEquals($result['sourceTemplate'], ['data' => $this->unAuditedRuleService->getSourceTemplate()]);
        $this->assertEquals($result['userGroup'], ['data' => $this->unAuditedRuleService->getUserGroup()]);
        $this->assertEquals($result['crew'], ['data' => $this->unAuditedRuleService->getCrew()]);
        $this->assertEquals($result['applyCrew'], ['data' => $this->unAuditedRuleService->getApplyCrew()]);
        $this->assertEquals($result['publishCrew'], ['data' => $this->unAuditedRuleService->getPublishCrew()]);
        $this->assertEquals($result['userGroup'], ['data' => $this->unAuditedRuleService->getUserGroup()]);
        $this->assertEquals($result['applyUserGroup'], ['data' => $this->unAuditedRuleService->getApplyUserGroup()]);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullWbjItemsDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullWbjItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsWbjItemsData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\WbjItemsData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

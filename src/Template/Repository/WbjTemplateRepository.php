<?php
namespace BaseData\Template\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Adapter\WbjTemplate\WbjTemplateDbAdapter;
use BaseData\Template\Adapter\WbjTemplate\WbjTemplateMockAdapter;

class WbjTemplateRepository extends Repository implements IWbjTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new WbjTemplateDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IWbjTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IWbjTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IWbjTemplateAdapter
    {
        return new WbjTemplateMockAdapter();
    }

    public function fetchOne($id) : WbjTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(WbjTemplate $wbjTemplate) : bool
    {
        return $this->getAdapter()->add($wbjTemplate);
    }

    public function edit(WbjTemplate $wbjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($wbjTemplate, $keys);
    }
}

<?php
namespace BaseData\WorkOrderTask\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Utils\WorkOrderTaskUtils;
use BaseData\WorkOrderTask\Utils\WorkOrderTaskMockFactory;

class WorkOrderTaskDbTranslatorTest extends TestCase
{
    use WorkOrderTaskUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WorkOrderTaskDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\WorkOrderTask\Model\NullWorkOrderTask', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObjectGbTemplate()
    {
        $value['templateType']  = ParentTask::TEMPLATE_TYPE['GB'];
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1, 1, $value);

        $this->initArrayToObject($workOrderTask);
    }

    public function testArrayToObjectBjTemplate()
    {
        $value['templateType']  = ParentTask::TEMPLATE_TYPE['BJ'];
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1, 1, $value);

        $this->initArrayToObject($workOrderTask);
    }

    protected function initArrayToObject(WorkOrderTask $workOrderTask)
    {
        $expression['work_order_task_id'] = $workOrderTask->getId();

        $expression['parent_task_id'] = $workOrderTask->getParentTask()->getId();
        $expression['title'] = $workOrderTask->getParentTask()->getTitle();
        $expression['end_time'] = $workOrderTask->getParentTask()->getEndTime();
        $expression['template_type'] = $workOrderTask->getParentTask()->getTemplateType();
        
        $expression['template_id'] = $workOrderTask->getParentTask()->getTemplate()->getId();
        $expression['template_id'] = $workOrderTask->getTemplate()->getId();
        $expression['template_name'] = $workOrderTask->getTemplate()->getName();

        $expression['assign_object_id'] = $workOrderTask->getAssignObject()->getId();
        $expression['assign_object_name'] = $workOrderTask->getAssignObject()->getName();

        $expression['reason'] = $workOrderTask->getReason();
        $expression['feedback_records'] = json_encode($workOrderTask->getFeedbackRecords());

        $expression['status'] = $workOrderTask->getStatus();
        $expression['status_time'] = $workOrderTask->getStatusTime();
        $expression['create_time'] = $workOrderTask->getCreateTime();
        $expression['update_time'] = $workOrderTask->getUpdateTime();

        $workOrderTask = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\WorkOrderTask\Model\WorkOrderTask', $workOrderTask);
        $this->compareArrayAndObject($expression, $workOrderTask);
    }

    public function testObjectToArray()
    {
        $workOrderTask = WorkOrderTaskMockFactory::generateWorkOrderTask(1);

        $expression = $this->translator->objectToArray($workOrderTask);

        $this->compareArrayAndObject($expression, $workOrderTask);
    }
}

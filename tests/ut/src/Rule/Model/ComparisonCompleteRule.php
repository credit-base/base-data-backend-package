<?php
namespace BaseData\Rule\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Rule\Model\IRule;
use BaseData\Template\Model\Template;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Model\MockTemplate;
use BaseData\Template\Repository\MockBjTemplateRepository;

/**
 * BaseData\Rule\Model\ComparisonRule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class ComparisonCompleteRule extends TestCase
{
    protected $rule;

    protected $transformationTemplateItems;

    protected $ruleTemplateList;

    protected $condition;

    protected $transformationTemplate;

    protected $ruleTemplateItems;

    public function setUp()
    {
        $this->transformationTemplateOriginalItems = array(
                array(
                    "name" => '主体名称',    //信息项名称
                    "identify" => 'ZTMC',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '200',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(),    //脱敏规则
                    "remarks" => '信用主体名称',    //备注
                ),
                array(
                    "name" => '统一社会信用代码',    //信息项名称
                    "identify" => 'TYSHXYDM',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                    "remarks" => '信用主体代码',    //备注
                )
        );
        $this->transformationTemplateItems = array();
        $this->ruleTemplateOriginalItems = array(
                array(
                    "name" => '主体名称',    //信息项名称
                    "identify" => 'ZTMC',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '200',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(),    //脱敏规则
                    "remarks" => '信用主体名称',    //备注
                ),
                array(
                    "name" => '统一社会信用代码',    //信息项名称
                    "identify" => 'TYSHXYDM',    //数据标识
                    "type" => 1,    //数据类型
                    "length" => '50',    //数据长度
                    "options" => array(),    //可选范围
                    "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                    "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                    "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                    "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                    "remarks" => '信用主体代码',    //备注
                )
        );
        $this->ruleTemplateItems = array();
        $this->ruleTemplateList = [];
        $this->condition = [];
        $this->transformationTemplate = new MockTemplate();
        $this->repository = new MockBjTemplateRepository();
        $this->ruleTemplateItems = [];
    }

    public function tearDown()
    {
        unset($this->rule);
        unset($this->ruleTemplateList);
        unset($this->transformationTemplateItems);
        unset($this->ruleTemplateItems);
        unset($this->condition);
        unset($this->transformationTemplate);
        unset($this->repository);
        unset($this->ruleTemplateItems);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\Rule", $this->rule);
    }

    public function testImplementsIRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\IRule", $this->rule);
    }

    /**
     * 初始化validate前置条件
     */
    protected function prepareValidate()
    {
        foreach ($this->transformationTemplateOriginalItems as $key => $item) {
            $this->transformationTemplateItems[$item['identify']] = $item;
        }

        foreach ($this->ruleTemplateOriginalItems as $key => $item) {
            $this->ruleTemplateItems[$item['identify']] = $item;
        }

        $this->rule->expects($this->exactly(1))
                         ->method('getTransformationTemplate')
                         ->willReturn($this->transformationTemplate);

        $this->rule->expects($this->exactly(1))
                         ->method('fetchTemplateList')
                         ->with($this->condition)
                         ->willReturn($this->ruleTemplateList);

        $this->rule->expects($this->exactly(1))
                         ->method('getCondition')
                         ->willReturn($this->condition);
    }

    public function testValidateEmptyCondition()
    {
        $this->condition = [];
        $this->prepareValidate();

        $result = $this->rule->validate();
        $this->assertTrue($result);
    }

    public function testValidateNotExistTransformationTemplateItem()
    {
        //初始化
        $this->condition = array(
            'NO_EXIST' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
                array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            )
        );

        $this->prepareValidate();

        $this->rule->expects($this->exactly(1))
                         ->method('fetchTemplateItems')
                         ->with($this->transformationTemplate)
                         ->willReturn($this->transformationTemplateItems);

        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    public function testValidateNotExistTemplate()
    {
        //构造数据 模板id 和 模板匹配不上
        $this->ruleTemplateList = [1=>new MockTemplate(), 2=>new MockTemplate()];

        $this->condition = array(
            'ZTMC' => array(
                array('id'=>3, 'base'=> array(1,2), 'item'=>'ZTMC')
            )
        );

        $this->prepareValidate();

        $this->rule->expects($this->exactly(1))
                         ->method('fetchTemplateItems')
                         ->with($this->transformationTemplate)
                         ->willReturn($this->transformationTemplateItems);

        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    public function testValidateBaseFalse()
    {
        $this->ruleTemplateList = [1=>new MockTemplate(), 2=>new MockTemplate()];

        $this->condition = array(
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC')
            )
        );

        $this->prepareValidate();

        $this->rule->expects($this->exactly(1))
                         ->method('fetchTemplateItems')
                         ->with($this->transformationTemplate)
                         ->willReturn($this->transformationTemplateItems);

        $this->rule->expects($this->exactly(1))
                         ->method('validateBase')
                         ->with(
                             $this->ruleTemplateList[1],
                             $this->condition['ZTMC'][0]['base']
                         )->willReturn(false);

        $result = $this->rule->validate();
        $this->assertFalse($result);
    }

    protected function prepareItemComparison()
    {
        $this->ruleTemplateList = [1=>new MockTemplate(), 2=>new MockTemplate()];
        
        $this->condition = array(
            'ZTMC' => array(
                array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC')
            )
        );
        
        $base = $this->condition['ZTMC'][0]['base'];
        $comparisonTemplate = $this->ruleTemplateList[1];

        $this->prepareValidate();


        $this->rule->expects($this->exactly(2))
                         ->method('fetchTemplateItems')
                        ->withConsecutive(
                            [$this->transformationTemplate],
                            [$comparisonTemplate]
                        )
                         ->will($this->onConsecutiveCalls(
                             $this->transformationTemplateItems,
                             $this->ruleTemplateItems
                         ));

        $this->rule->expects($this->exactly(1))
                         ->method('validateBase')
                         ->with($comparisonTemplate, $base)
                         ->willReturn(true);
    }
}

<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\Crew\Model\Crew;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Command\RuleService\EditRuleServiceCommand;

class EditRuleServiceCommandHandlerTest extends TestCase
{
    private $command;

    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new EditRuleServiceCommandHandler();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $this->command = new class implements ICommand {
        };
        $this->commandHandler->execute($this->command);
    }

    private function initialExecute($result, $id = 0)
    {
        //初始化
        $relationId = 1;
        $crew = 2;
        $rules = ['rules'];
        $applyInfo = ['applyInfo'];

        $this->command = new EditRuleServiceCommand(
            $rules,
            $relationId,
            $crew
        );

        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService(1);
        $crew = new Crew();

        $this->commandHandler = $this->getMockBuilder(EditRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         [
                                            'fetchRuleService',
                                            'getUnAuditedRuleService',
                                            'generateUnAuditedRuleServiceCrew',
                                            'generateUnAuditedRuleServiceRules',
                                            'applyInfo'
                                         ]
                                     )
                                     ->getMock();

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->setApplyInfo(Argument::exact($applyInfo))->shouldBeCalledTimes(1);
        $unAuditedRuleService->generateVersion()->shouldBeCalledTimes(1);
        $unAuditedRuleService->setId(Argument::exact($relationId))->shouldBeCalledTimes(1);
        $unAuditedRuleService->setRelationId(Argument::exact($relationId))->shouldBeCalledTimes(1);
        $unAuditedRuleService->setTransformationTemplate(
            Argument::exact($ruleService->getTransformationTemplate())
        )->shouldBeCalledTimes(1);
        $unAuditedRuleService->setSourceTemplate(
            Argument::exact($ruleService->getSourceTemplate())
        )->shouldBeCalledTimes(1);
        $unAuditedRuleService->setUserGroup(
            Argument::exact($ruleService->getSourceTemplate()->getSourceUnit())
        )->shouldBeCalledTimes(1);
        $unAuditedRuleService->setTransformationCategory(
            Argument::exact($ruleService->getTransformationCategory())
        )->shouldBeCalledTimes(1);
        $unAuditedRuleService->setSourceCategory(
            Argument::exact($ruleService->getSourceCategory())
        )->shouldBeCalledTimes(1);
        $unAuditedRuleService->setOperationType(
            Argument::exact($this->command->operationType)
        )->shouldBeCalledTimes(1);
        if ($result) {
            $unAuditedRuleService->getApplyId()->shouldBeCalledTimes(1)->willReturn($id);
        }
        $unAuditedRuleService->edit()->shouldBeCalledTimes(1)->willReturn($result);

        $this->commandHandler->expects($this->once())->method('fetchRuleService')->willReturn($ruleService);

        $this->commandHandler->expects($this->once())
                       ->method('applyInfo')
                       ->with($this->isInstanceOf('BaseData\Rule\Model\UnAuditedRuleService'))
                       ->willReturn($applyInfo);

        $this->commandHandler->expects($this->once())->method(
            'getUnAuditedRuleService'
        )->willReturn($unAuditedRuleService->reveal());

        $this->commandHandler->expects($this->once())
                       ->method('generateUnAuditedRuleServiceCrew')
                       ->with(
                           $this->command->crew,
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($unAuditedRuleService->reveal());

        $this->commandHandler->expects($this->once())
                       ->method('generateUnAuditedRuleServiceRules')
                       ->with(
                           $this->command->rules,
                           $this->isInstanceOf(
                               'BaseData\Rule\Model\UnAuditedRuleService'
                           )
                       )
                       ->willReturn($unAuditedRuleService->reveal());
    }

    public function testExecuteSuccess()
    {
        $id = 10;

        $this->initialExecute(true, $id);

        $result = $this->commandHandler->execute($this->command);
        $this->assertTrue($result);
        $this->assertEquals($id, $this->command->id);
    }

    public function testExecuteFalse()
    {
        $this->initialExecute(false);

        $result = $this->commandHandler->execute($this->command);
        $this->assertFalse($result);
        $this->assertEquals(0, $this->command->id);
    }
}

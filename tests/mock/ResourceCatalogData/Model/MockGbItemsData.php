<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter;

class MockGbItemsData extends GbItemsData
{
    public function getRepository() : IGbItemsDataAdapter
    {
        return parent::getRepository();
    }
}

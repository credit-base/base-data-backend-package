<?php
namespace BaseData\Task\Adapter\Task;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Task\Model\MockTask;

class TaskMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new TaskMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testAdd()
    {
        $this->assertTrue($this->adapter->add(new MockTask()));
    }

    public function testEdit()
    {
        $this->assertTrue($this->adapter->edit(new MockTask(), array()));
    }

    public function testNotifySubTasks()
    {
        $this->assertTrue($this->adapter->notifySubTasks(new MockTask()));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Model\Task',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Task\Model\Task',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Task\Model\Task',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }

    public function testSchedule()
    {
        $list = $this->adapter->schedule();

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Task\Model\Task',
                $each
            );
        }
    }

    public function testExpire()
    {
        list($list, $count) = $this->adapter->expire();

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Task\Model\Task',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

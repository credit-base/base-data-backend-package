<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use BaseData\ResourceCatalogData\Translator\Strategy\SearchDataDbTranslatorStrategyFactory;

class MockBaseSearchDataDbAdapter extends BaseSearchDataDbAdapter
{
    public function getTranslatorStrategyFactory() : SearchDataDbTranslatorStrategyFactory
    {
        return parent::getTranslatorStrategyFactory();
    }

    public function getDbTranslator(int $category)
    {
        return parent::getDbTranslator($category);
    }

    public function getRowQueryFactory() : RowQueryFactory
    {
        return parent::getRowQueryFactory();
    }

    public function getRowQuery(int $category)
    {
        return parent::getRowQuery($category);
    }

    public function getAdapterFactory() : AdapterFactory
    {
        return parent::getAdapterFactory();
    }

    public function getAdapter(int $category)
    {
        return parent::getAdapter($category);
    }
}

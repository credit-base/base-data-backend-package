<?php
namespace BaseData\Enterprise\Repository;

use BaseData\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;

class MockEnterpriseRepository extends EnterpriseRepository
{
    public function getAdapter() : IEnterpriseAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IEnterpriseAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IEnterpriseAdapter
    {
        return parent::getMockAdapter();
    }
}

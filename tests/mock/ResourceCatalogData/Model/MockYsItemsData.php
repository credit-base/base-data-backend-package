<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;

class MockYsItemsData extends YsItemsData
{
    public function getRepository() : IYsItemsDataAdapter
    {
        return parent::getRepository();
    }
}

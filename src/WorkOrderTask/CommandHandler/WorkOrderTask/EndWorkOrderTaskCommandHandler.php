<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;

class EndWorkOrderTaskCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new WorkOrderTaskRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EndWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getWorkOrderTaskRepository();
        $workOrderTask = $repository->fetchOne($command->id);

        return $workOrderTask->end($command->reason);
    }
}

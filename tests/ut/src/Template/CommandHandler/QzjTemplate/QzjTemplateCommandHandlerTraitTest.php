<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Repository\UserGroupRepository;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;

class QzjTemplateCommandHandlerTraitTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new MockQzjTemplateCommandHandlerTrait();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testGetUserGroupRepository()
    {
        $this->assertInstanceOf(
            'BaseData\UserGroup\Repository\UserGroupRepository',
            $this->commandHandler->publicGetUserGroupRepository()
        );
    }

    public function testFetchUserGroup()
    {
        $id = 1;
        $userGroup = new UserGroup();

        $this->commandHandler = $this->getMockBuilder(MockQzjTemplateCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getUserGroupRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(UserGroupRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($userGroup);

        $this->commandHandler->expects($this->once())
                       ->method('getUserGroupRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->publicFetchUserGroup($id);
        $this->assertInstanceOf(
            'BaseData\UserGroup\Model\UserGroup',
            $result
        );
    }

    public function testGetQzjTemplate()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\QzjTemplate',
            $this->commandHandler->publicGetQzjTemplate()
        );
    }

    public function testGetQzjTemplateRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Repository\QzjTemplateRepository',
            $this->commandHandler->publicGetQzjTemplateRepository()
        );
    }

    public function testFetchQzjTemplate()
    {
        $id = 1;
        $qzjTemplate = new QzjTemplate();

        $this->commandHandler = $this->getMockBuilder(MockQzjTemplateCommandHandlerTrait::class)
                                     ->setMethods(
                                         [
                                            'getQzjTemplateRepository'
                                         ]
                                     )
                                     ->getMock();

        $repository = $this->prophesize(QzjTemplateRepository::class);
        $repository->fetchOne($id)->shouldBeCalledTimes(1)->willReturn($qzjTemplate);

        $this->commandHandler->expects($this->once())
                       ->method('getQzjTemplateRepository')
                       ->willReturn($repository->reveal());

        $result = $this->commandHandler->publicFetchQzjTemplate($id);
        $this->assertInstanceOf(
            'BaseData\Template\Model\QzjTemplate',
            $result
        );
    }
}

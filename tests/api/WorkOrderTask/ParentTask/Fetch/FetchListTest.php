<?php
namespace BaseData\WorkOrderTask\ParentTask\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\WorkOrderTaskUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试父工单任务接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WorkOrderTaskUtils, TemplateSetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_bj_template');
    }

    /**
     * 存在多条父工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_parent_task' => $this->parentTask(),
                'pcore_bj_template' => $this->bjTemplate()
            ]
        );
    }

    public function testViewParentTask()
    {
        $setParentTaskList = $this->parentTask();
        $setTemplateList = $this->bjTemplate();
        
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'parentTasks/1,2?include=template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $parentTask) {
            $this->parentTaskCompareData($setParentTaskList[$key], $parentTask);
        }

        $included = $contents['included'];

        foreach ($included as $key => $template) {
            $this->assertEquals($setTemplateList[$key]['bj_template_id'], $template['id']);
            $this->assertEquals('bjTemplates', $template['type']);

            $attributes = $template['attributes'];
            $this->assertEquals($setTemplateList[$key]['name'], $attributes['name']);
        }
    }
}

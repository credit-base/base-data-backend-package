<?php
namespace BaseData\ResourceCatalogData\Strategy;

use Marmot\Core;

use Respect\Validation\Validator as V;

class DateStrategy implements IDataStrategy
{
    const DEFAULT_LENGTH = 8;

    public function validate($value, $rule = array()) : bool
    {
        unset($rule);
        if (!V::date('Ymd')->validate($value)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'data'));
            return false;
        }

        return true;
    }

    public function mock($rule = array())
    {
        unset($rule);
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed(mt_rand());
        return $faker->date('Ymd');
    }
}

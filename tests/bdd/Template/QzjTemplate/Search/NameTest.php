<?php
namespace BaseData\Template\QzjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看特定条件资源目录时,在目录管理子系统下的前置机资源目录管理中,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录数
 * @Scenario: 通过目录名称搜索
 */
class NameTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchQzjTemplateList()
    {
        $repository = new QzjTemplateRepository();
 
        $filter['name'] = '前置机';

        list($qzjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $qzjTemplateList;
    }

    /**
     * @Then: 我可以看到名称中带"前置机"的所有资源目录
     */
    public function testViewQzjTemplateList()
    {
        $nameQzjTemplateList = $this->qzjTemplate();

        foreach ($nameQzjTemplateList as $key => $nameQzjTemplate) {
            $nameQzjTemplateList[$key]['qzj_template_id'] = $key +1;
            $nameQzjTemplateList[$key]['subject_category'] = json_decode($nameQzjTemplate['subject_category'], true);
            $nameQzjTemplateList[$key]['items'] = json_decode($nameQzjTemplate['items'], true);
        }

        $qzjTemplateList = $this->fetchQzjTemplateList();
        $translator = new QzjTemplateDbTranslator();
        foreach ($qzjTemplateList as $qzjTemplate) {
            $qzjTemplateArray[] = $translator->objectToArray($qzjTemplate);
        }

        $this->assertEquals($qzjTemplateArray, $nameQzjTemplateList);

        foreach ($qzjTemplateArray as $qzjTemplate) {
            $this->assertEquals('前置机登记信息', $qzjTemplate['name']);
        }
    }
}

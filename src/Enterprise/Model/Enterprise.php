<?php
namespace BaseData\Enterprise\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\Enterprise\Repository\EnterpriseRepository;
use BaseData\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;

use BaseData\ResourceCatalogData\Model\Task;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class Enterprise implements IObject, IOperate
{
    use Object, OperateTrait;

    const STATUS = array(
        'NORMAL' => 0, //正常
        'DELETED' => -2, //删除
    );

    protected $id;
    /**
     * @var string $name 主体名称
     */
    protected $name;
    /**
     * @var string $unifiedSocialCreditCode 统一社会信用代码
     */
    protected $unifiedSocialCreditCode;
    /**
     * @var int $establishmentDate 成立日期/注册日期
     */
    protected $establishmentDate;
    /**
     * @var int $approvalDate 核准日期
     */
    protected $approvalDate;
    /**
     * @var string $address 住所/经营场所
     */
    protected $address;
    /**
     * @var string $registrationCapital 注册资本（万）
     */
    protected $registrationCapital;
    /**
     * @var int $businessTermStart 营业期限自
     */
    protected $businessTermStart;

    /**
     * @var int $businessTermTo 营业期限至
     */
    protected $businessTermTo;

    /**
     * @var string $businessScope 经营范围
     */
    protected $businessScope;

    /**
     * @var string $registrationAuthority 登记机关
     */
    protected $registrationAuthority;

    /**
     * @var string $principal 法定代表人/经营者
     */
    protected $principal;

    /**
     * @var string $principalCardId 法人身份证号
     */
    protected $principalCardId;

    /**
     * @var string $registrationStatus 登记状态
     */
    protected $registrationStatus;

    /**
     * @var string $enterpriseTypeCode 企业类型代码
     */
    protected $enterpriseTypeCode;

    /**
     * @var string $enterpriseType 企业类型
     */
    protected $enterpriseType;

    /**
     * @var array $data 其他数据
     */
    protected $data;

    /**
     * @var string $industryCategory 行业门类
     */
    protected $industryCategory;

    /**
     * @var string $industryCode 行业代码
     */
    protected $industryCode;

    /**
     * @var string $administrativeArea 所属区域
     */
    protected $administrativeArea;
    /**
     * @var string $hash 摘要hash
     */
    protected $hash;
    /**
     * @var Task $task 任务
     */
    protected $task;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->unifiedSocialCreditCode = '';
        $this->establishmentDate = 0;
        $this->approvalDate = 0;
        $this->address = '';
        $this->registrationCapital = '';
        $this->businessTermStart = 0;
        $this->businessTermTo = 0;
        $this->businessScope = '';
        $this->registrationAuthority = '';
        $this->principal = '';
        $this->principalCardId = '';
        $this->registrationStatus = '';
        $this->enterpriseTypeCode = '';
        $this->enterpriseType = '';
        $this->data = array();
        $this->industryCategory = '';
        $this->industryCode = '';
        $this->administrativeArea = 0;
        $this->hash = '';
        $this->task = new Task();
        $this->status = self::STATUS['NORMAL'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->unifiedSocialCreditCode);
        unset($this->establishmentDate);
        unset($this->approvalDate);
        unset($this->address);
        unset($this->registrationCapital);
        unset($this->businessTermStart);
        unset($this->businessTermTo);
        unset($this->businessScope);
        unset($this->registrationAuthority);
        unset($this->principal);
        unset($this->principalCardId);
        unset($this->registrationStatus);
        unset($this->enterpriseTypeCode);
        unset($this->enterpriseType);
        unset($this->data);
        unset($this->industryCategory);
        unset($this->industryCode);
        unset($this->administrativeArea);
        unset($this->hash);
        unset($this->task);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setUnifiedSocialCreditCode(string $unifiedSocialCreditCode) : void
    {
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
    }

    public function getUnifiedSocialCreditCode() : string
    {
        return $this->unifiedSocialCreditCode;
    }

    public function setEstablishmentDate(int $establishmentDate) : void
    {
        $this->establishmentDate = $establishmentDate;
    }

    public function getEstablishmentDate() : int
    {
        return $this->establishmentDate;
    }

    public function setApprovalDate(int $approvalDate) : void
    {
        $this->approvalDate = $approvalDate;
    }

    public function getApprovalDate() : int
    {
        return $this->approvalDate;
    }

    public function setAddress(string $address) : void
    {
        $this->address = $address;
    }

    public function getAddress() : string
    {
        return $this->address;
    }

    public function setRegistrationCapital(string $registrationCapital) : void
    {
        $this->registrationCapital = $registrationCapital;
    }

    public function getRegistrationCapital() : string
    {
        return $this->registrationCapital;
    }

    public function setBusinessTermStart(int $businessTermStart) : void
    {
        $this->businessTermStart = $businessTermStart;
    }

    public function getBusinessTermStart() : int
    {
        return $this->businessTermStart;
    }

    public function setBusinessTermTo(int $businessTermTo) : void
    {
        $this->businessTermTo = $businessTermTo;
    }

    public function getBusinessTermTo() : int
    {
        return $this->businessTermTo;
    }

    public function setBusinessScope(string $businessScope) : void
    {
        $this->businessScope = $businessScope;
    }

    public function getBusinessScope() : string
    {
        return $this->businessScope;
    }

    public function setRegistrationAuthority(string $registrationAuthority) : void
    {
        $this->registrationAuthority = $registrationAuthority;
    }

    public function getRegistrationAuthority() : string
    {
        return $this->registrationAuthority;
    }

    public function setPrincipal(string $principal) : void
    {
        $this->principal = $principal;
    }

    public function getPrincipal() : string
    {
        return $this->principal;
    }

    public function setPrincipalCardId(string $principalCardId) : void
    {
        $this->principalCardId = $principalCardId;
    }

    public function getPrincipalCardId() : string
    {
        return $this->principalCardId;
    }

    public function setRegistrationStatus(string $registrationStatus) : void
    {
        $this->registrationStatus = $registrationStatus;
    }

    public function getRegistrationStatus() : string
    {
        return $this->registrationStatus;
    }

    public function setEnterpriseTypeCode(string $enterpriseTypeCode) : void
    {
        $this->enterpriseTypeCode = $enterpriseTypeCode;
    }

    public function getEnterpriseTypeCode() : string
    {
        return $this->enterpriseTypeCode;
    }

    public function setEnterpriseType(string $enterpriseType) : void
    {
        $this->enterpriseType = $enterpriseType;
    }

    public function getEnterpriseType() : string
    {
        return $this->enterpriseType;
    }

    public function setData(array $data) : void
    {
        $this->data = $data;
    }

    public function getData() : array
    {
        return $this->data;
    }

    public function setIndustryCategory(string $industryCategory) : void
    {
        $this->industryCategory = $industryCategory;
    }

    public function getIndustryCategory() : string
    {
        return $this->industryCategory;
    }

    public function setIndustryCode(string $industryCode) : void
    {
        $this->industryCode = $industryCode;
    }

    public function getIndustryCode() : string
    {
        return $this->industryCode;
    }

    public function setAdministrativeArea(int $administrativeArea) : void
    {
        $this->administrativeArea = $administrativeArea;
    }

    public function getAdministrativeArea() : int
    {
        return $this->administrativeArea;
    }

    public function setTask(Task $task) : void
    {
        $this->task = $task;
    }

    public function getTask() : Task
    {
        return $this->task;
    }

    public function setHash(string $hash) : void
    {
        $this->hash = $hash;
    }

    public function getHash() : string
    {
        return $this->hash;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['NORMAL'];
    }

    protected function getRepository() : IEnterpriseAdapter
    {
        return $this->repository;
    }

    protected function addAction() : bool
    {
        return $this->isNameExist() && $this->isUnifiedSocialCreditCodeExist() && $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        return false;
    }
    
    protected function isNameExist() : bool
    {
        $filter = array();

        $filter['uniqueName'] = $this->getName();
        
        list($enterpriseList, $count) = $this->getRepository()->filter($filter);
        unset($enterpriseList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'enterpriseName'));
            return false;
        }

        return true;
    }
    
    protected function isUnifiedSocialCreditCodeExist() : bool
    {
        $filter = array();

        $filter['unifiedSocialCreditCode'] = $this->getUnifiedSocialCreditCode();
        
        list($enterpriseList, $count) = $this->getRepository()->filter($filter);
        unset($enterpriseList);
        
        if (!empty($count)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'unifiedSocialCreditCode'));
            return false;
        }

        return true;
    }
}

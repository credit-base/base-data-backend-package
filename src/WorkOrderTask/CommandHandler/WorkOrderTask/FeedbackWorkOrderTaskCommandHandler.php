<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;
use Marmot\Core;

use BaseData\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;

class FeedbackWorkOrderTaskCommandHandler implements ICommandHandler
{
    use WorkOrderTaskCommandHandlerTrait;

    private $repository;

    public function __construct()
    {
        $this->repository = new WorkOrderTaskRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getWorkOrderTaskRepository() : IWorkOrderTaskAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof FeedbackWorkOrderTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getWorkOrderTaskRepository();
        $workOrderTask = $repository->fetchOne($command->id);

        $crew = $this->fetchOneCrew($command->feedbackRecords[0]['crew']);
        $userGroup = $this->fetchOneUserGroup($command->feedbackRecords[0]['userGroup']);
        $command->feedbackRecords[0]['crewName'] = $crew->getRealName();
        $command->feedbackRecords[0]['userGroupName'] = $userGroup->getName();
        $command->feedbackRecords[0]['feedbackTime'] = Core::$container->get('time');

        if ($command->feedbackRecords[0]['templateId']) {
            $wbjTemplate = $this->fetchOneWbjTemplate($command->feedbackRecords[0]['templateId']);
            $command->feedbackRecords[0]['items'] = $wbjTemplate->getItems();
        }
        
        $feedbackRecordsData = $workOrderTask->getFeedbackRecords();
        $feedbackRecordsInfo = array_merge($feedbackRecordsData, $command->feedbackRecords);

        $workOrderTask->setFeedbackRecords($feedbackRecordsInfo);

        return $workOrderTask->feedback();
    }
}

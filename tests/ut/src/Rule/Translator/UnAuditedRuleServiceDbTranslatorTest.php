<?php
namespace BaseData\Rule\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Rule\Utils\RuleServiceUtils;

class UnAuditedRuleServiceDbTranslatorTest extends TestCase
{
    use RuleServiceUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new UnAuditedRuleServiceDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }
    
    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Rule\Model\NullUnAuditedRuleService', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $expression['rule_id'] = $unAuditedRuleService->getId();
        $expression['apply_form_rule_id'] = $unAuditedRuleService->getId();
        $expression['publish_crew_id'] = $unAuditedRuleService->getPublishCrew()->getId();
        $expression['usergroup_id'] = $unAuditedRuleService->getUserGroup()->getId();
        $expression['apply_crew_id'] = $unAuditedRuleService->getApplyCrew()->getId();
        $expression['apply_usergroup_id'] = $unAuditedRuleService->getApplyUserGroup()->getId();
        $expression['operation_type'] = $unAuditedRuleService->getOperationType();
        $expression['apply_info'] = base64_encode(gzcompress(serialize($unAuditedRuleService->getApplyInfo())));
        $expression['transformation_category'] = $unAuditedRuleService->getTransformationCategory();
        $expression['source_category'] = $unAuditedRuleService->getSourceCategory();
        $expression['transformation_template_id'] = $unAuditedRuleService->getTransformationTemplate()->getId();
        $expression['source_template_id'] = $unAuditedRuleService->getSourceTemplate()->getId();
        $expression['relation_id'] = $unAuditedRuleService->getRelationId();
        $expression['reject_reason'] = $unAuditedRuleService->getRejectReason();
        $expression['apply_status'] = $unAuditedRuleService->getApplyStatus();
        $expression['status_time'] = $unAuditedRuleService->getStatusTime();
        $expression['create_time'] = $unAuditedRuleService->getCreateTime();
        $expression['update_time'] = $unAuditedRuleService->getUpdateTime();

        $unAuditedRuleService = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Rule\Model\UnAuditedRuleService', $unAuditedRuleService);
        $this->compareArrayAndObjectUnAuditedRuleService($expression, $unAuditedRuleService);
    }

    public function testArrayToObjects()
    {
        $unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $expression['rule_id'] = $unAuditedRuleService->getApplyId();
        $expression['transformation_template_id'] = $unAuditedRuleService->getTransformationTemplate()->getId();
        $expression['source_template_id'] = $unAuditedRuleService->getSourceTemplate()->getId();
        $expression['rules'] = base64_encode(gzcompress(serialize($unAuditedRuleService->getRules())));
        $expression['version'] = $unAuditedRuleService->getVersion();
        $expression['data_total'] = $unAuditedRuleService->getDataTotal();
        $expression['crew_id'] = $unAuditedRuleService->getCrew()->getId();
        $expression['usergroup_id'] = $unAuditedRuleService->getUserGroup()->getId();
        $expression['transformation_category'] = $unAuditedRuleService->getTransformationCategory();
        $expression['source_category'] = $unAuditedRuleService->getSourceCategory();
        $expression['publish_crew_id'] = $unAuditedRuleService->getPublishCrew()->getId();

        $expression['status'] = $unAuditedRuleService->getStatus();
        $expression['status_time'] = $unAuditedRuleService->getStatusTime();
        $expression['create_time'] = $unAuditedRuleService->getCreateTime();
        $expression['update_time'] = $unAuditedRuleService->getUpdateTime();

        $unAuditedRuleService = $this->translator->arrayToObjects($expression, $unAuditedRuleService);

        $this->assertInstanceof('BaseData\Rule\Model\UnAuditedRuleService', $unAuditedRuleService);
        $this->compareArrayAndObjectsUnAuditedRuleService($expression, $unAuditedRuleService);
    }

    public function testObjectToArray()
    {
        $unAuditedRuleService = \BaseData\Rule\Utils\MockFactory::generateUnAuditedRuleService(1);

        $expression = $this->translator->objectToArray($unAuditedRuleService);

        $this->compareArrayAndObjectUnAuditedRuleService($expression, $unAuditedRuleService);
    }
}

<?php
namespace BaseData\WorkOrderTask\WidgetRule;

use Respect\Validation\Validator as V;

use Marmot\Core;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\Common\WidgetRule\CommonWidgetRule;
use BaseData\Template\WidgetRule\TemplateWidgetRule;

/**
 * @SuppressWarnings(PHPMD)
 */
class WorkOrderTaskWidgetRule
{
    const KEYS = array(
        'crew',
        'userGroup',
        'isExistedTemplate',
        'templateId',
        'items',
        'reason'
    );

    public function feedbackRecords($feedbackRecords) : bool
    {
        if (empty($feedbackRecords) || !is_array($feedbackRecords)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'feedbackRecords'));
            return false;
        }
        $commonWidgetRule = new CommonWidgetRule();
        foreach ($feedbackRecords as $each) {
            if (!empty(array_diff(self::KEYS, array_keys($each)))) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'feedbackRecords'));
                return false;
            }
            if (!V::intVal()->validate($each['crew'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'crew'));
                return false;
            }
            if (!V::intVal()->validate($each['userGroup'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'userGroup'));
                return false;
            }
            if (!$this->isExistedTemplate($each['isExistedTemplate'])) {
                return false;
            }
            if (!$this->templateId($each['isExistedTemplate'], $each['templateId'])) {
                return false;
            }
            if (!$this->items($each['isExistedTemplate'], $each['items'])) {
                return false;
            }
            if (!$commonWidgetRule->reason($each['reason'])) {
                return false;
            }
        }
        return true;
    }

    protected function isExistedTemplate($isExistedTemplate) : bool
    {
        if (!V::intVal()->validate($isExistedTemplate)
            || !in_array($isExistedTemplate, WorkOrderTask::IS_EXISTED_TEMPLATE)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'isExistedTemplate'));
            return false;
        }
        return true;
    }

    protected function templateId($isExistedTemplate, $templateId) : bool
    {
        if ($isExistedTemplate == WorkOrderTask::IS_EXISTED_TEMPLATE['FOU']) {
            if (!empty($templateId)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'templateId'));
                return false;
            }
        }
        if ($isExistedTemplate == WorkOrderTask::IS_EXISTED_TEMPLATE['SHI']) {
            if (!V::intVal()->validate($templateId)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'templateId'));
                return false;
            }
        }
        return true;
    }

    protected function items($isExistedTemplate, $items) : bool
    {
        if ($isExistedTemplate == WorkOrderTask::IS_EXISTED_TEMPLATE['FOU']) {
            $templateWidgetRule = new TemplateWidgetRule();
            if (!$templateWidgetRule->items(array(), $items)) {
                return false;
            }
        }
        if ($isExistedTemplate == WorkOrderTask::IS_EXISTED_TEMPLATE['SHI']) {
            if (!empty($items)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'items'));
                return false;
            }
        }
        return true;
    }
}

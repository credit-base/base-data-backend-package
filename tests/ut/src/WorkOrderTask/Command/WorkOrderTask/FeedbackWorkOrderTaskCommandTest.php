<?php
namespace BaseData\WorkOrderTask\Command\WorkOrderTask;

use PHPUnit\Framework\TestCase;

class FeedbackWorkOrderTaskCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $this->fakerData = array(
            'feedbackRecords' => array('feedbackRecords'),
            'id' => 10,
        );

        $this->command = new FeedbackWorkOrderTaskCommand(
            $this->fakerData['feedbackRecords'],
            $this->fakerData['id']
        );
    }
    
    /**
     * 1. 测试是否实现 ICommand
     */
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testFeedbackRecordsParameter()
    {
        $this->assertEquals($this->fakerData['feedbackRecords'], $this->command->feedbackRecords);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}

<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Command\RuleService\RevokeRuleServiceCommand;

use BaseData\Crew\Model\Crew;

class RevokeRuleServiceCommandHandlerTest extends TestCase
{
    private $commandHandler;

    public function setUp()
    {
        $this->commandHandler = new RevokeRuleServiceCommandHandler();
    }

    public function tearDown()
    {
        unset($this->commandHandler);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->commandHandler
        );
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->commandHandler->execute($command);
    }

    public function testExecute()
    {
        //初始化
        $id = 1;
        $crew = 2;
        $command = new RevokeRuleServiceCommand(
            $crew,
            $id
        );

        $crew = new Crew();

        $commandHandler = $this->getMockBuilder(RevokeRuleServiceCommandHandler::class)
                                     ->setMethods(
                                         ['fetchUnAuditedRuleService','fetchCrew']
                                     )
                                     ->getMock();

        $unAuditedRuleService = $this->prophesize(UnAuditedRuleService::class);
        $unAuditedRuleService->setPublishCrew(Argument::exact($crew))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->setCrew(Argument::exact($crew))
                             ->shouldBeCalledTimes(1);
        $unAuditedRuleService->revoke()->shouldBeCalledTimes(1)
                             ->willReturn(true);

        $commandHandler->expects($this->once())
                       ->method('fetchUnAuditedRuleService')
                       ->with($command->id)
                       ->willReturn($unAuditedRuleService->reveal());

        $commandHandler->expects($this->once())
                       ->method('fetchCrew')
                       ->with($command->crew)
                       ->willReturn($crew);

        $result = $commandHandler->execute($command);
        $this->assertTrue($result);
    }
}

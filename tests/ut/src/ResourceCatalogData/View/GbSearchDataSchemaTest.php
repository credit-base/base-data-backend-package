<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class GbSearchDataSchemaTest extends TestCase
{
    private $gbSearchDataSchema;

    private $gbSearchData;

    public function setUp()
    {
        $this->gbSearchDataSchema = new GbSearchDataSchema(new Factory());

        $this->gbSearchData = \BaseData\ResourceCatalogData\Utils\GbSearchDataMockFactory::generateGbSearchData(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->gbSearchDataSchema);
        unset($this->gbSearchData);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->gbSearchDataSchema);
    }

    public function testGetId()
    {
        $result = $this->gbSearchDataSchema->getId($this->gbSearchData);

        $this->assertEquals($result, $this->gbSearchData->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->gbSearchDataSchema->getAttributes($this->gbSearchData);

        $this->assertEquals($result['infoClassify'], $this->gbSearchData->getInfoClassify());
        $this->assertEquals($result['infoCategory'], $this->gbSearchData->getInfoCategory());
        $this->assertEquals($result['subjectCategory'], $this->gbSearchData->getSubjectCategory());
        $this->assertEquals($result['dimension'], $this->gbSearchData->getDimension());
        $this->assertEquals($result['name'], $this->gbSearchData->getName());
        $this->assertEquals($result['identify'], $this->gbSearchData->getIdentify());
        $this->assertEquals($result['expirationDate'], $this->gbSearchData->getExpirationDate());
        $this->assertEquals($result['description'], $this->gbSearchData->getDescription());
        $this->assertEquals($result['frontEndProcessorStatus'], $this->gbSearchData->getFrontEndProcessorStatus());
        $this->assertEquals($result['status'], $this->gbSearchData->getStatus());
        $this->assertEquals($result['createTime'], $this->gbSearchData->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->gbSearchData->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->gbSearchData->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->gbSearchDataSchema->getRelationships($this->gbSearchData, 0, array());

        $this->assertEquals($result['itemsData'], ['data' => $this->gbSearchData->getItemsData()]);
        $this->assertEquals($result['template'], ['data' => $this->gbSearchData->getTemplate()]);
        $this->assertEquals($result['sourceUnit'], ['data' => $this->gbSearchData->getSourceUnit()]);
        $this->assertEquals($result['crew'], ['data' => $this->gbSearchData->getCrew()]);
    }
}

# 问题反馈控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

* [互动类通用控件](./docs/WidgetRule/interaction.md "互动类通用控件")

## 问题反馈控件规范

* `TITLE`: 标题控件规范.
* `CONTENT`: 内容控件规范.
* `MEMBER`: 前台用户控件规范.
* `ACCEPT_USER_GROUP`: 受理委办局控件规范.
* `REPLAY_CONTENT`: 回复内容控件规范.
* `REPLAY_IMAGES`: 回复图片控件规范.
* `REPLAY_CREW`: 受理人控件规范.
* `REPLAY_ADMISSIBILITY`: 回复受理情况控件规范.

### `TITLE`
* [标题控件规范(TITLE)](interaction.md)

### `CONTENT`
* [内容控件规范(CONTENT)](interaction.md)

### `MEMBER`
* [前台用户控件规范(MEMBER)](interaction.md)

### `ACCEPT_USER_GROUP`
* [受理委办局控件规范(ACCEPT_USER_GROUP)](interaction.md)

### `REPLAY_CONTENT`
* [回复内容控件规范(REPLAY_CONTENT)](interaction.md)

### `REPLAY_IMAGES`
* [回复图片控件规范(REPLAY_IMAGES)](interaction.md)

### `REPLAY_CREW`
* [受理人控件规范(REPLAY_CREW)](interaction.md)

### `REPLAY_ADMISSIBILITY`
* [回复受理情况控件规范(REPLAY_ADMISSIBILITY)](interaction.md)

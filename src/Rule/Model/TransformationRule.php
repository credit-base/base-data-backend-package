<?php
namespace BaseData\Rule\Model;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class TransformationRule extends Rule implements IRule
{
    use RuleTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 验证转换规则格式
        * 规则格式说明及样例
            * array('ZTMC'=>'ZTMC','TYSHXYDM'=>TYSHXYDM, '被转换资源目录模板标识'=>'转换资源目录模板标识')
        * 验证流程
            * 1. 获取转换规则 $condition = $this->getCondition();
            * 2. 获取来源和目标资源目录
            * 3. 根据转换项标识获取转换项的所有数据,并验证转换项是否都存在
            * 4. 对比目标和转换项的模板字段的类型:
                * 数据类型对应关系(目标:来源): 日期:日期,字符串:全部,集合:集合,枚举:枚举,整型:整型,浮点:浮点-整型
                * 数据长度: 目标>=来源(全部),目标项包含来源(集合-枚举)
                * 公开范围: 目标>=来源
                * 是否脱敏: 不脱敏>脱敏
                * 脱敏规则: 目标>=来源
     * @return bool
     * @todo 后续需要对返回错误输出信息或日志，方便调试
     */
    public function validate() : bool
    {
        //获取来源资源目录的模板信息项
        $sourceTemplate = $this->getSourceTemplate();
        $sourceTemplateItems = $this->fetchTemplateItems($sourceTemplate);

        //获取目标资源目录的模板信息项
        $transformationTemplate = $this->getTransformationTemplate();
        $transformationTemplateItems = $this->fetchTemplateItems($transformationTemplate);

        //获取转换规则
        $condition = $this->getCondition();

        foreach ($condition as $transformationIdentify => $sourceIdentify) {
            //验证转换规则的目标信息项存在
            if (!isset($transformationTemplateItems[$transformationIdentify])) {
                Core::setLastError(
                    PARAMETER_FORMAT_ERROR,
                    array('pointer'=>'transformationRulesTransformationNotExist')
                );
                return false;
            }
            //验证转换规则的来源信息项存在
            if (!isset($sourceTemplateItems[$sourceIdentify])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'transformationRulesSourceNotExist'));
                return false;
            }

            if (!$this->templateItemComparison(
                $transformationTemplateItems[$transformationIdentify],
                $sourceTemplateItems[$sourceIdentify]
            )) {
                return false;
            }
        }

        return true;
    }

    //实现父类的抽象方法
    protected function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        unset($transformationSearchData);
        return true;
    }

    /**
     * 初始化数据(实现父类的抽象方法)
     * @param ISearchDataAble $sourceSearchData 来源资源目录数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * condition 转换条件
        * sourceItemsData 来源资源目录数据
        * transformationItemsData 目标资源目录数据
        * requiredItems 必填资源目录模板信息项
     */
    protected function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {
        //获取转换条件
        $condition = $this->getCondition();
        //获取目标资源目录
        $transformationTemplate = $transformationSearchData->getTemplate();
        //获取来源资源目录数据信息项
        $sourceItemsData = $sourceSearchData->getItemsData()->getData();
        //获取目标资源目录数据信息项
        $transformationItemsData = $transformationSearchData->getItemsData()->getData();
        //获取目标资源目录必填项
        $requiredItems = $this->transformationTemplateRequired($transformationTemplate);

        return array(
            'condition' => $condition,
            'sourceItemsData' => $sourceItemsData,
            'transformationItemsData' => $transformationItemsData,
            'requiredItems' => $requiredItems
        );
    }

    //验证初始化数据格式
    protected function validateInitializationDataFormat(array $data) : bool
    {
        return (isset($data['condition']) &&
                isset($data['sourceItemsData']) &&
                isset($data['transformationItemsData']) &&
                isset($data['requiredItems']));
    }

    /**
     * 执行转换过程(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * transformationItemsData 目标资源目录数据模板数据
        * incompleteItems 待补全的数据信息项
     */
    protected function transformationExecute(array $data, ISearchDataAble $transformationSearchData) : array
    {
        unset($transformationSearchData);
        //验证初始化数据格式,false返回空数组
        if (!$this->validateInitializationDataFormat($data)) {
            return array();
        }
        //通过转换条件将来源资源目录的数据项赋值给目标资源目录的目标项
        $transformationItemsData = $this->generateItemsData(
            $data['sourceItemsData'],
            $data['transformationItemsData'],
            $data['condition']
        );

        //获取待补全的数据信息项
        $incompleteItems = $this->fetchIncompleteItems($transformationItemsData, $data['requiredItems']);

        return array(
            'transformationItemsData' => $transformationItemsData,
            'incompleteItems' => $incompleteItems
        );
    }

    //验证执行后返回的数据格式
    protected function validateDataFormat(array $data) : bool
    {
        return (isset($data['transformationItemsData']) && isset($data['incompleteItems']));
    }

    /**
     * 判断并返回结果(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return ISearchDataAble
     */
    protected function transformationResult(array $data, ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        //验证数据格式不正确,直接返回异常失败的目标资源目录数据
        if (!$this->validateDataFormat($data)) {
            $transformationSearchData = $this->failureDataException(
                $transformationSearchData,
                ErrorData::STATUS['PROGRAM_EXCEPTION']
            );
        }

        $incompleteItems = isset($data['incompleteItems']) ? $data['incompleteItems'] : array();
        $transformationItemsData = isset($data['transformationItemsData']) ? $data['transformationItemsData'] : array();

        //判断待补全信息项是否为空,空则返回完善的资源目录数据,不为空则返回待补全的资源目录数据
        $transformationSearchData = $this->transformationDataResult($incompleteItems, $transformationSearchData);
        $transformationSearchData->getItemsData()->setData($transformationItemsData);
        $transformationSearchData->generateHash();

        return $transformationSearchData;
    }
    
    //通过转换条件将来源资源目录的数据项赋值给目标资源目录数据的数据项
    protected function generateItemsData(
        array $sourceItemsData,
        array $transformationItemsData,
        array $condition
    ) : array {
        foreach ($condition as $transformationItemIdentify => $sourceItemIdentify) {
            if (isset($sourceItemsData[$sourceItemIdentify])) {
                $transformationItemsData[$transformationItemIdentify] = $sourceItemsData[$sourceItemIdentify];
            }
        }

        return $transformationItemsData;
    }
}

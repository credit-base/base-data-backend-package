<?php
namespace BaseData\WorkOrderTask\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;
use BaseData\WorkOrderTask\CommandHandler\WorkOrderTask\WorkOrderTaskCommandHandlerFactory;

trait WorkOrderTaskControllerTrait
{
    protected function getWorkOrderTaskWidgetRule() : WorkOrderTaskWidgetRule
    {
        return new WorkOrderTaskWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : IWorkOrderTaskAdapter
    {
        return new WorkOrderTaskRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new WorkOrderTaskCommandHandlerFactory());
    }
}

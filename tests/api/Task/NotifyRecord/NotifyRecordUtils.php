<?php
namespace BaseData\Task\NotifyRecord;

trait NotifyRecordUtils
{
    private function notifyRecordCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['record_id'], $responseDataArray['id']);
        $this->assertEquals('notifyRecords', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['name'], $attributes['name']);
        $this->assertEquals($setDataArray['hash'], $attributes['hash']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);
    }
}

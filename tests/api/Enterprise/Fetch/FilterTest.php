<?php
namespace BaseData\Enterprise\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Enterprise\SetDataApiTrait;
use BaseData\Enterprise\EnterpriseUtils;
use BaseData\Enterprise\Model\Enterprise;

/**
 * 测试企业接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, EnterpriseUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_enterprise');
    }

    /**
     * 存在多条企业数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_enterprise' => $this->enterprise()
            ]
        );
    }

    public function testViewEnterprise()
    {
        $setEnterpriseList = $this->enterprise();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'enterprises?filter[name]=企业名称&filter[unifiedSocialCreditCode]=912908Q21903E74AX2&
            filter[principal]=张萍&filter[identify]=912908Q21903E74AX2',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];

        foreach ($data as $key => $enterprise) {
            $this->enterpriseCompareData($setEnterpriseList[$key], $enterprise);
        }
    }
}

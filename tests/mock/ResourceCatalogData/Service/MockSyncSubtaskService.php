<?php
namespace BaseData\ResourceCatalogData\Service;

use BaseData\Rule\Model\RuleService;

use BaseData\ResourceCatalogData\Model\Task;

class MockSyncSubtaskService extends SyncSubtaskService
{
    public function getRuleService() : RuleService
    {
        return parent::getRuleService();
    }

    public function getTask() : Task
    {
        return parent::getTask();
    }

    public function getSubtask() : Task
    {
        return parent::getSubtask();
    }

    public function validateDataCorrect(RuleService $ruleService, Task $task) : bool
    {
        return parent::validateDataCorrect($ruleService, $task);
    }

    public function isRuleServiceExist(RuleService $ruleService) : bool
    {
        return parent::isRuleServiceExist($ruleService);
    }

    public function isTaskExist(Task $task) : bool
    {
        return parent::isTaskExist($task);
    }

    public function fetchSourceSearchData(RuleService $ruleService, Task $task) : array
    {
        return parent::fetchSourceSearchData($ruleService, $task);
    }

    public function addSubtask(RuleService $ruleService, Task $task, int $count) : Task
    {
        return parent::addSubtask($ruleService, $task, $count);
    }
}

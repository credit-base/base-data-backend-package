<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class ErrorDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'error_data_id',
            new Persistence\ErrorDataCache(),
            new Persistence\ErrorDataDb()
        );
    }
}

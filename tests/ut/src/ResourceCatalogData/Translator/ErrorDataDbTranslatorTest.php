<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\ErrorDataUtils;

class ErrorDataDbTranslatorTest extends TestCase
{
    use ErrorDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new ErrorDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullErrorData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    private function initialIsHashExist($errorData, $errorReason)
    {
        $expression['error_data_id'] = $errorData->getId();
        $expression['task_id'] = $errorData->getTask()->getId();
        $expression['category'] = $errorData->getCategory();
        $expression['template_id'] = $errorData->getTemplate()->getId();
        $expression['items_data'] = base64_encode(
            gzcompress(serialize($errorData->getItemsData()->getData()))
        );
        $expression['error_type'] = $errorData->getErrorType();
        $expression['error_reason'] = $errorReason;
        $expression['status'] = $errorData->getStatus();
        $expression['status_time'] = $errorData->getStatusTime();
        $expression['create_time'] = $errorData->getCreateTime();
        $expression['update_time'] = $errorData->getUpdateTime();

        $errorData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\ErrorData', $errorData);
        $this->compareArrayAndObject($expression, $errorData);
    }
    public function testArrayToObject()
    {
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
        $errorReason = $errorData->getErrorReason();

        $this->initialIsHashExist($errorData, $errorReason);
    }

    public function testArrayToObjectEncodeErrorReason()
    {
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);
        $errorReason = json_encode($errorData->getErrorReason());

        $this->initialIsHashExist($errorData, $errorReason);
    }

    public function testObjectToArray()
    {
        $errorData = \BaseData\ResourceCatalogData\Utils\ErrorDataMockFactory::generateErrorData(1);

        $expression = $this->translator->objectToArray($errorData);

        $this->compareArrayAndObject($expression, $errorData);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullWbjSearchDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullWbjSearchData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsWbjSearchData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\WbjSearchData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

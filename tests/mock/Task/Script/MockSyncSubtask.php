<?php
namespace BaseData\Task\Script;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RuleServiceRepository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\TaskRepository;
use BaseData\ResourceCatalogData\Service\SyncSubtaskService;

class MockSyncSubtask extends SyncSubtask
{
    public function getTaskRepository() : TaskRepository
    {
        return parent::getTaskRepository();
    }

    public function fetchTask(int $id) : Task
    {
        return parent::fetchTask($id);
    }

    public function getRuleServiceRepository() : RuleServiceRepository
    {
        return parent::getRuleServiceRepository();
    }

    public function fetchRuleService(int $id) : RuleService
    {
        return parent::fetchRuleService($id);
    }

    public function getSyncSubtaskService(RuleService $ruleService, Task $task) : SyncSubtaskService
    {
        return parent::getSyncSubtaskService($ruleService, $task);
    }

    public function getSyncSubtask() : SyncSubtask
    {
        return parent::getSyncSubtask();
    }

    public function getSyncErrorDataDownload() : SyncErrorDataDownload
    {
        return parent::getSyncErrorDataDownload();
    }
}

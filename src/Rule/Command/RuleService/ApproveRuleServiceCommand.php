<?php
namespace BaseData\Rule\Command\RuleService;

use BaseData\Common\Command\ApproveCommand;

class ApproveRuleServiceCommand extends ApproveCommand
{
    public $applyCrew;

    public function __construct(
        int $applyCrew,
        int $id = 0
    ) {
        parent::__construct(
            $id
        );
        $this->applyCrew = $applyCrew;
    }
}

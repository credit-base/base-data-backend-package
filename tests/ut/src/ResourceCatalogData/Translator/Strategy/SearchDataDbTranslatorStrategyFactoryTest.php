<?php
namespace BaseData\ResourceCatalogData\Translator\Strategy;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class SearchDataDbTranslatorStrategyFactoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new SearchDataDbTranslatorStrategyFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullStrategy()
    {
        $stub = $this->stub->getStrategy(0);
        
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\Strategy\NullSearchDataDbTranslatorStrategy',
            $stub
        );
    }

    public function testGetStrategy()
    {
        foreach (SearchDataDbTranslatorStrategyFactory::MAPS as $key => $strategy) {
            $this->assertInstanceOf(
                $strategy,
                $this->stub->getStrategy($key)
            );
        }
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\Persistence;

class MockYsItemsDataDb extends YsItemsDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Utils\BjSearchDataMockFactory;

class BjSearchDataMockAdapter implements IBjSearchDataAdapter
{
    public function fetchOne($id) : BjSearchData
    {
        return BjSearchDataMockFactory::generateBjSearchData($id);
    }

    public function fetchList(array $ids) : array
    {
        $bjSearchDataList = array();

        foreach ($ids as $id) {
            $bjSearchDataList[$id] = BjSearchDataMockFactory::generateBjSearchData($id);
        }

        return $bjSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(BjSearchData $bjSearchData) : bool
    {
        unset($bjSearchData);
        return true;
    }

    public function edit(BjSearchData $bjSearchData, array $keys = array()) : bool
    {
        unset($bjSearchData);
        unset($keys);
        return true;
    }
}

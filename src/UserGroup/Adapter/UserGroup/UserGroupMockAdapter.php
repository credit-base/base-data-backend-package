<?php
namespace BaseData\UserGroup\Adapter\UserGroup;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Utils\MockFactory;

class UserGroupMockAdapter implements IUserGroupAdapter
{
    public function fetchOne(int $id) : UserGroup
    {
        return MockFactory::generateUserGroup($id);
    }

    public function fetchList(array $ids) : array
    {
        $userGroupList = array();

        foreach ($ids as $id) {
            $userGroupList[$id] = MockFactory::generateUserGroup($id);
        }

        return $userGroupList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }
}

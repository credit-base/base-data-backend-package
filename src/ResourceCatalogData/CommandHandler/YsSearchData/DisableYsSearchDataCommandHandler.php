<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\CommandHandler\DisableCommandHandler;

class DisableYsSearchDataCommandHandler extends DisableCommandHandler
{
    use YsSearchDataCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchYsSearchData($id);
    }
}

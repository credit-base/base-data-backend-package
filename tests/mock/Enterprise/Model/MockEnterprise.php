<?php
namespace BaseData\Enterprise\Model;

use BaseData\Enterprise\Adapter\Enterprise\IEnterpriseAdapter;

class MockEnterprise extends Enterprise
{
    public function getRepository() : IEnterpriseAdapter
    {
        return parent::getRepository();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editAction() : bool
    {
        return parent::editAction();
    }

    public function isNameExist() : bool
    {
        return parent::isNameExist();
    }

    public function isUnifiedSocialCreditCodeExist() : bool
    {
        return parent::isUnifiedSocialCreditCodeExist();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\YsItemsData;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\IYsItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\YsItemsDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\YsItemsData\YsItemsDataMockAdapter;

class YsItemsDataRepository extends Repository implements IYsItemsDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new YsItemsDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IYsItemsDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IYsItemsDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IYsItemsDataAdapter
    {
        return new YsItemsDataMockAdapter();
    }

    public function fetchOne($id) : YsItemsData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(YsItemsData $ysItemsData) : bool
    {
        return $this->getAdapter()->add($ysItemsData);
    }
}

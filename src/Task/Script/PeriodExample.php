<?php
namespace BaseData\Task\Script;

use BaseData\Task\Model\PeriodTask;

class PeriodExample extends PeriodTask
{
    public function executeAction() : bool
    {
        //echo 'run period', PHP_EOL;

        sleep(2);
        return true;
    }

    public function getPeriod() : int
    {
        return 30;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\Persistence;

class MockGbSearchDataDb extends GbSearchDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

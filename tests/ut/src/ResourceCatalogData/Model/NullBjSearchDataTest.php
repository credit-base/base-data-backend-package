<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullBjSearchDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(NullBjSearchData::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsBjSearchData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\BjSearchData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }

    public function testConfirm()
    {
        $this->mockResourceNotExist();
        
        $result = $this->stub->confirm();
        $this->assertFalse($result);
    }

    public function testIsConfirm()
    {
        $this->mockResourceNotExist();
        
        $result = $this->stub->isConfirm();
        $this->assertFalse($result);
    }

    public function testDisable()
    {
        $this->mockResourceNotExist();
        
        $result = $this->stub->disable();
        $this->assertFalse($result);
    }

    public function testIsEnabled()
    {
        $this->mockResourceNotExist();
        
        $result = $this->stub->isEnabled();
        $this->assertFalse($result);
    }

    public function testDelete()
    {
        $this->mockResourceNotExist();
        
        $result = $this->stub->delete();
        $this->assertFalse($result);
    }

    public function testUpdateStatus()
    {
        $this->mockResourceNotExist();
        
        $result = $this->stub->updateStatus(1);
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->stub->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }
}

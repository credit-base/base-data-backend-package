<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\GbItemsDataUtils;
use BaseData\ResourceCatalogData\Utils\GbItemsDataMockFactory;

class GbItemsDataDbTranslatorTest extends TestCase
{
    use GbItemsDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new GbItemsDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullGbItemsData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $gbItemsData = GbItemsDataMockFactory::generateGbItemsData(1);

        $expression['gb_items_data_id'] = $gbItemsData->getId();

        $expression['data'] = base64_encode(gzcompress(serialize($gbItemsData->getData())));

        $gbItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\GbItemsData', $gbItemsData);
        $this->compareArrayAndObject($expression, $gbItemsData);
    }

    public function testObjectToArray()
    {
        $gbItemsData = GbItemsDataMockFactory::generateGbItemsData(1);

        $expression = $this->translator->objectToArray($gbItemsData);

        $this->compareArrayAndObject($expression, $gbItemsData);
    }
}

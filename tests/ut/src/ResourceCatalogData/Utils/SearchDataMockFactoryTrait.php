<?php
namespace BaseData\ResourceCatalogData\Utils;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

trait SearchDataMockFactoryTrait
{
    protected static function generateInfoClassify(ISearchDataAble $searchData, $faker, $value) : void
    {
        $infoClassify = isset($value['infoClassify']) ?
            $value['infoClassify'] :
            $faker->randomElement(
                ISearchDataAble::INFO_CLASSIFY
            );
        
        $searchData->setInfoClassify($infoClassify);
    }

    protected static function generateInfoCategory(ISearchDataAble $searchData, $faker, $value) : void
    {
        $infoCategory = isset($value['infoCategory']) ?
            $value['infoCategory'] :
            $faker->randomElement(
                ISearchDataAble::INFO_CATEGORY
            );
        
        $searchData->setInfoCategory($infoCategory);
    }

    protected static function generateCrew(ISearchDataAble $searchData, $faker, $value) : void
    {
        $crew = isset($value['crew']) ?
        $value['crew'] :
        \BaseData\Crew\Utils\MockFactory::generateCrew($faker->randomDigit());
        
        $searchData->setCrew($crew);
    }

    protected static function generateSourceUnit(ISearchDataAble $searchData, $faker, $value) : void
    {
        $sourceUnit = isset($value['sourceUnit']) ?
        $value['sourceUnit'] :
        \BaseData\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
        
        $searchData->setSourceUnit($sourceUnit);
    }

    protected static function generateSubjectCategory(ISearchDataAble $searchData, $faker, $value) : void
    {
        $subjectCategory = isset($value['subjectCategory']) ?
            $value['subjectCategory'] :
            $faker->randomElement(
                ISearchDataAble::SUBJECT_CATEGORY
            );
        
        $searchData->setSubjectCategory($subjectCategory);
    }

    protected static function generateDimension(ISearchDataAble $searchData, $faker, $value) : void
    {
        $dimension = isset($value['dimension']) ?
            $value['dimension'] :
            $faker->randomElement(
                ISearchDataAble::DIMENSION
            );
        
        $searchData->setDimension($dimension);
    }

    protected static function generateExpirationDate(ISearchDataAble $searchData, $faker, $value) : void
    {
        $expirationDate = isset($value['expirationDate']) ?
            $value['expirationDate'] :
            $faker->unixTime();
        
        $searchData->setExpirationDate($expirationDate);
    }

    protected static function generateTask(ISearchDataAble $searchData, $faker, $value) : void
    {
        $task = isset($value['task']) ?
        $value['task'] :
        new Task($faker->randomDigit());
        
        $searchData->setTask($task);
    }
}

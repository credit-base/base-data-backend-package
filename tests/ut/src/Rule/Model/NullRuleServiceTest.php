<?php
namespace BaseData\Rule\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class NullRuleServiceTest extends TestCase
{
    private $ruleService;

    public function setUp()
    {
        $this->ruleService = $this->getMockBuilder(MockNullRuleService::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->ruleService);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Model\RuleService',
            $this->ruleService
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->ruleService
        );
    }

    public function testIsNormal()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->isNormal();
        $this->assertFalse($result);
    }

    public function testDelete()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->delete();
        $this->assertFalse($result);
    }
    
    public function testValidate()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->validate();
        $this->assertFalse($result);
    }

    public function testCommonValidate()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->commonValidate();
        $this->assertFalse($result);
    }

    public function testIsCrewNull()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->isCrewNull();
        $this->assertFalse($result);
    }

    public function testIsUserGroupNull()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->isUserGroupNull();
        $this->assertFalse($result);
    }

    public function testIsSourceTemplateNull()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->isSourceTemplateNull();
        $this->assertFalse($result);
    }

    public function testIsTransformationTemplateNull()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->isTransformationTemplateNull();
        $this->assertFalse($result);
    }

    public function testIsRuleExist()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->isRuleExist();
        $this->assertFalse($result);
    }

    public function testRequiredItemVerification()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->requiredItemVerification();
        $this->assertFalse($result);
    }

    public function testRulesValidate()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->rulesValidate();
        $this->assertFalse($result);
    }

    public function testUpdateTemplateRuleCount()
    {
        $this->mockResourceNotExist();

        $result = $this->ruleService->updateTemplateRuleCount();
        $this->assertFalse($result);
    }
    
    private function mockResourceNotExist()
    {
        $this->ruleService->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);
    }

    public function testTransformation()
    {
        $sourceSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);
        
        $result = $this->ruleService->transformation($sourceSearchData);
        
        $this->assertEquals($sourceSearchData, $result);
    }

    public function testGetSortRules()
    {
        $data = array('data');
        
        $result = $this->ruleService->getSortRules($data);
        
        $this->assertEquals($data, $result);
    }
}

<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Rule\Command\RuleService\EditRuleServiceCommand;

class EditRuleServiceCommandHandler implements ICommandHandler
{
    use RuleServiceCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditRuleServiceCommand)) {
            throw new \InvalidArgumentException;
        }

        $ruleService = $this->fetchRuleService($command->relationId);

        $unAuditedRuleService = $this->getUnAuditedRuleService();

        $unAuditedRuleService->setTransformationTemplate($ruleService->getTransformationTemplate());
        $unAuditedRuleService->setSourceTemplate($ruleService->getSourceTemplate());
        $unAuditedRuleService->setUserGroup($ruleService->getSourceTemplate()->getSourceUnit());
        $unAuditedRuleService->generateVersion();
        $unAuditedRuleService->setTransformationCategory($ruleService->getTransformationCategory());
        $unAuditedRuleService->setSourceCategory($ruleService->getSourceCategory());
        $unAuditedRuleService->setOperationType($command->operationType);
        $unAuditedRuleService->setId($command->relationId);
        $unAuditedRuleService->setRelationId($command->relationId);
        $unAuditedRuleService = $this->generateUnAuditedRuleServiceCrew($command->crew, $unAuditedRuleService);
        $unAuditedRuleService = $this->generateUnAuditedRuleServiceRules($command->rules, $unAuditedRuleService);
        $applyInfo = $this->applyInfo($unAuditedRuleService);
        $unAuditedRuleService->setApplyInfo($applyInfo);

        if ($unAuditedRuleService->edit()) {
            $command->id = $unAuditedRuleService->getApplyId();
            return true;
        }

        return false;
    }
}

<?php
namespace BaseData\Crew\Adapter\Crew;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Marmot\Basecode\Adapter\Restful\GuzzleAdapter;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Model\NullCrew;
use BaseData\Crew\Translator\CrewRestfulTranslator;

use BaseData\Common\Adapter\FetchAbleRestfulAdapterTrait;

class CrewRestfulAdapter extends GuzzleAdapter implements ICrewAdapter
{
    use FetchAbleRestfulAdapterTrait;

    private $translator;
    
    private $resource;

    const SCENARIOS = [
        'CREW_LIST'=>[
            'fields' => []
        ],
        'CREW_FETCH_ONE'=>[
            'fields' => []
        ]
    ];

    public function __construct(string $uri = '', array $authKey = [])
    {
        parent::__construct(
            $uri,
            $authKey
        );

        $this->translator = new CrewRestfulTranslator();
        $this->resource = 'crews';
        $this->scenario = array();
    }

    protected function getTranslator() : IRestfulTranslator
    {
        return $this->translator;
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne(int $id) : Crew
    {
        $this->get(
            $this->getResource().'/'.$id
        );

        return $this->isSuccess() ? $this->translateToObject() : NullCrew::getInstance();
    }
}

<?php
namespace BaseData\Template\Adapter\BaseTemplate;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\BaseTemplate;
use BaseData\Template\Model\NullBaseTemplate;
use BaseData\Template\Translator\BaseTemplateDbTranslator;
use BaseData\Template\Adapter\BaseTemplate\Query\BaseTemplateRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class BaseTemplateDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(BaseTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IBaseTemplateAdapter
     */
    public function testImplementsIBaseTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 BaseTemplateDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockBaseTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Translator\BaseTemplateDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 BaseTemplateRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockBaseTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\BaseTemplate\Query\BaseTemplateRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockBaseTemplateDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedBaseTemplate = new BaseTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedBaseTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedBaseTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedBaseTemplate = new BaseTemplate();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedBaseTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedBaseTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedBaseTemplate = new BaseTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedBaseTemplate);

        //验证
        $baseTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedBaseTemplate, $baseTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $baseTemplateOne = new BaseTemplate(1);
        $baseTemplateTwo = new BaseTemplate(2);

        $ids = [1, 2];

        $expectedBaseTemplateList = [];
        $expectedBaseTemplateList[$baseTemplateOne->getId()] = $baseTemplateOne;
        $expectedBaseTemplateList[$baseTemplateTwo->getId()] = $baseTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedBaseTemplateList);

        //验证
        $baseTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedBaseTemplateList, $baseTemplateList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 'name',
        );
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterIdentify()
    {
        $filter = array(
            'identify' => 'identify',
        );
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = 'identify = \''.$filter['identify'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterDimension()
    {
        $filter = array(
            'dimension' => BaseTemplate::DIMENSION['SHGK']
        );
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = 'dimension = '.$filter['dimension'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoClassify()
    {
        $filter = array(
            'infoClassify' => 1
        );
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = 'info_classify = '.$filter['infoClassify'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoCategory()
    {
        $filter = array(
            'infoCategory' => 1
        );
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = 'info_category = '.$filter['infoCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = ' ORDER BY base_template_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockBaseTemplateDbAdapter();

        $expectedCondition = ' ORDER BY base_template_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

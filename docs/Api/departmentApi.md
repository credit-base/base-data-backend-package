# 科室接口文档

---

## 简介

本文档主要描述科室模块相关接口请求及响应说明。

## 目录

* [参考文档](#参考文档)
    * [项目字典](#项目字典)
    * [控件规范](#控件规范)
    * [错误规范](#错误规范)
    * [接口错误返回说明](#接口错误返回说明) 
* [参数说明](#参数说明)
* [接口示例](#接口示例)
    * [获取单条数据](#获取单条数据)
    * [获取多条数据](#获取多条数据)
    * [根据检索条件查询数据](#根据检索条件查询数据)
    * [新增](#新增)
    * [编辑](#编辑)
    * [接口返回示例](#接口返回示例)
        * [单条示例](#单条示例)
        * [多条示例](#多条示例)

## <a name="参考文档">参考文档</a>

* <a name="项目字典">项目字典</a>
    * [通用字典](./docs/Dictionary/common.md "通用字典")
    * [科室字典](./docs/Dictionary/department.md "科室字典")
* <a name="控件规范">控件规范</a>
    * [通用控件规范](./docs/WidgetRule/common.md "通用控件规范")
    * [科室控件规范](./docs/WidgetRule/department.md "科室控件规范")
* <a name="错误规范">错误规范</a>
    * [通用错误规范](./docs/ErrorRule/common.md "通用错误规范")
    * [科室错误规范](./docs/ErrorRule/department.md "科室错误规范")
    * 错误映射
    ```
    100=>array(
        'userGroupId'=>所属委办局为空
    ),
    101=>array(
        'name'=>科室名称格式不正确
        'userGroupId'=>所属委办局格式不正确
    ),
    103=>array(
        'departmentName'=>科室名称已经存在
    ),
    ```
* <a name="接口错误返回说明">接口错误返回说明</a>
    * [接口错误返回说明](./docs/Api/errorApi.md "接口错误返回说明") 

## <a name="参数说明">参数说明</a>

<table>
  <tr>
    <th><b>英文名称</b></th>
    <th><b>类型</b></th>
    <th><b>请求参数是否必填</b></th>
    <th><b>示例</b></th>
    <th><b>描述</b></th>
  </tr> 
  <tr>
    <td>name</td>
    <td>string</td>
    <td>是</td>
    <td>财金科</td>
    <td>科室名称</td>
  </tr>
  <tr>
    <td>userGroup</td>
    <td>int</td>
    <td>是</td>
    <td>1</td>
    <td>所属委办局</td>
  </tr>
</table>

## <a name="接口示例">接口示例</a>

### 获取数据支持include、fields请求参数

```
1、include请求参数
    1.1 include=userGroup
2、fields[TYPE]请求参数
    2.1 fields[departments]
    2.2 fields[userGroups]
3、page请求参数
    3.1 page[number]=1 | 当前页
    3.2 page[size]=20 | 获取每页的数量
```

示例

```php
$response = $client->request('GET', 'departments/1?fields[departments]=name,updateTime',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取单条数据">获取单条数据示例</a>

路由

```
通过GET传参
/departments/{id:\d+}
```

示例

```php
$response = $client->request('GET', 'departments/1',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="获取多条数据">获取多条数据示例</a>

路由

```
通过GET传参
/departments/{ids:\d+,[\d,]+}
```

示例

```php
$response = $client->request('GET', 'departments/1,2,3',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="根据检索条件查询数据">根据检索条件查询数据示例</a>

路由

```
通过GET传参
/departments

1、检索条件
    1.1 filter[name] | 根据 科室名称 搜索
    1.2 filter[userGroup] | 根据 所属委办局 搜索
2、排序
    2.1 sort=-updateTime | -updateTime 根据更新时间倒序 | updateTime 根据更新时间正序
```

示例

```php
$response = $client->request('GET', 'departments?filter[userGroup]=1&sort=-updateTime',['headers'=>['Content-' => 'application/vnd.api+json']]);
```

### <a name="新增">新增示例</a>

路由

```
通过POST传参
/departments
```

示例

```php
$data = array("data"=>array(
                    "type"=>"departments",
                    "attributes"=>array(
                        "name"=>'财金科'
                    ),
                    "relationships"=>array(
                        "userGroup"=>array(
                            "data"=>array(
                                array("type"=>"userGroups","id"=>1)
                            )
                        )
                    )
                )
        );
$response = $client->request(
                'POST',
                'departments',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="编辑">编辑示例</a>

路由

```
通过PATCH传参
/departments/{id:\d+}
```

示例

```php
$data = array("data"=>array(
                    "type"=>"departments",
                    "attributes"=>array(
                        "name"=>'财金科'
                    )
                )
        );
$response = $client->request(
                'PATCH',
                'departments/1',
                [
                    'headers'=>['Content-Type' => 'application/vnd.api+json'],
                    'json' => $data
                ]
            );
```

### <a name="接口返回示例">接口返回示例</a>

#### <a name="单条示例">单条示例</a>

    {
        "meta": [],
        "data": {
            "type": "departments",
            "id": "4",
            "attributes": {
                "name": "财金科",
                "status": 0,
                "createTime": 1619336405,
                "updateTime": 1619336405,
                "statusTime": 0
            },
            "relationships": {
                "userGroup": {
                    "data": {
                        "type": "userGroups",
                        "id": "3"
                    }
                }
            },
            "links": {
                "self": "127.0.0.1:8080/departments/4"
            }
        },
        "included": [
            {
                "type": "userGroups",
                "id": "3",
                "attributes": {
                    "name": "萍乡市文明办",
                    "shortName": "文明办",
                    "status": 0,
                    "createTime": 1516168970,
                    "updateTime": 1516168970,
                    "statusTime": 0
                }
            }
        ]
    }

#### <a name="多条示例">多条示例</a>

    {
        "meta": {
            "count": 4,
            "links": {
                "first": null,
                "last": null,
                "prev": null,
                "next": null
            }
        },
        "links": {
            "first": null,
            "last": null,
            "prev": null,
            "next": null
        },
        "data": [
            {
                "type": "departments",
                "id": "1",
                "attributes": {
                    "name": "财金科",
                    "status": 0,
                    "createTime": 1618283949,
                    "updateTime": 1618283949,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8080/departments/1"
                }
            },
            {
                "type": "departments",
                "id": "2",
                "attributes": {
                    "name": "财金科",
                    "status": 0,
                    "createTime": 1618283970,
                    "updateTime": 1618283970,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "2"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8080/departments/2"
                }
            },
            {
                "type": "departments",
                "id": "3",
                "attributes": {
                    "name": "财金科",
                    "status": 0,
                    "createTime": 1619316013,
                    "updateTime": 1619316013,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "1"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8080/departments/3"
                }
            },
            {
                "type": "departments",
                "id": "4",
                "attributes": {
                    "name": "财金科",
                    "status": 0,
                    "createTime": 1619336405,
                    "updateTime": 1619336405,
                    "statusTime": 0
                },
                "relationships": {
                    "userGroup": {
                        "data": {
                            "type": "userGroups",
                            "id": "3"
                        }
                    }
                },
                "links": {
                    "self": "127.0.0.1:8080/departments/4"
                }
            }
        ],
        "included": [
            {
                "type": "userGroups",
                "id": "1",
                "attributes": {
                    "name": "萍乡市发展和改革委员会",
                    "shortName": "发改委",
                    "status": 0,
                    "createTime": 1516168970,
                    "updateTime": 1516168970,
                    "statusTime": 0
                }
            },
            {
                "type": "userGroups",
                "id": "2",
                "attributes": {
                    "name": "共青团萍乡市委",
                    "shortName": "团市委",
                    "status": 0,
                    "createTime": 1516168970,
                    "updateTime": 1516168970,
                    "statusTime": 0
                }
            },
            {
                "type": "userGroups",
                "id": "3",
                "attributes": {
                    "name": "萍乡市文明办",
                    "shortName": "文明办",
                    "status": 0,
                    "createTime": 1516168970,
                    "updateTime": 1516168970,
                    "statusTime": 0
                }
            }
        ]
    }
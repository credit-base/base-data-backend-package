<?php
namespace BaseData\Template\Utils;

use BaseData\Template\Model\GbTemplate;

class GbTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateGbTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : GbTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $gbTemplate = new GbTemplate($id);
        $gbTemplate->setId($id);

        self::generateName($gbTemplate, $faker, $value);
        self::generateIdentify($gbTemplate, $faker, $value);
        self::generateSubjectCategory($gbTemplate, $faker, $value);
        self::generateDimension($gbTemplate, $faker, $value);
        self::generateExchangeFrequency($gbTemplate, $faker, $value);
        self::generateInfoClassify($gbTemplate, $faker, $value);
        self::generateInfoCategory($gbTemplate, $faker, $value);
        self::generateDescription($gbTemplate, $faker, $value);
        self::generateItems($gbTemplate, $faker, $value);
        self::generateStatus($gbTemplate, $faker, $value);
        
        $gbTemplate->setCreateTime($faker->unixTime());
        $gbTemplate->setUpdateTime($faker->unixTime());
        $gbTemplate->setStatusTime($faker->unixTime());

        return $gbTemplate;
    }
}

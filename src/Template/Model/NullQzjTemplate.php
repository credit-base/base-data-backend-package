<?php
namespace BaseData\Template\Model;

use Marmot\Interfaces\INull;

use BaseData\Common\Model\NullOperateTrait;

class NullQzjTemplate extends QzjTemplate implements INull
{
    use NullOperateTrait;
    
    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function isTemplateExist() : bool
    {
        return $this->resourceNotExist();
    }
}

<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的原始数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_ys_items_data');
        $this->clear('pcore_ys_search_data');
    }

    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_ys_items_data' => $this->ysSearchData()['pcore_ys_items_data'],
            'pcore_ys_search_data' => $this->ysSearchData()['pcore_ys_search_data']
        ]);
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchYsSearchDataList()
    {
        $repository = new YsSearchDataRepository();

        list($ysSearchDataList, $count) = $repository->filter([]);
        
        unset($count);

        return $ysSearchDataList;
    }

    /**
     * @Then: 我可以看到资源目录数据的全部信息
     */
    public function testViewYsSearchDataList()
    {
        $setYsSearchDataList = $this->ysSearchData()['pcore_ys_search_data'];

        foreach ($setYsSearchDataList as $key => $setYsSearchData) {
            unset($setYsSearchData);
            $setYsSearchDataList[$key]['ys_search_data_id'] = $key +1;
        }

        $ysSearchDataList = $this->fetchYsSearchDataList();

        $translator = new YsSearchDataDbTranslator();
        foreach ($ysSearchDataList as $ysSearchData) {
            $ysSearchDataArray[] = $translator->objectToArray($ysSearchData);
        }

        $this->assertEquals($ysSearchDataArray, $setYsSearchDataList);
    }
}

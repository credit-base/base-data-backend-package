<?php
namespace BaseData\Template\Utils;

trait QzjTemplateUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $qzjTemplate
    ) {
        $this->assertEquals($expectedArray['qzj_template_id'], $qzjTemplate->getId());
        $this->assertEquals($expectedArray['name'], $qzjTemplate->getName());
        $this->assertEquals($expectedArray['identify'], $qzjTemplate->getIdentify());
        $this->assertEquals($expectedArray['category'], $qzjTemplate->getCategory());

        $subjectCategory = is_string($expectedArray['subject_category']) ?
        json_decode($expectedArray['subject_category'], true) :
        $expectedArray['subject_category'];
        $this->assertEquals($subjectCategory, $qzjTemplate->getSubjectCategory());

        $this->assertEquals($expectedArray['dimension'], $qzjTemplate->getDimension());
        $this->assertEquals($expectedArray['exchange_frequency'], $qzjTemplate->getExchangeFrequency());
        $this->assertEquals($expectedArray['info_classify'], $qzjTemplate->getInfoClassify());
        $this->assertEquals($expectedArray['info_category'], $qzjTemplate->getInfoCategory());
        $this->assertEquals($expectedArray['description'], $qzjTemplate->getDescription());

        $items = is_string($expectedArray['items']) ?
        json_decode($expectedArray['items'], true) :
        $expectedArray['items'];
        $this->assertEquals($items, $qzjTemplate->getItems());
        $this->assertEquals($expectedArray['source_unit_id'], $qzjTemplate->getSourceUnit()->getId());

        $this->assertEquals($expectedArray['status'], $qzjTemplate->getStatus());
        $this->assertEquals($expectedArray['create_time'], $qzjTemplate->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $qzjTemplate->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $qzjTemplate->getStatusTime());
    }
}

<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\View\GbSearchDataView;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use BaseData\ResourceCatalogData\Command\GbSearchData\ConfirmGbSearchDataCommand;
use BaseData\ResourceCatalogData\Command\GbSearchData\DeleteGbSearchDataCommand;
use BaseData\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand;
use BaseData\ResourceCatalogData\CommandHandler\GbSearchData\GbSearchDataCommandHandlerFactory;

class GbSearchDataStatusController extends Controller
{
    use JsonApiTrait;

    private $repository;
    
    private $commandBus;

    public function __construct()
    {
        parent::__construct();

        $this->repository = new GbSearchDataRepository();
        $this->commandBus = new CommandBus(new GbSearchDataCommandHandlerFactory());
    }

    public function __destruct()
    {
        parent::__destruct();

        unset($this->repository);
        unset($this->commandBus);
    }

    protected function getRepository() : IGbSearchDataAdapter
    {
        return $this->repository;
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }
    
    /**
     * 对应路由 /gbSearchData/{id:\d+}/confirm
     * 确认, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function confirm(int $id)
    {
        if (!empty($id)) {
            $command = new ConfirmGbSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $gbSearchData  = $this->getRepository()->fetchOne($id);
                if ($gbSearchData instanceof GbSearchData) {
                    $this->render(new GbSearchDataView($gbSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /gbSearchData/{id:\d+}/disable
     * 屏蔽, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function disable(int $id)
    {
        if (!empty($id)) {
            $command = new DisableGbSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $gbSearchData  = $this->getRepository()->fetchOne($id);
                if ($gbSearchData instanceof GbSearchData) {
                    $this->render(new GbSearchDataView($gbSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }

    /**
     * 对应路由 /gbSearchData/{id:\d+}/delete
     * 封存, 通过PATCH传参
     * @param int id 本级资源目录数据id
     * @return jsonApi
     */
    public function delete(int $id)
    {
        if (!empty($id)) {
            $command = new DeleteGbSearchDataCommand($id);

            if ($this->getCommandBus()->send($command)) {
                $gbSearchData  = $this->getRepository()->fetchOne($id);
                if ($gbSearchData instanceof GbSearchData) {
                    $this->render(new GbSearchDataView($gbSearchData));
                    return true;
                }
            }
        }
        $this->displayError();
        return false;
    }
}

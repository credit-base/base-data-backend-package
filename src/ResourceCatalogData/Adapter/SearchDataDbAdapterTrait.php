<?php
namespace BaseData\ResourceCatalogData\Adapter;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

trait SearchDataDbAdapterTrait
{
    abstract protected function getDbTranslator() : ITranslator;

    abstract protected function getTemplateRepository();

    abstract protected function getItemsDataRepository();

    protected function fetchTemplate($searchData)
    {
        return is_array($searchData) ?
        $this->fetchTemplateByList($searchData) :
        $this->fetchTemplateByObject($searchData);
    }

    protected function fetchTemplateByObject(ISearchDataAble $searchData)
    {
        $templateId = $searchData->getTemplate()->getId();
        $template = $this->getTemplateRepository()->fetchOne($templateId);
        $searchData->setTemplate($template);
    }

    protected function fetchTemplateByList(array $searchDataList)
    {
        $templateIds = array();
        foreach ($searchDataList as $searchData) {
            $templateIds[] = $searchData->getTemplate()->getId();
        }

        $templateList = $this->getTemplateRepository()->fetchList($templateIds);
        if (!empty($templateList)) {
            foreach ($searchDataList as $searchData) {
                $templateId = $searchData->getTemplate()->getId();
                if (isset($templateList[$templateId])
                    && ($templateList[$templateId]->getId() == $templateId)) {
                    $searchData->setTemplate($templateList[$templateId]);
                }
            }
        }
    }

    protected function fetchItemsData($searchData)
    {
        return is_array($searchData) ?
        $this->fetchItemsDataByList($searchData) :
        $this->fetchItemsDataByObject($searchData);
    }

    protected function fetchItemsDataByObject(ISearchDataAble $searchData)
    {
        $itemsId = $searchData->getItemsData()->getId();
        $items = $this->getItemsDataRepository()->fetchOne($itemsId);
        $searchData->setItemsData($items);
    }

    protected function fetchItemsDataByList(array $searchDataList)
    {
        $itemsIds = array();
        foreach ($searchDataList as $searchData) {
            $itemsIds[] = $searchData->getItemsData()->getId();
        }

        $itemsList = $this->getItemsDataRepository()->fetchList($itemsIds);
        if (!empty($itemsList)) {
            foreach ($searchDataList as $searchData) {
                $itemsId = $searchData->getItemsData()->getId();
                if (isset($itemsList[$itemsId])
                    && ($itemsList[$itemsId]->getId() == $itemsId)) {
                    $searchData->setItemsData($itemsList[$itemsId]);
                }
            }
        }
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function searchDataFormatFilter(array $filter, ISearchDataAble $searchData) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            if (isset($filter['infoClassify'])) {
                $searchData->setInfoClassify($filter['infoClassify']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('infoClassify'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoCategory'])) {
                $searchData->setInfoCategory($filter['infoCategory']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('infoCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['subjectCategory'])) {
                $searchData->setSubjectCategory($filter['subjectCategory']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('subjectCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['dimension'])) {
                $searchData->setDimension($filter['dimension']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('dimension'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $info = $this->getDbTranslator()->objectToArray($searchData, array('status'));
                $condition .= $conjection.key($info).' IN ('.$filter['status'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['expirationDate'])) {
                $searchData->setExpirationDate($filter['expirationDate']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('expirationDate'));
                $condition .= $conjection.key($info).' > '.current($info);
            }
            if (isset($filter['sourceUnit']) && !empty($filter['sourceUnit'])) {
                $searchData->getSourceUnit()->setId($filter['sourceUnit']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('sourceUnit'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['name'])) {
                $condition .= $conjection.'name LIKE \'%'.$filter['name'].'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['identify'])) {
                $condition .= $conjection.'identify = \''.$filter['identify'].'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['template']) && !empty($filter['template'])) {
                $searchData->getTemplate()->setId($filter['template']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('template'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['hash'])) {
                $searchData->setHash($filter['hash']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('hash'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['task'])) {
                $searchData->getTask()->setId($filter['task']);
                $info = $this->getDbTranslator()->objectToArray($searchData, array('task'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }
}

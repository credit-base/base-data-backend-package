<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData;

use BaseData\ResourceCatalogData\Model\YsItemsData;

interface IYsItemsDataAdapter
{
    public function fetchOne($id) : YsItemsData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(YsItemsData $ysItemsData) : bool;
}

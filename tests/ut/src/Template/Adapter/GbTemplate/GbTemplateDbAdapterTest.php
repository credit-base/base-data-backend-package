<?php
namespace BaseData\Template\Adapter\GbTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\NullGbTemplate;
use BaseData\Template\Translator\GbTemplateDbTranslator;
use BaseData\Template\Adapter\GbTemplate\Query\GbTemplateRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class GbTemplateDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(GbTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IGbTemplateAdapter
     */
    public function testImplementsIGbTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 GbTemplateDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockGbTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Translator\GbTemplateDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 GbTemplateRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockGbTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\GbTemplate\Query\GbTemplateRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockGbTemplateDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedGbTemplate = new GbTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedGbTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedGbTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedGbTemplate = new GbTemplate();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedGbTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedGbTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedGbTemplate = new GbTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedGbTemplate);

        //验证
        $gbTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedGbTemplate, $gbTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $gbTemplateOne = new GbTemplate(1);
        $gbTemplateTwo = new GbTemplate(2);

        $ids = [1, 2];

        $expectedGbTemplateList = [];
        $expectedGbTemplateList[$gbTemplateOne->getId()] = $gbTemplateOne;
        $expectedGbTemplateList[$gbTemplateTwo->getId()] = $gbTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedGbTemplateList);

        //验证
        $gbTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedGbTemplateList, $gbTemplateList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 'name',
        );
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterIdentify()
    {
        $filter = array(
            'identify' => 'identify',
        );
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = 'identify = \''.$filter['identify'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterDimension()
    {
        $filter = array(
            'dimension' => GbTemplate::DIMENSION['SHGK']
        );
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = 'dimension = '.$filter['dimension'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoClassify()
    {
        $filter = array(
            'infoClassify' => 1
        );
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = 'info_classify = '.$filter['infoClassify'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoCategory()
    {
        $filter = array(
            'infoCategory' => 1
        );
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = 'info_category = '.$filter['infoCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = ' ORDER BY gb_template_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockGbTemplateDbAdapter();

        $expectedCondition = ' ORDER BY gb_template_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

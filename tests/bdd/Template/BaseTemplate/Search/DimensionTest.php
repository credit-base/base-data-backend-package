<?php
namespace BaseData\Template\BaseTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Translator\BaseTemplateDbTranslator;

/**
 * @Feature: 我是超级管理员,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 查看公开范围为社会公开的数据
 */
class DimensionTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBaseTemplateList()
    {
        $repository = new BaseTemplateRepository();
        $filter['dimension'] = Template::DIMENSION['SHGK'];

        list($baseTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $baseTemplateList;
    }

    /**
     * @Then: 我可以看到公开范围为社会公开的所有资源目录
     */
    public function testViewBaseTemplateList()
    {
        $dimensionBaseTemplateList = $this->baseTemplate();

        foreach ($dimensionBaseTemplateList as $key => $dimensionTemplate) {
            $dimensionBaseTemplateList[$key]['base_template_id'] = $key +1;
            $dimensionBaseTemplateList[$key]['subject_category'] = json_decode(
                $dimensionTemplate['subject_category'],
                true
            );
            $dimensionBaseTemplateList[$key]['items'] = json_decode($dimensionTemplate['items'], true);
        }

        $baseTemplateList = $this->fetchBaseTemplateList();
        $translator = new BaseTemplateDbTranslator();
        foreach ($baseTemplateList as $baseTemplate) {
            $baseTemplateArray[] = $translator->objectToArray($baseTemplate);
        }

        $this->assertEquals($baseTemplateArray, $dimensionBaseTemplateList);

        foreach ($baseTemplateArray as $baseTemplate) {
            $this->assertEquals(Template::DIMENSION['SHGK'], $baseTemplate['dimension']);
        }
    }
}

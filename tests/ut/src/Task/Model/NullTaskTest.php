<?php
namespace BaseData\Task\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullTaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = $this->getMockBuilder(NullTask::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->task);
    }

    public function testExtendsTask()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Model\Task',
            $this->task
        );
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->task
        );
    }

    public function testAdd()
    {
        $this->task->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->task->add());
    }

    public function testEdit()
    {
        $this->task->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->task->edit());
    }

    public function testExecute()
    {
        $this->task->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->task->execute());
    }

    public function testScheduled()
    {
        $this->task->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->task->scheduled());
    }

    public function testExecuteAction()
    {
        $task = new MockNullTask();

        $this->assertFalse($task->executeAction());
    }

    public function testResourceNotExist()
    {
        $task = new MockNullTask();
        $result = $task->resourceNotExist();
        
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}

<?php
namespace BaseData\Template\Adapter\QzjTemplate;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Model\NullQzjTemplate;
use BaseData\Template\Translator\QzjTemplateDbTranslator;
use BaseData\Template\Adapter\QzjTemplate\Query\QzjTemplateRowCacheQuery;

class QzjTemplateDbAdapter implements IQzjTemplateAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new QzjTemplateDbTranslator();
        $this->rowCacheQuery = new QzjTemplateRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : QzjTemplateDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : QzjTemplateRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullQzjTemplate::getInstance();
    }

    public function add(QzjTemplate $qzjTemplate) : bool
    {
        return $this->addAction($qzjTemplate);
    }

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool
    {
        return $this->editAction($qzjTemplate, $keys);
    }

    public function fetchOne($id) : QzjTemplate
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    /**
     * @todo
     * @SuppressWarnings(PHPMD)
     */
    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $qzjTemplate = new QzjTemplate();

            if (isset($filter['name'])) {
                $qzjTemplate->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['identify'])) {
                $qzjTemplate->setIdentify($filter['identify']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('identify'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['category'])) {
                $qzjTemplate->setCategory($filter['category']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('category'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['dimension'])) {
                $qzjTemplate->setDimension($filter['dimension']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('dimension'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoClassify'])) {
                $qzjTemplate->setInfoClassify($filter['infoClassify']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('infoClassify'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoCategory'])) {
                $qzjTemplate->setInfoCategory($filter['infoCategory']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('infoCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceUnit'])) {
                $qzjTemplate->getSourceUnit()->setId($filter['sourceUnit']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('sourceUnit'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['id'])) {
                $qzjTemplate->setId($filter['id']);
                $info = $this->getDbTranslator()->objectToArray($qzjTemplate, array('id'));
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new QzjTemplate(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray(new QzjTemplate(), array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

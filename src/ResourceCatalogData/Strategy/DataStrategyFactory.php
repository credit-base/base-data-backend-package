<?php
namespace BaseData\ResourceCatalogData\Strategy;

use BaseData\Template\Model\Template;

class DataStrategyFactory
{
    const MAPS = array(
        Template::TYPE['ZFX']=>'\BaseData\ResourceCatalogData\Strategy\CharacterStrategy',
        Template::TYPE['RQX']=>'\BaseData\ResourceCatalogData\Strategy\DateStrategy',
        Template::TYPE['ZSX']=>'\BaseData\ResourceCatalogData\Strategy\IntegerStrategy',
        Template::TYPE['FDX']=>'\BaseData\ResourceCatalogData\Strategy\FloatStrategy',
        Template::TYPE['MJX']=>'\BaseData\ResourceCatalogData\Strategy\EnumerationStrategy',
        Template::TYPE['JHX']=>'\BaseData\ResourceCatalogData\Strategy\CollectionStrategy'
    );

    public function getStrategy(int $type) : IDataStrategy
    {
        $strategy = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';
        return class_exists($strategy) ? new $strategy : new NullDataStrategy();
    }
}

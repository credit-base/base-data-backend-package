<?php
namespace BaseData\Template\BjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\BjTemplate;

/**
 * 测试本级资源目录接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_bj_template');
    }

    /**
     * 存在多条本级资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_gb_template' => $this->gbTemplate()
            ]
        );
    }

    public function testViewBjTemplate()
    {
        $setBjTemplateList = $this->bjTemplate();
        $setGbTemplateList = $this->gbTemplate();
        foreach ($setGbTemplateList as $setGbTemplate) {
            $setGbTemplateList[$setGbTemplate['gb_template_id']] = $setGbTemplate;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'bjTemplates/1,2?include=gbTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $bjTemplate) {
            $this->bjTemplateCompareData($setBjTemplateList[$key], $bjTemplate);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'gbTemplates') {
                $this->gbTemplateCompareData($setGbTemplateList[$value['id']], $value);
            }
        }
    }
}

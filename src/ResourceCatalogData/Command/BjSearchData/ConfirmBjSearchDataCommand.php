<?php
namespace BaseData\ResourceCatalogData\Command\BjSearchData;

use Marmot\Interfaces\ICommand;

class ConfirmBjSearchDataCommand implements ICommand
{
    public $id;

    public function __construct(
        int $id
    ) {
        $this->id = $id;
    }
}

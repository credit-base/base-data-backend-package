<?php
namespace BaseData\Task\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use BaseData\Task\Adapter\Task\ITaskAdapter;

/**
 * BaseData\Task\Model\Task.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class TaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new MockTask();
    }

    public function tearDown()
    {
        unset($this->task);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->task);
    }

    /**
     * 测试构造函数
     */
    public function testConstructor()
    {
        $this->assertEquals(0, $this->task->getId());

        $this->assertEquals(0, $this->task->getParentId());

        $this->assertEquals(Task::TYPE['UNDEFINED'], $this->task->getType());

        $this->assertEquals([], $this->task->getData());

        $this->assertEquals(0, $this->task->getNextScheduleTime());

        $this->assertEquals(0, $this->task->getNice());

        $this->assertEquals(Task::STATUS['PENDING'], $this->task->getStatus());

        //测试初始化创建时间
        $this->assertEquals(Core::$container->get('time'), $this->task->getCreateTime());

        //测试初始化更新时间
        $this->assertEquals(Core::$container->get('time'), $this->task->getUpdateTime());

        //测试初始化状态更新时间
        $this->assertEquals(0, $this->task->getStatusTime());

        $this->assertInstanceof(
            'BaseData\Task\Adapter\Task\ITaskAdapter',
            $this->task->getRepository()
        );

        $this->assertEquals('BaseData\Task\Model\MockTask', $this->task->getIdentify());
        //测试初始化pid
        $this->assertEquals(0, $this->task->getPid());
    }

    public function testSetId()
    {
        $this->task->setId(1);
        $this->assertEquals(1, $this->task->getId());
    }

    //pid -- 开始
    /**
     * 设置 Task setPid() 正确的传参类型,期望传值正确
     */
    public function testSetPidCorrectType()
    {
        $pid = 1;
        $this->task->setPid($pid);
        $this->assertEquals($pid, $this->task->getPid());
    }
    /**
     * 设置 Task setPid() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPidWrongType()
    {
        $this->task->setPid(array(1, 2, 3));
    }
    //pid -- 结束

    //parentId -- 开始
    /**
     * 设置 Task setParentId() 正确的传参类型,期望传值正确
     */
    public function testSetParentIdCorrectType()
    {
        $parentId = 1;
        $this->task->setParentId($parentId);
        $this->assertEquals($parentId, $this->task->getParentId());
    }
    /**
     * 设置 Task setParentId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetParentIdWrongType()
    {
        $this->task->setParentId(array(1, 2, 3));
    }
    //parentId -- 结束

    //identify -- 开始
    /**
     * 设置 Task setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->task->setIdentify('identify');
        $this->assertEquals('identify', $this->task->getIdentify());
    }
    /**
     * 设置 Task setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->task->setIdentify(array(1, 2, 3));
    }
    //identify -- 结束

    //data -- 开始
    /**
     * 设置 Task setData() 正确的传参类型,期望传值正确
     */
    public function testSetDataCorrectType()
    {
        $data = [];

        $this->task->setData($data);
        $this->assertEquals($data, $this->task->getData());
    }
    /**
     * 设置 Task setData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDataWrongType()
    {
        $this->task->setData('data');
    }
    //data -- 结束

    //nextScheduleTime -- 开始
    /**
     * 设置 Task setNextScheduleTime() 正确的传参类型,期望传值正确
     */
    public function testSetNextScheduleTimeCorrectType()
    {
        $nextScheduleTime = 1;
        $this->task->setNextScheduleTime($nextScheduleTime);
        $this->assertEquals($nextScheduleTime, $this->task->getNextScheduleTime());
    }
    /**
     * 设置 Task setNextScheduleTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNextScheduleTimeWrongType()
    {
        $this->task->setNextScheduleTime('nextScheduleTime');
    }
    //nextScheduleTime -- 结束

    //nice -- 开始
    /**
     * 设置 Task setNice() 正确的传参类型,期望传值正确
     */
    public function testSetNiceCorrectType()
    {
        $nice = 1;
        $this->task->setNice($nice);
        $this->assertEquals($nice, $this->task->getNice());
    }
    /**
     * 设置 Task setNice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNiceWrongType()
    {
        $this->task->setNice('nice');
    }
    //nice -- 结束

    //status -- 开始
    /**
     * 循环测试 Task setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->task->setStatus($actual);
        $this->assertEquals($expected, $this->task->getStatus());
    }
    /**
     * 循环测试 Task setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(Task::STATUS['UNDEFINED'],Task::STATUS['UNDEFINED']),
            array(Task::STATUS['FAIL'],Task::STATUS['FAIL']),
            array(Task::STATUS['PENDING'],Task::STATUS['PENDING']),
            array(Task::STATUS['SLEEP'],Task::STATUS['SLEEP']),
            array(Task::STATUS['EXECUTING'],Task::STATUS['EXECUTING']),
            array(Task::STATUS['SUCCESS'],Task::STATUS['SUCCESS']),
            array(999,Task::STATUS['UNDEFINED']),
        );
    }
    /**
     * 设置 Task setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->task->setStatus('string');
    }
    //status -- 结束

    //type -- 开始
    /**
     * 循环测试 Task setType() 是否符合预定范围
     *
     * @dataProvider typeProvider
     */
    public function testSetType($actual, $expected)
    {
        $this->task->setType($actual);
        $this->assertEquals($expected, $this->task->getType());
    }
    /**
     * 循环测试 Task setType() 数据构建器
     */
    public function typeProvider()
    {
        return array(
            array(Task::TYPE['UNDEFINED'],Task::TYPE['UNDEFINED']),
            array(Task::TYPE['ONCE'],Task::TYPE['ONCE']),
            array(Task::TYPE['PERIOD'],Task::TYPE['PERIOD']),
            array(999,Task::TYPE['UNDEFINED']),
        );
    }
    /**
     * 设置 Task setType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTypeWrongType()
    {
        $this->task->setType('string');
    }
    //type -- 结束

    public function testBeforeExecute()
    {
        $this->assertTrue($this->task->beforeExecute());
    }

    public function testAfterExecute()
    {
        $this->assertTrue($this->task->afterExecute());
    }

    public function testEdit()
    {
        $this->assertFalse($this->task->edit());
    }

    private function statusSuccessTestCase(int $status)
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->task->setStatus($status);
        $this->task->setUpdateTime(time());

        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->edit(
            Argument::exact($this->task),
            Argument::exact(array(
                'status',
                'statusTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->task->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    private function statusFailureTestCase(int $status)
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->task->setStatus($status);
        $this->task->setUpdateTime(time());

        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->edit(
            Argument::exact($this->task),
            Argument::exact(array(
                'status',
                'statusTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(false);

        $this->task->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    //fail -- 开始
    public function testFailSuccess()
    {
        $this->statusSuccessTestCase(Task::STATUS['FAIL']);
        $result = $this->task->fail();
        $this->assertTrue($result);
    }

    public function testFailFailure()
    {
        $this->statusFailureTestCase(Task::STATUS['FAIL']);
        $result = $this->task->fail();
        $this->assertFalse($result);
    }
    //fail -- 结束

    //unusual -- 开始
    public function testUsualSuccess()
    {
        $this->statusSuccessTestCase(Task::STATUS['UNUSUAL']);
        $result = $this->task->unusual();
        $this->assertTrue($result);
    }

    public function testUsualFailure()
    {
        $this->statusFailureTestCase(Task::STATUS['UNUSUAL']);
        $result = $this->task->unusual();
        $this->assertFalse($result);
    }
    //unusual -- 结束

    //add -- 开始
    public function testAddSucessEmptySubtasks()
    {
        //初始化
        $subTasks = [];
        $task = $this->getMockBuilder(MockTask::class)
                           ->setMethods([
                            'getRepository',
                            'addSubTasks'
                            ])
                           ->getMock();

        //预言 INewsAdapter
        $repository = $this->prophesize(ITaskAdapter::class);
        $repository->add(Argument::exact($task))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $task->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $task->expects($this->exactly(0))
             ->method('addSubTasks');

        //验证
        $result = $task->add($subTasks);
        $this->assertTrue($result);
    }

    public function testAddSucessWithSubtasks()
    {
        //初始化
        $subTasks = ['subTasks'];
        $task = $this->getMockBuilder(MockTask::class)
                           ->setMethods([
                            'getRepository',
                            'addSubTasks'
                            ])
                           ->getMock();

        //预言 INewsAdapter
        $repository = $this->prophesize(ITaskAdapter::class);
        $repository->add(Argument::exact($task))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $task->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $task->expects($this->exactly(1))
             ->method('addSubTasks')
             ->with($subTasks);

        //验证
        $result = $task->add($subTasks);
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        //初始化
        $subTasks = [];
        $task = $this->getMockBuilder(MockTask::class)
                           ->setMethods([
                            'getRepository',
                            'addSubTasks'
                            ])
                           ->getMock();

        //预言 INewsAdapter
        $repository = $this->prophesize(ITaskAdapter::class);
        $repository->add(Argument::exact($task))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        //绑定
        $task->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $task->expects($this->exactly(0))
             ->method('addSubTasks');

        //验证
        $result = $task->add($subTasks);
        $this->assertFalse($result);
    }
    //add -- 结束

    //addSubTasks -- 开始
    public function testAddSubTasksFailEmptySubTasks()
    {
        $result = $this->task->addSubTasks([]);
        $this->assertFalse($result);
    }

    public function testAddSubTasksFailSubTaskAddFail()
    {
        $parentId = 1;
        $this->task->setId($parentId);

        $subTaskOne = $this->prophesize(Task::class);
        $subTaskOne->setParentId(
            Argument::exact($parentId)
        )->shouldBeCalledTimes(1);
        $subTaskOne->setStatus(
            Argument::exact(Task::STATUS['SLEEP'])
        )->shouldBeCalledTimes(1);
        $subTaskOne->add()->shouldBeCalledTimes(1)->willReturn(false);

        $subTaskTwo = $this->prophesize(Task::class);
        $subTaskTwo->setParentId(
            Argument::exact($parentId)
        )->shouldBeCalledTimes(0);
        $subTaskTwo->setStatus(
            Argument::exact(Task::STATUS['SLEEP'])
        )->shouldBeCalledTimes(0);
        $subTaskTwo->add()->shouldBeCalledTimes(0)->willReturn(false);

        $result = $this->task->addSubTasks([
            $subTaskOne->reveal(),
            $subTaskTwo->reveal()
        ]);

        $this->assertFalse($result);
    }

    public function testAddSubTasksSuccess()
    {
        $parentId = 1;
        $this->task->setId($parentId);

        $subTaskOne = $this->prophesize(Task::class);
        $subTaskOne->setParentId(
            Argument::exact($parentId)
        )->shouldBeCalledTimes(1);
        $subTaskOne->setStatus(
            Argument::exact(Task::STATUS['SLEEP'])
        )->shouldBeCalledTimes(1);
        $subTaskOne->add()->shouldBeCalledTimes(1)->willReturn(true);

        $subTaskTwo = $this->prophesize(Task::class);
        $subTaskTwo->setParentId(
            Argument::exact($parentId)
        )->shouldBeCalledTimes(1);
        $subTaskTwo->setStatus(
            Argument::exact(Task::STATUS['SLEEP'])
        )->shouldBeCalledTimes(1);
        $subTaskTwo->add()->shouldBeCalledTimes(1)->willReturn(true);

        $result = $this->task->addSubTasks([
            $subTaskOne->reveal(),
            $subTaskTwo->reveal()
        ]);

        $this->assertTrue($result);
    }
    //addSubTasks -- 结束

    //updateProcessId -- 开始
    public function testUpdateProcessId()
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->edit(
            Argument::exact($this->task),
            Argument::exact(array(
                'pid'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->task->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->task->updateProcessId();
        
        $this->assertTrue($result);
    }
    //updateProcessId -- 结束

    //notifySubTasks -- 开始
    public function testNotifySubTask()
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->notifySubTasks(
            Argument::exact($this->task)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->task->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->task->notifySubTasks();
        
        $this->assertTrue($result);
    }
    //notifySubTasks -- 结束

    //execute -- 开始
    public function testExecuteSucess()
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods([
                'beforeExecute',
                'updateProcessId',
                'executeAction',
                'notifySubTasks',
                'afterExecute'
            ])
            ->getMock();

        $this->task->expects($this->once())
                   ->method('beforeExecute');

        $this->task->expects($this->once())
                   ->method('updateProcessId')
                   ->willReturn(true);

        $this->task->expects($this->once())
                   ->method('executeAction')
                   ->willReturn(true);

        $this->task->expects($this->once())
                   ->method('afterExecute');

        $this->task->expects($this->once())
                   ->method('notifySubTasks');

        $result = $this->task->execute();
        
        $this->assertTrue($result);
    }

    public function testExecuteUpdateProcessIdFail()
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods([
                'beforeExecute',
                'updateProcessId',
                'executeAction',
                'notifySubTasks',
                'afterExecute'
            ])
            ->getMock();

        $this->task->expects($this->once())
                   ->method('beforeExecute');

        $this->task->expects($this->once())
                   ->method('updateProcessId')
                   ->willReturn(false);

        $this->task->expects($this->exactly(0))
                   ->method('executeAction');

         $this->task->expects($this->exactly(0))
                   ->method('notifySubTasks');

        $this->task->expects($this->exactly(0))
                   ->method('afterExecute');

        $result = $this->task->execute();
        
        $this->assertFalse($result);
    }

    public function testExecuteUpdateExecuteActionFail()
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods([
                'beforeExecute',
                'updateProcessId',
                'executeAction',
                'afterExecute'
            ])
            ->getMock();

        $this->task->expects($this->once())
                   ->method('beforeExecute');

        $this->task->expects($this->once())
                   ->method('updateProcessId')
                   ->willReturn(true);

        $this->task->expects($this->exactly(1))
                   ->method('executeAction')
                   ->willReturn(false);

        $this->task->expects($this->exactly(0))
                   ->method('afterExecute');

        $result = $this->task->execute();
        
        $this->assertFalse($result);
    }
    //execute -- 结束

    //scheduled -- 开始
    public function testScheduled()
    {
        $this->task = $this->getMockBuilder(MockTask::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->task->setStatus(Task::STATUS['EXECUTING']);
        $this->task->setUpdateTime(time());

        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->edit(
            Argument::exact($this->task),
            Argument::exact(array(
                'status',
                'statusTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->task->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->task->scheduled();
        
        $this->assertTrue($result);
    }
    //scheduled -- 结束

    //refresh -- 开始
    public function testRefresh()
    {
        $this->task->refresh();
        
        $this->assertInstanceof(
            'BaseData\Task\Adapter\Task\ITaskAdapter',
            $this->task->getRepository()
        );
        //测试初始化pid
        $this->assertGreaterThan(0, $this->task->getPid());
    }
    //refresh -- 结束
}

<?php
namespace BaseData\Template\BjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\BjTemplate;

/**
 * 测试本级资源目录接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在多条本级资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_bj_template' => $this->bjTemplate()
            ]
        );
    }

    public function testViewBjTemplate()
    {
        $setBjTemplateList = $this->bjTemplate();
        $setGbTemplateList = $this->gbTemplate();
        foreach ($setGbTemplateList as $setGbTemplate) {
            $setGbTemplateList[$setGbTemplate['gb_template_id']] = $setGbTemplate;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'bjTemplates?filter[sourceUnit]=1&filter[dimension]='.Template::DIMENSION['SHGK'].
            '&filter[infoClassify]='.Template::INFO_CLASSIFY['QT'].
            '&filter[infoCategory]='.Template::INFO_CATEGORY['QTXX'].
            '&filter[name]=企业基本信息&filter[identify]=BJQYJBXX&include=gbTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $bjTemplate) {
            $this->bjTemplateCompareData($setBjTemplateList[$key], $bjTemplate);
        }

        foreach ($included as $key => $value) {
            if ($value['type'] == 'gbTemplates') {
                $this->gbTemplateCompareData($setGbTemplateList[$value['id']], $value);
            }
        }
    }
}

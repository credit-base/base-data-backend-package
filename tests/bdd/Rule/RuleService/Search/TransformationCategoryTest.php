<?php
namespace BaseData\Rule\RuleService\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Rule\SetDataTrait;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Translator\RuleServiceDbTranslator;

use BaseData\Template\Model\Template;

 /**
 * @Feature: 我是超级管理员/平台管理员/委办局管理员/操作用户,我拥有目录规则管理权限，当我想要查看某个资源目录的规则时,
 *           在政务网OA中的目录规则管理模块,我可以搜索规则,通过列表形式查看我搜索到的规则数据,以便于我快速定位规则并进行下一步操作
 * @Scenario: 通过目标资源目录类型搜索
 */
class TransformationCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
    }

    /**
     * @Given: 存在规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_rule' => $this->rule()
        ]);
    }
    /**
     * @When: 当我查看规则数据列表时
     */
    public function fetchRuleList()
    {
        $repository = new RuleServiceRepository();
 
        $filter['transformationCategory'] = Template::CATEGORY['BJ'];

        list($ruleList, $count) = $repository->filter($filter);
        
        unset($count);

        return $ruleList;
    }

    /**
     * @Then: 我可以看到规则目标资源目录类型为"本级"的规则数据
     */
    public function testViewRuleList()
    {
        $setRuleList = $this->rule();

        foreach ($setRuleList as $key => $setRule) {
            unset($setRule);
            $setRuleList[$key]['rule_id'] = $key +1;
        }

        $ruleList = $this->fetchRuleList();
        $translator = new RuleServiceDbTranslator();
        foreach ($ruleList as $rule) {
            $ruleArray[] = $translator->objectToArray($rule);
        }

        $this->assertEquals($ruleArray, $setRuleList);

        foreach ($ruleArray as $rule) {
            $this->assertEquals(Template::CATEGORY['BJ'], $rule['transformation_category']);
        }
    }
}

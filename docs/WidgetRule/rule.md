# 规则控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 规则控件规范

* `SOURCE_CATEGORY`: 来源资源目录类型控件规范.
* `SOURCE_TEMPLATE`: 来源资源目录控件规范.
* `TRANSFORMATION_CATEGORY`: 目标资源目录类型控件规范.
* `TRANSFORMATION_TEMPLATE`: 目标资源目录控件规范.
* `RULES`: 规则控件规范.
* `CREW`: 发布人控件规范.

### `SOURCE_CATEGORY`

**来源资源目录类型** 用于表述来源资源目录类型

#### 规则

* int
	* CATEGORY[WBJ] 10 //委办局
	* CATEGORY[BJ] 1 //本级
	* CATEGORY[GB] 2 //国标
	* CATEGORY[BASE] 3 //基础
	* CATEGORY[QZJ_WBJ] 20 //前置机委办局
	* CATEGORY[QZJ_BJ] 21 //前置机本级
	* CATEGORY[QZJ_GB] 22 //前置机国标

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `SOURCE_TEMPLATE`

**来源资源目录** 用于表述来源资源目录

#### 规则

* int

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `TRANSFORMATION_CATEGORY`

**目标资源目录类型** 用于表述目标资源目录类型

#### 规则

* int
	* CATEGORY[WBJ] 10 //委办局
	* CATEGORY[BJ] 1 //本级
	* CATEGORY[GB] 2 //国标
	* CATEGORY[BASE] 3 //基础
	* CATEGORY[QZJ_WBJ] 20 //前置机委办局
	* CATEGORY[QZJ_BJ] 21 //前置机本级
	* CATEGORY[QZJ_GB] 22 //前置机国标

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `TRANSFORMATION_TEMPLATE`

**目标资源目录** 用于表述目标资源目录

#### 规则

* int

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `RULES`

**规则** 用于表述规则

#### 规则

* array
* 数组的键必须为以下四个

    ```
    RULE_NAME = array(
        'TRANSFORMATION_RULE' => 'transformationRule', //转换规则
        'COMPLETION_RULE' => 'completionRule',//补全规则
        'COMPARISON_RULE' => 'comparisonRule',//比对规则
        'DE_DUPLICATION_RULE' => 'deDuplicationRule', //去重规则
    );
    ```
* 转换规则transformationRule,必须为数组
* 补全规则completionRule,必须为数组,且每个字段的补全规则数量不能超过两个,每个补全规则必须包含id,category,base,item,且id的值为int,base必须为数组且数组值必须在1(企业名称),2(身份证号/统一社会信用代码)的范围内,item为字符串,category为int
* 比对规则comparisonRule,必须为数组,且每个字段的比对规则数量不能超过两个,每个比对规则必须包含id,category,base,item,且id的值为int,base必须为数组且数组值必须在1(企业名称),2(身份证号/统一社会信用代码)的范围内,item为字符串,category为int
* 去重规则deDuplicationRule,必须为数组,数组中必须包含result,items,result必须为int,且范围在1(丢弃),2(覆盖)之间,items必须是数组

### `CREW`

**发布人** 用于表述发布人

#### 规则

* int

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

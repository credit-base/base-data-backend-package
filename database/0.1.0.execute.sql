
-- --------------------------------------------------------

CREATE TABLE `pcore_gb_template` (
  `gb_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='国标资源目录表';

ALTER TABLE `pcore_gb_template`
  ADD PRIMARY KEY (`gb_template_id`),
  ADD UNIQUE KEY `identify` (`identify`);

ALTER TABLE `pcore_gb_template`
  MODIFY `gb_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_bj_template` (
  `bj_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',
  `source_unit_id` int(10) NOT NULL COMMENT '来源单位id',
  `gb_template_id` int(10) NOT NULL COMMENT '国标目录id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='本级资源目录表';

ALTER TABLE `pcore_bj_template`
  ADD PRIMARY KEY (`bj_template_id`),
  ADD UNIQUE KEY `identify` (`identify`, `source_unit_id`);

ALTER TABLE `pcore_bj_template`
  MODIFY `bj_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_wbj_template` (
  `wbj_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',
  `source_unit_id` int(10) NOT NULL COMMENT '来源单位id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='委办局资源目录表';

ALTER TABLE `pcore_wbj_template`
  ADD PRIMARY KEY (`wbj_template_id`),
  ADD UNIQUE KEY `identify` (`identify`, `source_unit_id`);

ALTER TABLE `pcore_wbj_template`
  MODIFY `wbj_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_qzj_template` (
  `qzj_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `category` tinyint(1) NOT NULL COMMENT '目录类别，前置机委办局 20 | 前置机本级 21 | 前置机国标 22',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',
  `source_unit_id` int(10) NOT NULL COMMENT '来源委办局id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='本级资源目录表';

ALTER TABLE `pcore_qzj_template`
  ADD PRIMARY KEY (`qzj_template_id`),
  ADD UNIQUE KEY `identify` (`identify`, `source_unit_id`, `category`);

ALTER TABLE `pcore_qzj_template`
  MODIFY `qzj_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_parent_task` (
  `parent_task_id` int(10) NOT NULL COMMENT '主键id',

  `template_type` tinyint(1) NOT NULL COMMENT '基础目录，国标目录 1 | 本级目录 2',
  `title` varchar(200) NOT NULL COMMENT '任务标题',
  `description` text NOT NULL COMMENT '任务描述',
  `end_time` date NOT NULL COMMENT '终结时间',
  `attachment` json NOT NULL COMMENT '依据附件',
  `template_id` int(10) NOT NULL COMMENT '指派目录id',
  `template_name` varchar(100) NOT NULL COMMENT '指派目录名称',
  `assign_objects` json NOT NULL COMMENT '指派对象',
  `finish_count` int(10) NOT NULL COMMENT '已完结总数，任务状态为已撤销、已确认、已终结时，算作已完结',
  `reason` text NOT NULL COMMENT '撤销原因',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工单任务父任务表';

ALTER TABLE `pcore_parent_task`
  ADD PRIMARY KEY (`parent_task_id`);

ALTER TABLE `pcore_parent_task`
  MODIFY `parent_task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_work_order_task` (
  `work_order_task_id` int(10) NOT NULL COMMENT '主键id',

  `title` varchar(200) NOT NULL COMMENT '任务标题',
  `end_time` date NOT NULL COMMENT '终结时间',
  `reason` text NOT NULL COMMENT '撤销原因或终结原因',
  `feedback_records` json NOT NULL COMMENT '反馈记录',

  `template_type` tinyint(1) NOT NULL COMMENT '基础目录，国标目录 1 | 本级目录 2',
  `template_id` int(10) NOT NULL COMMENT '指派目录id',
  `template_name` varchar(100) NOT NULL COMMENT '指派目录名称',
  `assign_object_id` int(10) NOT NULL COMMENT '指派对象id',
  `assign_object_name` varchar(100) NOT NULL COMMENT '指派对象名称',
  `parent_task_id` int(10) NOT NULL COMMENT '父任务id',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工单任务表';

ALTER TABLE `pcore_work_order_task`
  ADD PRIMARY KEY (`work_order_task_id`),
  ADD UNIQUE KEY `assign_object_id` (`assign_object_id`, `parent_task_id`);

ALTER TABLE `pcore_work_order_task`
  MODIFY `work_order_task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_ys_search_data` (
  `ys_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `wbj_template_id` int(10) NOT NULL COMMENT '委办局资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `ys_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认正常),(STATUS_DISABLED -2 屏蔽)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='原始资源目录数据搜索表';

ALTER TABLE `pcore_ys_search_data`
  ADD PRIMARY KEY (`ys_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`wbj_template_id`,`identify`),
  ADD KEY `wbj_template_id` (`wbj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_ys_search_data`
  MODIFY `ys_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_ys_items_data` (
  `ys_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='原始资源目录数据表';

ALTER TABLE `pcore_ys_items_data`
  ADD PRIMARY KEY (`ys_items_data_id`);

ALTER TABLE `pcore_ys_items_data`
  MODIFY `ys_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_wbj_search_data` (
  `wbj_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `wbj_template_id` int(10) NOT NULL COMMENT '委办局资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `wbj_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_ENABLED 0 默认正常),(STATUS_DISABLED -2 屏蔽)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='委办局资源目录数据搜索表';

ALTER TABLE `pcore_wbj_search_data`
  ADD PRIMARY KEY (`wbj_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`wbj_template_id`,`identify`),
  ADD KEY `wbj_template_id` (`wbj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_wbj_search_data`
  MODIFY `wbj_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_wbj_items_data` (
  `wbj_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='委办局资源目录数据表';

ALTER TABLE `pcore_wbj_items_data`
  ADD PRIMARY KEY (`wbj_items_data_id`);

ALTER TABLE `pcore_wbj_items_data`
  MODIFY `wbj_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_bj_search_data` (
  `bj_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `bj_template_id` int(10) NOT NULL COMMENT '本级资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `bj_items_data_id` char(24) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_CONFIRM 0 待确认),(STATUS_ENABLED 2 已确认),(STATUS_DISABLED -2 屏蔽),(STATUS_DELETED -4 封存)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id',
  `front_end_processor_status` tinyint(1) NOT NULL COMMENT '前置机数据导入状态 0 未导入 2 导入',
  `description` text NOT NULL COMMENT '待确认规则描述'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='本级资源目录数据搜索表';

ALTER TABLE `pcore_bj_search_data`
  ADD PRIMARY KEY (`bj_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`bj_template_id`,`identify`),
  ADD KEY `bj_template_id` (`bj_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_bj_search_data`
  MODIFY `bj_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_bj_items_data` (
  `bj_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='本级资源目录数据表';

ALTER TABLE `pcore_bj_items_data`
  ADD PRIMARY KEY (`bj_items_data_id`);

ALTER TABLE `pcore_bj_items_data`
  MODIFY `bj_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_gb_search_data` (
  `gb_search_data_id` int(10) NOT NULL COMMENT '主键id',
  `info_classify` tinyint(1) NOT NULL COMMENT '信息分类',
  `info_category` tinyint(1) NOT NULL COMMENT '信息类别',
  `gb_template_id` int(10) NOT NULL COMMENT '国标资源目录id',
  `usergroup_id` int(10) NOT NULL COMMENT '来源单位id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `subject_category` tinyint(1) NOT NULL COMMENT '主体类别',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围',
  `name` varchar(255) NOT NULL COMMENT '主体名称:企业名称/人名',
  `identify` varchar(50) NOT NULL COMMENT '主体标识:统一社会信用代码/身份证号',
  `expiration_date` bigint(10) NOT NULL COMMENT '有效期限',
  `gb_items_data_id` int(10) NOT NULL COMMENT '资源目录数据id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_CONFIRM 0 待确认),(STATUS_ENABLED 2 已确认),(STATUS_DISABLED -2 屏蔽),(STATUS_DELETED -4 封存)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id',
  `front_end_processor_status` tinyint(1) NOT NULL COMMENT '前置机数据导入状态 0 未导入 2 导入',
  `description` text NOT NULL COMMENT '待确认规则描述'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='国标资源目录数据搜索表';

ALTER TABLE `pcore_gb_search_data`
  ADD PRIMARY KEY (`gb_search_data_id`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `info_classify` (`status`,`info_classify`,`subject_category`,`dimension`,`usergroup_id`,`gb_template_id`,`identify`),
  ADD KEY `gb_template_id` (`gb_template_id`,`subject_category`,`status`,`dimension`,`usergroup_id`,`identify`);

ALTER TABLE `pcore_gb_search_data`
  MODIFY `gb_search_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

CREATE TABLE `pcore_gb_items_data` (
  `gb_items_data_id` int(10) NOT NULL COMMENT '主键id',
  `data` text NOT NULL COMMENT '数据'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='国标资源目录数据表';

ALTER TABLE `pcore_gb_items_data`
  ADD PRIMARY KEY (`gb_items_data_id`);

ALTER TABLE `pcore_gb_items_data`
  MODIFY `gb_items_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_schedule_task` (
  `task_id` int(10) NOT NULL COMMENT '主键id',
  `parent_id` int(10) NOT NULL COMMENT '父任务id',
  `identify` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1:once, 2:period',
  `data` text NOT NULL,
  `next_schedule_time` int(10) NOT NULL,
  `nice` int(10) NOT NULL,
  `pid` int(10) NOT NULL COMMENT '进程id',
  `status_time` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '-4:未定义, -3:失败, -2:异常, 0:睡眠, 2:待处理, 4:执行中, 6:成功',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `pcore_schedule_task`
  ADD PRIMARY KEY (`task_id`);

ALTER TABLE `pcore_schedule_task`
  MODIFY `task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_apply_form_rule` (
  `apply_form_rule_id` int(10) NOT NULL COMMENT '申请规则信息id',
  `publish_crew_id` int(10) NOT NULL COMMENT '发布人',
  `usergroup_id` int(10) NOT NULL COMMENT '来源委办局',
  `apply_crew_id` int(10) NOT NULL COMMENT '审核人',
  `apply_usergroup_id` int(10) NOT NULL COMMENT '审核委办局',
  `operation_type` tinyint(1) NOT NULL COMMENT '操作类型',
  `apply_info` text NOT NULL COMMENT '申请信息',
  `reject_reason` text NOT NULL COMMENT '驳回原因',
  `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `transformation_template_id` int(10) NOT NULL COMMENT '目标资源目录id',
  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源资源目录id',
  `relation_id` int(10) NOT NULL COMMENT '关联规则id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `apply_status` tinyint(1) NOT NULL COMMENT '审核状态 (0,待审核, 2 通过, -2 驳回, -4 撤销)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='申请信息表';

ALTER TABLE `pcore_apply_form_rule`
  ADD PRIMARY KEY (`apply_form_rule_id`);

ALTER TABLE `pcore_apply_form_rule`
  MODIFY `apply_form_rule_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '申请规则信息id';

-- --------------------------------------------------------

CREATE TABLE `pcore_rule` (
  `rule_id` int(10) NOT NULL COMMENT '主键id',
  `transformation_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `transformation_template_id` int(10) NOT NULL COMMENT '目标资源目录id(本级/国标)',
  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源资源目录id(委办局)',
  `usergroup_id` int(10) NOT NULL COMMENT '来源委办局id',
  `crew_id` int(10) NOT NULL COMMENT '发布人id',
  `rules` text NOT NULL COMMENT '规则',
  `version` int(10) NOT NULL COMMENT '版本号',
  `data_total` int(10) NOT NULL COMMENT '已转换数据量',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态 (0 正常, -2 删除)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='资源目录规则表';

ALTER TABLE `pcore_rule` ADD PRIMARY KEY (`rule_id`);

ALTER TABLE `pcore_rule` MODIFY `rule_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_task` (
  `task_id` int(10) NOT NULL COMMENT '主键id',
  `crew_id` int(10) NOT NULL COMMENT '员工id',
  `user_group_id` int(10) NOT NULL COMMENT '委办局id',
  `pid` int(10) NOT NULL COMMENT '父id',

  `total` int(10) NOT NULL COMMENT '总数',
  `success_number` int(10) NOT NULL COMMENT '成功数',
  `failure_number` int(10) NOT NULL COMMENT '失败数',

  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源目录id',
  `target_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `target_template_id` int(10) NOT NULL COMMENT '目标目录id',
  `target_rule_id` int(10) NOT NULL COMMENT '入目标库使用规则id',

  `schedule_task_id` int(10) NOT NULL COMMENT '调度任务id',
  `error_number` int(10) NOT NULL COMMENT '错误编号',
  `file_name` varchar(255) NOT NULL COMMENT '上传文件文件名',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态 (0 进行中, -2 失败, 2 成功, -3 失败文件下载成功)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='任务表';

ALTER TABLE `pcore_task` ADD PRIMARY KEY (`task_id`);

ALTER TABLE `pcore_task` MODIFY `task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_schedule_task_notify_record` (
  `record_id` int(10) NOT NULL COMMENT '主键id',
  `name` varchar(255) NOT NULL COMMENT '文件名',
  `hash` char(32) NOT NULL COMMENT 'hash',
  `status` tinyint(1) NOT NULL COMMENT '0: 待执行，2: 执行成功',
  `create_time` int(10) NOT NULL,
  `update_time` int(10) NOT NULL,
  `status_time` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `pcore_schedule_task_notify_record`
  ADD PRIMARY KEY (`record_id`);

ALTER TABLE `pcore_schedule_task_notify_record`
  MODIFY `record_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

ALTER TABLE `pcore_schedule_task_notify_record` ADD UNIQUE(`hash`);

-- --------------------------------------------------------

CREATE TABLE `pcore_error_data` (
  `error_data_id` int(10) NOT NULL COMMENT '主键id',

  `task_id` int(10) NOT NULL COMMENT '任务id',
  `category` tinyint(1) NOT NULL COMMENT '库类别',
  `template_id` int(10) NOT NULL COMMENT '目录id',
  `items_data` text NOT NULL COMMENT '资源目录数据',
  `error_type` tinyint(1) NOT NULL COMMENT '错误类型',
  `error_reason` json NOT NULL COMMENT '错误原因',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, 1 程序异常, 2 入库异常)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='错误数据表';

ALTER TABLE `pcore_error_data`
  ADD PRIMARY KEY (`error_data_id`);

ALTER TABLE `pcore_error_data`
  MODIFY `error_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_enterprise` (
  `enterprise_id` int(10) NOT NULL COMMENT '企业主键id',
  `name` varchar(255) NOT NULL COMMENT '企业名称/个体名称',
  `unified_social_credit_code` varchar(50) NOT NULL COMMENT '统一社会信用代码',
  `establishment_date` int(10) NOT NULL COMMENT '成立日期/注册日期',
  `approval_date` int(10) NOT NULL COMMENT '核准日期',
  `address` varchar(255) NOT NULL COMMENT '住所/经营场所',
  `registration_capital` varchar(50) NOT NULL COMMENT '注册资本（万）',
  `business_term_start` int(10) NOT NULL COMMENT '营业期限自',
  `business_term_to` int(10) NOT NULL COMMENT '营业期限至',
  `business_scope` text NOT NULL COMMENT '经营范围',
  `registration_authority` varchar(255) NOT NULL COMMENT '登记机关',
  `principal` varchar(255) NOT NULL COMMENT '法定代表人/经营者',
  `principal_card_id` varchar(20) NOT NULL COMMENT '法人身份证号',
  `registration_status` varchar(255) NOT NULL COMMENT '登记状态(1=存续(在营、开业、在册);2=吊销，未注销;3=吊 销，已注销;4=注销;5=撤 销;6=迁出;9=其他)',
  `enterprise_type_code` varchar(255) NOT NULL COMMENT '企业类型代码',
  `enterprise_type` varchar(255) NOT NULL COMMENT '企业类型',
  `data` text NOT NULL COMMENT '其他数据',
  `industry_category` char(1) NOT NULL COMMENT '行业门类',
  `industry_code` char(5) NOT NULL COMMENT '行业代码',
  `administrative_area` int(4) NOT NULL COMMENT '所属区域',
  `hash` char(32) NOT NULL COMMENT '摘要hash',
  `task_id` int(10) NOT NULL COMMENT '任务id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(STATUS_NORMAL,0,默认),(STATUS_DELETE,-2,删除)',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业表';

ALTER TABLE `pcore_enterprise`
  ADD PRIMARY KEY (`enterprise_id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD UNIQUE KEY `unified_social_credit_code` (`unified_social_credit_code`);

ALTER TABLE `pcore_enterprise`
  MODIFY `enterprise_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '企业主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_base_template` (
  `base_template_id` int(10) NOT NULL COMMENT '主键id',

  `name` varchar(100) NOT NULL COMMENT '目录名称',
  `identify` varchar(100) DEFAULT NULL COMMENT '目录标识',
  `subject_category` json NOT NULL COMMENT '主体类别，法人及非法人组织 1 | 自然人 2 | 个体工商户 3',
  `dimension` tinyint(1) NOT NULL COMMENT '公开范围，社会公开 1 | 政务共享 2 | 授权查询 3',
  `exchange_frequency` int(10) NOT NULL COMMENT '更新频率',
  `info_classify` int(10) NOT NULL COMMENT '信息分类',
  `info_category` int(10) NOT NULL COMMENT '信息类别',
  `description` text NOT NULL COMMENT '目录描述',
  `items` json NOT NULL COMMENT '模板信息',

  `status` tinyint(1) NOT NULL COMMENT '状态(默认 0)',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='基础资源目录表';

ALTER TABLE `pcore_base_template`
  ADD PRIMARY KEY (`base_template_id`),
  ADD UNIQUE KEY `identify` (`identify`);

ALTER TABLE `pcore_base_template`
  MODIFY `base_template_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

--
-- 统计企业的行政许可,行政处罚,守信激励,失信惩戒数据
--

DROP PROCEDURE IF EXISTS statics_enterprise_relation_information_count;

DELIMITER //
CREATE PROCEDURE statics_enterprise_relation_information_count(in ug1 varchar(255), in ug2 varchar(255)) 
BEGIN
  DECLARE licensing_total, penalty_total, incentive_total, punish_total INT;
  SET licensing_total = 0, penalty_total = 0, incentive_total = 0, punish_total = 0;
  SELECT COUNT(bj_search_data_id) INTO licensing_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 1 AND dimension IN (ug2);
    SELECT COUNT(bj_search_data_id) INTO penalty_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 2 AND dimension IN (ug2);
  SELECT COUNT(bj_search_data_id) INTO incentive_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 3 AND dimension IN (ug2);
  SELECT COUNT(bj_search_data_id) INTO punish_total FROM pcore_bj_search_data WHERE identify = CONCAT(ug1) AND status = 0 AND info_classify = 4 AND dimension IN (ug2);
  SELECT licensing_total, penalty_total, incentive_total, punish_total;
END//
DELIMITER ;

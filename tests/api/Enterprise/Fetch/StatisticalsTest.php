<?php
namespace BaseData\Enterprise\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Enterprise\SetDataApiTrait;
use BaseData\Enterprise\EnterpriseUtils;
use BaseData\Enterprise\Model\Enterprise;

/**
 * 测试企业接口根据检索条件查询数据
 */
class StatisticalsTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, EnterpriseUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_enterprise');
    }

    /**
     * 存在多条企业数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_enterprise' => $this->enterprise()
            ]
        );
    }

    public function testViewEnterprise()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'statisticals/enterpriseRelationInformationCount?
            filter[unifiedSocialCreditCode]=912908Q21903E74AX2&filter[dimension]=1',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $this->assertEquals([], $contents['meta']);

        $data = $contents['data'];

        $this->assertEquals(0, $data['id']);
        $this->assertEquals('statisticals', $data['type']);

        $attributes = $data['attributes'];

        $this->assertEquals(0, $attributes['result']['licensingTotal']);
        $this->assertEquals(0, $attributes['result']['penaltyTotal']);
        $this->assertEquals(0, $attributes['result']['incentiveTotal']);
        $this->assertEquals(0, $attributes['result']['punishTotal']);
    }
}

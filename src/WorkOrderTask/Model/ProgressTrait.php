<?php
namespace BaseData\WorkOrderTask\Model;

use Marmot\Core;

use BaseData\Template\Model\WbjTemplate;

trait ProgressTrait
{

    /**
     * @var string $reason 撤销原因或终结原因
     */
    protected $reason;
    /**
     * @var array $feedbackRecords 反馈记录
     */
    protected $feedbackRecords;

    public function setReason(string $reason) : void
    {
        $this->reason = $reason;
    }

    public function getReason() : string
    {
        return $this->reason;
    }

    public function setFeedbackRecords(array $feedbackRecords) : void
    {
        $this->feedbackRecords = $feedbackRecords;
    }

    public function getFeedbackRecords() : array
    {
        return $this->feedbackRecords;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['DQR'];
    }

    public function revoke(string $reason) : bool
    {
        // 撤销【当子任务状态为待确认时，可撤销】；说明撤销原因
        if (!$this->isDqr()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        $this->setStatus(self::STATUS['YCX']);
        $this->setReason($reason);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'reason',
                'statusTime',
                'updateTime'
            )
        ) && $this->getParentTask()->updateFinishCount();
    }

    public function confirm() : bool
    {
        // 确认【当子任务状态为待确认、跟进中时，可确认】
        if (!$this->isDqr() && !$this->isGjz()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        $this->setStatus(self::STATUS['YQR']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        if (!$this->isTemplateExist()) {
            $items = $this->getItems();
            $this->addWbjTemplate($items);
        }

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'statusTime',
                'updateTime'
            )
        ) && $this->getParentTask()->updateFinishCount();
    }

    public function end(string $reason) : bool
    {
        // 终结【当子任务状态为跟进中时，可终结】；说明终结原因
        if (!$this->isGjz()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        $this->setStatus(self::STATUS['YZJ']);
        $this->setReason($reason);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'reason',
                'statusTime',
                'updateTime'
            )
        ) && $this->getParentTask()->updateFinishCount();
    }

    public function feedback() : bool
    {
        // 反馈【当子任务状态为待确认、跟进中时，可反馈】
        if (!$this->isDqr() && !$this->isGjz()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        $this->setStatus(self::STATUS['GJZ']);
        $this->setStatusTime(Core::$container->get('time'));
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'status',
                'feedbackRecords',
                'statusTime',
                'updateTime'
            )
        );
    }

    protected function isDqr() : bool
    {
        return $this->getStatus() == self::STATUS['DQR'];
    }

    protected function isGjz() : bool
    {
        return $this->getStatus() == self::STATUS['GJZ'];
    }

    protected function isTemplateExist() : bool
    {
        $isExistedTemplate = self::IS_EXISTED_TEMPLATE['FOU'];
        $feedbackRecords = $this->getFeedbackRecords();
        if (!empty($feedbackRecords)) {
            $lastFeedbackRecords = end($feedbackRecords);
            $isExistedTemplate = $lastFeedbackRecords['isExistedTemplate'];
        }
        return $isExistedTemplate;
    }

    protected function getItems() : array
    {
        $items = array();
        $feedbackRecords = $this->getFeedbackRecords();
        if (!empty($feedbackRecords)) {
            $lastFeedbackRecords = end($feedbackRecords);
            $items = $lastFeedbackRecords['items'];
        }

        return $items;
    }

    protected function getWbjTemplate() : WbjTemplate
    {
        return new WbjTemplate();
    }

    protected function addWbjTemplate(array $items) : bool
    {
        $wbjTemplate = $this->getWbjTemplate();

        $template = $this->getTemplate();
        $wbjTemplate->setName($template->getName());
        $wbjTemplate->setIdentify($template->getIdentify());
        $wbjTemplate->setSubjectCategory($template->getSubjectCategory());
        $wbjTemplate->setDimension($template->getDimension());
        $wbjTemplate->setExchangeFrequency($template->getExchangeFrequency());
        $wbjTemplate->setInfoClassify($template->getInfoClassify());
        $wbjTemplate->setInfoCategory($template->getInfoCategory());
        $wbjTemplate->setDescription($template->getDescription());
        $wbjTemplate->setSourceUnit($this->getAssignObject());
        $wbjTemplate->setItems($items);

        return $wbjTemplate->add();
    }
}

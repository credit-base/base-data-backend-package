<?php
namespace BaseData\Enterprise\Model;

use PHPUnit\Framework\TestCase;

class NullEnterpriseTest extends TestCase
{
    private $enterprise;

    public function setUp()
    {
        $this->enterprise = $this->getMockBuilder(NullEnterprise::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->enterprise);
    }

    public function testExtendsEnterprise()
    {
        $this->assertInstanceof('BaseData\Enterprise\Model\Enterprise', $this->enterprise);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->enterprise);
    }

    public function testIsNameExist()
    {
        $this->enterprise->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->enterprise->isNameExist();
        $this->assertFalse($result);
    }

    public function testIsUnifiedSocialCreditCodeExist()
    {
        $this->enterprise->expects($this->exactly(1))
                    ->method('resourceNotExist')
                    ->willReturn(false);

        $result = $this->enterprise->isUnifiedSocialCreditCodeExist();
        $this->assertFalse($result);
    }
}

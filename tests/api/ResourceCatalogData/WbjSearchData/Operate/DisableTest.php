<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Common\Model\IEnableAble;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

/**
 * 测试委办局资源目录接口屏蔽
 */
class DisableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * 存在一条委办局资源目录数据数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data'],
            ]
        );
    }

    public function testViewCrew()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'wbjSearchData/1/disable',
            ['headers' => Core::$container->get('client.headers')]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IEnableAble::STATUS['DISABLED'], $attributes['status']);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class WbjItemsDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'wbj_items_data_id',
            new Persistence\WbjItemsDataCache(),
            new Persistence\WbjItemsDataDb()
        );
    }
}

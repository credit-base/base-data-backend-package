<?php
namespace BaseData\ResourceCatalogData\Utils;

trait ErrorDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $errorData
    ) {
        $this->assertEquals($expectedArray['error_data_id'], $errorData->getId());
        $this->assertEquals($expectedArray['task_id'], $errorData->getTask()->getId());
        $this->assertEquals($expectedArray['category'], $errorData->getCategory());
        $this->assertEquals($expectedArray['template_id'], $errorData->getTemplate()->getId());

        if (is_string($expectedArray['items_data'])) {
            $itemsData = unserialize(gzuncompress(base64_decode($expectedArray['items_data'], true)));
        }
        if (is_array($expectedArray['items_data'])) {
            $itemsData = $expectedArray['items_data'];
        }
        $this->assertEquals($itemsData, $errorData->getItemsData()->getData());
    
        $this->errorReasonEquals($expectedArray, $errorData);
        $this->assertEquals($expectedArray['error_type'], $errorData->getErrorType());
        $this->assertEquals($expectedArray['status'], $errorData->getStatus());
        $this->assertEquals($expectedArray['create_time'], $errorData->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $errorData->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $errorData->getStatusTime());
    }

    private function errorReasonEquals($expectedArray, $errorData)
    {
        $errorReason = array();

        if (is_string($expectedArray['error_reason'])) {
            $errorReason = json_decode($expectedArray['error_reason'], true);
        }
        if (is_array($expectedArray['error_reason'])) {
            $errorReason = $expectedArray['error_reason'];
        }

        $this->assertEquals($errorReason, $errorData->getErrorReason());
    }
}

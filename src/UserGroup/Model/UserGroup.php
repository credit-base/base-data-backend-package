<?php
namespace BaseData\UserGroup\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class UserGroup implements IObject
{
    use Object;
    
    const STATUS_NORMAL = 0; //正常
    
    private $id;
    
    private $name;
    
    private $shortName;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->name = '';
        $this->shortName = '';
        $this->updateTime = 0;
        $this->createTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->shortName);
        unset($this->updateTime);
        unset($this->createTime);
        unset($this->status);
        unset($this->statusTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setShortName(string $shortName): void
    {
        $this->shortName = $shortName;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }
    
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}

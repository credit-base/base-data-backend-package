<?php

return [
    // 100 - 500 -- start
    PARAMETER_IS_EMPTY=>
        [
            'id'=>PARAMETER_IS_EMPTY,
            'link'=>'',
            'status'=>403,
            'code'=>'PARAMETER_IS_EMPTY',
            'title'=>'数据为空',
            'detail'=>'表述传输了一个空的数据.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    PARAMETER_FORMAT_ERROR=>
        [
            'id'=>PARAMETER_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'PARAMETER_FORMAT_ERROR',
            'title'=>'数据格式不正确',
            'detail'=>'表述传输了一个格式不正确的字段.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    RESOURCE_CAN_NOT_MODIFY=>
        [
            'id'=>RESOURCE_CAN_NOT_MODIFY,
            'link'=>'',
            'status'=>403,
            'code'=>'RESOURCE_CAN_NOT_MODIFY',
            'title'=>'资源不能操作',
            'detail'=>'表述不能操作一个不具备操作权限的资源.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    RESOURCE_ALREADY_EXIST=>
        [
            'id'=>RESOURCE_ALREADY_EXIST,
            'link'=>'',
            'status'=>409,
            'code'=>'RESOURCE_ALREADY_EXIST',
            'title'=>'资源已存在',
            'detail'=>'表述传输了一个已存在的资源.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    PARAMETER_ERROR=>
        [
            'id'=>PARAMETER_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'PARAMETER_ERROR',
            'title'=>'数据不正确',
            'detail'=>'表述传输了一个不正确的字段.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    // 100 - 500 -- end
    // 501 - 1000 -- start
    USER_REAL_NAME_FORMAT_ERROR=>
        [
            'id'=>USER_REAL_NAME_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'USER_REAL_NAME_FORMAT_ERROR',
            'title'=>'用户姓名格式不正确',
            'detail'=>'用户姓名格式不正确.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    USER_CELLPHONE_FORMAT_ERROR=>
        [
            'id'=>USER_CELLPHONE_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'USER_CELLPHONE_FORMAT_ERROR',
            'title'=>'用户手机格式不正确',
            'detail'=>'用户手机格式不正确.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    USER_PASSWORD_FORMAT_ERROR=>
        [
            'id'=>USER_PASSWORD_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'USER_PASSWORD_FORMAT_ERROR',
            'title'=>'用户密码格式不正确',
            'detail'=>'用户密码格式不正确.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    USER_CARDID_FORMAT_ERROR=>
        [
            'id'=>USER_CARDID_FORMAT_ERROR,
            'link'=>'',
            'status'=>403,
            'code'=>'USER_CARDID_FORMAT_ERROR',
            'title'=>'用户身份证格式不正确',
            'detail'=>'用户身份证格式不正确.',
            'source'=>[
                'pointer'=>''
            ],
            'meta'=>[]
        ],
    PASSWORD_INCORRECT=>
        [
            'id'=>PASSWORD_INCORRECT,
            'link'=>'',
            'status'=>403,
            'code'=>'PASSWORD_INCORRECT',
            'title'=>'密码不正确',
            'detail'=>'密码不正确.',
            'source'=>[
                'pointer'=>'password'
            ],
            'meta'=>[]
        ],
    // 501 - 1000 -- end

    DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP=>
        [
            'id'=>DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP,
            'link'=>'',
            'status'=>403,
            'code'=>'DEPARTMENT_NOT_BELONG_TO_THE_USER_GROUP',
            'title'=>'该所属科室不属于该所属委办局',
            'detail'=>'该所属科室不属于该所属委办局.',
            'source'=>[
                'pointer'=>'department'
            ],
            'meta'=>[]
        ],
];

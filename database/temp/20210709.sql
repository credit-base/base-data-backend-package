

-- --------------------------------------------------------

CREATE TABLE `pcore_task` (
  `task_id` int(10) NOT NULL COMMENT '主键id',
  `crew_id` int(10) NOT NULL COMMENT '员工id',
  `user_group_id` int(10) NOT NULL COMMENT '委办局id',
  `pid` int(10) NOT NULL COMMENT '父id',

  `total` int(10) NOT NULL COMMENT '总数',
  `success_number` int(10) NOT NULL COMMENT '成功数',
  `failure_number` int(10) NOT NULL COMMENT '失败数',

  `source_category` tinyint(1) NOT NULL COMMENT '来源库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `source_template_id` int(10) NOT NULL COMMENT '来源目录id',
  `target_category` tinyint(1) NOT NULL COMMENT '目标库类别 (10 委办局, 1 本级, 2 国标, 20 前置机委办局, 21 前置机本级, 22 前置机国标)',
  `target_template_id` int(10) NOT NULL COMMENT '目标目录id',
  `target_rule_id` int(10) NOT NULL COMMENT '入目标库使用规则id',

  `schedule_task_id` int(10) NOT NULL COMMENT '调度任务id',
  `error_number` int(10) NOT NULL COMMENT '错误编号',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态 (0 进行中, -2 失败, 2 成功)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='任务表';

ALTER TABLE `pcore_task` ADD PRIMARY KEY (`task_id`);

ALTER TABLE `pcore_task` MODIFY `task_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------

CREATE TABLE `pcore_error_data` (
  `error_data_id` int(10) NOT NULL COMMENT '主键id',

  `task_id` int(10) NOT NULL COMMENT '任务id',
  `category` tinyint(1) NOT NULL COMMENT '库类别',
  `template_id` int(10) NOT NULL COMMENT '目录id',
  `items_data` text NOT NULL COMMENT '资源目录数据',
  `error_type` tinyint(1) NOT NULL COMMENT '错误类型',
  `error_reason` json NOT NULL COMMENT '错误原因',

  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  `status` tinyint(1) NOT NULL COMMENT '状态(0 正常, 1 程序异常, 2 入库异常)', 
  `status_time` int(10) NOT NULL COMMENT '状态更新时间'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='错误数据表';

ALTER TABLE `pcore_error_data`
  ADD PRIMARY KEY (`error_data_id`);

ALTER TABLE `pcore_error_data`
  MODIFY `error_data_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id';

-- --------------------------------------------------------
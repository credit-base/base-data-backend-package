<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData;

use BaseData\ResourceCatalogData\Model\GbSearchData;

interface IGbSearchDataAdapter
{
    public function fetchOne($id) : GbSearchData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(GbSearchData $gbSearchData) : bool;

    public function edit(GbSearchData $gbSearchData, array $keys = array()) : bool;
}

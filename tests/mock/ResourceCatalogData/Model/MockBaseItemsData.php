<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter;

class MockBaseItemsData extends BaseItemsData
{
    public function getRepository() : IBaseSearchDataAdapter
    {
        return parent::getRepository();
    }
}

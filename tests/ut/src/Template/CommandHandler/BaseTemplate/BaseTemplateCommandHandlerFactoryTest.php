<?php
namespace BaseData\Template\CommandHandler\BaseTemplate;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use BaseData\Template\Command\BaseTemplate\EditBaseTemplateCommand;

class BaseTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new BaseTemplateCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testEditBaseTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditBaseTemplateCommand(
                $this->faker->randomElements(array('1','2','3'), 2),
                $this->faker->randomElements(array('1','2','3'), 2),
                $this->faker->randomDigit(),
                $this->faker->randomDigit(),
                $this->faker->randomDigit(),
                $this->faker->randomDigit(),
                $this->faker->randomDigit()
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\Template\CommandHandler\BaseTemplate\EditBaseTemplateCommandHandler',
            $commandHandler
        );
    }
}

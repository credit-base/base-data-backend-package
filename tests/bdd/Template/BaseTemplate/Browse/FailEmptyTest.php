<?php
namespace BaseData\Template\BaseTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\BaseTemplateRepository;

/**
 * @Feature: 我是超级管理员,当我需要查看我的基础目录时,在政务网OA中,可以查看到我的所有基础目录,
 *           通过列表的形式查看所有基础目录,以便于我可以快速找到我的所有基础目录
 * @Scenario: 查看资源目录列表-异常数据为空
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 不存在资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBaseTemplateList()
    {
        $repository = new BaseTemplateRepository();

        list($gbTemplateList, $count) = $repository->filter([]);
        
        unset($count);

        return $gbTemplateList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewBaseTemplate()
    {
        $gbTemplateList = $this->fetchBaseTemplateList();

        $this->assertEmpty($gbTemplateList);
    }
}

# 员工字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [人通用字典](./docs/Dictionary/user.md "人通用字典")

### userGroup

**所属委办局** 

* UserGroup
* 必填项

### department

**所属科室** 

* Department
* 选填项

### cardId

**身份证号码**

* string
* 15位或18位、数字或数字+大写字母
* 选填项

### category

**员工类型**

* int
	* SUPER_ADMINISTRATOR => 1 | 超级管理员
	* PLATFORM_ADMINISTRATOR => 2 | 平台管理员
	* USER_GROUP_ADMINISTRATOR => 3 | 委办局管理员
	* OPERATOR => 4 | 操作用户
* 必填项

### purview

**权限范围** 

* array
* 必填项

### status

**状态** 状态的表述.

* int

	* STATUS_ENABLED 0 启用
	* STATUS_DISABLED -2 禁用

### [手机号](user.md)
### [账户](user.md)
### [姓名](user.md)
### [密码](user.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

### superAdministrator

**超级管理员**

### platformAdministrator

**平台管路员**

### userGroupAdministrator

**委办局管理员**

### operationUser

**操作用户**
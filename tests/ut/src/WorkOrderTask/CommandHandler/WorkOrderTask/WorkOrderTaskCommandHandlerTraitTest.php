<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use BaseData\Crew\Adapter\Crew\ICrewAdapter;
use BaseData\Crew\Model\Crew;

use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use BaseData\UserGroup\Model\UserGroup;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Model\WbjTemplate;

class WorkOrderTaskCommandHandlerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockWorkOrderTaskCommandHandlerTrait::class)
                           ->setMethods(
                               [
                                    'getCrewRepository',
                                    'getUserGroupRepository',
                                    'getWbjTemplateRepository'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetCrewRepository()
    {
        $trait = new MockWorkOrderTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'BaseData\Crew\Adapter\Crew\ICrewAdapter',
            $trait->getCrewRepositoryPublic()
        );
    }

    public function testGetUserGroupRepository()
    {
        $trait = new MockWorkOrderTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $trait->getUserGroupRepositoryPublic()
        );
    }

    public function testGetWbjTemplateRepository()
    {
        $trait = new MockWorkOrderTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $trait->getWbjTemplateRepositoryPublic()
        );
    }

    public function testFetchOneCrew()
    {
        $id = 1;

        $repository = $this->prophesize(ICrewAdapter::class);
        $repository->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getCrewRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchOneCrewPublic($id);
    }

    public function testFetchOneUserGroup()
    {
        $id = 1;

        $repository = $this->prophesize(IUserGroupAdapter::class);
        $repository->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchOneUserGroupPublic($id);
    }

    public function testFetchOneWbjTemplate()
    {
        $id = 1;

        $repository = $this->prophesize(IWbjTemplateAdapter::class);
        $repository->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getWbjTemplateRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchOneWbjTemplatePublic($id);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\NullTask;

class TaskDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $task = null)
    {
        if (!isset($expression['task_id'])) {
            return NullTask::getInstance();
        }

        if ($task == null) {
            $task = new Task();
        }

        $task->setId($expression['task_id']);

        $task->getCrew()->setId($expression['crew_id']);
        $task->getUserGroup()->setId($expression['user_group_id']);
        $task->setPid($expression['pid']);
        $task->setTotal($expression['total']);
        $task->setSuccessNumber($expression['success_number']);
        $task->setFailureNumber($expression['failure_number']);
        $task->setSourceCategory($expression['source_category']);
        $task->setSourceTemplate($expression['source_template_id']);
        $task->setTargetCategory($expression['target_category']);
        $task->setTargetTemplate($expression['target_template_id']);
        $task->setTargetRule($expression['target_rule_id']);
        $task->setScheduleTask($expression['schedule_task_id']);
        $task->setErrorNumber($expression['error_number']);
        $task->setFileName($expression['file_name']);

        $task->setStatus($expression['status']);
        $task->setCreateTime($expression['create_time']);
        $task->setUpdateTime($expression['update_time']);
        $task->setStatusTime($expression['status_time']);
        
        return $task;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($task, array $keys = array())
    {
        if (!$task instanceof Task) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'crew',
                'userGroup',

                'pid',
                'total',
                'successNumber',
                'failureNumber',
                'sourceCategory',
                'sourceTemplate',
                'targetCategory',
                'targetTemplate',
                'targetRule',
                'scheduleTask',
                'errorNumber',
                'fileName',

                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['task_id'] = $task->getId();
        }

        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $task->getCrew()->getId();
        }
        if (in_array('userGroup', $keys)) {
            $expression['user_group_id'] = $task->getUserGroup()->getId();
        }
        if (in_array('pid', $keys)) {
            $expression['pid'] = $task->getPid();
        }
        if (in_array('total', $keys)) {
            $expression['total'] = $task->getTotal();
        }
        if (in_array('successNumber', $keys)) {
            $expression['success_number'] = $task->getSuccessNumber();
        }
        if (in_array('failureNumber', $keys)) {
            $expression['failure_number'] = $task->getFailureNumber();
        }
        if (in_array('sourceCategory', $keys)) {
            $expression['source_category'] = $task->getSourceCategory();
        }
        if (in_array('sourceTemplate', $keys)) {
            $expression['source_template_id'] = $task->getSourceTemplate();
        }
        if (in_array('targetCategory', $keys)) {
            $expression['target_category'] = $task->getTargetCategory();
        }
        if (in_array('targetTemplate', $keys)) {
            $expression['target_template_id'] = $task->getTargetTemplate();
        }
        if (in_array('targetRule', $keys)) {
            $expression['target_rule_id'] = $task->getTargetRule();
        }
        if (in_array('scheduleTask', $keys)) {
            $expression['schedule_task_id'] = $task->getScheduleTask();
        }
        if (in_array('errorNumber', $keys)) {
            $expression['error_number'] = $task->getErrorNumber();
        }
        if (in_array('fileName', $keys)) {
            $expression['file_name'] = $task->getFileName();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $task->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $task->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $task->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $task->getStatusTime();
        }

        return $expression;
    }
}

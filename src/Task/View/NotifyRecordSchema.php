<?php
namespace BaseData\Task\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class NotifyRecordSchema extends SchemaProvider
{
    protected $resourceType = 'notifyRecords';

    public function getId($notifyRecord) : int
    {
        return $notifyRecord->getId();
    }

    public function getAttributes($notifyRecord) : array
    {
        return [
            'name' => $notifyRecord->getName(),
            'hash' => $notifyRecord->getHash(),
            'status' => $notifyRecord->getStatus(),
            'createTime' => $notifyRecord->getCreateTime(),
            'updateTime' => $notifyRecord->getUpdateTime(),
            'statusTime' => $notifyRecord->getStatusTime(),
        ];
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class BjSearchDataCache extends Cache
{
    const KEY = 'bj_search_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

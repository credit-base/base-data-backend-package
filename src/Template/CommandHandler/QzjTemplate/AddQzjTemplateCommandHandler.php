<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;

use BaseData\Template\Command\QzjTemplate\AddQzjTemplateCommand;
use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;

class AddQzjTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo, QzjTemplateCommandHandlerTrait;

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddQzjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $sourceUnit = $this->fetchUserGroup($command->sourceUnit);

        $qzjTemplate = $this->getQzjTemplate();

        $qzjTemplate->setSourceUnit($sourceUnit);
        $qzjTemplate->setName($command->name);
        $qzjTemplate->setIdentify($command->identify);
        $qzjTemplate->setCategory($command->category);
        $qzjTemplate->setSubjectCategory($command->subjectCategory);
        $qzjTemplate->setDimension($command->dimension);
        $qzjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $qzjTemplate->setInfoClassify($command->infoClassify);
        $qzjTemplate->setInfoCategory($command->infoCategory);
        $qzjTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $qzjTemplate->setItems($items);

        if ($qzjTemplate->add()) {
            $command->id = $qzjTemplate->getId();
            return true;
        }
        return false;
    }
}

<?php
namespace BaseData\Template\Adapter\QzjTemplate;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Model\NullQzjTemplate;
use BaseData\Template\Translator\QzjTemplateDbTranslator;
use BaseData\Template\Adapter\QzjTemplate\Query\QzjTemplateRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD)
 * @author chloroplast
 */
class QzjTemplateDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(QzjTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IQzjTemplateAdapter
     */
    public function testImplementsIQzjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 QzjTemplateDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockQzjTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Translator\QzjTemplateDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 QzjTemplateRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockQzjTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\QzjTemplate\Query\QzjTemplateRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockQzjTemplateDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedQzjTemplate = new QzjTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedQzjTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedQzjTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedQzjTemplate = new QzjTemplate();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedQzjTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedQzjTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedQzjTemplate = new QzjTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedQzjTemplate);

        //验证
        $qzjTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedQzjTemplate, $qzjTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $qzjTemplateOne = new QzjTemplate(1);
        $qzjTemplateTwo = new QzjTemplate(2);

        $ids = [1, 2];

        $expectedQzjTemplateList = [];
        $expectedQzjTemplateList[$qzjTemplateOne->getId()] = $qzjTemplateOne;
        $expectedQzjTemplateList[$qzjTemplateTwo->getId()] = $qzjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedQzjTemplateList);

        //验证
        $qzjTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedQzjTemplateList, $qzjTemplateList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 'name',
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';
        
        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterIdentify()
    {
        $filter = array(
            'identify' => 'identify',
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'identify = \''.$filter['identify'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-category
    public function testFormatFilterCategory()
    {
        $filter = array(
            'category' => QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ']
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'category = '.$filter['category'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterDimension()
    {
        $filter = array(
            'dimension' => QzjTemplate::DIMENSION['SHGK']
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'dimension = '.$filter['dimension'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoClassify()
    {
        $filter = array(
            'infoClassify' => 1
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'info_classify = '.$filter['infoClassify'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoCategory()
    {
        $filter = array(
            'infoCategory' => 1
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'info_category = '.$filter['infoCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterSourceUnit()
    {
        $filter = array(
            'sourceUnit' => 1
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'source_unit_id = '.$filter['sourceUnit'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterId()
    {
        $filter = array(
            'id' => 1
        );
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = 'qzj_template_id <> '.$filter['id'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY qzj_template_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockQzjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY qzj_template_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

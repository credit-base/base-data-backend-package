<?php
namespace BaseData\ResourceCatalogData\Controller;

use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;
use Marmot\Interfaces\IView;

class MockYsSearchDataFetchController extends YsSearchDataFetchController
{
    public function getRepository() : IYsSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

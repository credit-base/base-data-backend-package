<?php
namespace BaseData\Template\Adapter\WbjTemplate\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class WbjTemplateDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockWbjTemplateDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(WbjTemplateDb::TABLE, $this->database->getTable());
    }
}

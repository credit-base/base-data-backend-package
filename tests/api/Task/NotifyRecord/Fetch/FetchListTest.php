<?php
namespace BaseData\Task\NotifyRecord\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\NotifyRecord\SetDataApiTrait;
use BaseData\Task\NotifyRecord\NotifyRecordUtils;

/**
 * 测试上传记录接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, NotifyRecordUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_schedule_task_notify_record');
    }

    /**
     * 存在多条上传记录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_schedule_task_notify_record' => $this->notifyRecord()
            ]
        );
    }

    public function testViewNotifyRecord()
    {
        $setNotifyRecordList = $this->notifyRecord();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'notifyRecords/1,2',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $member) {
            $this->notifyRecordCompareData($setNotifyRecordList[$key], $member);
        }
    }
}

<?php
namespace BaseData\Template\CommandHandler\BjTemplate;

use BaseData\Template\Model\BjTemplate;

class MockAddBjTemplateCommandHandler extends AddBjTemplateCommandHandler
{
    public function getBjTemplate() : BjTemplate
    {
        return parent::getBjTemplate();
    }
}

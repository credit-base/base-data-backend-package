<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\WorkOrderTask\Model\ParentTask;

class ParentTaskMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new ParentTaskMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new ParentTask()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new ParentTask(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Model\ParentTask',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\WorkOrderTask\Model\ParentTask',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\WorkOrderTask\Model\ParentTask',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

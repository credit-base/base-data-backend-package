<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class RuleServiceCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\Rule\Command\RuleService\AddRuleServiceCommand' =>
        'BaseData\Rule\CommandHandler\RuleService\AddRuleServiceCommandHandler',
        'BaseData\Rule\Command\RuleService\EditRuleServiceCommand' =>
        'BaseData\Rule\CommandHandler\RuleService\EditRuleServiceCommandHandler',
        'BaseData\Rule\Command\RuleService\DeleteRuleServiceCommand' =>
        'BaseData\Rule\CommandHandler\RuleService\DeleteRuleServiceCommandHandler',
        'BaseData\Rule\Command\RuleService\RevokeRuleServiceCommand' =>
        'BaseData\Rule\CommandHandler\RuleService\RevokeRuleServiceCommandHandler',
        'BaseData\Rule\Command\RuleService\ResubmitRuleServiceCommand' =>
        'BaseData\Rule\CommandHandler\RuleService\ResubmitRuleServiceCommandHandler',
        'BaseData\Rule\Command\RuleService\ApproveRuleServiceCommand' =>
        'BaseData\Rule\CommandHandler\RuleService\ApproveRuleServiceCommandHandler',
        'BaseData\Rule\Command\RuleService\RejectRuleServiceCommand' =>
        'BaseData\Rule\CommandHandler\RuleService\RejectRuleServiceCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

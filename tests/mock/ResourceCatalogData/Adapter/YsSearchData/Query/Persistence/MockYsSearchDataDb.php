<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\Persistence;

class MockYsSearchDataDb extends YsSearchDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

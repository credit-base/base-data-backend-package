<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\GbItemsData;
use BaseData\ResourceCatalogData\Model\NullGbItemsData;
use BaseData\ResourceCatalogData\Translator\GbItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\GbItemsDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class GbItemsDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(GbItemsDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'fetchOneAction',
                                'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IGbItemsDataAdapter
     */
    public function testImplementsIGbItemsDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 GbItemsDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockGbItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\GbItemsDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 GbItemsDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockGbItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\GbItemsData\Query\GbItemsDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockGbItemsDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedGbItemsData = new GbItemsData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedGbItemsData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedGbItemsData);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedGbItemsData = new GbItemsData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedGbItemsData);

        //验证
        $gbItemsData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedGbItemsData, $gbItemsData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $gbItemsDataOne = new GbItemsData(1);
        $gbItemsDataTwo = new GbItemsData(2);

        $ids = [1, 2];

        $expectedGbItemsDataList = [];
        $expectedGbItemsDataList[$gbItemsDataOne->getId()] = $gbItemsDataOne;
        $expectedGbItemsDataList[$gbItemsDataTwo->getId()] = $gbItemsDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedGbItemsDataList);

        //验证
        $gbItemsDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedGbItemsDataList, $gbItemsDataList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockGbItemsDataDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockGbItemsDataDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }
}

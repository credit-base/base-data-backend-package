<?php
namespace BaseData\Rule\Utils;

use BaseData\Rule\Model\IRule;

trait RuleServiceUtils
{

    private function compareArrayAndObjectRuleService(
        array $expectedArray,
        $ruleService
    ) {
        $this->assertEquals($expectedArray['rule_id'], $ruleService->getId());
        
        $this->compareArrayAndObjectCommon($expectedArray, $ruleService);
    }

    private function compareArrayAndObjectsUnAuditedRuleService(
        array $expectedArray,
        $unAuditedRuleService
    ) {
        $this->assertEquals($expectedArray['rule_id'], $unAuditedRuleService->getApplyId());
        $this->assertEquals($expectedArray['publish_crew_id'], $unAuditedRuleService->getCrew()->getId());
    
        $this->compareArrayAndObjectCommon($expectedArray, $unAuditedRuleService);
    }

    private function compareArrayAndObjectUnAuditedRuleService(
        array $expectedArray,
        $unAuditedRuleService
    ) {
        $applyInfo = array();
        if (is_string($expectedArray['apply_info'])) {
            $applyInfo = unserialize(gzuncompress(base64_decode($expectedArray['apply_info'], true)));
        }
        if (is_array($expectedArray['apply_info'])) {
            $applyInfo = $expectedArray['apply_info'];
        }
        
        $this->assertEquals($applyInfo, $unAuditedRuleService->getApplyInfo());
        $this->assertEquals($expectedArray['apply_form_rule_id'], $unAuditedRuleService->getApplyId());
        $this->assertEquals($expectedArray['publish_crew_id'], $unAuditedRuleService->getPublishCrew()->getId());
        $this->assertEquals($expectedArray['apply_crew_id'], $unAuditedRuleService->getApplyCrew()->getId());
        $this->assertEquals($expectedArray['apply_usergroup_id'], $unAuditedRuleService->getApplyUserGroup()->getId());
        $this->assertEquals($expectedArray['operation_type'], $unAuditedRuleService->getOperationType());
        $this->assertEquals($expectedArray['relation_id'], $unAuditedRuleService->getRelationId());
        $this->assertEquals($expectedArray['reject_reason'], $unAuditedRuleService->getRejectReason());
        $this->assertEquals($expectedArray['apply_status'], $unAuditedRuleService->getApplyStatus());
    }

    private function compareArrayAndObjectCommon(
        array $expectedArray,
        $ruleService
    ) {
        $this->assertEquals(
            $expectedArray['transformation_template_id'],
            $ruleService->getTransformationTemplate()->getId()
        );
        $this->assertEquals(
            $expectedArray['source_template_id'],
            $ruleService->getSourceTemplate()->getId()
        );
        $expectedRules = array();
        if (is_string($expectedArray['rules'])) {
            $expectedRules = unserialize(gzuncompress(base64_decode($expectedArray['rules'], true)));
        }
        if (is_array($expectedArray['rules'])) {
            $expectedRules = $expectedArray['rules'];
        }

        $rules = $ruleService->getRules();
        foreach ($rules as $key => $rule) {
            if ($rule instanceof IRule) {
                $rules[$key] = $rule->getCondition();
            }
        }

        $this->assertEquals($expectedRules, $rules);
        $this->assertEquals($expectedArray['version'], $ruleService->getVersion());
        $this->assertEquals($expectedArray['data_total'], $ruleService->getDataTotal());
        $this->assertEquals($expectedArray['crew_id'], $ruleService->getCrew()->getId());
        $this->assertEquals($expectedArray['usergroup_id'], $ruleService->getUserGroup()->getId());
        $this->assertEquals($expectedArray['transformation_category'], $ruleService->getTransformationCategory());
        $this->assertEquals($expectedArray['source_category'], $ruleService->getSourceCategory());
        $this->assertEquals($expectedArray['status'], $ruleService->getStatus());
        $this->assertEquals($expectedArray['create_time'], $ruleService->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $ruleService->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $ruleService->getStatusTime());
    }
}

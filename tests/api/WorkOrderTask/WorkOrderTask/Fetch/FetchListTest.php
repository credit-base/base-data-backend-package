<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\WorkOrderTaskUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试工单任务接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, WorkOrderTaskUtils, TemplateSetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_work_order_task');
        $this->clear('pcore_bj_template');
    }

    /**
     * 存在多条工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_work_order_task' => $this->workOrderTask(),
                'pcore_parent_task' => $this->parentTask(),
                'pcore_bj_template' => $this->bjTemplate()
            ]
        );
    }

    public function testViewWorkOrderTask()
    {
        $setWorkOrderTaskList = $this->workOrderTask();

        $setTemplateList = $this->bjTemplate();
        foreach ($setTemplateList as $setTemplate) {
            $setTemplateList[$setTemplate['bj_template_id']] = $setTemplate;
        }
        $setParentTaskList = $this->parentTask();
        foreach ($setParentTaskList as $setParentTask) {
            $setParentTaskList[$setParentTask['parent_task_id']] = $setParentTask;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'workOrderTasks/1,2?include=template,parentTask',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $workOrderTask) {
            $this->workOrderTaskCompareData($setWorkOrderTaskList[$key], $workOrderTask);
        }

        $included = $contents['included'];
        foreach ($included as $key => $value) {
            if ($value['type'] == 'bjTemplates') {
                $this->assertEquals($setTemplateList[$value['id']]['bj_template_id'], $value['id']);
                $this->assertEquals('bjTemplates', $value['type']);

                $attributes = $value['attributes'];
                $this->assertEquals($setTemplateList[$value['id']]['name'], $attributes['name']);
            }
            if ($value['type'] == 'parentTasks') {
                $this->assertEquals($setParentTaskList[$value['id']]['parent_task_id'], $value['id']);
                $this->assertEquals('parentTasks', $value['type']);

                $attributes = $value['attributes'];
                $this->assertEquals($setParentTaskList[$value['id']]['template_type'], $attributes['templateType']);
                $this->assertEquals($setParentTaskList[$value['id']]['title'], $attributes['title']);
                $this->assertEquals($setParentTaskList[$value['id']]['end_time'], $attributes['endTime']);
            }
        }
    }
}

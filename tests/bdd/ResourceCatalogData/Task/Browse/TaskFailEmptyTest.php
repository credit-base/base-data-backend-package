<?php
namespace BaseData\ResourceCatalogData\Task\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\Repository\TaskRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的任务数据时,在资源目录数据子系统下的委办局数据中,可以查看到我的所有任务数据,
             通过列表形式查看我所有的任务数据,以便于我可以更快的了解资源目录数据的导入情况
 * @Scenario: 查看任务数据列表
 */
class TaskFailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
    }

    /**
     * @Given: 不存在任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看任务数据列表时
     */
    public function fetchTaskList()
    {
        $repository = new TaskRepository();

        list($taskList, $count) = $repository->filter([]);
        
        unset($count);

        return $taskList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewTask()
    {
        $taskList = $this->fetchTaskList();

        $this->assertEmpty($taskList);
    }
}

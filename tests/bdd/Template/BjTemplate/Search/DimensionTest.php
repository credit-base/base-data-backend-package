<?php
namespace BaseData\Template\BjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Translator\BjTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 查看公开范围为社会公开的数据
 */
class DimensionTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBjTemplateList()
    {
        $repository = new BjTemplateRepository();
        $filter['dimension'] = Template::DIMENSION['SHGK'];

        list($bjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $bjTemplateList;
    }

    /**
     * @Then: 我可以看到公开范围为社会公开的所有资源目录
     */
    public function testViewBjTemplateList()
    {
        $dimensionBjTemplateList = $this->bjTemplate();

        foreach ($dimensionBjTemplateList as $key => $dimensionBjTemplate) {
            $dimensionBjTemplateList[$key]['bj_template_id'] = $key +1;
            $dimensionBjTemplateList[$key]['subject_category'] = json_decode(
                $dimensionBjTemplate['subject_category'],
                true
            );
            $dimensionBjTemplateList[$key]['items'] = json_decode($dimensionBjTemplate['items'], true);
        }

        $bjTemplateList = $this->fetchBjTemplateList();
        $translator = new BjTemplateDbTranslator();
        foreach ($bjTemplateList as $bjTemplate) {
            $bjTemplateArray[] = $translator->objectToArray($bjTemplate);
        }

        $this->assertEquals($bjTemplateArray, $dimensionBjTemplateList);

        foreach ($bjTemplateArray as $bjTemplate) {
            $this->assertEquals(Template::DIMENSION['SHGK'], $bjTemplate['dimension']);
        }
    }
}

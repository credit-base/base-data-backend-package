<?php
namespace BaseData\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class NullProgressTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullProgressObject::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->trait);
    }

    public function testRevoke()
    {
        $reason = 'reason';
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->revoke($reason));
    }

    public function testConfirm()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->confirm());
    }

    public function testEnd()
    {
        $reason = 'reason';
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->end($reason));
    }

    public function testFeedback()
    {
        $this->trait->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->trait->feedback());
    }

    public function testResourceNotExist()
    {
        $mockNullProgressObject = new MockNullProgressObject();
        $result = $mockNullProgressObject->publicResourceNotExist();

        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }
}

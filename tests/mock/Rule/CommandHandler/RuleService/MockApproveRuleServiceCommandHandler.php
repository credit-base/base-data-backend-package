<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use BaseData\Common\Model\IApproveAble;
use BaseData\Common\Command\ApproveCommand;

class MockApproveRuleServiceCommandHandler extends ApproveRuleServiceCommandHandler
{
    public function executeAction(ApproveCommand $command)
    {
        return parent::executeAction($command);
    }

    public function fetchIApplyObject($id) : IApproveAble
    {
        return parent::fetchIApplyObject($id);
    }
}

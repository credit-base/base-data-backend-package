<?php
namespace BaseData\Rule\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\NullRuleService;

class RuleServiceDbTranslator implements ITranslator
{
    use RuleServiceTranslatorTrait;
    
    public function arrayToObject(array $expression, $ruleService = null) : RuleService
    {
        if (!isset($expression['rule_id'])) {
            return NullRuleService::getInstance();
        }

        if ($ruleService == null) {
            $ruleService = new RuleService($expression['rule_id']);
        }

        $ruleService->setId($expression['rule_id']);

        $this->arrayToObjectUtils($ruleService, $expression);

        return $ruleService;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($ruleService, array $keys = array()) : array
    {
        if (!$ruleService instanceof RuleService) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'transformationTemplate',
                'sourceTemplate',
                'rules',
                'version',
                'dataTotal',
                'crew',
                'userGroup',
                'transformationCategory',
                'sourceCategory',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['rule_id'] = $ruleService->getId();
        }

        $expressionInfo = $this->objectToArrayUtils($ruleService, $keys);
        
        return array_merge($expression, $expressionInfo);
    }
}

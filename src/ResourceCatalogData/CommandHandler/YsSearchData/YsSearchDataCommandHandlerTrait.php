<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;

trait YsSearchDataCommandHandlerTrait
{
    private $repository;

    public function __construct()
    {
        $this->repository = new YsSearchDataRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : YsSearchDataRepository
    {
        return $this->repository;
    }

    protected function fetchYsSearchData(int $id) : YsSearchData
    {
        return $this->getRepository()->fetchOne($id);
    }
}

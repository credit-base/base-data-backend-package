<?php
namespace BaseData\Task\Adapter\NotifyRecord\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class NotifyRecordDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockNotifyRecordDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(NotifyRecordDb::TABLE, $this->database->getTable());
    }
}

<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\WbjTemplate;

class WbjTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $wbjTemplate = new WbjTemplateView(new WbjTemplate());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $wbjTemplate);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Utils;

use BaseData\ResourceCatalogData\Model\WbjSearchData;

use BaseData\Common\Model\IEnableAble;

class WbjSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateWbjSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WbjSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $wbjSearchData = new WbjSearchData($id);
        $wbjSearchData->setId($id);

        self::generateInfoClassify($wbjSearchData, $faker, $value);
        self::generateInfoCategory($wbjSearchData, $faker, $value);
        self::generateCrew($wbjSearchData, $faker, $value);
        self::generateSourceUnit($wbjSearchData, $faker, $value);
        self::generateSubjectCategory($wbjSearchData, $faker, $value);
        self::generateDimension($wbjSearchData, $faker, $value);
        self::generateExpirationDate($wbjSearchData, $faker, $value);
        self::generateStatus($wbjSearchData, $faker, $value);
        self::generateTemplate($wbjSearchData, $faker, $value);
        self::generateItemsData($wbjSearchData, $faker, $value);
        self::generateTask($wbjSearchData, $faker, $value);
        
        $wbjSearchData->setCreateTime($faker->unixTime());
        $wbjSearchData->setUpdateTime($faker->unixTime());
        $wbjSearchData->setStatusTime($faker->unixTime());

        return $wbjSearchData;
    }

    private static function generateTemplate($wbjSearchData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($faker->randomDigit());
        
        $wbjSearchData->setTemplate($template);
    }

    private static function generateItemsData($wbjSearchData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] : WbjItemsDataMockFactory::generateWbjItemsData($faker->randomDigit());
        
        $wbjSearchData->setItemsData($itemsData);
    }

    protected static function generateStatus($wbjSearchData, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                IEnableAble::STATUS
            );
        
        $wbjSearchData->setStatus($status);
    }
}

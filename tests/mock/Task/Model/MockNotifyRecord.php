<?php
namespace BaseData\Task\Model;

use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;

use BaseData\Task\Script\SyncTask;

class MockNotifyRecord extends NotifyRecord
{
    public function getRepository() : INotifyRecordAdapter
    {
        return parent::getRepository();
    }

    public function getSyncTask() : SyncTask
    {
        return parent::getSyncTask();
    }

    public function generateHash() : string
    {
        return parent::generateHash();
    }

    public function validate() : bool
    {
        return parent::validate();
    }

    public function validateFileName(string $fileName) : bool
    {
        return parent::validateFileName($fileName);
    }

    public function validateExtension(string $extension) : bool
    {
        return parent::validateExtension($extension);
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }
}

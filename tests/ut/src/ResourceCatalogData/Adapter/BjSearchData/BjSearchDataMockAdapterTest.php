<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\BjSearchData;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class BjSearchDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new BjSearchDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new BjSearchData()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new BjSearchData(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\BjSearchData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\BjSearchData',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\BjSearchData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData;

use BaseData\ResourceCatalogData\Model\ErrorData;

interface IErrorDataAdapter
{
    public function fetchOne($id) : ErrorData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(ErrorData $errorData) : bool;

    public function edit(ErrorData $errorData, array $keys = array()) : bool;
}

<?php
namespace BaseData\Rule\Adapter\RuleService;

use Marmot\Interfaces\INull;

use Marmot\Interfaces\ITranslator;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Model\NullRuleService;
use BaseData\Rule\Translator\RuleServiceDbTranslator;
use BaseData\Rule\Adapter\RuleService\Query\RuleServiceRowCacheQuery;

use BaseData\Rule\Adapter\RuleServiceAdapterTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceDbAdapter implements IRuleServiceAdapter
{
    use DbAdapterTrait, RuleServiceAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new RuleServiceDbTranslator();
        $this->rowCacheQuery = new RuleServiceRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : ITranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : RuleServiceRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullRuleService::getInstance();
    }

    public function add(RuleService $ruleService) : bool
    {
        return $this->addAction($ruleService);
    }

    public function edit(RuleService $ruleService, array $keys = array()) : bool
    {
        return $this->editAction($ruleService, $keys);
    }

    public function fetchOne($id) : RuleService
    {
        $ruleService = $this->fetchOneAction($id);
        $this->fetchSourceTemplate($ruleService);
        $this->fetchTransformationTemplate($ruleService);
        $this->fetchRules($ruleService);

        return $ruleService;
    }

    public function fetchList(array $ids) : array
    {
        $ruleServiceList = $this->fetchListAction($ids);
        $this->fetchSourceTemplate($ruleServiceList);
        $this->fetchTransformationTemplate($ruleServiceList);

        return $ruleServiceList;
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $ruleService = new RuleService();
            if (isset($filter['userGroup'])) {
                $ruleService->getUserGroup()->setId($filter['userGroup']);
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('userGroup'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceTemplate'])) {
                $ruleService->getSourceTemplate()->setId($filter['sourceTemplate']);
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('sourceTemplate'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['transformationTemplate'])) {
                $ruleService->getTransformationTemplate()->setId($filter['transformationTemplate']);
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('transformationTemplate'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['transformationCategory'])) {
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('transformationCategory'));
                $condition .= $conjection.key($info).' IN ('.$filter['transformationCategory'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['excludeTransformationCategory'])) {
                $ruleService->setTransformationCategory($filter['excludeTransformationCategory']);
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('transformationCategory'));
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceCategory'])) {
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('sourceCategory'));
                $condition .= $conjection.key($info).' IN ('.$filter['sourceCategory'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['status'])) {
                $ruleService->setStatus($filter['status']);
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('status'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['sourceTemplateIds'])) {
                $info = $this->getDbTranslator()->objectToArray(
                    $ruleService,
                    array('sourceTemplate')
                );
                $condition .= $conjection.key($info).' IN ('.$filter['sourceTemplateIds'].')';
                $conjection = ' AND ';
            }
            if (isset($filter['id'])) {
                $ruleService->setId($filter['id']);
                $info = $this->getDbTranslator()->objectToArray($ruleService, array('id'));
                $condition .= $conjection.key($info).' <> '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $ruleService = new RuleService();

            foreach ($sort as $key => $val) {
                if ($key == 'updateTime') {
                    $info = $this->getDbTranslator()->objectToArray($ruleService, array('updateTime'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
                if ($key == 'status') {
                    $info = $this->getDbTranslator()->objectToArray($ruleService, array('status'));
                    $condition .= $conjection.key($info).' '.($val == -1 ? 'DESC' : 'ASC');
                    $conjection = ',';
                }
            }
        }
        
        return $condition;
    }
}

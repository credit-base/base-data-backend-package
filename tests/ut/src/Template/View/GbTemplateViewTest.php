<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\GbTemplate;

class GbTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $gbTemplate = new GbTemplateView(new GbTemplate());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $gbTemplate);
    }
}

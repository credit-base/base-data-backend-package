<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullYsItemsDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullYsItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsYsItemsData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\YsItemsData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

<?php
namespace BaseData\Crew\View;

use PHPUnit\Framework\TestCase;

use BaseData\Crew\Model\Crew;

class CrewViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $crew = new CrewView(new Crew());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $crew);
    }
}

# 科室字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### name

**科室名称**

* string
* 2-15位汉字
* 必填项

### userGroup

**所属委办局** 

* UserGroup
* 必填项

### [状态](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
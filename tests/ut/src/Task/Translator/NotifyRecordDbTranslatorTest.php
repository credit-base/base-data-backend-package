<?php
namespace BaseData\Task\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Task\Utils\NotifyRecordUtils;

class NotifyRecordDbTranslatorTest extends TestCase
{
    use NotifyRecordUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new NotifyRecordDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Task\Model\NullNotifyRecord', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $task = \BaseData\Task\Utils\MockFactory::generateNotifyRecord(1);

        $expression['record_id'] = $task->getId();
        $expression['name'] = $task->getName();
        $expression['hash'] = $task->getHash();

        $expression['status'] = $task->getStatus();
        $expression['status_time'] = $task->getStatusTime();
        $expression['create_time'] = $task->getCreateTime();
        $expression['update_time'] = $task->getUpdateTime();

        $task = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Task\Model\NotifyRecord', $task);
        $this->compareArrayAndObject($expression, $task);
    }

    public function testObjectToArray()
    {
        $task = \BaseData\Task\Utils\MockFactory::generateNotifyRecord(1);

        $expression = $this->translator->objectToArray($task);

        $this->compareArrayAndObject($expression, $task);
    }
}

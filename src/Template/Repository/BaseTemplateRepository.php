<?php
namespace BaseData\Template\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Template\Model\BaseTemplate;
use BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;
use BaseData\Template\Adapter\BaseTemplate\BaseTemplateDbAdapter;
use BaseData\Template\Adapter\BaseTemplate\BaseTemplateMockAdapter;

class BaseTemplateRepository extends Repository implements IBaseTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BaseTemplateDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IBaseTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IBaseTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IBaseTemplateAdapter
    {
        return new BaseTemplateMockAdapter();
    }

    public function fetchOne($id) : BaseTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(BaseTemplate $baseTemplate) : bool
    {
        return $this->getAdapter()->add($baseTemplate);
    }

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($baseTemplate, $keys);
    }
}

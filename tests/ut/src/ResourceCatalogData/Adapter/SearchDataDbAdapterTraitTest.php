<?php
namespace BaseData\ResourceCatalogData\Adapter;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\Repository\WbjItemsDataRepository;

use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\Common\Model\IEnableAble;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class SearchDataDbAdapterTraitTest extends TestCase
{
    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    //fetchTemplate
    public function testFetchTemplateIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchTemplateByObject'
                                ]
                           )
                           ->getMock();
        
        $wbjSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))
                         ->method('fetchTemplateByObject')
                         ->with($wbjSearchData);

        $adapter->publicFetchTemplate($wbjSearchData);
    }

    public function testFetchTemplateIsArray()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchTemplateByList'
                                ]
                           )
                           ->getMock();
                           
        $wbjSearchDataOne = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(
            $this->faker->randomNumber(1)
        );

        $wbjSearchDataTwo = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(
            $this->faker->randomNumber(2)
        );

        $wbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $wbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchTemplateByList')
                         ->with($wbjSearchDataList);

        $adapter->publicFetchTemplate($wbjSearchDataList);
    }

    //fetchTemplateByObject
    public function testFetchTemplateByObject()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getTemplateRepository'
                                ]
                           )
                           ->getMock();

        $id = $wbjTemplateId = $this->faker->randomNumber();
        $wbjSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);
        $wbjTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($wbjTemplateId);
        $wbjSearchData->setTemplate($wbjTemplate);

        $userGroupRepository = $this->prophesize(WbjTemplateRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($wbjTemplateId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjTemplate);

        $adapter->expects($this->exactly(1))
                         ->method('getTemplateRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->publicFetchTemplateByObject($wbjSearchData);
    }

    //fetchTemplateByList
    public function testFetchTemplateByList()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getTemplateRepository'
                                ]
                           )
                           ->getMock();

        $id = $wbjTemplateId = $this->faker->randomNumber();
        $wbjSearchDataOne = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);
        $wbjTemplateOne = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($wbjTemplateId);
        $wbjSearchDataOne->setTemplate($wbjTemplateOne);

        $idTwo = $wbjTemplateIdTwo = $this->faker->randomNumber(2);
        $wbjSearchDataTwo = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($idTwo);
        $wbjTemplateTwo = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($wbjTemplateIdTwo);
        $wbjSearchDataTwo->setTemplate($wbjTemplateTwo);

        $wbjSearchDataTwo->setTemplate($wbjTemplateTwo);
        $wbjTemplateIds = [$wbjTemplateId, $wbjTemplateIdTwo];
        
        $wbjTemplateList[$wbjTemplateOne->getId()] = $wbjTemplateOne;
        $wbjTemplateList[$wbjTemplateTwo->getId()] = $wbjTemplateTwo;
        
        $wbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $wbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        $userGroupRepository = $this->prophesize(WbjTemplateRepository::class);
        $userGroupRepository->fetchList(Argument::exact($wbjTemplateIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjTemplateList);

        $adapter->expects($this->exactly(1))
                         ->method('getTemplateRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->publicFetchTemplateByList($wbjSearchDataList);
    }

    //fetchItemsData
    public function testFetchItemsDataIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchItemsDataByObject'
                                ]
                           )
                           ->getMock();
        
        $wbjSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(
            $this->faker->randomNumber()
        );

        $adapter->expects($this->exactly(1))
                         ->method('fetchItemsDataByObject')
                         ->with($wbjSearchData);

        $adapter->publicFetchItemsData($wbjSearchData);
    }

    public function testFetchItemsDataIsArray()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'fetchItemsDataByList'
                                ]
                           )
                           ->getMock();
                           
        $wbjSearchDataOne = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(
            $this->faker->randomNumber(1)
        );

        $wbjSearchDataTwo = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(
            $this->faker->randomNumber(2)
        );

        $wbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $wbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchItemsDataByList')
                         ->with($wbjSearchDataList);

        $adapter->publicFetchItemsData($wbjSearchDataList);
    }

    //fetchItemsDataByObject
    public function testFetchItemsDataByObject()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getItemsDataRepository'
                                ]
                           )
                           ->getMock();

        $id = $wbjItemsDataId = $this->faker->randomNumber();
        $wbjSearchData = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);
        $wbjItemsData = \BaseData\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(
            $wbjItemsDataId
        );
        $wbjSearchData->setItemsData($wbjItemsData);

        $userGroupRepository = $this->prophesize(WbjItemsDataRepository::class);
        $userGroupRepository->fetchOne(Argument::exact($wbjItemsDataId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjItemsData);

        $adapter->expects($this->exactly(1))
                         ->method('getItemsDataRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->publicFetchItemsDataByObject($wbjSearchData);
    }

    //fetchItemsDataByList
    public function testFetchItemsDataByList()
    {
        $adapter = $this->getMockBuilder(MockSearchDataDbAdapterTrait::class)
                           ->setMethods(
                               [
                                    'getItemsDataRepository'
                                ]
                           )
                           ->getMock();

        $id = $wbjItemsDataId = $this->faker->randomNumber();
        $wbjSearchDataOne = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($id);
        $wbjItemsDataOne = \BaseData\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(
            $wbjItemsDataId
        );
        $wbjSearchDataOne->setItemsData($wbjItemsDataOne);

        $idTwo = $wbjItemsDataIdTwo = $this->faker->randomNumber(2);
        $wbjSearchDataTwo = \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData($idTwo);
        $wbjItemsDataTwo = \BaseData\ResourceCatalogData\Utils\WbjItemsDataMockFactory::generateWbjItemsData(
            $wbjItemsDataIdTwo
        );
        $wbjSearchDataTwo->setItemsData($wbjItemsDataTwo);

        $wbjSearchDataTwo->setItemsData($wbjItemsDataTwo);
        $wbjItemsDataIds = [$wbjItemsDataId, $wbjItemsDataIdTwo];
        
        $wbjItemsDataList[$wbjItemsDataOne->getId()] = $wbjItemsDataOne;
        $wbjItemsDataList[$wbjItemsDataTwo->getId()] = $wbjItemsDataTwo;
        
        $wbjSearchDataList[$wbjSearchDataOne->getId()] = $wbjSearchDataOne;
        $wbjSearchDataList[$wbjSearchDataTwo->getId()] = $wbjSearchDataTwo;

        $userGroupRepository = $this->prophesize(WbjItemsDataRepository::class);
        $userGroupRepository->fetchList(Argument::exact($wbjItemsDataIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($wbjItemsDataList);

        $adapter->expects($this->exactly(1))
                         ->method('getItemsDataRepository')
                         ->willReturn($userGroupRepository->reveal());

        $adapter->publicFetchItemsDataByList($wbjSearchDataList);
    }

    public function testSearchDataFormatFilter()
    {
        $adapter = new MockSearchDataDbAdapterTrait();

        $filter = array();
        $searchData = new WbjSearchData();

        $expectedCondition = ' 1 ';
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/InfoClassify
    public function testSearchDataFormatFilterInfoClassify()
    {
        $filter = array(
            'infoClassify' => $this->faker->randomElement(
                ISearchDataAble::INFO_CLASSIFY
            )
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'info_classify = '.$filter['infoClassify'];

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/InfoCategory
    public function testSearchDataFormatFilterInfoCategory()
    {
        $filter = array(
            'infoCategory' => $this->faker->randomElement(
                ISearchDataAble::INFO_CATEGORY
            )
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'info_category = '.$filter['infoCategory'];

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/SubjectCategory
    public function testSearchDataFormatFilterSubjectCategory()
    {
        $filter = array(
            'subjectCategory' => $this->faker->randomElement(
                ISearchDataAble::SUBJECT_CATEGORY
            )
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'subject_category = '.$filter['subjectCategory'];

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/task
    public function testSearchDataFormatFilterTask()
    {
        $filter = array('task' => 1);
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'task_id = '.$filter['task'];

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/Dimension
    public function testSearchDataFormatFilterDimension()
    {
        $filter = array(
            'dimension' => $this->faker->randomElement(
                ISearchDataAble::DIMENSION
            )
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'dimension = '.$filter['dimension'];

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/Status
    public function testSearchDataFormatFilterStatus()
    {
        $filter = array(
            'status' => $this->faker->randomElement(
                IEnableAble::STATUS
            )
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'status IN ('.$filter['status'].')';

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/ExpirationDate
    public function testSearchDataFormatFilterExpirationDate()
    {
        $filter = array(
            'expirationDate' => $this->faker->unixTime()
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'expiration_date > '.$filter['expirationDate'];

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/name
    public function testSearchDataFormatFilterName()
    {
        $filter = array(
            'name' => $this->faker->name()
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';
        
        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/identify
    public function testSearchDataFormatFilterIdentify()
    {
        $filter = array(
            'identify' => $this->faker->creditCardNumber()
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'identify = \''.$filter['identify'].'\'';

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/sourceUnit
    public function testSearchDataFormatFilterSourceUnit()
    {
        $filter = array(
            'sourceUnit' => $this->faker->randomNumber()
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'usergroup_id = '.$filter['sourceUnit'];

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }

    //filter/hash
    public function testSearchDataFormatFilterHash()
    {
        $filter = array(
            'hash' => $this->faker->md5()
        );
        $adapter = new MockSearchDataDbAdapterTrait();
        $searchData = new WbjSearchData();

        $expectedCondition = 'hash = \''.$filter['hash'].'\'';

        //验证
        $condition = $adapter->publicSearchDataFormatFilter($filter, $searchData);
        $this->assertEquals($expectedCondition, $condition);
    }
}

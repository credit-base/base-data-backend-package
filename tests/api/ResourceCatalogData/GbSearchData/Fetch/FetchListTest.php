<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试国标资源目录接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_search_data');
        $this->clear('pcore_gb_items_data');
    }

    /**
     * 存在多条国标资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data'],
                'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data']
            ]
        );
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function testViewGbSearchData()
    {
        $setGbSearchDataList = $this->gbSearchData()['pcore_gb_search_data'];

        $setTemplateList = $this->gbTemplate();
        foreach ($setTemplateList as $setTemplate) {
            $setTemplateList[$setTemplate['gb_template_id']] = $setTemplate;
        }
        $setGbItemsDataList = $this->gbSearchData()['pcore_gb_items_data'];
        foreach ($setGbItemsDataList as $setGbItemsData) {
            $setGbItemsDataList[$setGbItemsData['gb_items_data_id']] = $setGbItemsData;
        }

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'gbSearchData/1,2?include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $gbSearchData) {
            $this->gbSearchDataCompareData($setGbSearchDataList[$key], $gbSearchData);
        }

        $included = $contents['included'];

        foreach ($included as $key => $value) {
            if ($value['type'] == 'gbItemsData') {
                $this->gbItemsDataCompareData($setGbItemsDataList[$value['id']], $value);
            }
            if ($value['type'] == 'gbTemplates') {
                $this->gbTemplateCompareData($setTemplateList[$value['id']], $value);
            }
        }
    }
}

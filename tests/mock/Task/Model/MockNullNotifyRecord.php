<?php
namespace BaseData\Task\Model;

class MockNullNotifyRecord extends NullNotifyRecord
{
    public function resourceNotExist() : bool
    {
        return parent::resourceNotExist();
    }
}

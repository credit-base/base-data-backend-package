<?php
namespace BaseData\Task\Model;

use PHPUnit\Framework\TestCase;

class RunTimeTest extends TestCase
{
    private $runTime;

    public function setUp()
    {
        $this->runTime = new RunTime();
    }

    public function tearDown()
    {
        unset($this->runTime);
    }

    public function testConstructor()
    {
        $this->assertGreaterThan(0, $this->runTime->getStartTime());
        $this->assertEquals(0, $this->runTime->getEndTime());
    }

    public function testUpdateEndTime()
    {
        $this->runTime->updateEndTime();
        $this->assertGreaterThan(0, $this->runTime->getEndTime());
    }
}

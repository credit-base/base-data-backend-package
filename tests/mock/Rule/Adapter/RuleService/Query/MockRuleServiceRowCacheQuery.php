<?php
namespace BaseData\Rule\Adapter\RuleService\Query;

use Marmot\Interfaces\CacheLayer;
use Marmot\Framework\Interfaces\DbLayer;

class MockRuleServiceRowCacheQuery extends RuleServiceRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

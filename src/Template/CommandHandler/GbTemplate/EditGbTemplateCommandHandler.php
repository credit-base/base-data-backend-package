<?php
namespace BaseData\Template\CommandHandler\GbTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;
use BaseData\Template\Command\GbTemplate\EditGbTemplateCommand;
use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseData\Template\Repository\GbTemplateRepository;

class EditGbTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo;
    
    private $repository;

    public function __construct()
    {
        $this->repository = new GbTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getGbTemplateRepository() : IGbTemplateAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof EditGbTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getGbTemplateRepository();
        $gbTemplate = $repository->fetchOne($command->id);

        $gbTemplate->setName($command->name);
        $gbTemplate->setIdentify($command->identify);
        $gbTemplate->setSubjectCategory($command->subjectCategory);
        $gbTemplate->setDimension($command->dimension);
        $gbTemplate->setExchangeFrequency($command->exchangeFrequency);
        $gbTemplate->setInfoClassify($command->infoClassify);
        $gbTemplate->setInfoCategory($command->infoCategory);
        $gbTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $gbTemplate->setItems($items);

        return $gbTemplate->edit();
    }
}

<?php
namespace BaseData\ResourceCatalogData\View;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Model\WbjSearchData;

class WbjSearchDataViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $wbjSearchData = new WbjSearchDataView(new WbjSearchData());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $wbjSearchData);
    }
}

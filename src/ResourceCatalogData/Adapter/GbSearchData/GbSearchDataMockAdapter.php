<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Utils\GbSearchDataMockFactory;

class GbSearchDataMockAdapter implements IGbSearchDataAdapter
{
    public function fetchOne($id) : GbSearchData
    {
        return GbSearchDataMockFactory::generateGbSearchData($id);
    }

    public function fetchList(array $ids) : array
    {
        $gbSearchDataList = array();

        foreach ($ids as $id) {
            $gbSearchDataList[$id] = GbSearchDataMockFactory::generateGbSearchData($id);
        }

        return $gbSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(GbSearchData $gbSearchData) : bool
    {
        unset($gbSearchData);
        return true;
    }

    public function edit(GbSearchData $gbSearchData, array $keys = array()) : bool
    {
        unset($gbSearchData);
        unset($keys);
        return true;
    }
}

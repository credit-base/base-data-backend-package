<?php
namespace BaseData\UserGroup\Utils;

trait UserGroupRestfulUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $userGroup
    ) {
        $this->assertEquals($expectedArray['data']['id'], $userGroup->getId());
        $this->assertEquals($expectedArray['data']['attributes']['name'], $userGroup->getName());
        $this->assertEquals($expectedArray['data']['attributes']['shortName'], $userGroup->getShortName());
        if (isset($expectedArray['data']['attributes']['createTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['createTime'], $userGroup->getCreateTime());
        }
        if (isset($expectedArray['data']['attributes']['updateTime'])) {
            $this->assertEquals($expectedArray['data']['attributes']['updateTime'], $userGroup->getUpdateTime());
        }
        if (isset($expectedArray['data']['attributes']['status'])) {
            $this->assertEquals($expectedArray['data']['attributes']['status'], $userGroup->getStatus());
        }
    }
}

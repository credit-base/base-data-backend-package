<?php
namespace BaseData\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class ErrorDataSchema extends SchemaProvider
{
    protected $resourceType = 'errorDatas';

    public function getId($errorData) : int
    {
        return $errorData->getId();
    }

    public function getAttributes($errorData) : array
    {
        return [
            'category' => $errorData->getCategory(),
            'itemsData' => $errorData->getItemsData()->getData(),
            'errorType' => $errorData->getErrorType(),
            'errorReason' => $errorData->getErrorReason(),
            
            'status' => $errorData->getStatus(),
            'createTime' => $errorData->getCreateTime(),
            'updateTime' => $errorData->getUpdateTime(),
            'statusTime' => $errorData->getStatusTime(),
        ];
    }

    public function getRelationships($errorData, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'task' => [self::DATA => $errorData->getTask()],
            'template' => [self::DATA => $errorData->getTemplate()]
        ];
    }
}

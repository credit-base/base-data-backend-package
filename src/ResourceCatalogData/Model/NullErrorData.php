<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Interfaces\INull;

use BaseData\Common\Model\NullOperateTrait;

class NullErrorData extends ErrorData implements INull
{
    use NullOperateTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}

<?php
namespace BaseData\Template\CommandHandler\QzjTemplate;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Repository\UserGroupRepository;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;

class MockQzjTemplateCommandHandlerTrait
{
    use QzjTemplateCommandHandlerTrait;

    public function publicGetUserGroupRepository() : UserGroupRepository
    {
        return $this->getUserGroupRepository();
    }

    public function publicFetchUserGroup(int $id) : UserGroup
    {
        return $this->fetchUserGroup($id);
    }

    public function publicGetQzjTemplateRepository() : QzjTemplateRepository
    {
        return $this->getQzjTemplateRepository();
    }

    public function publicFetchQzjTemplate(int $id) : QzjTemplate
    {
        return $this->fetchQzjTemplate($id);
    }

    public function publicGetQzjTemplate() : QzjTemplate
    {
        return $this->getQzjTemplate();
    }
}

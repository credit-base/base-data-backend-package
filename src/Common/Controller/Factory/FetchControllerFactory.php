<?php
namespace BaseData\Common\Controller\Factory;

use BaseData\Common\Controller\NullFetchController;
use Marmot\Framework\Common\Controller\IFetchController;

class FetchControllerFactory
{
    const MAPS = array(
        'userGroups'=>'BaseData\UserGroup\Controller\UserGroupFetchController',
        'gbTemplates'=>'BaseData\Template\Controller\GbTemplateFetchController',
        'bjTemplates'=>'BaseData\Template\Controller\BjTemplateFetchController',
        'baseTemplates'=>'BaseData\Template\Controller\BaseTemplateFetchController',
        'templates'=>'BaseData\Template\Controller\WbjTemplateFetchController',
        'qzjTemplates'=>'BaseData\Template\Controller\QzjTemplateFetchController',
        'parentTasks'=>'BaseData\WorkOrderTask\Controller\ParentTaskFetchController',
        'workOrderTasks'=>'BaseData\WorkOrderTask\Controller\WorkOrderTaskFetchController',
        'ysSearchData'=>'BaseData\ResourceCatalogData\Controller\YsSearchDataFetchController',
        'wbjSearchData'=>'BaseData\ResourceCatalogData\Controller\WbjSearchDataFetchController',
        'bjSearchData'=>'BaseData\ResourceCatalogData\Controller\BjSearchDataFetchController',
        'gbSearchData'=>'BaseData\ResourceCatalogData\Controller\GbSearchDataFetchController',
        'rules'=>'\BaseData\Rule\Controller\RuleServiceFetchController',
        'unAuditedRules'=>'\BaseData\Rule\Controller\UnAuditedRuleServiceFetchController',
        'tasks'=>'BaseData\ResourceCatalogData\Controller\TaskFetchController',
        'errorDatas'=>'BaseData\ResourceCatalogData\Controller\ErrorDataFetchController',
        'notifyRecords'=>'BaseData\Task\Controller\NotifyRecordFetchController',
        'enterprises'=>'BaseData\Enterprise\Controller\EnterpriseFetchController'
    );

    public static function getController(string $resource) : IFetchController
    {
        $controller = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($controller) ? new $controller : NullFetchController::getInstance();
    }
}

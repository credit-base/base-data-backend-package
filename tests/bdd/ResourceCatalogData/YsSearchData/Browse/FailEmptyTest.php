<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的原始数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据列表
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_ys_items_data');
        $this->clear('pcore_ys_search_data');
    }

    /**
     * @Given: 不存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchYsSearchDataList()
    {
        $repository = new YsSearchDataRepository();

        list($ysSearchDataList, $count) = $repository->filter([]);
        
        unset($count);

        return $ysSearchDataList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewYsSearchData()
    {
        $ysSearchDataList = $this->fetchYsSearchDataList();

        $this->assertEmpty($ysSearchDataList);
    }
}

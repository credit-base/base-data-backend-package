<?php
namespace BaseData\Template\CommandHandler\WbjTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;
use BaseData\Template\Command\WbjTemplate\AddWbjTemplateCommand;
use BaseData\Template\Model\WbjTemplate;
use BaseData\UserGroup\Model\UserGroup;

class AddWbjTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo;
    
    protected function getWbjTemplate() : WbjTemplate
    {
        return new WbjTemplate();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddWbjTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $wbjTemplate = $this->getWbjTemplate();

        $wbjTemplate->setSourceUnit(new UserGroup($command->sourceUnit));
        $wbjTemplate->setName($command->name);
        $wbjTemplate->setIdentify($command->identify);
        $wbjTemplate->setSubjectCategory($command->subjectCategory);
        $wbjTemplate->setDimension($command->dimension);
        $wbjTemplate->setExchangeFrequency($command->exchangeFrequency);
        $wbjTemplate->setInfoClassify($command->infoClassify);
        $wbjTemplate->setInfoCategory($command->infoCategory);
        $wbjTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $wbjTemplate->setItems($items);

        if ($wbjTemplate->add()) {
            $command->id = $wbjTemplate->getId();
            return true;
        }
        return false;
    }
}

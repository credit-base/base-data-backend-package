<?php
namespace BaseData\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseData\Template\View\GbTemplateView;
use BaseData\Template\Repository\GbTemplateRepository;

class GbTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new GbTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IGbTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new GbTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'gbTemplates';
    }
}

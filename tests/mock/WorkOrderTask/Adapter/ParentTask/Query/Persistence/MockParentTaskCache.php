<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask\Query\Persistence;

class MockParentTaskCache extends ParentTaskCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData;

use BaseData\ResourceCatalogData\Model\BjItemsData;
use BaseData\ResourceCatalogData\Utils\BjItemsDataMockFactory;

class BjItemsDataMockAdapter implements IBjItemsDataAdapter
{
    public function fetchOne($id) : BjItemsData
    {
        return BjItemsDataMockFactory::generateBjItemsData($id);
    }

    public function fetchList(array $ids) : array
    {
        $bjItemsDataList = array();

        foreach ($ids as $id) {
            $bjItemsDataList[$id] = BjItemsDataMockFactory::generateBjItemsData($id);
        }

        return $bjItemsDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(BjItemsData $bjItemsData) : bool
    {
        unset($bjItemsData);
        return true;
    }
}

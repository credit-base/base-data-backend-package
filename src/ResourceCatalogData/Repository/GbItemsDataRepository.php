<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\GbItemsData;
use BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\GbItemsData\GbItemsDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\GbItemsData\GbItemsDataMockAdapter;

class GbItemsDataRepository extends Repository implements IGbItemsDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new GbItemsDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IGbItemsDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IGbItemsDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IGbItemsDataAdapter
    {
        return new GbItemsDataMockAdapter();
    }

    public function fetchOne($id) : GbItemsData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(GbItemsData $gbItemsData) : bool
    {
        return $this->getAdapter()->add($gbItemsData);
    }
}

<?php
namespace BaseData\Template\Adapter\GbTemplate\Query\Persistence;

class MockGbTemplateCache extends GbTemplateCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

# 委办局字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### name

**委办局名称**

* string

### shortName

**委办局简称**

* string

### [状态](common.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)
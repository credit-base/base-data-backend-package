<?php
namespace BaseData\Enterprise\Utils;

trait EnterpriseUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $enterprise
    ) {
        $this->assertEquals($expectedArray['enterprise_id'], $enterprise->getId());
        $this->assertEquals($expectedArray['name'], $enterprise->getName());
        $this->assertEquals($expectedArray['unified_social_credit_code'], $enterprise->getUnifiedSocialCreditCode());
        $this->assertEquals($expectedArray['establishment_date'], $enterprise->getEstablishmentDate());
        $this->assertEquals($expectedArray['approval_date'], $enterprise->getApprovalDate());
        $this->assertEquals($expectedArray['address'], $enterprise->getAddress());
        $this->assertEquals($expectedArray['registration_capital'], $enterprise->getRegistrationCapital());
        $this->assertEquals($expectedArray['business_term_start'], $enterprise->getBusinessTermStart());
        $this->assertEquals($expectedArray['business_term_to'], $enterprise->getBusinessTermTo());
        $this->assertEquals($expectedArray['business_scope'], $enterprise->getBusinessScope());
        $this->assertEquals($expectedArray['registration_authority'], $enterprise->getRegistrationAuthority());
        $this->assertEquals($expectedArray['principal'], $enterprise->getPrincipal());
        $this->assertEquals($expectedArray['principal_card_id'], $enterprise->getPrincipalCardId());
        $this->assertEquals($expectedArray['registration_status'], $enterprise->getRegistrationStatus());
        $this->assertEquals($expectedArray['enterprise_type_code'], $enterprise->getEnterpriseTypeCode());
        $this->assertEquals($expectedArray['enterprise_type'], $enterprise->getEnterpriseType());

        $data = array();
        if (is_string($expectedArray['data'])) {
            $data = unserialize(gzuncompress(base64_decode($expectedArray['data'], true)));
        }
        if (is_array($expectedArray['data'])) {
            $data = $expectedArray['data'];
        }
        $this->assertEquals($data, $enterprise->getData());
        $this->assertEquals($expectedArray['industry_category'], $enterprise->getIndustryCategory());
        $this->assertEquals($expectedArray['industry_code'], $enterprise->getIndustryCode());
        $this->assertEquals($expectedArray['administrative_area'], $enterprise->getAdministrativeArea());
        $this->assertEquals($expectedArray['hash'], $enterprise->getHash());
        $this->assertEquals($expectedArray['task_id'], $enterprise->getTask()->getId());
        $this->assertEquals($expectedArray['status'], $enterprise->getStatus());
        $this->assertEquals($expectedArray['create_time'], $enterprise->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $enterprise->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $enterprise->getStatusTime());
    }
}

<?php
namespace BaseData\Template\GbTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\GbTemplate;

/**
 * 测试国标资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    private $gbTemplate;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->gbTemplate);
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在一条国标资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate()
            ]
        );
    }

    public function testViewGbTemplate()
    {
        $setGbTemplate = $this->gbTemplate()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'gbTemplates/1',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->gbTemplateCompareData($setGbTemplate, $contents['data']);
    }
}

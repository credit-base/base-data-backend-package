<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\Template;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\FailureData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

use BaseData\ResourceCatalogData\Strategy\CharacterStrategy;
use BaseData\ResourceCatalogData\Strategy\DataStrategyFactory;

/**
 * BaseData\Rule\Model\FormatValidationRule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class FormatValidationRuleTest extends TestCase
{
    private $rule;

    private $sourceSearchData;

    private $transformationSearchData;

    private $initializationData;

    private $data;

    private $items;

    private $itemData;

    private $itemsData;

    public function setUp()
    {
        $this->sourceSearchData =
            \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(1);
        $this->transformationSearchData =
            \BaseData\ResourceCatalogData\Utils\WbjSearchDataMockFactory::generateWbjSearchData(2);

        $this->sourceItemsData = array(
            'ZTMC' => '陕西有限公司'
        );
        
        $this->items = array(
            'ZTMC' => array(
                "name" => '主体名称',    //信息项名称
                "identify" => 'ZTMC',    //数据标识
                "type" => 1,    //数据类型
                "length" => '255',    //数据长度
                "options" => array(),    //可选范围
                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                "maskRule" => array(),    //脱敏规则
                "remarks" => '信用主体名称',    //备注
            )
        );

        $condition = $this->items;

        $this->initializationData = array(
            'condition' => $condition,
            'sourceItemsData' => $this->sourceSearchData->getItemsData()->getData(),
        );

        $this->itemData = array(
            "value" => '陕西有限公司',
            "name" => '主体名称',    //信息项名称
            "identify" => 'ZTMC',    //数据标识
            "type" => 1,    //数据类型
            "length" => '255',    //数据长度
            "options" => array(),    //可选范围
            "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
            "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
            "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
            "maskRule" => array(),    //脱敏规则
            "remarks" => '信用主体名称',    //备注
        );

        $this->itemsData = array(
            'ZTMC' => $this->itemData
        );

        $this->data = array(
            'failureItems' => $this->itemsData,
            'transformationItemsData' => $this->sourceSearchData->getItemsData()->getData()
        );

        $this->rule = new MockFormatValidationRule();
    }

    public function tearDown()
    {
        unset($this->sourceSearchData);
        unset($this->transformationSearchData);
        unset($this->initializationData);
        unset($this->data);
        unset($this->itemData);
        unset($this->itemsData);
        unset($this->rule);
    }

    public function testExtendsRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\Rule", $this->rule);
    }

    public function testImplementsIRule()
    {
        $this->assertInstanceof("BaseData\Rule\Model\IRule", $this->rule);
    }

    public function testGetDataStrategyFactory()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Strategy\DataStrategyFactory',
            $this->rule->getDataStrategyFactory()
        );
    }

    public function testValidate()
    {
        $result = $this->rule->validate();

        $this->assertTrue($result);
    }

    public function testValidateTransformation()
    {
        $result = $this->rule->validateTransformation($this->transformationSearchData);

        $this->assertTrue($result);
    }

    public function testTransformationInitialization()
    {
        $rule = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'getCondition'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('getCondition')
             ->willReturn($this->initializationData['condition']);

        $result = $rule->transformationInitialization($this->sourceSearchData, $this->transformationSearchData);

        $this->assertEquals($result, $this->initializationData);
    }

    public function testValidateInitializationDataFormat()
    {
        $result = $this->rule->validateInitializationDataFormat($this->initializationData);

        $this->assertTrue($result);
    }

    public function testTransformationExecuteFailure()
    {
        $rule = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'validateInitializationDataFormat'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(false);

        $result = $rule->transformationExecute($this->initializationData, $this->transformationSearchData);

        $this->assertEquals($result, array());
    }

    public function testTransformationExecuteItemsDataFormatFailure()
    {
        $rule = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'validateInitializationDataFormat',
                'combinationItemsData',
                'validateItemsDataFormat'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(true);

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(array('itemsData'));

        $rule->expects($this->exactly(1))
             ->method('validateItemsDataFormat')
             ->willReturn(false);

        $result = $rule->transformationExecute($this->initializationData, $this->transformationSearchData);

        $this->assertEquals($result, array());
    }

    public function testTransformationExecute()
    {
        $rule = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'validateInitializationDataFormat',
                'combinationItemsData',
                'validateItemsDataFormat',
                'isNecessaryItemEmpty',
                'validateItemDataFormat'
            ])->getMock();

        $rule->expects($this->exactly(1))
             ->method('validateInitializationDataFormat')
             ->willReturn(true);

        $initializationData = $this->initializationData;
    
        $rule->expects($this->exactly(1))
             ->method('combinationItemsData')
             ->with(
                 $initializationData['condition'],
                 $initializationData['sourceItemsData']
             )->willReturn($this->itemsData);

        $rule->expects($this->exactly(1))
             ->method('validateItemsDataFormat')
             ->willReturn(true);

        $rule->expects($this->exactly(1))
             ->method('isNecessaryItemEmpty')
             ->willReturn(false);

        $rule->expects($this->exactly(1))
             ->method('validateItemDataFormat')
             ->willReturn(false);

        $result = $rule->transformationExecute($this->initializationData, $this->transformationSearchData);

        $this->assertEquals($result, $this->data);
    }

    public function testCombinationItemsData()
    {
        $result = $this->rule->combinationItemsData($this->items, $this->sourceItemsData);

        $this->assertEquals($result, $this->itemsData);
    }

    public function testValidateItemsDataFormat()
    {
        $result = $this->rule->validateItemsDataFormat($this->itemsData);

        $this->assertTrue($result);
    }

    public function testValidateItemsDataFormatFalse()
    {
        $itemsData = array('itemsData');
        $result = $this->rule->validateItemsDataFormat($itemsData);

        $this->assertFalse($result);
    }

    public function testIsNecessaryItemEmpty()
    {
        $itemsData = array(
            'isNecessary' => Template::IS_NECESSARY['SHI'],
            'value' => ''
        );

        $result = $this->rule->isNecessaryItemEmpty($itemsData);

        $this->assertTrue($result);
    }

    public function testValidateItemDataFormat()
    {
        $rule = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'getDataStrategyFactory'
            ])->getMock();

        $factory = $this->prophesize(DataStrategyFactory::class);
        $strategy = $this->prophesize(CharacterStrategy::class);

        $strategy->validate(
            Argument::exact($this->itemData['value']),
            Argument::exact($this->itemData)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $factory->getStrategy(
            Argument::exact($this->itemData['type'])
        )->shouldBeCalledTimes(1)->willReturn($strategy->reveal());

        $rule->expects($this->once())->method('getDataStrategyFactory')->willReturn($factory->reveal());

        $result = $rule->validateItemDataFormat($this->itemData);

        $this->assertTrue($result);
    }

    public function testValidateDataFormat()
    {
        $result = $this->rule->validateDataFormat($this->data);

        $this->assertTrue($result);
    }

    public function testTransformationResultFailure()
    {
        $stub = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'validateDataFormat',
                'failureDataException'
            ])->getMock();

        $data = array();
        $failureData = new FailureData();
        $stub->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(false);
             
        $stub->expects($this->exactly(1))
             ->method('failureDataException')
             ->with(
                 $failureData,
                 ErrorData::STATUS['PROGRAM_EXCEPTION']
             )
             ->willReturn($failureData);
             
         $result = $stub->transformationResult($data, $failureData);

        $this->assertEquals($result, $failureData);
    }
    
    public function testTransformationResult()
    {
        $stub = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'validateDataFormat',
                'transformationSearchData'
            ])->getMock();
            
        $stub->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(true);
             
        $data = $this->data;
        $data['failureItems'] = array();

        $transformationSearchData = $this->transformationSearchData;
        $stub->expects($this->exactly(1))
             ->method('transformationSearchData')
             ->with($transformationSearchData)
             ->willReturn($transformationSearchData);

        $transformationSearchData->getItemsData()->setData($data['transformationItemsData']);
        $hash = md5(base64_encode(gzcompress(serialize($data['transformationItemsData']))));
        $transformationSearchData->setHash($hash);
             
        $result = $stub->transformationResult($data, $this->transformationSearchData);

        $this->assertEquals($result, $transformationSearchData);
    }
    
    public function testTransformationResultFailureData()
    {
        $rule = $this->getMockBuilder(MockFormatValidationRule::class)
            ->setMethods([
                'validateDataFormat',
                'failureData'
            ])->getMock();
            
        $failureData = new FailureData();
          
        $rule->expects($this->exactly(1))
             ->method('validateDataFormat')
             ->willReturn(true);
             
        $rule->expects($this->exactly(1))
             ->method('failureData')
             ->with(
                 $this->transformationSearchData,
                 ErrorData::ERROR_TYPE['FORMAT_VALIDATION_FAILED'],
                 $this->data['failureItems']
             )->willReturn($failureData);
             
        $failureData->getItemsData()->setData($this->data['transformationItemsData']);
        $hash = md5(base64_encode(gzcompress(serialize($this->data['transformationItemsData']))));
        $failureData->setHash($hash);
             
        $result = $rule->transformationResult($this->data, $this->transformationSearchData);

        $this->assertEquals($result, $failureData);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\CommandBus;

use BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;
use BaseData\ResourceCatalogData\Command\YsSearchData\EnableYsSearchDataCommand;
use BaseData\ResourceCatalogData\Command\YsSearchData\DisableYsSearchDataCommand;

class YsSearchDataEnableControllerTest extends TestCase
{
    private $ysStub;

    public function setUp()
    {
        $this->ysStub = $this->getMockBuilder(MockYsSearchDataEnableController::class)
        ->setMethods([
            'displayError',
            ])
        ->getMock();
    }

    public function teatDown()
    {
        unset($this->ysStub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\YsSearchDataRepository',
            $this->ysStub->getRepository()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->ysStub->getCommandBus()
        );
    }

    private function initialDisable($result)
    {
        $this->ysStub = $this->getMockBuilder(MockYsSearchDataEnableController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new DisableYsSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->ysStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testDisableFailure()
    {
        $command = $this->initialDisable(false);

        $this->ysStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->ysStub->disable($command->id);
        $this->assertFalse($result);
    }

    public function testDisableSuccess()
    {
        $command = $this->initialDisable(true);

        $crew = YsSearchDataMockFactory::generateYsSearchData($command->id);

        $repository = $this->prophesize(YsSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);
        $this->ysStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->ysStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->ysStub->disable($command->id);
        $this->assertTrue($result);
    }

    private function initialEnable($result)
    {
        $this->ysStub = $this->getMockBuilder(MockYsSearchDataEnableController::class)
                    ->setMethods([
                        'getCommandBus',
                        'getRepository',
                        'render',
                        'displayError'
                    ])->getMock();

        $id = 1;
        $command = new EnableYsSearchDataCommand($id);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact($command))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($result);
        $this->ysStub->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());

        return $command;
    }

    public function testEnableFailure()
    {
        $command = $this->initialEnable(false);

        $this->ysStub->expects($this->exactly(1))
             ->method('displayError');

        $result = $this->ysStub->enable($command->id);
        $this->assertFalse($result);
    }

    public function testEnableSuccess()
    {
        $command = $this->initialEnable(true);

        $crew = YsSearchDataMockFactory::generateYsSearchData($command->id);

        $repository = $this->prophesize(YsSearchDataRepository::class);
        $repository->fetchOne(Argument::exact($command->id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($crew);

        $this->ysStub->expects($this->exactly(1))
             ->method('getRepository')
             ->willReturn($repository->reveal());
             
        $this->ysStub->expects($this->exactly(1))
        ->method('render');

        $result = $this->ysStub->enable($command->id);
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Template\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Utils\WbjTemplateUtils;
use BaseData\Template\Utils\WbjTemplateMockFactory;

class WbjTemplateDbTranslatorTest extends TestCase
{
    use WbjTemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new WbjTemplateDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Template\Model\NullWbjTemplate', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $wbjTemplate = WbjTemplateMockFactory::generateWbjTemplate(1);

        $expression['wbj_template_id'] = $wbjTemplate->getId();
        $expression['name'] = $wbjTemplate->getName();
        $expression['identify'] = $wbjTemplate->getIdentify();
        $expression['subject_category'] = json_encode($wbjTemplate->getSubjectCategory());
        $expression['dimension'] = $wbjTemplate->getDimension();
        $expression['exchange_frequency'] = $wbjTemplate->getExchangeFrequency();
        $expression['info_classify'] = $wbjTemplate->getInfoClassify();
        $expression['info_category'] = $wbjTemplate->getInfoCategory();
        $expression['description'] = $wbjTemplate->getDescription();
        $expression['items'] = json_encode($wbjTemplate->getItems());
        $expression['source_unit_id'] = $wbjTemplate->getSourceUnit()->getId();

        $expression['status'] = $wbjTemplate->getStatus();
        $expression['status_time'] = $wbjTemplate->getStatusTime();
        $expression['create_time'] = $wbjTemplate->getCreateTime();
        $expression['update_time'] = $wbjTemplate->getUpdateTime();

        $wbjTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Template\Model\WbjTemplate', $wbjTemplate);
        $this->compareArrayAndObject($expression, $wbjTemplate);
    }

    public function testObjectToArray()
    {
        $wbjTemplate = WbjTemplateMockFactory::generateWbjTemplate(1);

        $expression = $this->translator->objectToArray($wbjTemplate);

        $this->compareArrayAndObject($expression, $wbjTemplate);
    }
}

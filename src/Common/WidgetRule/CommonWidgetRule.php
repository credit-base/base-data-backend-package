<?php
namespace BaseData\Common\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

use BaseData\Common\Model\IEnableAble;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class CommonWidgetRule
{
    public function formatNumeric($data, string $pointer = '') : bool
    {
        if (!is_numeric($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    public function formatString($data, string $pointer = '') : bool
    {
        if (!is_string($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function formatArray($data, string $pointer = '') : bool
    {
        if (!is_array($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const REASON_MIN_LENGTH = 1;
    const REASON_MAX_LENGTH = 2000;
    public function reason($reason) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::REASON_MIN_LENGTH,
            self::REASON_MAX_LENGTH
        )->validate($reason)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'reason'));
            return false;
        }
        return true;
    }

    public function pdf($attachment, $pointer = 'attachment') : bool
    {
        if (empty($attachment) || !V::arrayType()->validate($attachment)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        if (!isset($attachment['identify'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        if (!V::extension('pdf')->validate($attachment['identify'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const NAME_MIN_LENGTH = 2;
    const NAME_MAX_LENGTH = 20;

    public function name($name) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'name'));
            return false;
        }

        return true;
    }

    const TITLE_MIN_LENGTH = 5;
    const TITLE_MAX_LENGTH = 80;

    public function title($title) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::TITLE_MIN_LENGTH,
            self::TITLE_MAX_LENGTH
        )->validate($title)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'title'));
            return false;
        }

        return true;
    }

    const SOURCE_MIN_LENGTH = 2;
    const SOURCE_MAX_LENGTH = 15;

    public function source($source) : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::SOURCE_MIN_LENGTH,
            self::SOURCE_MAX_LENGTH
        )->validate($source)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'source'));
            return false;
        }

        return true;
    }

    public function image($image, $pointer = 'image') : bool
    {
        if (!V::arrayType()->validate($image)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (!isset($image['identify']) || !$this->validateImageExtension($image['identify'])) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }

    public function images($images, $pointer = 'image') : bool
    {
        if (!V::arrayType()->validate($images)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        foreach ($images as $image) {
            if (!$this->validateImageExtension($image['identify'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
                return false;
            }
        }
        return true;
    }

    private function validateImageExtension($image) : bool
    {
        if (!V::extension('png')->validate($image)
            && !V::extension('jpg')->validate($image)
            && !V::extension('jpeg')->validate($image)) {
            return false;
        }

        return true;
    }

    const ATTACHMENTS_MAX_COUNT = 5;
    public function attachments($attachments, $pointer = 'attachments') : bool
    {
        if (!V::arrayType()->validate($attachments)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        if (count($attachments) > self::ATTACHMENTS_MAX_COUNT) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        foreach ($attachments as $attachment) {
            if (!$this->validateAttachmentExtension($attachment['identify'])) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
                return false;
            }
        }

        return true;
    }

    private function validateAttachmentExtension(string $attachment) : bool
    {
        if (!V::extension('zip')->validate($attachment)
            && !V::extension('doc')->validate($attachment)
            && !V::extension('docx')->validate($attachment)
            && !V::extension('xls')->validate($attachment)
            && !V::extension('xlsx')->validate($attachment)
            && !V::extension('pdf')->validate($attachment)
            && !V::extension('rar')->validate($attachment)
            && !V::extension('ppt')->validate($attachment)
        ) {
            return false;
        }

        return true;
    }

    public function status($status) : bool
    {
        if (!V::numeric()->validate($status) || !in_array($status, IEnableAble::STATUS)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'status'));
            return false;
        }

        return true;
    }
    
    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 200;

    public function description($description, $pointer = 'description') : bool
    {
        if (!V::charset('UTF-8')->stringType()->length(
            self::DESCRIPTION_MIN_LENGTH,
            self::DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>$pointer));
            return false;
        }

        return true;
    }
}

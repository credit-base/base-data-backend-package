<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\ErrorData;

class ErrorDataMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new ErrorDataMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new ErrorData()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new ErrorData(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\ErrorData',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\ErrorData',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\ResourceCatalogData\Model\ErrorData',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

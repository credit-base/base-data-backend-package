<?php
namespace BaseData\Rule\Command\RuleService;

use PHPUnit\Framework\TestCase;

class DeleteRuleServiceCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'crew' => $faker->randomDigit(),
            'id' => $faker->randomDigit()
        );

        $this->command = new DeleteRuleServiceCommand(
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }

    public function tearDown()
    {
        unset($this->command);
    }
    
    public function testImplementsCommand()
    {
        $this->assertInstanceOf('Marmot\Interfaces\ICommand', $this->command);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}

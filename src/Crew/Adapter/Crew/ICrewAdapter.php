<?php
namespace BaseData\Crew\Adapter\Crew;

use BaseData\Crew\Model\Crew;

interface ICrewAdapter
{
    public function fetchOne(int $id) : Crew;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;
}

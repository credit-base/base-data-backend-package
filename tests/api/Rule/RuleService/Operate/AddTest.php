<?php
namespace BaseData\Rule\RuleService\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\RuleUtils;
use BaseData\Rule\Model\RuleService;
use BaseData\Rule\SetDataApiTrait;

use BaseData\Template\Model\Template;
use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试规则接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, RuleUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_wbj_template');
    }

    /**
     * 不存在规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_gb_template' => $this->gbTemplate(),
            ]
        );
    }

    public function testViewRuleService()
    {
        $setRuleService = $this->applyFormRule()[0];
        $data = array(
            'data' => array(
                "type"=>"rules",
                "attributes"=>array(
                    "rules"=>array(
                        'transformationRule' => array(
                            "ZTMC" => "ZTMC",
                            "TYSHXYDM" => "TYSHXYDM",
                            "ZTLB" => "ZTLB",
                            "GKFW" => "GKFW",
                            "GXPL" => "GXPL",
                            "XXFL" => "XXFL",
                            "XXLB" => "XXLB",
                        ),
                    ),
                    "transformationCategory" => Template::CATEGORY['GB'],
                    "sourceCategory" => Template::CATEGORY['WBJ']
                ),
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    ),
                    "transformationTemplate"=>array(
                        "data"=>array(
                            array("type"=>"gbTemplates","id"=>1)
                        )
                    ),
                    "sourceTemplate"=>array(
                        "data"=>array(
                            array("type"=>"templates","id"=>1)
                        )
                    ),
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'rules',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->unAuditedRuleCompareData($setRuleService, $contents['data']);
    }
}

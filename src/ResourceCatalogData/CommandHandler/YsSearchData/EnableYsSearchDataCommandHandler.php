<?php
namespace BaseData\ResourceCatalogData\CommandHandler\YsSearchData;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\CommandHandler\EnableCommandHandler;

class EnableYsSearchDataCommandHandler extends EnableCommandHandler
{
    use YsSearchDataCommandHandlerTrait;
    
    protected function fetchIEnableObject($id) : IEnableAble
    {
        return $this->fetchYsSearchData($id);
    }
}

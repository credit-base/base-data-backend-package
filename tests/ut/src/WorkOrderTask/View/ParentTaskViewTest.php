<?php
namespace BaseData\WorkOrderTask\View;

use PHPUnit\Framework\TestCase;

use BaseData\WorkOrderTask\Model\ParentTask;

class ParentTaskViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $parentTask = new ParentTaskView(new ParentTask());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $parentTask);
    }
}

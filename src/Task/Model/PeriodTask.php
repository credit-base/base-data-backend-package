<?php
namespace BaseData\Task\Model;

use Marmot\Core;

abstract class PeriodTask extends Task
{
    public function __construct()
    {
        parent::__construct();
        $this->type = Task::TYPE['PERIOD'];
    }

    abstract public function getPeriod() : int;

    public function updateSchedule() : bool
    {
        $this->setStatus(Task::STATUS['PENDING']);
        $this->setStatusTime(Core::$container->get('time'));

        $nextScheduleTime = $this->getPeriod() + Core::$container->get('time');
        $this->setNextScheduleTime($nextScheduleTime);

        return $this->getRepository()->edit(
            $this,
            array(
                'nextScheduleTime',
                'status',
                'statusTime'
            )
        );
    }

    protected function afterExecute() : bool
    {
        return $this->updateSchedule();
    }
}

<?php
namespace BaseData\Rule\Command\RuleService;

use BaseData\Common\Command\EditApproveCommand;

class EditRuleServiceCommand extends EditApproveCommand
{
    public $rules;

    public $relationId;
    
    public function __construct(
        array $rules,
        int $relationId,
        int $crew,
        int $id = 0
    ) {
        parent::__construct(
            $crew,
            $id
        );
        
        $this->rules = $rules;
        $this->relationId = $relationId;
    }
}

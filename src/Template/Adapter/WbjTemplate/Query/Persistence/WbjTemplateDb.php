<?php
namespace BaseData\Template\Adapter\WbjTemplate\Query\Persistence;

use Marmot\Framework\Classes\Db;

class WbjTemplateDb extends Db
{
    const TABLE = 'wbj_template';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

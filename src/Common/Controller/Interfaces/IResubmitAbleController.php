<?php
namespace BaseData\Common\Controller\Interfaces;

interface IResubmitAbleController
{
    public function resubmit(int $id);
}

<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataDbAdapter;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\GbSearchDataMockAdapter;

class GbSearchDataRepository extends Repository implements IGbSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new GbSearchDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IGbSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IGbSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IGbSearchDataAdapter
    {
        return new GbSearchDataMockAdapter();
    }

    public function fetchOne($id) : GbSearchData
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(GbSearchData $gbSearchData) : bool
    {
        return $this->getAdapter()->add($gbSearchData);
    }

    public function edit(GbSearchData $gbSearchData, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($gbSearchData, $keys);
    }
}

<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Add;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\Repository\GbTemplateRepository;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\GbItemsData;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;

/**
 * @Feature: 我是开发人员,当委办局工作人员上传资源目录数据后,在资源目录数据子系统下的国标数据管理,开发定时任务新增国标数据
 *           根据委办局数据库的资源目录数据通过国标规则验证新增国标数据
 *           以便于我实现资源目录数据的报送和平台资源目录数据的呈现
 * @Scenario: 异常流程-数据重复,新增失败
 */
class FailRepeatTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_items_data');
        $this->clear('pcore_gb_search_data');
    }

    /**
     * @Given: 我已经新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data'],
            'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data']
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回false
     */
    public function add()
    {
        $templateRepository = new GbTemplateRepository();
        $gbTemplate = $templateRepository->fetchOne(1);

        $gbItemsDataArray = $this->gbSearchData()['pcore_gb_items_data'][0];
        $gbSearchDataArray = $this->gbSearchData()['pcore_gb_search_data'][0];

        $gbItemsData = new GbItemsData();
        $gbItemsData->setData(unserialize(gzuncompress(base64_decode($gbItemsDataArray['data'], true))));

        $gbSearchData = new GbSearchData();

        $gbSearchData->getCrew()->setId($gbSearchDataArray['crew_id']);
        $gbSearchData->getSourceUnit()->setId($gbSearchDataArray['usergroup_id']);
        $gbSearchData->setSubjectCategory($gbSearchDataArray['subject_category']);
        $gbSearchData->setDimension($gbSearchDataArray['dimension']);
        $gbSearchData->setExpirationDate($gbSearchDataArray['expiration_date']);
        $gbSearchData->setTemplate($gbTemplate);
        $gbSearchData->setItemsData($gbItemsData);
        $gbSearchData->setHash($gbSearchDataArray['hash']);
        
        return $gbSearchData->add();
    }

    /**
     * @Then: 数据已经存在
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertFalse($result);
    }
}

<?php
namespace BaseData\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class EnableControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new EnableControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getController('');
            $this->assertInstanceOf(
                'BaseData\Common\Controller\NullEnableController',
                $controller
            );
    }

    public function testGetController()
    {
        foreach (EnableControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getController($key)
            );
        }
    }
}

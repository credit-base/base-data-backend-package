<?php
namespace BaseData\Common\Controller\Factory;

use BaseData\Common\Controller\NullOperateController;
use Marmot\Framework\Common\Controller\IOperateController;

class OperateControllerFactory
{
    const MAPS = array(
        'gbTemplates'=>'\BaseData\Template\Controller\GbTemplateOperateController',
        'bjTemplates'=>'\BaseData\Template\Controller\BjTemplateOperateController',
        'baseTemplates'=>'BaseData\Template\Controller\BaseTemplateOperateController',
        'templates'=>'\BaseData\Template\Controller\WbjTemplateOperateController',
        'qzjTemplates'=>'BaseData\Template\Controller\QzjTemplateOperateController',
        'rules'=>'\BaseData\Rule\Controller\RuleServiceOperateController'
    );

    public static function getController(string $resource) : IOperateController
    {
        $controller = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($controller) ? new $controller : NullOperateController::getInstance();
    }
}

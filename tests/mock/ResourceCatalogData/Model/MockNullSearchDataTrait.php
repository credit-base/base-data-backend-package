<?php
namespace BaseData\ResourceCatalogData\Model;

class MockNullSearchDataTrait
{
    use NullSearchDataTrait;

    public function publicResourceNotExist() : bool
    {
        return $this->resourceNotExist();
    }

    protected function resourceNotExist() : bool
    {
        return false;
    }
}

<?php
namespace BaseData\WorkOrderTask\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

class NullParentTaskTest extends TestCase
{
    private $parentTask;

    public function setUp()
    {
        $this->parentTask = $this->getMockBuilder(NullParentTask::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->parentTask);
    }

    public function testExtendsParentTask()
    {
        $this->assertInstanceof('BaseData\WorkOrderTask\Model\ParentTask', $this->parentTask);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->parentTask);
    }

    public function testAdd()
    {
        $this->parentTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->parentTask->add());
    }

    public function testRevoke()
    {
        $reason = 'reason';
        $this->parentTask->expects($this->once())
             ->method('resourceNotExist')
             ->willReturn(false);

        $this->assertFalse($this->parentTask->revoke($reason));
    }

    public function testResourceNotExist()
    {
        $mockNullParentTask = new MockNullParentTask();
        $result = $mockNullParentTask->publicResourceNotExist();

        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }
}

<?php
namespace BaseData\WorkOrderTask\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\WorkOrderTask\WidgetRule\ParentTaskWidgetRule;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;

class MockParentTaskControllerTrait extends Controller
{
    use ParentTaskControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getParentTaskWidgetRulePublic() : ParentTaskWidgetRule
    {
        return $this->getParentTaskWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : ParentTaskRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}

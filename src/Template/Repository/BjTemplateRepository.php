<?php
namespace BaseData\Template\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseData\Template\Adapter\BjTemplate\BjTemplateDbAdapter;
use BaseData\Template\Adapter\BjTemplate\BjTemplateMockAdapter;

class BjTemplateRepository extends Repository implements IBjTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BjTemplateDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IBjTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IBjTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IBjTemplateAdapter
    {
        return new BjTemplateMockAdapter();
    }

    public function fetchOne($id) : BjTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(BjTemplate $bjTemplate) : bool
    {
        return $this->getAdapter()->add($bjTemplate);
    }

    public function edit(BjTemplate $bjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($bjTemplate, $keys);
    }
}

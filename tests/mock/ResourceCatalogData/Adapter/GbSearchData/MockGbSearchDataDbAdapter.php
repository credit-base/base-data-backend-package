<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData;

use Marmot\Interfaces\INull;

use BaseData\ResourceCatalogData\Repository\GbItemsDataRepository;
use BaseData\ResourceCatalogData\Translator\GbSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\GbSearchDataRowCacheQuery;

use BaseData\Template\Repository\GbTemplateRepository;

class MockGbSearchDataDbAdapter extends GbSearchDataDbAdapter
{
    public function getDbTranslator() : GbSearchDataDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : GbSearchDataRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function getItemsDataRepository() : GbItemsDataRepository
    {
        return parent::getItemsDataRepository();
    }

    public function getTemplateRepository() : GbTemplateRepository
    {
        return parent::getTemplateRepository();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }
}

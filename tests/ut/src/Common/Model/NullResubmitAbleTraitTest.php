<?php
namespace BaseData\Common\Model;

use PHPUnit\Framework\TestCase;

class NullResubmitAbleTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockNullResubmitAbleTrait::class)
                            ->setMethods(['resourceNotExist'])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testResubmit()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->Resubmit();
        $this->assertFalse($result);
    }

    public function testResubmitAction()
    {
        $this->mockResourceNotExist();

        $result = $this->trait->ResubmitAction();
        $this->assertFalse($result);
    }

    private function mockResourceNotExist()
    {
        $this->trait->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);
    }
}

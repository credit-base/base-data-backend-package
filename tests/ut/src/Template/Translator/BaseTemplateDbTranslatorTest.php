<?php
namespace BaseData\Template\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Utils\BaseTemplateUtils;
use BaseData\Template\Utils\BaseTemplateMockFactory;

class BaseTemplateDbTranslatorTest extends TestCase
{
    use BaseTemplateUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BaseTemplateDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\Template\Model\NullBaseTemplate', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $baseTemplate = BaseTemplateMockFactory::generateBaseTemplate(1);

        $expression['base_template_id'] = $baseTemplate->getId();
        $expression['name'] = $baseTemplate->getName();
        $expression['identify'] = $baseTemplate->getIdentify();
        $expression['subject_category'] = json_encode($baseTemplate->getSubjectCategory());
        $expression['dimension'] = $baseTemplate->getDimension();
        $expression['exchange_frequency'] = $baseTemplate->getExchangeFrequency();
        $expression['info_classify'] = $baseTemplate->getInfoClassify();
        $expression['info_category'] = $baseTemplate->getInfoCategory();
        $expression['description'] = $baseTemplate->getDescription();
        $expression['items'] = json_encode($baseTemplate->getItems());

        $expression['status'] = $baseTemplate->getStatus();
        $expression['status_time'] = $baseTemplate->getStatusTime();
        $expression['create_time'] = $baseTemplate->getCreateTime();
        $expression['update_time'] = $baseTemplate->getUpdateTime();

        $baseTemplate = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Template\Model\BaseTemplate', $baseTemplate);
        $this->compareArrayAndObject($expression, $baseTemplate);
    }

    public function testObjectToArray()
    {
        $baseTemplate = BaseTemplateMockFactory::generateBaseTemplate(1);

        $expression = $this->translator->objectToArray($baseTemplate);

        $this->compareArrayAndObject($expression, $baseTemplate);
    }
}

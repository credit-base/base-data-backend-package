<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;

class MockWbjItemsData extends WbjItemsData
{
    public function getRepository() : IWbjItemsDataAdapter
    {
        return parent::getRepository();
    }
}

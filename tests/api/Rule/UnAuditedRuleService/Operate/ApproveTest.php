<?php
namespace BaseData\Rule\UnAuditedRuleService\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Common\Model\IApproveAble;

use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\Model\UnAuditedRuleService;

use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试规则接口审核通过
 */
class ApproveTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_rule');
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在一条规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_apply_form_rule' => $this->applyFormRule(),
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_wbj_template' => $this->wbjTemplate()
            ]
        );
    }

    public function testViewUnAuditedRuleService()
    {
        $data = array(
            'data' => array(
                "type"=>"unAuditedRules",
                "relationships"=>array(
                    "applyCrew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'PATCH',
            'unAuditedRules/1/approve',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(IApproveAble::APPLY_STATUS['APPROVE'], $attributes['applyStatus']);

        $relationships = $contents['data']['relationships'];
        $relationshipsCrew = $relationships['applyCrew']['data'];
        $this->assertEquals('crews', $relationshipsCrew['type']);
        $this->assertEquals(1, $relationshipsCrew['id']);
    }
}

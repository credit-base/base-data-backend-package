<?php
namespace BaseData\Enterprise\Utils;

use BaseData\Common\Model\IEnableAble;

use BaseData\Enterprise\Model\Enterprise;

use BaseData\ResourceCatalogData\Model\Task;

class MockFactory
{
    public static function generateEnterprise(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Enterprise {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $enterprise = new Enterprise($id);
        $enterprise->setId($id);

        self::generateName($enterprise, $faker, $value);
        self::generateUnifiedSocialCreditCode($enterprise, $faker, $value);
        self::generateEstablishmentDate($enterprise, $faker, $value);
        self::generateApprovalDate($enterprise, $faker, $value);
        self::generateAddress($enterprise, $faker, $value);
        self::generateRegistrationCapital($enterprise, $faker, $value);
        self::generateBusinessTermStart($enterprise, $faker, $value);
        self::generateBusinessTermTo($enterprise, $faker, $value);
        self::generateBusinessScope($enterprise, $faker, $value);
        self::generateRegistrationAuthority($enterprise, $faker, $value);
        self::generatePrincipal($enterprise, $faker, $value);
        self::generatePrincipalCardId($enterprise, $faker, $value);
        self::generateRegistrationStatus($enterprise, $faker, $value);
        self::generateEnterpriseTypeCode($enterprise, $faker, $value);
        self::generateEnterpriseType($enterprise, $faker, $value);
        self::generateData($enterprise, $faker, $value);
        self::generateIndustryCategory($enterprise, $faker, $value);
        self::generateIndustryCode($enterprise, $faker, $value);
        self::generateAdministrativeArea($enterprise, $faker, $value);
        self::generateStatus($enterprise, $faker, $value);
        self::generateTask($enterprise, $faker, $value);

        $enterprise->setCreateTime($faker->unixTime());
        $enterprise->setUpdateTime($faker->unixTime());
        $enterprise->setStatusTime($faker->unixTime());

        return $enterprise;
    }

    private static function generateName($enterprise, $faker, $value) : void
    {
        $name = isset($value['name'])
        ? $value['name']
        : $faker->company();
        $enterprise->setName($name);
    }

    private static function generateUnifiedSocialCreditCode($enterprise, $faker, $value) : void
    {
        $unifiedSocialCreditCode = isset($value['unifiedSocialCreditCode'])
        ? $value['unifiedSocialCreditCode']
        : $faker->bothify('##############????');
        $enterprise->setUnifiedSocialCreditCode($unifiedSocialCreditCode);
    }

    private static function generateEstablishmentDate($enterprise, $faker, $value) : void
    {
        $establishmentDate = isset($value['establishmentDate'])
        ? $value['establishmentDate']
        : $faker->date('ymd');
        $enterprise->setEstablishmentDate($establishmentDate);
    }

    private static function generateApprovalDate($enterprise, $faker, $value) : void
    {
        $approvalDate = isset($value['approvalDate'])
        ? $value['approvalDate']
        : $faker->date('ymd');
        $enterprise->setApprovalDate($approvalDate);
    }

    private static function generateAddress($enterprise, $faker, $value) : void
    {
        $address = isset($value['address'])
        ? $value['address']
        : $faker->address();
        $enterprise->setAddress($address);
    }

    private static function generateRegistrationCapital($enterprise, $faker, $value) : void
    {
        $registrationCapital = isset($value['registrationCapital'])
        ? $value['registrationCapital']
        : $faker->randomFloat();
        $enterprise->setRegistrationCapital($registrationCapital);
    }

    private static function generateBusinessTermStart($enterprise, $faker, $value) : void
    {
        $businessTermStart = isset($value['businessTermStart'])
        ? $value['businessTermStart']
        : $faker->date('ymd');
        $enterprise->setBusinessTermStart($businessTermStart);
    }

    private static function generateBusinessTermTo($enterprise, $faker, $value) : void
    {
        $businessTermTo = isset($value['businessTermTo'])
        ? $value['businessTermTo']
        : $faker->date('ymd');
        $enterprise->setBusinessTermTo($businessTermTo);
    }

    private static function generateBusinessScope($enterprise, $faker, $value) : void
    {
        $businessScope = isset($value['businessScope'])
        ? $value['businessScope']
        : $faker->text(200);
        $enterprise->setBusinessScope($businessScope);
    }

    private static function generateRegistrationAuthority($enterprise, $faker, $value) : void
    {
        $registrationAuthority = isset($value['registrationAuthority'])
        ? $value['registrationAuthority']
        : $faker->company();
        $enterprise->setRegistrationAuthority($registrationAuthority);
    }

    private static function generatePrincipal($enterprise, $faker, $value) : void
    {
        $principal = isset($value['principal'])
        ? $value['principal']
        : $faker->name();
        $enterprise->setPrincipal($principal);
    }

    private static function generatePrincipalCardId($enterprise, $faker, $value) : void
    {
        $principalCardId = isset($value['principalCardId'])
        ? $value['principalCardId']
        : $faker->creditCardNumber();
        $enterprise->setPrincipalCardId($principalCardId);
    }

    private static function generateRegistrationStatus($enterprise, $faker, $value) : void
    {
        $registrationStatus = isset($value['registrationStatus'])
        ? $value['registrationStatus']
        : $faker->randomElement(
            array('存续(在营、开业、在册)', '吊销，未注销', '吊 销，已注销', '注销', '撤销', '迁出')
        );
        $enterprise->setRegistrationStatus($registrationStatus);
    }

    private static function generateEnterpriseTypeCode($enterprise, $faker, $value) : void
    {
        $enterpriseTypeCode = isset($value['enterpriseTypeCode'])
        ? $value['enterpriseTypeCode']
        : $faker->numberBetween(100, 1000);
        $enterprise->setEnterpriseTypeCode($enterpriseTypeCode);
    }

    private static function generateEnterpriseType($enterprise, $faker, $value) : void
    {
        $enterpriseType = isset($value['enterpriseType'])
        ? $value['enterpriseType']
        : $faker->randomElement(array('个体工商户', '内资企业', '国有企业', '集体企业', '股份合作企业', '联营企业'));
        $enterprise->setEnterpriseType($enterpriseType);
    }

    private static function generateData($enterprise, $faker, $value) : void
    {
        $data = isset($value['data'])
        ? $value['data']
        : $faker->words(3, false);
        $enterprise->setData($data);
    }

    private static function generateIndustryCategory($enterprise, $faker, $value) : void
    {
        $industryCategory = isset($value['industryCategory'])
        ? $value['industryCategory']
        : $faker->randomElement(array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'));
        $enterprise->setIndustryCategory($industryCategory);
    }
    
    private static function generateIndustryCode($enterprise, $faker, $value) : void
    {
        $industryCode = isset($value['industryCode'])
        ? $value['industryCode']
        : $faker->numberBetween(0000, 9999);
        $enterprise->setIndustryCode($industryCode);
    }

    private static function generateAdministrativeArea($enterprise, $faker, $value) : void
    {
        $administrativeArea = isset($value['administrativeArea'])
        ? $value['administrativeArea']
        : $faker->numberBetween(0000, 9999);
        $enterprise->setAdministrativeArea($administrativeArea);
    }

    protected static function generateStatus($enterprise, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                Enterprise::STATUS
            );
        
        $enterprise->setStatus($status);
    }

    protected static function generateTask($enterprise, $faker, $value) : void
    {
        $task = isset($value['task']) ?
        $value['task'] :
        new Task($faker->randomDigit());
        
        $enterprise->setTask($task);
    }
}

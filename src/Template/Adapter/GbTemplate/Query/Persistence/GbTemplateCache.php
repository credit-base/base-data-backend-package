<?php
namespace BaseData\Template\Adapter\GbTemplate\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class GbTemplateCache extends Cache
{
    const KEY = 'gb_template';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

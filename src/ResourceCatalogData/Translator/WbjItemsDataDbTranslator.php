<?php
namespace BaseData\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\WbjItemsData;
use BaseData\ResourceCatalogData\Model\NullWbjItemsData;

class WbjItemsDataDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $wbjItemsData = null) : WbjItemsData
    {
        if (!isset($expression['wbj_items_data_id'])) {
            return NullWbjItemsData::getInstance();
        }

        if ($wbjItemsData == null) {
            $wbjItemsData = new WbjItemsData();
        }

        $wbjItemsData->setId($expression['wbj_items_data_id']);

        if (isset($expression['data'])) {
            $wbjItemsData->setData(unserialize(gzuncompress(base64_decode($expression['data'], true))));
        }

        return $wbjItemsData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($wbjItemsData, array $keys = array()) : array
    {
        if (!$wbjItemsData instanceof WbjItemsData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'data'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['wbj_items_data_id'] = $wbjItemsData->getId();
        }
        if (in_array('data', $keys)) {
            $expression['data'] = base64_encode(gzcompress(serialize($wbjItemsData->getData())));
        }

        return $expression;
    }
}

# 前置机资源目录控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 前置机资源目录控件规范

* `NAME`: 目录名称控件规范.
* `IDENTIFY`: 目录标识控件规范.
* `SUBJECT_CATEGORY`: 主体类别控件规范.
* `DIMENSION`: 公开范围控件规范.
* `EXCHANGE_FREQUENCY`: 更新频率控件规范.
* `INFO_CLASSIFY`: 信息分类控件规范.
* `INFO_CATEGORY`: 信息类别控件规范.
* `CATEGORY`: 目录类别控件规范.
* `DESCRIPTION`: 目录描述控件规范.
* `ITEMS`: 模板信息控件规范.
* `SOURCE_UNIT`: 来源委办局控件规范.

### `NAME`
* [目录名称控件规范(NAME)](template.md)

### `IDENTIFY`
* [目录标识控件规范(IDENTIFY)](template.md)

### `SUBJECT_CATEGORY`
* [主体类别控件规范(SUBJECT_CATEGORY)](template.md)

### `DIMENSION`
* [公开范围控件规范(DIMENSION)](template.md)

### `EXCHANGE_FREQUENCY`
* [更新频率控件规范(CREW)](template.md)

### `INFO_CLASSIFY`
* [信息分类控件规范(INFO_CLASSIFY)](template.md)

### `INFO_CATEGORY`
* [信息类别控件规范(INFO_CATEGORY)](template.md)

### `CATEGORY`

**目录类别** 用于表述目录类别

#### 规则

* int, 内容必须在以下范围内

     * QZJ_WBJ | 前置机委办局 20
     * QZJ_BJ | 前置机本级 21
     * QZJ_GB | 前置机国标 22

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `DESCRIPTION`
* [目录描述控件规范(SUBTASK)](template.md)

### `ITEMS`
* [模板信息控件规范(SUBTASK)](template.md)

### `SOURCE_UNIT`
* [来源委办局控件规范(CREW)](template.md)

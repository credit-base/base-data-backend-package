<?php
namespace BaseData\Task\Adapter\Task;

use Marmot\Interfaces\INull;

use BaseData\Task\Translator\TaskDbTranslator;
use BaseData\Task\Adapter\Task\Query\TaskRowQuery;

class MockTaskDbAdapter extends TaskDbAdapter
{
    public function getDbTranslator() : TaskDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : TaskRowQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function formatGroup(array $group) : string
    {
        return parent::formatGroup($group);
    }

    public function schedulParentTasks() : array
    {
        return parent::schedulParentTasks();
    }

    public function scheduleSubTaks() : array
    {
        return parent::scheduleSubTaks();
    }
}

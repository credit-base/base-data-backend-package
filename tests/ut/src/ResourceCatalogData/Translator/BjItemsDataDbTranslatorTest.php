<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\BjItemsDataUtils;
use BaseData\ResourceCatalogData\Utils\BjItemsDataMockFactory;

class BjItemsDataDbTranslatorTest extends TestCase
{
    use BjItemsDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new BjItemsDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullBjItemsData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $bjItemsData = BjItemsDataMockFactory::generateBjItemsData(1);

        $expression['bj_items_data_id'] = $bjItemsData->getId();

        $expression['data'] = base64_encode(gzcompress(serialize($bjItemsData->getData())));

        $bjItemsData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\BjItemsData', $bjItemsData);
        $this->compareArrayAndObject($expression, $bjItemsData);
    }

    public function testObjectToArray()
    {
        $bjItemsData = BjItemsDataMockFactory::generateBjItemsData(1);

        $expression = $this->translator->objectToArray($bjItemsData);

        $this->compareArrayAndObject($expression, $bjItemsData);
    }
}

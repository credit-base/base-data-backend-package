<?php
namespace BaseData\Rule\Command\RuleService;

use PHPUnit\Framework\TestCase;

class EditRuleServiceCommandTest extends TestCase
{
    private $fakerData = array();
    
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->fakerData = array(
            'rules' => ['rules'],
            'relationId' => $faker->randomDigit(),
            'crew' => $faker->randomDigit(),
            'id' => $faker->randomDigit(),
        );

        $this->command = new EditRuleServiceCommand(
            $this->fakerData['rules'],
            $this->fakerData['relationId'],
            $this->fakerData['crew'],
            $this->fakerData['id']
        );
    }

    public function tearDown()
    {
        unset($this->command);
    }
    
    public function testExtendsEditApproveCommand()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Command\EditApproveCommand',
            $this->command
        );
    }

    public function testRulesParameter()
    {
        $this->assertEquals($this->fakerData['rules'], $this->command->rules);
    }

    public function testRelationIdParameter()
    {
        $this->assertEquals($this->fakerData['relationId'], $this->command->relationId);
    }

    public function testCrewParameter()
    {
        $this->assertEquals($this->fakerData['crew'], $this->command->crew);
    }

    public function testIdParameter()
    {
        $this->assertEquals($this->fakerData['id'], $this->command->id);
    }
}

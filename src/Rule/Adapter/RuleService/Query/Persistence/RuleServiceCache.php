<?php
namespace BaseData\Rule\Adapter\RuleService\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class RuleServiceCache extends Cache
{
    const KEY = 'rule';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class WorkOrderTaskControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockWorkOrderTaskControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetWorkOrderTaskWidgetRule()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule',
            $this->trait->getWorkOrderTaskWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'BaseData\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }
}

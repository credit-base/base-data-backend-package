<?php
namespace BaseData\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Model\NullWbjTemplate;
use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\View\WbjTemplateView;

class WbjTemplateFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WbjTemplateFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockWbjTemplateFetchController();

        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockWbjTemplateFetchController();

        $this->assertInstanceOf(
            'BaseData\Template\View\WbjTemplateView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockWbjTemplateFetchController();

        $this->assertEquals(
            'templates',
            $controller->getResourceName()
        );
    }
}

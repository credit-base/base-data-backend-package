<?php
namespace BaseData\Template\Adapter\QzjTemplate;

use BaseData\Template\Model\QzjTemplate;

interface IQzjTemplateAdapter
{
    public function fetchOne($id) : QzjTemplate;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(QzjTemplate $qzjTemplate) : bool;

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool;
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class WbjSearchDataCache extends Cache
{
    const KEY = 'wbj_search_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

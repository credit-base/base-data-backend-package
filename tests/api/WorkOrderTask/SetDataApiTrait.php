<?php
namespace BaseData\WorkOrderTask;

use Marmot\Core;

use BaseData\Template\Model\Template;

use BaseData\WorkOrderTask\Model\IProgress;
use BaseData\WorkOrderTask\Model\ParentTask;

trait SetDataApiTrait
{
    protected function parentTask() : array
    {
        return
        [
            [
                'parent_task_id' => 1,
                'template_type' => ParentTask::TEMPLATE_TYPE['BJ'],
                'title' => '归集企业基本信息',
                'description' => '依据国家要求，现需要按要求归集企业基本信息',
                'end_time' => '2022-10-01',
                'attachment' => json_encode(array('name'=>'附件名称', 'identify'=>'依据附件.pdf')),
                'template_id' => 1,
                'template_name' => '本级企业基本信息',
                'assign_objects' => json_encode(array(1), true),
                'finish_count' => 0,
                'reason' => '',

                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ],
            [
                'parent_task_id' => 2,
                'template_type' => ParentTask::TEMPLATE_TYPE['BJ'],
                'title' => '归集本级行政许可信息',
                'description' => '依据国家要求，现需要按要求归集本级行政许可信息',
                'end_time' => '2022-10-01',
                'attachment' => json_encode(array('name'=>'国203号文', 'identify'=>'依据附件.pdf')),
                'template_id' => 2,
                'template_name' => '本级行政许可信息',
                'assign_objects' => json_encode(array(1), true),
                'finish_count' => 0,
                'reason' => '撤销原因',

                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function workOrderTask(int $status = IProgress::STATUS['DQR'])  : array
    {
        $feedbackRecords = [
            [
                "crew" => 1,
                "items" => [
                    [
                        "name" => "主体名称",
                        "identify" => "ZTMC",
                        "type" => 1,
                        "length" => 200,
                        "options" => [],
                        "remarks" => "主体名称",
                        "isMasked" => 0,
                        "maskRule" => [],
                        "dimension" => 1,
                        "isNecessary" => 1
                    ],
                    [
                        "name" => "统一社会信用代码",
                        "identify" => "TYSHXYDM",
                        "type" => 1,
                        "length" => 20,
                        "options" => [],
                        "remarks" => "统一社会信用代码",
                        "isMasked" => 0,
                        "maskRule" => [],
                        "dimension" => 1,
                        "isNecessary" => 1
                    ],
                ],
                "reason" => "测试反馈信息",
                "crewName" => "张科",
                "userGroup" => 1,
                "templateId" => 0,
                "feedbackTime" => Core::$container->get('time'),
                "userGroupName" => "发展和改革委员会",
                "isExistedTemplate" => "0"
            ]
        ];

        return
        [
            [
                'work_order_task_id' => 1,
                'title' => '归集企业基本信息',
                'end_time' => '2022-10-01',
                'reason' => '测试反馈信息',
                'feedback_records' => json_encode($feedbackRecords, true),
                'template_type' => ParentTask::TEMPLATE_TYPE['BJ'],
                'template_id' => 1,
                'template_name' => '本级企业基本信息',
                'assign_object_id' => 1,
                'assign_object_name' => '发展和改革委员会',
                'parent_task_id' => 1,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'status' => $status
            ],
            [
                'work_order_task_id' => 2,
                'title' => '归集本级行政许可信息',
                'end_time' => '2022-10-01',
                'reason' => '测试反馈信息',
                'feedback_records' => json_encode(array(), true),
                'template_type' => ParentTask::TEMPLATE_TYPE['BJ'],
                'template_id' => 2,
                'template_name' => '本级行政许可信息',
                'assign_object_id' => 1,
                'assign_object_name' => '发展和改革委员会',
                'parent_task_id' => 2,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'status' => $status
            ],
        ];
    }
}

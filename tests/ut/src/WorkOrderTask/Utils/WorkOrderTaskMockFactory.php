<?php
namespace BaseData\WorkOrderTask\Utils;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;

class WorkOrderTaskMockFactory
{
    public static function generateWorkOrderTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WorkOrderTask {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $workOrderTask = new WorkOrderTask($id);
        $workOrderTask->setId($id);

        self::generateTemplateType($workOrderTask, $faker, $value);
        self::generateTemplate($workOrderTask, $faker, $value);
        self::generateStatus($workOrderTask, $faker, $value);
        
        $workOrderTask->setCreateTime($faker->unixTime());
        $workOrderTask->setUpdateTime($faker->unixTime());
        $workOrderTask->setStatusTime($faker->unixTime());

        return $workOrderTask;
    }

    private static function generateTemplateType($workOrderTask, $faker, $value) : void
    {
        $templateType = isset($value['templateType']) ?
            $value['templateType'] :
            $faker->randomElement(ParentTask::TEMPLATE_TYPE);
        
        $workOrderTask->getParentTask()->setTemplateType($templateType);
    }

    private static function generateTemplate($workOrderTask, $faker, $value) : void
    {
        unset($value);
        if ($workOrderTask->getParentTask()->getTemplateType() == ParentTask::TEMPLATE_TYPE['GB']) {
            $template = new GbTemplate($faker->randomDigit);
        }
        if ($workOrderTask->getParentTask()->getTemplateType() == ParentTask::TEMPLATE_TYPE['BJ']) {
            $template = new BjTemplate($faker->randomDigit);
        }
        
        $workOrderTask->setTemplate($template);
        $workOrderTask->getParentTask()->setTemplate($template);
    }

    private static function generateStatus($workOrderTask, $faker, $value) : void
    {
        unset($faker);
        $status = isset($value['status']) ?
            $value['status'] :
            0;
        
        $workOrderTask->setStatus($status);
    }
}

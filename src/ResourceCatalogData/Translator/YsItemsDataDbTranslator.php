<?php
namespace BaseData\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\YsItemsData;
use BaseData\ResourceCatalogData\Model\NullYsItemsData;

class YsItemsDataDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $ysItemsData = null) : YsItemsData
    {
        if (!isset($expression['ys_items_data_id'])) {
            return NullYsItemsData::getInstance();
        }

        if ($ysItemsData == null) {
            $ysItemsData = new YsItemsData();
        }

        $ysItemsData->setId($expression['ys_items_data_id']);

        if (isset($expression['data'])) {
            $ysItemsData->setData(unserialize(gzuncompress(base64_decode($expression['data'], true))));
        }

        return $ysItemsData;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($ysItemsData, array $keys = array()) : array
    {
        if (!$ysItemsData instanceof YsItemsData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'data'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['ys_items_data_id'] = $ysItemsData->getId();
        }
        if (in_array('data', $keys)) {
            $expression['data'] = base64_encode(gzcompress(serialize($ysItemsData->getData())));
        }

        return $expression;
    }
}

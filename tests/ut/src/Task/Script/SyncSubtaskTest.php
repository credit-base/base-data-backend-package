<?php
namespace BaseData\Task\Script;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Service\SyncSubtaskService;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RuleServiceRepository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\TaskRepository;

class SyncSubtaskTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockSyncSubtask();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsOnceTask()
    {
        $this->assertInstanceof("BaseData\Task\Model\OnceTask", $this->stub);
    }
    
    public function testGetTaskRepository()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Repository\TaskRepository',
            $this->stub->getTaskRepository()
        );
    }

    public function testFetchTask()
    {
        $stub = $this->getMockBuilder(MockSyncSubtask::class)
                        ->setMethods(['getTaskRepository'])
                        ->getMock();
        $id = 1;

        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask($id);

        $repository = $this->prophesize(TaskRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($task);
        $stub->expects($this->exactly(1))->method('getTaskRepository')->willReturn($repository->reveal());

        $result = $stub->fetchTask($id);

        $this->assertEquals($result, $task);
    }

    public function testGetRuleServiceRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RuleServiceRepository',
            $this->stub->getRuleServiceRepository()
        );
    }

    public function testFetchRuleService()
    {
        $stub = $this->getMockBuilder(MockSyncSubtask::class)
                        ->setMethods(['getRuleServiceRepository'])
                        ->getMock();
        $id = 1;

        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService($id);

        $repository = $this->prophesize(RuleServiceRepository::class);
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($ruleService);
        $stub->expects($this->exactly(1))->method('getRuleServiceRepository')->willReturn($repository->reveal());

        $result = $stub->fetchRuleService($id);

        $this->assertEquals($result, $ruleService);
    }

    public function testGetSyncSubtaskService()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Service\SyncSubtaskService',
            $this->stub->getSyncSubtaskService(new RuleService(), new Task())
        );
    }

    public function testGetSyncSubtask()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Script\SyncSubtask',
            $this->stub->getSyncSubtask()
        );
    }

    public function testGetSyncErrorDataDownload()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Script\SyncErrorDataDownload',
            $this->stub->getSyncErrorDataDownload()
        );
    }

    public function testExecuteAction()
    {
        $stub = $this->getMockBuilder(MockSyncSubtask::class)
            ->setMethods([
                'getData',
                'fetchTask',
                'fetchRuleService',
                'getSyncSubtaskService',
                'getSyncSubtask',
                'getSyncErrorDataDownload'
            ])->getMock();

        $data = array(
            'task' => 1,
            'ruleService' => 1
        );

        $stub->expects($this->exactly(1))
             ->method('getData')
             ->willReturn($data);

        $task = \BaseData\ResourceCatalogData\Utils\TaskMockFactory::generateTask($data['task']);
        $task->setStatus(Task::STATUS['FAILURE']);
        $ruleService = \BaseData\Rule\Utils\MockFactory::generateRuleService($data['ruleService']);

        $stub->expects($this->exactly(1))
             ->method('fetchTask')
             ->willReturn($task);

        $stub->expects($this->exactly(1))
             ->method('fetchRuleService')
             ->willReturn($ruleService);

        $ruleServiceList = array($ruleService);

        $syncTaskService = $this->prophesize(SyncSubtaskService::class);
        $syncTaskService->execute()->shouldBeCalledTimes(1)->willReturn([$ruleServiceList, $task]);
        $stub->expects($this->once())->method('getSyncSubtaskService')->willReturn($syncTaskService->reveal());

        $syncErrorDataDownload = $this->prophesize(SyncErrorDataDownload::class);
        $syncErrorDataDownload->setData(Argument::exact(
            array('task' => $task->getId())
        ))->shouldBeCalledTimes(1);
        $syncErrorDataDownload->add()->shouldBeCalledTimes(1);
        $stub->expects($this->once())->method('getSyncErrorDataDownload')->willReturn($syncErrorDataDownload->reveal());

        $syncSubtask = $this->prophesize(SyncSubtask::class);
        $syncSubtask->setData(Argument::exact(
            array('task' => $task->getId(), 'ruleService' => $ruleService->getId())
        ))->shouldBeCalledTimes(1);
        $syncSubtask->add()->shouldBeCalledTimes(1);
        $stub->expects($this->once())->method('getSyncSubtask')->willReturn($syncSubtask->reveal());

        $result = $stub->executeAction();

        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Template\WbjTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Translator\WbjTemplateDbTranslator;

/**
 * @Feature: 我是委办局领导,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 通过资源信息类别搜索
 */
class InfoCategoryTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchWbjTemplateList()
    {
        $repository = new WbjTemplateRepository();
 
        $filter['infoCategory'] = Template::INFO_CATEGORY['SHOUXXX'];

        list($wbjTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $wbjTemplateList;
    }

    /**
     * @Then: 我可以看到信息分类为守信信息的所有资源目录
     */
    public function testViewWbjTemplateList()
    {
        $setWbjTemplateList = $this->wbjTemplate();

        foreach ($setWbjTemplateList as $key => $setWbjTemplate) {
            $setWbjTemplateList[$key]['wbj_template_id'] = $key +1;
            $setWbjTemplateList[$key]['subject_category'] = json_decode($setWbjTemplate['subject_category'], true);
            $setWbjTemplateList[$key]['items'] = json_decode($setWbjTemplate['items'], true);
        }

        $wbjTemplateList = $this->fetchWbjTemplateList();
        $translator = new WbjTemplateDbTranslator();
        foreach ($wbjTemplateList as $wbjTemplate) {
            $wbjTemplateArray[] = $translator->objectToArray($wbjTemplate);
        }

        $this->assertEquals($wbjTemplateArray, $setWbjTemplateList);

        foreach ($wbjTemplateArray as $wbjTemplate) {
            $this->assertEquals(Template::INFO_CATEGORY['SHOUXXX'], $wbjTemplate['info_category']);
        }
    }
}

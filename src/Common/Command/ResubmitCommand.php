<?php
namespace BaseData\Common\Command;

use Marmot\Interfaces\ICommand;

abstract class ResubmitCommand implements ICommand
{
    public $id;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
    }
}

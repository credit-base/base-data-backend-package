<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Classes\Controller;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\BaseTemplateRepository;

class MockBaseTemplateControllerTrait extends Controller
{
    use BaseTemplateControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getTemplateWidgetRulePublic() : TemplateWidgetRule
    {
        return $this->getTemplateWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : BaseTemplateRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateOperateScenarioPublic(
        $subjectCategory,
        $dimension,
        $exchangeFrequency,
        $infoClassify,
        $infoCategory,
        $items
    ) : bool {
        return $this->validateOperateScenario(
            $subjectCategory,
            $dimension,
            $exchangeFrequency,
            $infoClassify,
            $infoCategory,
            $items
        );
    }
}

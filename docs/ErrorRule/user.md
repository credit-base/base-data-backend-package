# 人通用错误规范（501-1000）

### ER-数字

**id**

错误的唯一id

**code**

程序用的错误状态码,用字符串表述

**title**

简短的,可读性高的问题总结.

**detail**

针对该问题的高可读性解释

**links**

可以在请求文档中取消应用的关联资源

**status**

状态码

---

* `ER-501`: 用户姓名格式不正确
* `ER-502`: 手机号码格式不正确
* `ER-503`: 密码格式不正确
* `ER-504`: 身份证号码格式不正确
* `ER-505`: 密码错误
* `ER-506` - `ER-1000`: 预留

---

### <a name="ER-501">ER-501</a>

**id**

`501`

**code**

`REAL_NAME_FORMAT_ERROR`

**title**

用户姓名格式不正确

**detail**

用户姓名格式不正确

**links**

待补充

**status**

403

### <a name="ER-502">ER-502</a>

**id**

`502`

**code**

`CELLPHONE_FORMAT_ERROR`

**title**

手机号码格式不正确

**detail**

手机号码格式不正确

**links**

待补充

**status**

403

### <a name="ER-503">ER-503</a>

**id**

`503`

**code**

`PASSWORD_FORMAT_ERROR`

**title**

密码格式不正确

**detail**

密码格式不正确

**links**

待补充

**status**

403

### <a name="ER-504">ER-504</a>

**id**

`504`

**code**

`CARDID_FORMAT_ERROR`

**title**

身份证号码格式不正确

**detail**

身份证号码格式不正确

**links**

待补充

**status**

403

### <a name="ER-505">ER-505</a>

**id**

`505`

**code**

`PASSWORD_INCORRECT`

**title**

密码错误

**detail**

密码错误

**links**

待补充

**status**

403

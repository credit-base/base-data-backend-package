<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;

class MockBjSearchDataCommandHandlerTrait
{
    use BjSearchDataCommandHandlerTrait;

    public function publicGetRepository() : BjSearchDataRepository
    {
        return $this->getRepository();
    }

    public function publicFetchBjSearchData(int $id) : BjSearchData
    {
        return $this->fetchBjSearchData($id);
    }
}

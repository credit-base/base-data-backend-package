<?php
namespace BaseData\Template\WbjTemplate\Search;

use PHPUnit\DbUnit\DataSet\ArrayDataSet;

trait SearchTrait
{
    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
        ]);
    }
}

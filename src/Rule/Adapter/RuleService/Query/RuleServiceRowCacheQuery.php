<?php
namespace BaseData\Rule\Adapter\RuleService\Query;

use Marmot\Framework\Query\RowCacheQuery;

class RuleServiceRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'rule_id',
            new Persistence\RuleServiceCache(),
            new Persistence\RuleServiceDb()
        );
    }
}

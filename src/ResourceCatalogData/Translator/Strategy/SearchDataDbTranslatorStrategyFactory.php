<?php
namespace BaseData\ResourceCatalogData\Translator\Strategy;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class SearchDataDbTranslatorStrategyFactory
{
    const MAPS = array(
        ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'] =>
            '\BaseData\ResourceCatalogData\Translator\Strategy\EnterpriserDbTranslatorStrategy',
        ISearchDataAble::SUBJECT_CATEGORY['GTGSH'] =>
            '\BaseData\ResourceCatalogData\Translator\Strategy\EnterpriserDbTranslatorStrategy',
    );

    public function getStrategy(int $type) : IDbTranslatorStrategy
    {
        $strategy = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($strategy) ? new $strategy : new NullSearchDataDbTranslatorStrategy();
    }
}

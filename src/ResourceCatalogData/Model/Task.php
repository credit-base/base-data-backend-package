<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\ResourceCatalogData\Adapter\Task\ITaskAdapter;
use BaseData\ResourceCatalogData\Repository\TaskRepository;

use BaseData\Template\Model\Template;

use BaseData\Crew\Model\Crew;
use BaseData\UserGroup\Model\UserGroup;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class Task implements IObject, IOperate
{

    use Object, OperateTrait;

    /**
     * @var STATUS['DEFAULT']  进行中,默认 0
     * @var STATUS['SUCCESS']  成功 2
     * @var STATUS['FAILURE']  失败 -2
     * @var STATUS['FAILURE_FILE_DOWNLOAD']  失败文件下载成功 -3
     */
    const STATUS = array(
        'DEFAULT' => 0,
        'SUCCESS' => 2,
        'FAILURE' => -2,
        'FAILURE_FILE_DOWNLOAD' => -3
    );

    //失败文件名前缀
    const ERROR_FILE_NAME_PREFIX = 'FAILURE';
    
    protected $id;
    /**
     * @var Crew $crew 发布人
     */
    protected $crew;
    /**
     * @var UserGroup $userGroup 发布委办局
     */
    protected $userGroup;
    /**
     * @var int $pid 父id
     */
    protected $pid;
    /**
     * @var int $total 总数
     */
    protected $total;
    /**
     * @var int $successNumber 成功数
     */
    protected $successNumber;
    /**
     * @var int $failureNumber 失败数
     */
    protected $failureNumber;
    /**
     * @var int $sourceCategory 来源资源目录类型
     */
    protected $sourceCategory;
    /**
     * @var Template $sourceTemplate 来源资源目录
     */
    protected $sourceTemplate;
    /**
     * @var int $targetCategory 目标资源目录类型
     */
    protected $targetCategory;
    /**
     * @var Template $targetTemplate 目标资源目录
     */
    protected $targetTemplate;
    /**
     * @var Rule $targetRule 入目标库使用规则id
     */
    protected $targetRule;
    /**
     * @var int $scheduleTask 调度任务id
     */
    protected $scheduleTask;
    /**
     * @var int $errorNumber 错误编号
     */
    protected $errorNumber;
    /**
     * @var string $fileName 文件名
     */
    protected $fileName;

    protected $repository;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->crew = new Crew();
        $this->userGroup = new UserGroup();
        $this->pid = 0;
        $this->total = 0;
        $this->successNumber = 0;
        $this->failureNumber = 0;
        $this->sourceCategory = 0;
        $this->sourceTemplate = 0;
        $this->targetCategory = 0;
        $this->targetTemplate = 0;
        $this->targetRule = 0;
        $this->scheduleTask = 0;
        $this->errorNumber = 0;
        $this->fileName = '';
        $this->status = self::STATUS['DEFAULT'];
        $this->statusTime = 0;
        $this->createTime = Core::$container->get('time');
        $this->updateTime = Core::$container->get('time');
        $this->repository = new TaskRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->crew);
        unset($this->userGroup);
        unset($this->pid);
        unset($this->total);
        unset($this->successNumber);
        unset($this->failureNumber);
        unset($this->sourceCategory);
        unset($this->sourceTemplate);
        unset($this->targetCategory);
        unset($this->targetTemplate);
        unset($this->targetRule);
        unset($this->scheduleTask);
        unset($this->errorNumber);
        unset($this->fileName);
        unset($this->status);
        unset($this->statusTime);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setCrew(Crew $crew) : void
    {
        $this->crew = $crew;
    }

    public function getCrew() : Crew
    {
        return $this->crew;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setPid(int $pid) : void
    {
        $this->pid = $pid;
    }

    public function getPid() : int
    {
        return $this->pid;
    }

    public function setTotal(int $total) : void
    {
        $this->total = $total;
    }

    public function getTotal() : int
    {
        return $this->total;
    }

    public function setSuccessNumber(int $successNumber) : void
    {
        $this->successNumber = $successNumber;
    }

    public function getSuccessNumber() : int
    {
        return $this->successNumber;
    }

    public function setFailureNumber(int $failureNumber) : void
    {
        $this->failureNumber = $failureNumber;
    }

    public function getFailureNumber() : int
    {
        return $this->failureNumber;
    }

    public function setSourceCategory(int $sourceCategory) : void
    {
        $this->sourceCategory = in_array($sourceCategory, Template::CATEGORY) ? $sourceCategory : 0;
    }

    public function getSourceCategory() : int
    {
        return $this->sourceCategory;
    }

    public function setSourceTemplate(int $sourceTemplate) : void
    {
        $this->sourceTemplate = $sourceTemplate;
    }

    public function getSourceTemplate() : int
    {
        return $this->sourceTemplate;
    }

    public function setTargetCategory(int $targetCategory) : void
    {
        $this->targetCategory = in_array($targetCategory, Template::CATEGORY) ? $targetCategory : 0;
    }

    public function getTargetCategory() : int
    {
        return $this->targetCategory;
    }

    public function setTargetTemplate(int $targetTemplate) : void
    {
        $this->targetTemplate = $targetTemplate;
    }

    public function getTargetTemplate() : int
    {
        return $this->targetTemplate;
    }

    public function setTargetRule(int $targetRule) : void
    {
        $this->targetRule = $targetRule;
    }

    public function getTargetRule() : int
    {
        return $this->targetRule;
    }

    public function setScheduleTask(int $scheduleTask) : void
    {
        $this->scheduleTask = $scheduleTask;
    }

    public function getScheduleTask() : int
    {
        return $this->scheduleTask;
    }

    public function setErrorNumber(int $errorNumber) : void
    {
        $this->errorNumber = $errorNumber;
    }

    public function getErrorNumber() : int
    {
        return $this->errorNumber;
    }

    public function setFileName(string $fileName) : void
    {
        $this->fileName = $fileName;
    }

    public function getFileName() : string
    {
        return $this->fileName;
    }

    public function setStatus(int $status) : void
    {
        $this->status = in_array($status, self::STATUS) ? $status : self::STATUS['DEFAULT'];
    }

    protected function getRepository() : ITaskAdapter
    {
        return $this->repository;
    }
    
    public function isFailureStatus() : bool
    {
        return $this->getStatus() == self::STATUS['FAILURE'];
    }

    public function isFailureFileDownloadStatus() : bool
    {
        return $this->getStatus() == self::STATUS['FAILURE_FILE_DOWNLOAD'];
    }

    protected function addAction() : bool
    {
        return $this->getRepository()->add($this);
    }

    protected function editAction() : bool
    {
        $this->setStatusTime(Core::$container->get('time'));
        
        return $this->getRepository()->edit(
            $this,
            array(
                'total',
                'successNumber',
                'failureNumber',
                'errorNumber',
                'status',
                'statusTime'
            )
        );
    }
}

<?php
namespace BaseData\Rule\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\JsonApiTrait;
use Marmot\Framework\Common\Controller\IOperateController;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\View\RuleServiceView;
use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\View\UnAuditedRuleServiceView;

use BaseData\Rule\Command\RuleService\AddRuleServiceCommand;
use BaseData\Rule\Command\RuleService\EditRuleServiceCommand;
use BaseData\Rule\Command\RuleService\DeleteRuleServiceCommand;

class RuleServiceOperateController extends Controller implements IOperateController
{
    use JsonApiTrait, RuleServiceControllerTrait;
    
    /**
     * 规则新增功能,通过POST传参
     * 对应路由 /rules
     * @return jsonApi
     */
    public function add()
    {
        //初始化数据
        $data = $this->getRequest()->post('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rules = $attributes['rules'];
        $transformationCategory = $attributes['transformationCategory'];
        $sourceCategory = $attributes['sourceCategory'];

        $crew = $relationships['crew']['data'][0]['id'];
        $transformationTemplate = $relationships['transformationTemplate']['data'][0]['id'];
        $sourceTemplate = $relationships['sourceTemplate']['data'][0]['id'];

        //验证
        if ($this->validateCommonScenario(
            $rules,
            $crew
        ) && $this->validateAddScenario(
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory
        )) {
            //初始化命令
            $command = new AddRuleServiceCommand(
                $rules,
                $transformationTemplate,
                $sourceTemplate,
                $transformationCategory,
                $sourceCategory,
                $crew
            );

            //执行命令
            if ($this->getCommandBus()->send($command)) {
                //获取最新数据
                $ruleService = $this->getUnAuditedRuleServiceRepository()->fetchOne($command->id);
                if ($ruleService instanceof UnAuditedRuleService) {
                    $this->getResponse()->setStatusCode(201);
                    $this->render(new UnAuditedRuleServiceView($ruleService));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 规则编辑功能,通过PATCH传参
     * 对应路由 /rules/{id:\d+}
     * @param int id 规则 id
     * @return jsonApi
     */
    public function edit(int $id)
    {
        $data = $this->getRequest()->patch('data');
        $attributes = $data['attributes'];
        $relationships = $data['relationships'];

        $rules = $attributes['rules'];

        $crew = $relationships['crew']['data'][0]['id'];
        
        if ($this->validateCommonScenario(
            $rules,
            $crew
        )) {
            $command = new EditRuleServiceCommand(
                $rules,
                $id,
                $crew
            );

            if ($this->getCommandBus()->send($command)) {
                $ruleService = $this->getUnAuditedRuleServiceRepository()->fetchOne($command->id);
                if ($ruleService instanceof UnAuditedRuleService) {
                    $this->render(new UnAuditedRuleServiceView($ruleService));
                    return true;
                }
            }
        }

        $this->displayError();
        return false;
    }

    /**
     * 规则删除功能,通过DELETE传参
     * 对应路由 /rules/{id:\d+}/delete
     * @param int id 规则 id
     * @return jsonApi
     */
    public function delete(int $id)
    {
        if (!empty($id)) {
            $data = $this->getRequest()->patch('data');
            $relationships = $data['relationships'];

            $crew = $relationships['crew']['data'][0]['id'];

            if ($this->validateStatusScenario($crew)) {
                $command = new DeleteRuleServiceCommand($crew, $id);

                if ($this->getCommandBus()->send($command)) {
                    $ruleService  = $this->getRepository()->fetchOne($id);
                    if ($ruleService instanceof RuleService) {
                        $this->render(new RuleServiceView($ruleService));
                        return true;
                    }
                }
            }
        }
        $this->displayError();
        return false;
    }
}

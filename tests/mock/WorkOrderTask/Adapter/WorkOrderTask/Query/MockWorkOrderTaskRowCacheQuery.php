<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query;

use Marmot\Framework\Interfaces\DbLayer;
use Marmot\Interfaces\CacheLayer;

class MockWorkOrderTaskRowCacheQuery extends WorkOrderTaskRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

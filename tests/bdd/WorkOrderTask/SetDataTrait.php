<?php
namespace BaseData\WorkOrderTask;

use Marmot\Core;

use BaseData\Template\Model\Template;

use BaseData\WorkOrderTask\Model\IProgress;
use BaseData\WorkOrderTask\Model\ParentTask;

trait SetDataTrait
{
    protected function parentTask() : array
    {
        return
        [
            [
                'parent_task_id' => 1,
                'template_type' => ParentTask::TEMPLATE_TYPE['GB'],
                'title' => '归集地方性红名单信息',
                'description' => '依据国家要求，现需要按要求归集地方性红名单信息',
                'end_time' => '2020-01-01',
                'attachment' => json_encode(array('name'=>'国203号文', 'identify'=>'依据附件.pdf')),
                'template_id' => 1,
                'template_name' => '国标登记信息',
                'assign_objects' => json_encode(array(1), true),
                'finish_count' => 0,
                'reason' => '撤销原因',

                'status' => 0,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0
            ]
        ];
    }

    protected function workOrderTask(int $status = IProgress::STATUS['DQR'])  : array
    {
        $feedbackRecords = [
            [
                "crew" => "1",
                "items" => [
                    [
                        "name" => "主体名称",
                        "type" => "1",
                        "length" => "100",
                        "options" => [],
                        "remarks" => "",
                        "identify" => "ZTMC",
                        "isMasked" => "0",
                        "maskRule" => [],
                        "dimension" => "1",
                        "isNecessary" => "1"
                    ],
                    [
                        "name" => "统一社会信用代码",
                        "type" => "1",
                        "length" => "18",
                        "options" => [],
                        "remarks" => "",
                        "identify" => "TYSHXYDM",
                        "isMasked" => "1",
                        "maskRule" => [
                            "5",
                            "4"
                        ],
                        "dimension" => "1",
                        "isNecessary" => "1"
                    ],
                ],
                "reason" => "测试反馈信息",
                "crewName" => "张科",
                "userGroup" => "1",
                "templateId" => "",
                "feedbackTime" => "1615440609",
                "userGroupName" => "发展和改革委员会",
                "isExistedTemplate" => "0"
            ]
        ];

        return
        [
            [
                'work_order_task_id' => 1,
                'title' => '归集地方性红名单信息',
                'end_time' => '2020-01-01',
                'reason' => '撤销原因',
                'feedback_records' => json_encode($feedbackRecords, true),
                'template_type' => ParentTask::TEMPLATE_TYPE['GB'],
                'template_id' => 1,
                'template_name' => '国标登记信息',
                'assign_object_id' => 1,
                'assign_object_name' => '发展和改革委员会',
                'parent_task_id' => 1,
                'create_time' => Core::$container->get('time'),
                'update_time' => Core::$container->get('time'),
                'status_time' => 0,
                'status' => $status
            ]
        ];
    }
}

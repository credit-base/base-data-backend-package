<?php
namespace BaseData\Rule\RuleService\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\Model\RuleService;

/**
 * 测试规则接口禁用
 */
class DisableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
    }

    /**
     * 存在一条规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_rule' => $this->rule()
            ]
        );
    }

    public function testViewRuleService()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        
        $data = array(
            'data' => array(
                "type"=>"rules",
                "relationships"=>array(
                    "crew"=>array(
                        "data"=>array(
                            array("type"=>"crews","id"=>1)
                        )
                    )
                )
            )
        );

        $response = $client->request(
            'PATCH',
            'rules/1/delete',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals(RuleService::STATUS['DELETED'], $attributes['status']);
    }
}

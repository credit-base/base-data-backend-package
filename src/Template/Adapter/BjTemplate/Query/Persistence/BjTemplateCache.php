<?php
namespace BaseData\Template\Adapter\BjTemplate\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class BjTemplateCache extends Cache
{
    const KEY = 'bj_template';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

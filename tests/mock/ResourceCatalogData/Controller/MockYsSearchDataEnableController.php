<?php
namespace BaseData\ResourceCatalogData\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;

class MockYsSearchDataEnableController extends YsSearchDataEnableController
{
    public function getRepository() : IYsSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }
}

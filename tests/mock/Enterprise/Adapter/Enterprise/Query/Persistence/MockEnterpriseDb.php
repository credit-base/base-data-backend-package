<?php
namespace BaseData\Enterprise\Adapter\Enterprise\Query\Persistence;

class MockEnterpriseDb extends EnterpriseDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

<?php
namespace BaseData\Enterprise\Adapter\Enterprise;

use BaseData\Enterprise\Model\Enterprise;

interface IEnterpriseAdapter
{
    public function fetchOne($id) : Enterprise;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(Enterprise $enterprise) : bool;

    public function edit(Enterprise $enterprise, array $keys = array()) : bool;
}

<?php
namespace BaseData\WorkOrderTask\View;

use PHPUnit\Framework\TestCase;
use Neomerx\JsonApi\Factories\Factory;

class WorkOrderTaskSchemaTest extends TestCase
{
    private $workOrderTaskSchema;

    private $workOrderTask;

    public function setUp()
    {
        $this->workOrderTaskSchema = new WorkOrderTaskSchema(new Factory());

        $this->workOrderTask = \BaseData\WorkOrderTask\Utils\WorkOrderTaskMockFactory::generateWorkOrderTask(1);
    }

    public function tearDown()
    {
        parent::tearDown();
        unset($this->workOrderTaskSchema);
        unset($this->workOrderTask);
    }

    public function testExtendsSchemaProvider()
    {
        $this->assertInstanceof('Neomerx\JsonApi\Schema\SchemaProvider', $this->workOrderTaskSchema);
    }

    public function testGetId()
    {
        $result = $this->workOrderTaskSchema->getId($this->workOrderTask);

        $this->assertEquals($result, $this->workOrderTask->getId());
    }

    public function testGetAttributes()
    {
        $result = $this->workOrderTaskSchema->getAttributes($this->workOrderTask);

        $this->assertEquals($result['reason'], $this->workOrderTask->getReason());
        $this->assertEquals($result['feedbackRecords'], $this->workOrderTask->getFeedbackRecords());
        $this->assertEquals($result['status'], $this->workOrderTask->getStatus());
        $this->assertEquals($result['createTime'], $this->workOrderTask->getCreateTime());
        $this->assertEquals($result['updateTime'], $this->workOrderTask->getUpdateTime());
        $this->assertEquals($result['statusTime'], $this->workOrderTask->getStatusTime());
    }

    public function testGetRelationships()
    {
        $result = $this->workOrderTaskSchema->getRelationships($this->workOrderTask, 0, array());

        $this->assertEquals($result['template'], ['data' => $this->workOrderTask->getTemplate()]);
        $this->assertEquals($result['assignObject'], ['data' => $this->workOrderTask->getAssignObject()]);
        $this->assertEquals($result['parentTask'], ['data' => $this->workOrderTask->getParentTask()]);
    }
}

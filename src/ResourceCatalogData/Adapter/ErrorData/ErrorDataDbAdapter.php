<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\NullErrorData;
use BaseData\ResourceCatalogData\Repository\TaskRepository;
use BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\ErrorData\Query\ErrorDataRowCacheQuery;

use BaseData\Rule\Repository\RepositoryFactory;

class ErrorDataDbAdapter implements IErrorDataAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $taskRepository;

    private $repositoryFactory;

    public function __construct()
    {
        $this->dbTranslator = new ErrorDataDbTranslator();
        $this->rowCacheQuery = new ErrorDataRowCacheQuery();
        $this->taskRepository = new TaskRepository();
        $this->repositoryFactory = new RepositoryFactory();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->taskRepository);
        unset($this->repositoryFactory);
    }
    
    protected function getDbTranslator() : ErrorDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : ErrorDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getTaskRepository() : TaskRepository
    {
        return $this->taskRepository;
    }

    protected function getRepositoryFactory() : RepositoryFactory
    {
        return $this->repositoryFactory;
    }

    protected function getNullObject() : INull
    {
        return NullErrorData::getInstance();
    }

    public function add(ErrorData $errorData) : bool
    {
        return $this->addAction($errorData);
    }

    public function edit(ErrorData $errorData, array $keys = array()) : bool
    {
        return $this->editAction($errorData, $keys);
    }

    public function fetchOne($id) : ErrorData
    {
        $errorData = $this->fetchOneAction($id);

        $this->fetchTask($errorData);
        $this->fetchTemplate($errorData);

        return $errorData;
    }

    public function fetchList(array $ids) : array
    {
        $errorDataList = $this->fetchListAction($ids);

        $this->fetchTask($errorDataList);
        $this->fetchTemplate($errorDataList);

        return $errorDataList;
    }

    protected function fetchTask($errorData)
    {
        return is_array($errorData) ?
        $this->fetchTaskByList($errorData) :
        $this->fetchTaskByObject($errorData);
    }

    protected function fetchTaskByObject($errorData)
    {
        if (!$errorData instanceof INull) {
            $taskId = $errorData->getTask()->getId();
            $task = $this->getTaskRepository()->fetchOne($taskId);
            $errorData->setTask($task);
        }
    }

    protected function fetchTaskByList(array $errorDataList)
    {
        if (!empty($errorDataList)) {
            $taskIds = array();
            foreach ($errorDataList as $errorData) {
                $taskIds[] = $errorData->getTask()->getId();
            }

            $taskList = $this->getTaskRepository()->fetchList($taskIds);
            if (!empty($taskList)) {
                foreach ($errorDataList as $errorData) {
                    if (isset($taskList[$errorData->getTask()->getId()])) {
                        $errorData->setTask($taskList[$errorData->getTask()->getId()]);
                    }
                }
            }
        }
    }

    protected function fetchTemplate($errorData)
    {
        return is_array($errorData) ?
        $this->fetchTemplateByList($errorData) :
        $this->fetchTemplateByObject($errorData);
    }

    protected function fetchTemplateByObject($errorData)
    {
        if (!$errorData instanceof INull) {
            $templateId = $errorData->getTemplate()->getId();
            $repository = $this->getRepositoryFactory()->getRepository($errorData->getCategory());
            $template = $repository->fetchOne($templateId);
            $itemsData = $errorData->getItemsData()->getData();
            $errorData->setTemplate($template);
            $errorData->getItemsData()->setData($itemsData);
        }
    }

    protected function fetchTemplateByList(array $errorDataList)
    {
        if (!empty($errorDataList)) {
            $templateIds = array();
            foreach ($errorDataList as $errorData) {
                $templateIds[$errorData->getCategory()][] = array(
                    'templateId' => $errorData->getTemplate()->getId(),
                    'errorDataId' => $errorData->getId()
                );
            }

            $objectReferenceList = array();

            foreach ($templateIds as $type => $val) {
                $ids = array_unique(array_column($val, 'templateId'));
                $templateRepository = $this->getRepositoryFactory()->getRepository($type);
                $templateListTemp = $templateRepository->fetchList($ids);
                
                foreach ($val as $v) {
                    if (isset($templateListTemp[$v['templateId']])) {
                        $objectReferenceList[$v['errorDataId']] =
                        $templateListTemp[$v['templateId']];
                    }
                }
            }

            if (!empty($objectReferenceList)) {
                foreach ($errorDataList as $errorData) {
                    $errorDataId = $errorData->getId();
                    if (isset($objectReferenceList[$errorDataId])) {
                        $itemsData = $errorData->getItemsData()->getData();
                        $errorData->setTemplate($objectReferenceList[$errorDataId]);
                        $errorData->getItemsData()->setData($itemsData);
                    }
                }
            }
        }
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $errorData = new ErrorData();

            if (isset($filter['task'])) {
                $errorData->getTask()->setId($filter['task']);
                $info = $this->getDbTranslator()->objectToArray($errorData, array('task'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['category'])) {
                $errorData->setCategory($filter['category']);
                $info = $this->getDbTranslator()->objectToArray($errorData, array('category'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['template'])) {
                $errorData->getTemplate()->setId($filter['template']);
                $info = $this->getDbTranslator()->objectToArray($errorData, array('template'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray(new ErrorData(), array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Service;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Repository\RuleServiceRepository;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;

use BaseData\Template\Model\Template;

class MockSyncTaskTrait
{
    use SyncTaskTrait;

    public function publicGetErrorDataRepository() : ErrorDataRepository
    {
        return $this->getErrorDataRepository();
    }

    public function publicGetRuleServiceRepository() : RuleServiceRepository
    {
        return $this->getRuleServiceRepository();
    }

    public function publicSearchDataTransformationAndAdd(
        array $sourceSearchDataList,
        RuleService $ruleService,
        Task $task
    ) {
        return $this->searchDataTransformationAndAdd($sourceSearchDataList, $ruleService, $task);
    }

    public function publicSuccessNumber(Task $task, RuleService $ruleService) : int
    {
        return $this->successNumber($task, $ruleService);
    }

    public function publicFailureNumber(Task $task) : int
    {
        return $this->failureNumber($task);
    }

    public function publicUpdateTask(Task $task, RuleService $ruleService) : bool
    {
        return $this->updateTask($task, $ruleService);
    }

    public function publicFetchRuleServiceList(Template $template) : array
    {
        return $this->fetchRuleServiceList($template);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Strategy;

use PHPUnit\Framework\TestCase;

class NullDataStrategyTest extends TestCase
{
    private $strategy;

    public function setUp()
    {
        $this->strategy = new NullDataStrategy();
    }

    public function tearDown()
    {
        unset($this->strategy);
    }

    public function testImplementIDataStrategy()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Strategy\IDataStrategy', $this->strategy);
    }

    public function testValidate()
    {
        $value = 1;
        $rule = array('1');

        $result = $this->strategy->validate($value, $rule);
        $this->assertFalse($result);
    }

    public function testMock()
    {
        $rule = array('1');

        $result = $this->strategy->mock($rule);
        $this->assertFalse($result);
    }
}

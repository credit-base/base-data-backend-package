<?php
namespace BaseData\Template\BaseTemplate\Search;

use PHPUnit\DbUnit\DataSet\ArrayDataSet;

trait SearchTrait
{
    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_base_template' => $this->baseTemplate(),
        ]);
    }
}

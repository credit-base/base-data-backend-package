<?php
namespace BaseData\ResourceCatalogData\Task\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

/**
 * 测试任务接口查看多条
 */
class FetchListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
    }

    /**
     * 存在多条任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_task' => $this->task()
            ]
        );
    }

    public function testViewTask()
    {
        $setTaskList = $this->task();
       
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'tasks/1,2',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $data = $contents['data'];

        foreach ($data as $key => $task) {
            $this->taskCompareData($setTaskList[$key], $task);
        }
    }
}

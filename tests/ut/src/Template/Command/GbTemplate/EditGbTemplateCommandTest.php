<?php
namespace BaseData\Template\Command\GbTemplate;

use PHPUnit\Framework\TestCase;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class EditGbTemplateCommandTest extends TestCase
{
    private $command;
    
    public function setUp()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $this->command = new EditGbTemplateCommand(
            $faker->word,
            $faker->regexify('[A-Z_]{1,100}'),
            $faker->randomElements(array(1,2,3), 2),
            $faker->randomElement(array(1,2,3)),
            $faker->randomDigit,
            $faker->randomDigit,
            $faker->randomDigit,
            $faker->sentence,
            $faker->randomElements(array('1','2','3'), 2),
            $faker->randomDigit
        );
    }
    
    public function testExtendsCommand()
    {
        $this->assertInstanceOf('BaseData\Template\Command\Template\EditTemplateCommand', $this->command);
    }
}

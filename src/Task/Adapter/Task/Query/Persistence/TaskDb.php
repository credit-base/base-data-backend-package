<?php
namespace BaseData\Task\Adapter\Task\Query\Persistence;

use Marmot\Framework\Classes\Db;

class TaskDb extends Db
{
    const TABLE = 'schedule_task';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

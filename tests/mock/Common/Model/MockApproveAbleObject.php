<?php
namespace BaseData\Common\Model;

class MockApproveAbleObject implements IApproveAble
{
    public function approve() : bool
    {
        return false;
    }

    public function reject() : bool
    {
        return false;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\Common\Model\IEnableAble;
use BaseData\Common\Model\EnableAbleTrait;

use BaseData\Template\Model\WbjTemplate;

use BaseData\ResourceCatalogData\Repository\WbjItemsDataRepository;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class WbjSearchData extends SearchData implements IEnableAble
{
    use EnableAbleTrait;

    protected $repository;

    protected $wbjItemsDataRepository;

    public function __construct()
    {
        parent::__construct();
        $this->status = IEnableAble::STATUS['ENABLED'];
        $this->template = new WbjTemplate();
        $this->itemsData = new WbjItemsData();
        $this->repository = new WbjSearchDataRepository();
        $this->wbjItemsDataRepository = new WbjItemsDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->status);
        unset($this->template);
        unset($this->itemsData);
        unset($this->repository);
        unset($this->wbjItemsDataRepository);
    }

    public function setItemsData(WbjItemsData $itemsData) : void
    {
        $this->itemsData = $itemsData;
    }

    public function getItemsData() : WbjItemsData
    {
        return $this->itemsData;
    }

    protected function getRepository() : IWbjSearchDataAdapter
    {
        return $this->repository;
    }
    
    protected function getWbjItemsDataRepository() : IWbjItemsDataAdapter
    {
        return $this->wbjItemsDataRepository;
    }

    protected function addItemsData() : bool
    {
        return $this->getWbjItemsDataRepository()->add($this->getItemsData());
    }

    protected function addAction() : bool
    {
        $this->assignment();
        
        return $this->validate() && $this->addItemsData() && $this->getRepository()->add($this);
    }
}

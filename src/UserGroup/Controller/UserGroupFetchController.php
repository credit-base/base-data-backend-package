<?php
namespace BaseData\UserGroup\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\UserGroup\View\UserGroupView;
use BaseData\UserGroup\Repository\UserGroupRepository;
use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class UserGroupFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UserGroupRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IUserGroupAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new UserGroupView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'userGroups';
    }
}

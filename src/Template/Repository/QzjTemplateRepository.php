<?php
namespace BaseData\Template\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use BaseData\Template\Adapter\QzjTemplate\QzjTemplateDbAdapter;
use BaseData\Template\Adapter\QzjTemplate\QzjTemplateMockAdapter;

class QzjTemplateRepository extends Repository implements IQzjTemplateAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new QzjTemplateDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IQzjTemplateAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IQzjTemplateAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IQzjTemplateAdapter
    {
        return new QzjTemplateMockAdapter();
    }

    public function fetchOne($id) : QzjTemplate
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(QzjTemplate $qzjTemplate) : bool
    {
        return $this->getAdapter()->add($qzjTemplate);
    }

    public function edit(QzjTemplate $qzjTemplate, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($qzjTemplate, $keys);
    }
}

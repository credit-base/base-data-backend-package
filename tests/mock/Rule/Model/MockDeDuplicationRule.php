<?php
namespace BaseData\Rule\Model;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockDeDuplicationRule extends DeDuplicationRule
{
    use MockConcretenessRuleTrait;

    public function validate() : bool
    {
        return parent::validate();
    }

    public function fetchDeDuplicationItems(
        array $deDuplicationItemsIdentify,
        ISearchDataAble $transformationSearchData
    ) : array {
        return parent::fetchDeDuplicationItems($deDuplicationItemsIdentify, $transformationSearchData);
    }

    public function generateDeDuplicationItemsHash(
        array $deDuplicationItems,
        ISearchDataAble $transformationSearchData
    ) : string {
        return parent::generateDeDuplicationItemsHash($deDuplicationItems, $transformationSearchData);
    }

    public function isSearchDataExist(string $hash, ISearchDataAble $transformationSearchData) : bool
    {
        return parent::isSearchDataExist($hash, $transformationSearchData);
    }

    public function filterFormatChange(string $hash, ISearchDataAble $transformationSearchData) : array
    {
        return parent::filterFormatChange($hash, $transformationSearchData);
    }
}

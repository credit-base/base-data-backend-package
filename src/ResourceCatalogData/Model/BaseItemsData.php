<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Repository\BaseSearchDataRepository;
use BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter;

class BaseItemsData extends ItemsData
{
    public function __construct()
    {
        parent::__construct();
        $this->repository = new BaseSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IBaseSearchDataAdapter
    {
        return $this->repository;
    }
}

<?php
namespace BaseData\ResourceCatalogData\ErrorData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;
use BaseData\ResourceCatalogData\Translator\ErrorDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要了解我数据上传数据的原因时,在资源目录数据子系统下的上传任务列表中,点击失败数据可以查看到我的所有上传失败的数据,
 *           通过列表形式呈现,以便于我可以更快的修复错误数据并重新上传
 * @Scenario: 查看失败资源目录数据详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_error_data');
    }

    /**
     * @Given: 存在一条失败资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_task' => $this->task(),
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_error_data' => $this->errorData()
        ]);
    }

    /**
     * @When: 当我查看该条资源目录数据详情时
     */
    public function fetchErrorData($id)
    {
        $repository = new ErrorDataRepository();

        $errorData = $repository->fetchOne($id);

        return $errorData;
    }

    /**
     * @Then: 我可以看见该条资源目录数据的全部信息
     */
    public function testViewErrorDataList()
    {
        $id = 1;
        
        $setErrorData = $this->errorData()[0];
        $setErrorData['error_data_id'] = $id;
        $setErrorData['error_reason'] = json_decode($setErrorData['error_reason'], true);

        $errorData = $this->fetchErrorData($id);
        $translator = new ErrorDataDbTranslator();
        $errorDataArray = $translator->objectToArray($errorData);

        $this->assertEquals($errorDataArray, $setErrorData);
    }
}

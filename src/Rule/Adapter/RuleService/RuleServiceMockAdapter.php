<?php
namespace BaseData\Rule\Adapter\RuleService;

use BaseData\Rule\Model\RuleService;
use BaseData\Rule\Utils\MockFactory;

class RuleServiceMockAdapter implements IRuleServiceAdapter
{
    public function fetchOne($id) : RuleService
    {
        return MockFactory::generateRuleService($id);
    }

    public function fetchList(array $ids) : array
    {
        $ruleServiceList = array();

        foreach ($ids as $id) {
            $ruleServiceList[$id] = MockFactory::generateRuleService($id);
        }

        return $ruleServiceList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(RuleService $ruleService) : bool
    {
        unset($ruleService);
        return true;
    }

    public function edit(RuleService $ruleService, array $keys = array()) : bool
    {
        unset($ruleService);
        unset($keys);
        return true;
    }
}

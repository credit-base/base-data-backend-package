<?php
namespace BaseData\ResourceCatalogData\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\ResourceCatalogData\Model\SearchData;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
abstract class SearchDataDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $searchData = null)
    {
        if (isset($expression['info_classify'])) {
            $searchData->setInfoClassify($expression['info_classify']);
        }
        if (isset($expression['info_category'])) {
            $searchData->setInfoCategory($expression['info_category']);
        }
        if (isset($expression['crew_id'])) {
            $searchData->getCrew()->setId($expression['crew_id']);
        }
        if (isset($expression['usergroup_id'])) {
            $searchData->getSourceUnit()->setId($expression['usergroup_id']);
        }
        if (isset($expression['subject_category'])) {
            $searchData->setSubjectCategory($expression['subject_category']);
        }
        if (isset($expression['dimension'])) {
            $searchData->setDimension($expression['dimension']);
        }
        if (isset($expression['expiration_date'])) {
            $searchData->setExpirationDate($expression['expiration_date']);
        }
        if (isset($expression['create_time'])) {
            $searchData->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $searchData->setUpdateTime($expression['update_time']);
        }
        if (isset($expression['status'])) {
            $searchData->setStatus($expression['status']);
        }
        if (isset($expression['status_time'])) {
            $searchData->setStatusTime($expression['status_time']);
        }
        if (isset($expression['hash'])) {
            $searchData->setHash($expression['hash']);
        }
        if (isset($expression['task_id'])) {
            $searchData->getTask()->setId($expression['task_id']);
        }

        return $searchData;
    }

    public function objectToArray($searchData, array $keys = array())
    {
        if (!$searchData instanceof SearchData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'infoClassify',
                'infoCategory',
                'crew',
                'sourceUnit',
                'subjectCategory',
                'dimension',
                'name',
                'identify',
                'expirationDate',
                'status',
                'createTime',
                'updateTime',
                'statusTime',
                'hash',
                'task'
            );
        }

        $expression = array();

        if (in_array('infoClassify', $keys)) {
            $expression['info_classify'] = $searchData->getInfoClassify();
        }
        if (in_array('infoCategory', $keys)) {
            $expression['info_category'] = $searchData->getInfoCategory();
        }
        if (in_array('crew', $keys)) {
            $expression['crew_id'] = $searchData->getCrew()->getId();
        }
        if (in_array('sourceUnit', $keys)) {
            $expression['usergroup_id'] = $searchData->getSourceUnit()->getId();
        }
        if (in_array('subjectCategory', $keys)) {
            $expression['subject_category'] = $searchData->getSubjectCategory();
        }
        if (in_array('dimension', $keys)) {
            $expression['dimension'] = $searchData->getDimension();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $searchData->getName();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $searchData->getIdentify();
        }
        if (in_array('expirationDate', $keys)) {
            $expression['expiration_date'] = $searchData->getExpirationDate();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $searchData->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $searchData->getUpdateTime();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $searchData->getStatus();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $searchData->getStatusTime();
        }
        if (in_array('hash', $keys)) {
            $expression['hash'] = $searchData->getHash();
        }
        if (in_array('task', $keys)) {
            $expression['task_id'] = $searchData->getTask()->getId();
        }
        
        return $expression;
    }
}

<?php
namespace BaseData\Crew\Repository;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Crew\Adapter\Crew\ICrewAdapter;
use BaseData\Crew\Adapter\Crew\CrewRestfulAdapter;

class CrewRepositoryTest extends TestCase
{
    private $repository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(CrewRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->repository);
    }

    public function testImplementsICrewAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Crew\Adapter\Crew\ICrewAdapter',
            $this->repository
        );
    }

    public function testGetActualAdapter()
    {
        Core::$container->set('env.test', '');
        $repository = new CrewRepository();
        $this->assertInstanceOf(
            'BaseData\Crew\Adapter\Crew\CrewRestfulAdapter',
            $repository->getActualAdapter()
        );

        Core::$container->set('env.test', CrewRepository::ENV_TEST);
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Crew\Adapter\Crew\CrewMockAdapter',
            $this->repository->getMockAdapter()
        );
    }

    public function testScenario()
    {
        $scenario = array();

        $adapter = $this->prophesize(CrewRestfulAdapter::class);
        $adapter->scenario(Argument::exact($scenario))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->scenario($scenario);
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->fetchList(Argument::exact($ids))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(ICrewAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }
}

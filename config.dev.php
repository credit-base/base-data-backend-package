<?php
ini_set("display_errors","on");

return [
	//database
    'database.host'     => 'credit-mysql',
    'database.port'     => 3306,
    'database.dbname'   => 'common_data_backend',
    'database.user'		=> 'root',
    'database.password'	=> '123456',
    'database.tablepre' => 'pcore_',
    'database.charset' => 'utf8mb4',
    //mongo
    'mongo.host' => '',
    'mongo.uriOptions' => [
    ],
    'mongo.driverOptions' => [
    ],
    //cache
    'cache.route.disable' => true,
    //memcached
    'memcached.service'=>[['memcached-1',11211],['memcached-2',11211]],
    //附件上传路径
    'attachment.upload.path' => '/var/www/html/attachment/upload/',
    //附件下载路径
    'attachment.download.path' => '/var/www/html/attachment/download/',

    // 'baseSdk.url' => 'http://api.base.qixinyun.com/',
    // 'sdk.url' => 'http://api.base.qixinyun.com/',
    'baseSdk.url' => 'http://common-backend-nginx/',
    'sdk.url' => 'http://common-backend-nginx/',
];
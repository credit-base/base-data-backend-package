<?php
namespace BaseData\Common\Model;

class MockEnableAbleTrait implements IEnableAble
{
    use EnableAbleTrait;

    private $status;
    private $updateTime;
    private $statusTime;

    public function getStatus() : int
    {
        return $this->status;
    }

    public function setUpdateTime($updateTime) : void
    {
        $this->updateTime = $updateTime;
    }

    public function setStatusTime($statusTime) : void
    {
        $this->statusTime = $statusTime;
    }

    public function publicUpdateStatus(int $status) : bool
    {
        return $this->updateStatus($status);
    }
}

<?php
namespace BaseData\Task\Adapter\NotifyRecord\Query\Persistence;

class MockNotifyRecordDb extends NotifyRecordDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

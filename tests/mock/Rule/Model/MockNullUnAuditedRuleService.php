<?php
namespace BaseData\Rule\Model;

class MockNullUnAuditedRuleService extends NullUnAuditedRuleService
{
    public function apply() : bool
    {
        return parent::apply();
    }

    public function approveApplyInfo(int $operationType) : bool
    {
        return parent::approveApplyInfo($operationType);
    }

    public function updateApplyStatus(int $applyStatus) : bool
    {
        return parent::updateApplyStatus($applyStatus);
    }

    public function updateRelationId() : bool
    {
        return parent::updateRelationId();
    }
}

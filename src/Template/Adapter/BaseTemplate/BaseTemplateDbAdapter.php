<?php
namespace BaseData\Template\Adapter\BaseTemplate;

use Marmot\Interfaces\INull;
use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\Template\Model\BaseTemplate;
use BaseData\Template\Model\NullBaseTemplate;
use BaseData\Template\Translator\BaseTemplateDbTranslator;
use BaseData\Template\Adapter\BaseTemplate\Query\BaseTemplateRowCacheQuery;

class BaseTemplateDbAdapter implements IBaseTemplateAdapter
{
    use DbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    public function __construct()
    {
        $this->dbTranslator = new BaseTemplateDbTranslator();
        $this->rowCacheQuery = new BaseTemplateRowCacheQuery();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
    }
    
    protected function getDbTranslator() : BaseTemplateDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : BaseTemplateRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullBaseTemplate::getInstance();
    }

    public function add(BaseTemplate $baseTemplate) : bool
    {
        return $this->addAction($baseTemplate);
    }

    public function edit(BaseTemplate $baseTemplate, array $keys = array()) : bool
    {
        return $this->editAction($baseTemplate, $keys);
    }

    public function fetchOne($id) : BaseTemplate
    {
        return $this->fetchOneAction($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->fetchListAction($ids);
    }

    protected function formatFilter(array $filter) : string
    {
        $condition = $conjection = '';

        if (!empty($filter)) {
            $baseTemplate = new BaseTemplate();

            if (isset($filter['name'])) {
                $baseTemplate->setName($filter['name']);
                $info = $this->getDbTranslator()->objectToArray($baseTemplate, array('name'));
                $condition .= $conjection.key($info).' LIKE \'%'.current($info).'%\'';
                $conjection = ' AND ';
            }
            if (isset($filter['identify'])) {
                $baseTemplate->setIdentify($filter['identify']);
                $info = $this->getDbTranslator()->objectToArray($baseTemplate, array('identify'));
                $condition .= $conjection.key($info).' = \''.current($info).'\'';
                $conjection = ' AND ';
            }
            if (isset($filter['dimension'])) {
                $baseTemplate->setDimension($filter['dimension']);
                $info = $this->getDbTranslator()->objectToArray($baseTemplate, array('dimension'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoClassify'])) {
                $baseTemplate->setInfoClassify($filter['infoClassify']);
                $info = $this->getDbTranslator()->objectToArray($baseTemplate, array('infoClassify'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
            if (isset($filter['infoCategory'])) {
                $baseTemplate->setInfoCategory($filter['infoCategory']);
                $info = $this->getDbTranslator()->objectToArray($baseTemplate, array('infoCategory'));
                $condition .= $conjection.key($info).' = '.current($info);
                $conjection = ' AND ';
            }
        }

        return empty($condition) ? ' 1 ' : $condition ;
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray(new BaseTemplate(), array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray(new BaseTemplate(), array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

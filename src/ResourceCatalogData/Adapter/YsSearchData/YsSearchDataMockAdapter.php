<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory;

class YsSearchDataMockAdapter implements IYsSearchDataAdapter
{
    public function fetchOne($id) : YsSearchData
    {
        return YsSearchDataMockFactory::generateYsSearchData($id);
    }

    public function fetchList(array $ids) : array
    {
        $ysSearchDataList = array();

        foreach ($ids as $id) {
            $ysSearchDataList[$id] = YsSearchDataMockFactory::generateYsSearchData($id);
        }

        return $ysSearchDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(YsSearchData $ysSearchData) : bool
    {
        unset($ysSearchData);
        return true;
    }

    public function edit(YsSearchData $ysSearchData, array $keys = array()) : bool
    {
        unset($ysSearchData);
        unset($keys);
        return true;
    }
}

<?php
namespace BaseData\Template\Command\Template;

use Marmot\Interfaces\ICommand;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class EditTemplateCommand implements ICommand
{
    public $id;
    /**
     * @var string $name 目录名称
     */
    public $name;
    /**
     * @var string $identify 目录标识
     */
    public $identify;
    /**
     * @var array $subjectCategory 主体类别
     */
    public $subjectCategory;
    /**
     * @var int $dimension 公开范围
     */
    public $dimension;
    /**
     * @var int $exchangeFrequency 更新频率
     */
    public $exchangeFrequency;
    /**
     * @var int $infoClassify 信息分类
     */
    public $infoClassify;
    /**
     * @var int $infoCategory 信息类别
     */
    public $infoCategory;
    /**
     * @var string $description 目录描述
     */
    public $description;
    /**
     * @var array $items 模板信息
     */
    public $items;

    public function __construct(
        string $name,
        string $identify,
        array $subjectCategory,
        int $dimension,
        int $exchangeFrequency,
        int $infoClassify,
        int $infoCategory,
        string $description,
        array $items,
        int $id
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->identify = $identify;
        $this->subjectCategory = $subjectCategory;
        $this->dimension = $dimension;
        $this->exchangeFrequency = $exchangeFrequency;
        $this->infoClassify = $infoClassify;
        $this->infoCategory = $infoCategory;
        $this->description = $description;
        $this->items = $items;
    }
}

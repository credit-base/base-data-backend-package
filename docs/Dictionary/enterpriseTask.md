# 企业数据导入任务字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")

### crew

**发布人** 

* Crew

### userGroup

**发布单位** 

* UserGroup

### total

**总数** 总数的表述.

* int

### successNumber

**成功数** 成功数的表述.

* int

### failureNumber

**失败数** 失败数的表述.

* int

### status

**状态** 状态的表述.

* int

	* DEFAULT 0 进行中,默认
	* SUCCESS 2 成功
	* FAILURE -2 失败
	* FAILURE_FILE_DOWNLOAD -3 失败文件下载成功

### scheduleTask

**调度任务id** 调度任务id的表述.

* int

### errorNumber

**错误编号** 错误编号的表述.

* int

### fileName

**文件名** 上传文件文件名的表述.

* string
* ENTERPRISE_员工id_文件内容hash.xls/FAILURE_ENTERPRISE_员工id_文件内容hash.xls

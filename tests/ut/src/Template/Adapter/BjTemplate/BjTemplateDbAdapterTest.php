<?php
namespace BaseData\Template\Adapter\BjTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\NullBjTemplate;
use BaseData\Template\Translator\BjTemplateDbTranslator;
use BaseData\Template\Adapter\BjTemplate\Query\BjTemplateRowCacheQuery;
use BaseData\Template\Repository\GbTemplateRepository;

/**
 *
 * @SuppressWarnings(PHPMD)
 * @author chloroplast
 */
class BjTemplateDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(BjTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction',
                                    'fetchGbTemplate'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IBjTemplateAdapter
     */
    public function testImplementsIBjTemplateAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 BjTemplateDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockBjTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Translator\BjTemplateDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 BjTemplateRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockBjTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\BjTemplate\Query\BjTemplateRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    /**
     * 测试是否初始化 GbTemplateRepository
     */
    public function testGetGbTemplateRepository()
    {
        $adapter = new MockBjTemplateDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Template\Repository\GbTemplateRepository',
            $adapter->getGbTemplateRepository()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockBjTemplateDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedBjTemplate = new BjTemplate();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedBjTemplate)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedBjTemplate);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedBjTemplate = new BjTemplate();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedBjTemplate, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedBjTemplate, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedBjTemplate = new BjTemplate();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedBjTemplate);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchGbTemplate')
                         ->with($expectedBjTemplate);

        //验证
        $bjTemplate = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedBjTemplate, $bjTemplate);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $bjTemplateOne = new BjTemplate(1);
        $bjTemplateTwo = new BjTemplate(2);

        $ids = [1, 2];

        $expectedBjTemplateList = [];
        $expectedBjTemplateList[$bjTemplateOne->getId()] = $bjTemplateOne;
        $expectedBjTemplateList[$bjTemplateTwo->getId()] = $bjTemplateTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedBjTemplateList);
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchGbTemplate')
                         ->with($expectedBjTemplateList);

        //验证
        $bjTemplateList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedBjTemplateList, $bjTemplateList);
    }

    //fetchGbTemplate
    public function testFetchGbTemplateIsNotArray()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchGbTemplateByObject'
                                ]
                           )
                           ->getMock();
        
        $bjTemplate = new BjTemplate();

        $adapter->expects($this->exactly(1))
                         ->method('fetchGbTemplateByObject')
                         ->with($bjTemplate);

        $adapter->fetchGbTemplate($bjTemplate);
    }

    public function testFetchGbTemplateIsArray()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                    'fetchGbTemplateByList'
                                ]
                           )
                           ->getMock();
        
        $bjTemplateOne = new BjTemplate(1);
        $bjTemplateTwo = new BjTemplate(2);

        $bjTemplateList[$bjTemplateOne->getId()] = $bjTemplateOne;
        $bjTemplateList[$bjTemplateTwo->getId()] = $bjTemplateTwo;

        $adapter->expects($this->exactly(1))
                         ->method('fetchGbTemplateByList')
                         ->with($bjTemplateList);

        $adapter->fetchGbTemplate($bjTemplateList);
    }

    //fetchGbTemplateByObject
    public function testFetchGbTemplateByObject()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                    'getGbTemplateRepository'
                                ]
                           )
                           ->getMock();

        $bjTemplate = new BjTemplate();
        $bjTemplate->setGbTemplate(new GbTemplate(1));
        
        $gbTemplateId = 1;

        // 为 GbTemplate 类建立预言
        $gbTemplate  = $this->prophesize(GbTemplate::class);
        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchOne(Argument::exact($gbTemplateId))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($gbTemplate);

        $adapter->expects($this->exactly(1))
                         ->method('getGbTemplateRepository')
                         ->willReturn($gbTemplateRepository->reveal());

        $adapter->fetchGbTemplateByObject($bjTemplate);
    }

    //fetchGbTemplateByList
    public function testFetchGbTemplateByList()
    {
        $adapter = $this->getMockBuilder(MockBjTemplateDbAdapter::class)
                           ->setMethods(
                               [
                                    'getGbTemplateRepository'
                                ]
                           )
                           ->getMock();

        $bjTemplateOne = new BjTemplate(1);
        $gbTemplateOne = new GbTemplate(1);
        $bjTemplateOne->setGbTemplate($gbTemplateOne);

        $bjTemplateTwo = new BjTemplate(2);
        $gbTemplateTwo = new GbTemplate(2);
        $bjTemplateTwo->setGbTemplate($gbTemplateTwo);
        $gbTemplateIds = [1, 2];
        
        $gbTemplateList[$gbTemplateOne->getId()] = $gbTemplateOne;
        $gbTemplateList[$gbTemplateTwo->getId()] = $gbTemplateTwo;

        $bjTemplateList[$bjTemplateOne->getId()] = $bjTemplateOne;
        $bjTemplateList[$bjTemplateTwo->getId()] = $bjTemplateTwo;

        $gbTemplateRepository = $this->prophesize(GbTemplateRepository::class);
        $gbTemplateRepository->fetchList(Argument::exact($gbTemplateIds))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($gbTemplateList);

        $adapter->expects($this->exactly(1))
                         ->method('getGbTemplateRepository')
                         ->willReturn($gbTemplateRepository->reveal());

        $adapter->fetchGbTemplateByList($bjTemplateList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 'name',
        );
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';
        
        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterIdentify()
    {
        $filter = array(
            'identify' => 'identify',
        );
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = 'identify = \''.$filter['identify'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterDimension()
    {
        $filter = array(
            'dimension' => BjTemplate::DIMENSION['SHGK']
        );
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = 'dimension = '.$filter['dimension'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoClassify()
    {
        $filter = array(
            'infoClassify' => 1
        );
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = 'info_classify = '.$filter['infoClassify'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterInfoCategory()
    {
        $filter = array(
            'infoCategory' => 1
        );
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = 'info_category = '.$filter['infoCategory'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterSourceUnit()
    {
        $filter = array(
            'sourceUnit' => 1
        );
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = 'source_unit_id = '.$filter['sourceUnit'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdDesc()
    {
        $sort = array('id' => -1);
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY bj_template_id DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortIdAsc()
    {
        $sort = array('id' => 1);
        $adapter = new MockBjTemplateDbAdapter();

        $expectedCondition = ' ORDER BY bj_template_id ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

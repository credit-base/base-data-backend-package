<?php
namespace BaseData\ResourceCatalogData\Translator;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\NullBjSearchData;

class BjSearchDataDbTranslator extends SearchDataDbTranslator
{
    public function arrayToObject(array $expression, $bjSearchData = null)
    {
        if (!isset($expression['bj_search_data_id'])) {
            return NullBjSearchData::getInstance();
        }

        if ($bjSearchData == null) {
            $bjSearchData = new BjSearchData();
        }
        
        $bjSearchData->setId($expression['bj_search_data_id']);

        if (isset($expression['bj_template_id'])) {
            $bjSearchData->getTemplate()->setId($expression['bj_template_id']);
        }
        if (isset($expression['bj_items_data_id'])) {
            $bjSearchData->getItemsData()->setId($expression['bj_items_data_id']);
        }
        if (isset($expression['description'])) {
            $bjSearchData->setDescription($expression['description']);
        }
        if (isset($expression['front_end_processor_status'])) {
            $bjSearchData->setFrontEndProcessorStatus($expression['front_end_processor_status']);
        }

        $bjSearchData = parent::arrayToObject($expression, $bjSearchData);
        
        return $bjSearchData;
    }

    public function objectToArray($bjSearchData, array $keys = array())
    {
        $searchDataExpression = parent::objectToArray($bjSearchData, $keys);

        if (!$bjSearchData instanceof BjSearchData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'template',
                'itemsData',
                'description',
                'frontEndProcessorStatus'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['bj_search_data_id'] = $bjSearchData->getId();
        }
        if (in_array('template', $keys)) {
            $expression['bj_template_id'] = $bjSearchData->getTemplate()->getId();
        }
        if (in_array('itemsData', $keys)) {
            $expression['bj_items_data_id'] = $bjSearchData->getItemsData()->getId();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $bjSearchData->getDescription();
        }
        if (in_array('frontEndProcessorStatus', $keys)) {
            $expression['front_end_processor_status'] = $bjSearchData->getFrontEndProcessorStatus();
        }

        $expression = array_merge($expression, $searchDataExpression);

        return $expression;
    }
}

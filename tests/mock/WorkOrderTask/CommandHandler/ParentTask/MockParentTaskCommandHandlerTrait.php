<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseData\Template\Model\GbTemplate;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseData\Template\Model\BjTemplate;

use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class MockParentTaskCommandHandlerTrait
{
    use ParentTaskCommandHandlerTrait;

    public function getGbTemplateRepositoryPublic() : IGbTemplateAdapter
    {
        return $this->getGbTemplateRepository();
    }

    public function getBjTemplateRepositoryPublic() : IBjTemplateAdapter
    {
        return $this->getBjTemplateRepository();
    }

    public function getUserGroupRepositoryPublic() : IUserGroupAdapter
    {
        return $this->getUserGroupRepository();
    }

    public function fetchOneGbTemplatePublic($id) : GbTemplate
    {
        return $this->fetchOneGbTemplate($id);
    }

    public function fetchOneBjTemplatePublic($id) : BjTemplate
    {
        return $this->fetchOneBjTemplate($id);
    }

    public function fetchListUserGroupPublic(array $ids) : array
    {
        return $this->fetchListUserGroup($ids);
    }
}

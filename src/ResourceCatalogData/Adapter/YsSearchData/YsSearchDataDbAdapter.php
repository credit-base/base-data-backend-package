<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Model\NullYsSearchData;
use BaseData\ResourceCatalogData\Repository\YsItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\SearchDataDbAdapterTrait;
use BaseData\ResourceCatalogData\Translator\YsSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\YsSearchDataRowCacheQuery;

use BaseData\Template\Repository\WbjTemplateRepository;

class YsSearchDataDbAdapter implements IYsSearchDataAdapter
{
    use DbAdapterTrait, SearchDataDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $ysItemsDataRepository;

    private $wbjTemplateRepository;

    public function __construct()
    {
        $this->dbTranslator = new YsSearchDataDbTranslator();
        $this->rowCacheQuery = new YsSearchDataRowCacheQuery();
        $this->ysItemsDataRepository = new YsItemsDataRepository();
        $this->wbjTemplateRepository = new WbjTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->ysItemsDataRepository);
        unset($this->wbjTemplateRepository);
    }
    
    protected function getDbTranslator() : YsSearchDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : YsSearchDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullYsSearchData::getInstance();
    }

    protected function getItemsDataRepository() : YsItemsDataRepository
    {
        return $this->ysItemsDataRepository;
    }

    protected function getTemplateRepository() : WbjTemplateRepository
    {
        return $this->wbjTemplateRepository;
    }

    public function add(YsSearchData $ysSearchData) : bool
    {
        return $this->addAction($ysSearchData);
    }

    public function edit(YsSearchData $ysSearchData, array $keys = array()) : bool
    {
        return $this->editAction($ysSearchData, $keys);
    }

    public function fetchOne($id) : YsSearchData
    {
        $ysSearchData = $this->fetchOneAction($id);
        $this->fetchTemplate($ysSearchData);
        $this->fetchItemsData($ysSearchData);
        
        return $ysSearchData;
    }

    public function fetchList(array $ids) : array
    {
        $ysSearchDataList = $this->fetchListAction($ids);
        $this->fetchTemplate($ysSearchDataList);
        $this->fetchItemsData($ysSearchDataList);

        return $ysSearchDataList;
    }

    protected function formatFilter(array $filter) : string
    {
        $ysSearchData = new YsSearchData();

        return $this->searchDataFormatFilter($filter, $ysSearchData);
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $ysSearchData = new YsSearchData();
 
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray($ysSearchData, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($ysSearchData, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }
        
        return $condition;
    }
}

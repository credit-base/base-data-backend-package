<?php
namespace BaseData\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Common\Controller\IOperateController;

class OperationControllerTest extends TestCase
{
    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(OperateController::class)
                                 ->setMethods(['getController'])
                                 ->getMock();
                                 
        $this->childController = new class extends OperateController
        {
            public function getController(string $resource) : IOperateController
            {
                return parent::getController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
    }

    public function testGetController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IOperateController',
            $this->childController->getController($this->resource)
        );
    }

    public function testAdd()
    {
        $operateController = $this->prophesize(IOperateController::class);
        $operateController->add()->shouldBeCalledTimes(1);
        $this->controller->expects($this->once())
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($operateController->reveal());

        $this->controller->add($this->resource);
    }

    public function testEdit()
    {
        $id = 1;

        $operateController = $this->prophesize(IOperateController::class);
        $operateController->edit(Argument::exact($id))->shouldBeCalledTimes(1);
        $this->controller->expects($this->once())
                         ->method('getController')
                         ->with($this->resource)
                         ->willReturn($operateController->reveal());

        $this->controller->edit($this->resource, $id);
    }
}

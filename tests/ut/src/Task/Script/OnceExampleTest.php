<?php
namespace BaseData\Task\Script;

use PHPUnit\Framework\TestCase;

class OnceExampleTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new OnceExample();
    }

    public function testExtendsOnceTask()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Model\OnceTask',
            $this->task
        );
    }

    public function testExecuteAction()
    {
        $result = $this->task->executeAction();

        $this->assertTrue($result);
    }
}

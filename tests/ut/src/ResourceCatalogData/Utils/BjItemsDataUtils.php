<?php
namespace BaseData\ResourceCatalogData\Utils;

trait BjItemsDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $bjItemsData
    ) {
        $this->assertEquals($expectedArray['bj_items_data_id'], $bjItemsData->getId());

        $data = array();
        if (is_string($expectedArray['data'])) {
            $data = unserialize(gzuncompress(base64_decode($expectedArray['data'], true)));
        }
        if (is_array($expectedArray['data'])) {
            $data = $expectedArray['data'];
        }

        $this->assertEquals($data, $bjItemsData->getData());
    }
}

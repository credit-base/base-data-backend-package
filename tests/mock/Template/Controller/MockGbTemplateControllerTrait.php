<?php
namespace BaseData\Template\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Template\WidgetRule\TemplateWidgetRule;
use BaseData\Template\Repository\GbTemplateRepository;

class MockGbTemplateControllerTrait extends Controller
{
    use GbTemplateControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getTemplateWidgetRulePublic() : TemplateWidgetRule
    {
        return $this->getTemplateWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : GbTemplateRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}

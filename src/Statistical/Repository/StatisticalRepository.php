<?php
namespace BaseData\Statistical\Repository;

use BaseData\Statistical\Model\Statistical;
use BaseData\Statistical\Adapter\Statistical\IStatisticalAdapter;

class StatisticalRepository implements IStatisticalAdapter
{
    private $adapter;
    
    public function __construct(IStatisticalAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setAdapter(IStatisticalAdapter $adapter) : void
    {
        $this->adapter = $adapter;
    }

    private function getAdapter() : IStatisticalAdapter
    {
        return $this->adapter;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        return $this->getAdapter()->analyse($filter);
    }
}

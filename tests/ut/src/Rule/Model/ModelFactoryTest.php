<?php
namespace BaseData\Rule\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Model\NullRule;

class ModelFactoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new ModelFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullModel()
    {
        $stub = $this->stub->getModel(0);
            $this->assertInstanceOf(
                'BaseData\Rule\Model\NullRule',
                $stub
            );
    }

    public function testGetModel()
    {
        foreach (ModelFactory::MAPS as $key => $model) {
            $this->assertInstanceOf(
                $model,
                $this->stub->getModel($key)
            );
        }
    }
}

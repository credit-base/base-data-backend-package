<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query\Persistence;

use PHPUnit\Framework\TestCase;

class BaseTemplateDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockBaseTemplateDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(BaseTemplateDb::TABLE, $this->database->getTable());
    }
}

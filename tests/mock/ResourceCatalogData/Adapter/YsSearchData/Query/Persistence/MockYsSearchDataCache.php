<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\Persistence;

class MockYsSearchDataCache extends YsSearchDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

<?php
namespace BaseData\Template\CommandHandler\GbTemplate;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\Template\CommandHandler\SetTemplateItemsDefaultInfo;
use BaseData\Template\Command\GbTemplate\AddGbTemplateCommand;
use BaseData\Template\Model\GbTemplate;

class AddGbTemplateCommandHandler implements ICommandHandler
{
    use SetTemplateItemsDefaultInfo;
    
    protected function getGbTemplate() : GbTemplate
    {
        return new GbTemplate();
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof AddGbTemplateCommand)) {
            throw new \InvalidArgumentException;
        }

        $gbTemplate = $this->getGbTemplate();

        $gbTemplate->setName($command->name);
        $gbTemplate->setIdentify($command->identify);
        $gbTemplate->setSubjectCategory($command->subjectCategory);
        $gbTemplate->setDimension($command->dimension);
        $gbTemplate->setExchangeFrequency($command->exchangeFrequency);
        $gbTemplate->setInfoClassify($command->infoClassify);
        $gbTemplate->setInfoCategory($command->infoCategory);
        $gbTemplate->setDescription($command->description);
        $items = $this->setDefaultInfo($command);
        $gbTemplate->setItems($items);

        if ($gbTemplate->add()) {
            $command->id = $gbTemplate->getId();
            return true;
        }
        return false;
    }
}

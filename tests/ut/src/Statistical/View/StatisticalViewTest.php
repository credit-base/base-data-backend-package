<?php
namespace BaseData\Statistical\View;

use PHPUnit\Framework\TestCase;

use BaseData\Statistical\Model\Statistical;

class StatisticalViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $statistical = new StatisticalView(new Statistical());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $statistical);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class GbSearchDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'gb_search_data_id',
            new Persistence\GbSearchDataCache(),
            new Persistence\GbSearchDataDb()
        );
    }
}

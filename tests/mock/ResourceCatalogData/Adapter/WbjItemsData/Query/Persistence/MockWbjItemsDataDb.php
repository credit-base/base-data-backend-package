<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\Persistence;

class MockWbjItemsDataDb extends WbjItemsDataDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

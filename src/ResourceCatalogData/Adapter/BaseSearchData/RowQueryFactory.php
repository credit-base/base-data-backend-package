<?php
namespace BaseData\ResourceCatalogData\Adapter\BaseSearchData;

use Marmot\Framework\Query\RowCacheQuery;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

use BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery;

class RowQueryFactory
{
    const MAPS = array(
        ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'] =>
            '\BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery',
        ISearchDataAble::SUBJECT_CATEGORY['GTGSH'] =>
            '\BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery',
    );

    public function getRowQuery(int $category) : RowCacheQuery
    {
        $rowQuery = isset(self::MAPS[$category]) ? self::MAPS[$category] : '';
        return class_exists($rowQuery) ? new $rowQuery : new EnterpriseRowCacheQuery();
    }
}

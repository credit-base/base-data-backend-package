<?php
namespace BaseData\Rule\Adapter\RuleService\Query\Persistence;

use PHPUnit\Framework\TestCase;

class RuleServiceDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockRuleServiceDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(RuleServiceDb::TABLE, $this->database->getTable());
    }
}

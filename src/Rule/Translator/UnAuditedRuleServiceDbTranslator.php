<?php
namespace BaseData\Rule\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Rule\Model\UnAuditedRuleService;
use BaseData\Rule\Model\NullUnAuditedRuleService;

/**
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class UnAuditedRuleServiceDbTranslator implements ITranslator
{
    use RuleServiceTranslatorTrait;

    public function arrayToObject(array $expression, $ruleService = null)
    {
        if (!isset($expression['apply_form_rule_id'])) {
            return NullUnAuditedRuleService::getInstance();
        }

        if ($ruleService == null) {
            $ruleService = new UnAuditedRuleService();
        }

        $ruleService->setId($expression['apply_form_rule_id']);
        $ruleService->setApplyId($expression['apply_form_rule_id']);

        if (isset($expression['publish_crew_id'])) {
            $ruleService->getPublishCrew()->setId($expression['publish_crew_id']);
        }
        if (isset($expression['usergroup_id'])) {
            $ruleService->getUserGroup()->setId($expression['usergroup_id']);
        }
        if (isset($expression['apply_crew_id'])) {
            $ruleService->getApplyCrew()->setId($expression['apply_crew_id']);
        }
        if (isset($expression['apply_usergroup_id'])) {
            $ruleService->getApplyUserGroup()->setId($expression['apply_usergroup_id']);
        }
        if (isset($expression['operation_type'])) {
            $ruleService->setOperationType($expression['operation_type']);
        }
        if (isset($expression['apply_info'])) {
            $ruleService->setApplyInfo(
                unserialize(gzuncompress(base64_decode(
                    $expression['apply_info'],
                    true
                )))
            );
        }
        if (isset($expression['transformation_category'])) {
            $ruleService->setTransformationCategory($expression['transformation_category']);
        }
        if (isset($expression['transformation_template_id'])) {
            $ruleService->getTransformationTemplate()->setId($expression['transformation_template_id']);
        }
        if (isset($expression['source_category'])) {
            $ruleService->setSourceCategory($expression['source_category']);
        }
        if (isset($expression['source_template_id'])) {
            $ruleService->getSourceTemplate()->setId($expression['source_template_id']);
        }
        if (isset($expression['relation_id'])) {
            $ruleService->setRelationId($expression['relation_id']);
        }
        if (isset($expression['reject_reason'])) {
            $ruleService->setRejectReason($expression['reject_reason']);
        }
        if (isset($expression['apply_status'])) {
            $ruleService->setApplyStatus($expression['apply_status']);
        }
        if (isset($expression['create_time'])) {
            $ruleService->setCreateTime($expression['create_time']);
        }
        if (isset($expression['update_time'])) {
            $ruleService->setUpdateTime($expression['update_time']);
        }
        if (isset($expression['status_time'])) {
            $ruleService->setStatusTime($expression['status_time']);
        }

        return $ruleService;
    }

    public function arrayToObjects(array $expression, $ruleService = null)
    {
        if (isset($expression['rule_id'])) {
            $ruleService->setId($expression['rule_id']);
        }
        
        $this->arrayToObjectUtils($ruleService, $expression);

        if (isset($expression['publish_crew_id'])) {
            $ruleService->getCrew()->setId($expression['publish_crew_id']);
        }
        
        return $ruleService;
    }

    public function objectToArray($ruleService, array $keys = array())
    {
        if (!$ruleService instanceof UnAuditedRuleService) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'applyId',
                'publishCrew',
                'userGroup',
                'applyCrew',
                'applyUserGroup',
                'operationType',
                'applyInfo',
                'transformationCategory',
                'transformationTemplate',
                'relationId',
                'sourceCategory',
                'sourceTemplate',
                'rejectReason',
                'applyStatus',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('applyId', $keys)) {
            $expression['apply_form_rule_id'] = $ruleService->getApplyId();
        }
        if (in_array('publishCrew', $keys)) {
            $expression['publish_crew_id'] = $ruleService->getPublishCrew()->getId();
        }
        if (in_array('userGroup', $keys)) {
            $expression['usergroup_id'] = $ruleService->getUserGroup()->getId();
        }
        if (in_array('applyCrew', $keys)) {
            $expression['apply_crew_id'] = $ruleService->getApplyCrew()->getId();
        }
        if (in_array('applyUserGroup', $keys)) {
            $expression['apply_usergroup_id'] = $ruleService->getApplyUserGroup()->getId();
        }
        if (in_array('operationType', $keys)) {
            $expression['operation_type'] = $ruleService->getOperationType();
        }
        if (in_array('applyInfo', $keys)) {
            $expression['apply_info'] = base64_encode(gzcompress(serialize($ruleService->getApplyInfo())));
        }
        if (in_array('transformationCategory', $keys)) {
            $expression['transformation_category'] = $ruleService->getTransformationCategory();
        }
        if (in_array('transformationTemplate', $keys)) {
            $expression['transformation_template_id'] = $ruleService->getTransformationTemplate()->getId();
        }
        if (in_array('relationId', $keys)) {
            $expression['relation_id'] = $ruleService->getRelationId();
        }
        if (in_array('sourceCategory', $keys)) {
            $expression['source_category'] = $ruleService->getSourceCategory();
        }
        if (in_array('sourceTemplate', $keys)) {
            $expression['source_template_id'] = $ruleService->getSourceTemplate()->getId();
        }
        if (in_array('rejectReason', $keys)) {
            $expression['reject_reason'] = $ruleService->getRejectReason();
        }
        if (in_array('applyStatus', $keys)) {
            $expression['apply_status'] = $ruleService->getApplyStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $ruleService->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $ruleService->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $ruleService->getStatusTime();
        }

        return $expression;
    }
}

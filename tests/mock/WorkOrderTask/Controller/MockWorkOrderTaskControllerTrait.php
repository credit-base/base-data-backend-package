<?php
namespace BaseData\WorkOrderTask\Controller;

use Marmot\Framework\Classes\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;

class MockWorkOrderTaskControllerTrait extends Controller
{
    use WorkOrderTaskControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getWorkOrderTaskWidgetRulePublic() : WorkOrderTaskWidgetRule
    {
        return $this->getWorkOrderTaskWidgetRule();
    }

    public function getCommonWidgetRulePublic() : CommonWidgetRule
    {
        return $this->getCommonWidgetRule();
    }

    public function getRepositoryPublic() : WorkOrderTaskRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }
}

<?php
namespace BaseData\WorkOrderTask;

use BaseData\WorkOrderTask\Model\WorkOrderTask;

trait WorkOrderTaskUtils
{
    private function parentTaskCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['parent_task_id'], $responseDataArray['id']);
        $this->assertEquals('parentTasks', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['template_type'], $attributes['templateType']);
        $this->assertEquals($setDataArray['title'], $attributes['title']);
        $this->assertEquals($setDataArray['description'], $attributes['description']);
        $this->assertEquals($setDataArray['end_time'], $attributes['endTime']);
        $this->assertEquals(json_decode($setDataArray['attachment'], true), $attributes['attachment']);
        $this->assertEquals(0, $attributes['ratio']);
        $this->assertEquals($setDataArray['reason'], $attributes['reason']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsTemplate = $responseDataArray['relationships']['template']['data'];
        $this->assertEquals('bjTemplates', $relationshipsTemplate['type']);
        $this->assertEquals($setDataArray['template_id'], $relationshipsTemplate['id']);

        $relationshipsAssignObjects = $responseDataArray['relationships']['assignObjects']['data'];
        $assignObjects = json_decode($setDataArray['assign_objects'], true);

        foreach ($relationshipsAssignObjects as $key => $assignObject) {
            $this->assertEquals('userGroups', $assignObject['type']);
            $this->assertEquals($assignObjects[$key], $assignObject['id']);
        }
    }

    private function workOrderTaskCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['work_order_task_id'], $responseDataArray['id']);
        $this->assertEquals('workOrderTasks', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];
        $this->assertEquals($setDataArray['reason'], $attributes['reason']);
        $this->assertEquals(json_decode($setDataArray['feedback_records'], true), $attributes['feedbackRecords']);

        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);

        $relationshipsTemplate = $responseDataArray['relationships']['template']['data'];
        $this->assertEquals('bjTemplates', $relationshipsTemplate['type']);
        $this->assertEquals($setDataArray['template_id'], $relationshipsTemplate['id']);

        $relationshipsAssignObject = $responseDataArray['relationships']['assignObject']['data'];
        $this->assertEquals('userGroups', $relationshipsAssignObject['type']);
        $this->assertEquals($setDataArray['assign_object_id'], $relationshipsAssignObject['id']);

        $relationshipsParentTask = $responseDataArray['relationships']['parentTask']['data'];
        $this->assertEquals('parentTasks', $relationshipsParentTask['type']);
        $this->assertEquals($setDataArray['parent_task_id'], $relationshipsParentTask['id']);
    }
}

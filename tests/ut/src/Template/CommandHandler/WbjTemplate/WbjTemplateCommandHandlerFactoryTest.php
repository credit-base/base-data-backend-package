<?php
namespace BaseData\Template\CommandHandler\WbjTemplate;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use BaseData\Template\Command\WbjTemplate\AddWbjTemplateCommand;
use BaseData\Template\Command\WbjTemplate\EditWbjTemplateCommand;

class WbjTemplateCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WbjTemplateCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testAddWbjTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new AddWbjTemplateCommand(
                $this->faker->word,
                $this->faker->regexify('[A-Z_]{1,100}'),
                $this->faker->randomElements(array(1,2,3), 3),
                $this->faker->randomElement(array(1,2,3)),
                $this->faker->randomDigitNotNull,
                $this->faker->randomDigitNotNull,
                $this->faker->randomDigitNotNull,
                $this->faker->sentence,
                $this->faker->randomElements(array('1','2','3'), 2),
                $this->faker->randomDigitNotNull
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\Template\CommandHandler\WbjTemplate\AddWbjTemplateCommandHandler',
            $commandHandler
        );
    }

    public function testEditWbjTemplateCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EditWbjTemplateCommand(
                $this->faker->word,
                $this->faker->regexify('[A-Z_]{1,100}'),
                $this->faker->randomElements(array(1,2,3), 2),
                $this->faker->randomElement(array(1,2,3)),
                $this->faker->randomDigitNot(0),
                $this->faker->randomDigit,
                $this->faker->randomDigitNot(0),
                $this->faker->sentence,
                $this->faker->randomElements(array('1','2','3'), 2),
                $this->faker->randomDigitNot(0),
                $this->faker->randomDigitNot(0)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\Template\CommandHandler\WbjTemplate\EditWbjTemplateCommandHandler',
            $commandHandler
        );
    }
}

<?php
namespace BaseData\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class BjItemsDataSchema extends SchemaProvider
{
    protected $resourceType = 'bjItemsData';

    public function getId($bjItemsData) : int
    {
        return $bjItemsData->getId();
    }

    public function getAttributes($bjItemsData) : array
    {
        return [
            'data' => $bjItemsData->getData()
        ];
    }
}

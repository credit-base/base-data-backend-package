<?php
namespace BaseData\Template\BaseTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Translator\BaseTemplateDbTranslator;

/**
 * @Feature: 我是超级管理员,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 通过信息分类搜索
 */
class InfoClassifyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBaseTemplateList()
    {
        $repository = new BaseTemplateRepository();
 
        $filter['infoClassify'] = Template::INFO_CLASSIFY['QT'];

        list($baseTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $baseTemplateList;
    }

    /**
     * @Then: 我可以看到信息分类为其他的所有资源目录
     */
    public function testViewBaseTemplateList()
    {
        $classifyBaseTemplateList = $this->baseTemplate();

        foreach ($classifyBaseTemplateList as $key => $classifyBaseTemplate) {
            $classifyBaseTemplateList[$key]['base_template_id'] = $key +1;
            $classifyBaseTemplateList[$key]['subject_category'] = json_decode(
                $classifyBaseTemplate['subject_category'],
                true
            );
            $classifyBaseTemplateList[$key]['items'] = json_decode($classifyBaseTemplate['items'], true);
        }

        $baseTemplateList = $this->fetchBaseTemplateList();
        $translator = new BaseTemplateDbTranslator();
        foreach ($baseTemplateList as $baseTemplate) {
            $baseTemplateArray[] = $translator->objectToArray($baseTemplate);
        }

        $this->assertEquals($baseTemplateArray, $classifyBaseTemplateList);

        foreach ($baseTemplateArray as $baseTemplate) {
            $this->assertEquals(Template::INFO_CLASSIFY['QT'], $baseTemplate['info_classify']);
        }
    }
}

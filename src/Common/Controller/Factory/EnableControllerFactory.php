<?php
namespace BaseData\Common\Controller\Factory;

use BaseData\Common\Controller\NullEnableController;
use BaseData\Common\Controller\Interfaces\IEnableAbleController;

class EnableControllerFactory
{
    const MAPS = array(
        'ysSearchData'=>'BaseData\ResourceCatalogData\Controller\YsSearchDataEnableController',
        'wbjSearchData'=>'BaseData\ResourceCatalogData\Controller\WbjSearchDataEnableController'
    );

    public static function getController(string $resource) : IEnableAbleController
    {
        $enableController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        
        return class_exists($enableController) ? new $enableController : NullEnableController::getInstance();
    }
}

<?php
namespace BaseData\Template\Model;

use Marmot\Core;

use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Repository\WbjTemplateRepository;

use BaseData\UserGroup\Model\UserGroup;

class WbjTemplate extends Template implements IOperate
{
    use OperateTrait;

    /**
     * @var int $sourceUnit 来源单位
     */
    protected $sourceUnit;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->sourceUnit = new UserGroup();
        $this->repository = new WbjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->sourceUnit);
        unset($this->repository);
    }

    public function setSourceUnit(UserGroup $sourceUnit) : void
    {
        $this->sourceUnit = $sourceUnit;
    }

    public function getSourceUnit() : UserGroup
    {
        return $this->sourceUnit;
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['WBJ'];
    }

    protected function getRepository() : IWbjTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        if (!$this->getRepository()->add($this)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'identify'));
            return false;
        }
        return true;
    }

    /**
     * @todo 编辑时，本次未考虑目录版本化
     */
    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));

        if (!$this->getRepository()->edit(
            $this,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'sourceUnit',
                'updateTime'
            )
        )) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'identify'));
            return false;
        }
        return true;
    }
}

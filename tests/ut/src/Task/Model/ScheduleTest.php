<?php
namespace BaseData\Task\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Task\Adapter\Task\ITaskAdapter;

/**
 * BaseData\Task\Model\Schedule.class.php 测试文件
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @author chloroplast
 */
class ScheduleTest extends TestCase
{
    private $schedule;

    public function setUp()
    {
        $this->schedule = new MockSchedule();
    }

    public function tearDown()
    {
        unset($this->schedule);
    }

    public function testConstructor()
    {
        $this->assertInstanceof(
            'BaseData\Task\Adapter\Task\ITaskAdapter',
            $this->schedule->getRepository()
        );

        $this->assertInstanceof(
            'BaseData\Task\Model\RunTime',
            $this->schedule->getRunTime()
        );
    }

    public function testDispatch()
    {
        $list = ['list'];

        $this->schedule = $this->getMockBuilder(MockSchedule::class)
            ->setMethods(['getRepository'])
            ->getMock();
        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->schedule()->shouldBeCalledTimes(1)->willReturn($list);

        $this->schedule->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->schedule->dispatch();
        
        $this->assertEquals($list, $result);
    }
}

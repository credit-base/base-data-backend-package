<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData\Query;

use Marmot\Framework\Query\RowCacheQuery;

class YsItemsDataRowCacheQuery extends RowCacheQuery
{
    public function __construct()
    {
        parent::__construct(
            'ys_items_data_id',
            new Persistence\YsItemsDataCache(),
            new Persistence\YsItemsDataDb()
        );
    }
}

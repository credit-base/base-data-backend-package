<?php
namespace BaseData\WorkOrderTask\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\WidgetRule\WorkOrderTaskWidgetRule;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Command\WorkOrderTask\RevokeWorkOrderTaskCommand;
use BaseData\WorkOrderTask\Command\WorkOrderTask\ConfirmWorkOrderTaskCommand;
use BaseData\WorkOrderTask\Command\WorkOrderTask\EndWorkOrderTaskCommand;
use BaseData\WorkOrderTask\Command\WorkOrderTask\FeedbackWorkOrderTaskCommand;

class WorkOrderTaskProgressControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new WorkOrderTaskProgressController();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    /**
     * 测试 revoke 成功
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、getRepository、render方法
     * 2. 调用 $this->initRevoke(), 期望结果为 true
     * 3. 为 WorkOrderTask 类建立预言
     * 4. 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
     *    且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->revoke 方法被调用1次, 且返回结果为 true
     */
    public function testRevoke()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initRevoke($controller, $id, true);

        // 为 WorkOrderTask 类建立预言
        $workOrderTask = $this->prophesize(WorkOrderTask::class);

        // 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
        // 且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($workOrderTask);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->revoke 方法被调用一次, 且返回结果为 true
        $result = $controller->revoke($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 revoke 失败
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、displayError 方法
     * 2. 调用 $this->initRevoke(), 期望结果为 false
     * 3. displayError 方法被调用1次, 且controller返回结果为 false
     * 4. controller->revoke 方法被调用1次, 且返回结果为 false
     */
    public function testRevokeFail()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initRevoke(), 期望结果为 false
        $this->initRevoke($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->revoke 方法被调用一次, 且返回结果为 false
        $result = $controller->revoke($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 revoke 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数, getCommonWidgetRule 方法被调用1次
     * 4. 为 CommandBus 类建立预言, 传入 RevokeWorkOrderTaskCommand参数, 且 send 方法被调1次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initRevoke(WorkOrderTaskProgressController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->initCommonWidgetRule($attributes, $controller);

        // 为 CommandBus 类建立预言, 传入 RevokeWorkOrderTaskCommand参数, 且 send 方法被调用1次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用1次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new RevokeWorkOrderTaskCommand(
                    $attributes['reason'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 confirm 成功
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿getCommandBus、displayError 方法
     * 2. 调用 $this->initRevoke(), 期望结果为 true
     * 3. 为 WorkOrderTask 类建立预言
     * 4. 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
     *    且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->confirm 方法被调用1次, 且返回结果为 true
     */
    public function testConfirm()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿getRepository、render方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initConfirm($controller, $id, true);

        // 为 WorkOrderTask 类建立预言
        $workOrderTask = $this->prophesize(WorkOrderTask::class);

        // 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
        // 且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($workOrderTask);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->confirm 方法被调用一次, 且返回结果为 true
        $result = $controller->confirm($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 confirm 失败
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿getCommandBus、displayError 方法
     * 2. 调用 $this->initConfirm(), 期望结果为 false
     * 3. displayError 方法被调用1次, 且controller返回结果为 false
     * 4. controller->confirm 方法被调用1次, 且返回结果为 false
     */
    public function testConfirmFail()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initConfirm(), 期望结果为 false
        $this->initConfirm($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->confirm 方法被调用一次, 且返回结果为 false
        $result = $controller->confirm($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 confirm 方法
     * 1. 为 CommandBus 类建立预言, 传入 ConfirmWorkOrderTaskCommand参数, 且 send 方法被调1次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initConfirm(WorkOrderTaskProgressController $controller, int $id, bool $result)
    {
        // 为 CommandBus 类建立预言, 传入 ConfirmWorkOrderTaskCommand参数, 且 send 方法被调用1次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用1次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ConfirmWorkOrderTaskCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    /**
     * 测试 end 成功
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、getRepository、render方法
     * 2. 调用 $this->initRevoke(), 期望结果为 true
     * 3. 为 WorkOrderTask 类建立预言
     * 4. 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
     *    且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->end 方法被调用1次, 且返回结果为 true
     */
    public function testEnd()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initEnd($controller, $id, true);

        // 为 WorkOrderTask 类建立预言
        $workOrderTask = $this->prophesize(WorkOrderTask::class);

        // 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
        // 且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($workOrderTask);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->end 方法被调用一次, 且返回结果为 true
        $result = $controller->end($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 end 失败
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
     *    getCommandBus、displayError 方法
     * 2. 调用 $this->initEnd(), 期望结果为 false
     * 3. displayError 方法被调用1次, 且controller返回结果为 false
     * 4. controller->end 方法被调用1次, 且返回结果为 false
     */
    public function testEndFail()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getCommonWidgetRule、
        // getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getCommonWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initEnd(), 期望结果为 false
        $this->initEnd($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->end 方法被调用一次, 且返回结果为 false
        $result = $controller->end($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 end 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数, getCommonWidgetRule 方法被调用1次
     * 4. 为 CommandBus 类建立预言, 传入 EndWorkOrderTaskCommand参数, 且 send 方法被调1次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initEnd(WorkOrderTaskProgressController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestData();
        $attributes = $data['attributes'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->initCommonWidgetRule($attributes, $controller);

        // 为 CommandBus 类建立预言, 传入 EndWorkOrderTaskCommand参数, 且 send 方法被调用1次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用1次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EndWorkOrderTaskCommand(
                    $attributes['reason'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    private function mockRequestData() : array
    {
        return array(
            "attributes" => array(
                "reason" => '原因'
            )
        );
    }

    private function initCommonWidgetRule(array $attributes, $controller)
    {
        $reason = $attributes['reason'];

        // 为 CommonWidgetRule 类建立预言, 验证请求参数,  getCommonWidgetRule 方法被调用1次
        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->reason(Argument::exact($reason))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());
    }

    /**
     * 测试 feedback 成功
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getWorkOrderTaskWidgetRule、
     *    getCommandBus、getRepository、render方法
     * 2. 调用 $this->initRevoke(), 期望结果为 true
     * 3. 为 WorkOrderTask 类建立预言
     * 4. 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
     *    且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
     * 5. render 方法被调用一次, 且controller返回结果为 true
     * 6. controller->feedback 方法被调用1次, 且返回结果为 true
     */
    public function testFeedback()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getWorkOrderTaskWidgetRule、
        // getCommandBus、getRepository、render方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getWorkOrderTaskWidgetRule',
                    'getCommandBus',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        $id = 1;

        $this->initFeedback($controller, $id, true);

        // 为 WorkOrderTask 类建立预言
        $workOrderTask = $this->prophesize(WorkOrderTask::class);

        // 为 WorkOrderTaskRepository 类建立预言, WorkOrderTaskRepository->fetchOne 方法被调用1次,
        // 且 返回结果为 预言的WorkOrderTask, getRepository 方法被调用1次
        $repository = $this->prophesize(WorkOrderTaskRepository::class);
        $repository->fetchOne(Argument::exact($id))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($workOrderTask);
        $controller->expects($this->once())
                             ->method('getRepository')
                             ->willReturn($repository->reveal());

        // render 方法被调用一次, 且controller返回结果为 true
        $controller->expects($this->exactly(1))
            ->method('render')
            ->willReturn(true);

        // controller->feedback 方法被调用一次, 且返回结果为 true
        $result = $controller->feedback($id);
        $this->assertTrue($result);
    }

    /**
     * 测试 feedback 失败
     * 1. 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getWorkOrderTaskWidgetRule、
     *    getCommandBus、displayError 方法
     * 2. 调用 $this->initFeedback(), 期望结果为 false
     * 3. displayError 方法被调用1次, 且controller返回结果为 false
     * 4. controller->feedback 方法被调用1次, 且返回结果为 false
     */
    public function testFeedbackFail()
    {
        // 为 WorkOrderTaskProgressController 类建立桩件, 并模仿 getRequest、getWorkOrderTaskWidgetRule、
        // getCommandBus、displayError 方法
        $controller = $this->getMockBuilder(WorkOrderTaskProgressController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getWorkOrderTaskWidgetRule',
                    'getCommandBus',
                    'displayError'
                ]
            )->getMock();
        $id = 1;

        // 调用 $this->initFeedback(), 期望结果为 false
        $this->initFeedback($controller, $id, false);

        // displayError 方法被调用一次, 且controller返回结果为 false
        $controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        // controller->feedback 方法被调用一次, 且返回结果为 false
        $result = $controller->feedback($id);
        $this->assertFalse($result);
    }

    /**
     * 初始化 feedback 方法
     * 1. mock 请求参数
     * 2. 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
     * 3. 为 CommonWidgetRule 类建立预言, 验证请求参数, getWorkOrderTaskWidgetRule 方法被调用1次
     * 4. 为 CommandBus 类建立预言, 传入 FeedbackWorkOrderTaskCommand参数, 且 send 方法被调1次,
     *    且返回结果为预期结果$result, getCommandBus 方法被调用一次
     */
    protected function initFeedback(WorkOrderTaskProgressController $controller, int $id, bool $result)
    {
        // mock 请求参数
        $data = $this->mockRequestFeedbackData();
        $attributes = $data['attributes'];

        // 为 Request 类建立预言, 验证请求参数, getRequest 方法被调用1次
        $request = $this->prophesize(Request::class);
        $request->patch(Argument::exact('data'))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);
        $controller->expects($this->exactly(1))
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->initWorkOrderTaskWidgetRule($attributes, $controller);

        // 为 CommandBus 类建立预言, 传入 FeedbackWorkOrderTaskCommand参数, 且 send 方法被调用1次,
        // 且返回结果为预期结果$result, getCommandBus 方法被调用1次
        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new FeedbackWorkOrderTaskCommand(
                    $attributes['feedbackRecords'],
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);
        $controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    private function mockRequestFeedbackData() : array
    {
        return array(
            "attributes" => array(
                "feedbackRecords" => array(  //反馈记录
                    array(
                        'crew' => 1,  //反馈人
                        'userGroup' => 1,  //反馈委办局
                        'isExistedTemplate' => 0,  //是否已存在目录，是 1 | 否 0
                        'templateId' => '',  //目录id
                        'items' => array(
                            array(
                                "name" => '主体名称',    //信息项名称
                                "identify" => 'ZTMC',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '200',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 0,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(),    //脱敏规则
                                "remarks" => '信用主体名称',    //备注
                            ),
                            array(
                                "name" => '统一社会信用代码',    //信息项名称
                                "identify" => 'TYSHXYDM',    //数据标识
                                "type" => 1,    //数据类型
                                "length" => '50',    //数据长度
                                "options" => array(),    //可选范围
                                "dimension" => 1,    //公开范围，社会公开 1 | 政务共享 2 | 授权查询 3
                                "isNecessary" => 1,    //是否必填，否 0 | 默认 是 1
                                "isMasked" => 1,    //是否脱敏，默认 否 0 | 是 1
                                "maskRule" => array(3, 4),    //脱敏规则，即左边保留3位字符，右边保留4位字符，如913***********HE86
                                "remarks" => '信用主体代码',    //备注
                            )
                        ),  //信息项
                        'reason' => '反馈原因',  //反馈原因
                    )
                )
            )
        );
    }

    private function initWorkOrderTaskWidgetRule(array $attributes, $controller)
    {
        $feedbackRecords = $attributes['feedbackRecords'];

        // 为 WorkOrderTaskWidgetRule 类建立预言, 验证请求参数,  getWorkOrderTaskWidgetRule 方法被调用1次
        $workOrderTaskWidgetRule = $this->prophesize(WorkOrderTaskWidgetRule::class);
        $workOrderTaskWidgetRule->feedbackRecords(Argument::exact($feedbackRecords))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $controller->expects($this->exactly(1))
            ->method('getWorkOrderTaskWidgetRule')
            ->willReturn($workOrderTaskWidgetRule->reveal());
    }
}

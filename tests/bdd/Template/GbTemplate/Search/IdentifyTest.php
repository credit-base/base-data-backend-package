<?php
namespace BaseData\Template\GbTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Translator\GbTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 通过标识搜索
 */
class IdentifyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchGbTemplateList()
    {
        $repository = new GbTemplateRepository();
 
        $filter['identify'] = 'QSXX';

        list($gbTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $gbTemplateList;
    }

    /**
     * @Then: 我可以看到标识为'QSXX'的所有资源目录
     */
    public function testViewGbTemplateList()
    {
        $identifyGbTemplateList = $this->gbTemplate();

        foreach ($identifyGbTemplateList as $key => $identifyGbTemplate) {
            $identifyGbTemplateList[$key]['gb_template_id'] = $key +1;
            $identifyGbTemplateList[$key]['subject_category'] = json_decode(
                $identifyGbTemplate['subject_category'],
                true
            );
            $identifyGbTemplateList[$key]['items'] = json_decode($identifyGbTemplate['items'], true);
        }

        $gbTemplateList = $this->fetchGbTemplateList();
        $translator = new GbTemplateDbTranslator();
        foreach ($gbTemplateList as $gbTemplate) {
            $gbTemplateArray[] = $translator->objectToArray($gbTemplate);
        }

        $this->assertEquals($gbTemplateArray, $identifyGbTemplateList);

        foreach ($gbTemplateArray as $gbTemplate) {
            $this->assertEquals('QSXX', $gbTemplate['identify']);
        }
    }
}

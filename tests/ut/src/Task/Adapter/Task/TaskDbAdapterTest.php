<?php
namespace BaseData\Task\Adapter\Task;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Task\Model\Task;
use BaseData\Task\Model\MockTask;
use BaseData\Task\Model\NullTask;
use BaseData\Task\Translator\TaskDbTranslator;
use BaseData\Task\Adapter\Task\Query\TaskRowQuery;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class TaskDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(TaskDbAdapter::class)
                           ->setMethods(
                               [
                                    'addAction',
                                    'editAction',
                                    'fetchOneAction',
                                    'fetchListAction'
                               ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 ITaskAdapter
     */
    public function testImplementsITaskAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\Task\ITaskAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 TaskDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Task\Translator\TaskDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 TaskRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\Task\Query\TaskRowQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockTaskDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedTask = new MockTask();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedTask)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedTask);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedTask = new MockTask();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedTask, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedTask, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedTask = new MockTask();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedTask);
        //验证
        $task = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedTask, $task);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $taskOne = new MockTask(1);
        $taskTwo = new MockTask(2);

        $ids = [1, 2];

        $expectedTaskList = [];
        $expectedTaskList[$taskOne->getId()] = $taskOne;
        $expectedTaskList[$taskTwo->getId()] = $taskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedTaskList);

        //验证
        $taskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedTaskList, $taskList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //notifySubTasks -- 开始
    public function testNotifySubTasks()
    {
        //初始化
        $id = 1;
        $task = new MockTask($id);
        $parent = ['parent_id'=>2];
        $status = ['status'=>Task::STATUS['SLEEP']];

        $condition['parent_id'] = $id;
        $info['status'] = Task::STATUS['PENDING'];

        //预言
        $adapter = $this->getMockBuilder(TaskDbAdapter::class)
            ->setMethods([
                'getRowQuery',
                'getDbTranslator'
            ])
            ->getMock();

        $rowQuery = $this->prophesize(TaskRowQuery::class);
        $rowQuery->update(
            Argument::exact($info),
            Argument::exact($condition)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $translator = $this->prophesize(TaskDbTranslator::class);
        $translator->objectToArray(
            Argument::exact($task),
            Argument::exact(['status'])
        )->shouldBeCalledTimes(1)->willReturn($status);
        $translator->objectToArray(
            Argument::exact($task),
            Argument::exact(['parent'])
        )->shouldBeCalledTimes(1)->willReturn($parent);

        //绑定
        $adapter->expects($this->once())
                ->method('getRowQuery')
                ->willReturn($rowQuery->reveal());
        $adapter->expects($this->exactly(2))
                ->method('getDbTranslator')
                ->willReturn($translator->reveal());

        //揭示
        $result = $adapter->notifySubTasks($task);
        $this->assertTrue($result);
    }
    //notifySubTasks -- 结束

    //schedule -- 开始
    public function testSchedule()
    {
        //初始化
        $primaryKey = 'primaryKey';
        $parentTasks = [['primaryKey'=>1]];
        $subTasks = [['primaryKey'=>2]];
        $ids = [1,2];
        $result = ['result'];

        //预言
        $adapter = $this->getMockBuilder(TaskDbAdapter::class)
            ->setMethods([
                'schedulParentTasks',
                'scheduleSubTaks',
                'getRowQuery',
                'fetchList'
            ])
            ->getMock();

        $rowQuery = $this->prophesize(TaskRowQuery::class);
        $rowQuery->getPrimaryKey()->shouldBeCalledTimes(1)->willReturn($primaryKey);

        //绑定
        $adapter->expects($this->once())
                   ->method('getRowQuery')
                   ->willReturn($rowQuery->reveal());

        $adapter->expects($this->once())
                   ->method('schedulParentTasks')
                   ->willReturn($parentTasks);

        $adapter->expects($this->once())
                   ->method('scheduleSubTaks')
                   ->willReturn($subTasks);

        $adapter->expects($this->once())
                   ->method('fetchList')
                   ->with($ids)
                   ->willReturn($result);

        //揭示
        $expect = $adapter->schedule();
        $this->assertEquals($expect, $result);
    }
    //schedule -- 结束

    //schedulParentTasks -- 开始
    public function testSchedulParentTasks()
    {
        //初始化
        $sql = 'next_schedule_time <= '.Core::$container->get('time').
            ' AND status = '.Task::STATUS['PENDING'].' AND parent_id = 0';

        $filter['nextScheduleTime'] = Core::$container->get('time');
        $filter['status'] = Task::STATUS['PENDING'];
        $filter['isParentTask'] = TaskDbAdapter::IS_PARENT_TASK['YES'];

        $offset = 0;
        $list = ['list'];

        //预言
        $adapter = $this->getMockBuilder(MockTaskDbAdapter::class)
            ->setMethods(['getRowQuery', 'formatFilter'])
            ->getMock();

        $adapter->expects($this->once())->method('formatFilter')->with($filter)->willReturn($sql);

        $rowQuery = $this->prophesize(TaskRowQuery::class);
        $rowQuery->find(
            Argument::exact($sql),
            Argument::exact($offset),
            Argument::exact(TaskDbAdapter::PATENT_TASK_SIZE)
        )->shouldBeCalledTimes(1)->willReturn($list);

        //绑定
        $adapter->expects($this->once())
                   ->method('getRowQuery')
                   ->willReturn($rowQuery->reveal());

        //揭示
        $result = $adapter->schedulParentTasks();
        $this->assertEquals($list, $result);
    }
    //schedulParentTasks -- 结束

    //scheduleSubTaks -- 开始
    public function testScheduleSubTaks()
    {
        //初始化
        $condition = 'next_schedule_time <= '.Core::$container->get('time').
            ' AND status = '.Task::STATUS['PENDING'].
            ' AND parent_id != 0 GROUP BY parent_id ORDER BY nice ASC';
            
        $filter['nextScheduleTime'] = Core::$container->get('time');
        $filter['status'] = Task::STATUS['PENDING'];
        $filter['isParentTask'] = TaskDbAdapter::IS_PARENT_TASK['NO'];

        $filterCondition = 'next_schedule_time <= '.Core::$container->get('time').
        ' AND status = '.Task::STATUS['PENDING'].
        ' AND parent_id != 0';
        $groupCondition = ' GROUP BY parent_id';
        $sortCondition = ' ORDER BY nice ASC';
        $group['parent'] = 1;
        $sort['nice'] = 1;
        $offset = 0;
        $list = ['list'];

        //预言
        $adapter = $this->getMockBuilder(MockTaskDbAdapter::class)
            ->setMethods(['getRowQuery', 'formatFilter', 'formatGroup', 'formatSort'])
            ->getMock();

        $adapter->expects($this->once())->method('formatFilter')->with($filter)->willReturn($filterCondition);
        $adapter->expects($this->once())->method('formatGroup')->with($group)->willReturn($groupCondition);
        $adapter->expects($this->once())->method('formatSort')->with($sort)->willReturn($sortCondition);

        $rowQuery = $this->prophesize(TaskRowQuery::class);
        $rowQuery->find(
            Argument::exact($condition),
            Argument::exact($offset),
            Argument::exact(TaskDbAdapter::SUB_TASK_SIZE)
        )->shouldBeCalledTimes(1)->willReturn($list);

        //绑定
        $adapter->expects($this->once())
                   ->method('getRowQuery')
                   ->willReturn($rowQuery->reveal());

        //揭示
        $result = $adapter->scheduleSubTaks();
        $this->assertEquals($list, $result);
    }
    //scheduleSubTaks -- 结束

    //formatFilter -- pid
    public function testFormatFilterPidSuccess()
    {
        $adapter = new MockTaskDbAdapter();
        $filter = array(
            'pid' => [1,2,3]
        );

        $expectedCondition = 'pid IN ('.implode(',', $filter['pid']).')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatFilterPidEmpty()
    {
        $adapter = new MockTaskDbAdapter();
        $filter = array(
            'pid' => []
        );

        $expectedCondition = 'pid IN ('.TaskDbAdapter::NOT_EXIST_ID.')';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testExpire()
    {
        $list = ['list'];
        $filter['status'] = Task::STATUS['EXECUTING'];
        $filter['updateTime'] = Core::$container->get('time') - TaskDbAdapter::EXPIRE_TIME;

        //预言
        $adapter = $this->getMockBuilder(MockTaskDbAdapter::class)
            ->setMethods(['filter'])
            ->getMock();

        //绑定
        $adapter->expects($this->once())
                   ->method('filter')
                   ->with($filter)
                   ->willReturn($list);

        //揭示
        $result = $adapter->expire();
        $this->assertEquals($list, $result);
    }

    //formatFilter -- status
    public function testFormatFilterStatusSuccess()
    {
        $adapter = new MockTaskDbAdapter();
        $filter = array(
            'status' => TASK::STATUS['PENDING']
        );

        $expectedCondition = 'status = '.TASK::STATUS['PENDING'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter -- updateTime
    public function testFormatFilterUpdateTimeSuccess()
    {
        $adapter = new MockTaskDbAdapter();
        $filter = array(
            'updateTime' => Core::$container->get('time') - TaskDbAdapter::EXPIRE_TIME
        );

        $expectedCondition = 'update_time < '.$filter['updateTime'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter -- nextScheduleTime
    public function testFormatFilterNextScheduleTimeSuccess()
    {
        $adapter = new MockTaskDbAdapter();
        $filter = array(
            'nextScheduleTime' => Core::$container->get('time')
        );

        $expectedCondition = 'next_schedule_time <= '.$filter['nextScheduleTime'];

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter -- isParentTask
    public function testFormatFilterIsParentYesTaskSuccess()
    {
        $adapter = new MockTaskDbAdapter();
        $filter = array(
            'isParentTask' => TaskDbAdapter::IS_PARENT_TASK['YES']
        );

        $expectedCondition = 'parent_id = 0';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    //formatFilter -- isParentTask
    public function testFormatFilterIsParentNoTaskSuccess()
    {
        $adapter = new MockTaskDbAdapter();
        $filter = array(
            'isParentTask' => TaskDbAdapter::IS_PARENT_TASK['NO']
        );

        $expectedCondition = 'parent_id != 0';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--nice
    public function testFormatSortNiceDesc()
    {
        $sort = array('nice' => -1);
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = ' ORDER BY nice DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-nice
    public function testFormatSortNiceAsc()
    {
        $sort = array('nice' => 1);
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = ' ORDER BY nice ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatGroup-parent
    public function testFormatGroupParent()
    {
        $group = array('parent' => 1);
        $adapter = new MockTaskDbAdapter();

        $expectedCondition = ' GROUP BY parent_id';

        //验证
        $condition = $adapter->formatGroup($group);
        $this->assertEquals($expectedCondition, $condition);
    }
}

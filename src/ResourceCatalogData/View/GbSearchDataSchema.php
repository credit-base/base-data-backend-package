<?php
namespace BaseData\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class GbSearchDataSchema extends SchemaProvider
{
    protected $resourceType = 'gbSearchData';

    public function getId($gbSearchData) : int
    {
        return $gbSearchData->getId();
    }

    public function getAttributes($gbSearchData) : array
    {
        return [
            'infoClassify' => $gbSearchData->getInfoClassify(),
            'infoCategory' => $gbSearchData->getInfoCategory(),
            'subjectCategory' => $gbSearchData->getSubjectCategory(),
            'dimension' => $gbSearchData->getDimension(),
            'name' => $gbSearchData->getName(),
            'identify' => $gbSearchData->getIdentify(),
            'expirationDate' => $gbSearchData->getExpirationDate(),
            'description' => $gbSearchData->getDescription(),
            'status' => $gbSearchData->getStatus(),
            'frontEndProcessorStatus' => $gbSearchData->getFrontEndProcessorStatus(),
            'createTime' => $gbSearchData->getCreateTime(),
            'updateTime' => $gbSearchData->getUpdateTime(),
            'statusTime' => $gbSearchData->getStatusTime(),
        ];
    }

    public function getRelationships($gbSearchData, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'crew' => [self::DATA => $gbSearchData->getCrew()],
            'sourceUnit' => [self::DATA => $gbSearchData->getSourceUnit()],
            'itemsData' => [self::DATA => $gbSearchData->getItemsData()],
            'template' => [self::DATA => $gbSearchData->getTemplate()]
        ];
    }
}

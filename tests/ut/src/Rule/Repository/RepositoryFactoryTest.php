<?php
namespace BaseData\Rule\Repository;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use BaseData\Common\Repository\NullRepository;

class RepositoryFactoryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new RepositoryFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $stub = $this->stub->getRepository(0);
            $this->assertInstanceOf(
                'BaseData\Common\Repository\NullRepository',
                $stub
            );
    }

    public function testGetController()
    {
        foreach (RepositoryFactory::MAPS as $key => $repository) {
            $this->assertInstanceOf(
                $repository,
                $this->stub->getRepository($key)
            );
        }
    }
}

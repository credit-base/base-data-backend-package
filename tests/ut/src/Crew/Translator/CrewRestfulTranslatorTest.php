<?php
namespace BaseData\Crew\Translator;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Utils\CrewRestfulUtils;

use BaseData\UserGroup\Model\UserGroup;
use BaseData\UserGroup\Translator\UserGroupRestfulTranslator;

class CrewRestfulTranslatorTest extends TestCase
{
    use CrewRestfulUtils;

    private $translator;

    private $childTranslator;

    public function setUp()
    {
        $this->translator = new CrewRestfulTranslator();
        $this->childTranslator = new class extends CrewRestfulTranslator
        {
            public function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
            {
                return parent::getUserGroupRestfulTranslator();
            }
        };
    }

    public function tearDown()
    {
        unset($this->translator);
        unset($this->childTranslator);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->translator
        );
    }

    public function testGetUserGroupRestfulTranslator()
    {
        $this->assertInstanceOf(
            'BaseData\UserGroup\Translator\UserGroupRestfulTranslator',
            $this->childTranslator->getUserGroupRestfulTranslator()
        );
    }

    public function testArrayToObject()
    {
        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew(1);

        $expression['data']['id'] = $crew->getId();
        $expression['data']['attributes']['cellphone'] = $crew->getCellphone();
        $expression['data']['attributes']['realName'] = $crew->getRealName();
        $expression['data']['attributes']['createTime'] = $crew->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $crew->getUpdateTime();
        $expression['data']['attributes']['status'] = $crew->getStatus();

        $crewObject = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Crew\Model\Crew', $crewObject);
        $this->compareArrayAndObject($expression, $crewObject);
    }

    public function testArrayToObjectFail()
    {
        $expression = array();

        $department = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Crew\Model\NullCrew', $department);
    }

    public function testObjectToArray()
    {
        $crew = \BaseData\Crew\Utils\MockFactory::generateCrew(1);

        $expression = $this->translator->objectToArray($crew);

        $this->compareArrayAndObject($expression, $crew);
    }

    public function testObjectToArrayFail()
    {
        $crew = null;

        $expression = $this->translator->objectToArray($crew);
        $this->assertEquals(array(), $expression);
    }

    public function testArrayToObjectWithIncluded()
    {
        //初始化
        $expression = [
            'data'=>[
                'relationships'=>'mock',
                'id' => 1
            ],
            'included'=>'mock'
        ];
        $relationships = [
            'userGroup'=>['data'=>'mockUserGroup']
        ];

        $userGroupInfo = ['mockUserGroupInfo'];

        $userGroup = new UserGroup();

        $translator = $this->getMockBuilder(CrewRestfulTranslator::class)
                           ->setMethods([
                                'relationship',
                                'changeArrayFormat',
                                'getUserGroupRestfulTranslator',
                            ])
                           ->getMock();

        //预言
        $translator->expects($this->once())
            ->method('relationship')
            ->with(
                $expression['included'],
                $expression['data']['relationships']
            )
            ->willReturn($relationships);

        //调用1次, 依次入参userGroup
        //依次返回$userGroupInfo
        $translator->expects($this->exactly(1))
            ->method('changeArrayFormat')
            ->with($relationships['userGroup']['data'])
            ->willReturn($userGroupInfo);

        $userGroupRestfulTranslator = $this->prophesize(UserGroupRestfulTranslator::class);
        $userGroupRestfulTranslator->arrayToObject(Argument::exact($userGroupInfo))
                                    ->shouldBeCalledTimes(1)
                                    ->willReturn($userGroup);
        //绑定
        $translator->expects($this->exactly(1))
            ->method('getUserGroupRestfulTranslator')
            ->willReturn($userGroupRestfulTranslator->reveal());

        //揭示
        $crew = $translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\Crew\Model\Crew', $crew);
        $this->assertEquals($userGroup, $crew->getUserGroup());
    }
}

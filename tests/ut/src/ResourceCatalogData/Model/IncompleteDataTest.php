<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class IncompleteDataTest extends TestCase
{
    public function testCorrectExtendsErrorData()
    {
        $model = new IncompleteData();
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\ErrorData', $model);
    }
}

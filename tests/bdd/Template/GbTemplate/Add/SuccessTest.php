<?php
namespace BaseData\Template\GbTemplate\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Translator\GbTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要使用国标目录但又没有对应国标目录时,在目录管理子系统,新增对应的国标目录
 *           根据我需要归集的内容和规范新增国标目录
 *           以便于我有国标目录可以使用
 * @Scenario: 新增资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $gbTemplateArray = $this->gbTemplate()[0];

        $gbTemplate = new GbTemplate();

        $gbTemplate->setName($gbTemplateArray['name']);
        $gbTemplate->setIdentify($gbTemplateArray['identify']);
        $gbTemplate->setSubjectCategory(json_decode($gbTemplateArray['subject_category'], true));
        $gbTemplate->setDimension($gbTemplateArray['dimension']);
        $gbTemplate->setExchangeFrequency($gbTemplateArray['exchange_frequency']);
        $gbTemplate->setInfoClassify($gbTemplateArray['info_classify']);
        $gbTemplate->setInfoCategory($gbTemplateArray['info_category']);
        $gbTemplate->setDescription($gbTemplateArray['description']);
        $gbTemplate->setItems(json_decode($gbTemplateArray['items'], true));

        return $gbTemplate->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setGbTemplate = $this->gbTemplate()[0];
        $setGbTemplate['gb_template_id'] = 1;
        $setGbTemplate['subject_category'] = json_decode($setGbTemplate['subject_category'], true);
        $setGbTemplate['items'] = json_decode($setGbTemplate['items'], true);
        
        $repository = new GbTemplateRepository();

        $gbTemplate = $repository->fetchOne($result);

        $translator = new GbTemplateDbTranslator();
        $gbTemplateArray = $translator->objectToArray($gbTemplate);

        $this->assertEquals($gbTemplateArray, $setGbTemplate);
    }
}

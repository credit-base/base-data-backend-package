<?php
namespace BaseData\ResourceCatalogData\Service;

use \PhpOffice\PhpSpreadsheet\Reader\BaseReader;

class MockReader extends BaseReader
{
    public function __construct()
    {
    }
    
    public function canRead($pFilename)
    {
        unset($pFilename);
    }

    public function load($pFilename)
    {
        unset($pFilename);
    }
}

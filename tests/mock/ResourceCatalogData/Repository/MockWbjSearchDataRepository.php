<?php
namespace BaseData\ResourceCatalogData\Repository;

use BaseData\ResourceCatalogData\Adapter\WbjSearchData\IWbjSearchDataAdapter;

class MockWbjSearchDataRepository extends WbjSearchDataRepository
{
    public function getAdapter() : IWbjSearchDataAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWbjSearchDataAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWbjSearchDataAdapter
    {
        return parent::getMockAdapter();
    }
}

<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试本级资源目录接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * 存在多条本级资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
                'pcore_bj_search_data' => $this->bjSearchData()['pcore_bj_search_data']
            ]
        );
    }

    public function testViewBjSearchData()
    {
        $setBjSearchDataList = $this->bjSearchData()['pcore_bj_search_data'];
        $setTemplateList = $this->bjTemplate();
        $setBjItemsDataList = $this->bjSearchData()['pcore_bj_items_data'];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);

        $response = $client->request(
            'GET',
            'bjSearchData?filter[sourceUnit]=1&filter[template]=1&filter[dimension]='
            .ISearchDataAble::DIMENSION['SHGK'].
            '&filter[infoClassify]='.ISearchDataAble::INFO_CLASSIFY['HONGMD'].
            '&filter[infoCategory]='.ISearchDataAble::INFO_CATEGORY['SHOUXXX'].
            '&filter[subjectCategory]='.ISearchDataAble::SUBJECT_CATEGORY['FRJFFRZZ'].
            '&filter[status]='.BjSearchData::STATUS['CONFIRM'].'&filter[expirationDate]=2147483656
            &filter[name]=陕西大药房&filter[identify]=91120104MA06FBJY39
            &include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $bjSearchData) {
            $this->bjSearchDataCompareData($setBjSearchDataList[$key], $bjSearchData);
        }

        $this->bjItemsDataCompareData($setBjItemsDataList[0], $included[0]);
        $this->bjTemplateCompareData($setTemplateList[0], $included[1]);
    }
}

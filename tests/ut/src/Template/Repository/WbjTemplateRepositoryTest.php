<?php
namespace BaseData\Template\Repository;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Adapter\WbjTemplate\WbjTemplateDbAdapter;

use BaseData\Template\Utils\WbjTemplateMockFactory;

class WbjTemplateRepositoryTest extends TestCase
{
    private $repository;
    
    private $mockRepository;

    public function setUp()
    {
        $this->repository = $this->getMockBuilder(WbjTemplateRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
                           
        $this->mockRepository = new MockWbjTemplateRepository();
    }

    public function testCorrectInstanceExtendsRepository()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Repository',
            $this->repository
        );
    }

    public function testSetAdapterCorrectType()
    {
        $adapter = new WbjTemplateDbAdapter();

        $this->repository->setAdapter($adapter);
        $this->assertEquals($adapter, $this->mockRepository->getAdapter());
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $this->mockRepository->getActualAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\WbjTemplateDbAdapter',
            $this->mockRepository->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $this->mockRepository->getMockAdapter()
        );
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\WbjTemplateMockAdapter',
            $this->mockRepository->getMockAdapter()
        );
    }

    public function testFetchOne()
    {
        $id = 1;

        $adapter = $this->prophesize(IWbjTemplateAdapter::class);
        $adapter->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchOne($id);
    }

    public function testFetchList()
    {
        $ids = [1, 2, 3];

        $adapter = $this->prophesize(IWbjTemplateAdapter::class);
        $adapter->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->fetchList($ids);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $adapter = $this->prophesize(IWbjTemplateAdapter::class);
        $adapter->filter(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($offset),
            Argument::exact($size)
        )->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());
                
        $this->repository->filter($filter, $sort, $offset, $size);
    }

    public function testAdd()
    {
        $id = 1;
        $wbjTemplate = WbjTemplateMockFactory::generateWbjTemplate($id);
        $keys = array();
        
        $adapter = $this->prophesize(IWbjTemplateAdapter::class);
        $adapter->add(Argument::exact($wbjTemplate))->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->add($wbjTemplate, $keys);
    }

    public function testEdit()
    {
        $id = 1;
        $wbjTemplate = WbjTemplateMockFactory::generateWbjTemplate($id);
        $keys = array();
        
        $adapter = $this->prophesize(IWbjTemplateAdapter::class);
        $adapter->edit(Argument::exact($wbjTemplate), Argument::exact($keys))
                ->shouldBeCalledTimes(1);

        $this->repository->expects($this->exactly(1))
                         ->method('getAdapter')
                         ->willReturn($adapter->reveal());

        $this->repository->edit($wbjTemplate, $keys);
    }
}

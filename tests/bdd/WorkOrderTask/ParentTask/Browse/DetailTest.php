<?php
namespace BaseData\WorkOrderTask\ParentTask\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;
use BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator;

use BaseData\Template\SetDataTrait as TemplateSetDataTrait;

/**
 * @Feature: 我是发改委领导,当我有指派后的工单任务时,在政务网OA中,我可以管理我指派的工单任务,
 *           可以对整个任务作出管理，也可以对单独的指派给委办局的任务作出管理,以便于我指派的任务可以有结果
 * @Scenario: 查看工单任务详情
 */
class DetailTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, TemplateSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_parent_task');
    }

    /**
     * @Given: 存在一条工单任务
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_parent_task' => $this->parentTask()
        ]);
    }

    /**
     * @When: 当我查看该条工单任务详情时
     */
    public function fetchParentTask($id)
    {
        $repository = new ParentTaskRepository();

        $parentTask = $repository->fetchOne($id);

        return $parentTask;
    }

    /**
     * @Then: 我可以看见该条工单任务的全部信息
     */
    public function testViewParentTaskList()
    {
        $id = 1;
        
        $setParentTask = $this->parentTask()[0];
        $setParentTask['parent_task_id'] = $id;
        $setParentTask['attachment'] = json_decode($setParentTask['attachment'], true);
        $setParentTask['assign_objects'] = json_decode($setParentTask['assign_objects'], true);

        $parentTask = $this->fetchParentTask($id);
        $translator = new ParentTaskDbTranslator();
        $parentTaskArray = $translator->objectToArray($parentTask);

        $this->assertEquals($parentTaskArray, $setParentTask);
    }
}

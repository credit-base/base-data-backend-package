<?php
namespace BaseData\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;
use BaseData\ResourceCatalogData\Model\NullErrorData;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\View\ErrorDataView;

class ErrorDataFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(ErrorDataFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockErrorDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockErrorDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\View\ErrorDataView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockErrorDataFetchController();

        $this->assertEquals(
            'errorDatas',
            $controller->getResourceName()
        );
    }
}

<?php
namespace BaseData\ResourceCatalogData\Service;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\Task;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Repository\ErrorDataRepository;

use BaseData\Template\Model\Template;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx as Writer;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class SyncErrorDataDownloadService
{
    use SyncTaskTrait;

    const EXCEL_LETTER = array(
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
        'J', 'K', 'L', 'M', 'N',
        'O', 'P', 'Q', 'R', 'S','T', 'U', 'V', 'W',
        'X', 'Y', 'Z', 'AA', 'AB',
        'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI',
        'AJ','AK', 'AL', 'AM', 'AN',
        'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU',
        'AV', 'AW', 'AX', 'AY', 'AZ',
        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG',
        'BH', 'BI', 'BJ', 'BK', 'BL',
        'BM', 'BN', 'BO', 'BP','BQ', 'BR',
        'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'
      );

    const EXCEL_HEADER = 1;
    const EXCEL_CONTENT_ROWS_NUMBER = 2;
    const EXCEL_HEADER_ERROR_REASON_CN_NAME = '失败原因';

    private $errorDataRepository;

    private $spreadsheet;

    public function __construct(Task $task)
    {
        $this->task = $task;
        $this->errorDataRepository = new ErrorDataRepository();
        $this->spreadsheet = new Spreadsheet();
    }

    public function __destruct()
    {
        unset($this->task);
        unset($this->errorDataRepository);
        unset($this->spreadsheet);
    }

    protected function getTask() : Task
    {
        return $this->task;
    }

    protected function getErrorDataRepository() : ErrorDataRepository
    {
        return $this->errorDataRepository;
    }

    protected function getWriter(Spreadsheet $spreadsheet) : Writer
    {
        return new Writer($spreadsheet);
    }

    /**
     * 将失败数据导出成excel,并在导出成功后修改任务状态
     * 为了避免用户在下载失败文件时,文件内容过大,时间过长,所以此处将失败文件提前生成,在用户点击下载时,可直接下载失败文件
     * @return bool
     */
    public function execute() : bool
    {
        $task = $this->getTask();

        $errorDataList = $this->fetchErrorDataList($task);

        if ($this->download($errorDataList, $task)) {
            return $this->updateTaskStatus($task);
        }

        return false;
    }

    protected function fetchErrorDataList(Task $task) : array
    {
        $filter['task'] = $task->getId();

        list($list, $count) = $this->getErrorDataRepository()->filter($filter);

        unset($count);

        return $list;
    }

    /**
     * 导出excel
     * @param array $errorDataList 失败数据列表,填充excel文件内容
     * @param Task $task 任务信息
     * @return bool
     */
    protected function download(array $errorDataList, Task $task) : bool
    {
        //实例化
        $spreadsheet = $this->getSpreadsheet();

        //填充文档内容
        $spreadsheet = $this->fillWorksheet($task, $errorDataList, $spreadsheet);
       
        //将文档保存
        $writer = $this->getWriter($spreadsheet);

        //文件路径
        $filePath = $this->splicingFilePath($task);

        $writer->save($filePath);

        return file_exists($filePath);
    }

    /**
     * 生成文件路径
        * 拼接文件名称(文件名称中包含来源目标资源目录类型是为了避免文件名称重复覆盖)
            * 文件名称格式: FAILURE_来源资源目录类型_目标资源目录id_目标资源目录类型_文件上传时的文件名
        * 拼接文件路径
     * @param Task $task 任务信息
     * @return string
     */
    protected function splicingFilePath(Task $task) : string
    {
        $fileName = Task::ERROR_FILE_NAME_PREFIX.'_'.$task->getSourceCategory().
        '_'.$task->getTargetTemplate().'_'.$task->getTargetCategory().'_'.$task->getFileName();
        
        return  Core::$container->get('attachment.download.path').$fileName;
    }

    /**
     * 实例化
     * @return Spreadsheet
     */
    protected function getSpreadsheet() : Spreadsheet
    {
        $pool = new \Cache\Adapter\Doctrine\DoctrineCachePool(\Marmot\Core::$cacheDriver);
        $simpleCache = new \Cache\Bridge\SimpleCache\SimpleCacheBridge($pool);
        \PhpOffice\PhpSpreadsheet\Settings::setCache($simpleCache);

        //实例化
        $spreadsheet = $this->spreadsheet;

        //设置文档属性
        $spreadsheet->getProperties()
                    ->setCreator('qixinyun')    //作者
                    ->setTitle("error data download")  //标题
                    ->setSubject("error data download") //副标题
                    ->setDescription("error data download")  //描述
                    ->setCategory("error data"); //分类

        return $spreadsheet;
    }

    /**
     * 将失败数据写入文档
     * @param array $errorDataList 失败数据列表,填充excel文件内容
     * @param Task $task 任务信息
     * @param Spreadsheet $spreadsheet
     * @return Spreadsheet
     */
    protected function fillWorksheet(
        Task $task,
        array $errorDataList,
        Spreadsheet $spreadsheet
    ) : Spreadsheet {
        $sheet = $spreadsheet->getActiveSheet();

        //设置单元格格式
        $this->fillWorksheetStyle($sheet);
        ////设置表头
        $this->fillWorksheetHeader($sheet, $task);
        //设置文档内容
        $this->fillWorksheetContent($sheet, $errorDataList);

        return $spreadsheet;
    }

    /**
     * 设置单元格格式
        * 设置A-BZ列的格式为文本格式
     * @param Worksheet $sheet
     * @return Worksheet
     */
    protected function fillWorksheetStyle(Worksheet $sheet)
    {
        $sheet->getStyle('A:BZ')->getNumberFormat()->setFormatCode(
            \PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_TEXT
        );
    }

    /**
     * 设置表头
        * 获取目标资源目录中的模板信息
        * 通过获取到的模板信息设置表头
        * 将"失败原因"写入表头,为了让用户能更快的了解到数据上传失败的原因,并做相关的修改
     * @param Worksheet $sheet 活动工作薄
     * @param Task $task 任务信息
     * @return Worksheet
     */
    protected function fillWorksheetHeader(Worksheet $sheet, Task $task)
    {
        //通过任务中的目标资源目录获取到目标资源目录
        $targetTemplate = $this->fetchTargetTemplate($task);
        $items = $targetTemplate->getItems();

        //通过获取到的模板信息将中文名称设置为表头
        foreach ($items as $key => $item) {
            $sheet->setCellValue(self::EXCEL_LETTER[$key].self::EXCEL_HEADER, $item['name']);
        }

        $count = count($items);
        //将"失败原因"写入表头
        $sheet->setCellValue(self::EXCEL_LETTER[$count].self::EXCEL_HEADER, self::EXCEL_HEADER_ERROR_REASON_CN_NAME);
    }

    /**
     * 设置文档内容
        * 获取目标资源目录数据
        * 将失败数据写入,从第二行开始
     * @param Worksheet $sheet 活动工作薄
     * @param array $errorDataList 失败数据
     * @return Worksheet
     */
    protected function fillWorksheetContent(Worksheet $sheet, array $errorDataList)
    {
        //设置文档内容
        $excelKey = self::EXCEL_CONTENT_ROWS_NUMBER;

        foreach ($errorDataList as $errorData) {
            $itemsData = $errorData->getItemsData()->getData();

            $key = 0;

            //写入失败数据
            foreach ($itemsData as $itemData) {
                //明确设置单元格的数据类型,此处是为了解决超长的数字显示科学记数法
                $sheet->setCellValueExplicit(self::EXCEL_LETTER[$key].$excelKey, $itemData, DataType::TYPE_STRING);
                $key++;
            }

            $count = count($itemsData);
            $errorReason = $this->splicingErrorReason($errorData);
            //写入失败原因
            $sheet->setCellValue(self::EXCEL_LETTER[$count].$excelKey, $errorReason);

            $excelKey++;
        }
    }

    /**
     * 拼接错误原因
        * 失败原因格式样例: array('ZTMC' => array(2), '模板标识'=>array(失败类型))
        * 将失败原因拼成一句话: 主体名称比对失败
     * @param ErrorData $errorData 失败资源目录数据
     * @return Worksheet
     */
    protected function splicingErrorReason(ErrorData $errorData)
    {
        $errorReasonOld = $errorData->getErrorReason();
        $items = $this->fetchTemplateItems($errorData->getTemplate());
        $errorReason = array();

        foreach ($errorReasonOld as $identify => $errorTypes) {
            $splicingReason = isset($items[$identify]['name']) ? $items[$identify]['name'] : '';
            $count = count($errorTypes)-1;
            foreach ($errorTypes as $key => $errorType) {
                $reason = isset(ErrorData::ERROR_REASON[$errorType]) ? ErrorData::ERROR_REASON[$errorType] : '';
                $symbol = ($key>=$count) ? '' : '、';
                $splicingReason = $splicingReason.$reason.$symbol;
            }

            $errorReason[] = $splicingReason;
        }

        return implode(',', $errorReason);
    }

    //获取目标资源目录
    protected function fetchTargetTemplate(Task $task) : Template
    {
        $repository = $this->getRepositoryFactory()->getRepository($task->getTargetCategory());

        return $repository->fetchOne($task->getTargetTemplate());
    }

    //修改任务状态
    protected function updateTaskStatus(Task $task) : bool
    {
        $task->setStatus(Task::STATUS['FAILURE_FILE_DOWNLOAD']);
        $task->setStatusTime(Core::$container->get('time'));

        return $task->edit();
    }
}

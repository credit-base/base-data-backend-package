<?php
namespace BaseData\Crew\Model;

use PHPUnit\Framework\TestCase;

class NullCrewTest extends TestCase
{
    private $crew;

    public function setUp()
    {
        $this->crew = NullCrew::getInstance();
    }

    public function tearDown()
    {
        unset($this->crew);
    }

    public function testExtendsNews()
    {
        $this->assertInstanceof('BaseData\Crew\Model\Crew', $this->crew);
    }

    public function testImplementsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->crew);
    }
}

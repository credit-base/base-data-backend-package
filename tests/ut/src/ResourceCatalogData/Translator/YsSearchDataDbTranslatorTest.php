<?php
namespace BaseData\ResourceCatalogData\Translator;

use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Utils\YsSearchDataUtils;
use BaseData\ResourceCatalogData\Utils\YsSearchDataMockFactory;

class YsSearchDataDbTranslatorTest extends TestCase
{
    use YsSearchDataUtils;

    private $translator;

    public function setUp()
    {
        $this->translator = new YsSearchDataDbTranslator();
    }

    public function tearDown()
    {
        unset($this->translator);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->translator
        );
    }

    public function testArrayToObjectIncorrectArray()
    {
        $result = $this->translator->arrayToObject(array());
        $this->assertInstanceOf('BaseData\ResourceCatalogData\Model\NullYsSearchData', $result);
    }

    public function testObjectToArrayIncorrectObject()
    {
        $result = $this->translator->objectToArray(null);
        $this->assertEquals([], $result);
    }

    public function testArrayToObject()
    {
        $ysSearchData = YsSearchDataMockFactory::generateYsSearchData(1);

        $expression['ys_search_data_id'] = $ysSearchData->getId();
        $expression['info_classify'] = $ysSearchData->getInfoClassify();
        $expression['info_category'] = $ysSearchData->getInfoCategory();
        $expression['crew_id'] = $ysSearchData->getCrew()->getId();
        $expression['usergroup_id'] = $ysSearchData->getSourceUnit()->getId();
        $expression['subject_category'] = $ysSearchData->getSubjectCategory();
        $expression['dimension'] = $ysSearchData->getDimension();
        $expression['name'] = $ysSearchData->getName();
        $expression['identify'] = $ysSearchData->getIdentify();
        $expression['expiration_date'] = $ysSearchData->getExpirationDate();
        $expression['wbj_template_id'] = $ysSearchData->getTemplate()->getId();
        $expression['ys_items_data_id'] = $ysSearchData->getItemsData()->getId();
        $expression['status'] = $ysSearchData->getStatus();
        $expression['hash'] = $ysSearchData->getHash();
        $expression['status_time'] = $ysSearchData->getStatusTime();
        $expression['create_time'] = $ysSearchData->getCreateTime();
        $expression['update_time'] = $ysSearchData->getUpdateTime();
        $expression['task_id'] = $ysSearchData->getTask()->getId();

        $ysSearchData = $this->translator->arrayToObject($expression);
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\YsSearchData', $ysSearchData);
        $this->compareArrayAndObject($expression, $ysSearchData);
    }

    public function testObjectToArray()
    {
        $ysSearchData = YsSearchDataMockFactory::generateYsSearchData(1);

        $expression = $this->translator->objectToArray($ysSearchData);

        $this->compareArrayAndObject($expression, $ysSearchData);
    }
}

<?php
namespace BaseData\Enterprise\Enterprise\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Enterprise\Repository\EnterpriseRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要查看企业数据时,在信用信息查询应用系统下的企业管理中
 *           可以查看到所有的企业数据,通过列表和详情的形式查看我所有的企业, 以便于我可以精确的查看到不同的企业信息
 * @Scenario: 查看企业数据列表-异常不存在数据
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @Given: 不存在企业数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看企业数据列表时
     */
    public function fetchEnterpriseList()
    {
        $repository = new EnterpriseRepository();

        list($enterpriseList, $count) = $repository->filter([]);
        
        unset($count);

        return $enterpriseList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewEnterprise()
    {
        $enterpriseList = $this->fetchEnterpriseList();

        $this->assertEmpty($enterpriseList);
    }
}

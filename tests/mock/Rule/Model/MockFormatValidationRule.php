<?php
namespace BaseData\Rule\Model;

use BaseData\ResourceCatalogData\Strategy\DataStrategyFactory;

class MockFormatValidationRule extends FormatValidationRule
{
    use MockConcretenessRuleTrait;
    
    public function getDataStrategyFactory() : DataStrategyFactory
    {
        return parent::getDataStrategyFactory();
    }

    public function combinationItemsData(array $items, array $sourceItemsData) : array
    {
        return parent::combinationItemsData($items, $sourceItemsData);
    }

    public function validateItemsDataFormat(array $itemsData) : bool
    {
        return parent::validateItemsDataFormat($itemsData);
    }

    public function isNecessaryItemEmpty(array $itemData) : bool
    {
        return parent::isNecessaryItemEmpty($itemData);
    }

    public function validateItemDataFormat(array $itemData) : bool
    {
        return parent::validateItemDataFormat($itemData);
    }
}

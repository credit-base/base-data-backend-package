<?php
namespace BaseData\ResourceCatalogData\Translator\Strategy;

class NullSearchDataDbTranslatorStrategy implements IDbTranslatorStrategy
{
    public function objectToArray($baseSearchData, array $keys = array())
    {
        unset($baseSearchData);
        unset($keys);
        return false;
    }
}

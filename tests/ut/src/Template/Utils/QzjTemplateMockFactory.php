<?php
namespace BaseData\Template\Utils;

use BaseData\Template\Model\QzjTemplate;

class QzjTemplateMockFactory
{
    use TemplateMockFactoryTrait;
    
    public static function generateQzjTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : QzjTemplate {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $qzjTemplate = new QzjTemplate($id);
        $qzjTemplate->setId($id);

        self::generateName($qzjTemplate, $faker, $value);
        self::generateIdentify($qzjTemplate, $faker, $value);
        self::generateCategory($qzjTemplate, $faker, $value);
        self::generateSubjectCategory($qzjTemplate, $faker, $value);
        self::generateDimension($qzjTemplate, $faker, $value);
        self::generateExchangeFrequency($qzjTemplate, $faker, $value);
        self::generateInfoClassify($qzjTemplate, $faker, $value);
        self::generateInfoCategory($qzjTemplate, $faker, $value);
        self::generateDescription($qzjTemplate, $faker, $value);
        self::generateItems($qzjTemplate, $faker, $value);
        self::generateStatus($qzjTemplate, $faker, $value);
        self::generateSourceUnit($qzjTemplate, $faker, $value);
        
        $qzjTemplate->setCreateTime($faker->unixTime());
        $qzjTemplate->setUpdateTime($faker->unixTime());
        $qzjTemplate->setStatusTime($faker->unixTime());

        return $qzjTemplate;
    }

    private static function generateSourceUnit($qzjTemplate, $faker, $value) : void
    {
        $sourceUnit = isset($value['sourceUnit']) ?
                    $value['sourceUnit'] :
                    \BaseData\UserGroup\Utils\MockFactory::generateUserGroup($faker->randomDigit());
                    
        $qzjTemplate->setSourceUnit($sourceUnit);
    }

    protected static function generateCategory($qzjTemplate, $faker, $value) : void
    {
        $category = isset($value['category']) ?
            $value['category'] :
            $faker->randomElement(
                QzjTemplate::QZJ_TEMPLATE_CATEGORY
            );
        
        $qzjTemplate->setCategory($category);
    }
}

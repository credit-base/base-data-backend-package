<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试国标资源目录接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_items_data');
        $this->clear('pcore_gb_search_data');
    }

    /**
     * 存在多条国标资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data'],
                'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data']
            ]
        );
    }

    public function testViewGbSearchData()
    {
        $setGbSearchDataList = $this->gbSearchData()['pcore_gb_search_data'];
        $setTemplateList = $this->gbTemplate();
        $setGbItemsDataList = $this->gbSearchData()['pcore_gb_items_data'];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);

        $response = $client->request(
            'GET',
            'gbSearchData?filter[sourceUnit]=1&filter[template]=1&filter[dimension]='
            .ISearchDataAble::DIMENSION['SQCX'].
            '&filter[infoClassify]='.ISearchDataAble::INFO_CLASSIFY['QT'].
            '&filter[infoCategory]='.ISearchDataAble::INFO_CATEGORY['JCXX'].
            '&filter[sugbectCategory]='.ISearchDataAble::SUBJECT_CATEGORY['GTGSH'].
            '&filter[status]='.GbSearchData::STATUS['CONFIRM'].'&filter[expirationDate]=2147483645
            &filter[name]=陕西出租公司&filter[identify]=91220104MA06FBJY39
            &include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $gbSearchData) {
            $this->gbSearchDataCompareData($setGbSearchDataList[$key], $gbSearchData);
        }

        $this->gbItemsDataCompareData($setGbItemsDataList[0], $included[0]);
        $this->gbTemplateCompareData($setTemplateList[0], $included[1]);
    }
}

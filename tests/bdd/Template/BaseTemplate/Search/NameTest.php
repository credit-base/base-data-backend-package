<?php
namespace BaseData\Template\BaseTemplate\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Translator\BaseTemplateDbTranslator;

/**
 * @Feature: 我是超级管理员,当我需要查看特定条件资源目录时,在目录管理子系统,可以搜索我需要的所有资源目录
 *           通过列表的形式展现我搜索的所有资源目录,以便于我可以快速查看到我需要的资源目录
 * @Scenario: 通过名称搜索
 */
class NameTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_base_template');
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchBaseTemplateList()
    {
        $repository = new BaseTemplateRepository();
 
        $filter['name'] = '基础登记信息';

        list($baseTemplateList, $count) = $repository->filter($filter);
        
        unset($count);

        return $baseTemplateList;
    }

    /**
     * @Then: 我可以看到名称中带"基础登记信息"的所有资源目录
     */
    public function testViewBaseTemplateList()
    {
        $nameBaseTemplateList = $this->baseTemplate();

        foreach ($nameBaseTemplateList as $key => $nameBaseTemplate) {
            $nameBaseTemplateList[$key]['base_template_id'] = $key +1;
            $nameBaseTemplateList[$key]['subject_category'] = json_decode(
                $nameBaseTemplate['subject_category'],
                true
            );
            $nameBaseTemplateList[$key]['items'] = json_decode($nameBaseTemplate['items'], true);
        }

        $baseTemplateList = $this->fetchBaseTemplateList();
        $translator = new BaseTemplateDbTranslator();
        foreach ($baseTemplateList as $baseTemplate) {
            $baseTemplateArray[] = $translator->objectToArray($baseTemplate);
        }

        $this->assertEquals($baseTemplateArray, $nameBaseTemplateList);

        foreach ($baseTemplateArray as $baseTemplate) {
            $this->assertEquals('基础登记信息', $baseTemplate['name']);
        }
    }
}

<?php
namespace BaseData\Crew\Translator;

use Marmot\Interfaces\IRestfulTranslator;

use BaseData\Common\Translator\RestfulTranslatorTrait;

use BaseData\Crew\Model\Crew;
use BaseData\Crew\Model\NullCrew;

use BaseData\UserGroup\Translator\UserGroupRestfulTranslator;

class CrewRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getUserGroupRestfulTranslator() : UserGroupRestfulTranslator
    {
        return new UserGroupRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $crew = null)
    {
        return $this->translateToObject($expression, $crew);
    }
    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $crew = null)
    {
        if (empty($expression)) {
            return NullCrew::getInstance();
        }

        if ($crew == null) {
            $crew = new Crew();
        }
        
        $data = $expression['data'];

        $crew->setId($data['id']);

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['cellphone'])) {
            $crew->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['realName'])) {
            $crew->setRealName($attributes['realName']);
        }
        if (isset($attributes['updateTime'])) {
            $crew->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['createTime'])) {
            $crew->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['status'])) {
            $crew->setStatus($attributes['status']);
        }

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }
        if (isset($relationships['userGroup']['data'])) {
            $userGroup = $this->changeArrayFormat($relationships['userGroup']['data']);
            
            $crew->setUserGroup($this->getUserGroupRestfulTranslator()->arrayToObject($userGroup));
        }

        return $crew;
    }

    public function objectToArray($crew, array $keys = array())
    {
        if (!$crew instanceof Crew) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'cellphone',
                'realName',
                'userGroup'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'crews'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $crew->getId();
        }

        if (in_array('cellphone', $keys)) {
            $expression['data']['attributes']['cellphone'] = $crew->getCellphone();
        }
        if (in_array('realName', $keys)) {
            $expression['data']['attributes']['realName'] = $crew->getRealName();
        }

        if (in_array('userGroup', $keys)) {
            $expression['data']['relationships']['userGroup']['data'] = array(
                array(
                    'type' => 'userGroups',
                    'id' => $crew->getUserGroup()->getId()
                )
            );
        }
        
        return $expression;
    }
}

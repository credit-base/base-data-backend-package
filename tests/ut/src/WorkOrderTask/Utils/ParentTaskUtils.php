<?php
namespace BaseData\WorkOrderTask\Utils;

trait ParentTaskUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $parentTask
    ) {
        $this->assertEquals($expectedArray['parent_task_id'], $parentTask->getId());

        $this->assertEquals($expectedArray['status'], $parentTask->getStatus());
        $this->assertEquals($expectedArray['create_time'], $parentTask->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $parentTask->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $parentTask->getStatusTime());
    }
}

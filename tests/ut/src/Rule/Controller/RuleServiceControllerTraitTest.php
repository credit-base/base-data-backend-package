<?php
namespace BaseData\Rule\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Rule\WidgetRule\RuleServiceWidgetRule;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class RuleServiceControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockRuleServiceControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetRuleServiceWidgetRule()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\WidgetRule\RuleServiceWidgetRule',
            $this->trait->getRuleServiceWidgetRulePublic()
        );
    }

    public function testGetCommonWidgetRule()
    {
        $this->assertInstanceOf(
            'BaseData\Common\WidgetRule\CommonWidgetRule',
            $this->trait->getCommonWidgetRulePublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\RuleServiceRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetUnAuditedRuleServiceRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Rule\Repository\UnAuditedRuleServiceRepository',
            $this->trait->getUnAuditedRuleServiceRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateRejectScenario()
    {
        $controller = $this->getMockBuilder(MockRuleServiceControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule']) ->getMock();

        $rejectReason = 'rejectReason';
        $applyCrew = 2;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->description(Argument::exact($rejectReason), Argument::exact('rejectReason'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);
        $commonWidgetRule->formatNumeric(Argument::exact($applyCrew), Argument::exact('applyCrewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(2))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateRejectScenarioPublic($rejectReason, $applyCrew);
        
        $this->assertTrue($result);
    }

    public function testValidateApproveScenario()
    {
        $controller = $this->getMockBuilder(MockRuleServiceControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule']) ->getMock();

        $applyCrew = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($applyCrew), Argument::exact('applyCrewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateApproveScenarioPublic($applyCrew);
        
        $this->assertTrue($result);
    }

    public function testValidateStatusScenario()
    {
        $controller = $this->getMockBuilder(MockRuleServiceControllerTrait::class)
                 ->setMethods(['getCommonWidgetRule']) ->getMock();

        $crew = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($crew), Argument::exact('crewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $result = $controller->validateStatusScenarioPublic($crew);
        
        $this->assertTrue($result);
    }

    public function testValidateCommonScenario()
    {
        $controller = $this->getMockBuilder(MockRuleServiceControllerTrait::class)
                    ->setMethods(['getCommonWidgetRule', 'getRuleServiceWidgetRule'])
                    ->getMock();

        $crew = 1;
        $rules = array('rules');

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(Argument::exact($crew), Argument::exact('crewId'))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $ruleServiceWidgetRule = $this->prophesize(RuleServiceWidgetRule::class);
        $ruleServiceWidgetRule->rules(Argument::exact($rules))
            ->shouldBeCalledTimes(1)
            ->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getRuleServiceWidgetRule')
            ->willReturn($ruleServiceWidgetRule->reveal());

        $result = $controller->validateCommonScenarioPublic($rules, $crew);
        
        $this->assertTrue($result);
    }

    public function testValidateAddScenario()
    {
        $controller = $this->getMockBuilder(MockRuleServiceControllerTrait::class)
                    ->setMethods(['getCommonWidgetRule', 'getRuleServiceWidgetRule'])
                    ->getMock();

        $transformationTemplate = 1;
        $sourceTemplate = 1;
        $transformationCategory = 1;
        $sourceCategory = 1;

        $commonWidgetRule = $this->prophesize(CommonWidgetRule::class);
        $commonWidgetRule->formatNumeric(
            Argument::exact($transformationTemplate),
            Argument::exact('transformationTemplateId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $commonWidgetRule->formatNumeric(
            Argument::exact($sourceTemplate),
            Argument::exact('sourceTemplateId')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(2))
            ->method('getCommonWidgetRule')
            ->willReturn($commonWidgetRule->reveal());

        $ruleServiceWidgetRule = $this->prophesize(RuleServiceWidgetRule::class);
        $ruleServiceWidgetRule->category(
            Argument::exact($transformationCategory),
            Argument::exact('transformationCategory')
        )->shouldBeCalledTimes(1)->willReturn(true);
        
        $ruleServiceWidgetRule->category(
            Argument::exact($sourceCategory),
            Argument::exact('sourceCategory')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(2))
            ->method('getRuleServiceWidgetRule')
            ->willReturn($ruleServiceWidgetRule->reveal());

        $result = $controller->validateAddScenarioPublic(
            $transformationTemplate,
            $sourceTemplate,
            $transformationCategory,
            $sourceCategory
        );
        
        $this->assertTrue($result);
    }
}

<?php
namespace BaseData\Template\Controller;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use Marmot\Interfaces\IView;

class MockGbTemplateFetchController extends GbTemplateFetchController
{
    public function getRepository() : IGbTemplateAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

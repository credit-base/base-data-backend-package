<?php
namespace BaseData\Template\Adapter\WbjTemplate;

use Marmot\Interfaces\INull;

use BaseData\Template\Model\WbjTemplate;
use BaseData\Template\Translator\WbjTemplateDbTranslator;
use BaseData\Template\Adapter\WbjTemplate\Query\WbjTemplateRowCacheQuery;

use BaseData\UserGroup\Repository\UserGroupRepository;

class MockWbjTemplateDbAdapter extends WbjTemplateDbAdapter
{
    public function getDbTranslator() : WbjTemplateDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : WbjTemplateRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getUserGroupRepository() : UserGroupRepository
    {
        return parent::getUserGroupRepository();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function fetchSourceUnit($wbjTemplate)
    {
        return parent::fetchSourceUnit($wbjTemplate);
    }

    public function fetchSourceUnitByObject(WbjTemplate $wbjTemplate)
    {
        return parent::fetchSourceUnitByObject($wbjTemplate);
    }

    public function fetchSourceUnitByList(array $wbjTemplateList)
    {
        return parent::fetchSourceUnitByList($wbjTemplateList);
    }
}

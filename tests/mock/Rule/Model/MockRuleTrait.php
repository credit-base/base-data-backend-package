<?php
namespace BaseData\Rule\Model;

use BaseData\Template\Model\Template;
use BaseData\Template\Repository\GbTemplateRepository;

use BaseData\Rule\Repository\RepositoryFactory;
use BaseData\Rule\Model\SearchDataModelFactory;
use BaseData\Rule\Repository\SearchDataRepositoryFactory;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 */
class MockRuleTrait
{
    use RuleTrait;

    public function getRepositoryFactoryPublic() : RepositoryFactory
    {
        return $this->getRepositoryFactory();
    }

    public function getGbTemplateRepositoryPublic() : GbTemplateRepository
    {
        return $this->getGbTemplateRepository();
    }

    public function getGbSearchDataRepositoryPublic() : GbSearchDataRepository
    {
        return $this->getGbSearchDataRepository();
    }

    public function getSearchDataRepositoryFactoryPublic() : SearchDataRepositoryFactory
    {
        return $this->getSearchDataRepositoryFactory();
    }

    public function getSearchDataModelFactoryPublic() : SearchDataModelFactory
    {
        return $this->getSearchDataModelFactory();
    }

    public function templateItemComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->templateItemComparison($transformationItem, $sourceItem);
    }

    public function comparisonRuleComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->comparisonRuleComparison($transformationItem, $sourceItem);
    }

    public function dataTypeComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataTypeComparison($transformationItem, $sourceItem);
    }

    public function dataLengthComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataLengthComparison($transformationItem, $sourceItem);
    }

    public function dataOptionComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataOptionComparison($transformationItem, $sourceItem);
    }

    public function dataDimensionComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataDimensionComparison($transformationItem, $sourceItem);
    }

    public function dataIsMaskedComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataIsMaskedComparison($transformationItem, $sourceItem);
    }

    public function dataMaskRuleComparisonPublic(array $transformationItem, array $sourceItem) : bool
    {
        return $this->dataMaskRuleComparison($transformationItem, $sourceItem);
    }

    public function fetchTemplateItemsPublic(Template $template) : array
    {
        return $this->fetchTemplateItems($template);
    }

    public function fetchTemplateListPublic(array $condition) : array
    {
        return $this->fetchTemplateList($condition);
    }

    public function validateBasePublic(Template $template, array $base) : bool
    {
        return $this->validateBase($template, $base);
    }

    public function initializationTransformationSearchDataPublic(
        ISearchDataAble $sourceSearchData,
        Template $transformationTemplate
    ) : ISearchDataAble {
        return $this->initializationTransformationSearchData($sourceSearchData, $transformationTemplate);
    }

    public function generateCommonSearchDataPublic(
        ISearchDataAble $searchData,
        ISearchDataAble $transformationSearchData,
        Template $transformationTemplate
    ) {
        return $this->generateCommonSearchData($searchData, $transformationSearchData, $transformationTemplate);
    }

    public function generateErrorDataPublic(
        ISearchDataAble $errorData,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        return $this->generateErrorData($errorData, $transformationSearchData);
    }

    public function getSearchDataPublic(Template $transformationTemplate)
    {
        return $this->getSearchData($transformationTemplate);
    }

    public function fetchTransformationSearchDataPublic(
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        return $this->fetchTransformationSearchData($transformationSearchData);
    }

    public function fetchFailureDataPublic(
        ISearchDataAble $transformationSearchData,
        int $errorType,
        array $failureItems
    ) : ISearchDataAble {
        return $this->fetchFailureData($transformationSearchData, $errorType, $failureItems);
    }

    public function fetchIncompleteDataPublic(
        ISearchDataAble $transformationSearchData,
        int $errorType,
        array $failureItems
    ) : ISearchDataAble {
        return $this->fetchIncompleteData($transformationSearchData, $errorType, $failureItems);
    }

    public function errorInfoPublic(
        ISearchDataAble $errorSearchData,
        int $errorType,
        array $failureItems
    ) : ISearchDataAble {
        return $this->errorInfo($errorSearchData, $errorType, $failureItems);
    }

    public function transformationTemplateRequiredPublic(Template $transformationTemplate) : array
    {
        return $this->transformationTemplateRequired($transformationTemplate);
    }

    public function getSearchDataRepositoryPublic(ISearchDataAble $searchData)
    {
        return $this->getSearchDataRepository($searchData);
    }

    public function fetchSearchDataListPublic(array $patterns, ISearchDataAble $transformationSearchData) : array
    {
        return $this->fetchSearchDataList($patterns, $transformationSearchData);
    }

    public function filterFormatChangePublic(array $patterns, ISearchDataAble $transformationSearchData) : array
    {
        return $this->filterFormatChange($patterns, $transformationSearchData);
    }

    public function fetchIncompleteItemsPublic(
        array $transformationItemsData,
        array $requiredItems
    ) : array {
        return $this->fetchIncompleteItems($transformationItemsData, $requiredItems);
    }

    public function fetchItemsIdentifyPublic(array $items) : array
    {
        return $this->fetchItemsIdentify($items);
    }

    public function filterExistItemsDataByIdentifyPublic(array $searchDataList, string $identify)
    {
        return $this->filterExistItemsDataByIdentify($searchDataList, $identify);
    }

    public function generateHashPublic(array $itemsData) : string
    {
        return $this->generateHash($itemsData);
    }

    public function incompleteDataPublic(
        ISearchDataAble $transformationSearchData,
        int $type,
        array $incompleteItems
    ) : ISearchDataAble {
        return $this->incompleteData($transformationSearchData, $type, $incompleteItems);
    }

    public function transformationSearchDataPublic(ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        return $this->transformationSearchData($transformationSearchData);
    }

    public function failureDataPublic(
        ISearchDataAble $transformationSearchData,
        int $errorType,
        array $failureItems
    ) : ISearchDataAble {
        return $this->failureData($transformationSearchData, $errorType, $failureItems);
    }

    public function failureDataExceptionPublic(
        ISearchDataAble $transformationSearchData,
        int $status
    ) : ISearchDataAble {
        return $this->failureDataException($transformationSearchData, $status);
    }

    public function errorReasonFormatConversionPublic(ErrorData $errorData) : ErrorData
    {
        return $this->errorReasonFormatConversion($errorData);
    }

    public function transformationDataResultPublic(
        array $incompleteItems,
        ISearchDataAble $transformationSearchData
    ) : ISearchDataAble {
        return $this->transformationDataResult($incompleteItems, $transformationSearchData);
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use BaseData\Common\Model\IEnableAble;

class MockDisableWbjSearchDataCommandHandler extends DisableWbjSearchDataCommandHandler
{
    public function fetchIEnableObject($id) : IEnableAble
    {
        return parent::fetchIEnableObject($id);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter;

class WbjItemsDataTest extends TestCase
{
    private $wbjItemsData;

    public function setUp()
    {
        $this->wbjItemsData = new WbjItemsData();
    }

    public function tearDown()
    {
        unset($this->wbjItemsData);
    }

    public function testGetRepository()
    {
        $wbjItemsData = new MockWbjItemsData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter',
            $wbjItemsData->getRepository()
        );
    }

    public function testAddSuccess()
    {
        $wbjItemsData = $this->getMockBuilder(WbjItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IWbjItemsDataAdapter::class);
        $repository->add(Argument::exact($wbjItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        $wbjItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $wbjItemsData->add();
        $this->assertTrue($result);
    }

    public function testAddFail()
    {
        $wbjItemsData = $this->getMockBuilder(WbjItemsData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $repository = $this->prophesize(IWbjItemsDataAdapter::class);
        $repository->add(Argument::exact($wbjItemsData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        $wbjItemsData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        $result = $wbjItemsData->add();
        $this->assertFalse($result);
    }
}

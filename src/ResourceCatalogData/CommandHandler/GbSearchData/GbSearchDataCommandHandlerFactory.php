<?php
namespace BaseData\ResourceCatalogData\CommandHandler\GbSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class GbSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\ResourceCatalogData\Command\GbSearchData\ConfirmGbSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\GbSearchData\ConfirmGbSearchDataCommandHandler',
        'BaseData\ResourceCatalogData\Command\GbSearchData\DisableGbSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\GbSearchData\DisableGbSearchDataCommandHandler',
        'BaseData\ResourceCatalogData\Command\GbSearchData\DeleteGbSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\GbSearchData\DeleteGbSearchDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

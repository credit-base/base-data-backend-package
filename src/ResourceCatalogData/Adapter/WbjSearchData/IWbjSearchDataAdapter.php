<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjSearchData;

use BaseData\ResourceCatalogData\Model\WbjSearchData;

interface IWbjSearchDataAdapter
{
    public function fetchOne($id) : WbjSearchData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(WbjSearchData $wbjSearchData) : bool;

    public function edit(WbjSearchData $wbjSearchData, array $keys = array()) : bool;
}

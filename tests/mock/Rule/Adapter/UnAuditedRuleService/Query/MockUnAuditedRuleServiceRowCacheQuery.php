<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query;

use Marmot\Interfaces\CacheLayer;
use Marmot\Framework\Interfaces\DbLayer;

class MockUnAuditedRuleServiceRowCacheQuery extends UnAuditedRuleServiceRowCacheQuery
{
    public function getCacheLayer() : CacheLayer
    {
        return parent::getCacheLayer();
    }

    public function getDbLayer() : DbLayer
    {
        return parent::getDbLayer();
    }
}

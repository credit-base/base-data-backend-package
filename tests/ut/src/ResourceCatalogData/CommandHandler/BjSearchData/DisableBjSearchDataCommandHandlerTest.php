<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Interfaces\ICommand;

use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Command\BjSearchData\DisableBjSearchDataCommand;

class DisableBjSearchDataCommandHandlerTest extends TestCase
{
    private $disableStub;

    private $faker;

    public function setUp()
    {
        $this->disableStub = $this->getMockBuilder(DisableBjSearchDataCommandHandler::class)
                                     ->setMethods(['fetchBjSearchData'])
                                     ->getMock();

        $this->faker = \Faker\Factory::create('zh_CN');
    }

    public function tearDown()
    {
        unset($this->disableStub);
        unset($this->faker);
    }

    public function testCorrectImplementsICommandHandler()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ICommandHandler',
            $this->disableStub
        );
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgumentException()
    {
        $command = new class implements ICommand {
        };
        $this->disableStub->execute($command);
    }

    public function testExecute()
    {
        $command = new DisableBjSearchDataCommand(
            $this->faker->randomNumber(1)
        );

        $bjSearchData = $this->prophesize(BjSearchData::class);
        $bjSearchData->disable()->shouldBeCalledTimes(1)->willReturn(true);

        $this->disableStub->expects($this->once())
             ->method('fetchBjSearchData')
             ->with($command->id)
             ->willReturn($bjSearchData->reveal());

        $result = $this->disableStub->execute($command);
        
        $this->assertTrue($result);
    }
}

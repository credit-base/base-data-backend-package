<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommand;

use BaseData\WorkOrderTask\Command\ParentTask\RevokeParentTaskCommand;
use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;

class RevokeParentTaskCommandHandler implements ICommandHandler
{
    private $repository;

    public function __construct()
    {
        $this->repository = new ParentTaskRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getParentTaskRepository() : IParentTaskAdapter
    {
        return $this->repository;
    }

    public function execute(ICommand $command)
    {
        if (!($command instanceof RevokeParentTaskCommand)) {
            throw new \InvalidArgumentException;
        }

        $repository = $this->getParentTaskRepository();
        $parentTask = $repository->fetchOne($command->id);
        
        return $parentTask->revoke($command->reason);
    }
}

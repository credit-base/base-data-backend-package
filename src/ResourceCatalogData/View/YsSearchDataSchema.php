<?php
namespace BaseData\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class YsSearchDataSchema extends SchemaProvider
{
    protected $resourceType = 'ysSearchData';

    public function getId($ysSearchData) : int
    {
        return $ysSearchData->getId();
    }

    public function getAttributes($ysSearchData) : array
    {
        return [
            'infoClassify' => $ysSearchData->getInfoClassify(),
            'infoCategory' => $ysSearchData->getInfoCategory(),
            'subjectCategory' => $ysSearchData->getSubjectCategory(),
            'dimension' => $ysSearchData->getDimension(),
            'name' => $ysSearchData->getName(),
            'identify' => $ysSearchData->getIdentify(),
            'expirationDate' => $ysSearchData->getExpirationDate(),
            'status' => $ysSearchData->getStatus(),
            'createTime' => $ysSearchData->getCreateTime(),
            'updateTime' => $ysSearchData->getUpdateTime(),
            'statusTime' => $ysSearchData->getStatusTime(),
        ];
    }

    public function getRelationships($ysSearchData, $isPrimary, array $includeList)
    {
        unset($isPrimary);
        unset($includeList);
        
        return [
            'crew' => [self::DATA => $ysSearchData->getCrew()],
            'sourceUnit' => [self::DATA => $ysSearchData->getSourceUnit()],
            'itemsData' => [self::DATA => $ysSearchData->getItemsData()],
            'template' => [self::DATA => $ysSearchData->getTemplate()]
        ];
    }
}

<?php
namespace BaseData\Template\Model;

use Marmot\Core;

use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\Template\Repository\BaseTemplateRepository;
use BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class BaseTemplate extends Template implements IOperate
{
    use OperateTrait;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new BaseTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['BASE'];
    }

    protected function getRepository() : IBaseTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        return false;
    }

    /**
     * @todo 编辑时，本次未考虑目录版本化
     */
    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));

        return $this->getRepository()->edit(
            $this,
            array(
                'subjectCategory',
                'dimension',
                'infoClassify',
                'infoCategory',
                'items',
                'updateTime'
            )
        );
    }
}

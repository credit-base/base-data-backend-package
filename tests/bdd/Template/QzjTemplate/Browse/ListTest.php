<?php
namespace BaseData\Template\QzjTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录时,在目录管理子系统下的前置机资源目录管理中,可以查看到我所属委办局下的所有资源目录,
 *           通过列表和详情的形式呈现,以便于我可以更好的管理资源目录
 * @Scenario: 查看资源目录列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @Given: 存在资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qzj_template' => $this->qzjTemplate(),
        ]);
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchQzjTemplateList()
    {
        $repository = new QzjTemplateRepository();

        list($qzjTemplateList, $count) = $repository->filter([]);
        
        unset($count);

        return $qzjTemplateList;
    }

    /**
     * @Then: 我可以看到资源目录的全部信息
     */
    public function testViewQzjTemplateList()
    {
        $setQzjTemplateList = $this->qzjTemplate();

        foreach ($setQzjTemplateList as $key => $setQzjTemplate) {
            $setQzjTemplateList[$key]['qzj_template_id'] = $key +1;
            $setQzjTemplateList[$key]['subject_category'] = json_decode($setQzjTemplate['subject_category'], true);
            $setQzjTemplateList[$key]['items'] = json_decode($setQzjTemplate['items'], true);
        }

        $qzjTemplateList = $this->fetchQzjTemplateList();

        $translator = new QzjTemplateDbTranslator();
        foreach ($qzjTemplateList as $qzjTemplate) {
            $qzjTemplateArray[] = $translator->objectToArray($qzjTemplate);
        }

        $this->assertEquals($qzjTemplateArray, $setQzjTemplateList);
    }
}

<?php
namespace BaseData\Template\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;
use BaseData\Template\Model\NullQzjTemplate;
use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\View\QzjTemplateView;

class QzjTemplateFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(QzjTemplateFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockQzjTemplateFetchController();

        $this->assertInstanceOf(
            'BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockQzjTemplateFetchController();

        $this->assertInstanceOf(
            'BaseData\Template\View\QzjTemplateView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockQzjTemplateFetchController();

        $this->assertEquals(
            'qzjTemplates',
            $controller->getResourceName()
        );
    }
}

<?php
namespace BaseData\WorkOrderTask\ParentTask\Operate;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\WorkOrderTask\SetDataApiTrait;
use BaseData\WorkOrderTask\ParentTask\Model\ParentTask;

/**
 * 测试父工单任务接口撤销
 */
class RevokeTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_parent_task');
        $this->clear('pcore_work_order_task');
    }

    /**
     * 存在一条父工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_parent_task' => $this->parentTask(),
                'pcore_work_order_task' => $this->workOrderTask(),
            ]
        );
    }

    public function testViewParentTask()
    {
        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $data = array(
            "data"=>array(
                "type"=>"parentTasks",
                "attributes"=>array(
                    "reason"=>'撤销原因'
                )
            )
        );

        $response = $client->request(
            'PATCH',
            'parentTasks/1/revoke',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(200, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $attributes = $contents['data']['attributes'];

        $this->assertEquals('撤销原因', $attributes['reason']);
    }
}

<?php
namespace BaseData\Rule\Command\RuleService;

use Marmot\Interfaces\ICommand;

class DeleteRuleServiceCommand implements ICommand
{
    public $crew;

    public $id;

    public function __construct(
        int $crew,
        int $id
    ) {
        $this->crew = $crew;
        $this->id = $id;
    }
}

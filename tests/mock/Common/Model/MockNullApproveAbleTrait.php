<?php
namespace BaseData\Common\Model;

class MockNullApproveAbleTrait
{
    use NullApproveAbleTrait;

    protected function resourceNotExist() : bool
    {
        return false;
    }
}

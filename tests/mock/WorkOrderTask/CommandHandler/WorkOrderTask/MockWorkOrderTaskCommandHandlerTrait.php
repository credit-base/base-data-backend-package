<?php
namespace BaseData\WorkOrderTask\CommandHandler\WorkOrderTask;

use BaseData\Crew\Adapter\Crew\ICrewAdapter;
use BaseData\Crew\Model\Crew;

use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;
use BaseData\UserGroup\Model\UserGroup;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;
use BaseData\Template\Model\WbjTemplate;

class MockWorkOrderTaskCommandHandlerTrait
{
    use WorkOrderTaskCommandHandlerTrait;

    public function getCrewRepositoryPublic() : ICrewAdapter
    {
        return $this->getCrewRepository();
    }

    public function getUserGroupRepositoryPublic() : IUserGroupAdapter
    {
        return $this->getUserGroupRepository();
    }

    public function getWbjTemplateRepositoryPublic() : IWbjTemplateAdapter
    {
        return $this->getWbjTemplateRepository();
    }

    public function fetchOneCrewPublic($id) : Crew
    {
        return $this->fetchOneCrew($id);
    }

    public function fetchOneUserGroupPublic($id) : UserGroup
    {
        return $this->fetchOneUserGroup($id);
    }

    public function fetchOneWbjTemplatePublic($id) : WbjTemplate
    {
        return $this->fetchOneWbjTemplate($id);
    }
}

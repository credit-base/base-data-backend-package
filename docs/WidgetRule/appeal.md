# 异议申诉控件规范

### 控件类型(input,select...)-名称

**title** detail

#### 规则

* 控件规则1
* 控件规则2
* 控件规则3
* ....

#### 错误提示

* `ER-id1`
* `ER-id2`
* ...

---

## 异议申诉控件规范

* `TITLE`: 标题控件规范.
* `CONTENT`: 内容控件规范.
* `MEMBER`: 前台用户控件规范.
* `ACCEPT_USER_GROUP`: 受理委办局控件规范.
* `REPLAY_CONTENT`: 回复内容控件规范.
* `REPLAY_IMAGES`: 回复图片控件规范.
* `REPLAY_CREW`: 受理人控件规范.
* `REPLAY_ADMISSIBILITY`: 回复受理情况控件规范.
* `TYPE`: 类型控件规范.
* `NAME`: 姓名/企业名称控件规范.
* `IDENTIFY`: 统一社会信用代码/身份证号控件规范.
* `CONTACT`: 联系方式控件规范.
* `CERTIFICATES`: 上传身份证/营业执照控件规范.
* `IMAGES`: 图片控件规范.

### `TITLE`
* [标题控件规范(TITLE)](interaction.md)

### `CONTENT`
* [内容控件规范(CONTENT)](interaction.md)

### `MEMBER`
* [前台用户控件规范(MEMBER)](interaction.md)

### `ACCEPT_USER_GROUP`
* [受理委办局控件规范(ACCEPT_USER_GROUP)](interaction.md)

### `REPLAY_CONTENT`
* [回复内容控件规范(REPLAY_CONTENT)](interaction.md)

### `REPLAY_IMAGES`
* [回复图片控件规范(REPLAY_IMAGES)](interaction.md)

### `REPLAY_CREW`
* [受理人控件规范(REPLAY_CREW)](interaction.md)

### `REPLAY_ADMISSIBILITY`
* [回复受理情况控件规范(REPLAY_ADMISSIBILITY)](interaction.md)

### `TYPE`

**类型** 用于表述类型

#### 规则

* int, 内容必须在以下范围内

	* TYPE[PERSONAL] 1 个人
	* TYPE[ENTERPRISE] 2 企业
* 类型范围必须在以上范围内 

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `NAME`

**姓名/企业名称** 用于表述姓名/企业名称

#### 规则

* 不能为空
* min: 2 max: 50 字符

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `IDENTIFY`

**统一社会信用代码/身份证号** 用于表述统一社会信用代码/身份证号

#### 规则

* 不能为空
* 15 或 18位字符

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `CONTACT`

**联系方式** 用于表述联系方式

#### 规则

* 不能为空
* min: 1 max: 26 字符

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`, 数据格式不正确

### `IMAGES`

**图片** 控件规范，用于图片

#### 规则

* jpg,png,jpeg
* 附件不大于2M
* 最多9张

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`: 图片格式不正确

### `CERTIFICATES`

**上传身份证/营业执照** 控件规范，用于上传身份证/营业执照证件图

#### 规则

* jpg,png,jpeg
* 附件不大于2M
* 最多2张

#### 错误提示

* `ER-0100`, 数据不能为空
* `ER-0101`: 图片格式不正确

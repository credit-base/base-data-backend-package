# 去重规则BDD测试

## 测试用例

* 去重
	* 有去重规则的去重验证
	* 没有去重规则的去重验证

### 1. 有去重规则的去重验证

#### 1.1 背景

设定行政许可的去重规则，选取字段

* 行政相对人名称
* 行政相对人代码_1(统一社会信用代码)

做唯一判断，验证数据的`hash`一致 或 不能入库

两条行政许可信息<font color='red'>完全一致</font>

修改其中一条行政许可信息中的<font color='red'>法定代表人</font>，期望<font color='red'>不可以</font>入库

#### 1.2 数据准备

#### 1.2.1 规则

```php
$rules = ['transformationRule'=>[],'deDuplicationRule'=>''];

//转换规则
$rules['transformationRule'] = [
    "ZTMC" => "ZTMC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "TYSHXYDM" => "TYSHXYDM",//2
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    "BZ" => "BZ"//27
];

$rules['deDuplicationRule'] = ["result"=>1, "items"=>array("ZTMC","TYSHXYDM")];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    );
//测试结果-唯一-GbSearchData
array(
	'infoClassify' => 1,
	'infoCategory' => 1,
	'crewId' => 1,
	'sourceUnitId' => 1,
	'subjectCategory' => 1,
	'dimension' => 1,
	'name' => '濮阳市嘉业运输有限公司',
	'identify' => '91410900MA46184E7Q',
	'expirationDate' => 20991231,
	'hash' => '8a788a3b4319407a9613bed48abc250b',
	'templateId' => 1,
	'itemsData' => array(
		'ZTMC' => "濮阳市嘉业运输有限公司",
		'XK_XDR_LB' => "法人及非法人组织",
		'TYSHXYDM' => "91410900MA46184E7Q",
		'XK_XDR_GSZC' => "91410900MA46184E7Q",
		'XK_XDR_ZZJG' => "91410900MA46184E7Q",
		'XK_XDR_SWDJ' => "91410900MA46184E7Q",
		'XK_XDR_SYDW' => "91410900MA46184E7Q",
		'XK_XDR_SHZZ' => "MA46184E-7",
		'XK_FRDB' => "张晓苹",
		'XK_FR_ZJLX' => "身份证号",
		'XK_FR_ZJHM' => "412825199809097654",
		'XK_XDR_ZJLX' => "身份证号",
		'XK_XDR_ZJHM' => "412825199809097654",
		'XK_XKWS' => "高速公路联网管理中心交通行政许可决定书",
		'XK_WSH' => "豫高速公路许 字第20210705000090号",
		'XK_XKLB' => "普通",
		'XK_XKZS' => "高速公路超限运输车辆通行证",
		'XK_XKBH' => "410000017210705A000090",
		'XK_NR' => "高速公路超限运输车辆通行证",
		'XK_JDRQ' => "20210714",
		'XK_YXQZ' => "20210714",
		'XK_YXQZI' => "20210807",
		'XK_XKJG' => "河南省高速公路联网管理中心",
		'XK_XKJGDM' => "12410000MB14527979",
		'XK_ZT' => "1",
		'XK_LYDW' => "河南省高速公路联网管理中心",
		'XK_LYDWDM' => "12410000MB14527979",
		'BZ' => ""
	),
	'statusTime' => 0,
	'status' => 0,
	'createTime' => '1626506618',
	'updateTime' => '1626506618'
);    
//测试结果-不唯一-FailureSearchData
array(
	'errorType' => 4,
	'errorReason' => '数据重复,去重失败',
	'type'=>2,
	'infoClassify' => 1,
	'infoCategory' => 1,
	'crewId' => 1,
	'sourceUnitId' => 1,
	'subjectCategory' => 1,
	'dimension' => 1,
	'name' => '濮阳市嘉业运输有限公司',
	'identify' => '91410900MA46184E7Q',
	'expirationDate' => 20991231,
	'hash' => 'f7008574848bca7580350fbcc913ddf6',
	'templateId' => 1,
	'itemsData' => array(
		'ZTMC' => "濮阳市嘉业运输有限公司",
		'XK_XDR_LB' => "法人及非法人组织",
		'TYSHXYDM' => "91410900MA46184E7Q",
		'XK_XDR_GSZC' => "91410900MA46184E7Q",
		'XK_XDR_ZZJG' => "91410900MA46184E7Q",
		'XK_XDR_SWDJ' => "91410900MA46184E7Q",
		'XK_XDR_SYDW' => "91410900MA46184E7Q",
		'XK_XDR_SHZZ' => "MA46184E-7",
		'XK_FRDB' => "张晓苹",
		'XK_FR_ZJLX' => "身份证号",
		'XK_FR_ZJHM' => "412825199809097654",
		'XK_XDR_ZJLX' => "身份证号",
		'XK_XDR_ZJHM' => "412825199809097654",
		'XK_XKWS' => "高速公路联网管理中心交通行政许可决定书",
		'XK_WSH' => "豫高速公路许 字第20210705000090号",
		'XK_XKLB' => "普通",
		'XK_XKZS' => "高速公路超限运输车辆通行证",
		'XK_XKBH' => "410000017210705A000090",
		'XK_NR' => "高速公路超限运输车辆通行证",
		'XK_JDRQ' => "20210714",
		'XK_YXQZ' => "20210714",
		'XK_YXQZI' => "20210807",
		'XK_XKJG' => "河南省高速公路联网管理中心",
		'XK_XKJGDM' => "12410000MB14527979",
		'XK_ZT' => "1",
		'XK_LYDW' => "河南省高速公路联网管理中心",
		'XK_LYDWDM' => "12410000MB14527979",
		'BZ' => ""
	),
	'statusTime' => 0,
	'status' => 0,
	'createTime' => '1626506618',
	'updateTime' => '1626506618'
);

```

#### 1.2.2 第一条行政许可 <font color='red'>委办局</font>

默认数据

#### 1.2.3 第二条行政许可 <font color='red'>委办局</font> 

数据项 | 数据 | 备注
----- | ---- | -----
行政相对人名称 |濮阳市嘉业运输有限公司 | |
行政相对人类别 |法人及非法人组织 | |
行政相对人代码_1(统一社会信用代码) |91410900MA46184E7Q | |
行政相对人代码_2 (工商注册号) | | |
行政相对人代码_3(组织机构代码) | | |
行政相对人代码_4(税务登记号) | | |
行政相对人代码_5(事业单位证书号)| | |
行政相对人代码_6((社会组织登记证号) | MA46184E-7| |
法定代表人 | 李勇|<font color='red'>与第一条不一致</font> |
法定代表人证件类型 | | |
法定代表人证件号码 | | |
证件类型 | | |
证件号码 | | |
行政许可决定文书名称 |高速公路联网管理中心交通行政许可决定书 | |
行政许可决定文书号 |豫高速公路许 字第20210705000090号 | |
许可类别 |普通 | |
许可证书名称 |高速公路超限运输车辆通行证 | |
许可编号 |410000017210705A000090 | |
许可内容 |高速公路超限运输车辆通行证 | |
许可决定日期 |20210714 | |
有效期自 | 20210714 | |
有效期至 | 20210807 | |
许可机关 |河南省高速公路联网管理中心 | |
许可机关统一社会信用代码 |12410000MB14527979 | |
当前状态 |1 | |
数据来源单位 | 河南省高速公路联网管理中心| |
数据来源单位统一社会信用代码 |12410000MB14527979 | |
备注 | | |

```php
$itemsData = [
	'ZTMC' => '濮阳市嘉业运输有限公司',
	'XK_XDR_LB' => '法人及非法人组织',
	'TYSHXYDM' => '91410900MA46184E7Q',
	'XK_XDR_GSZC' => '',
	'XK_XDR_ZZJG' => '',
	'XK_XDR_SWDJ' => '',,
	'XK_XDR_SYDW' => '',,
	'XK_XDR_SHZZ' => 'MA46184E-7',,
	'XK_FRDB' => '李勇',
	'XK_FR_ZJLX' => '',
	'XK_FR_ZJHM' => '',
	'XK_XDR_ZJLX' => '',
	'XK_XDR_ZJHM' => '',
	'XK_XKWS' => '高速公路联网管理中心交通行政许可决定书',
	'XK_WSH' => '豫高速公路许 字第20210705000090号',
	'XK_XKLB' => '普通',
	'XK_XKZS' => '高速公路超限运输车辆通行证',
	'XK_XKBH' => '410000017210705A000090',
	'XK_NR' => '高速公路超限运输车辆通行证',
	'XK_JDRQ' => '20210714',
	'XK_YXQZ' => '20210714',
	'XK_YXQZI' => '20210807',
	'XK_XKJG' => '河南省高速公路联网管理中心',
	'XK_XKJGDM' => '12410000MB14527979',
	'XK_ZT' => '1',
	'XK_LYDW' => '河南省高速公路联网管理中心',
	'XK_LYDWDM' => '12410000MB14527979',
	'BZ'  => ''
];
```

### 2. 没有去重规则的去重验证

#### 2.1 背景

无去重规则，按照默认全部字段做`hash`规则走

两条行政许可信息<font color='red'>完全一致</font>

修改其中一条行政许可信息中的<font color='red'>法定代表人</font>，期望<font color='red'>可以</font>入库

#### 2.2 数据准备

#### 2.2.1 规则

```php
$rules = ['transformationRule'=>[]];

//转换规则
$rules['transformationRule'] = [
    "ZTMC" => "ZTMC",//0
    "XK_XDR_LB" => "XK_XDR_LB",//1
    "TYSHXYDM" => "TYSHXYDM",//2
    "XK_XDR_GSZC" => "XK_XDR_GSZC",//3
    "XK_XDR_ZZJG" => "XK_XDR_ZZJG",//4
    "XK_XDR_SWDJ" => "XK_XDR_SWDJ",//5
    "XK_XDR_SYDW" => "XK_XDR_SYDW",//6
    "XK_XDR_SHZZ" => "XK_XDR_SHZZ",//7
    "XK_FRDB" => "XK_FRDB",//8
    "XK_FR_ZJLX" => "XK_FR_ZJLX",//9
    "XK_FR_ZJHM" => "XK_FR_ZJHM",//10
    "XK_XDR_ZJLX" => "XK_XDR_ZJLX",//11
    "XK_XDR_ZJHM" => "XK_XDR_ZJHM",//12
    "XK_XKWS" => "XK_XKWS",//13
    "XK_WSH" => "XK_WSH",//14
    "XK_XKLB" => "XK_XKLB",//15
    "XK_XKZS" => "XK_XKZS",//16
    "XK_XKBH" => "XK_XKBH",//17
    "XK_NR" => "XK_NR",//18
    "XK_JDRQ" => "XK_JDRQ",//19
    "XK_YXQZ" => "XK_YXQZ",//20
    "XK_YXQZI" => "XK_YXQZI",//21
    "XK_XKJG" => "XK_XKJG",//22
    "XK_XKJGDM" => "XK_XKJGDM",//23
    "XK_ZT" => "XK_ZT",//24
    "XK_LYDW" => "XK_LYDW",//25
    "XK_LYDWDM" => "XK_LYDWDM",//26
    "BZ" => "BZ"//27
];

$data = array(
    'data' => array(
        "type"=>"rules",
        "attributes"=>array(
            "rules"=>$rules,
            "type" => 2
        ),
        "relationships"=>array(
            "crew"=>array(
                "data"=>array(
                    array("type"=>"crews","id"=>1)
                )
            ),
            "transformationTemplate"=>array(
                "data"=>array(
                    array("type"=>"gbTemplates","id"=>1)
                )
            ),
            "sourceTemplate"=>array(
                "data"=>array(
                    array("type"=>"wbjTemplates","id"=>1)
                )
            ),
        )
    );
```
  
#### 122.2 第一条行政许可 <font color='red'>委办局</font>

默认数据

#### 2.2.3 第二条行政许可 <font color='red'>委办局</font> 

数据项 | 数据 | 备注
----- | ---- | -----
行政相对人名称 |濮阳市嘉业运输有限公司 | |
行政相对人类别 |法人及非法人组织 | |
行政相对人代码_1(统一社会信用代码) |91410900MA46184E7Q | |
行政相对人代码_2 (工商注册号) | | |
行政相对人代码_3(组织机构代码) | | |
行政相对人代码_4(税务登记号) | | |
行政相对人代码_5(事业单位证书号)| | |
行政相对人代码_6((社会组织登记证号) | MA46184E-7| |
法定代表人 | 李勇|<font color='red'>与第一条不一致</font> |
法定代表人证件类型 | | |
法定代表人证件号码 | | |
证件类型 | | |
证件号码 | | |
行政许可决定文书名称 |高速公路联网管理中心交通行政许可决定书 | |
行政许可决定文书号 |豫高速公路许 字第20210705000090号 | |
许可类别 |普通 | |
许可证书名称 |高速公路超限运输车辆通行证 | |
许可编号 |410000017210705A000090 | |
许可内容 |高速公路超限运输车辆通行证 | |
许可决定日期 |20210714 | |
有效期自 | 20210714 | |
有效期至 | 20210807 | |
许可机关 |河南省高速公路联网管理中心 | |
许可机关统一社会信用代码 |12410000MB14527979 | |
当前状态 |1 | |
数据来源单位 | 河南省高速公路联网管理中心| |
数据来源单位统一社会信用代码 |12410000MB14527979 | |
备注 | | |

```php
$itemsData = [
	'ZTMC' => '濮阳市嘉业运输有限公司',
	'XK_XDR_LB' => '法人及非法人组织',
	'TYSHXYDM' => '91410900MA46184E7Q',
	'XK_XDR_GSZC' => '',
	'XK_XDR_ZZJG' => '',
	'XK_XDR_SWDJ' => '',,
	'XK_XDR_SYDW' => '',,
	'XK_XDR_SHZZ' => 'MA46184E-7',,
	'XK_FRDB' => '李勇',
	'XK_FR_ZJLX' => '',
	'XK_FR_ZJHM' => '',
	'XK_XDR_ZJLX' => '',
	'XK_XDR_ZJHM' => '',
	'XK_XKWS' => '高速公路联网管理中心交通行政许可决定书',
	'XK_WSH' => '豫高速公路许 字第20210705000090号',
	'XK_XKLB' => '普通',
	'XK_XKZS' => '高速公路超限运输车辆通行证',
	'XK_XKBH' => '410000017210705A000090',
	'XK_NR' => '高速公路超限运输车辆通行证',
	'XK_JDRQ' => '20210714',
	'XK_YXQZ' => '20210714',
	'XK_YXQZI' => '20210807',
	'XK_XKJG' => '河南省高速公路联网管理中心',
	'XK_XKJGDM' => '12410000MB14527979',
	'XK_ZT' => '1',
	'XK_LYDW' => '河南省高速公路联网管理中心',
	'XK_LYDWDM' => '12410000MB14527979',
	'BZ'  => ''
];
```
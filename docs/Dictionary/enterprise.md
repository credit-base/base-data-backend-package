# 企业字典

### 英文名称

**中文名称** 描述信息

---

### name

**主体名称** 主体名称的表述.

* string

### unifiedSocialCreditCode

**统一社会信用代码** 统一社会信用代码的表述.

* string

### establishmentDate

**成立日期/注册日期** 成立日期/注册日期的表述.

* int
* YYYYMMDD

### approvalDate

**核准日期** 核准日期的表述.

* int
* YYYYMMDD

### address

**住所/经营场所** 住所/经营场所的表述.

* string

### registrationCapital

**注册资本（万）** 注册资本（万）的表述.

* string

### businessTermStart

**营业期限自** 营业期限自的表述.

* int
* YYYYMMDD

### businessTermTo

**营业期限至** 营业期限至的表述.

* int
* YYYYMMDD

### businessScope

**经营范围** 经营范围的表述.

* string

### registrationAuthority

**登记机关** 登记机关的表述.

* string

### principal

**法定代表人/经营者** 法定代表人/经营者的表述.

* string

### principalCardId

**法人身份证号** 法人身份证号的表述.

* string

### registrationStatus

**登记状态** 登记状态的表述.

* string
* 1=存续(在营、开业、在册);2=吊销，未注销;3=吊 销，已注销;4=注销;5=撤 销;6=迁出;9=其他

### enterpriseTypeCode

**企业类型代码** 企业类型代码的表述.

* string

### enterpriseType

**企业类型** 企业类型的表述.

* string

## data

**其他数据** 其他数据的表述.

* array

### industryCategory

**行业门类** 行业门类的表述.

* string

	```
	INDUSTRY_CATEGORY_CN => array(
		'' => '未知',
		'A' => '农、林、牧、渔业',
		'B' => '采矿业',
		'C' => '制造业',
		'D' => '电力、热力、燃气及水生产和供应业',
		'E' => '建筑业',
		'F' => '交通运输、仓储和邮政业',
		'G' => '信息传输、计算机服务和软件业',
		'H' => '批发和零售业',
		'I' => '住宿和餐饮业',
		'J' => '金融业',
		'K' => '房地产业',
		'L' => '租赁和商务服务业',
		'M' => '科学研究和技术服务业',
		'N' => '水利、环境和公共设施管理业',
		'O' => '居民服务、修理和其他服务业',
		'P' => '教育',
		'Q' => '卫生和社会工作',
		'R' => '文化、体育和娱乐业',
		'S' => '公共管理、社会保障和社会组织',
		'T' => '国际组织',
		'0' => '未知'
	)
	```

### industryCode

**行业代码** 行业代码的表述.

* string

### administrativeArea

**所属区域** 所属区域的表述.

* int
* 参考:http://www.ccb.com/cn/OtherResource/bankroll/html/code_help.html

### status

**状态** 企业状态的表述.

* int

	* STATUS_NORMAL | 正常 | 0
	* STATUS_DELETED | 删除 | -2

### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

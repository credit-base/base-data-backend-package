<?php
namespace BaseData\Task\Adapter\Task;

use BaseData\Task\Model\Task;
use BaseData\Task\Utils\MockFactory;

class TaskMockAdapter implements ITaskAdapter
{
    public function fetchOne($id) : Task
    {
        return MockFactory::generateTask($id);
    }

    public function fetchList(array $ids) : array
    {
        $taskList = array();

        foreach ($ids as $id) {
            $taskList[$id] = MockFactory::generateTask($id);
        }

        return $taskList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(Task $task) : bool
    {
        unset($task);
        return true;
    }

    public function edit(Task $task, array $keys = array()) : bool
    {
        unset($task);
        unset($keys);
        return true;
    }

    public function notifySubTasks(Task $parentTask) : bool
    {
        unset($parentTask);
        return true;
    }

    public function schedule() : array
    {
        $ids = [1, 2, 3, 4];

        $taskList = array();

        foreach ($ids as $id) {
            $taskList[$id] = MockFactory::generateTask($id);
        }

        return $taskList;
    }

    public function expire() : array
    {
        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }
}

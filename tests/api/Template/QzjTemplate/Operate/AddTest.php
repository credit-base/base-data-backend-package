<?php
namespace BaseData\Template\QzjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\QzjTemplate;

/**
 * 测试前置机资源目录接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    private $qzjTemplate;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->qzjTemplate);
        $this->clear('pcore_qzj_template');
    }

    /**
     * 不存在前置机资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            []
        );
    }

    public function testViewQzjTemplate()
    {
        $setQzjTemplate = $this->qzjTemplate()[0];
        $items = $this->items();

        $data = array(
            "data" => array(
                "type" => "qzjTemplates",
                "attributes" => array(
                    "name" => '前置机企业基本信息',
                    "identify" => 'QZJQYJBXX',
                    "category" => QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'],
                    "subjectCategory" => array(Template::SUBJECT_CATEGORY['FRJFFRZZ']),
                    "dimension" => Template::DIMENSION['SHGK'],
                    "exchangeFrequency" => 1,
                    "infoClassify" => Template::INFO_CLASSIFY['QT'],
                    "infoCategory" => Template::INFO_CATEGORY['QTXX'],
                    "description" => "前置机企业基本信息",
                    "items" => $items
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'qzjTemplates',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->qzjTemplateCompareData($setQzjTemplate, $contents['data']);
    }
}

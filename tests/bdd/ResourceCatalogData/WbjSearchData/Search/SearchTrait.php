<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Search;

use PHPUnit\DbUnit\DataSet\ArrayDataSet;

trait SearchTrait
{
    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
            'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data'],
            'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data']
        ]);
    }
}

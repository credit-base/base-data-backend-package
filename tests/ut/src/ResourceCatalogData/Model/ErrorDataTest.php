<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ErrorDataTest extends TestCase
{
    private $errorData;

    public function setUp()
    {
        $this->errorData = new ErrorData();
    }

    public function tearDown()
    {
        unset($this->errorData);
    }

    /**
     * ErrorData 测试构造函数
     */
    public function testConstructor()
    {
        $this->assertEquals(0, $this->errorData->getId());
        $this->assertEquals(0, $this->errorData->getCategory());
        $this->assertEquals(0, $this->errorData->getErrorType());
        $this->assertInstanceof(
            'BaseData\Template\Model\Template',
            $this->errorData->getTemplate()
        );
        $this->assertInstanceof(
            'BaseData\ResourceCatalogData\Model\ErrorItemsData',
            $this->errorData->getItemsData()
        );
        $this->assertEquals(array(), $this->errorData->getErrorReason());
        $this->assertEquals(ErrorData::STATUS['NORMAL'], $this->errorData->getStatus());
    }

    public function testExtendsSearchData()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Model\SearchData',
            $this->errorData
        );
    }

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->errorData->setCategory(1);
        $this->assertEquals(1, $this->errorData->getCategory());
    }

    /**
     * 设置 ErrorData setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->errorData->setCategory(array(1,2,3));
    }
    //category 测试 --------------------------------------------------------   end

    //itemsData 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setItemsData() 正确的传参类型,期望传值正确
     */
    public function testSetItemsDataCorrectType()
    {
        $expectedItemsData = new ErrorItemsData();

        $this->errorData->setItemsData($expectedItemsData);
        $this->assertEquals($expectedItemsData, $this->errorData->getItemsData());
    }

    /**
     * 设置 ErrorData setItemsData() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsDataWrongType()
    {
        $this->errorData->setItemsData('itemsData');
    }
    //itemsData 测试 --------------------------------------------------------   end

    //errorReason 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setErrorReason() 正确的传参类型,期望传值正确
     */
    public function testSetErrorReasonCorrectType()
    {
        $this->errorData->setErrorReason(array('string'));
        $this->assertEquals(array('string'), $this->errorData->getErrorReason());
    }

    /**
     * 设置 ErrorData setErrorReason() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorReasonWrongType()
    {
        $this->errorData->setErrorReason('errorReason');
    }
    //errorReason 测试 --------------------------------------------------------   end
    //status 测试 ------------------------------------------------------ start
    /**
     * 循环测试 setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->errorData->setStatus($actual);
        $this->assertEquals($expected, $this->errorData->getStatus());
    }
    /**
     * 循环测试 DispatchDepartment setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(ErrorData::STATUS['NORMAL'],ErrorData::STATUS['NORMAL']),
            array(ErrorData::STATUS['PROGRAM_EXCEPTION'],ErrorData::STATUS['PROGRAM_EXCEPTION']),
            array(ErrorData::STATUS['STORAGE_EXCEPTION'],ErrorData::STATUS['STORAGE_EXCEPTION']),
            array(999, ErrorData::STATUS['NORMAL'])
        );
    }
    /**
     * 设置 setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->errorData->setStatus('status');
    }
    //status 测试 ------------------------------------------------------   end
    
    //errorType 测试 -------------------------------------------------------- start
    /**
     * 设置 ErrorData setErrorType() 正确的传参类型,期望传值正确
     */
    public function testSetErrorTypeCorrectType()
    {
        $this->errorData->setErrorType(0);
        $this->assertEquals(0, $this->errorData->getErrorType());
    }

    /**
     * 设置 ErrorData setErrorType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorTypeWrongType()
    {
        $this->errorData->setErrorType(array(1,2,3));
    }
    //errorType 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $errorData = new MockErrorData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $errorData->getRepository()
        );
    }

    public function testAddSuccess()
    {
        //初始化
        $errorData = $this->getMockBuilder(MockErrorData::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IErrorDataAdapter
        $repository = $this->prophesize(IErrorDataAdapter::class);
        $repository->add(Argument::exact($errorData))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $errorData->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $errorData->add();
        $this->assertTrue($result);
    }
}

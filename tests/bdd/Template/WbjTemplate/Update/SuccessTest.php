<?php
namespace BaseData\Template\WbjTemplate\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Repository\WbjTemplateRepository;
use BaseData\Template\Translator\WbjTemplateDbTranslator;

/**
 * @Feature: 我是委办局领导,当我之前的委办局目录需要更改时,在目录管理子系统,修改委办局目录内容
 *           根据我需要 修改的内容和规范修改原有委办局目录
 *           以便于我有委办局目录可以使用
 * @Scenario: 编辑资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_wbj_template' => $this->wbjTemplate(),
        ]);
    }

    /**
     * @When: 获取需要编辑的用户数据
     */
    public function fetchTemplate($id)
    {
        $repository = new WbjTemplateRepository();

        $wbjTemplate = $repository->fetchOne($id);

        return $wbjTemplate;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $wbjTemplate = $this->fetchTemplate(1);

        $wbjTemplate->setSubjectCategory(array(Template::SUBJECT_CATEGORY['ZRR']));
        $wbjTemplate->setDimension(Template::DIMENSION['ZWGX']);
        $wbjTemplate->setExchangeFrequency(2);
        $wbjTemplate->setInfoClassify(Template::INFO_CLASSIFY['QT']);
        $wbjTemplate->setInfoCategory(Template::INFO_CATEGORY['QTXX']);
        $wbjTemplate->setItems(array('items'));
        
        return $wbjTemplate->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $wbjTemplate = $this->fetchTemplate(1);

        $this->assertEquals(array(Template::SUBJECT_CATEGORY['ZRR']), $wbjTemplate->getSubjectCategory());
        $this->assertEquals(Template::DIMENSION['ZWGX'], $wbjTemplate->getDimension());
        $this->assertEquals(2, $wbjTemplate->getExchangeFrequency());
        $this->assertEquals(Template::INFO_CLASSIFY['QT'], $wbjTemplate->getInfoClassify());
        $this->assertEquals(Template::INFO_CATEGORY['QTXX'], $wbjTemplate->getInfoCategory());
        $this->assertEquals(array('items'), $wbjTemplate->getItems());
    }
}

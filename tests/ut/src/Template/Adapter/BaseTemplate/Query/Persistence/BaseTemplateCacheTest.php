<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query\Persistence;

use PHPUnit\Framework\TestCase;

class BaseTemplateCacheTest extends TestCase
{
    private $cache;

    public function setUp()
    {
        $this->cache = new MockBaseTemplateCache();
    }

    /**
     * 测试该文件是否正确的继承cache类
     */
    public function testCorrectInstanceExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->cache);
    }

    /**
     * 测试 key
     */
    public function testGetKey()
    {
        $this->assertEquals(BaseTemplateCache::KEY, $this->cache->getKey());
    }
}

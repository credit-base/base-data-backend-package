<?php
namespace BaseData\Rule\RuleService\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\RuleUtils;
use BaseData\Rule\Model\IRule;
use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\Model\RuleService;

use BaseData\Template\Model\Template;
use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试规则接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, RuleUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_rule');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_wbj_template');
    }

    /**
     * 存在多条规则数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_rule' => $this->rule()
            ]
        );
    }

    public function testViewRuleService()
    {
        $setRuleServiceList = $this->rule();
        $setWbjTemplateList = $this->wbjTemplate();
        $setGbTemplateList = $this->gbTemplate();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'rules?filter[transformationTemplate]=1&filter[userGroup]=1&filter[sourceTemplate]=1&
            filter[status]='.RuleService::STATUS['NORMAL'].
            '&filter[transformationCategory]='.Template::CATEGORY['GB'].
            '&filter[sourceCategory]='.Template::CATEGORY['WBJ'].
            '&filter[excludeTransformationCategory]='.Template::CATEGORY['BJ'].
            '&include=transformationTemplate,sourceTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $ruleService) {
            $this->ruleCompareData($setRuleServiceList[$key], $ruleService);
        }

        $included = $contents['included'];

        $this->gbTemplateCompareData($setGbTemplateList[0], $included[0]);
        $this->wbjTemplateCompareData($setWbjTemplateList[0], $included[1]);
    }
}

<?php
namespace BaseData\Statistical\Adapter\Statistical;

use Marmot\Core;
use Marmot\Framework\Classes\MyPdo;

use BaseData\Statistical\Model\Statistical;
use BaseData\Statistical\Model\NullStatistical;
use BaseData\Statistical\Translator\EnterpriseRelationInformationCountTranslator;

use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class EnterpriseRelationInformationCountAdapter implements IStatisticalAdapter
{
    private $staticsByPolicyAwardPenaltyTranslator;

    public function __construct()
    {
        $this->staticsByPolicyAwardPenaltyTranslator = new EnterpriseRelationInformationCountTranslator();
    }

    public function __destruct()
    {
        unset($this->staticsByPolicyAwardPenaltyTranslator);
    }
    
    protected function getEnterpriseRelationInformationCountTranslator() : EnterpriseRelationInformationCountTranslator
    {
        return $this->staticsByPolicyAwardPenaltyTranslator;
    }
    
    protected function getDbDriver() : MyPdo
    {
        return Core::$dbDriver;
    }

    public function analyse(array $filter = array()) : Statistical
    {
        $info = array();

        $unifiedSocialCreditCode = isset($filter['unifiedSocialCreditCode']) ? $filter['unifiedSocialCreditCode'] : '';
        $dimension = isset($filter['dimension']) ?
                    $filter['dimension'] :
                    ISearchDataAble::DIMENSION['SHGK'].','.ISearchDataAble::DIMENSION['ZWGX'].
                    ','.ISearchDataAble::DIMENSION['SQCX'];

        $info = $this->getDbDriver()->query(
            'call statics_enterprise_relation_information_count(\''.$unifiedSocialCreditCode.'\',\''.$dimension.'\');'
        );

        if (empty($info)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return NullStatistical::getInstance();
        }
        
        return $this->getEnterpriseRelationInformationCountTranslator()->arrayToObject($info[0]);
    }
}

<?php
namespace BaseData\Task\View;

class MockNotifyRecordView extends NotifyRecordView
{
    public function getData()
    {
        return $this->data;
    }
}

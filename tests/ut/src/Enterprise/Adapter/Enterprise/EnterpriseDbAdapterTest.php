<?php
namespace BaseData\Enterprise\Adapter\Enterprise;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Enterprise\Model\Enterprise;
use BaseData\Enterprise\Model\NullEnterprise;
use BaseData\Enterprise\Translator\EnterpriseDbTranslator;
use BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(EnterpriseDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction',
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IEnterpriseAdapter
     */
    public function testImplementsIEnterpriseAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Enterprise\Adapter\Enterprise\IEnterpriseAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 EnterpriseDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockEnterpriseDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Enterprise\Translator\EnterpriseDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 EnterpriseRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockEnterpriseDbAdapter();
        $this->assertInstanceOf(
            'BaseData\Enterprise\Adapter\Enterprise\Query\EnterpriseRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockEnterpriseDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $expectedEnterprise = new Enterprise();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedEnterprise)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedEnterprise);
        $this->assertTrue($result);
    }

    //edit
    public function testEdit()
    {
        //初始化
        $expectedEnterprise = new Enterprise();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedEnterprise, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedEnterprise, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedEnterprise = new Enterprise();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedEnterprise);

        //验证
        $enterprise = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedEnterprise, $enterprise);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $enterpriseOne = new Enterprise(1);
        $enterpriseTwo = new Enterprise(2);

        $ids = [1, 2];

        $expectedEnterpriseList = [];
        $expectedEnterpriseList[$enterpriseOne->getId()] = $enterpriseOne;
        $expectedEnterpriseList[$enterpriseTwo->getId()] = $enterpriseTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedEnterpriseList);

        //验证
        $enterpriseList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedEnterpriseList, $enterpriseList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-unifiedSocialCreditCode
    public function testFormatFilterUnifiedSocialCreditCode()
    {
        $filter = array(
            'unifiedSocialCreditCode' => 'unifiedSocialCreditCode'
        );
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = 'unified_social_credit_code = \''.$filter['unifiedSocialCreditCode'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-uniqueName
    public function testFormatFilterUniqueName()
    {
        $filter = array(
            'uniqueName' => 'name'
        );
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = 'name = \''.$filter['uniqueName'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-name
    public function testFormatFilterName()
    {
        $filter = array(
            'name' => 'name'
        );
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['name'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-principal
    public function testFormatFilterPrincipal()
    {
        $filter = array(
            'principal' => 'principal'
        );
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = 'principal LIKE \'%'.$filter['principal'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }
    
    //formatFilter-identify
    public function testFormatFilterIdentify()
    {
        $filter = array(
            'identify' => 'identify'
        );
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = 'name LIKE \'%'.$filter['identify'].'%\''.
        ' OR unified_social_credit_code LIKE \'%'.$filter['identify'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-hash
    public function testFormatFilterHash()
    {
        $filter = array(
            'hash' => 'hash'
        );
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = 'hash = \''.$filter['hash'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter-task
    public function testFormatFilterTask()
    {
        $filter = array(
            'task' => 1
        );
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = 'task_id = \''.$filter['task'].'\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort--updateTime
    public function testFormatSortUpdateTimeDesc()
    {
        $sort = array('updateTime' => -1);
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = ' ORDER BY update_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort-updateTime
    public function testFormatSortUpdateTimeAsc()
    {
        $sort = array('updateTime' => 1);
        $adapter = new MockEnterpriseDbAdapter();

        $expectedCondition = ' ORDER BY update_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

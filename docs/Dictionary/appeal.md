# 异议申诉项目字典

### 英文名称

**中文名称** 描述信息

---

* [通用字典](./docs/Dictionary/common.md "通用字典")
* [互动类通用字典](./docs/Dictionary/interaction.md "互动类通用字典")

### [标题](interaction.md)
### [内容](interaction.md)
### [前台用户](interaction.md)
### [受理委办局](interaction.md)
### [受理状态](interaction.md)
### [回复信息](interaction.md)
### [状态](interaction.md)
### [创建时间](common.md)
### [更新时间](common.md)
### [状态更新时间](common.md)

### type

**类型** 

* int 
	* TYPE[PERSONAL] 0 个人
	* TYPE[ENTERPRISE] 1 企业
* 必填项

### name

**姓名/企业名称** 

* string 
* 必填项

### identify

**统一社会信用代码/身份证号** 

* string
* 必填项

### contact 

**联系方式** 联系方式的表述.

* string 
* 必填项

### certificates 

**上传身份证/营业执照** 身份证/营业执照证件图的表述.

* array 
* 必填项

### images 

**图片** 图片的表述.

* array 
* 必填项





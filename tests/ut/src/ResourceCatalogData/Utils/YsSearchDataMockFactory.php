<?php
namespace BaseData\ResourceCatalogData\Utils;

use BaseData\ResourceCatalogData\Model\YsSearchData;

use BaseData\Common\Model\IEnableAble;

class YsSearchDataMockFactory
{
    use SearchDataMockFactoryTrait;
    
    public static function generateYsSearchData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : YsSearchData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $ysSearchData = new YsSearchData($id);
        $ysSearchData->setId($id);

        self::generateInfoClassify($ysSearchData, $faker, $value);
        self::generateInfoCategory($ysSearchData, $faker, $value);
        self::generateCrew($ysSearchData, $faker, $value);
        self::generateSourceUnit($ysSearchData, $faker, $value);
        self::generateSubjectCategory($ysSearchData, $faker, $value);
        self::generateDimension($ysSearchData, $faker, $value);
        self::generateExpirationDate($ysSearchData, $faker, $value);
        self::generateStatus($ysSearchData, $faker, $value);
        self::generateTemplate($ysSearchData, $faker, $value);
        self::generateItemsData($ysSearchData, $faker, $value);
        self::generateTask($ysSearchData, $faker, $value);
        
        $ysSearchData->setCreateTime($faker->unixTime());
        $ysSearchData->setUpdateTime($faker->unixTime());
        $ysSearchData->setStatusTime($faker->unixTime());

        return $ysSearchData;
    }

    private static function generateTemplate($ysSearchData, $faker, $value) : void
    {
        $template = isset($value['template']) ?
        $value['template'] :
        \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate($faker->randomDigit());
        
        $ysSearchData->setTemplate($template);
    }

    private static function generateItemsData($ysSearchData, $faker, $value) : void
    {
        $itemsData = isset($value['itemsData']) ?
        $value['itemsData'] : YsItemsDataMockFactory::generateYsItemsData($faker->randomDigit());
        
        $ysSearchData->setItemsData($itemsData);
    }

    protected static function generateStatus($ysSearchData, $faker, $value) : void
    {
        $status = isset($value['status']) ?
            $value['status'] :
            $faker->randomElement(
                IEnableAble::STATUS
            );
        
        $ysSearchData->setStatus($status);
    }
}

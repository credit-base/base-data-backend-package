<?php
namespace BaseData\Template\CommandHandler\GbTemplate;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class GbTemplateCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\Template\Command\GbTemplate\AddGbTemplateCommand' =>
        'BaseData\Template\CommandHandler\GbTemplate\AddGbTemplateCommandHandler',
        'BaseData\Template\Command\GbTemplate\EditGbTemplateCommand' =>
        'BaseData\Template\CommandHandler\GbTemplate\EditGbTemplateCommandHandler',
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

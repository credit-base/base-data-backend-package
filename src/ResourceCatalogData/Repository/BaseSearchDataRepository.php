<?php
namespace BaseData\ResourceCatalogData\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\ResourceCatalogData\Model\BaseSearchData;
use BaseData\ResourceCatalogData\Adapter\BaseSearchData\IBaseSearchDataAdapter;
use BaseData\ResourceCatalogData\Adapter\BaseSearchData\BaseSearchDataDbAdapter;

class BaseSearchDataRepository extends Repository implements IBaseSearchDataAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new BaseSearchDataDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IBaseSearchDataAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    protected function getActualAdapter() : IBaseSearchDataAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IBaseSearchDataAdapter
    {
        return $this->adapter;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }
    
    public function add(BaseSearchData $baseSearchData) : bool
    {
        return $this->getAdapter()->add($baseSearchData);
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\YsSearchData\Query\Persistence;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class YsSearchDataDbTest extends TestCase
{
    private $database;

    public function setUp()
    {
        $this->database = new MockYsSearchDataDb();
    }

    /**
     * 测试该文件是否正确的继承db类
     */
    public function testCorrectInstanceExtendsDb()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Db', $this->database);
    }

    /**
     * 测试 table
     */
    public function testGetTable()
    {
        $this->assertEquals(YsSearchDataDb::TABLE, $this->database->getTable());
    }
}

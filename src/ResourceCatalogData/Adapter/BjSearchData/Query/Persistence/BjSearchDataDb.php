<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData\Query\Persistence;

use Marmot\Framework\Classes\Db;

class BjSearchDataDb extends Db
{
    const TABLE = 'bj_search_data';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

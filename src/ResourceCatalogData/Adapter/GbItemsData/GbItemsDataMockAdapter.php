<?php
namespace BaseData\ResourceCatalogData\Adapter\GbItemsData;

use BaseData\ResourceCatalogData\Model\GbItemsData;
use BaseData\ResourceCatalogData\Utils\GbItemsDataMockFactory;

class GbItemsDataMockAdapter implements IGbItemsDataAdapter
{
    public function fetchOne($id) : GbItemsData
    {
        return GbItemsDataMockFactory::generateGbItemsData($id);
    }

    public function fetchList(array $ids) : array
    {
        $gbItemsDataList = array();

        foreach ($ids as $id) {
            $gbItemsDataList[$id] = GbItemsDataMockFactory::generateGbItemsData($id);
        }

        return $gbItemsDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(GbItemsData $gbItemsData) : bool
    {
        unset($gbItemsData);
        return true;
    }
}

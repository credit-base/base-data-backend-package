<?php
namespace BaseData\ResourceCatalogData\ErrorData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试错误资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils,
    TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_task');
        $this->clear('pcore_error_data');
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * 存在一条错误资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_task' => $this->task(),
                'pcore_error_data' => $this->errorData(),
                'pcore_bj_template' => $this->bjTemplate(),
                'pcore_gb_template' => $this->gbTemplate()
            ]
        );
    }

    public function testViewErrorData()
    {
        $setErrorData = $this->errorData()[0];
        $setTask = $this->task()[0];
        $setTemplate = $this->bjTemplate()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'errorDatas/1?include=task,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->errorDataCompareData($setErrorData, $contents['data']);
        $this->taskCompareData($setTask, $contents['included'][0]);
        $this->bjTemplateCompareData($setTemplate, $contents['included'][1]);
    }
}

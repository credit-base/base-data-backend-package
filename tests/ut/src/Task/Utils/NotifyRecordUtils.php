<?php
namespace BaseData\Task\Utils;

trait NotifyRecordUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $notifyRecord
    ) {
        $this->assertEquals($expectedArray['record_id'], $notifyRecord->getId());
        $this->assertEquals($expectedArray['name'], $notifyRecord->getName());
        $this->assertEquals($expectedArray['hash'], $notifyRecord->getHash());

        $this->assertEquals($expectedArray['status'], $notifyRecord->getStatus());
        $this->assertEquals($expectedArray['create_time'], $notifyRecord->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $notifyRecord->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $notifyRecord->getStatusTime());
    }
}

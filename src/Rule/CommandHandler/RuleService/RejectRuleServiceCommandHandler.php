<?php
namespace BaseData\Rule\CommandHandler\RuleService;

use BaseData\Common\Model\IApproveAble;
use BaseData\Common\Command\RejectCommand;
use BaseData\Common\CommandHandler\RejectCommandHandler;

class RejectRuleServiceCommandHandler extends RejectCommandHandler
{
    use RuleServiceCommandHandlerTrait;

    protected function fetchIApplyObject($id) : IApproveAble
    {
        return $this->fetchUnAuditedRuleService($id);
    }

    protected function executeAction(RejectCommand $command)
    {
        $this->rejectAble = $this->fetchIApplyObject($command->id);

        $this->rejectAble->setRejectReason($command->rejectReason);
        $applyCrew = $this->fetchCrew($command->applyCrew);
        $this->rejectAble->setApplyCrew($applyCrew);
        
        return $this->rejectAble->reject();
    }
}

<?php
namespace BaseData\WorkOrderTask\Adapter\ParentTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Model\NullParentTask;
use BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator;
use BaseData\WorkOrderTask\Adapter\ParentTask\Query\ParentTaskRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class ParentTaskDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(ParentTaskDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'editAction',
                                'fetchOneAction',
                                'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IWorkOrderTaskAdapter
     */
    public function testImplementsIWorkOrderTaskAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 WorkOrderTaskDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockParentTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Translator\ParentTaskDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 WorkOrderTaskRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockParentTaskDbAdapter();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\ParentTask\Query\ParentTaskRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockParentTaskDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedParentTask = new ParentTask();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedParentTask)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedParentTask);
        $this->assertTrue($result);
    }

    //edit
    public function testUpdate()
    {
        //初始化
        $expectedParentTask = new ParentTask();
        $keys = ['keys'];

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('editAction')
                         ->with($expectedParentTask, $keys)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->edit($expectedParentTask, $keys);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedParentTask = new ParentTask();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedParentTask);

        //验证
        $parentTask = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedParentTask, $parentTask);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $parentTaskOne = new ParentTask(1);
        $parentTaskTwo = new ParentTask(2);

        $ids = [1, 2];

        $expectedParentTaskList = [];
        $expectedParentTaskList[$parentTaskOne->getId()] = $parentTaskOne;
        $expectedParentTaskList[$parentTaskTwo->getId()] = $parentTaskTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedParentTaskList);

        //验证
        $parentTaskList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedParentTaskList, $parentTaskList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockParentTaskDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatFilter
    public function testFormatFilterTitle()
    {
        $filter = array(
            'title' => 'title'
        );
        $adapter = new MockParentTaskDbAdapter();

        $expectedCondition = 'title LIKE \'%'.$filter['title'].'%\'';

        //验证
        $condition = $adapter->formatFilter($filter);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockParentTaskDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortEndTimeDesc()
    {
        $sort = array('endTime' => -1);
        $adapter = new MockParentTaskDbAdapter();

        $expectedCondition = ' ORDER BY end_time DESC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }

    //formatSort
    public function testFormatSortEndTimeAsc()
    {
        $sort = array('endTime' => 1);
        $adapter = new MockParentTaskDbAdapter();

        $expectedCondition = ' ORDER BY end_time ASC';

        //验证
        $condition = $adapter->formatSort($sort);
        $this->assertEquals($expectedCondition, $condition);
    }
}

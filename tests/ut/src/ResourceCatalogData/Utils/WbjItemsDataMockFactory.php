<?php
namespace BaseData\ResourceCatalogData\Utils;

use BaseData\ResourceCatalogData\Model\WbjItemsData;

class WbjItemsDataMockFactory
{
    public static function generateWbjItemsData(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : WbjItemsData {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $wbjItemsData = new WbjItemsData($id);
        $wbjItemsData->setId($id);

        self::generateData($wbjItemsData, $faker, $value);

        return $wbjItemsData;
    }

    private static function generateData($wbjItemsData, $faker, $value) : void
    {
        $data = isset($value['data']) ?
            $value['data'] :
            $faker->words(3, false);
        
        $wbjItemsData->setData($data);
    }
}

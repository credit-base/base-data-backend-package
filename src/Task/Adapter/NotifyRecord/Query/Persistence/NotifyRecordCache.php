<?php
namespace BaseData\Task\Adapter\NotifyRecord\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class NotifyRecordCache extends Cache
{
    const KEY = 'schedule_task_notify_record';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\Common\Adapter;

use Marmot\Interfaces\INull;
use PHPUnit\Framework\TestCase;

class FetchAbleRestfulAdapterTraitTest extends TestCase
{
    public function testFetchList()
    {
        $ids = array(1,2,3);

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapter::class)
                      ->setMethods(['fetchListAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('fetchListAction')
             ->with($this->equalTo($ids))
             ->willReturn(array());
             
        $this->assertArraySubset(array(), $trait->fetchList($ids));
    }

    public function testFetchListActionPublic()
    {
        $ids = array(1,2,3);
        $resources = 'resources';
        $list = array($ids, 1);

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObjects'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObjects')
             ->willReturn($list);
             
        $result = $trait->fetchListActionPublic($ids);
        $this->assertEquals($ids, $result);
    }

    public function testFetchListActionPublicFail()
    {
        $ids = array(1,2,3);
        $resources = 'resources';
        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->fetchListActionPublic($ids);
        $this->assertEquals(array(), $result);
    }

    public function testFilter()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapter::class)
                      ->setMethods(['filterAction'])
                      ->getMock();

        $trait->expects($this->any())
             ->method('filterAction')
             ->with($this->equalTo($filter), $this->equalTo($sort), $this->equalTo($offset), $this->equalTo($size))
             ->willReturn(array());
             
        $this->assertArraySubset(array(), $trait->filter($filter, $sort, $offset, $size));
    }

    public function testFilterActionPublic()
    {
        $filter = array();
        $sort = ['-updateTime'];
        $offset = 0;
        $size = 20;
        $resources = 'resources';
        $list = array(array('list'), 1);

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess', 'translateToObjects'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(true);

        $trait->expects($this->exactly(1))
             ->method('translateToObjects')
             ->willReturn($list);
        
        $result = $trait->filterActionPublic($filter, $sort, $offset, $size);

        $this->assertEquals($list, $result);
    }

    public function testFilterActionPublicFail()
    {
        $filter = array();
        $sort = array();
        $offset = 0;
        $size = 20;
        $resources = 'resources';

        $trait = $this->getMockBuilder(MockFetchAbleRestfulAdapter::class)
                      ->setMethods(['getResource', 'get', 'isSuccess'])
                      ->getMock();

        $trait->expects($this->exactly(1))
             ->method('getResource')
             ->willReturn($resources);

        $trait->expects($this->exactly(1))
             ->method('get');

        $trait->expects($this->exactly(1))
             ->method('isSuccess')
             ->willReturn(false);
        
        $result = $trait->filterActionPublic($filter, $sort, $offset, $size);
        $this->assertEquals(array(array(), 0), $result);
    }
}

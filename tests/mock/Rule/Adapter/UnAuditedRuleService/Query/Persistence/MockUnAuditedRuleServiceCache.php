<?php
namespace BaseData\Rule\Adapter\UnAuditedRuleService\Query\Persistence;

class MockUnAuditedRuleServiceCache extends UnAuditedRuleServiceCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

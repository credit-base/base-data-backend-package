<?php
namespace BaseData\WorkOrderTask\WorkOrderTask\Search;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\Model\Template;

use BaseData\WorkOrderTask\SetDataTrait;
use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Translator\WorkOrderTaskDbTranslator;

use BaseData\Template\SetDataTrait as TemplateSetDataTrait;

/**
 * @Feature: 我是委办局领导,当我需要查看特定条件工单任务时,在政务网OA中,我可以搜索给我指派的工单任务,
 *           通过列表的形式呈现 ,以便于我可以快速查看到我需要的工单任务数据
 * @Scenario: 通过指派对象搜索
 */
class AssignObjectTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, TemplateSetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_work_order_task');
    }

    /**
     * @Given: 存在一条工单任务数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_work_order_task' => $this->workOrderTask()
        ]);
    }

    /**
     * @When: 当我查看工单任务数据列表时
     */
    public function fetchWorkOrderTaskList()
    {
        $repository = new WorkOrderTaskRepository();
 
        $filter['assignObject'] = 1;

        list($workOrderTaskList, $count) = $repository->filter($filter);
        
        unset($count);

        return $workOrderTaskList;
    }

    /**
     * @Then: 我可以看到指派对象为"1"的所有工单任务数据
     */
    public function testViewWorkOrderTaskList()
    {
        $setWorkOrderTaskList = $this->workOrderTask();

        foreach ($setWorkOrderTaskList as $key => $setWorkOrderTask) {
            $setWorkOrderTaskList[$key]['work_order_task_id'] = $key +1;
            $setWorkOrderTaskList[$key]['feedback_records'] = json_decode($setWorkOrderTask['feedback_records'], true);
        }

        $workOrderTaskList = $this->fetchWorkOrderTaskList();
        $translator = new WorkOrderTaskDbTranslator();
        foreach ($workOrderTaskList as $workOrderTask) {
            $workOrderTaskArray[] = $translator->objectToArray($workOrderTask);
        }

        $this->assertEquals($workOrderTaskArray, $setWorkOrderTaskList);

        foreach ($workOrderTaskArray as $workOrderTask) {
            $this->assertEquals(1, $workOrderTask['assign_object_id']);
        }
    }
}

<?php
namespace BaseData\Template\View;

use PHPUnit\Framework\TestCase;

use BaseData\Template\Model\QzjTemplate;

class QzjTemplateViewTest extends TestCase
{
    public function testExtendsCommonView()
    {
        $qzjTemplate = new QzjTemplateView(new QzjTemplate());
        $this->assertInstanceof('BaseData\Common\View\CommonView', $qzjTemplate);
    }
}

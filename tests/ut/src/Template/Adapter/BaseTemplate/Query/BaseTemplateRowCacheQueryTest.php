<?php
namespace BaseData\Template\Adapter\BaseTemplate\Query;

use PHPUnit\Framework\TestCase;

class BaseTemplateRowCacheQueryTest extends TestCase
{
    private $rowCacheQuery;

    public function setUp()
    {
        $this->rowCacheQuery = new MockBaseTemplateRowCacheQuery();
    }

    public function tearDown()
    {
        unset($this->rowCacheQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowCacheQuery', $this->rowCacheQuery);
    }

    /**
     * 测试是否cache层赋值正确
     */
    public function testCorrectCacheLayer()
    {
        $this->assertInstanceof(
            'BaseData\Template\Adapter\BaseTemplate\Query\Persistence\BaseTemplateCache',
            $this->rowCacheQuery->getCacheLayer()
        );
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'BaseData\Template\Adapter\BaseTemplate\Query\Persistence\BaseTemplateDb',
            $this->rowCacheQuery->getDbLayer()
        );
    }
}

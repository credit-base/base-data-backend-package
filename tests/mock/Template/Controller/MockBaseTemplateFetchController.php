<?php
namespace BaseData\Template\Controller;

use Marmot\Interfaces\IView;
use BaseData\Template\Adapter\BaseTemplate\IBaseTemplateAdapter;

class MockBaseTemplateFetchController extends BaseTemplateFetchController
{
    public function getRepository() : IBaseTemplateAdapter
    {
        return parent::getRepository();
    }

    public function generateView($data) : IView
    {
        return parent::generateView($data);
    }

    public function getResourceName() : string
    {
        return parent::getResourceName();
    }
}

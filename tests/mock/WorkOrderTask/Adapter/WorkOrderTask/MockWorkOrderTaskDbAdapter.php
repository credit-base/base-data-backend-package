<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask;

use Marmot\Interfaces\INull;

use BaseData\WorkOrderTask\Model\WorkOrderTask;
use BaseData\WorkOrderTask\Translator\WorkOrderTaskDbTranslator;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\WorkOrderTaskRowCacheQuery;

use BaseData\Template\Repository\GbTemplateRepository;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\WorkOrderTask\Repository\ParentTaskRepository;

class MockWorkOrderTaskDbAdapter extends WorkOrderTaskDbAdapter
{
    public function getDbTranslator() : WorkOrderTaskDbTranslator
    {
        return parent::getDbTranslator();
    }

    public function getRowQuery() : WorkOrderTaskRowCacheQuery
    {
        return parent::getRowQuery();
    }

    public function getNullObject() : INull
    {
        return parent::getNullObject();
    }

    public function formatFilter(array $filter) : string
    {
        return parent::formatFilter($filter);
    }

    public function formatSort(array $sort) : string
    {
        return parent::formatSort($sort);
    }

    public function getGbTemplateRepository() : GbTemplateRepository
    {
        return parent::getGbTemplateRepository();
    }

    public function getBjTemplateRepository() : BjTemplateRepository
    {
        return parent::getBjTemplateRepository();
    }

    public function getParentTaskRepository() : ParentTaskRepository
    {
        return parent::getParentTaskRepository();
    }

    public function fetchGbTemplateByObject(WorkOrderTask $workOrderTask)
    {
        return parent::fetchGbTemplateByObject($workOrderTask);
    }

    public function fetchBjTemplateByObject(WorkOrderTask $workOrderTask)
    {
        return parent::fetchBjTemplateByObject($workOrderTask);
    }

    public function fetchParentTaskByObject(WorkOrderTask $workOrderTask)
    {
        return parent::fetchParentTaskByObject($workOrderTask);
    }
}

<?php
namespace BaseData\Template\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;

class WbjTemplateTest extends TestCase
{
    private $wbjTemplate;

    public function setUp()
    {
        $this->wbjTemplate = new WbjTemplate();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->wbjTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\Template',
            $this->wbjTemplate
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IOperate',
            $this->wbjTemplate
        );
    }

    public function testGetRepository()
    {
        $wbjTemplate = new MockWbjTemplate();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter',
            $wbjTemplate->getRepository()
        );
    }

    public function testGetCategory()
    {
        $result = $this->wbjTemplate->getCategory();
        $this->assertEquals($result, Template::CATEGORY['WBJ']);
    }
    //add()
    /**
     * 测试添加成功
     * 1. 期望返回true
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IWbjTemplateAdapter 调用 add 并且传参 $wbjTemplate 对象, 返回 true
     */
    public function testAddSuccess()
    {
        //初始化
        $wbjTemplate = $this->getMockBuilder(WbjTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IWbjTemplateAdapter
        $repository = $this->prophesize(IWbjTemplateAdapter::class);
        $repository->add(Argument::exact($wbjTemplate))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $wbjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $wbjTemplate->add();
        $this->assertTrue($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望返回false
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IWbjTemplateAdapter 调用 add 并且传参 $wbjTemplate 对象, 返回 false
     */
    public function testAddFail()
    {
        //初始化
        $wbjTemplate = $this->getMockBuilder(WbjTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IWbjTemplateAdapter
        $repository = $this->prophesize(IWbjTemplateAdapter::class);
        $repository->add(Argument::exact($wbjTemplate))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(false);

        //绑定
        $wbjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $wbjTemplate->add();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IWbjTemplateAdapter 调用 edit 并且传参 $wbjTemplate 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $wbjTemplate = $this->getMockBuilder(WbjTemplate::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        //预言修改updateTime
        $wbjTemplate->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IWbjTemplateAdapter
        $repository = $this->prophesize(IWbjTemplateAdapter::class);
        $repository->edit(
            Argument::exact($wbjTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items',
                    'sourceUnit',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $wbjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $wbjTemplate->edit();
        $this->assertTrue($result);
    }

    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 updateTime
     * 2. 期望 getRepository 被调用一次
     * 3. 期望 IWbjTemplateAdapter 调用 edit 并且传参 $wbjTemplate 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $wbjTemplate = $this->getMockBuilder(WbjTemplate::class)
                           ->setMethods(['setUpdateTime', 'getRepository'])
                           ->getMock();

        //预言修改updateTime
        $wbjTemplate->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IWbjTemplateAdapter
        $repository = $this->prophesize(IWbjTemplateAdapter::class);
        $repository->edit(
            Argument::exact($wbjTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items',
                    'sourceUnit',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $wbjTemplate->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $wbjTemplate->edit();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
    }
}

<?php
namespace BaseData\Rule\Adapter;

use Marmot\Framework\Classes\Repository;

use BaseData\Rule\Model\Rule;
use BaseData\Rule\Repository\RepositoryFactory;

use BaseData\Template\Repository\GbTemplateRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class MockRuleServiceAdapterTrait
{
    use RuleServiceAdapterTrait;

    public function publicGetRepositoryFactory() : RepositoryFactory
    {
        return $this->getRepositoryFactory();
    }

    public function publicGetRepository(int $category) : Repository
    {
        return $this->getRepository($category);
    }
    
    public function publicGetGbTemplateRepository() : GbTemplateRepository
    {
        return $this->getGbTemplateRepository();
    }
    
    public function publicFetchTransformationTemplate($ruleService)
    {
        return $this->fetchTransformationTemplate($ruleService);
    }

    public function publicFetchTransformationTemplateByObject($ruleService)
    {
        return $this->fetchTransformationTemplateByObject($ruleService);
    }

    public function publicFetchTransformationTemplateByList(array $ruleServiceList)
    {
        return $this->fetchTransformationTemplateByList($ruleServiceList);
    }

    public function publicFetchSourceTemplate($ruleService)
    {
        return $this->fetchSourceTemplate($ruleService);
    }

    public function publicFetchSourceTemplateByObject($ruleService)
    {
        return $this->fetchSourceTemplateByObject($ruleService);
    }

    public function publicFetchSourceTemplateByList(array $ruleServiceList)
    {
        return $this->fetchSourceTemplateByList($ruleServiceList);
    }

    public function publicFetchRules($ruleService)
    {
        return $this->fetchRules($ruleService);
    }

    public function publicFetchCondition(Rule $rule)
    {
        return $this->fetchCondition($rule);
    }

    public function publicFetchRuleTemplate(array $rules)
    {
        return $this->fetchRuleTemplate($rules);
    }

    public function publicFetchTemplateName(array $templateList, array $pattern)
    {
        return $this->fetchTemplateName($templateList, $pattern);
    }

    public function publicFetchItemName($templateList, $pattern)
    {
        return $this->fetchItemName($templateList, $pattern);
    }
}

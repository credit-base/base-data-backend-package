<?php
namespace BaseData\Enterprise\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Enterprise\SetDataApiTrait;
use BaseData\Enterprise\EnterpriseUtils;
use BaseData\Enterprise\Model\Enterprise;

/**
 * 测试企业接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, EnterpriseUtils;

    private $enterprise;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->enterprise);
        $this->clear('pcore_enterprise');
    }

    /**
     * 存在一条企业数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_enterprise' => $this->enterprise(),
            ]
        );
    }

    public function testViewEnterprise()
    {
        $setEnterprise = $this->enterprise()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request('GET', 'enterprises/1', ['headers'=> Core::$container->get('client.headers')]);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->enterpriseCompareData($setEnterprise, $contents['data']);
    }
}

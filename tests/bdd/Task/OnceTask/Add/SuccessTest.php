<?php
namespace BaseData\Task\Model;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Task\Model\MockOnceTask;
use BaseData\Task\Repository\TaskRepository;
use BaseData\Task\Translator\TaskDbTranslator;

use tests\DbTrait;

/**
 * @Feature: 作为开发人员, 当我需要运行调度任务时, 我可以新增一个一次性任务，以便于调度器异步执行
 * @Scenario: 新增一次性任务
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait;

    private $data;

    private $expectedTask;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_schedule_task');
    }

    protected function getDataSet()
    {
        return new ArrayDataSet(
            []
        );
    }

    /**
     * @Given: 准备添加一次性任务
     */
    public function prepareData()
    {
        $this->data = [
            'data' => ['data']
        ];
    }

    /**
     * @When: 当调用添加时, 期望返回添加id
     */
    public function add()
    {
        $this->expectedTask = new MockOnceTask();
        $this->expectedTask->setData($this->data['data']);
        return $this->expectedTask->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $this->prepareData();
        $id = $this->add();

        $this->assertTrue($id);

        $repository = new TaskRepository();
        $translator = new TaskDbTranslator();

        $actualTask = $repository->fetchOne($id);


        $expectedResult = $translator->objectToArray($this->expectedTask);
        $actualResult = $translator->objectToArray($actualTask);
        $this->assertEquals($expectedResult, $actualResult);
    }
}

<?php
namespace BaseData\Template\WbjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\Model\Template;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\WbjTemplate;

/**
 * 测试委办局资源目录接口新增
 */
class AddTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
    }

    /**
     * 不存在委办局资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            []
        );
    }

    public function testViewWbjTemplate()
    {
        $setWbjTemplate = $this->wbjTemplate()[0];
        $items = $this->items();

        $data = array(
            "data" => array(
                "type" => "wbjTemplates",
                "attributes" => array(
                    "name" => '委办局企业基本信息',
                    "identify" => 'WBJQYJBXX',
                    "subjectCategory" => array(Template::SUBJECT_CATEGORY['FRJFFRZZ']),
                    "dimension" => Template::DIMENSION['SHGK'],
                    "exchangeFrequency" => 1,
                    "infoClassify" => Template::INFO_CLASSIFY['QT'],
                    "infoCategory" => Template::INFO_CATEGORY['QTXX'],
                    "description" => "委办局企业基本信息",
                    "items" => $items
                ),
                "relationships" => array(
                    "sourceUnit" => array(
                        "data" => array(
                            array("type" => "userGroups", "id" => 1)
                        )
                    )
                )
            )
        );

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'POST',
            'templates',
            ['headers' => Core::$container->get('client.headers'), 'json' => $data]
        );

        $status = $response->getStatusCode();
        $this->assertEquals(201, $status);

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->wbjTemplateCompareData($setWbjTemplate, $contents['data']);
    }
}

<?php
namespace BaseData\Template\QzjTemplate\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Repository\QzjTemplateRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录时,在目录管理子系统下的前置机资源目录管理中,可以查看到我所属委办局下的所有资源目录,
 *           通过列表和详情的形式呈现,以便于我可以更好的管理资源目录
 * @Scenario: 查看资源目录列表-异常数据为空
 */
class FailEmptyTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @Given: 不存在资源目录
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([]);
    }

    /**
     * @When: 当我查看资源目录列表时
     */
    public function fetchQzjTemplateList()
    {
        $repository = new QzjTemplateRepository();

        list($qzjTemplateList, $count) = $repository->filter([]);
        
        unset($count);

        return $qzjTemplateList;
    }

    /**
     * @Then: 获取不到数据
     */
    public function testViewQzjTemplate()
    {
        $qzjTemplateList = $this->fetchQzjTemplateList();

        $this->assertEmpty($qzjTemplateList);
    }
}

<?php
namespace BaseData\Template\Adapter\QzjTemplate;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Template\Model\QzjTemplate;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class QzjTemplateMockAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new QzjTemplateMockAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testInsert()
    {
        $this->assertTrue($this->adapter->add(new QzjTemplate()));
    }

    public function testUpdate()
    {
        $this->assertTrue($this->adapter->edit(new QzjTemplate(), ['keys']));
    }

    public function testFetchOne()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\QzjTemplate',
            $this->adapter->fetchOne(1)
        );
    }

    public function testFetchList()
    {
        $list = $this->adapter->fetchList([1, 2, 3]);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Template\Model\QzjTemplate',
                $each
            );
        }
    }

    public function testFilter()
    {
        list($list, $count) = $this->adapter->filter(['filter'], ['sort']);

        foreach ($list as $each) {
            $this->assertInstanceOf(
                'BaseData\Template\Model\QzjTemplate',
                $each
            );
        }

        $this->assertEquals(sizeof($list), $count);
    }
}

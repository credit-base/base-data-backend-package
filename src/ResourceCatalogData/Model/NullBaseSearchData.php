<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Interfaces\INull;

use BaseData\Common\Model\NullEnableAbleTrait;

class NullBaseSearchData extends BaseSearchData implements INull
{
    use NullSearchDataTrait, NullEnableAbleTrait;

    protected static $instance;
    
    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}

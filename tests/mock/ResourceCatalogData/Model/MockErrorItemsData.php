<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class MockErrorItemsData extends ErrorItemsData
{
    public function getRepository() : IErrorDataAdapter
    {
        return parent::getRepository();
    }
}

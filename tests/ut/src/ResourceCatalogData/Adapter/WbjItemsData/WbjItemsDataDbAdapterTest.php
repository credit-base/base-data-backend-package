<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Model\WbjItemsData;
use BaseData\ResourceCatalogData\Model\NullWbjItemsData;
use BaseData\ResourceCatalogData\Translator\WbjItemsDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\WbjItemsDataRowCacheQuery;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @author chloroplast
 */
class WbjItemsDataDbAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = $this->getMockBuilder(WbjItemsDataDbAdapter::class)
                           ->setMethods(
                               [
                                'addAction',
                                'fetchOneAction',
                                'fetchListAction'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    /**
     * 测试是否 实现 IWbjItemsDataAdapter
     */
    public function testImplementsIWbjItemsDataAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjItemsData\IWbjItemsDataAdapter',
            $this->adapter
        );
    }

    /**
     * 测试是否初始化 WbjItemsDataDbTranslator
     */
    public function testGetDbTranslator()
    {
        $adapter = new MockWbjItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Translator\WbjItemsDataDbTranslator',
            $adapter->getDbTranslator()
        );
    }

    /**
     * 测试是否初始化 WbjItemsDataRowCacheQuery
     */
    public function testGetRowCacheQuery()
    {
        $adapter = new MockWbjItemsDataDbAdapter();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\WbjItemsData\Query\WbjItemsDataRowCacheQuery',
            $adapter->getRowQuery()
        );
    }

    public function testGetNullObject()
    {
        $adapter = new MockWbjItemsDataDbAdapter();
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $adapter->getNullObject()
        );
    }

    //add
    public function testInsert()
    {
        //初始化
        $expectedWbjItemsData = new WbjItemsData();

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('addAction')
                         ->with($expectedWbjItemsData)
                         ->willReturn(true);

        //验证
        $result = $this->adapter->add($expectedWbjItemsData);
        $this->assertTrue($result);
    }

    //fetchOne
    public function testFetchOne()
    {
        //初始化
        $expectedWbjItemsData = new WbjItemsData();
        $id = 1;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchOneAction')
                         ->with($id)
                         ->willReturn($expectedWbjItemsData);

        //验证
        $wbjItemsData = $this->adapter->fetchOne($id);
        $this->assertEquals($expectedWbjItemsData, $wbjItemsData);
    }

    //fetchList
    public function testFetchList()
    {
        //初始化
        $wbjItemsDataOne = new WbjItemsData(1);
        $wbjItemsDataTwo = new WbjItemsData(2);

        $ids = [1, 2];

        $expectedWbjItemsDataList = [];
        $expectedWbjItemsDataList[$wbjItemsDataOne->getId()] = $wbjItemsDataOne;
        $expectedWbjItemsDataList[$wbjItemsDataTwo->getId()] = $wbjItemsDataTwo;

        //绑定
        $this->adapter->expects($this->exactly(1))
                         ->method('fetchListAction')
                         ->with($ids)
                         ->willReturn($expectedWbjItemsDataList);

        //验证
        $wbjItemsDataList = $this->adapter->fetchList($ids);
        $this->assertEquals($expectedWbjItemsDataList, $wbjItemsDataList);
    }

    public function testFormatFilter()
    {
        $adapter = new MockWbjItemsDataDbAdapter();

        $expectedCondition = ' 1 ';
        $condition = $adapter->formatFilter([]);
        $this->assertEquals($expectedCondition, $condition);
    }

    public function testFormatSort()
    {
        $adapter = new MockWbjItemsDataDbAdapter();

        $expectedCondition = '';
        $condition = $adapter->formatSort([]);
        $this->assertEquals($expectedCondition, $condition);
    }
}

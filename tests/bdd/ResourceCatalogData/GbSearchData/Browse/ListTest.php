<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\GbSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\GbSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的国标数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_gb_template');
        $this->clear('pcore_gb_items_data');
        $this->clear('pcore_gb_search_data');
    }

    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data'],
            'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data']
        ]);
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchGbSearchDataList()
    {
        $repository = new GbSearchDataRepository();

        list($gbSearchDataList, $count) = $repository->filter([]);
        
        unset($count);

        return $gbSearchDataList;
    }

    /**
     * @Then: 我可以看到资源目录数据的全部信息
     */
    public function testViewGbSearchDataList()
    {
        $setGbSearchDataList = $this->gbSearchData()['pcore_gb_search_data'];

        foreach ($setGbSearchDataList as $key => $setGbSearchData) {
            unset($setGbSearchData);
            $setGbSearchDataList[$key]['gb_search_data_id'] = $key +1;
        }

        $gbSearchDataList = $this->fetchGbSearchDataList();

        $translator = new GbSearchDataDbTranslator();
        foreach ($gbSearchDataList as $gbSearchData) {
            $gbSearchDataArray[] = $translator->objectToArray($gbSearchData);
        }

        $this->assertEquals($gbSearchDataArray, $setGbSearchDataList);
    }
}

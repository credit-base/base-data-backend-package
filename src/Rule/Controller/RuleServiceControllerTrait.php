<?php
namespace BaseData\Rule\Controller;

use Marmot\Framework\Classes\CommandBus;

use BaseData\Common\WidgetRule\CommonWidgetRule;

use BaseData\Rule\WidgetRule\RuleServiceWidgetRule;
use BaseData\Rule\Repository\RuleServiceRepository;
use BaseData\Rule\Repository\UnAuditedRuleServiceRepository;
use BaseData\Rule\CommandHandler\RuleService\RuleServiceCommandHandlerFactory;

trait RuleServiceControllerTrait
{
    protected function getRuleServiceWidgetRule() : RuleServiceWidgetRule
    {
        return new RuleServiceWidgetRule();
    }

    protected function getCommonWidgetRule() : CommonWidgetRule
    {
        return new CommonWidgetRule();
    }

    protected function getRepository() : RuleServiceRepository
    {
        return new RuleServiceRepository();
    }

    protected function getUnAuditedRuleServiceRepository() : UnAuditedRuleServiceRepository
    {
        return new UnAuditedRuleServiceRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new RuleServiceCommandHandlerFactory());
    }

    protected function validateRejectScenario(
        $rejectReason,
        $applyCrew
    ) {
        return $this->getCommonWidgetRule()->description($rejectReason, 'rejectReason')
            && $this->getCommonWidgetRule()->formatNumeric($applyCrew, 'applyCrewId');
    }
    
    protected function validateApproveScenario(
        $applyCrew
    ) {
        return $this->getCommonWidgetRule()->formatNumeric($applyCrew, 'applyCrewId');
    }

    protected function validateStatusScenario(
        $crew
    ) {
        return $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }

    protected function validateCommonScenario(
        $rules,
        $crew
    ) {
        return $this->getRuleServiceWidgetRule()->rules($rules)
            && $this->getCommonWidgetRule()->formatNumeric($crew, 'crewId');
    }

    protected function validateAddScenario(
        $transformationTemplate,
        $sourceTemplate,
        $transformationCategory,
        $sourceCategory
    ) {
        return $this->getCommonWidgetRule()->formatNumeric($transformationTemplate, 'transformationTemplateId')
            && $this->getCommonWidgetRule()->formatNumeric($sourceTemplate, 'sourceTemplateId')
            && $this->getRuleServiceWidgetRule()->category($transformationCategory, 'transformationCategory')
            && $this->getRuleServiceWidgetRule()->category($sourceCategory, 'sourceCategory');
    }
}

<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Model\BjSearchData;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要封存资源目录数据时,在资源目录数据子系统下的本级数据管理,封存对应的资源目录数据
 *           根据我核实需要封存的数据封存,以便于我实现平台资源目录数据的呈现
 * @Scenario: 封存资源目录数据
 */
class DeleteTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * @Given: 存在需要封存的资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
            'pcore_bj_search_data' => $this->bjSearchData(BjSearchData::STATUS['CONFIRM'])['pcore_bj_search_data']
        ]);
    }

    /**
     * @When: 获取需要封存的资源目录数据
     */
    public function fetchBjSearchData($id)
    {
        $repository = new BjSearchDataRepository();

        $bjSearchData = $repository->fetchOne($id);

        return $bjSearchData;
    }

    /**
     * @And: 当我调用封存函数,期待返回true
     */
    public function delete()
    {
        $bjSearchData = $this->fetchBjSearchData(1);
        
        return $bjSearchData->delete();
    }

    /**
     * @Then: 数据已经被封存
     */
    public function testValidate()
    {
        $result = $this->delete();

        $this->assertTrue($result);

        $bjSearchData = $this->fetchBjSearchData(1);

        $this->assertEquals(BjSearchData::STATUS['DELETED'], $bjSearchData->getStatus());
        $this->assertEquals(Core::$container->get('time'), $bjSearchData->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $bjSearchData->getStatusTime());
    }
}

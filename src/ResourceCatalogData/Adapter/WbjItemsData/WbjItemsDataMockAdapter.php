<?php
namespace BaseData\ResourceCatalogData\Adapter\WbjItemsData;

use BaseData\ResourceCatalogData\Model\WbjItemsData;
use BaseData\ResourceCatalogData\Utils\WbjItemsDataMockFactory;

class WbjItemsDataMockAdapter implements IWbjItemsDataAdapter
{
    public function fetchOne($id) : WbjItemsData
    {
        return WbjItemsDataMockFactory::generateWbjItemsData($id);
    }

    public function fetchList(array $ids) : array
    {
        $wbjItemsDataList = array();

        foreach ($ids as $id) {
            $wbjItemsDataList[$id] = WbjItemsDataMockFactory::generateWbjItemsData($id);
        }

        return $wbjItemsDataList;
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {

        unset($filter);
        unset($sort);
        unset($offset);
        unset($size);

        $ids = [1, 2, 3, 4];
        $count = 4;

        return array($this->fetchList($ids), $count);
    }

    public function add(WbjItemsData $wbjItemsData) : bool
    {
        unset($wbjItemsData);
        return true;
    }
}

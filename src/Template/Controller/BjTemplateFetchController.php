<?php
namespace BaseData\Template\Controller;

use Marmot\Interfaces\IView;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Common\Controller\IFetchController;
use Marmot\Framework\Common\Controller\FetchControllerTrait;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseData\Template\View\BjTemplateView;
use BaseData\Template\Repository\BjTemplateRepository;

class BjTemplateFetchController extends Controller implements IFetchController
{
    use FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : IBjTemplateAdapter
    {
        return $this->repository;
    }

    protected function generateView($data) : IView
    {
        return new BjTemplateView($data);
    }

    protected function getResourceName() : string
    {
        //资源的复数
        return 'bjTemplates';
    }
}

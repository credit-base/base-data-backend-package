<?php
namespace BaseData\Template\Model;

use Marmot\Core;

use BaseData\Common\Model\IOperate;
use BaseData\Common\Model\OperateTrait;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseData\Template\Repository\GbTemplateRepository;

class GbTemplate extends Template implements IOperate
{
    use OperateTrait;
    
    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->repository = new GbTemplateRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    public function getCategory() : int
    {
        return Template::CATEGORY['GB'];
    }

    protected function getRepository() : IGbTemplateAdapter
    {
        return $this->repository;
    }
    
    protected function addAction() : bool
    {
        if (!$this->getRepository()->add($this)) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'identify'));
            return false;
        }
        return true;
    }

    /**
     * @todo 编辑时，本次未考虑目录版本化
     */
    protected function editAction() : bool
    {
        $this->setUpdateTime(Core::$container->get('time'));

        if (!$this->getRepository()->edit(
            $this,
            array(
                'name',
                'identify',
                'subjectCategory',
                'dimension',
                'exchangeFrequency',
                'infoClassify',
                'infoCategory',
                'description',
                'items',
                'updateTime'
            )
        )) {
            Core::setLastError(RESOURCE_ALREADY_EXIST, array('pointer'=>'identify'));
            return false;
        }
        return true;
    }
}

<?php
namespace BaseData\ResourceCatalogData\Utils;

trait YsItemsDataUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $ysItemsData
    ) {
        $this->assertEquals($expectedArray['ys_items_data_id'], $ysItemsData->getId());

        $data = array();
        if (is_string($expectedArray['data'])) {
            $data = unserialize(gzuncompress(base64_decode($expectedArray['data'], true)));
        }
        if (is_array($expectedArray['data'])) {
            $data = $expectedArray['data'];
        }

        $this->assertEquals($data, $ysItemsData->getData());
    }
}

<?php
namespace BaseData\Task\Script;

use BaseData\ResourceCatalogData\Service\SyncTaskService;

class MockSyncTask extends SyncTask
{
    public function getSyncTaskService(string $fileName) : SyncTaskService
    {
        return parent::getSyncTaskService($fileName);
    }

    public function getSyncSubtask() : SyncSubtask
    {
        return parent::getSyncSubtask();
    }

    public function getSyncErrorDataDownload() : SyncErrorDataDownload
    {
        return parent::getSyncErrorDataDownload();
    }
}

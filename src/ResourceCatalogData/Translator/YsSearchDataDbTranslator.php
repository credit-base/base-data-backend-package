<?php
namespace BaseData\ResourceCatalogData\Translator;

use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\Model\NullYsSearchData;

class YsSearchDataDbTranslator extends SearchDataDbTranslator
{
    public function arrayToObject(array $expression, $ysSearchData = null)
    {
        if (!isset($expression['ys_search_data_id'])) {
            return NullYsSearchData::getInstance();
        }

        if ($ysSearchData == null) {
            $ysSearchData = new YsSearchData();
        }

        $ysSearchData->setId($expression['ys_search_data_id']);

        if (isset($expression['wbj_template_id'])) {
            $ysSearchData->getTemplate()->setId($expression['wbj_template_id']);
        }

        if (isset($expression['ys_items_data_id'])) {
            $ysSearchData->getItemsData()->setId($expression['ys_items_data_id']);
        }

        $ysSearchData = parent::arrayToObject($expression, $ysSearchData);
        
        return $ysSearchData;
    }

    public function objectToArray($ysSearchData, array $keys = array())
    {
        $searchDataExpression = parent::objectToArray($ysSearchData, $keys);

        if (!$ysSearchData instanceof YsSearchData) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'template',
                'itemsData'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['ys_search_data_id'] = $ysSearchData->getId();
        }
        if (in_array('template', $keys)) {
            $expression['wbj_template_id'] = $ysSearchData->getTemplate()->getId();
        }
        if (in_array('itemsData', $keys)) {
            $expression['ys_items_data_id'] = $ysSearchData->getItemsData()->getId();
        }

        $expression = array_merge($expression, $searchDataExpression);

        return $expression;
    }
}

<?php
namespace BaseData\Crew\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use BaseData\UserGroup\Model\UserGroup;

class Crew implements IObject
{
    use Object;
    
    const STATUS_NORMAL = 0; //正常

    private $id;

    private $cellphone;

    private $realName;

    private $userGroup;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->cellphone = '';
        $this->realName = '';
        $this->userGroup = new UserGroup();
        $this->updateTime = 0;
        $this->createTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->cellphone);
        unset($this->realName);
        unset($this->userGroup);
        unset($this->updateTime);
        unset($this->createTime);
        unset($this->status);
        unset($this->statusTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = is_numeric($cellphone) ? $cellphone : '';
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setRealName(string $realName) : void
    {
        $this->realName = $realName;
    }

    public function getRealName() : string
    {
        return $this->realName;
    }

    public function setUserGroup(UserGroup $userGroup) : void
    {
        $this->userGroup = $userGroup;
    }

    public function getUserGroup() : UserGroup
    {
        return $this->userGroup;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}

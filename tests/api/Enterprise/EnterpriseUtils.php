<?php
namespace BaseData\Enterprise;

trait EnterpriseUtils
{
    private function enterpriseCompareData(
        array $setDataArray,
        array $responseDataArray
    ) {
        $this->assertEquals($setDataArray['enterprise_id'], $responseDataArray['id']);
        $this->assertEquals('enterprises', $responseDataArray['type']);

        $attributes = $responseDataArray['attributes'];

        $this->assertEquals($setDataArray['name'], $attributes['name']);
        $this->assertEquals($setDataArray['unified_social_credit_code'], $attributes['unifiedSocialCreditCode']);
        $this->assertEquals($setDataArray['establishment_date'], $attributes['establishmentDate']);
        $this->assertEquals($setDataArray['approval_date'], $attributes['approvalDate']);
        $this->assertEquals($setDataArray['address'], $attributes['address']);
        $this->assertEquals($setDataArray['registration_capital'], $attributes['registrationCapital']);
        $this->assertEquals($setDataArray['business_term_start'], $attributes['businessTermStart']);
        $this->assertEquals($setDataArray['business_term_to'], $attributes['businessTermTo']);
        $this->assertEquals($setDataArray['business_scope'], $attributes['businessScope']);
        $this->assertEquals($setDataArray['registration_authority'], $attributes['registrationAuthority']);
        $this->assertEquals($setDataArray['principal'], $attributes['principal']);
        $this->assertEquals($setDataArray['principal_card_id'], $attributes['principalCardId']);
        $this->assertEquals($setDataArray['registration_status'], $attributes['registrationStatus']);
        $this->assertEquals($setDataArray['enterprise_type_code'], $attributes['enterpriseTypeCode']);
        $this->assertEquals($setDataArray['enterprise_type'], $attributes['enterpriseType']);
        $this->assertEquals(
            unserialize(gzuncompress(base64_decode($setDataArray['data'], true))),
            $attributes['data']
        );
        $this->assertEquals($setDataArray['industry_category'], $attributes['industryCategory']);
        $this->assertEquals($setDataArray['industry_code'], $attributes['industryCode']);
        $this->assertEquals($setDataArray['administrative_area'], $attributes['administrativeArea']);
        $this->assertEquals($setDataArray['status'], $attributes['status']);
        $this->assertEquals($setDataArray['create_time'], $attributes['createTime']);
        $this->assertEquals($setDataArray['update_time'], $attributes['updateTime']);
        $this->assertEquals($setDataArray['status_time'], $attributes['statusTime']);
    }
}

<?php
namespace BaseData\Statistical\Adapter\Statistical;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\MyPdo;

use BaseData\Statistical\Model\Statistical;
use BaseData\Statistical\Model\NullStatistical;
use BaseData\Statistical\Translator\EnterpriseRelationInformationCountTranslator;

class EnterpriseRelationInformationCountAdapterTest extends TestCase
{
    private $adapter;

    public function setUp()
    {
        $this->adapter = new MockEnterpriseRelationInformationCountAdapter();
    }

    public function tearDown()
    {
        unset($this->adapter);
    }

    public function testImplementsIStatisticalAdapter()
    {
        $this->assertInstanceOf(
            'BaseData\Statistical\Adapter\Statistical\IStatisticalAdapter',
            $this->adapter
        );
    }

    public function testGetEnterpriseRelationInformationCountTranslator()
    {
        $this->assertInstanceOf(
            'BaseData\Statistical\Translator\EnterpriseRelationInformationCountTranslator',
            $this->adapter->getEnterpriseRelationInformationCountTranslator()
        );
    }

    public function testGetDbDriver()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\MyPdo',
            $this->adapter->getDbDriver()
        );
    }

    private function initialAnalyse($info, $sql)
    {
        $this->adapter = $this->getMockBuilder(MockEnterpriseRelationInformationCountAdapter::class)
                           ->setMethods([
                                'getEnterpriseRelationInformationCountTranslator',
                                'getDbDriver'
                            ])->getMock();


        $dbDriver = $this->prophesize(MyPdo::class);
        $dbDriver->query($sql)->shouldBeCalledTimes(1)->willReturn($info);
        $this->adapter->expects($this->any())->method('getDbDriver')->willReturn($dbDriver->reveal());
    }

    public function testAnalyseFailure()
    {
        $info = array();
        $filter = array();
        $sql = "call statics_enterprise_relation_information_count('','1,2,3');";
        $this->initialAnalyse($info, $sql);

        $result = $this->adapter->analyse($filter);

        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertInstanceOf('BaseData\Statistical\Model\NullStatistical', $result);
    }

    public function testAnalyseSuccess()
    {
        $info = array(array('info'));
        $filter['unifiedSocialCreditCode'] = 'unifiedSocialCreditCode';
        $filter['dimension'] = 1;
        $sql = "call statics_enterprise_relation_information_count('unifiedSocialCreditCode','1');";
        $statistical = new Statistical();
        $this->initialAnalyse($info, $sql);
        
        $translator = $this->prophesize(EnterpriseRelationInformationCountTranslator::class);
        $translator->arrayToObject(Argument::exact($info[0]))->shouldBeCalledTimes(1)->willReturn($statistical);
        $this->adapter->expects($this->any())
                   ->method('getEnterpriseRelationInformationCountTranslator')
                   ->willReturn($translator->reveal());

        $result = $this->adapter->analyse($filter);
        $this->assertEquals($statistical, $result);
    }
}

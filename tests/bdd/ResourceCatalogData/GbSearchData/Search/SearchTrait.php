<?php
namespace BaseData\ResourceCatalogData\GbSearchData\Search;

use PHPUnit\DbUnit\DataSet\ArrayDataSet;

trait SearchTrait
{
    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate(),
            'pcore_gb_items_data' => $this->gbSearchData()['pcore_gb_items_data'],
            'pcore_gb_search_data' => $this->gbSearchData()['pcore_gb_search_data']
        ]);
    }
}

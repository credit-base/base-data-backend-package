<?php
namespace BaseData\Enterprise\Translator;

use Marmot\Interfaces\ITranslator;

use BaseData\Enterprise\Model\Enterprise;
use BaseData\Enterprise\Model\NullEnterprise;

/**
 * @todo
 * @SuppressWarnings(PHPMD)
 */
class EnterpriseDbTranslator implements ITranslator
{
    public function arrayToObject(array $expression, $enterprise = null) : Enterprise
    {
        if (!isset($expression['enterprise_id'])) {
            return NullEnterprise::getInstance();
        }

        if ($enterprise == null) {
            $enterprise = new Enterprise($expression['enterprise_id']);
        }

        if (isset($expression['name'])) {
            $enterprise->setName($expression['name']);
        }
        if (isset($expression['unified_social_credit_code'])) {
            $enterprise->setUnifiedSocialCreditCode($expression['unified_social_credit_code']);
        }
        if (isset($expression['establishment_date'])) {
            $enterprise->setEstablishmentDate($expression['establishment_date']);
        }
        if (isset($expression['approval_date'])) {
            $enterprise->setApprovalDate($expression['approval_date']);
        }
        if (isset($expression['address'])) {
            $enterprise->setAddress($expression['address']);
        }
        if (isset($expression['registration_capital'])) {
            $enterprise->setRegistrationCapital($expression['registration_capital']);
        }
        if (isset($expression['business_term_start'])) {
            $enterprise->setBusinessTermStart($expression['business_term_start']);
        }
        if (isset($expression['business_term_to'])) {
            $enterprise->setBusinessTermTo($expression['business_term_to']);
        }
        if (isset($expression['business_scope'])) {
            $enterprise->setBusinessScope($expression['business_scope']);
        }
        if (isset($expression['registration_authority'])) {
            $enterprise->setRegistrationAuthority($expression['registration_authority']);
        }
        if (isset($expression['principal'])) {
            $enterprise->setPrincipal($expression['principal']);
        }
        if (isset($expression['principal_card_id'])) {
            $enterprise->setPrincipalCardId($expression['principal_card_id']);
        }
        if (isset($expression['registration_status'])) {
            $enterprise->setRegistrationStatus($expression['registration_status']);
        }
        if (isset($expression['enterprise_type_code'])) {
            $enterprise->setEnterpriseTypeCode($expression['enterprise_type_code']);
        }
        if (isset($expression['enterprise_type'])) {
            $enterprise->setEnterpriseType($expression['enterprise_type']);
        }
        if (isset($expression['data'])) {
            $enterprise->setData(unserialize(gzuncompress(base64_decode($expression['data'], true))));
        }
        if (isset($expression['industry_category'])) {
            $enterprise->setIndustryCategory($expression['industry_category']);
        }
        if (isset($expression['industry_code'])) {
            $enterprise->setIndustryCode($expression['industry_code']);
        }
        if (isset($expression['hash'])) {
            $enterprise->setHash($expression['hash']);
        }
        if (isset($expression['task_id'])) {
            $enterprise->getTask()->setId($expression['task_id']);
        }
        if (isset($expression['administrative_area'])) {
            $enterprise->setAdministrativeArea($expression['administrative_area']);
        }
        $enterprise->setStatus($expression['status']);
        $enterprise->setCreateTime($expression['create_time']);
        $enterprise->setUpdateTime($expression['update_time']);
        $enterprise->setStatusTime($expression['status_time']);

        return $enterprise;
    }

    public function objectToArray($enterprise, array $keys = array()) : array
    {
        if (!$enterprise instanceof Enterprise) {
            return [];
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'unifiedSocialCreditCode',
                'establishmentDate',
                'approvalDate',
                'address',
                'registrationCapital',
                'businessTermStart',
                'businessTermTo',
                'businessScope',
                'registrationAuthority',
                'principal',
                'principalCardId',
                'registrationStatus',
                'enterpriseTypeCode',
                'enterpriseType',
                'data',
                'industryCategory',
                'industryCode',
                'administrativeArea',
                'hash',
                'task',
                'status',
                'createTime',
                'updateTime',
                'statusTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['enterprise_id'] = $enterprise->getId();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $enterprise->getName();
        }
        if (in_array('unifiedSocialCreditCode', $keys)) {
            $expression['unified_social_credit_code'] = $enterprise->getUnifiedSocialCreditCode();
        }
        if (in_array('establishmentDate', $keys)) {
            $expression['establishment_date'] = $enterprise->getEstablishmentDate();
        }
        if (in_array('approvalDate', $keys)) {
            $expression['approval_date'] = $enterprise->getApprovalDate();
        }
        if (in_array('address', $keys)) {
            $expression['address'] = $enterprise->getAddress();
        }
        if (in_array('registrationCapital', $keys)) {
            $expression['registration_capital'] = $enterprise->getRegistrationCapital();
        }
        if (in_array('businessTermStart', $keys)) {
            $expression['business_term_start'] = $enterprise->getBusinessTermStart();
        }
        if (in_array('businessTermTo', $keys)) {
            $expression['business_term_to'] = $enterprise->getBusinessTermTo();
        }
        if (in_array('businessScope', $keys)) {
            $expression['business_scope'] = $enterprise->getBusinessScope();
        }
        if (in_array('registrationAuthority', $keys)) {
            $expression['registration_authority'] = $enterprise->getRegistrationAuthority();
        }
        if (in_array('principal', $keys)) {
            $expression['principal'] = $enterprise->getPrincipal();
        }
        if (in_array('principalCardId', $keys)) {
            $expression['principal_card_id'] = $enterprise->getPrincipalCardId();
        }
        if (in_array('registrationStatus', $keys)) {
            $expression['registration_status'] = $enterprise->getRegistrationStatus();
        }
        if (in_array('enterpriseTypeCode', $keys)) {
            $expression['enterprise_type_code'] = $enterprise->getEnterpriseTypeCode();
        }
        if (in_array('enterpriseType', $keys)) {
            $expression['enterprise_type'] = $enterprise->getEnterpriseType();
        }
        if (in_array('data', $keys)) {
            $expression['data'] = base64_encode(gzcompress(serialize($enterprise->getData())));
        }
        if (in_array('industryCategory', $keys)) {
            $expression['industry_category'] = $enterprise->getIndustryCategory();
        }
        if (in_array('industryCode', $keys)) {
            $expression['industry_code'] = $enterprise->getIndustryCode();
        }
        if (in_array('administrativeArea', $keys)) {
            $expression['administrative_area'] = $enterprise->getAdministrativeArea();
        }
        if (in_array('hash', $keys)) {
            $expression['hash'] = $enterprise->getHash();
        }
        if (in_array('task', $keys)) {
            $expression['task_id'] = $enterprise->getTask()->getId();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $enterprise->getStatus();
        }
        if (in_array('createTime', $keys)) {
            $expression['create_time'] = $enterprise->getCreateTime();
        }
        if (in_array('updateTime', $keys)) {
            $expression['update_time'] = $enterprise->getUpdateTime();
        }
        if (in_array('statusTime', $keys)) {
            $expression['status_time'] = $enterprise->getStatusTime();
        }
        
        return $expression;
    }
}

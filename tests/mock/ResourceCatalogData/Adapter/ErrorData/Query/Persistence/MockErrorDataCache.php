<?php
namespace BaseData\ResourceCatalogData\Adapter\ErrorData\Query\Persistence;

class MockErrorDataCache extends ErrorDataCache
{
    public function getKey() : string
    {
        return parent::getKey();
    }
}

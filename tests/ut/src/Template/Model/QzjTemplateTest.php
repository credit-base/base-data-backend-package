<?php
namespace BaseData\Template\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\UserGroup\Model\UserGroup;

use BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class QzjTemplateTest extends TestCase
{
    private $qzjTemplate;

    public function setUp()
    {
        $this->qzjTemplate = new QzjTemplate();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->qzjTemplate);
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceOf(
            'BaseData\Template\Model\Template',
            $this->qzjTemplate
        );
    }

    public function testImplementsIOperate()
    {
        $this->assertInstanceOf(
            'BaseData\Common\Model\IOperate',
            $this->qzjTemplate
        );
    }

    public function testQzjTemplateConstructor()
    {
        $this->assertEquals(0, $this->qzjTemplate->getCategory());
        $this->assertInstanceOf(
            'BaseData\UserGroup\Model\UserGroup',
            $this->qzjTemplate->getSourceUnit()
        );
    }

    //category 测试 ------------------------------------------------------ start
    /**
     * 循环测试 QzjTemplate setCategory() 是否符合预定范围
     *
     * @dataProvider categoryProvider
     */
    public function testSetCategory($actual, $expected)
    {
        $this->qzjTemplate->setCategory($actual);
        $this->assertEquals($expected, $this->qzjTemplate->getCategory());
    }

    /**
     * 循环测试 QzjTemplate setCategory() 数据构建器
     */
    public function categoryProvider()
    {
        return array(
            array(
                QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ'],
                QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_WBJ']
            ),
            array(
                QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_BJ'],
                QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_BJ']
            ),
            array(
                QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_GB'],
                QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_GB']
            ),
        );
    }

    /**
     * 设置 QzjTemplate setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->qzjTemplate->setCategory('string');
    }
    //category 测试 ------------------------------------------------------   end
    //sourceUnit 测试 ---------------------------------------------------- start
    /**
     * 设置 setSourceUnit() 正确的传参类型,期望传值正确
     */
    public function testSetSourceUnitCorrectType()
    {
        $sourceUnit = new UserGroup();

        $this->qzjTemplate->setSourceUnit($sourceUnit);
        $this->assertEquals($sourceUnit, $this->qzjTemplate->getSourceUnit());
    }

    /**
     * 设置 setSourceUnit() 错误的传参类型, 期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSourceUnitWrongType()
    {
        $sourceUnit = array(1);

        $this->qzjTemplate->setSourceUnit($sourceUnit);
    }
    //sourceUnit 测试 ----------------------------------------------------   end
        
    public function testGetRepository()
    {
        $qzjTemplate = new MockQzjTemplate();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\QzjTemplate\IQzjTemplateAdapter',
            $qzjTemplate->getRepository()
        );
    }

    //add()
    /**
     * 测试添加成功
     * 1. 期望isTemplateExist被执行一次,且返回true
     * 2. 预言 IQzjTemplateAdapter 调用 add 并且传参 $qzjTemplate 对象, 返回 true
     * 3. 期望 getRepository被调用一次
     */
    public function testAddSuccess()
    {
        //初始化
        $qzjTemplate = $this->getMockBuilder(QzjTemplate::class)
                           ->setMethods(['getRepository', 'isTemplateExist'])
                           ->getMock();

        $qzjTemplate->expects($this->once())->method('isTemplateExist')->willReturn(true);

        //预言 IQzjTemplateAdapter
        $repository = $this->prophesize(IQzjTemplateAdapter::class);
        $repository->add(Argument::exact($qzjTemplate))->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $qzjTemplate->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        //验证
        $result = $qzjTemplate->add();
        $this->assertTrue($result);
    }

    //add()
    /**
     * 测试添加失败
     * 1. 期望isTemplateExist被执行一次,且返回true
     * 2. 预言 IQzjTemplateAdapter 调用 add 并且传参 $qzjTemplate 对象, 返回 false
     * 3. 期望 getRepository被调用一次
     */
    public function testAddFail()
    {
        //初始化
        $qzjTemplate = $this->getMockBuilder(QzjTemplate::class)
                           ->setMethods(['getRepository', 'isTemplateExist'])
                           ->getMock();

        $qzjTemplate->expects($this->once())->method('isTemplateExist')->willReturn(true);
        //预言 IQzjTemplateAdapter
        $repository = $this->prophesize(IQzjTemplateAdapter::class);
        $repository->add(Argument::exact($qzjTemplate))->shouldBeCalledTimes(1)->willReturn(false);

        //绑定
        $qzjTemplate->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        //验证
        $result = $qzjTemplate->add();
        $this->assertFalse($result);
    }

    //edit
    /**
     * 期望编辑成功
     * 1. 期望更新 updateTime
     * 2. 期望isTemplateExist被执行一次返回true
     * 3. 期望 getRepository 被调用一次
     * 4. 期望 IQzjTemplateAdapter 调用 edit 并且传参 $qzjTemplate 对象, 和相应字段, 返回true
     */
    public function testEditSuccess()
    {
        //初始化
        $qzjTemplate = $this->getMockBuilder(QzjTemplate::class)
                           ->setMethods(['setUpdateTime', 'getRepository', 'isTemplateExist'])
                           ->getMock();

        $qzjTemplate->expects($this->once())->method('isTemplateExist')->willReturn(true);
        //预言修改updateTime
        $qzjTemplate->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));

        //预言 IQzjTemplateAdapter
        $repository = $this->prophesize(IQzjTemplateAdapter::class);
        $repository->edit(
            Argument::exact($qzjTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'category',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items',
                    'sourceUnit',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $qzjTemplate->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $qzjTemplate->edit();
        $this->assertTrue($result);
    }

    //edit
    /**
     * 期望编辑失败
     * 1. 期望更新 updateTime
     * 2. 期望isTemplateExist被执行一次返回true
     * 3. 期望 getRepository 被调用一次
     * 4. 期望 IQzjTemplateAdapter 调用 edit 并且传参 $qzjTemplate 对象, 和相应字段, 返回false
     */
    public function testEditFail()
    {
        //初始化
        $qzjTemplate = $this->getMockBuilder(QzjTemplate::class)
                           ->setMethods(['setUpdateTime', 'isTemplateExist', 'getRepository'])
                           ->getMock();

        $qzjTemplate->expects($this->once())->method('isTemplateExist')->willReturn(true);
        //预言修改updateTime
        $qzjTemplate->expects($this->exactly(1))->method('setUpdateTime')->with(Core::$container->get('time'));

        //预言 IQzjTemplateAdapter
        $repository = $this->prophesize(IQzjTemplateAdapter::class);
        $repository->edit(
            Argument::exact($qzjTemplate),
            Argument::exact(
                [
                    'name',
                    'identify',
                    'category',
                    'subjectCategory',
                    'dimension',
                    'exchangeFrequency',
                    'infoClassify',
                    'infoCategory',
                    'description',
                    'items',
                    'sourceUnit',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(false);

        //绑定
        $qzjTemplate->expects($this->once())->method('getRepository')->willReturn($repository->reveal());
        
        //验证
        $result = $qzjTemplate->edit();
        $this->assertFalse($result);
    }

    private function initialIsTemplateExist($result)
    {
        $this->qzjTemplate = $this->getMockBuilder(MockQzjTemplate::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        $id = 1;
        
        $qzjTemplate = \BaseData\Template\Utils\QzjTemplateMockFactory::generateQzjTemplate($id);

        $count = 0;
        $list = array();

        if (!$result) {
            $count = 1;
            $list = array($qzjTemplate);
        }

        $this->qzjTemplate->setId($qzjTemplate->getId());
        $this->qzjTemplate->setIdentify($qzjTemplate->getIdentify());
        $this->qzjTemplate->setCategory($qzjTemplate->getCategory());
        $this->qzjTemplate->setSourceUnit($qzjTemplate->getSourceUnit());

        $filter['id'] = $qzjTemplate->getId();
        $filter['identify'] = $qzjTemplate->getIdentify();
        $filter['category'] = $qzjTemplate->getCategory();
        $filter['sourceUnit'] = $qzjTemplate->getSourceUnit()->getId();

        $repository = $this->prophesize(IQzjTemplateAdapter::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->qzjTemplate->expects($this->any())->method('getRepository')->willReturn($repository->reveal());
    }

    public function testIsTemplateExistFailure()
    {
        $this->initialIsTemplateExist(false);

        $result = $this->qzjTemplate->isTemplateExist();

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_ALREADY_EXIST, Core::getLastError()->getId());
        $this->assertEquals('qzjTemplate', Core::getLastError()->getSource()['pointer']);
    }

    public function testIsTemplateExistSuccess()
    {
        $this->initialIsTemplateExist(true);
        
        $result = $this->qzjTemplate->isTemplateExist();

        $this->assertTrue($result);
    }
}

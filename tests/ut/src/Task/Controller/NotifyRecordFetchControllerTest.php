<?php
namespace BaseData\Task\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;
use BaseData\Task\Model\NullNotifyRecord;
use BaseData\Task\Model\NotifyRecord;
use BaseData\Task\View\NotifyRecordView;

class NotifyRecordFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(NotifyRecordFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockNotifyRecordFetchController();

        $this->assertInstanceOf(
            'BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockNotifyRecordFetchController();

        $this->assertInstanceOf(
            'BaseData\Task\View\NotifyRecordView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockNotifyRecordFetchController();

        $this->assertEquals(
            'notifyRecords',
            $controller->getResourceName()
        );
    }
}

<?php
namespace BaseData\Task\Utils;

trait TaskUtils
{
    private function compareArrayAndObject(
        array $expectedArray,
        $task
    ) {
        $this->assertEquals($expectedArray['task_id'], $task->getId());
        $this->assertEquals($expectedArray['parent_id'], $task->getParentId());
        $this->assertEquals($expectedArray['identify'], marmot_encode($task->getIdentify()));
        $this->assertEquals($expectedArray['type'], $task->getType());
        $this->assertEquals($expectedArray['data'], serialize($task->getData()));
        $this->assertEquals($expectedArray['nice'], $task->getNice());
        $this->assertEquals($expectedArray['pid'], $task->getPid());
        $this->assertEquals($expectedArray['next_schedule_time'], $task->getNextScheduleTime());

        $this->assertEquals($expectedArray['status'], $task->getStatus());
        $this->assertEquals($expectedArray['create_time'], $task->getCreateTime());
        $this->assertEquals($expectedArray['update_time'], $task->getUpdateTime());
        $this->assertEquals($expectedArray['status_time'], $task->getStatusTime());
    }
}

<?php
namespace BaseData\Template\Adapter\BjTemplate\Query\Persistence;

use Marmot\Framework\Classes\Db;

class BjTemplateDb extends Db
{
    const TABLE = 'bj_template';

    public function __construct()
    {
        parent::__construct(self::TABLE);
    }
}

<?php
namespace BaseData\ResourceCatalogData\CommandHandler\WbjSearchData;

use PHPUnit\Framework\TestCase;

use Marmot\Interfaces\ICommand;
use Marmot\Framework\Classes\NullCommandHandler;

use BaseData\ResourceCatalogData\Command\WbjSearchData\EnableWbjSearchDataCommand;
use BaseData\ResourceCatalogData\Command\WbjSearchData\DisableWbjSearchDataCommand;

class WbjSearchDataCommandHandlerFactoryTest extends TestCase
{
    private $commandHandler;

    private $faker;

    public function setUp()
    {
        $this->faker = \Faker\Factory::create('zh_CN');
        //初始化工厂桩件
        $this->commandHandler = new WbjSearchDataCommandHandlerFactory();
    }

    public function testDefaultCommandHandler()
    {
        $command = $this->getMockBuilder(ICommand::class)
                        ->getMock();

        $commandHandler = $this->commandHandler->getHandler(
            $command
        );

        $this->assertInstanceOf('Marmot\Framework\Classes\NullCommandHandler', $commandHandler);
    }

    public function testDisableWbjSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new DisableWbjSearchDataCommand(
                $this->faker->randomNumber(1)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\CommandHandler\WbjSearchData\DisableWbjSearchDataCommandHandler',
            $commandHandler
        );
    }

    public function testEnableWbjSearchDataCommandHandler()
    {
        $commandHandler = $this->commandHandler->getHandler(
            new EnableWbjSearchDataCommand(
                $this->faker->randomNumber(3)
            )
        );

        $this->assertInstanceOf('Marmot\Interfaces\ICommandHandler', $commandHandler);
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\CommandHandler\WbjSearchData\EnableWbjSearchDataCommandHandler',
            $commandHandler
        );
    }
}

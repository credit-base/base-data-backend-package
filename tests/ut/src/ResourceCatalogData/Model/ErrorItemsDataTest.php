<?php
namespace BaseData\ResourceCatalogData\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter;

class ErrorItemsDataTest extends TestCase
{
    private $errorItemsData;

    public function setUp()
    {
        $this->errorItemsData = new ErrorItemsData();
    }

    public function tearDown()
    {
        unset($this->errorItemsData);
    }

    public function testGetRepository()
    {
        $errorItemsData = new MockErrorItemsData();
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\ErrorData\IErrorDataAdapter',
            $errorItemsData->getRepository()
        );
    }
}

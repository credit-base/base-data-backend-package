<?php
namespace BaseData\Task\Model;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Task\Adapter\Task\ITaskAdapter;

class PeriodTaskTest extends TestCase
{
    private $task;

    public function setUp()
    {
        $this->task = new MockPeriodTask();
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("BaseData\Task\Model\Task", $this->task);
    }

    public function testConstructor()
    {
        $this->assertEquals(Task::TYPE['PERIOD'], $this->task->getType());
    }

    //updateSchedule -- 开始
    public function testClose()
    {
        $this->task = $this->getMockBuilder(MockPeriodTask::class)
            ->setMethods(['getRepository'])
            ->getMock();

        $this->task->setStatus(Task::STATUS['PENDING']);
        $this->task->setUpdateTime(time());
        $this->task->setNextScheduleTime($this->task->getPeriod() + time());

        $repository = $this->prophesize(ITaskAdapter::class);

        $repository->edit(
            Argument::exact($this->task),
            Argument::exact(array(
                'nextScheduleTime',
                'status',
                'statusTime'
            ))
        )->shouldBeCalledTimes(1)->willReturn(true);

        $this->task->expects($this->once())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());

        $result = $this->task->updateSchedule();
        
        $this->assertTrue($result);
    }
    //updateSchedule -- 结束

    //afterExecute -- 开始
    public function testAfterExecute()
    {
        $this->task = $this->getMockBuilder(MockPeriodTask::class)
            ->setMethods(['updateSchedule'])
            ->getMock();

        $this->task->expects($this->once())
                   ->method('updateSchedule')
                   ->willReturn(true);

        $result = $this->task->afterExecute();
        
        $this->assertTrue($result);
    }
    //afterExecute -- 结束
}

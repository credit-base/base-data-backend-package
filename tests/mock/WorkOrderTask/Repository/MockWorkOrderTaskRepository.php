<?php
namespace BaseData\WorkOrderTask\Repository;

use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

class MockWorkOrderTaskRepository extends WorkOrderTaskRepository
{
    public function getAdapter() : IWorkOrderTaskAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IWorkOrderTaskAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IWorkOrderTaskAdapter
    {
        return parent::getMockAdapter();
    }
}

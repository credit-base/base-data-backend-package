<?php
namespace BaseData\ResourceCatalogData\Adapter\YsItemsData\Query\Persistence;

use Marmot\Framework\Classes\Cache;

class YsItemsDataCache extends Cache
{
    const KEY = 'ys_items_data';

    public function __construct()
    {
        parent::__construct(self::KEY);
    }
}

<?php
namespace BaseData\Task\Model;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\Task\Script\SyncTask;

use BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class NotifyRecordTest extends TestCase
{
    private $record;

    public function setUp()
    {
        $this->record = new MockNotifyRecord();
    }

    public function tearDown()
    {
        unset($this->record);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceof("Marmot\Common\Model\IObject", $this->record);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->record->getRepository()
        );
    }

    public function testGetSyncTask()
    {
        $this->assertInstanceOf(
            'BaseData\Task\Script\SyncTask',
            $this->record->getSyncTask()
        );
    }

    /**
     * 测试构造函数
     */
    public function testConstructor()
    {
        $this->assertEquals(0, $this->record->getId());

        $this->assertEquals('', $this->record->getName());

        $this->assertEquals(0, $this->record->getStatus());

        //测试初始化创建时间
        $this->assertEquals(Core::$container->get('time'), $this->record->getCreateTime());

        //测试初始化更新时间
        $this->assertEquals(Core::$container->get('time'), $this->record->getUpdateTime());


        $this->assertInstanceof(
            'BaseData\Task\Adapter\NotifyRecord\INotifyRecordAdapter',
            $this->record->getRepository()
        );

        //测试初始化状态更新时间
        $this->assertEquals(0, $this->record->getStatusTime());
    }

    //name -- 开始
    /**
     * 设置 NotifyRecord setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->record->setName('name');
        $this->assertEquals('name', $this->record->getName());
    }
    /**
     * 设置 NotifyRecord setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->record->setName(array(1, 2, 3));
    }
    //name -- 结束

    //hash -- 开始
    /**
     * 设置 NotifyRecord setHash() 正确的传参类型,期望传值正确
     */
    public function testSetHashCorrectType()
    {
        $this->record->setHash('hash');
        $this->assertEquals('hash', $this->record->getHash());
    }
    /**
     * 设置 NotifyRecord setHash() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHashWrongType()
    {
        $this->record->setHash(array(1, 2, 3));
    }
    //hash -- 结束

    //status -- 开始
    /**
     * 循环测试 NotifyRecord setStatus() 是否符合预定范围
     *
     * @dataProvider statusProvider
     */
    public function testSetStatus($actual, $expected)
    {
        $this->record->setStatus($actual);
        $this->assertEquals($expected, $this->record->getStatus());
    }
    /**
     * 循环测试 NotifyRecord setStatus() 数据构建器
     */
    public function statusProvider()
    {
        return array(
            array(NotifyRecord::STATUS['PENDING'],NotifyRecord::STATUS['PENDING']),
            array(NotifyRecord::STATUS['SUCCESS'],NotifyRecord::STATUS['SUCCESS']),
            array(999,NotifyRecord::STATUS['PENDING']),
        );
    }
    /**
     * 设置 NotifyRecord setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->record->setStatus('string');
    }
    //status -- 结束

    //hash
    public function testGenerateHash()
    {
        $name = 'XZXK_10_1_hash.xlsx';
        $notifyRecord = new MockNotifyRecord();
        $notifyRecord->setName($name);

        $this->assertEquals('hash', $notifyRecord->generateHash());
    }

    //validate
    public function testValidate()
    {
        $name = 'XZXK_10_1_hash.xlsx';
        $fileName = 'XZXK_10_1_hash';
        $extenstion = 'xlsx';

        //初始化
        $notifyRecord = $this->getMockBuilder(MockNotifyRecord::class)
                           ->setMethods([
                            'getName',
                            'validateFileName',
                            'validateExtension'
                        ])->getMock();

        $notifyRecord->expects($this->once())
                     ->method('getName')
                     ->willReturn($name);

        $notifyRecord->expects($this->once())
                     ->method('validateFileName')
                     ->with($fileName)
                     ->willReturn(true);

        $notifyRecord->expects($this->once())
                     ->method('validateExtension')
                     ->with($extenstion)
                     ->willReturn(true);

        $result = $notifyRecord->validate();
        $this->assertTrue($result);
    }

    /**
     * validateFileName
     * @dataProvider fileNameProvider
     */
    public function testValidateFileName($actual, $expected)
    {
        $notifyRecord = new MockNotifyRecord();
        $result = $notifyRecord->validateFileName($actual);
        $this->assertEquals($expected, $result);
    }
    /**
     * 循环测试 validateFileName() 数据构建器
     */
    public function fileNameProvider()
    {
        return array(
            array('XZXK_10_1_hash', true),
            array('', false),
            array('XZXK_10_1', false),
            array('XZXK_10', false),
            array('XZXK', false)
        );
    }

    /**
     * validateExtension
     * @dataProvider extensionProvider
     */
    public function testValidateExtension($actual, $expected)
    {
        $notifyRecord = new MockNotifyRecord();
        $result = $notifyRecord->validateExtension($actual);
        $this->assertEquals($expected, $result);
    }

    /**
     * 循环测试 validateExtension() 数据构建器
     */
    public function extensionProvider()
    {
        return array(
            array('xls', true),
            array('xlsx', true),
            array('doc', false),
        );
    }

    //add
    public function testAdd()
    {
        //初始化
        $notifyRecord = $this->getMockBuilder(NotifyRecord::class)
                           ->setMethods([
                                'generateHash',
                                'validate',
                                'addAction',
                                'notify'
                            ])
                           ->getMock();

        $notifyRecord->expects($this->once())->method('generateHash')->willReturn(true);
        $notifyRecord->expects($this->once())->method('validate')->willReturn(true);
        $notifyRecord->expects($this->once())->method('addAction')->willReturn(true);
        $notifyRecord->expects($this->once())->method('notify')->willReturn(true);

        //验证
        $result = $notifyRecord->add();
        $this->assertTrue($result);
    }

    //addAction
    public function testAddAction()
    {
        //初始化
        $notifyRecord = $this->getMockBuilder(MockNotifyRecord::class)
                           ->setMethods([
                            'getRepository',
                            ])
                           ->getMock();

        //预言 INewsAdapter
        $repository = $this->prophesize(INotifyRecordAdapter::class);
        $repository->add(Argument::exact($notifyRecord))
                   ->shouldBeCalledTimes(1)
                   ->willReturn(true);

        //绑定
        $notifyRecord->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());

        //验证
        $result = $notifyRecord->addAction();
        $this->assertTrue($result);
    }

    //notify
    public function testNotify()
    {
        $notifyRecord = $this->getMockBuilder(NotifyRecord::class)
                           ->setMethods(['getSyncTask'])
                           ->getMock();

        $notifyRecord->setName('XZXK_10_1_1626859792.xlsx');
        
        $syncTask = $this->prophesize(SyncTask::class);
        $syncTask->setData(array('name' => 'XZXK_10_1_1626859792.xlsx'))->shouldBeCalledTimes(1);
        $syncTask->add()->shouldBeCalledTimes(1)->willReturn(true);
        $notifyRecord->expects($this->once())->method('getSyncTask')->willReturn($syncTask->reveal());

        $result = $notifyRecord->notify();
        $this->assertTrue($result);
    }
}

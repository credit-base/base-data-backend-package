<?php
namespace BaseData\Template\Translator;

use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Model\NullQzjTemplate;

class QzjTemplateDbTranslator extends TemplateDbTranslator
{
    public function arrayToObject(array $expression, $qzjTemplate = null) : QzjTemplate
    {
        if (!isset($expression['qzj_template_id'])) {
            return NullQzjTemplate::getInstance();
        }

        if ($qzjTemplate == null) {
            $qzjTemplate = new QzjTemplate();
        }

        $qzjTemplate->setId($expression['qzj_template_id']);

        if (isset($expression['source_unit_id'])) {
            $qzjTemplate->getSourceUnit()->setId($expression['source_unit_id']);
        }
        if (isset($expression['category'])) {
            $qzjTemplate->setCategory($expression['category']);
        }

        $qzjTemplate = parent::arrayToObject($expression, $qzjTemplate);
        
        return $qzjTemplate;
    }

    public function objectToArray($qzjTemplate, array $keys = array()) : array
    {
        if (!$qzjTemplate instanceof QzjTemplate) {
            return [];
        }

        $templateExpression = parent::objectToArray($qzjTemplate, $keys);
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'sourceUnit',
                'category'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['qzj_template_id'] = $qzjTemplate->getId();
        }
        if (in_array('sourceUnit', $keys)) {
            $expression['source_unit_id'] = $qzjTemplate->getSourceUnit()->getId();
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $qzjTemplate->getCategory();
        }

        $expression = array_merge($expression, $templateExpression);
        
        return $expression;
    }
}

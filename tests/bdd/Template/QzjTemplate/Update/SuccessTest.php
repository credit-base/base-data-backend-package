<?php
namespace BaseData\Template\QzjTemplate\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\Template;
use BaseData\Template\Model\QzjTemplate;
use BaseData\Template\Repository\QzjTemplateRepository;
use BaseData\Template\Translator\QzjTemplateDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当需要报送的资源目录发生变动时,我需要修改自己的前置机资源目录,在目录管理子系统下的前置机资源目录管理中,编辑前置机资源目录
 *           根据需要报送的资源目录修改现有前置机资源目录目录
 *           以便于我能及时报送数据
 * @Scenario: 正常编辑资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_qzj_template');
    }

    /**
     * @Given: 我并未编辑过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_qzj_template' => $this->qzjTemplate(),
        ]);
    }

    /**
     * @When: 获取需要编辑的用户数据
     */
    public function fetchTemplate($id)
    {
        $repository = new QzjTemplateRepository();

        $qzjTemplate = $repository->fetchOne($id);

        return $qzjTemplate;
    }
    /**
     * @When: 当我调用编辑函数,期待返回true
     */
    public function edit()
    {
        $qzjTemplate = $this->fetchTemplate(1);

        $qzjTemplate->setSubjectCategory(array(Template::SUBJECT_CATEGORY['ZRR']));
        $qzjTemplate->setDimension(Template::DIMENSION['ZWGX']);
        $qzjTemplate->setExchangeFrequency(2);
        $qzjTemplate->setInfoClassify(Template::INFO_CLASSIFY['QT']);
        $qzjTemplate->setInfoCategory(Template::INFO_CATEGORY['QTXX']);
        $qzjTemplate->setCategory(QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_BJ']);
        $qzjTemplate->setItems(array('items'));
        
        return $qzjTemplate->edit();
    }

    /**
     * @Then: 可以查到编辑的数据
     */
    public function testValidate()
    {
        $result = $this->edit();

        $this->assertTrue($result);
        
        $qzjTemplate = $this->fetchTemplate(1);

        $this->assertEquals(array(Template::SUBJECT_CATEGORY['ZRR']), $qzjTemplate->getSubjectCategory());
        $this->assertEquals(Template::DIMENSION['ZWGX'], $qzjTemplate->getDimension());
        $this->assertEquals(2, $qzjTemplate->getExchangeFrequency());
        $this->assertEquals(Template::INFO_CLASSIFY['QT'], $qzjTemplate->getInfoClassify());
        $this->assertEquals(Template::INFO_CATEGORY['QTXX'], $qzjTemplate->getInfoCategory());
        $this->assertEquals(QzjTemplate::QZJ_TEMPLATE_CATEGORY['QZJ_BJ'], $qzjTemplate->getCategory());
        $this->assertEquals(array('items'), $qzjTemplate->getItems());
    }
}

<?php
namespace BaseData\Template\BjTemplate\Add;

use tests\DbTrait;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\SetDataTrait;
use BaseData\Template\Model\BjTemplate;
use BaseData\Template\Model\GbTemplate;
use BaseData\Template\Repository\BjTemplateRepository;
use BaseData\Template\Translator\BjTemplateDbTranslator;

/**
 * @Feature: 我是发改委领导,当我需要使用本级目录但又没有对应本级目录时,在目录管理子系统,创建对应的本级目录
 *           选择一个国标目录根据我需要归集的内容和规范新增本级目录
 *           以便于我有本级目录可以使用
 * @Scenario: 新增资源目录数据
 */
class SuccessTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_gb_template');
    }

    /**
     * @Given: 我并未新增过该条数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_gb_template' => $this->gbTemplate()
        ]);
    }

    /**
     * @When: 当我调用添加函数,期待返回true
     */
    public function add()
    {
        $bjTemplateArray = $this->bjTemplate()[0];

        $bjTemplate = new BjTemplate();

        $bjTemplate->getSourceUnit()->setId($bjTemplateArray['source_unit_id']);
        $bjTemplate->getGbTemplate()->setId($bjTemplateArray['gb_template_id']);
        $bjTemplate->setName($bjTemplateArray['name']);
        $bjTemplate->setIdentify($bjTemplateArray['identify']);
        $bjTemplate->setSubjectCategory(json_decode($bjTemplateArray['subject_category'], true));
        $bjTemplate->setDimension($bjTemplateArray['dimension']);
        $bjTemplate->setExchangeFrequency($bjTemplateArray['exchange_frequency']);
        $bjTemplate->setInfoClassify($bjTemplateArray['info_classify']);
        $bjTemplate->setInfoCategory($bjTemplateArray['info_category']);
        $bjTemplate->setDescription($bjTemplateArray['description']);
        $bjTemplate->setItems(json_decode($bjTemplateArray['items'], true));

        return $bjTemplate->add();
    }

    /**
     * @Then: 可以查到新增的数据
     */
    public function testValidate()
    {
        $result = $this->add();

        $this->assertTrue($result);

        $setBjTemplate = $this->bjTemplate()[0];
        $setBjTemplate['bj_template_id'] = 1;
        $setBjTemplate['subject_category'] = json_decode($setBjTemplate['subject_category'], true);
        $setBjTemplate['items'] = json_decode($setBjTemplate['items'], true);
        
        $repository = new BjTemplateRepository();

        $bjTemplate = $repository->fetchOne($result);

        $translator = new BjTemplateDbTranslator();
        $bjTemplateArray = $translator->objectToArray($bjTemplate);

        $this->assertEquals($bjTemplateArray, $setBjTemplate);
    }
}

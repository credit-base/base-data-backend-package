<?php
namespace BaseData\Template\QzjTemplate\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait;
use BaseData\Template\Model\QzjTemplate;

/**
 * 测试前置机资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, TemplateUtils;

    private $qzjTemplate;

    public function tearDown()
    {
        parent::tearDown();
        unset($this->qzjTemplate);
        $this->clear('pcore_qzj_template');
    }

    /**
     * 存在一条前置机资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_qzj_template' => $this->qzjTemplate()
            ]
        );
    }

    public function testViewQzjTemplate()
    {
        $setQzjTemplate = $this->qzjTemplate()[0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'qzjTemplates/1',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->qzjTemplateCompareData($setQzjTemplate, $contents['data']);
    }
}

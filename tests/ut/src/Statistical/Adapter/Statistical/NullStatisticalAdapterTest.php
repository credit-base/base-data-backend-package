<?php
namespace BaseData\Statistical\Adapter\Statistical;

use PHPUnit\Framework\TestCase;

use BaseData\Statistical\Model\NullStatistical;

class NullStatisticalAdapterTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new NullStatisticalAdapter();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testAnalyse()
    {
        $result = $this->stub->analyse(array());

        $this->assertEquals(new NullStatistical(), $result);
    }
}

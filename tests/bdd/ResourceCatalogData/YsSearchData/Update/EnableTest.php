<?php
namespace BaseData\ResourceCatalogData\YsSearchData\Update;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\Common\Model\IEnableAble;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\YsSearchDataRepository;

/**
 * @Feature: 我是委办局工作人员,当我需要将禁用掉的资源目录数据启用时,在资源目录数据子系统下的原始数据管理,启用对应的资源目录数据
 *           根据我核实需要启用的数据启用,以便于我实现平台资源目录数据的呈现
 * @Scenario: 启用资源目录数据
 */
class EnableTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_ys_items_data');
        $this->clear('pcore_ys_search_data');
    }

    /**
     * @Given: 存在需要启用的资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_ys_items_data' => $this->ysSearchData(IEnableAble::STATUS['DISABLED'])['pcore_ys_items_data'],
            'pcore_ys_search_data' => $this->ysSearchData(IEnableAble::STATUS['DISABLED'])['pcore_ys_search_data']
        ]);
    }

    /**
     * @When: 获取需要启用的资源目录数据
     */
    public function fetchYsSearchData($id)
    {
        $repository = new YsSearchDataRepository();

        $ysSearchData = $repository->fetchOne($id);

        return $ysSearchData;
    }

    /**
     * @And: 当我调用启用函数,期待返回true
     */
    public function enable()
    {
        $ysSearchData = $this->fetchYsSearchData(1);
        
        return $ysSearchData->enable();
    }

    /**
     * @Then: 数据已经被启用
     */
    public function testValidate()
    {
        $result = $this->enable();

        $this->assertTrue($result);

        $ysSearchData = $this->fetchYsSearchData(1);

        $this->assertEquals(IEnableAble::STATUS['ENABLED'], $ysSearchData->getStatus());
        $this->assertEquals(Core::$container->get('time'), $ysSearchData->getUpdateTime());
        $this->assertEquals(Core::$container->get('time'), $ysSearchData->getStatusTime());
    }
}

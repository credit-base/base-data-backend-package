<?php
namespace BaseData\ResourceCatalogData\View;

use Neomerx\JsonApi\Schema\SchemaProvider;

class YsItemsDataSchema extends SchemaProvider
{
    protected $resourceType = 'ysItemsData';

    public function getId($ysItemsData) : int
    {
        return $ysItemsData->getId();
    }

    public function getAttributes($ysItemsData) : array
    {
        return [
            'data' => $ysItemsData->getData()
        ];
    }
}

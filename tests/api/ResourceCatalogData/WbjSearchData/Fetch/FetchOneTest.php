<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\ResourceCatalogData\SetDataApiTrait;
use BaseData\ResourceCatalogData\Model\WbjSearchData;
use BaseData\ResourceCatalogData\ResourceCatalogDataUtils;

use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

/**
 * 测试委办局资源目录接口查看单条
 */
class FetchOneTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, ResourceCatalogDataUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_wbj_items_data');
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * 存在一条委办局资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_wbj_items_data' => $this->wbjSearchData()['pcore_wbj_items_data'],
                'pcore_wbj_search_data' => $this->wbjSearchData()['pcore_wbj_search_data']
            ]
        );
    }

    public function testViewWbjSearchData()
    {
        $setWbjSearchData = $this->wbjSearchData()['pcore_wbj_search_data'][0];
        $setTemplate = $this->wbjTemplate()[0];
        $setWbjItemsData = $this->wbjSearchData()['pcore_wbj_items_data'][0];

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'wbjSearchData/1?include=itemsData,template',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals([], $meta);

        $this->wbjSearchDataCompareData($setWbjSearchData, $contents['data']);
        $this->wbjItemsDataCompareData($setWbjItemsData, $contents['included'][0]);
        $this->wbjTemplateCompareData($setTemplate, $contents['included'][1]);
    }
}

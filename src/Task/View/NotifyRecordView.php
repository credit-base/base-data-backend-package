<?php
namespace BaseData\Task\View;

use Marmot\Interfaces\IView;

use Marmot\Framework\View\JsonApiTrait;

class NotifyRecordView implements IView
{
    use JsonApiTrait;

    protected $rules;

    protected $data;
    
    protected $encodingParameters;

    public function __construct($data = '', $encodingParameters = null)
    {
        $this->data = $data;
        $this->encodingParameters = $encodingParameters;

        $this->rules = array(
            \BaseData\Task\Model\NotifyRecord::class =>
            \BaseData\Task\View\NotifyRecordSchema::class
        );
    }

    public function setData($data) : void
    {
        $this->data = $data;
    }
    
    public function display()
    {
        return $this->jsonApiFormat($this->data, $this->rules, $this->encodingParameters);
    }
}

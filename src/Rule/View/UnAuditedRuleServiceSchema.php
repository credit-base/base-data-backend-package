<?php
namespace BaseData\Rule\View;

class UnAuditedRuleServiceSchema extends RuleServiceSchema
{
    protected $resourceType = 'unAuditedRules';

    public function getId($unAuditRuleService) : int
    {
        return $unAuditRuleService->getApplyId();
    }

    public function getAttributes($unAuditRuleService) : array
    {
        $attributes = parent::getAttributes($unAuditRuleService);
        $applyAttributes = array(
            'applyStatus'  => $unAuditRuleService->getApplyStatus(),
            'rejectReason' => $unAuditRuleService->getRejectReason(),
            'operationType'  => $unAuditRuleService->getOperationType(),
            'relationId'  => $unAuditRuleService->getRelationId()
        );
        
        return array_merge($applyAttributes, $attributes);
    }

    public function getRelationships($unAuditRuleService, $isPrimary, array $includeList)
    {
        $relationships = parent::getRelationships($unAuditRuleService, $isPrimary, $includeList);
        $applyRelationships = [
            'applyCrew' => [self::DATA => $unAuditRuleService->getApplyCrew()],
            'publishCrew' => [self::DATA => $unAuditRuleService->getPublishCrew()],
            'userGroup' => [self::DATA => $unAuditRuleService->getUserGroup()],
            'applyUserGroup' => [self::DATA => $unAuditRuleService->getApplyUserGroup()],
        ];

        return array_merge($applyRelationships, $relationships);
    }
}

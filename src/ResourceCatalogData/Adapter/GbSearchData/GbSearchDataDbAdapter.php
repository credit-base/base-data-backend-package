<?php
namespace BaseData\ResourceCatalogData\Adapter\GbSearchData;

use Marmot\Interfaces\INull;

use Marmot\Framework\Common\Adapter\DbAdapterTrait;

use BaseData\ResourceCatalogData\Model\GbSearchData;
use BaseData\ResourceCatalogData\Model\NullGbSearchData;
use BaseData\ResourceCatalogData\Repository\GbItemsDataRepository;
use BaseData\ResourceCatalogData\Adapter\SearchDataDbAdapterTrait;
use BaseData\ResourceCatalogData\Translator\GbSearchDataDbTranslator;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\Query\GbSearchDataRowCacheQuery;

use BaseData\Template\Repository\GbTemplateRepository;

class GbSearchDataDbAdapter implements IGbSearchDataAdapter
{
    use DbAdapterTrait, SearchDataDbAdapterTrait;

    private $dbTranslator;

    private $rowCacheQuery;

    private $gbItemsDataRepository;

    private $gbTemplateRepository;

    public function __construct()
    {
        $this->dbTranslator = new GbSearchDataDbTranslator();
        $this->rowCacheQuery = new GbSearchDataRowCacheQuery();
        $this->gbItemsDataRepository = new GbItemsDataRepository();
        $this->gbTemplateRepository = new GbTemplateRepository();
    }

    public function __destruct()
    {
        unset($this->dbTranslator);
        unset($this->rowCacheQuery);
        unset($this->gbItemsDataRepository);
        unset($this->gbTemplateRepository);
    }
    
    protected function getDbTranslator() : GbSearchDataDbTranslator
    {
        return $this->dbTranslator;
    }

    protected function getRowQuery() : GbSearchDataRowCacheQuery
    {
        return $this->rowCacheQuery;
    }

    protected function getNullObject() : INull
    {
        return NullGbSearchData::getInstance();
    }

    protected function getItemsDataRepository() : GbItemsDataRepository
    {
        return $this->gbItemsDataRepository;
    }

    protected function getTemplateRepository() : GbTemplateRepository
    {
        return $this->gbTemplateRepository;
    }

    public function add(GbSearchData $gbSearchData) : bool
    {
        return $this->addAction($gbSearchData);
    }

    public function edit(GbSearchData $gbSearchData, array $keys = array()) : bool
    {
        return $this->editAction($gbSearchData, $keys);
    }

    public function fetchOne($id) : GbSearchData
    {
        $gbSearchData = $this->fetchOneAction($id);
        $this->fetchTemplate($gbSearchData);
        $this->fetchItemsData($gbSearchData);
        
        return $gbSearchData;
    }

    public function fetchList(array $ids) : array
    {
        $gbSearchDataList = $this->fetchListAction($ids);
        $this->fetchTemplate($gbSearchDataList);
        $this->fetchItemsData($gbSearchDataList);

        return $gbSearchDataList;
    }

    protected function formatFilter(array $filter) : string
    {
        $gbSearchData = new GbSearchData();

        return $this->searchDataFormatFilter($filter, $gbSearchData);
    }

    protected function formatSort(array $sort) : string
    {
        $condition = '';
        $conjection = ' ORDER BY ';

        if (!empty($sort)) {
            $gbSearchData = new GbSearchData();
 
            if (isset($sort['id'])) {
                $info = $this->getDbTranslator()->objectToArray($gbSearchData, array('id'));
                $condition .= $conjection.key($info).' '.($sort['id'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
            if (isset($sort['updateTime'])) {
                $info = $this->getDbTranslator()->objectToArray($gbSearchData, array('updateTime'));
                $condition .= $conjection.key($info).' '.($sort['updateTime'] == -1 ? 'DESC' : 'ASC');
                $conjection = ',';
            }
        }

        return $condition;
    }
}

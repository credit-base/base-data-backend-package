<?php
namespace BaseData\Task\Adapter\Task\Query;

use PHPUnit\Framework\TestCase;
use Marmot\Core;

class TaskRowQueryTest extends TestCase
{
    private $rowQuery;

    public function setUp()
    {
        $this->rowQuery = new MockTaskRowQuery();
    }

    public function tearDown()
    {
        unset($this->rowQuery);
    }

    /**
     * 测试该文件是否正确的继承RowCacheQuery类
     */
    public function testCorrectInstanceExtendsRowCacheQuery()
    {
        $this->assertInstanceof('Marmot\Framework\Query\RowQuery', $this->rowQuery);
    }

    /**
     * 测试是否db层赋值正确
     */
    public function testCorrectDbLayer()
    {
        $this->assertInstanceof(
            'BaseData\Task\Adapter\Task\Query\Persistence\TaskDb',
            $this->rowQuery->getDbLayer()
        );
    }
}

<?php
namespace BaseData\WorkOrderTask\Adapter\WorkOrderTask\Query\Persistence;

class MockWorkOrderTaskDb extends WorkOrderTaskDb
{
    public function getTable() : string
    {
        return parent::getTable();
    }
}

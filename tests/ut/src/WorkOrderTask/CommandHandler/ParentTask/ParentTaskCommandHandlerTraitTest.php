<?php
namespace BaseData\WorkOrderTask\CommandHandler\ParentTask;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter;
use BaseData\Template\Model\GbTemplate;

use BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter;
use BaseData\Template\Model\BjTemplate;

use BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter;

class ParentTaskCommandHandlerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockParentTaskCommandHandlerTrait::class)
                           ->setMethods(
                               [
                                    'getGbTemplateRepository',
                                    'getBjTemplateRepository',
                                    'getUserGroupRepository'
                                ]
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetGbTemplateRepository()
    {
        $trait = new MockParentTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\GbTemplate\IGbTemplateAdapter',
            $trait->getGbTemplateRepositoryPublic()
        );
    }

    public function testGetBjTemplateRepository()
    {
        $trait = new MockParentTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'BaseData\Template\Adapter\BjTemplate\IBjTemplateAdapter',
            $trait->getBjTemplateRepositoryPublic()
        );
    }

    public function testGetUserGroupRepository()
    {
        $trait = new MockParentTaskCommandHandlerTrait();
        $this->assertInstanceOf(
            'BaseData\UserGroup\Adapter\UserGroup\IUserGroupAdapter',
            $trait->getUserGroupRepositoryPublic()
        );
    }

    public function testFetchOneGbTemplate()
    {
        $id = 1;

        $repository = $this->prophesize(IGbTemplateAdapter::class);
        $repository->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getGbTemplateRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchOneGbTemplatePublic($id);
    }

    public function testFetchOneBjTemplate()
    {
        $id = 1;

        $repository = $this->prophesize(IBjTemplateAdapter::class);
        $repository->fetchOne(Argument::exact($id))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getBjTemplateRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchOneBjTemplatePublic($id);
    }

    public function testFetchListUserGroup()
    {
        $ids = [1, 2, 3];

        $repository = $this->prophesize(IUserGroupAdapter::class);
        $repository->fetchList(Argument::exact($ids))
                ->shouldBeCalledTimes(1);

        $this->trait->expects($this->exactly(1))
                         ->method('getUserGroupRepository')
                         ->willReturn($repository->reveal());

        $this->trait->fetchListUserGroupPublic($ids);
    }
}

<?php
namespace BaseData\Rule\Model;

use Marmot\Core;

use BaseData\Rule\Model\IRule;

use BaseData\ResourceCatalogData\Model\ErrorData;
use BaseData\ResourceCatalogData\Model\ISearchDataAble;

class ComparisonRule extends Rule implements IRule
{
    const ITEM_SUBJECT_NAME_IDENTIFY = 'ZTMC';
    const ITEM_NATURAL_PERSON_SUBJECT_IDENTIFY_IDENTIFY = 'ZJHM';
    const ITEM_ENTERPRISE_SUBJECT_IDENTIFY_IDENTIFY = 'TYSHXYDM';

    use RuleTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    /**
     * 验证比对规则格式
        * 规则格式说明及样例
            * array(
            *     '比对信息项标识' => array(
            *         array('id'=>比对信息项来源id, 'base'=> array(比对依据), 'item'=>'被比对信息项标识'),
            *     ),
            *     'ZTMC' => array(
            *         array('id'=>1, 'base'=> array(1,2), 'item'=>'ZTMC'),
            *         array('id'=>2, 'base'=> array(1), 'item'=>'ZTMC'),
            *     ),
            *     'TYSHXYDM' => array(
            *         array('id'=>1, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
            *         array('id'=>2, 'base'=> array(1,2), 'item'=>'TYSHXYDM'),
            *     ),
            * )
        * 验证流程
            * 1. 获取比对规则 $condition = $this->getCondition();
            * 2. 获取目标资源目录和根据获取到的比对规则获取比对资源目录id集合
            * 3. 通过获取到的资源目录id集合获取资源目录
            * 4. 通过获取到的资源目录验证比对信息项存在,且验证比对信息项的字段格式
            * 5. 对比目标和比对信息项的模板字段的类型:
            *    (1). 数据类型对应关系(目标:来源): 日期:日期,字符串:全部,集合:集合,枚举:枚举,整型:整型,浮点:浮点-整型
            *    (2). 数据长度: 目标>=来源(全部),目标项包含来源(集合-枚举)
            * 6. 验证比对依据的合理性:
            *    (1). 如果比对资源目录为自然人,比对依据必须包含身份证号
            *    (2). 如果比对项为企业名称,比对依据不能为1(企业名称)
            *    (3). 如果比对项为统一社会信用代码或身份证号,那比对依据不能为2(统一社会信用代码或身份证号)
     * @return bool
     * @todo 后续需要对返回错误输出信息或日志，方便调试
     */
    public function validate() : bool
    {
        //获取目标资源目录
        $transformationTemplate = $this->getTransformationTemplate();

        //获取目标资源目录的模板信息项
        $transformationTemplateItems = $this->fetchTemplateItems($transformationTemplate);

        //获取比对规则
        $condition = $this->getCondition();

        //获取比对的资源目录集合
        $comparisonTemplateList = $this->fetchTemplateList($condition);

        foreach ($condition as $transformationItemIdentify => $patterns) {
            //验证目标资源目录模板信息项存在
            if (!isset($transformationTemplateItems[$transformationItemIdentify])) {
                Core::setLastError(
                    PARAMETER_FORMAT_ERROR,
                    array('pointer'=>'comparisonRulesTransformationItemNotExist')
                );
                return false;
            }
            foreach ($patterns as $pattern) {
                //验证比对资源目录存在
                if (!isset($comparisonTemplateList[$pattern['id']])) {
                    Core::setLastError(
                        PARAMETER_FORMAT_ERROR,
                        array('pointer'=>'comparisonRulesComparisonTemplateNotExist')
                    );
                    return false;
                }
                //如果比对资源目录为自然人,依据必须包含身份证号
                $comparisonTemplate = $comparisonTemplateList[$pattern['id']];
                if (!$this->validateBase($comparisonTemplate, $pattern['base'])) {
                    return false;
                }
                //验证比对依据合理性
                if (!$this->validateIdentify($transformationItemIdentify, $pattern['base'])) {
                    return false;
                }

                //获取比对资源目录模板信息项
                $comparisonTemplateItems = $this->fetchTemplateItems($comparisonTemplate);

                //验证比对资源目录模板信息项存在
                if (!isset($comparisonTemplateItems[$pattern['item']])) {
                    Core::setLastError(
                        PARAMETER_FORMAT_ERROR,
                        array('pointer'=>'comparisonRulesComparisonTemplateItemNotExist')
                    );
                    return false;
                }

                //验证信息项格式
                $transformationItem = $transformationTemplateItems[$transformationItemIdentify];
                $sourceItem = $comparisonTemplateItems[$pattern['item']];
                if (!$this->comparisonRuleComparison($transformationItem, $sourceItem)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * 验证比对依据合理性
        * 1. 如果比对信息项标识为"ZTMC",比对依据不能包含1(企业名称)
        * 2. 如果比对项为统一社会信用代码或身份证号,那比对依据不能为2(统一社会信用代码或身份证号)
     * @param string $transformationItemIdentify 比对信息项标识
     * @param string $base 比对依据
     * @return bool
     */
    protected function validateIdentify(string $transformationItemIdentify, array $base) : bool
    {
        return $this->identifyAndBaseDuplicateWithCompanyName($transformationItemIdentify, $base) &&
               $this->identifyAndBaseDuplicateWithIdentify($transformationItemIdentify, $base);
    }
    /**
     * 如果比对信息项标识为"ZTMC",比对依据不能包含1(企业名称)
     * @param string $transformationItemIdentify 比对信息项标识
     * @param string $base 比对依据
     * @return bool
     */
    protected function identifyAndBaseDuplicateWithCompanyName(string $transformationItemIdentify, array $base) : bool
    {
        if ($transformationItemIdentify == self::ITEM_SUBJECT_NAME_IDENTIFY) {
            if (in_array(IRule::COMPARISON_BASE['NAME'], $base)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'comparisonRulesBaseCannotName'));
                return false;
            }
        }

        return true;
    }

    /**
     * 如果比对项为统一社会信用代码或身份证号,那比对依据不能为2(统一社会信用代码或身份证号)
     * @param string $transformationItemIdentify 比对信息项标识
     * @param string $base 比对依据
     * @return bool
     */
    protected function identifyAndBaseDuplicateWithIdentify(string $transformationItemIdentify, array $base) : bool
    {
        if ($transformationItemIdentify == self::ITEM_ENTERPRISE_SUBJECT_IDENTIFY_IDENTIFY ||
            $transformationItemIdentify == self::ITEM_NATURAL_PERSON_SUBJECT_IDENTIFY_IDENTIFY
        ) {
            if (in_array(IRule::COMPARISON_BASE['IDENTIFY'], $base)) {
                Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer'=>'comparisonRulesBaseCannotIdentify'));
                return false;
            }
        }

        return true;
    }

    //实现父类的抽象方法
    protected function validateTransformation(ISearchDataAble $transformationSearchData) : bool
    {
        unset($transformationSearchData);
        return true;
    }

    /**
     * 初始化数据(实现父类的抽象方法)
     * @param ISearchDataAble $sourceSearchData 来源资源目录数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array
        * condition 比对条件
        * transformationItemsData 目标资源目录数据
        * transformationTemplateItems 目标资源目录模板信息项
     */
    protected function transformationInitialization(
        ISearchDataAble $sourceSearchData,
        ISearchDataAble $transformationSearchData
    ) : array {
        unset($sourceSearchData);
        //获取比对条件
        $condition = $this->getCondition();
        
        //获取目标资源目录数据信息项
        $transformationItemsData = $transformationSearchData->getItemsData()->getData();
        //获取目标资源目录
        $transformationTemplate = $transformationSearchData->getTemplate();
        //获取目标资源目录的模板信息项
        $transformationTemplateItems = $this->fetchTemplateItems($transformationTemplate);

        return array(
            'condition' => $condition,
            'transformationItemsData' => $transformationItemsData,
            'transformationTemplateItems' => $transformationTemplateItems
        );
    }

    //验证初始化数据格式
    protected function validateInitializationDataFormat(array $data) : bool
    {
        return (isset($data['condition']) &&
                isset($data['transformationItemsData']) &&
                isset($data['transformationTemplateItems']));
    }

    /**
     * 执行转换过程(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return array 失败的资源目录模板信息项
     */
    protected function transformationExecute(array $data, ISearchDataAble $transformationSearchData) : array
    {
        //验证初始化数据格式,false返回空数组
        if (!$this->validateInitializationDataFormat($data)) {
            return array();
        }
        
        $transformationItemsData = $data['transformationItemsData'];
        $transformationTemplateItems = $data['transformationTemplateItems'];
        $failureItems = array();

        //通过比对条件对数据进行比对
        foreach ($data['condition'] as $transformationIdentify => $patterns) {
            foreach ($patterns as $pattern) {
                //获取比对的资源目录数据列表
                $searchDataList = $this->fetchSearchDataList($pattern, $transformationSearchData);
                //获取比对信息项的数据
                $comparisonItemData = $this->filterExistItemsDataByIdentify($searchDataList, $transformationIdentify);

                $transformationItemData = isset($transformationItemsData[$transformationIdentify]) ?
                                        $transformationItemsData[$transformationIdentify] :
                                        '';
                //对数据进行比对,如果比对失败则返回失败资源目录模板信息项
                if ((empty($comparisonItemData) && empty($transformationItemData) ||
                    $comparisonItemData != $transformationItemData
                )) {
                    $failureItems[$transformationIdentify] =
                    isset($transformationTemplateItems[$transformationIdentify]) ?
                    $transformationTemplateItems[$transformationIdentify] :
                    array();
                }
            }
        }

        return array('failureItems' => $failureItems);
    }

    //验证执行后返回的数据格式
    protected function validateDataFormat(array $data) : bool
    {
        return isset($data['failureItems']);
    }

    /**
     * 判断并返回结果(实现父类的抽象方法)
     * @param array $data 初始化的数据
     * @param ISearchDataAble $transformationSearchData 目标资源目录数据
     * @return ISearchDataAble
     */
    protected function transformationResult(array $data, ISearchDataAble $transformationSearchData) : ISearchDataAble
    {
        //验证数据格式不正确,直接返回异常失败的目标资源目录数据
        if (!$this->validateDataFormat($data)) {
            $transformationSearchData = $this->failureDataException(
                $transformationSearchData,
                ErrorData::STATUS['PROGRAM_EXCEPTION']
            );
        }

        $failureItems = isset($data['failureItems']) ? $data['failureItems'] : array();

        //判断比对失败的资源目录信息项是否为空,为空则返回失败资源目录数据,不为空则返回完善的资源目录数据
        return empty($failureItems) ?
                $transformationSearchData :
                $this->failureData(
                    $transformationSearchData,
                    ErrorData::ERROR_TYPE['COMPARISON_FAILED'],
                    $failureItems
                );
    }
}

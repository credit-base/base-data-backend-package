<?php
namespace BaseData\Template\CommandHandler\WbjTemplate;

use BaseData\Template\Adapter\WbjTemplate\IWbjTemplateAdapter;

class MockEditWbjTemplateCommandHandler extends EditWbjTemplateCommandHandler
{
    public function getWbjTemplateRepository() : IWbjTemplateAdapter
    {
        return parent::getWbjTemplateRepository();
    }
}

<?php
namespace BaseData\WorkOrderTask\Model;

use Marmot\Core;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use BaseData\WorkOrderTask\Repository\WorkOrderTaskRepository;
use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter;

/**
 *
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ParentTaskTest extends TestCase
{
    private $parentTask;

    public function setUp()
    {
        $this->parentTask = new ParentTask();
    }

    public function tearDown()
    {
        parent::tearDown();
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->parentTask);
    }

    public function testImplementsIObjec()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->parentTask
        );
    }

    public function testGetRepository()
    {
        $parentTask = new MockParentTask();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter',
            $parentTask->getRepository()
        );
    }

    public function testGetWorkOrderTask()
    {
        $parentTask = new MockParentTask();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Model\WorkOrderTask',
            $parentTask->getWorkOrderTask()
        );
    }

    public function testGetWorkOrderTaskRepository()
    {
        $parentTask = new MockParentTask();
        $this->assertInstanceOf(
            'BaseData\WorkOrderTask\Adapter\WorkOrderTask\IWorkOrderTaskAdapter',
            $parentTask->getWorkOrderTaskRepository()
        );
    }

    public function testAdd()
    {
        //初始化
        $parentTask = $this->getMockBuilder(ParentTask::class)
                           ->setMethods(['addAction', 'bindTask'])
                           ->getMock();

        $parentTask->expects($this->exactly(1))
             ->method('addAction')
             ->willReturn(true);
        $parentTask->expects($this->exactly(1))
             ->method('bindTask')
             ->willReturn(true);
        
        //验证
        $result = $parentTask->add();
        $this->assertTrue($result);
    }

    public function testAddActionPublic()
    {
        //初始化
        $parentTask = $this->getMockBuilder(MockParentTask::class)
                           ->setMethods(['getRepository'])
                           ->getMock();

        //预言 IParentTaskAdapter
        $repository = $this->prophesize(IParentTaskAdapter::class);
        $repository->add(
            Argument::exact($parentTask)
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $parentTask->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $parentTask->addActionPublic();
        $this->assertTrue($result);
    }

    public function testRevoke()
    {
        //初始化
        $reason = 'reason';
        $parentTask = $this->getMockBuilder(ParentTask::class)
                           ->setMethods(['revokeWorkOrderTask', 'revokeAction'])
                           ->getMock();

        $parentTask->expects($this->exactly(1))
             ->method('revokeWorkOrderTask')
             ->with($reason)
             ->willReturn(true);
        $parentTask->expects($this->exactly(1))
             ->method('revokeAction')
             ->with($reason)
             ->willReturn(true);
        
        //验证
        $result = $parentTask->revoke($reason);
        $this->assertTrue($result);
    }

    public function testRevokeActionPublicFail()
    {
        //初始化
        $reason = 'reason';
        $parentTask = new MockParentTask();
        $parentTask->setFinishCount(1);
        
        //验证
        $result = $parentTask->revokeActionPublic($reason);
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testRevokeActionPublicSuccess()
    {
        //初始化
        $reason = 'reason';
        $parentTask = $this->getMockBuilder(MockParentTask::class)
                           ->setMethods(['setReason', 'setFinishCount', 'setUpdateTime', 'getRepository'])
                           ->getMock();

        $parentTask->expects($this->exactly(1))
             ->method('setReason')
             ->with($reason);
        $parentTask->expects($this->exactly(1))
             ->method('setFinishCount')
             ->with(count($parentTask->getAssignObjects()));
        $parentTask->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IParentTaskAdapter
        $repository = $this->prophesize(IParentTaskAdapter::class);
        $repository->edit(
            Argument::exact($parentTask),
            Argument::exact(
                [
                    'finishCount',
                    'reason',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $parentTask->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $parentTask->revokeActionPublic($reason);
        $this->assertTrue($result);
    }

    public function testUpdateFinishCount()
    {
        //初始化
        $finishCount = 0;
        $parentTask = $this->getMockBuilder(ParentTask::class)
                           ->setMethods(['setFinishCount', 'setUpdateTime', 'getRepository'])
                           ->getMock();

        $parentTask->expects($this->exactly(1))
             ->method('setFinishCount')
             ->with(intval($finishCount+1));
        $parentTask->expects($this->exactly(1))
             ->method('setUpdateTime')
             ->with(Core::$container->get('time'));

        //预言 IParentTaskAdapter
        $repository = $this->prophesize(IParentTaskAdapter::class);
        $repository->edit(
            Argument::exact($parentTask),
            Argument::exact(
                [
                    'finishCount',
                    'updateTime'
                ]
            )
        )->shouldBeCalledTimes(1)
         ->willReturn(true);

        //绑定
        $parentTask->expects($this->once())
             ->method('getRepository')
             ->willReturn($repository->reveal());
        
        //验证
        $result = $parentTask->updateFinishCount();
        $this->assertTrue($result);
    }

    public function testBindTask()
    {
        //初始化
        $parentTask = $this->getMockBuilder(MockParentTask::class)
                           ->setMethods(['getWorkOrderTaskRepository', 'getWorkOrderTask'])
                           ->getMock();
        
        $userGroup = \BaseData\UserGroup\Utils\MockFactory::generateUserGroup(1);
        $assignObjects = array($userGroup);
        $wbjTemplate = \BaseData\Template\Utils\WbjTemplateMockFactory::generateWbjTemplate(1);
        $parentTask->setAssignObjects($assignObjects);
        $parentTask->setTemplate($wbjTemplate);

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->setParentTask(Argument::exact($parentTask))->shouldBeCalledTimes(1);
        $workOrderTask->setTemplate(Argument::exact($parentTask->getTemplate()))->shouldBeCalledTimes(1);
        $workOrderTask->setAssignObject(Argument::exact($userGroup))->shouldBeCalledTimes(1);
        
        $parentTask->expects($this->once())->method('getWorkOrderTask')->willReturn($workOrderTask->reveal());

        $workOrderTaskRepository = $this->prophesize(WorkOrderTaskRepository::class);
        $workOrderTaskRepository->add(Argument::exact($workOrderTask->reveal()))
                                ->shouldBeCalledTimes(1)
                                ->willReturn(true);

        $parentTask->expects($this->once())
            ->method('getWorkOrderTaskRepository')
            ->willReturn($workOrderTaskRepository->reveal());
        
        //验证
        $result = $parentTask->bindTaskPublic();
        $this->assertTrue($result);
    }

    public function testRevokeWorkOrderTask()
    {
        //初始化
        $parentTask = $this->getMockBuilder(MockParentTask::class)
                           ->setMethods(['fetchWorkOrderTask'])
                           ->getMock();
        
        $reason = 'reason';

        $workOrderTask = $this->prophesize(WorkOrderTask::class);
        $workOrderTask->revoke(Argument::exact($reason))->shouldBeCalledTimes(1)->willReturn(true);

        $parentTask->expects($this->exactly(2))->method('fetchWorkOrderTask')->willReturn([$workOrderTask->reveal()]);

        //验证
        $result = $parentTask->revokeWorkOrderTaskPublic($reason);
        $this->assertTrue($result);
    }

    private function initialFetchWorkOrderTask($count, $list)
    {
        $this->parentTask = $this->getMockBuilder(MockParentTask::class)
                           ->setMethods(['getWorkOrderTaskRepository'])
                           ->getMock();

        $id = 1;
        $this->parentTask->setId($id);
        $filter['parentTask'] = $id;
        
        $repository = $this->prophesize(WorkOrderTaskRepository::class);

        $repository->filter(Argument::exact($filter))->shouldBeCalledTimes(1)->willReturn([$list, $count]);

        $this->parentTask->expects($this->any())
                   ->method('getWorkOrderTaskRepository')
                   ->willReturn($repository->reveal());
    }

    public function testFetchWorkOrderTaskFailure()
    {
        $this->initialFetchWorkOrderTask(0, array());

        $result = $this->parentTask->fetchWorkOrderTaskPublic();

        $this->assertEquals(array(), $result);
    }

    public function testFetchWorkOrderTaskSuccess()
    {
        $list = array('workOrderTaskList');
        
        $this->initialFetchWorkOrderTask(1, $list);
        
        $result = $this->parentTask->fetchWorkOrderTaskPublic();

        $this->assertEquals($list, $result);
    }
}

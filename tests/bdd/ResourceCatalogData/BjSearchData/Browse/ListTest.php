<?php
namespace BaseData\ResourceCatalogData\BjSearchData\Browse;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\BjSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\BjSearchDataDbTranslator;

/**
 * @Feature: 我是委办局工作人员,当我需要查看我的资源目录数据时,在资源目录数据子系统下的本级数据中,可以查看到我的所有资源目录数据,
 *           通过列表形式查看我所有的资源目录数据,以便于我可以看到我的所有资源目录数据
 * @Scenario: 查看资源目录数据列表
 */
class ListTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_bj_template');
        $this->clear('pcore_bj_items_data');
        $this->clear('pcore_bj_search_data');
    }

    /**
     * @Given: 存在资源目录数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet([
            'pcore_bj_template' => $this->bjTemplate(),
            'pcore_bj_items_data' => $this->bjSearchData()['pcore_bj_items_data'],
            'pcore_bj_search_data' => $this->bjSearchData()['pcore_bj_search_data']
        ]);
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchBjSearchDataList()
    {
        $repository = new BjSearchDataRepository();

        list($bjSearchDataList, $count) = $repository->filter([]);
        
        unset($count);

        return $bjSearchDataList;
    }

    /**
     * @Then: 我可以看到资源目录数据的全部信息
     */
    public function testViewBjSearchDataList()
    {
        $setBjSearchDataList = $this->bjSearchData()['pcore_bj_search_data'];

        foreach ($setBjSearchDataList as $key => $setBjSearchData) {
            unset($setBjSearchData);
            $setBjSearchDataList[$key]['bj_search_data_id'] = $key +1;
        }

        $bjSearchDataList = $this->fetchBjSearchDataList();

        $translator = new BjSearchDataDbTranslator();
        foreach ($bjSearchDataList as $bjSearchData) {
            $bjSearchDataArray[] = $translator->objectToArray($bjSearchData);
        }

        $this->assertEquals($bjSearchDataArray, $setBjSearchDataList);
    }
}

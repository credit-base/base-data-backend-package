<?php
namespace BaseData\ResourceCatalogData\WbjSearchData\Search;

use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;

use tests\DbTrait;

use BaseData\ResourceCatalogData\SetDataTrait;
use BaseData\ResourceCatalogData\Repository\WbjSearchDataRepository;
use BaseData\ResourceCatalogData\Translator\WbjSearchDataDbTranslator;

/**
 * @Feature: 我是前台用户,当我需要查看特定条件资源目录数据时,在门户网portal中,可以搜索我需要的所有资源目录数据
 *           通过列表的形式展现我搜索的所有资源目录数据,以便于我可以快速查看到我需要的资源目录数据
 * @Scenario: 前台用户只能查看有效期内的数据
 */
class ExpirationDateTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataTrait, SearchTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_wbj_template');
        $this->clear('pcore_wbj_items_data');
        $this->clear('pcore_wbj_search_data');
    }

    /**
     * @When: 当我查看资源目录数据列表时
     */
    public function fetchWbjSearchDataList()
    {
        $repository = new WbjSearchDataRepository();
 
        $filter['expirationDate'] = time();

        list($wbjSearchDataList, $count) = $repository->filter($filter);
        
        unset($count);

        return $wbjSearchDataList;
    }

    /**
     * @Then: 我可以看到状态为启用状态的所有资源目录数据
     */
    public function testViewWbjSearchDataList()
    {
        $setWbjSearchDataList = $this->wbjSearchData()['pcore_wbj_search_data'];

        foreach ($setWbjSearchDataList as $key => $setWbjSearchData) {
            unset($setWbjSearchData);
            $setWbjSearchDataList[$key]['wbj_search_data_id'] = $key +1;
        }

        $wbjSearchDataList = $this->fetchWbjSearchDataList();
        $translator = new WbjSearchDataDbTranslator();
        foreach ($wbjSearchDataList as $wbjSearchData) {
            $wbjSearchDataArray[] = $translator->objectToArray($wbjSearchData);
        }

        $this->assertEquals($wbjSearchDataArray, $setWbjSearchDataList);

        foreach ($wbjSearchDataArray as $wbjSearchData) {
            $this->assertLessThan($wbjSearchData['expiration_date'], time());
        }
    }
}

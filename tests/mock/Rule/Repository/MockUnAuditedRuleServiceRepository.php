<?php
namespace BaseData\Rule\Repository;

use BaseData\Rule\Adapter\UnAuditedRuleService\IUnAuditedRuleServiceAdapter;

class MockUnAuditedRuleServiceRepository extends UnAuditedRuleServiceRepository
{
    public function getAdapter() : IUnAuditedRuleServiceAdapter
    {
        return parent::getAdapter();
    }

    public function getActualAdapter() : IUnAuditedRuleServiceAdapter
    {
        return parent::getActualAdapter();
    }

    public function getMockAdapter() : IUnAuditedRuleServiceAdapter
    {
        return parent::getMockAdapter();
    }
}

<?php
namespace BaseData\ResourceCatalogData\Adapter\BjItemsData;

use BaseData\ResourceCatalogData\Model\BjItemsData;

interface IBjItemsDataAdapter
{
    public function fetchOne($id) : BjItemsData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(BjItemsData $bjItemsData) : bool;
}

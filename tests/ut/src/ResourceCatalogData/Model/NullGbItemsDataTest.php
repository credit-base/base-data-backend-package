<?php
namespace BaseData\ResourceCatalogData\Model;

use PHPUnit\Framework\TestCase;

class NullGbItemsDataTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = NullGbItemsData::getInstance();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsGbItemsData()
    {
        $this->assertInstanceof('BaseData\ResourceCatalogData\Model\GbItemsData', $this->stub);
    }

    public function testImplementIsNull()
    {
        $this->assertInstanceof('Marmot\Interfaces\INull', $this->stub);
    }
}

<?php
namespace BaseData\WorkOrderTask\Repository;

use Marmot\Framework\Classes\Repository;

use BaseData\WorkOrderTask\Model\ParentTask;
use BaseData\WorkOrderTask\Adapter\ParentTask\IParentTaskAdapter;
use BaseData\WorkOrderTask\Adapter\ParentTask\ParentTaskDbAdapter;
use BaseData\WorkOrderTask\Adapter\ParentTask\ParentTaskMockAdapter;

class ParentTaskRepository extends Repository implements IParentTaskAdapter
{
    private $adapter;

    public function __construct()
    {
        $this->adapter = new ParentTaskDbAdapter();
    }

    public function __destruct()
    {
        unset($this->adapter);
    }

    public function setAdapter(IParentTaskAdapter $adapter)
    {
        $this->adapter = $adapter;
    }
    
    protected function getActualAdapter() : IParentTaskAdapter
    {
        return $this->adapter;
    }

    protected function getMockAdapter() : IParentTaskAdapter
    {
        return new ParentTaskMockAdapter();
    }

    public function fetchOne($id) : ParentTask
    {
        return $this->getAdapter()->fetchOne($id);
    }

    public function fetchList(array $ids) : array
    {
        return $this->getAdapter()->fetchList($ids);
    }

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 20
    ) : array {
        return $this->getAdapter()->filter($filter, $sort, $offset, $size);
    }

    public function add(ParentTask $parentTask) : bool
    {
        return $this->getAdapter()->add($parentTask);
    }

    public function edit(ParentTask $parentTask, array $keys = array()) : bool
    {
        return $this->getAdapter()->edit($parentTask, $keys);
    }
}

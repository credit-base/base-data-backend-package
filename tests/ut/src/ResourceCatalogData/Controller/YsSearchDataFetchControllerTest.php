<?php
namespace BaseData\ResourceCatalogData\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;

use BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter;
use BaseData\ResourceCatalogData\Model\NullYsSearchData;
use BaseData\ResourceCatalogData\Model\YsSearchData;
use BaseData\ResourceCatalogData\View\YsSearchDataView;

class YsSearchDataFetchControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(YsSearchDataFetchController::class)
                           ->setMethods(
                               ['getRepository', 'renderView', 'displayError']
                           )
                           ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testImplementsIOperateController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Common\Controller\IFetchController',
            $this->controller
        );
    }

    public function testGetRepository()
    {
        $controller = new MockYsSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Adapter\YsSearchData\IYsSearchDataAdapter',
            $controller->getRepository()
        );
    }

    public function testGenerateView()
    {
        $controller = new MockYsSearchDataFetchController();

        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\View\YsSearchDataView',
            $controller->generateView('')
        );
    }

    public function testGetResourceName()
    {
        $controller = new MockYsSearchDataFetchController();

        $this->assertEquals(
            'ysSearchData',
            $controller->getResourceName()
        );
    }
}

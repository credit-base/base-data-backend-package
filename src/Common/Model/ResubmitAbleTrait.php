<?php
namespace BaseData\Common\Model;

use Marmot\Core;

trait ResubmitAbleTrait
{
    public function resubmit() : bool
    {
        if (!$this->isReject()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'status'));
            return false;
        }

        return $this->resubmitAction();
    }

    abstract protected function isReject() : bool;

    abstract protected function resubmitAction() : bool;
}

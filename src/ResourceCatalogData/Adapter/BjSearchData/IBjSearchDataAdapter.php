<?php
namespace BaseData\ResourceCatalogData\Adapter\BjSearchData;

use BaseData\ResourceCatalogData\Model\BjSearchData;

interface IBjSearchDataAdapter
{
    public function fetchOne($id) : BjSearchData;

    public function fetchList(array $ids) : array;

    public function filter(
        array $filter = array(),
        array $sort = array(),
        int $offset = 0,
        int $size = 0
    ) : array;

    public function add(BjSearchData $bjSearchData) : bool;

    public function edit(BjSearchData $bjSearchData, array $keys = array()) : bool;
}

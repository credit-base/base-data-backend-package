<?php
namespace BaseData\ResourceCatalogData\Model;

use BaseData\ResourceCatalogData\Adapter\GbItemsData\IGbItemsDataAdapter;
use BaseData\ResourceCatalogData\Adapter\GbSearchData\IGbSearchDataAdapter;

class MockGbSearchData extends GbSearchData
{
    public function getRepository() : IGbSearchDataAdapter
    {
        return parent::getRepository();
    }

    public function getGbItemsDataRepository() : IGbItemsDataAdapter
    {
        return parent::getGbItemsDataRepository();
    }

    public function addItemsData() : bool
    {
        return parent::addItemsData();
    }

    public function updateStatus(int $status) : bool
    {
        return parent::updateStatus($status);
    }
}

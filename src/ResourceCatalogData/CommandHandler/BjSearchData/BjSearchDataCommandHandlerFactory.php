<?php
namespace BaseData\ResourceCatalogData\CommandHandler\BjSearchData;

use Marmot\Interfaces\ICommand;
use Marmot\Interfaces\ICommandHandler;
use Marmot\Interfaces\ICommandHandlerFactory;
use Marmot\Framework\Classes\NullCommandHandler;

class BjSearchDataCommandHandlerFactory implements ICommandHandlerFactory
{
    const MAPS = array(
        'BaseData\ResourceCatalogData\Command\BjSearchData\ConfirmBjSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\BjSearchData\ConfirmBjSearchDataCommandHandler',
        'BaseData\ResourceCatalogData\Command\BjSearchData\DisableBjSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\BjSearchData\DisableBjSearchDataCommandHandler',
        'BaseData\ResourceCatalogData\Command\BjSearchData\DeleteBjSearchDataCommand'=>
        'BaseData\ResourceCatalogData\CommandHandler\BjSearchData\DeleteBjSearchDataCommandHandler'
    );

    public function getHandler(ICommand $command) : ICommandHandler
    {
        $commandClass = get_class($command);
        $commandHandler = isset(self::MAPS[$commandClass]) ? self::MAPS[$commandClass] : '';

        return class_exists($commandHandler) ? new $commandHandler : NullCommandHandler::getInstance();
    }
}

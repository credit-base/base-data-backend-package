<?php
namespace BaseData\Rule\UnAuditedRuleService\Fetch;

use Marmot\Core;
use tests\DbTrait;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\ArrayDataSet;

use BaseData\Rule\RuleUtils;
use BaseData\Rule\Model\IRule;
use BaseData\Rule\SetDataApiTrait;
use BaseData\Rule\Model\UnAuditedRuleService;

use BaseData\Template\Model\Template;
use BaseData\Template\TemplateUtils;
use BaseData\Template\SetDataApiTrait as TemplateSetDataApiTrait;

use BaseData\Common\Model\IApproveAble;

/**
 * 测试规则审核接口根据检索条件查询数据
 */
class FilterTest extends TestCase
{
    use TestCaseTrait, DbTrait, SetDataApiTrait, RuleUtils, TemplateUtils, TemplateSetDataApiTrait;

    public function tearDown()
    {
        parent::tearDown();
        $this->clear('pcore_apply_form_rule');
        $this->clear('pcore_gb_template');
        $this->clear('pcore_wbj_template');
    }

    /**
     * 存在多条规则审核数据
     */
    protected function getDataSet()
    {
        return new ArrayDataSet(
            [
                'pcore_wbj_template' => $this->wbjTemplate(),
                'pcore_gb_template' => $this->gbTemplate(),
                'pcore_apply_form_rule' => $this->applyFormRule()
            ]
        );
    }

    public function testViewUnAuditedRuleService()
    {
        $setUnAuditedRuleServiceList = $this->applyFormRule();
        $setGbTemplateList = $this->gbTemplate();
        $setWbjTemplateList = $this->wbjTemplate();

        $client = new Client(['base_uri' => Core::$container->get('backend.service')]);
        $response = $client->request(
            'GET',
            'unAuditedRules?filter[transformationTemplate]=1&filter[userGroup]=1&filter[sourceTemplate]=1&
            filter[applyStatus]='.IApproveAble::APPLY_STATUS['PENDING'].
            '&filter[transformationCategory]='.Template::CATEGORY['GB'].
            '&filter[operationType]='.IApproveAble::OPERATION_TYPE['ADD'].
            '&filter[sourceCategory]='.Template::CATEGORY['WBJ'].
            '&filter[relationId]=0&filter[excludeTransformationCategory]='.Template::CATEGORY['BJ'].
            '&include=transformationTemplate,sourceTemplate',
            ['headers'=> Core::$container->get('client.headers')]
        );

        $contents = $response->getBody()->getContents();
        $contents = json_decode($contents, true);

        $meta = $contents['meta'];
        $this->assertEquals(1, $meta['count']);

        $data = $contents['data'];
        $included = $contents['included'];

        foreach ($data as $key => $unAuditedRuleService) {
            $this->unAuditedRuleCompareData($setUnAuditedRuleServiceList[$key], $unAuditedRuleService);
        }

        $included = $contents['included'];

        $this->gbTemplateCompareData($setGbTemplateList[0], $included[0]);
        $this->wbjTemplateCompareData($setWbjTemplateList[0], $included[1]);
    }
}

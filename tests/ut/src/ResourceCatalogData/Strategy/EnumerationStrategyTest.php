<?php
namespace BaseData\ResourceCatalogData\Strategy;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class EnumerationStrategyTest extends TestCase
{
    private $enumerationStub;
    
    public function setUp()
    {
        $this->enumerationStub = new EnumerationStrategy();
    }

    public function tearDown()
    {
        unset($this->enumerationStub);
    }

    public function testImplementsIDataStrategy()
    {
        $this->assertInstanceOf(
            'BaseData\ResourceCatalogData\Strategy\IDataStrategy',
            $this->enumerationStub
        );
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($actual, $rule, $expected)
    {
        $result = $this->enumerationStub->validate($actual, $rule);

        if (!$expected) {
            $this->assertFalse($result);
            $this->assertEquals('data', Core::getLastError()->getSource()['pointer']);
            $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        }
        
        if ($expected) {
            $this->assertTrue($result);
        }
    }

    public function validateProvider()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $rule['options'] = $faker->words(3, false);

        return array(
            array($faker->randomElement($rule['options']), $rule, true),
            array(array($faker->date('Ymd')), $rule, false)
        );
    }

    public function testMock()
    {
        $faker = \Faker\Factory::create('zh_CN');

        $rule['options'] = $faker->words(3, false);

        $result = $this->enumerationStub->mock($rule);
        
        $this->assertIsString($result);
    }
}
